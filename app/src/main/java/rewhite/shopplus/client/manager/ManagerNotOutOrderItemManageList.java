package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.NotOutOrderItemManageList;

public class ManagerNotOutOrderItemManageList {
    private static final String TAG = "ManagerNotOutOrderItemManageList";

    @SerializedName("Data")
    public List<NotOutOrderItemManageList> NotOutOrderItemManageList = new ArrayList<>();

    public List<NotOutOrderItemManageList> getNotOutOrderItemManageList() {
        return NotOutOrderItemManageList;
    }

    public void setNotOutOrderItemManageList(List<NotOutOrderItemManageList> notOutOrderItemManageList) {
        this.NotOutOrderItemManageList = notOutOrderItemManageList;
    }

    private static ManagerNotOutOrderItemManageList mInstance = null;

    public static ManagerNotOutOrderItemManageList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerNotOutOrderItemManageList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerNotOutOrderItemManageList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
