package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetVisitOrderList;

public class ManagerGetVisitOrder {
    private static final String TAG = "ManagerGetVisitOrder";

    @SerializedName("ResultCode")
    private String ResultCode;
    @SerializedName("Data")
    public List<GetVisitOrderList> itemGetVisitOrder = new ArrayList<>();

    public List<GetVisitOrderList> getGetVisitOrder() {
        return itemGetVisitOrder;
    }

    public void setGetVisitOrder(List<GetVisitOrderList> storeUserInfoItemList) {
        this.itemGetVisitOrder = storeUserInfoItemList;
    }

    private static ManagerGetVisitOrder mInstance = null;

    public static ManagerGetVisitOrder getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetVisitOrder.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetVisitOrder();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }

    public String getResultCode() {
        return ResultCode;
    }
}
