package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.StoreItemList;

/**
 * 가맹점 상품 리스트
 */
public class ManagerStoreItemList {
    private static final String TAG = "ManagerStoreItemList";

    @SerializedName("Data")
    public List<StoreItemList> storeItemList = new ArrayList<>();

    public List<StoreItemList> getStoreItemList() {
        return storeItemList;
    }

    public void setStoreItemList(List<StoreItemList> storeItemList) {
        this.storeItemList = storeItemList;
    }

    private static ManagerStoreItemList mInstance = null;

    public static ManagerStoreItemList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerStoreItemList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerStoreItemList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
