package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetMonthSalesStatList;

/**
 * 일별 매출현황(기본기간:이번달) ManagerClass
 */
public class ManagerGetMonthSalesStatList {
    private static final String TAG = "ManagerGetHandleItemList";

    @SerializedName("Data")
    public List<GetMonthSalesStatList> itemGetMonthSalesStatList = new ArrayList<>();

    public List<GetMonthSalesStatList> getGetMonthSalesStatList() {
        return itemGetMonthSalesStatList;
    }

    public void setGetHandleItemList(List<GetMonthSalesStatList> storeGetMonthSalesStatList) {
        this.itemGetMonthSalesStatList = storeGetMonthSalesStatList;
    }

    private static ManagerGetMonthSalesStatList mInstance = null;

    public static ManagerGetMonthSalesStatList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetMonthSalesStatList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetMonthSalesStatList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}