package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.ChangePasswordModel;

public class ManagerResponesCode {
    private static final String TAG = "ManagerResponesCode";

    @SerializedName("Data")
    public List<ChangePasswordModel> itemChangePasswordModel = new ArrayList<>();

    public List<ChangePasswordModel> getChangePasswordModel() {
        return itemChangePasswordModel;
    }

    public void setChangePasswordModel(List<ChangePasswordModel> getChangePasswordModel) {
        this.itemChangePasswordModel = getChangePasswordModel;
    }

    private static ManagerResponesCode mInstance = null;

    public static ManagerResponesCode getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerResponesCode.class) {
                if (mInstance == null) {
                    mInstance = new ManagerResponesCode();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}