package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetPrePaidUseHistory;

/**
 * 가맹점 회원 선충전금 충전내역 ManagerClass
 */
public class ManagerGetPrePaidChargeHistory {
    private static final String TAG = "ManagerGetPrePaidChargeHistory";

    @SerializedName("Data")
    public List<GetPrePaidUseHistory> getPrePaidUseHistory = new ArrayList<>();

    public List<GetPrePaidUseHistory> getManagerGetPrePaidUseHistory() {
        return getPrePaidUseHistory;
    }

    public void setManagerGetPrePaidUseHistory(List<GetPrePaidUseHistory> prePaidUseHistory) {
        this.getPrePaidUseHistory = prePaidUseHistory;
    }

    private static ManagerGetPrePaidChargeHistory mInstance = null;

    public static ManagerGetPrePaidChargeHistory getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetPrePaidChargeHistory.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetPrePaidChargeHistory();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}