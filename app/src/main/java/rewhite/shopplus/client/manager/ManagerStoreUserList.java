package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.StoreUserInfoList;

/**
 * 가맹점 회원정보 리스트
 */
public class ManagerStoreUserList {
    private static final String TAG = "ManagerStoreUserList";

    @SerializedName("Data")
    public List<StoreUserInfoList> storeUserInfoItemLists = new ArrayList<>();

    public List<StoreUserInfoList> getStoreUserInfo() {
        return storeUserInfoItemLists;
    }

    public void setStoreUserInfo(List<StoreUserInfoList> storeUserInfoItemList) {
        this.storeUserInfoItemLists = storeUserInfoItemList;
    }

    private static ManagerStoreUserList mInstance = null;

    public static ManagerStoreUserList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerStoreUserList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerStoreUserList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
