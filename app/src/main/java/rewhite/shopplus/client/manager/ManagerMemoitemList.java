package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.MemoItemList;

/**
 * 가맹점 상품 리스트
 */
public class ManagerMemoitemList {
    private static final String TAG = "ManagerStoreItemList";

    @SerializedName("Data")
    public List<MemoItemList> memoItemList = new ArrayList<>();

    public List<MemoItemList> getStoreItemList() {
        return memoItemList;
    }

    public void setStoreItemList(List<MemoItemList> memoItemList) {
        this.memoItemList = memoItemList;
    }

    private static ManagerMemoitemList mInstance = null;

    public static ManagerMemoitemList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerMemoitemList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerMemoitemList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
