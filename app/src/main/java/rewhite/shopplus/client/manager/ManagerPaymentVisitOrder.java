package rewhite.shopplus.client.manager;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetVisitOrderList;

public class ManagerPaymentVisitOrder {
    private static final String TAG = "ManagerPaymentVisitOrder";

    public List<GetVisitOrderList> itemGetVisitOrder = new ArrayList<>();

    public List<GetVisitOrderList> getGetVisitOrder() {
        return itemGetVisitOrder;
    }

    public void setGetVisitOrder(List<GetVisitOrderList> storeUserInfoItemList) {
        this.itemGetVisitOrder = storeUserInfoItemList;
    }

    private static ManagerPaymentVisitOrder mInstance = null;

    public static ManagerPaymentVisitOrder getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerPaymentVisitOrder.class) {
                if (mInstance == null) {
                    mInstance = new ManagerPaymentVisitOrder();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
