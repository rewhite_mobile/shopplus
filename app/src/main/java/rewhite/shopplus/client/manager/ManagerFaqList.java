package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.FaqListModel;

/**
 * FAQ ManagerClass
 */
public class ManagerFaqList {
    private static final String TAG = "ManagerFaqList";
    @SerializedName("Data")
    public List<FaqListModel> managerFaqListModel = new ArrayList<>();

    public List<FaqListModel> getManagerFaqList() {
        return managerFaqListModel;
    }

    public void setManagerFaqList(List<FaqListModel> getFaqListModel) {
        this.managerFaqListModel = getFaqListModel;
    }

    private static ManagerFaqList mInstance = null;

    public static ManagerFaqList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerFaqList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerFaqList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}