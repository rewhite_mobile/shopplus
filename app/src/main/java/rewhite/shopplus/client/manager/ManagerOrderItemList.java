package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetOrderItemList;

/**
 * 주문 품목 조회(주문ID)
 */
public class ManagerOrderItemList {
    private static final String TAG = "ManagerOrderItemList";

    @SerializedName("Data")
    public List<GetOrderItemList> managerManagerAreaCity = new ArrayList<>();

    public List<GetOrderItemList> getManagerAreaCity() {
        return managerManagerAreaCity;
    }

    public void setManagerAreaCity(List<GetOrderItemList> getManagerAreaCity) {
        this.managerManagerAreaCity = getManagerAreaCity;
    }

    private static ManagerOrderItemList mInstance = null;

    public static ManagerOrderItemList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerOrderItemList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerOrderItemList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
