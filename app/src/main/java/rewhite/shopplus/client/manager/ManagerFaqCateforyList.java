package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.FaqCategoryListModel;

public class ManagerFaqCateforyList {
    private static final String TAG = "ManagerFaqCateforyList";

    @SerializedName("Data")
    public List<FaqCategoryListModel> itemFaqCategoryListModel = new ArrayList<>();

    public List<FaqCategoryListModel> getFaqCategoryListModel() {
        return itemFaqCategoryListModel;
    }

    public void setFaqCategoryListModel(List<FaqCategoryListModel> getFaqCategoryListModel) {
        this.itemFaqCategoryListModel = getFaqCategoryListModel;
    }

    private static ManagerFaqCateforyList mInstance = null;

    public static ManagerFaqCateforyList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerFaqCateforyList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerFaqCateforyList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
