package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetMaxTagNoModel;

/**
 * 최근사용한 택번호 조회 ManagerClass
 */
public class ManagerMaxTagModel {
    private static final String TAG = "ManagerAreaCity";

    @SerializedName("Data")
    public List<GetMaxTagNoModel> managerGetMaxTagNoModel = new ArrayList<>();

    public List<GetMaxTagNoModel> getManagerGetMaxTagNoModel() {
        return managerGetMaxTagNoModel;
    }

    public void setManagerGetMaxTagNoModel(List<GetMaxTagNoModel> getMaxTagNoModel) {
        this.managerGetMaxTagNoModel = getMaxTagNoModel;
    }

    private static ManagerMaxTagModel mInstance = null;

    public static ManagerMaxTagModel getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerMaxTagModel.class) {
                if (mInstance == null) {
                    mInstance = new ManagerMaxTagModel();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
