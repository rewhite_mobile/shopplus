package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.ManagerGetPaymentInfoModel;

/**
 * 결제 상세 정보 리스트 조회 ManagerClass
 */
public class ManagerGetPaymentInfoList {
    private static final String TAG = "ManagerGetPaymentInfoList";

    @SerializedName("Data")
    public List<ManagerGetPaymentInfoModel> canGetOrderList = new ArrayList<>();

    public List<ManagerGetPaymentInfoModel> getGetOrderList() {
        return canGetOrderList;
    }

    public void setGetOrderList(List<ManagerGetPaymentInfoModel> getGetOrderList) {
        this.canGetOrderList = getGetOrderList;
    }

    private static ManagerGetPaymentInfoList mInstance = null;

    public static ManagerGetPaymentInfoList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetPaymentInfoList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetPaymentInfoList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}