package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.LaundryCategoryItemList;

public class ManagerLaundryCategory {
    private static final String TAG = "ManagerLaundryCategory";

    @SerializedName("Data")
    public List<LaundryCategoryItemList> laundryCategoryItemLists = new ArrayList<>();

    public List<LaundryCategoryItemList> getLaundryCategoty() {
        return laundryCategoryItemLists;
    }

    public void setLaundryCategoty(List<LaundryCategoryItemList> laundryCategoryItemList) {
        this.laundryCategoryItemLists = laundryCategoryItemList;
    }

    private static ManagerLaundryCategory mInstance = null;

    public static ManagerLaundryCategory getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerLaundryCategory.class) {
                if (mInstance == null) {
                    mInstance = new ManagerLaundryCategory();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
