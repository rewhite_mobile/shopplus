package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetStoreManageInfo;

/**
 * 매장 관리 정보 조회
 */
public class ManagerGetStoreManageInfo {
    private static final String TAG = "ManagerGetStoreManageInfo";

    @SerializedName("Data")
    public List<GetStoreManageInfo> itemStoreManageInfoList = new ArrayList<>();

    public List<GetStoreManageInfo> getStoreManageInfoList() {
        return itemStoreManageInfoList;
    }

    public void setGetVisitOrder(List<GetStoreManageInfo> storeManageInfoList) {
        this.itemStoreManageInfoList = storeManageInfoList;
    }

    private static ManagerGetStoreManageInfo mInstance = null;

    public static ManagerGetStoreManageInfo getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetStoreManageInfo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetStoreManageInfo();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}