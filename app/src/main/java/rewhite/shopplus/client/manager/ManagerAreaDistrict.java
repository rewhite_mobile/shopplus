package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.AreaDistrictListModel;

/**
 * 가입신청 지역(시/도)조회 ManagerClass
 */
public class ManagerAreaDistrict {
    private static final String TAG = "ManagerAreaDistrict";

    @SerializedName("Data")
    public List<AreaDistrictListModel> managerAreaDistrictListModel = new ArrayList<>();

    public List<AreaDistrictListModel> getAreaDistrictListModel() {
        return managerAreaDistrictListModel;
    }

    public void setAreaDistrictListModel(List<AreaDistrictListModel> getAreaDistrictListModel) {
        this.managerAreaDistrictListModel = getAreaDistrictListModel;
    }

    private static ManagerAreaDistrict mInstance = null;

    public static ManagerAreaDistrict getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerAreaDistrict.class) {
                if (mInstance == null) {
                    mInstance = new ManagerAreaDistrict();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
