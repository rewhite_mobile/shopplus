package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.data.dto.ItemPaymentStoreOrderItem;

public class ManagerItemPaymentStoreOrder {
    private static final String TAG = "ManagerItemPaymentStoreOrder";

    @SerializedName("Data")
    public List<ItemPaymentStoreOrderItem> itemAdditionalData = new ArrayList<>();

    public List<ItemPaymentStoreOrderItem> getItemAdditionalData() {
        return itemAdditionalData;
    }

    public void setItemAdditionalData(List<ItemPaymentStoreOrderItem> storeUserInfoItemList) {
        this.itemAdditionalData = storeUserInfoItemList;
    }

    private static ManagerItemPaymentStoreOrder mInstance = null;

    public static ManagerItemPaymentStoreOrder getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerItemPaymentStoreOrder.class) {
                if (mInstance == null) {
                    mInstance = new ManagerItemPaymentStoreOrder();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}