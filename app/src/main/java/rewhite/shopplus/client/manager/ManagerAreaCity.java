package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.AreaCityListModel;

/**
 * 가입신청 지역(시/도)조회 ManagerClass
 */
public class ManagerAreaCity {
    private static final String TAG = "ManagerAreaCity";

    @SerializedName("Data")
    public List<AreaCityListModel> managerManagerAreaCity = new ArrayList<>();

    public List<AreaCityListModel> getManagerAreaCity() {
        return managerManagerAreaCity;
    }

    public void setManagerAreaCity(List<AreaCityListModel> getManagerAreaCity) {
        this.managerManagerAreaCity = getManagerAreaCity;
    }

    private static ManagerAreaCity mInstance = null;

    public static ManagerAreaCity getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerAreaCity.class) {
                if (mInstance == null) {
                    mInstance = new ManagerAreaCity();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
