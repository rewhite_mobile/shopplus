package rewhite.shopplus.client.manager;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.StoreLoginModel;

/**
 * 로그인 토큰 롼리 하기 위한 ManagerClass
 */
public class ManagerAuthStoreLogin {
    private static final String TAG = "ManagerAuthStoreLogin";
    public List<StoreLoginModel> managerAuthStoreLogin = new ArrayList<>();

    public List<StoreLoginModel> getmanagerAuthStoreLogin() {
        return managerAuthStoreLogin;
    }

    public void setmanagerAuthStoreLogin(List<StoreLoginModel> getManagerAuthStoreLogin) {
        this.managerAuthStoreLogin = getManagerAuthStoreLogin;
    }

    private static ManagerAuthStoreLogin mInstance = null;

    public static ManagerAuthStoreLogin getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerAuthStoreLogin.class) {
                if (mInstance == null) {
                    mInstance = new ManagerAuthStoreLogin();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}