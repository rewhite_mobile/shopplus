package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.CancelAvailOrder;

/**
 * 취소 가능한 주문 품목 조회(for 품목취소) ManagerClass
 */
public class ManagerCancelAvailOrderList {
    private static final String TAG = "ManagerCancelAvailOrderList";

    @SerializedName("ResultCode")
    public String ResultCode;

    @SerializedName("Data")
    public List<CancelAvailOrder> cancelAvailOrders = new ArrayList<>();

    public List<CancelAvailOrder> getcancelAvailOrder() {
        return cancelAvailOrders;
    }

    public void setcancelAvailOrder(List<CancelAvailOrder> getCancelAvailOrder) {
        this.cancelAvailOrders = getCancelAvailOrder;
    }

    private static ManagerCancelAvailOrderList mInstance = null;

    public static ManagerCancelAvailOrderList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerCancelAvailOrderList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerCancelAvailOrderList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }

    public String getResultCode() {
        return ResultCode;
    }
}