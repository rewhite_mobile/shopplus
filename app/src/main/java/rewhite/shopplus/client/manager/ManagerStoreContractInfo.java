package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetStoreContractInfoModel;

/**
 * 계약정보회 관리 ManagerClass
 */
public class ManagerStoreContractInfo {
    private static final String TAG = "ManagerStoreContractInfo";

    @SerializedName("Data")
    public List<GetStoreContractInfoModel> managerStoreContractInfo = new ArrayList<>();

    public List<GetStoreContractInfoModel> getStoreContractInfo() {
        return managerStoreContractInfo;
    }

    public void setStoreContractInfo(List<GetStoreContractInfoModel> getManagerStoreContractInfo) {
        this.managerStoreContractInfo = getManagerStoreContractInfo;
    }

    private static ManagerStoreContractInfo mInstance = null;

    public static ManagerStoreContractInfo getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerStoreContractInfo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerStoreContractInfo();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}