package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.StorePaymentCancelAvailList;

/**
 * 취소가능한 품목 리스트 조회
 */
public class ManagerStorePaymentCancel {
    private static final String TAG = "ManagerStorePaymentManagerStorePaymentCancelCancel";

    @SerializedName("Data")
    public List<StorePaymentCancelAvailList> storePaymentCancelAvailList = new ArrayList<>();

    public List<StorePaymentCancelAvailList> getPaymentCancelAvailList() {
        return storePaymentCancelAvailList;
    }

    public void setPaymentCancelAvailList(List<StorePaymentCancelAvailList> storPaymentCancelAvailList) {
        this.storePaymentCancelAvailList = storPaymentCancelAvailList;
    }

    private static ManagerStorePaymentCancel mInstance = null;

    public static ManagerStorePaymentCancel getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerStorePaymentCancel.class) {
                if (mInstance == null) {
                    mInstance = new ManagerStorePaymentCancel();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
