package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetDaySalesStatList;

/**
 * 일별 매출현황(기본기간:이번달) ManagerClass
 */
public class ManagerGetDaySalesStatList {
    private static final String TAG = "ManagerGetHandleItemList";

    @SerializedName("Data")
    public List<GetDaySalesStatList> itemGetDaySalesStatList = new ArrayList<>();

    public List<GetDaySalesStatList> getGetDaySalesStatList() {
        return itemGetDaySalesStatList;
    }

    public void setGetHandleItemList(List<GetDaySalesStatList> storeGetDaySalesStatList) {
        this.itemGetDaySalesStatList = storeGetDaySalesStatList;
    }

    private static ManagerGetDaySalesStatList mInstance = null;

    public static ManagerGetDaySalesStatList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetDaySalesStatList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetDaySalesStatList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}