package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetStoreOutSideList;

/**
 * 외주업체 리스트
 */
public class ManagerGetStoreOutSideList {
    private static final String TAG = "ManagerGetStoreOutSideList";

    @SerializedName("Data")
    public List<GetStoreOutSideList> itemGetStoreOutSideList = new ArrayList<>();

    public List<GetStoreOutSideList> getGetStoreOutSideList() {
        return itemGetStoreOutSideList;
    }

    public void setManagerGetStoreOutSideList(List<GetStoreOutSideList> getStoreOutSideList) {
        this.itemGetStoreOutSideList = getStoreOutSideList;
    }

    private static ManagerGetStoreOutSideList mInstance = null;

    public static ManagerGetStoreOutSideList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetStoreOutSideList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetStoreOutSideList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}