package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetSelectOrderItemList;

/**
 * 주문 품목 조회 ManagerClass
 */
public class ManagerGetSelectOrderItemList {
    private static final String TAG = "ManagerGetSelectOrderItemList";

    @SerializedName("Data")
    public List<GetSelectOrderItemList> itemGetSelectOrderItemList = new ArrayList<>();

    public List<GetSelectOrderItemList> getSelectOrderItemList() {
        return itemGetSelectOrderItemList;
    }

    public void setSelectOrderItemList(List<GetSelectOrderItemList> selectOrderItemList) {
        this.itemGetSelectOrderItemList = selectOrderItemList;
    }

    private static ManagerGetSelectOrderItemList mInstance = null;

    public static ManagerGetSelectOrderItemList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetSelectOrderItemList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetSelectOrderItemList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }

}
