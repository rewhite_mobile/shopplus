package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.StoreUserInfoItemList;

/**
 * 가맹점 회원정보 리스트
 */
public class ManagerStoreUserInfo {
    private static final String TAG = "ManagerStoreUserList";

    @SerializedName("Data")
    public List<StoreUserInfoItemList> storeUserInfoItemList = new ArrayList<>();

    public List<StoreUserInfoItemList> getStoreUserInfo() {
        return storeUserInfoItemList;
    }

    public void setStoreUserInfo(List<StoreUserInfoItemList> storeUserInfoItemList) {

        this.storeUserInfoItemList = storeUserInfoItemList;
    }

    private static ManagerStoreUserInfo mInstance = null;

    public static ManagerStoreUserInfo getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerStoreUserInfo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerStoreUserInfo();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}