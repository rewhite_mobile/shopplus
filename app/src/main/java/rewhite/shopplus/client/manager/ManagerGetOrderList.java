package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetVisitOrderList;

/**
 * 주문 품목 조회(for 주문조회) ManagerClass
 */
public class ManagerGetOrderList {
    private static final String TAG = "ManagerGetOrderList";

    @SerializedName("ResultCode")
    private String ResultCode;
    @SerializedName("GetVisitOrderList")
    public List<GetVisitOrderList> canGetOrderList = new ArrayList<>();

    public List<GetVisitOrderList> getGetOrderList() {
        return canGetOrderList;
    }

    public void setGetOrderList(List<GetVisitOrderList> getGetOrderList) {
        this.canGetOrderList = getGetOrderList;
    }

    private static ManagerGetOrderList mInstance = null;

    public static ManagerGetOrderList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetOrderList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetOrderList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }

    public String getResultCode() {
        return ResultCode;
    }
}