package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetPaymentInfoModel;

/**
 * 선충전금 결제정보상세
 */
public class ManagerGetPaymentInfo {
    private static final String TAG = "ManagerGetPaymentInfo";

    @SerializedName("Data")
    public List<GetPaymentInfoModel> paymentInfo = new ArrayList<>();

    public List<GetPaymentInfoModel> getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(List<GetPaymentInfoModel> getPaymentInfo) {
        this.paymentInfo = getPaymentInfo;
    }

    private static ManagerGetPaymentInfo mInstance = null;

    public static ManagerGetPaymentInfo getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetPaymentInfo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetPaymentInfo();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
