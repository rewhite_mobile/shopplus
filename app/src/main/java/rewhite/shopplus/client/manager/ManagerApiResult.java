package rewhite.shopplus.client.manager;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.ApiResult;

public class ManagerApiResult {
    private static final String TAG = "ManagerApiResult";

    public List<ApiResult> managerApiResult = new ArrayList<>();

    public List<ApiResult> getApiResult() {
        return managerApiResult;
    }

    public void setApiResult(List<ApiResult> getApiResult) {
        this.managerApiResult = getApiResult;
    }

    private static ManagerApiResult mInstance = null;

    public static ManagerApiResult getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerApiResult.class) {
                if (mInstance == null) {
                    mInstance = new ManagerApiResult();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
