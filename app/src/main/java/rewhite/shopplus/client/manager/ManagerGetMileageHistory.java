package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetMileageHistory;

/**
 * 가맹점 회원 마일리지 내역조회 ManagerClass
 */
public class ManagerGetMileageHistory {
    private static final String TAG = "ManagerGetMileageHistory";

    @SerializedName("Data")
    public List<GetMileageHistory> mMileageHistory = new ArrayList<>();

    public List<GetMileageHistory> getmileageHistory() {
        return mMileageHistory;
    }

    public void setmileageHistory(List<GetMileageHistory> mileageHistory) {
        this.mMileageHistory = mileageHistory;
    }

    private static ManagerGetMileageHistory mInstance = null;

    public static ManagerGetMileageHistory getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetMileageHistory.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetMileageHistory();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}