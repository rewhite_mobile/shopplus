package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetVisitOrderListNeed;

public class ManagerGetVisitOrderNeedpay {
    private static final String TAG = "ManagerGetVisitOrderNeedpay";

    @SerializedName("Data")
    public List<GetVisitOrderListNeed> itemGetVisitOrderNeedPay = new ArrayList<>();

    public List<GetVisitOrderListNeed> getGetVisitOrderNeedPay() {
        return itemGetVisitOrderNeedPay;
    }

    public void setGetVisitOrderNeedPay(List<GetVisitOrderListNeed> storeUserInfoItemListNeedPay) {
        this.itemGetVisitOrderNeedPay = storeUserInfoItemListNeedPay;
    }

    private static ManagerGetVisitOrderNeedpay mInstance = null;

    public static ManagerGetVisitOrderNeedpay getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetVisitOrderNeedpay.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetVisitOrderNeedpay();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
