package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.NonPayUserManageList;

/**
 * 미수 회원 조회
 */
public class ManagerNonPayUserManageList {
    private static final String TAG = "ManagerNonPayUserManageList";

    @SerializedName("Data")
    public List<NonPayUserManageList> nonPayUserManageList = new ArrayList<>();

    public List<NonPayUserManageList> getNonPayUserManageList() {
        return nonPayUserManageList;
    }

    public void setUserManageList(List<NonPayUserManageList> userNonPayUserManageList) {

        this.nonPayUserManageList = userNonPayUserManageList;
    }

    private static ManagerNonPayUserManageList mInstance = null;

    public static ManagerNonPayUserManageList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerNonPayUserManageList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerNonPayUserManageList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
