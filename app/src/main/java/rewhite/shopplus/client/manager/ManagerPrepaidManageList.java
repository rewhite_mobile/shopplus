package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.PrepaidManageList;

public class ManagerPrepaidManageList {
    private static final String TAG = "ManagerPrepaidManageList";

    @SerializedName("Data")
    public List<PrepaidManageList> NotPrepaidManageList = new ArrayList<>();

    public List<PrepaidManageList> getPrepaidManageList() {
        return NotPrepaidManageList;
    }

    public void setNotOutOrderItemManageList(List<PrepaidManageList> notPrepaidManageList) {
        this.NotPrepaidManageList = notPrepaidManageList;
    }

    private static ManagerPrepaidManageList mInstance = null;

    public static ManagerPrepaidManageList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerPrepaidManageList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerPrepaidManageList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
