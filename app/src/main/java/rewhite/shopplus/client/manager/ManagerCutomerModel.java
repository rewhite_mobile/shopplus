package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.CutomerModel;

public class ManagerCutomerModel {
    private static final String TAG = "ManagerCutomerModel";

    @SerializedName("Data")
    public List<CutomerModel> itemCutomerModel = new ArrayList<>();

    public List<CutomerModel> getCutomerModel() {
        return itemCutomerModel;
    }

    public void setCutomerModel(List<CutomerModel> getCutomerModel) {
        this.itemCutomerModel = getCutomerModel;
    }

    private static ManagerCutomerModel mInstance = null;

    public static ManagerCutomerModel getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerCutomerModel.class) {
                if (mInstance == null) {
                    mInstance = new ManagerCutomerModel();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
