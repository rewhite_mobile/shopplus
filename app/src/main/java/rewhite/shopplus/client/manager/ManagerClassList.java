package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.ClassListModel;

public class ManagerClassList {
    private static final String TAG = "ManagerClassList";

    @SerializedName("Data")
    public List<ClassListModel> itemClassListInfo = new ArrayList<>();

    public List<ClassListModel> getClassListInfo() {
        return itemClassListInfo;
    }

    public void setClassListInfo(List<ClassListModel> getClassListInfo) {
        this.itemClassListInfo = getClassListInfo;
    }

    private static ManagerClassList mInstance = null;

    public static ManagerClassList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerClassList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerClassList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}