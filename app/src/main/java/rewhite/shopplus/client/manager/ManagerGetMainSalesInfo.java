package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetMainSalesInfo;

public class ManagerGetMainSalesInfo {
    private static final String TAG = "ManagerGetMainSalesInfo";

    @SerializedName("Data")
    public List<GetMainSalesInfo> itemGetMainSalesInfo = new ArrayList<>();

    public List<GetMainSalesInfo> getGetMainSalesInfo() {
        return itemGetMainSalesInfo;
    }

    public void setGetHandleItemList(List<GetMainSalesInfo> storeGetMainSalesInfo) {
        this.itemGetMainSalesInfo = storeGetMainSalesInfo;
    }

    private static ManagerGetMainSalesInfo mInstance = null;

    public static ManagerGetMainSalesInfo getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetMainSalesInfo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetMainSalesInfo();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}