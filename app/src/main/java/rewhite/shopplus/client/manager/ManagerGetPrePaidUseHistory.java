package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetPrePaidUseHistory;

/**
 * 가맹점 회원 선충전금 사용내역 ManagerClass
 */
public class ManagerGetPrePaidUseHistory {
    private static final String TAG = "ManagerGetPrePaidUseHistory";

    @SerializedName("Data")
    public List<GetPrePaidUseHistory> getPrePaidUseHistory = new ArrayList<>();

    public List<GetPrePaidUseHistory> getGetPrePaidUseHistory() {
        return getPrePaidUseHistory;
    }

    public void setGetPrePaidUseHistory(List<GetPrePaidUseHistory> prePaidUseHistory) {
        this.getPrePaidUseHistory = prePaidUseHistory;
    }

    private static ManagerGetPrePaidUseHistory mInstance = null;

    public static ManagerGetPrePaidUseHistory getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetPrePaidUseHistory.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetPrePaidUseHistory();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}