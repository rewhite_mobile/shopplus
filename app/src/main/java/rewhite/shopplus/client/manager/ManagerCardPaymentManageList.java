package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.UserManageList;

public class ManagerCardPaymentManageList {
    private static final String TAG = "ManagerCardPaymentManageList";

    @SerializedName("Data")
    public List<UserManageList> NonPayOrderItemManageList = new ArrayList<>();

    public List<UserManageList> getNonPayOrderItemManageList() {
        return NonPayOrderItemManageList;
    }

    public void setUserManageList(List<UserManageList> NonPayOrderItemManageList) {

        this.NonPayOrderItemManageList = NonPayOrderItemManageList;
    }

    private static ManagerCardPaymentManageList mInstance = null;

    public static ManagerCardPaymentManageList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerCardPaymentManageList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerCardPaymentManageList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
