package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.NotOutUserManageList;

/**
 * 미출고 회원 조회
 */
public class ManagerNotOutUserManageList {
    private static final String TAG = "ManagerNotOutUserManageList";

    @SerializedName("Data")
    public List<NotOutUserManageList> userManageList = new ArrayList<>();

    public List<NotOutUserManageList> getUserManageList() {
        return userManageList;
    }

    public void setUserManageList(List<NotOutUserManageList> userManageList) {

        this.userManageList = userManageList;
    }

    private static ManagerNotOutUserManageList mInstance = null;

    public static ManagerNotOutUserManageList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerNotOutUserManageList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerNotOutUserManageList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
