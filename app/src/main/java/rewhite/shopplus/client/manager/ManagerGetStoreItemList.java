package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetStoreItemList;

/**
 * 품목 리스트 조회(매장관리용)
 */
public class ManagerGetStoreItemList {
    private static final String TAG = "ManagerGetStoreItemList";

    @SerializedName("Data")
    public List<GetStoreItemList> itemStoreItemList = new ArrayList<>();

    public List<GetStoreItemList> getStoreItemList() {
        return itemStoreItemList;
    }

    public void setGetHandleItemList(List<GetStoreItemList> getStoreItemList) {
        this.itemStoreItemList = getStoreItemList;
    }

    private static ManagerGetStoreItemList mInstance = null;

    public static ManagerGetStoreItemList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetHandleItemList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetStoreItemList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}