package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.UserManageList;

/**
 * 운영관리 회원 조회
 */
public class ManagerUserManageList {
    private static final String TAG = "ManagerUserManageList";
    @SerializedName("ResultCode")
    String ResultCode;//결과 코드
    @SerializedName("Message")
    String Message;//결과 메시지
    @SerializedName("Count")
    int Count;//레코드 수
    @SerializedName("TotalCount")
    int TotalCount;//총 레코드 수
    @SerializedName("PageBlock")
    int PageBlock;//페이지 크기
    @SerializedName("LastPage")
    int LastPage;//마지막 페이지 번호
    @SerializedName("Data")
    public List<UserManageList> userManageList = new ArrayList<>();

    public List<UserManageList> getUserManageList() {
        return userManageList;
    }

    public void setUserManageList(List<UserManageList> userManageList) {

        this.userManageList = userManageList;
    }

    private static ManagerUserManageList mInstance = null;

    public static ManagerUserManageList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerUserManageList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerUserManageList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }

    public String getResultCode() {
        return ResultCode;
    }

    public String getMessage() {
        return Message;
    }

    public int getCount() {
        return Count;
    }

    public int getTotalCount() {
        return TotalCount;
    }

    public int getPageBlock() {
        return PageBlock;
    }

    public int getLastPage() {
        return LastPage;
    }
}
