package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.data.dto.ItemAdditionalDataType;

/**
 * 품목등록 Requeest
 */
public class ManagerItemAddRequest {
    private static final String TAG = "ManagerItemAddRequest";

    @SerializedName("Data")
    public List<ItemAdditionalDataType> itemAdditionalData = new ArrayList<>();

    public List<ItemAdditionalDataType> getItemAdditionalData() {
        return itemAdditionalData;
    }

    public void setItemAdditionalData(List<ItemAdditionalDataType> storeUserInfoItemList) {
        this.itemAdditionalData = storeUserInfoItemList;
    }

    private static ManagerItemAddRequest mInstance = null;

    public static ManagerItemAddRequest getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerItemAddRequest.class) {
                if (mInstance == null) {
                    mInstance = new ManagerItemAddRequest();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
