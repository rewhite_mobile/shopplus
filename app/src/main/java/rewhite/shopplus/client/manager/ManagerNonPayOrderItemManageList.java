package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.UserOperationModel;

public class ManagerNonPayOrderItemManageList {
    private static final String TAG = "ManagerNonPayOrderItemManageList";

    @SerializedName("Data")
    public List<UserOperationModel> NonPayOrderItemManageList = new ArrayList<>();

    public List<UserOperationModel> getNonPayOrderItemManageList() {
        return NonPayOrderItemManageList;
    }

    public void setUserManageList(List<UserOperationModel> NonPayOrderItemManageList) {

        this.NonPayOrderItemManageList = NonPayOrderItemManageList;
    }

    private static ManagerNonPayOrderItemManageList mInstance = null;

    public static ManagerNonPayOrderItemManageList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerNonPayOrderItemManageList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerNonPayOrderItemManageList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
