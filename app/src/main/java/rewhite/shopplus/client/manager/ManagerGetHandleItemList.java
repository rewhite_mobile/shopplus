package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetHandleItemList;

/**
 * 세탁물 취급 품목 리스트 ManagerClass
 */
public class ManagerGetHandleItemList {
    private static final String TAG = "ManagerGetHandleItemList";

    @SerializedName("Data")
    public List<GetHandleItemList> itemGetHandleItemList = new ArrayList<>();

    public List<GetHandleItemList> getGetHandleItemList() {
        return itemGetHandleItemList;
    }

    public void setGetHandleItemList(List<GetHandleItemList> storeGetHandleItemList) {
        this.itemGetHandleItemList = storeGetHandleItemList;
    }

    private static ManagerGetHandleItemList mInstance = null;

    public static ManagerGetHandleItemList getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetHandleItemList.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetHandleItemList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}