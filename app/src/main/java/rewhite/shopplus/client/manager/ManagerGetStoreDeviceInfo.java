package rewhite.shopplus.client.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetStoreDeviceInfo;

/**
 * 장비설정 > 인쇄설정
 */
public class ManagerGetStoreDeviceInfo {
    private static final String TAG = "ManagerGetStoreDeviceInfo";

    @SerializedName("Data")
    public List<GetStoreDeviceInfo> itemGetStoreDeviceInfo = new ArrayList<>();

    public List<GetStoreDeviceInfo> getGetStoreDeviceInfo() {
        return itemGetStoreDeviceInfo;
    }

    public void setGetStoreDeviceInfo(List<GetStoreDeviceInfo> getStoreDeviceInfo) {
        this.itemGetStoreDeviceInfo = getStoreDeviceInfo;
    }

    private static ManagerGetStoreDeviceInfo mInstance = null;

    public static ManagerGetStoreDeviceInfo getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetStoreDeviceInfo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetStoreDeviceInfo();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}