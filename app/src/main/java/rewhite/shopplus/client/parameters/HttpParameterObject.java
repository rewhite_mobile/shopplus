package rewhite.shopplus.client.parameters;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import rewhite.shopplus.util.Logger;

public abstract class HttpParameterObject {
    protected static final String TAG = "HttpParameterObject";

    protected static final String DefaultEncoding = "UTF-8";
    protected static final String DelimiterKeyValue = "=";
    protected static final String DelimiterKeyKey = "&";

    public abstract String toHttpParameterString();

    protected String getEncodeStr(Object str) {
        try {
            return URLEncoder.encode(str.toString(), DefaultEncoding);
        } catch (UnsupportedEncodingException e) {
            Logger.e(TAG, "encode err.", e);
            return "";
        }
    }

}
