package rewhite.shopplus.client.constant;

/**
 * network timeout contant
 */
public class NetConstant {
    private NetConstant() {
    }

    public final static int CONNECT_TIME_OUT = 10;//sec
    public final static int READ_TIME_OUT = 10;//sec
    public final static int WRITE_TIME_OUT = 10;//sec

    public final static int CONNECT_TIME_OUT_LONG_TIMEOUT = 60;//sec
    public final static int READ_TIME_OUT_LONG_TIMEOUT = 60;//sec
    public final static int WRITE_TIME_OUT_LONG_TIMEOUT = 60;//sec
}
