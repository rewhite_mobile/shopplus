package rewhite.shopplus.client.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rewhite.shopplus.client.constant.NetConstant;
import rewhite.shopplus.client.manager.ManagerCancelAvailOrderList;
import rewhite.shopplus.client.manager.ManagerGetSelectOrderItemList;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.manager.ManagerGetVisitOrderNeedpay;

/**
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
interface VisitReceptionAPI {

    public static final HttpLoggingInterceptor LOGGING = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);

    public static final OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .connectTimeout(NetConstant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(NetConstant.WRITE_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(NetConstant.READ_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(LOGGING)
            .build();

    public static final Retrofit AUTH_SERVER = new Retrofit.Builder()
            //프로토콜 TEST / LIVE CHECK
            .baseUrl(ServerApis.TEST_PROTOCOL + ServerApis.TESTBED_AUTHURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OK_HTTP_CLIENT)
            .build();

    @POST("Order/GetVisitOrderList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //주문 품목 조회(for 회원상세)
    Call<ManagerGetVisitOrder> getGetVisitOrder(@Query("SearchType") String serchType, @Query("storeUserID") String storeUserID, @Query("page") String page, @Query("block") String block, @Header("accesstoken") String authorization);


    @POST("Order/GetVisitOrderList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //주문 품목 조회(for 회원상세)
    Call<ManagerGetVisitOrderNeedpay> getGetVisitOrderNeed(@Query("SearchType") String serchType, @Query("storeUserID") String storeUserID, @Query("page") String page, @Query("block") String block, @Header("accesstoken") String authorization);


    @POST("/Order/GetOrderList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //주문 품목 조회(for 주문조회)
    Call<ManagerGetVisitOrder> getGetOrderList(@Query("startDate") String startDate, @Query("endDate") String endDate, @Query("orderStatus") String orderStatus,
                                              @Query("userGrade") String userGrade, @Query("searchPhone") String searchPhone, @Query("userName") String userName,
                                              @Query("tagNo") String tagNo, @Query("page") int page, @Query("block") int block,
                                              @Header("accesstoken") String authorization);

    @POST("/Order/GetCancelAvailOrderList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

    //취소 가능한 주문 품목 조회(for 품목취소)
    Call<ManagerCancelAvailOrderList> getCancelAvailorder(@Query("storeUserID") long storeUserID, @Header("accesstoken") String authorization);

    @POST("/Order/GetSelectOrderItemList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

    //주문 품목 조회(다수의 주문품목ID)
    Call<ManagerGetSelectOrderItemList> GetSelectOrderItemList(@Query("orderItemIDs") String orderItemIDs, @Header("accesstoken") String authorization);

}
