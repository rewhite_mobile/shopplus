package rewhite.shopplus.client.network;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rewhite.shopplus.client.constant.NetConstant;
import rewhite.shopplus.client.manager.ManagerGetHandleItemList;
import rewhite.shopplus.client.manager.ManagerGetStoreItemList;
import rewhite.shopplus.client.manager.ManagerGetStoreOutSideList;
import rewhite.shopplus.client.model.ApiResult;
import rewhite.shopplus.client.model.ChangePasswordModel;
import rewhite.shopplus.client.model.GetStoreContractInfoModel;
import rewhite.shopplus.client.model.GetStoreDeviceInfo;
import rewhite.shopplus.client.model.GetStoreManageInfo;

/**
 * 환경설정 관련 API
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
interface EnviromentAPI {

    public HttpLoggingInterceptor LOGGING = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);

    public OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .connectTimeout(NetConstant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(NetConstant.WRITE_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(NetConstant.READ_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(LOGGING)
            .build();

    public Retrofit AUTH_SERVER = new Retrofit.Builder()
            //프로토콜 TEST / LIVE CHECK
            .baseUrl(ServerApis.TEST_PROTOCOL + ServerApis.TESTBED_AUTHURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OK_HTTP_CLIENT)
            .build();

    @POST("/StoreSetting/GetStoreManageInfo")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //매장 관리 정보 조회
    Call<GetStoreManageInfo> getManagerGetStoreManageInfo(@Header("accesstoken") String authorization);

    @POST("/StoreSetting/GetHandleItemList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //세탁물 취급 품목 리스트 조회
    Call<ManagerGetHandleItemList> getHandleItemList(@Query("itemGroupType") long storeUserID, @Header("accesstoken") String authorization);

    @POST("/StoreSetting/GetStoreOutSideList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //외주업체 리스트 리스트 조회
    Call<ManagerGetStoreOutSideList> getStoreOutSideList(@Header("accesstoken") String authorization);

    @POST("/StoreSetting/GetStoreItemListForManage")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //품목 리스트 조회(매장관리용) 조회
    Call<ManagerGetStoreItemList> getGetStoreItemList(@Query("categoryType") int storeUserID, @Query("categoryID") int categoryID, @Query("classID") int classID, @Header("accesstoken") String authorization);

    @POST("/StoreSetting/GetStoreDeviceInfo")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //가맹점 장비설정 정보 조회
    Call<GetStoreDeviceInfo> getStoreDeviceInfo(@Header("accesstoken") String authorization);

    //@ 매장 인사말 변경 REQUEST
    @POST("/StoreSetting/UpdateStoreGreetingText")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateStoreGreeting(@Query("text") String greeting, @Header("accesstoken") String authorization);

    //@ 택번호 타입 변경 REQUEST
    @POST("/StoreSetting/UpdateTagNoType")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateTagNoType(@Query("tagNoType") int tagNoType, @Header("accesstoken") String authorization);

    //지원결제 방식 변경 REQUEST
    @POST("/StoreSetting/UpdateAvailPaymentType")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateAvailPaymentType(@Query("availPaymentType") String availPaymentType, @Header("accesstoken") String authorization);

    //선충전금 설정 변경 REQUEST
    @POST("/StoreSetting/UpdatePrePaidManage")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ApiResult> getUpdatePrePaidManage(@Query("isGiveBonusPrePaid") String isGiveBonusPrePaid, @Query("cashBonusPrePaidRate") Double cashBonusPrePaidRate, @Query("cardBonusPrePaidRate") Double cardBonusPrePaidRate,
                                      @Query("isPrePaidCashReceipt") String isPrePaidCashReceipt, @Header("accesstoken") String authorization);


    //마일리지 설정 변경 REQUEST
    @POST("/StoreSetting/UpdateMileageManage")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ApiResult> getUpdateMileageManage(@Query("isSaveMileage") String isSaveMileage, @Query("cashMileageRate") double cashMileageRate,
                                           @Query("cardMileageRate") double cardMileageRate, @Query("mileageUseMinPrice") int mileageUseMinPrice, @Header("accesstoken") String authorization);

    //정기휴무일 설정 변경 REQUEST
    @POST("/StoreSetting/UpdateClosingWeek")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateClosingWeek(@Query("weekType") int weekType, @Query("closingWeek") String closingWeek, @Header("accesstoken") String authorization);

    //휴무일 추가 설정 변경 REQUEST
    @POST("/StoreSetting/AddClosingDay")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getAddClosingDay(@Query("ClosingDay") String ClosingDay, @Header("accesstoken") String authorization);

    //휴무일 제거 설정 변경 REQUEST
    @POST("/StoreSetting/RemoveClosingDay")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getRemoveClosingDay(@Query("ClosingDay") String ClosingDay, @Header("accesstoken") String authorization);

    //영업시간 설정 변경 REQUEST
    @POST("/StoreSetting/UpdateServiceTime")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateServiceTime(@Query("serviceStartTime") String serviceStartTime, @Query("serviceEndTime") String serviceEndTime, @Query("serviceStartTime_Sat") String serviceStartTime_Sat, @Query("serviceEndTime_Sat") String serviceEndTime_Sat,
                                    @Query("serviceStartTime_Sun") String serviceStartTime_Sun, @Query("serviceEndTime_Sun") String serviceEndTime_Sun, @Header("accesstoken") String authorization);

    //세탁소요기간 설정 변경 REQUEST
    @POST("/StoreSetting/UpdateWashingCostDay")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateWashingCostDay(@Query("washingCostDay") int washingCostDay, @Header("accesstoken") String authorization);

    //세탁물 취급 품목 설정 변경 REQUEST
    @POST("/StoreSetting/SetHandleItem")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getSetHandleItem(@Query("itemGroupID") int itemGroupID, @Query("isUse") String isUse, @Header("accesstoken") String authorization);

    //외주업체 추가 설정 변경 REQUEST
    @POST("/StoreSetting/AddStoreOutSide")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getAddStoreOutSide(@Body Map<String, Object> parameter, @Header("accesstoken") String authorization);

    //외주업체 수정 변경 REQUEST
    @POST("/StoreSetting/UpdateStoreOutSide")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateStoreOutSide(@Body Map<String, Object> parameter, @Header("accesstoken") String authorization);

    //외주업체 삭제 변경 REQUEST
    @POST("/StoreSetting/RemoveStoreOutSide")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getRemoveStoreOutSide(@Body Map<String, Object> parameter, @Header("accesstoken") String authorization);

    //품목 요금 변경 REQUEST
    @POST("/StoreSetting/UpdateStoreItemPrice")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getRUpdateStoreItemPrice(@Query("storeItemID") long storeItemID, @Query("itemVisitPrice") int itemVisitPrice, @Query("itemVisitPrice2") int itemVisitPrice2, @Query("itemVisitPrice3") int itemVisitPrice3,
                                        @Query("itemPickupPrice") int itemPickupPrice, @Query("itemPickupPrice2") int itemPickupPrice2, @Query("itemPickupPrice3") int itemPickupPrice3, @Header("accesstoken") String authorization);

    //품목 상태 변경 REQUEST
    @POST("/StoreSetting/UpdateStoreItemState")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateStoreItemState(@Query("storeItemID") long storeItemID, @Query("isUse") String isUse, @Header("accesstoken") String authorization);

    //품목 추가 변경 REQUEST
    @POST("/StoreSetting/AddStoreItem")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getAddStoreItem(@Query("classID") int classID, @Query("itemName") String itemName, @Header("accesstoken") String authorization);

    //품목명 수정 REQUEST
    @POST("/StoreSetting/UpdateStoreItem")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateStoreItem(@Query("storeItemID") long storeItemID, @Query("itemName") String itemName, @Header("accesstoken") String authorization);


    //품목 추가 변경 REQUEST
    @POST("/StoreSetting/UpdateStorePrintSetting")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateStorePrintSetting(@Query("printCount") int printCount, @Query("isPrintBalance") String isPrintBalance, @Header("accesstoken") String authorization);

    //손해배상기준 인쇄 설정 변경 REQUEST
    @POST("/StoreSetting/UpdateDamageNotice")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateDamageNotice(@Query("isPrintDamageNotice") String isPrintDamageNotice, @Query("damageNotice") String damageNotice, @Header("accesstoken") String authorization);

    //세탁물 보관료 안내 인쇄 설정 변경 REQUEST
    @POST("/StoreSetting/UpdateStorageNotice")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateStorageNotice(@Query("IsPrintStotageNotice") String IsPrintStotageNotice, @Query("storageNotice") String storageNotice, @Header("accesstoken") String authorization);

    //세탁물 보관료 안내 인쇄 설정 변경 REQUEST
    @POST("/StoreSetting/UpdateStoreNotice")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getUpdateStoreNotice(@Query("isPrintStoreNotice") String isPrintStoreNotice, @Query("StoreNotice") String StoreNotice, @Header("accesstoken") String authorization);

    //가맹점 계정 비밀번호 변경 REQUEST
    @POST("/StoreSetting/ChangePassword")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ChangePasswordModel> getChangePassword(@Query("old_Password") String old_Password, @Query("new_Password") String new_Password, @Header("accesstoken") String authorization);

    //계약정보 조회
    @POST("/StoreSetting/GetStoreContractInfo")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<GetStoreContractInfoModel> getStoreContractInfo(@Header("accesstoken") String authorization);
}
