package rewhite.shopplus.client.network;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rewhite.shopplus.client.constant.NetConstant;
import rewhite.shopplus.client.manager.ManagerAreaCity;
import rewhite.shopplus.client.manager.ManagerAreaDistrict;
import rewhite.shopplus.client.model.ApiResult;
import rewhite.shopplus.client.model.StoreLoginModel;

/**
 * user Accesstoken Api
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
interface AuthAPI {
    public static final HttpLoggingInterceptor LOGGING = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    public static final OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .connectTimeout(NetConstant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(NetConstant.WRITE_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(NetConstant.READ_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(LOGGING)
            .build();

    public static final Retrofit AUTH_SERVER = new Retrofit.Builder()
            //프로토콜 TEST / LIVE CHECK
            .baseUrl(ServerApis.TEST_PROTOCOL + ServerApis.TESTBED_AUTHURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OK_HTTP_CLIENT)
            .build();

    //가입상담신청 REQUEST
    @POST("/StoreAuth/RequestJoinStore")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getRequestJoinStore(@Query("StoreName") String StoreName, @Query("Phone") String Phone, @Query("Sido") String Sido, @Query("Gugun") String Gugun,
                                   @Query("IsDelivery") String IsDelivery, @Header("accesstoken") String authorization);

    //로그인 ID 찾기 REQUEST
    @POST("/StoreAuth/FindLoginID")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ApiResult> getFindLoginID(@Query("phoneNum") String phoneNum);

    //로그인 PW 찾기 REQUEST
    @POST("/StoreAuth/FindLoginPassword")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ApiResult> getFindLoginPassword(@Query("phoneNum") String phoneNum);


    //가맹점 로그인 REQUEST
    @POST("/StoreAuth/StoreLogin")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<StoreLoginModel> getStoreLogin(@Query("loginID") String loginID, @Query("password") String password, @Query("deviceToken") String deviceToken);

    //엑세 토큰 확인 REQUEST
    @POST("/StoreAuth/CheckAccessToken")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<StoreLoginModel> CheckAccessToken(@Query("accessToken") String accessToken);

    // 지역(시/도) 조회
    @POST("/StoreAuth/AreaCityList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ManagerAreaCity> getAreaCityList(@Query("accessToken") String accessToken);

    // 지역(시/구/군) 조회 조회
    @POST("/StoreAuth/AreaDistrictList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ManagerAreaDistrict> getAreaDistrictList(@Query("parnetAreaID") int parnetAreaID, @Query("accessToken") String accessToken);

    //품목등록 REQUEST
    @POST("Order/RequestVisitOrder")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ApiResult> getItemAddReques(@Body Map<String, Object> parameters, @Header("accesstoken") String authorization);

}
