package rewhite.shopplus.client.network;


import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import rewhite.shopplus.util.Logger;

public class NetworkRequest {
    private static final String TAG = "NetworkRequest";

    private static final long TIME_OUT = 4 * 1000L;
    private static final long RETRY_SKIP_DURATION = 0L;
    private static final int RETRY_COUNT = 1;

    private static final byte[] LOCK = new byte[0];
    private static long lastErrorTime = 0;

//    private static void showErrorPopup(final Activity activity) {
//        Log.d(TAG, "showErrorPopup()");
//        //activity가 있으면 종료시키고 popup을 보여줌.
//        if (activity != null) {
//            activity.finish();
//        }
//
////        Context context = activity != null ? activity.getApplicationContext() : ActivityMain.getInstance().getApplicationContext();
////        Intent intent = new Intent(context, ActivityNetworkError.class);
////        context.startActivity(intent);
//    }

    private static boolean isErrorState() {
        synchronized (LOCK) {
            return (System.currentTimeMillis() - lastErrorTime <= RETRY_SKIP_DURATION);
        }
    }

    private static void setErrorState() {
        synchronized (LOCK) {
            lastErrorTime = System.currentTimeMillis();
        }
    }

    protected static void execute(AppCompatActivity activity, Runnable task) {
        execute(activity, TIME_OUT, task);
    }

    protected static void execute(AppCompatActivity activity, long timeout, Runnable task) {
        Logger.d(TAG, "execute() -----------------------");

        if (isErrorState()) {
            Logger.d(TAG, "execute() - errorState, do not request (" + (System.currentTimeMillis() - lastErrorTime) + "/" + RETRY_SKIP_DURATION + ")");
//            showErrorPopup(activity);
            return;
        }

        boolean success = false;
        for (int i = 0; i < RETRY_COUNT; i++) {
            success = execute(task, timeout);
            if (success) {
                break;
            }
        }
        if (!success) {
            Logger.d(TAG, "execute() - catch error - setErrorState()");
            setErrorState();
//            showErrorPopup(activity);
        }
        Logger.d(TAG, "execute() done. -----------------------");
    }

    private static boolean execute(Runnable task, long timeout) {
        Logger.d(TAG, "  execute() ------ (timeout:" + timeout + ")");
        Thread thread = new Thread(task, "NetworkRequestThread");
        thread.setDaemon(true);
        thread.start();

        long t1 = 0, t2 = 0;
        t1 = System.currentTimeMillis();

        try {
            thread.join(timeout);
            t2 = System.currentTimeMillis();

        } catch (InterruptedException e) {
            Log.d(TAG, "  execute() - catch InterruptedException");

        }
        if (thread.isAlive()) {
            Log.d(TAG, "  execute() - thread interrupted. (" + (t2 - t1) + ") - timeout!!!");
            thread.interrupt();
            //fail
            return false;
        } else {
            Log.d(TAG, "  execute() - thread run done. (" + (t2 - t1) + ")");
            return true;
        }
    }
}