package rewhite.shopplus.client.network;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rewhite.shopplus.client.constant.NetConstant;
import rewhite.shopplus.client.manager.ManagerClassList;
import rewhite.shopplus.client.manager.ManagerLaundryCategory;
import rewhite.shopplus.client.manager.ManagerMemoitemList;
import rewhite.shopplus.client.manager.ManagerOrderItemList;
import rewhite.shopplus.client.manager.ManagerStoreItemList;
import rewhite.shopplus.client.model.ApiResult;
import rewhite.shopplus.client.model.GetMaxTagNoModel;

/**
 * 품목등록 상품카테고리 정보 API
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
interface LaundryRegistrationAPI {

    public HttpLoggingInterceptor LOGGING = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);

    public OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .connectTimeout(NetConstant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(NetConstant.WRITE_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(NetConstant.READ_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(LOGGING)
            .build();

    public Retrofit AUTH_SERVER = new Retrofit.Builder()
            //프로토콜 TEST / LIVE CHECK
            .baseUrl(ServerApis.TEST_PROTOCOL + ServerApis.TESTBED_AUTHURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OK_HTTP_CLIENT)
            .build();

    @POST("Item/CategoryList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //품목 대분류 리스트
    Call<ManagerLaundryCategory> getLaundryCategoryList(@Query("categoryType") long storeUserID, @Header("accesstoken") String authorization);

    @POST("Item/ClassList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //품목 중분류 리스트
    Call<ManagerClassList> getLaundryClassList(@Query("categoryID") long categoryID, @Header("accesstoken") String authorization);


    @POST("Item/StoreItemList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //가맹점 품목 리스트
    Call<ManagerStoreItemList> getStoreItemList(@Query("categoryType") int categoryType, @Query("categoryID") int categoryID, @Query("classID") int classID , @Header("accesstoken") String authorization);

    @POST("Item/StoreItemList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //품목등록 수선요금
    Call<ManagerStoreItemList> getRepairItemList(@Query("categoryType") long categoryType, @Header("accesstoken") String authorization);


    @POST("Item/StoreItemList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //품목등록 기술요금
    Call<ManagerStoreItemList> getTechnicalItemList(@Query("categoryType") long categoryType, @Header("accesstoken") String authorization);

    @POST("Item/StoreItemList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //품목등록 부속
    Call<ManagerStoreItemList> getComponentItemList(@Query("categoryType") long categoryType, @Header("accesstoken") String authorization);


    @POST("/Manage/MemoList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //품목등록 부속
    Call<ManagerMemoitemList> getMemoItemList(@Query("memoType") int categoryType, @Header("accesstoken") String authorization);


    //품목등록 REQUEST
    @POST("Order/RequestVisitOrder")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ApiResult> getItemAddRequest(@Body Map<String, Object> parameters, @Header("accesstoken") String authorization);

    //최근 사용한 택번호
    @POST("Order/GetMaxTagNo")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<GetMaxTagNoModel> getGetMaxTagNo(@Header("accesstoken") String authorization);

    //주문 품목 조회(주문ID)
    @POST("/Order/GetOrderItemList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ManagerOrderItemList> GetOrderItemList(@Query("orderID") int orderID, @Header("accesstoken") String authorization);


    //품목수정 Update
    @POST("/Order/UpdateVisitOrder")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ApiResult> getUpdateVisitOrder(@Body Map<String, Object> parameters, @Header("accesstoken") String authorization);


}
