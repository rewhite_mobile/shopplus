package rewhite.shopplus.client.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rewhite.shopplus.client.constant.NetConstant;
import rewhite.shopplus.client.manager.ManagerGetMileageHistory;
import rewhite.shopplus.client.manager.ManagerGetPrePaidChargeHistory;
import rewhite.shopplus.client.manager.ManagerGetPrePaidUseHistory;
import rewhite.shopplus.client.manager.ManagerStoreUserList;
import rewhite.shopplus.client.model.ApiResult;
import rewhite.shopplus.client.model.StoreUserInfoItemList;

/**
 * StoreUser Server Data Api
 * HttpConnection Data Logic Management
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
interface StoreUserAPI {

    public static final HttpLoggingInterceptor LOGGING = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);

    public static final OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .connectTimeout(NetConstant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(NetConstant.WRITE_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(NetConstant.READ_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(LOGGING)
            .build();

    public static final Retrofit AUTH_SERVER = new Retrofit.Builder()
            //프로토콜 TEST / LIVE CHECK
            .baseUrl(ServerApis.TEST_PROTOCOL + ServerApis.TESTBED_AUTHURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OK_HTTP_CLIENT)
            .build();

    @POST("StoreUser/Info")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //가맹점 회원정보
    Call<StoreUserInfoItemList> getUserInfo(@Query("storeUserID") long storeUserID, @Header("accesstoken") String authorization);

    @POST("/StoreUser/List")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //가맹점 회원 리스트
    Call<ManagerStoreUserList> getStoreUserList(@Query("searchName") String searchName, @Query("searchPhone") String user, @Header("accesstoken") String authorization);


    //가맹점 회원정보등록 REQUEST
    @POST("StoreUser/Register")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ApiResult> getStoreUserRegister(@Query("storeUserGrade") String storeUserGrade, @Query("userName") String userName, @Query("address") String address, @Query("userPhone") String userPhone,
                                        @Query("gender") String gender, @Query("birthDay") String birthDay, @Query("anniversary") String anniversary,
                                         @Query("isNoticeAgree") String isNoticeAgree, @Query("isPromoteArgee") String isPromoteArgee, @Header("accesstoken") String authorization);

    //가맹점 회원정보수정 REQUEST
    @POST("/StoreUser/Update")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ApiResult> getStoreUserUpdate(@Query("storeUserID") long storeUserID, @Query("storeUserGrade") String storeUserGrade, @Query("userName") String userName, @Query("address") String address,
                                  @Query("userPhone") String userPhone, @Query("gender") String gender, @Query("memo") String memo, @Query("birthDay") String birthDay,
                                  @Query("anniversary") String anniversary, @Query("isNoticeAgree") String isNoticeAgree, @Query("isPromoteArgee") String isPromoteArgee, @Header("accesstoken") String authorization);

    @POST("/StoreUser/GetMileageHistory")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //가맹점 회원 마일리지 내역조회
    Call<ManagerGetMileageHistory> getMileageHistory(@Query("storeUserID") long storeUserID, @Header("accesstoken") String authorization);


    @POST("StoreUser/GetPrePaidChargeHistory")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //가맹점 회원 선충전금 충전내역
    Call<ManagerGetPrePaidChargeHistory> getPrePaidChargeHistory(@Query("storeUserID") long storeUserID, @Header("accesstoken") String authorization);


    @POST("StoreUser/GetPrePaidUseHistory")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //가맹점 회원 선충전금 사용내역
    Call<ManagerGetPrePaidUseHistory> getStoreUserList(@Query("storeUserID") long storeUserID, @Header("accesstoken") String authorization);

}

