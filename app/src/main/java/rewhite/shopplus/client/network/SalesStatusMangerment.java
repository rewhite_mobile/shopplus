package rewhite.shopplus.client.network;

import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import rewhite.shopplus.client.manager.ManagerGetDaySalesStatList;
import rewhite.shopplus.client.manager.ManagerGetMainSalesInfo;
import rewhite.shopplus.client.manager.ManagerGetMonthSalesStatList;
import rewhite.shopplus.client.model.GetMainSalesInfo;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;

import static rewhite.shopplus.client.network.NetworkRequest.execute;

/**
 * 매출 현황 API Manager Class
 */
public class SalesStatusMangerment {
    private static final String TAG = "SalesStatusMangerment";

    /**
     * 메인화면 매출 현황 정보 조회
     *
     * @param activity View
     * @return
     */
    public static void getSalesStatusMangerment(AppCompatActivity activity) {
        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    SalesStatusAPI salesStatusAPI = SalesStatusAPI.AUTH_SERVER.create(SalesStatusAPI.class);
                    Call<GetMainSalesInfo> call = salesStatusAPI.getMainSalesInfo(SettingLogic.getPreAccessToken(activity));

                    Response<GetMainSalesInfo> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ArrayList<GetMainSalesInfo> arrayList = new ArrayList<>();
                                arrayList.add(response.body());
                                ManagerGetMainSalesInfo.getmInstance().setGetHandleItemList(arrayList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
    }

    /**
     * 일별 매출 현황 (기본기간:이번달)
     * @param activity
     * @param startDate 조회시작일(yyyy-MM-dd, 기본값:금월 1일)
     * @param endDate   조회종료일(yyyy-MM-dd, 기본값: 전일)
     * @return
     */
    public static ManagerGetDaySalesStatList getManagerGetHandleItemList(AppCompatActivity activity, String startDate, String endDate) {

        final ManagerGetDaySalesStatList[] managerGetHandleItemList = {new ManagerGetDaySalesStatList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    SalesStatusAPI salesStatusAPI = SalesStatusAPI.AUTH_SERVER.create(SalesStatusAPI.class);
                    Call<ManagerGetDaySalesStatList> call = salesStatusAPI.getDaySalesStatList(startDate, endDate,SettingLogic.getPreAccessToken(activity));

                    Response<ManagerGetDaySalesStatList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                ManagerGetDaySalesStatList.getmInstance().setGetHandleItemList(response.body().itemGetDaySalesStatList);
                                managerGetHandleItemList[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerGetHandleItemList[0];
    }


    /**
     * 월별 매출 현황 (기본기간:최근3개월)
     * @param activity
     * @param startDate 조회시작일(yyyy-MM-dd, 기본값: 2개월 전 1일)
     * @param endDate   조회종료일(yyyy-MM-dd, 기본값: 전일)
     * @return
     */
    public static ManagerGetMonthSalesStatList getGetMonthSalesStatList(AppCompatActivity activity, String startDate, String endDate) {

        final ManagerGetMonthSalesStatList[] managerGetMonthSalesStatLists = {new ManagerGetMonthSalesStatList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    SalesStatusAPI salesStatusAPI = SalesStatusAPI.AUTH_SERVER.create(SalesStatusAPI.class);
                    Call<ManagerGetMonthSalesStatList> call = salesStatusAPI.getMonthSalesStatList(startDate, endDate,SettingLogic.getPreAccessToken(activity));

                    Response<ManagerGetMonthSalesStatList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                ManagerGetMonthSalesStatList.getmInstance().setGetHandleItemList(response.body().itemGetMonthSalesStatList);
                                managerGetMonthSalesStatLists[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerGetMonthSalesStatLists[0];
    }
}