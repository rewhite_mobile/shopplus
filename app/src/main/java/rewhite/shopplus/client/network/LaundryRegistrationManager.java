package rewhite.shopplus.client.network;

import android.support.v7.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import rewhite.shopplus.client.manager.ManagerClassList;
import rewhite.shopplus.client.manager.ManagerLaundryCategory;
import rewhite.shopplus.client.manager.ManagerMaxTagModel;
import rewhite.shopplus.client.manager.ManagerMemoitemList;
import rewhite.shopplus.client.manager.ManagerOrderItemList;
import rewhite.shopplus.client.manager.ManagerStoreItemList;
import rewhite.shopplus.client.model.ApiResult;
import rewhite.shopplus.client.model.GetMaxTagNoModel;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.data.dto.ItemAdditionalDataType;
import rewhite.shopplus.data.manager.ManagerItemAdditionalDataEditType;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.SharedPreferencesUtil;

import static rewhite.shopplus.client.network.NetworkRequest.execute;

public class LaundryRegistrationManager {
    private static final String TAG = "LaundryRegistrationManager";

    /**
     * 상품(품목)카테고리 리스트
     *
     * @param activity View
     * @param storeId  storeId
     * @return
     */
    public static ManagerLaundryCategory getManagerStoreUserList(AppCompatActivity activity, long storeId) {
        final ManagerLaundryCategory[] managerLaundryCategories = {new ManagerLaundryCategory()};
        execute(activity, new Runnable() {

            @Override
            public void run() {
                try {
                    LaundryRegistrationAPI laundryRegistrationApi = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);
                    Call<ManagerLaundryCategory> call = laundryRegistrationApi.getLaundryCategoryList(storeId, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerLaundryCategory> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "-------------getManagerStoreUserList response is null!!!---------------");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response getManagerStoreUserList Success --------");
                                ManagerLaundryCategory.getmInstance().setLaundryCategoty(response.body().laundryCategoryItemLists);
                                managerLaundryCategories[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response getManagerStoreUserList Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerLaundryCategories[0];

    }

    /**
     * 상품(품목) 중분류 카테고리 리스트
     *
     * @param activity   View
     * @param categoryID storeId
     * @return
     */
    public static ManagerClassList getManagerClassList(AppCompatActivity activity, long categoryID) {
        final ManagerClassList[] managerClassLists = {new ManagerClassList()};
        execute(activity, new Runnable() {

            @Override
            public void run() {
                try {
                    LaundryRegistrationAPI laundryRegistrationApi = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);
                    Call<ManagerClassList> call = laundryRegistrationApi.getLaundryClassList(categoryID, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerClassList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                ManagerClassList.getmInstance().setClassListInfo(response.body().itemClassListInfo);
                                managerClassLists[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerClassLists[0];
    }

    /**
     * 가맹점 품목 리스트
     *
     * @param activity
     * @param categoryTypem
     * @param categoryID
     * @param classID
     * @return
     */
    public static ManagerStoreItemList getStoreItemList(AppCompatActivity activity, int categoryTypem, int categoryID, int classID) {
        final ManagerStoreItemList[] storeItemList = {new ManagerStoreItemList()};
        execute(activity, new Runnable() {

            @Override
            public void run() {
                try {
                    LaundryRegistrationAPI laundryRegistrationAPI = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);
                    Call<ManagerStoreItemList> call = laundryRegistrationAPI.getStoreItemList(categoryTypem, categoryID, classID, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerStoreItemList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                ManagerStoreItemList.getmInstance().setStoreItemList(response.body().storeItemList);
                                storeItemList[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return storeItemList[0];

    }

    /**
     * 품목등록 수선요금
     *
     * @param activity
     * @param storeId
     * @return
     */
    public static ManagerStoreItemList getRepairItemList(AppCompatActivity activity, long storeId) {
        final ManagerStoreItemList[] storeItemList = {new ManagerStoreItemList()};
        execute(activity, new Runnable() {

            @Override
            public void run() {
                try {
                    LaundryRegistrationAPI laundryRegistrationAPI = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);
                    Call<ManagerStoreItemList> call = laundryRegistrationAPI.getRepairItemList(2, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerStoreItemList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                storeItemList[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return storeItemList[0];
    }


    /**
     * 품목등록 수선요금
     *
     * @param activity
     * @param storeId
     * @return
     */
    public static ManagerStoreItemList getTechnicalItemList(AppCompatActivity activity, long storeId) {
        final ManagerStoreItemList[] storeItemList = {new ManagerStoreItemList()};
        execute(activity, new Runnable() {

            @Override
            public void run() {
                try {
                    LaundryRegistrationAPI laundryRegistrationAPI = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);
                    Call<ManagerStoreItemList> call = laundryRegistrationAPI.getTechnicalItemList(5, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerStoreItemList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                storeItemList[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return storeItemList[0];
    }

    /**
     * 품목등록 부속품요금
     *
     * @param activity
     * @param storeId
     * @return
     */
    public static ManagerStoreItemList getComponentItemList(AppCompatActivity activity, long storeId) {
        final ManagerStoreItemList[] storeItemList = {new ManagerStoreItemList()};
        execute(activity, new Runnable() {

            @Override
            public void run() {
                try {
                    LaundryRegistrationAPI laundryRegistrationAPI = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);
                    Call<ManagerStoreItemList> call = laundryRegistrationAPI.getComponentItemList(6, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerStoreItemList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                storeItemList[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return storeItemList[0];
    }

    /**
     * 메모리스트
     *
     * @param activity
     * @param memoType
     * @return
     */
    public static ManagerMemoitemList getMemoItemList(AppCompatActivity activity, final int memoType) {
        final ManagerMemoitemList[] storeItemList = {new ManagerMemoitemList()};
        execute(activity, new Runnable() {

            @Override
            public void run() {
                try {
                    LaundryRegistrationAPI laundryRegistrationAPI = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);
                    Call<ManagerMemoitemList> call = laundryRegistrationAPI.getMemoItemList(memoType, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerMemoitemList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                storeItemList[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return storeItemList[0];

    }
    /**
     * 주문 품목 조회(주문ID)
     *
     * @param activity
     * @param orderID 주문ID
     * @return
     */
    public static ManagerOrderItemList getManagerOrderItemList(AppCompatActivity activity, int orderID) {
        final ManagerOrderItemList[] managerOrderItemList = {new ManagerOrderItemList()};
        execute(activity, new Runnable() {

            @Override
            public void run() {
                try {
                    LaundryRegistrationAPI laundryRegistrationAPI = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);
                    Call<ManagerOrderItemList> call = laundryRegistrationAPI.GetOrderItemList(orderID, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerOrderItemList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                managerOrderItemList[0] = response.body();
                                ManagerOrderItemList.getmInstance().setManagerAreaCity(response.body().managerManagerAreaCity);
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerOrderItemList[0];

    }

    /**
     * 품목 등록 Request
     *
     * @param itemAdditionalData
     * @return
     */
    public static String setStoreItemAdd(AppCompatActivity activity, final ArrayList<ItemAdditionalDataType> itemAdditionalData) {
        final LaundryRegistrationAPI laundryRegistrationAPI = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);
        final String[] ResultCode = new String[1];
        if (itemAdditionalData != null) {
            Thread workingThread = new Thread() {
                public void run() {

                    Map<String, Object> param = new HashMap<>();
                    param.put("StoreUserID", SettingLogic.getStoreUserId(activity));
                    param.put("OrderPrice", itemAdditionalData.get(0).getItemAddData().get(0).getOrderItemPrice());
                    for(ItemAdditionalDataType itemAdditionalDataType : itemAdditionalData){
                        param.put("Items", itemAdditionalDataType.getItemAddData());
                    }

                    Call<ApiResult> call = laundryRegistrationAPI.getItemAddRequest(param, SettingLogic.getPreAccessToken(activity));

                    try {
                        Response<ApiResult> response = call.execute();

                        int responseCode = response.code();

                        switch (responseCode) {

                            case 200:
                                Logger.d(TAG, "-------- Item Add Response Success --------");
                                ResultCode[0] = response.body().getResultCode();

                                Logger.d(TAG, "response.body().getResultCode() : " + response.body().getResultCode());
                                Logger.d(TAG, "response.body().getMessage() : " + response.body().getMessage());
                                Logger.d(TAG, "response.body().getData() : " + response.body().getData());

                                break;

                            case 401:
                                Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                                break;

                            case 500:
                                Logger.d(TAG, "서버에러");
                                break;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            workingThread.start();
            try {
                workingThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return ResultCode[0];
    }

    /**
     * 최근 사용한 택번호
     *
     * @param activity
     * @return
     */
    public static void getManagerMaxTagModel(AppCompatActivity activity) {
        execute(activity, new Runnable() {

            @Override
            public void run() {
                final LaundryRegistrationAPI laundryRegistrationAPI = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);

                Thread workingThread = new Thread() {
                    public void run() {
                        Call<GetMaxTagNoModel> call = laundryRegistrationAPI.getGetMaxTagNo(SettingLogic.getPreAccessToken(activity));

                        try {
                            Response<GetMaxTagNoModel> response = call.execute();
                            int responseCode = response.code();

                            switch (responseCode) {
                                case 200:
                                    Logger.d(TAG, "-------- Item Add Response Success --------");
                                    List<GetMaxTagNoModel> managerGetMaxTagNoModel = new ArrayList<>();
                                    managerGetMaxTagNoModel.add(response.body());
                                    ManagerMaxTagModel.getmInstance().setManagerGetMaxTagNoModel(managerGetMaxTagNoModel);
                                    break;

                                case 401:
                                    Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                                    break;

                                case 500:
                                    Logger.d(TAG, "서버에러");
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                };
                workingThread.start();
                try {
                    workingThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 품목 수정 Update
     *
     * @return
     */
    public static String setUpdateVisitOrder(AppCompatActivity activity, int orderId, int totalAmount) {
        final String[] ResultCode = new String[1];
        Thread workingThread = new Thread() {
            public void run() {
                final LaundryRegistrationAPI laundryRegistrationAPI = LaundryRegistrationAPI.AUTH_SERVER.create(LaundryRegistrationAPI.class);

                Map<String, Object> param = new HashMap<>();
                param.put("OrderID", orderId);
                param.put("OrderPrice", totalAmount);
                for (int i = 0; i < ManagerItemAdditionalDataEditType.getInstance().getItemAdditionalData().size(); i++) {
                    param.put("Items", ManagerItemAdditionalDataEditType.getInstance().getItemAdditionalData().get(i).getItemAddData());
                }
                Call<ApiResult> call = laundryRegistrationAPI.getUpdateVisitOrder(param, SettingLogic.getPreAccessToken(activity));

                SharedPreferencesUtil.removeSharedPreference(activity, PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY);
                try {
                    Response<ApiResult> response = call.execute();

                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            ResultCode[0] = response.body().getResultCode();

                            Logger.d(TAG, "response.body().getResultCode() : " + response.body().getResultCode());
                            Logger.d(TAG, "response.body().getMessage() : " + response.body().getMessage());
                            Logger.d(TAG, "response.body().getData() : " + response.body().getData());

                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {

            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return ResultCode[0];
    }

}
