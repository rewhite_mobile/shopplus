package rewhite.shopplus.client.network;


import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import rewhite.shopplus.client.manager.ManagerCutomerModel;
import rewhite.shopplus.client.manager.ManagerFaqCateforyList;
import rewhite.shopplus.client.manager.ManagerFaqList;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;

import static rewhite.shopplus.client.network.NetworkRequest.execute;

/**
 * 고겍센터 APi Manager Class
 */

public class CustomerManager {
    private static final String TAG = "AuthManager";

    /**
     * 공지사항 리스트
     *
     * @param activity
     * @param page     페이지번호
     * @param block    페이지크기
     */
    public static ManagerCutomerModel getNoticeListe(AppCompatActivity activity, int page, int block) {
        final ManagerCutomerModel[] managerCutomerModel = {new ManagerCutomerModel()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    CustomerAPI customerAPI = CustomerAPI.AUTH_SERVER.create(CustomerAPI.class);
                    Call<ManagerCutomerModel> call = customerAPI.getNoticeList(page, block, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerCutomerModel> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ManagerCutomerModel.getmInstance().setCutomerModel(response.body().itemCutomerModel);
                                managerCutomerModel[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerCutomerModel[0];
    }


    /**
     * FAQ 카테고리 리스트
     *
     * @param activity
     */
    public static ManagerFaqCateforyList getFaqCateforyListe(AppCompatActivity activity) {
        final ManagerFaqCateforyList[] managerFaqCateforyList = {new ManagerFaqCateforyList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    CustomerAPI customerAPI = CustomerAPI.AUTH_SERVER.create(CustomerAPI.class);
                    Call<ManagerFaqCateforyList> call = customerAPI.getFaqCategoryList(SettingLogic.getPreAccessToken(activity));

                    Response<ManagerFaqCateforyList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ManagerFaqCateforyList.getmInstance().setFaqCategoryListModel(response.body().itemFaqCategoryListModel);
                                managerFaqCateforyList[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerFaqCateforyList[0];
    }

    /**
     * FAQ 카테고리 리스트
     *
     * @param activity
     * @param faqCategoryID FAQ 카테고리 ID
     * @param page          페이지번호
     * @param block         페이지크기
     * @return
     */
    public static ManagerFaqList getFaqList(AppCompatActivity activity, int faqCategoryID, String searchText, int page, int block) {
        final ManagerFaqList[] managerFaqLists = {new ManagerFaqList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    Call<ManagerFaqList> call;
                    CustomerAPI customerAPI = CustomerAPI.AUTH_SERVER.create(CustomerAPI.class);

                    if (searchText.equals("") || TextUtils.isEmpty(searchText)) {
                        call = customerAPI.getFaqList(faqCategoryID, page, block, SettingLogic.getPreAccessToken(activity));
                    } else {
                        call = customerAPI.getFaqList(faqCategoryID, searchText, page, block, SettingLogic.getPreAccessToken(activity));
                    }
                    Response<ManagerFaqList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ManagerFaqList.getmInstance().setManagerFaqList(response.body().managerFaqListModel);

                                managerFaqLists[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerFaqLists[0];
    }

    /**
     * 상담신청 REQUEST
     * @param activity
     * @param storeID      가맹점ID
     * @param ReqStoreName 신청 가맹점 명
     * @param ReqPhone     신청자 연락처
     * @param ReqContext   신청 내용
     */
    public static void getRequsetCounsel(AppCompatActivity activity, long storeID, String ReqStoreName, String ReqPhone, String ReqContext) {
        final CustomerAPI customerAPI = CustomerAPI.AUTH_SERVER.create(CustomerAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = customerAPI.getRequsetCounsel(storeID, ReqStoreName, ReqPhone, ReqContext, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}