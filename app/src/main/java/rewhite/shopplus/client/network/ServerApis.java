package rewhite.shopplus.client.network;

/**
 * 서버 API 호출시 사용되는 클래스
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */

public class ServerApis {

    public static final String TEST_PROTOCOL = "https://";  //TEST_BED 프로토콜
    public static final String LIVEBED_PROTOCL = "http://"; //LIVE_BED 프로토콜

    public static final String LIVEBED_AUTHURL = ""; //LIVE_BED Vertion.
    public static final String TESTBED_AUTHURL = "tshopapi.rewhite.me/"; //TEST_BED Vertion.

    private static final String API_VER = "v1.1"; //API Vertion

    /**
     * Http로 통신하는 API들
     */
    public enum HttpAPI {
        /**
         * 세탁 주문 정보 반환
         */
        PURCHASE_INFO,
        /**
         * Shop+ 서버 타임 반환
         */
        SERVER_TIME,
        /**
         * 암호화 키 반환 (데이터 암호화 서버단에서 처리 한다고 했으나, 대응코드는 필요함.)
         */
        SECURE_KEY,
        /**
         * Shop+ 업데이트 체크
         */
        UPDATE_CHECK;

        @Override
        public String toString() {
            StringBuilder result = new StringBuilder("act=");

            switch (this) {
                case PURCHASE_INFO:
                    result.append("purchaseinfo");
                    break;
                case SERVER_TIME:
                    result.append("ymd");
                    break;
                case SECURE_KEY:
                    result.append("gk");
                    break;
                case UPDATE_CHECK:
                    result.append("verChk");
                    break;
                default:
                    break;
            }

            return result.toString();
        }

    }

    /**
     * Http 통신 API URL 반환
     *
     * @param query : HttpAPI 클래스 참조
     * @return
     */
    public static String getHttpApiUrl(HttpAPI query) {
        String url = null;

        switch (query) {
            case UPDATE_CHECK:
            case PURCHASE_INFO:
            case SECURE_KEY:
            case SERVER_TIME:
            default:
                //ver API
//                url = getVerApiUrl(query);
//                break;
        }

        return url;
    }

}
