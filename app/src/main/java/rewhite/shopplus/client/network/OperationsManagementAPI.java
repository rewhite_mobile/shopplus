package rewhite.shopplus.client.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rewhite.shopplus.client.constant.NetConstant;
import rewhite.shopplus.client.manager.ManagerCardPaymentManageList;
import rewhite.shopplus.client.manager.ManagerNonPayOrderItemManageList;
import rewhite.shopplus.client.manager.ManagerNonPayUserManageList;
import rewhite.shopplus.client.manager.ManagerNotOutOrderItemManageList;
import rewhite.shopplus.client.manager.ManagerNotOutUserManageList;
import rewhite.shopplus.client.manager.ManagerPrepaidManageList;
import rewhite.shopplus.client.manager.ManagerUserManageList;

/**
 * 운영관리 API
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
interface OperationsManagementAPI {

    public HttpLoggingInterceptor LOGGING = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);

    public OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .connectTimeout(NetConstant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(NetConstant.WRITE_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(NetConstant.READ_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(LOGGING)
            .build();

    public Retrofit AUTH_SERVER = new Retrofit.Builder()
            //프로토콜 TEST / LIVE CHECK
            .baseUrl(ServerApis.TEST_PROTOCOL + ServerApis.TESTBED_AUTHURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OK_HTTP_CLIENT)
            .build();


    @POST("/Manage/UserManageList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //회원 조회
    Call<ManagerUserManageList> getManagerUserManageList(@Query("userGrade") String userGrade, @Query("startDate") String startDate, @Query("endDate") String endDate, @Header("accesstoken") String authorization);


    @POST("/Manage/NonPayUserManageList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //미수 회원 조회
    Call<ManagerNonPayUserManageList> getNonPayUserManageList(@Query("searchPhone") String searchPhone, @Query("userName") String userName, @Header("accesstoken") String authorization);

    @POST("/Manage/NonPayOrderItemManageList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //미수 품목 조회
    Call<ManagerNonPayOrderItemManageList> getNonPayOrderItemManageList(@Query("searchType") String searchType, @Header("accesstoken") String authorization);

    @POST("/Manage/NotOutUserManageList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //미출고 회원 조회
    Call<ManagerNotOutUserManageList> getNotOutUserManageList(@Query("searchPhone") String searchPhone, @Query("userName") String userName, @Header("accesstoken") String authorization);

    @POST("/Manage/NotOutOrderItemManageList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //미출고 품목 조회
    Call<ManagerNotOutOrderItemManageList> getNotOutOrderItemManageList(@Query("stayDay") int stayDay, @Query("startDate") String startDate, @Query("endDate") String endDate, @Query("statusType") String statusType, @Header("accesstoken") String authorization);


    @POST("/Manage/CardPaymentManageList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //카드 결제 조회
    Call<ManagerCardPaymentManageList> getCardPaymentManageList(@Query("searchPhone") String searchPhone,@Query("userName") String userName,@Query("startDate") String startDate,
                                                                @Query("endDate") String endDate,@Query("status") int status,@Header("accesstoken") String authorization);

    @POST("/Manage/PrepaidManageList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //선충전금 조회
    Call<ManagerPrepaidManageList> gePrepaidManageList(@Query("searchPhone") String searchPhone, @Query("userName") String userName, @Query("startDate") String startDate,
                                                       @Query("endDate") String endDate, @Query("status") int status,@Header("accesstoken") String authorization);

}