package rewhite.shopplus.client.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rewhite.shopplus.client.constant.NetConstant;
import rewhite.shopplus.client.manager.ManagerGetDaySalesStatList;
import rewhite.shopplus.client.manager.ManagerGetMonthSalesStatList;
import rewhite.shopplus.client.model.GetMainSalesInfo;

/**
 * 매출 현황 API
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
interface SalesStatusAPI {
    public static final HttpLoggingInterceptor LOGGING = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);

    public static final OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .connectTimeout(NetConstant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(NetConstant.WRITE_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(NetConstant.READ_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(LOGGING)
            .build();

    public static final Retrofit AUTH_SERVER = new Retrofit.Builder()
            //프로토콜 TEST / LIVE CHECK
            .baseUrl(ServerApis.TEST_PROTOCOL + ServerApis.TESTBED_AUTHURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OK_HTTP_CLIENT)
            .build();

    @POST("/Sales/GetMainSalesInfo")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    //메인화면 매출 현황 정보 조회
    Call<GetMainSalesInfo> getMainSalesInfo(@Header("accesstoken") String authorization);


    @POST("/Sales/GetDaySalesStatList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    //일별 매출 현황 (기본기간:이번달)
    Call<ManagerGetDaySalesStatList> getDaySalesStatList(@Query("startDate") String startDate, @Query("endDate") String endDate, @Header("accesstoken") String authorization);

    @POST("/Sales/GetMonthSalesStatList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    //월별 매출 현황 (기본기간:최근3개월)
    Call<ManagerGetMonthSalesStatList> getMonthSalesStatList(@Query("startDate") String startDate, @Query("endDate") String endDate, @Header("accesstoken") String authorization);
}