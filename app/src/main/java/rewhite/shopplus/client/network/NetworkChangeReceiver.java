package rewhite.shopplus.client.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.text.TextUtils;

import rewhite.shopplus.activity.ShopPlusApplication;
import rewhite.shopplus.data.type.NetworkStatus;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.NetworkUtils;

public class NetworkChangeReceiver extends BroadcastReceiver {
    private static final String TAG = "NetworkChangedReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.d(TAG, "change network");
        String action = intent.getAction();

        if (!TextUtils.isEmpty(action) && action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            try {
                NetworkStatus status = NetworkUtils.getNetworkStatus(context);

                ShopPlusApplication.setCurrentNetworkStatus(status);
            } catch (Exception e) {
                Logger.e(TAG, "exception ", e);
            }
        }

    }

}