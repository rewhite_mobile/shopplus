package rewhite.shopplus.client.network;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import rewhite.shopplus.client.manager.ManagerGetHandleItemList;
import rewhite.shopplus.client.manager.ManagerGetStoreDeviceInfo;
import rewhite.shopplus.client.manager.ManagerGetStoreItemList;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.manager.ManagerGetStoreOutSideList;
import rewhite.shopplus.client.manager.ManagerStoreContractInfo;
import rewhite.shopplus.client.model.ApiResult;
import rewhite.shopplus.client.model.ChangePasswordModel;
import rewhite.shopplus.client.model.GetStoreContractInfoModel;
import rewhite.shopplus.client.model.GetStoreDeviceInfo;
import rewhite.shopplus.client.model.GetStoreManageInfo;
import rewhite.shopplus.data.manager.ManagerChangePasswordModel;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;

import static rewhite.shopplus.client.network.NetworkRequest.execute;

/**
 * 환경설정 관련 API Manager
 */
public class EnviromentManager {
    private static final String TAG = "EnviromentManager";

    /**
     * 매장 관리 정보 조회
     *
     * @param activity View
     * @return
     */
    public static void getManagerStoreManageInfo(AppCompatActivity activity) {
        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);
                    Call<GetStoreManageInfo> call = enviromentAPI.getManagerGetStoreManageInfo(SettingLogic.getPreAccessToken(activity));

                    Response<GetStoreManageInfo> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ArrayList<GetStoreManageInfo> arrayList = new ArrayList<>();
                                arrayList.add(response.body());
                                ManagerGetStoreManageInfo.getmInstance().setGetVisitOrder(arrayList);

                                Logger.d(TAG, "ManagerGetStoreManageInfo.getmInstance() : " + ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType());
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
    }

    /**
     * 가맹점 장비설정 정보 조회
     *
     * @param activity View
     * @return
     */
    public static void getStoreDeviceInfo(AppCompatActivity activity) {
        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);
                    Call<GetStoreDeviceInfo> call = enviromentAPI.getStoreDeviceInfo(SettingLogic.getPreAccessToken(activity));

                    Response<GetStoreDeviceInfo> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ArrayList<GetStoreDeviceInfo> arrayList = new ArrayList<>();
                                arrayList.add(response.body());

                                ManagerGetStoreDeviceInfo.getmInstance().setGetStoreDeviceInfo(arrayList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
    }


    /**
     * 세탁물 취급 품목 리스트 조회
     *
     * @param activity View
     * @param storeId  storeId
     * @return
     */
    public static ManagerGetHandleItemList getManagerGetHandleItemList(AppCompatActivity activity, long storeId) {

        final ManagerGetHandleItemList[] managerGetHandleItemList = {new ManagerGetHandleItemList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);
                    Call<ManagerGetHandleItemList> call = enviromentAPI.getHandleItemList(storeId, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerGetHandleItemList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                ManagerGetHandleItemList.getmInstance().setGetHandleItemList(response.body().itemGetHandleItemList);
                                managerGetHandleItemList[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerGetHandleItemList[0];
    }

    /**
     * 외주업체 리스트 리스트 조회
     *
     * @param activity View
     * @return
     */
    public static ManagerGetStoreOutSideList getManagerGetStoreOutSideList(AppCompatActivity activity) {
        final ManagerGetStoreOutSideList[] managerGetStoreOutSideLists = {new ManagerGetStoreOutSideList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);
                    Call<ManagerGetStoreOutSideList> call = enviromentAPI.getStoreOutSideList(SettingLogic.getPreAccessToken(activity));

                    Response<ManagerGetStoreOutSideList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                ManagerGetStoreOutSideList.getmInstance().setManagerGetStoreOutSideList(response.body().itemGetStoreOutSideList);
                                managerGetStoreOutSideLists[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerGetStoreOutSideLists[0];
    }

    /**
     * 품목 리스트 조회(매장관리용) 조회
     *
     * @param activity     View
     * @param categoryType categoryType
     * @param categoryID   categoryID
     * @param classID      classID
     * @return
     */
    public static ManagerGetStoreItemList getManagerGetStoreItemList(AppCompatActivity activity, int categoryType, int categoryID, int classID) {
        final ManagerGetStoreItemList[] managerLaundryCategories = {new ManagerGetStoreItemList()};
        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);
                    Call<ManagerGetStoreItemList> call = enviromentAPI.getGetStoreItemList(categoryType, categoryID, classID, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerGetStoreItemList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                ManagerGetStoreItemList.getmInstance().setGetHandleItemList(response.body().itemStoreItemList);
                                managerLaundryCategories[0] = response.body();
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }

                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerLaundryCategories[0];
    }

    /**
     * 매장 인사말 변경 REQUEST
     *
     * @param greetings 매장인사말
     */
    public static void getUpdateStoreGreeting(AppCompatActivity activity, String greetings) {
        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getUpdateStoreGreeting(greetings, SettingLogic.getPreAccessToken(activity));
                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {
                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 택번호 타입 변경 REQUEST
     *
     * @param tagNoType 택 번호 Type
     */
    public static void getUpdateTagNoType(AppCompatActivity activity, int tagNoType) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getUpdateTagNoType(tagNoType, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 지원결제 방식 변경 REQUEST
     *
     * @param availPaymentType 결제 지원 방식 Type
     */
    public static void getUpdateAvailPaymentType(AppCompatActivity activity, String availPaymentType) {
        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {

                Logger.d(TAG, "availPaymentType : " + availPaymentType);
                Call<Void> call = enviromentAPI.getUpdateAvailPaymentType(availPaymentType, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 선충전금 설정 변경 REQUEST
     *
     * @param isGiveBonusPrePaid   선충전금 보너스 지급 여부(Y/N)
     * @param cashBonusPrePaidRate 현금 선충전금 보너스 지급율
     * @param cardBonusPrePaidRate 카드 선충전금 보너스 지급율
     * @param isPrePaidCashReceipt 선충전금 현금영수증 발급 여부(Y/N)
     */
    public static String getUpdatePrePaidManage(AppCompatActivity activity, String isGiveBonusPrePaid, double cashBonusPrePaidRate, double cardBonusPrePaidRate, String isPrePaidCashReceipt) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);
        final String[] ResultCode = new String[1];

        Thread workingThread = new Thread() {
            public void run() {
                Call<ApiResult> call = enviromentAPI.getUpdatePrePaidManage(isGiveBonusPrePaid, cashBonusPrePaidRate, cardBonusPrePaidRate, isPrePaidCashReceipt, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "--------getUpdatePrePaidManage Response Success --------");
                            ResultCode[0] = response.body().getResultCode();

                            break;

                        case 401:
                            Logger.d(TAG, "--------getUpdatePrePaidManage Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "getUpdatePrePaidManage 서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }

    /**
     * 마일리지 설정 변경 REQUEST
     *
     * @param isSaveMileage      마일리지 적립여부(Y/N)
     * @param cashMileageRate    현금 결제 마일리지 적립율
     * @param cardMileageRate    카드 결제 마일리지 적립율
     * @param mileageUseMinPrice 마일리지 사용 최소 금액
     */
    public static String getUpdateMileageManage(AppCompatActivity activity, String isSaveMileage, double cashMileageRate, double cardMileageRate, int mileageUseMinPrice) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);
        final String[] ResultCode = new String[1];
        Thread workingThread = new Thread() {
            public void run() {

                Call<ApiResult> call = enviromentAPI.getUpdateMileageManage(isSaveMileage, cashMileageRate, cardMileageRate, mileageUseMinPrice, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            ResultCode[0] = response.body().getResultCode();
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return ResultCode[0];
    }

    /**
     * 정기휴무일 설정 변경 REQUEST
     *
     * @param weekType    휴무간격타입(1:매주, 2:격주(1,3주), 3:격주(2,4주))
     * @param closingWeek 휴무요일(월화수목금토일(ex:"1111100"))
     */
    public static void getUpdateClosedRegularly(AppCompatActivity activity, int weekType, String closingWeek) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Map<String, Object> param = new HashMap<>();

                Call<Void> call = enviromentAPI.getUpdateClosingWeek(weekType, closingWeek, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "--------UpdateClosedRegularly Response Success --------");
                            Logger.d(TAG, "response mag : " + response.body());
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 정기휴무일 설정 변경 REQUEST
     *
     * @param closingDay 추가할 휴무일(yyyy-MM-dd)
     */
    public static void getAddClosingDay(AppCompatActivity activity, String closingDay) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);
        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getAddClosingDay(closingDay, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 정기휴무일 제 변경 REQUEST
     *
     * @param activity
     * @param closingDay 제거할 휴무일(yyyy-MM-dd)
     */
    public static void getRemoveClosingDay(AppCompatActivity activity, String closingDay) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getRemoveClosingDay(closingDay, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * 영업시간 설정 변경 REQUEST
     *
     * @param activity
     * @param serviceStartTime     평일 오픈(HH:mm)
     * @param serviceEndTime       평일 마감(HH:mm)
     * @param serviceStartTime_Sat 토요일 오픈(HH:mm)
     * @param serviceEndTime_Sat   토요일 마감(HH:mm)
     * @param serviceStartTime_Sun 일요일 오픈(HH:mm)
     * @param serviceEndTime_Sun   일요일 마감(HH:mm
     */
    public static void getUpdateServiceTime(AppCompatActivity activity, String serviceStartTime, String serviceEndTime, String serviceStartTime_Sat, String serviceEndTime_Sat, String serviceStartTime_Sun, String serviceEndTime_Sun) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getUpdateServiceTime(serviceStartTime, serviceEndTime, serviceStartTime_Sat, serviceEndTime_Sat, serviceStartTime_Sun, serviceEndTime_Sun, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 세탁소요기간 설정 변경 REQUEST
     *
     * @param washingCostDay 세탁소요일
     */
    public static void getUpdateWashingCostDay(AppCompatActivity activity, int washingCostDay) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getUpdateWashingCostDay(washingCostDay, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 세탁물 취급 품목 설정 변경 REQUEST
     *
     * @param itemGroupID 세탁물 취급품목ID
     * @param isUse       취급여부(Y/N)
     */
    public static void getSetHandleItem(Context context, int itemGroupID, String isUse) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getSetHandleItem(itemGroupID, isUse, SettingLogic.getPreAccessToken(context));
                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 외주업체 추가 설정 변경 REQUEST
     *
     * @param activity
     * @param outSideName 외주업체명
     * @param processDay  처리기간
     * @param itemGroupID 품목그룹ID
     */
    public static void getAddStoreOutSide(AppCompatActivity activity, String outSideName, int processDay, int itemGroupID) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Map<String, Object> param = new HashMap<>();
                param.put("outSideName", outSideName);
                param.put("processDay", processDay);
                param.put("itemGroupID", itemGroupID);

                Call<Void> call = enviromentAPI.getAddStoreOutSide(param, "1");

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 외주업체 수정 변경 REQUEST
     *
     * @param activity
     * @param storeOutSideID 외주업체ID
     * @param outSideName    외주업체명
     * @param processDay     처리기간
     * @param itemGroupID    품목그룹ID
     */
    public static void getUpdateStoreOutSide(AppCompatActivity activity, long storeOutSideID, String outSideName, int processDay, int itemGroupID) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Map<String, Object> param = new HashMap<>();
                param.put("storeOutSideID", storeOutSideID);
                param.put("outSideName", outSideName);
                param.put("processDay", processDay);
                param.put("itemGroupID", itemGroupID);

                Call<Void> call = enviromentAPI.getUpdateStoreOutSide(param, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * 외주업체 삭제 변경 REQUEST
     *
     * @param activity
     * @param storeOutSideID 외주업체ID
     */
    public static void getRemoveStoreOutSide(AppCompatActivity activity, long storeOutSideID) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Map<String, Object> param = new HashMap<>();
                param.put("storeOutSideID", storeOutSideID);

                Call<Void> call = enviromentAPI.getRemoveStoreOutSide(param, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 품목 요금 변경 REQUEST
     *
     * @param storeItemID      가맹점품목ID
     * @param itemVisitPrice   방문가격(일반)
     * @param itemVisitPrice2  방문가격(명품)
     * @param itemVisitPrice3  방문가격(아동)
     * @param itemPickupPrice  수거가격(일반)
     * @param itemPickupPrice2 수거가격(명품)
     * @param itemPickupPrice3 수거가격(아동)
     */
    public static void getRUpdateStoreItemPrice(AppCompatActivity activity, long storeItemID, int itemVisitPrice, int itemVisitPrice2, int itemVisitPrice3, int itemPickupPrice, int itemPickupPrice2, int itemPickupPrice3) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {

                Call<Void> call = enviromentAPI.getRUpdateStoreItemPrice(storeItemID, itemVisitPrice, itemVisitPrice2, itemVisitPrice3, itemPickupPrice, itemPickupPrice2, itemPickupPrice3, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 품목 상태 변경 REQUEST
     *
     * @param storeItemID 가맹점 품목ID
     * @param isUse       사용여부(Y/N)
     */
    public static void getUpdateStoreItemState(Context context, long storeItemID, String isUse) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getUpdateStoreItemState(storeItemID, isUse, SettingLogic.getPreAccessToken(context));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 품목 추가 변경 REQUEST
     *
     * @param classID  중분류ID
     * @param itemName 품목명
     */
    public static void getAddStoreItem(AppCompatActivity activity, int classID, String itemName) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getAddStoreItem(classID, itemName, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 품목명 수정 REQUEST
     *
     * @param storeItemID 중분류ID
     * @param itemName    품목명
     */
    public static void getUpdateStoreItem(AppCompatActivity activity, long storeItemID, String itemName) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getUpdateStoreItem(storeItemID, itemName, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 가맹점 접수증 인쇄 설정 변경 REQUEST
     *
     * @param printCount     인쇄매수
     * @param isPrintBalance 회원 잔액(선충전금, 마일리지 잔액) 인쇄 여부(Y/N)
     */
    public static void getUpdateStorePrintSetting(AppCompatActivity activity, int printCount, String isPrintBalance) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getUpdateStorePrintSetting(printCount, isPrintBalance, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 손해배상기준 인쇄 설정 변경 REQUEST
     *
     * @param isPrintDamageNotice 손해배상기준 인쇄 여부(Y/N)
     * @param damageNotice        손해배상기준 안내 문구
     */
    public static void getUpdateDamageNotice(AppCompatActivity activity, String isPrintDamageNotice, String damageNotice) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Map<String, Object> param = new HashMap<>();
                Call<Void> call = enviromentAPI.getUpdateDamageNotice(isPrintDamageNotice, damageNotice, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 세탁물 보관료 안내 인쇄 설정 변경 REQUEST
     *
     * @param IsPrintStotageNotice 손해배상기준 인쇄 여부(Y/N)
     * @param storageNotice        손해배상기준 안내 문구
     */
    public static void getUpdateStorageNotice(AppCompatActivity activity, String IsPrintStotageNotice, String storageNotice) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Map<String, Object> param = new HashMap<>();

                Call<Void> call = enviromentAPI.getUpdateStorageNotice(IsPrintStotageNotice, storageNotice, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 가맹점 안내(세탁소 인사말) 인쇄 설정 변경 REQUEST
     *
     * @param isPrintStoreNotice 가맹점 안내 인쇄 여부(Y/N)
     * @param StoreNotice        가맹점 안내 문구
     */
    public static void getUpdateStoreNotice(AppCompatActivity activity, String isPrintStoreNotice, String StoreNotice) {

        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = enviromentAPI.getUpdateStoreNotice(isPrintStoreNotice, StoreNotice, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 가맹점 계정 비밀번호 변경 REQUEST
     *
     * @param old_Password 현재 패스워드
     * @param new_Password 신규 패스워드
     */
    public static String getChangePassword(AppCompatActivity activity, String old_Password, String new_Password) {

        final String[] ResultCode = new String[1];
        final EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<ChangePasswordModel> call = enviromentAPI.getChangePassword(old_Password, new_Password, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<ChangePasswordModel> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                ArrayList<ChangePasswordModel> changePasswordModels = new ArrayList<>();
                                ChangePasswordModel changePasswordModel = new ChangePasswordModel();

                                changePasswordModel.setMessage(response.message());
                                changePasswordModel.setResultCode(response.body().getResultCode());
                                changePasswordModels.add(changePasswordModel);

                                ManagerChangePasswordModel.getmInstance().ManagerChangePasswordModel(changePasswordModels);
                            }
                            break;
                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }

    /**
     * 계약정보 조회
     *
     * @param activity
     * @return
     */
    public static void getStoreContractInfo(AppCompatActivity activity) {
        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    EnviromentAPI enviromentAPI = EnviromentAPI.AUTH_SERVER.create(EnviromentAPI.class);
                    Call<GetStoreContractInfoModel> call = enviromentAPI.getStoreContractInfo(SettingLogic.getPreAccessToken(activity));

                    Response<GetStoreContractInfoModel> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "-------- Response Success --------");
                                ArrayList<GetStoreContractInfoModel> arrayList = new ArrayList<>();
                                arrayList.add(response.body());
                                ManagerStoreContractInfo.getmInstance().setStoreContractInfo(arrayList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "-------- Response Error --------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
    }
}