package rewhite.shopplus.client.network;


import android.support.v7.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import rewhite.shopplus.client.manager.ManagerAreaCity;
import rewhite.shopplus.client.manager.ManagerAreaDistrict;
import rewhite.shopplus.client.manager.ManagerAuthStoreLogin;
import rewhite.shopplus.client.model.ApiResult;
import rewhite.shopplus.client.model.StoreLoginModel;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.data.manager.ManagerItemAdditionalDataType;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.SharedPreferencesUtil;

import static rewhite.shopplus.client.network.NetworkRequest.execute;

/**
 * 자동로그인 등 user토큰을 관리 하기위해서 사용하는 class
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */

public class AuthManager {
    private static final String TAG = "AuthManager";

    /**
     * 가입상담 신청
     *
     * @param activity
     * @param StoreName  가입상담 가맹점명
     * @param Phone      연락처
     * @param Sido       지역(시/도)
     * @param Gugun      지역(구/시/도)
     * @param IsDelivery 수거배송여부
     */
    public static void getRequestJoinStore(AppCompatActivity activity, String StoreName, String Phone, String Sido, String Gugun, String IsDelivery) {

        final AuthAPI authAPI = AuthAPI.AUTH_SERVER.create(AuthAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<Void> call = authAPI.getRequestJoinStore(StoreName, Phone, Sido, Gugun, IsDelivery, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {
                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 로그인ID 찾기
     *
     * @param activity
     */
    public static String getgetFindLoginID(AppCompatActivity activity, String phoneNum) {
        final String[] ResultCode = new String[1];
        final AuthAPI authAPI = AuthAPI.AUTH_SERVER.create(AuthAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<ApiResult> call = authAPI.getFindLoginID(phoneNum);

                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {
                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            ResultCode[0] = response.body().getResultCode();

                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }


    /**
     * 로그인PW 찾기
     *
     * @param activity
     */
    public static String getFindLoginPassword(AppCompatActivity activity, String phoneNum) {
        final String[] ResultCode = new String[1];
        final AuthAPI authAPI = AuthAPI.AUTH_SERVER.create(AuthAPI.class);

        Thread workingThread = new Thread() {
            public void run() {
                Call<ApiResult> call = authAPI.getFindLoginPassword(phoneNum);

                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {
                        case 200:
                            ResultCode[0] = response.body().getResultCode();

                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }


    /**
     * 가맹점 로그인
     *
     * @param activity View
     * @return
     */
    public static void getStoreLogin(AppCompatActivity activity, String loginID, String password, String deviceToken) {
        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    AuthAPI authAPI = AuthAPI.AUTH_SERVER.create(AuthAPI.class);
                    Call<StoreLoginModel> call = authAPI.getStoreLogin(loginID, password, deviceToken);

                    Response<StoreLoginModel> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ArrayList<StoreLoginModel> arrayList = new ArrayList<>();
                                arrayList.add(response.body());
                                ManagerAuthStoreLogin.getmInstance().setmanagerAuthStoreLogin(arrayList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
    }

    /**
     * 엑세 토큰 확인
     *
     * @param activity View
     * @param AccessToken 디바이스 토큰
     * @return
     */
    public static String CheckAccessToken(AppCompatActivity activity, String AccessToken) {
        final String[] ResultCode = new String[1];

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    AuthAPI authAPI = AuthAPI.AUTH_SERVER.create(AuthAPI.class);
                    Call<StoreLoginModel> call = authAPI.CheckAccessToken(AccessToken);

                    Response<StoreLoginModel> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ArrayList<StoreLoginModel> arrayList = new ArrayList<>();
                                arrayList.add(response.body());
                                ManagerAuthStoreLogin.getmInstance().setmanagerAuthStoreLogin(arrayList);

                                ResultCode[0] = response.body().getResultCode();
                                Logger.d(TAG, "ResultCode[0]  :: " + ResultCode[0] );
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return ResultCode[0];
    }

    /**
     * 지역(시/도) 조회
     *
     * @param activity View
     * @return
     */
    public static ManagerAreaCity getAreaCityList(AppCompatActivity activity) {
        final ManagerAreaCity[] managerAreaCities = {new ManagerAreaCity()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    AuthAPI authAPI = AuthAPI.AUTH_SERVER.create(AuthAPI.class);
                    Call<ManagerAreaCity> call = authAPI.getAreaCityList(SettingLogic.getPreAccessToken(activity));

                    Response<ManagerAreaCity> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ManagerAreaCity.getmInstance().setManagerAreaCity(response.body().managerManagerAreaCity);


                                managerAreaCities[0] = response.body();

                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerAreaCities[0];
    }


    /**
     * 지역(시/구/군) 조회
     *
     * @param activity View
     * @return
     */
    public static ManagerAreaDistrict getAreaDistrictList(AppCompatActivity activity, int parnetAreaID) {
        final ManagerAreaDistrict[] managerAreaCities = {new ManagerAreaDistrict()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    AuthAPI authAPI = AuthAPI.AUTH_SERVER.create(AuthAPI.class);
                    Call<ManagerAreaDistrict> call = authAPI.getAreaDistrictList(parnetAreaID, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerAreaDistrict> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ManagerAreaDistrict.getmInstance().setAreaDistrictListModel(response.body().managerAreaDistrictListModel);
                                managerAreaCities[0] = response.body();

                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerAreaCities[0];
    }

    /**
     * 품목 등록 Request
     *
     * @return
     */
    public static String setStoreItemAd(AppCompatActivity activity) {
        final AuthAPI laundryRegistrationAPI = AuthAPI.AUTH_SERVER.create(AuthAPI.class);
        final String[] ResultCode = new String[1];
        Thread workingThread = new Thread() {
            public void run() {

                Map<String, Object> param = new HashMap<>();
                param.put("StoreUserID", SettingLogic.getStoreUserId(activity));
                param.put("OrderPrice", SharedPreferencesUtil.getIntSharedPreference(activity, PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY));
                for (int i = 0; i < ManagerItemAdditionalDataType.getInstance().getItemAdditionalData().size(); i++) {
                    param.put("Items", ManagerItemAdditionalDataType.getInstance().getItemAdditionalData().get(i).getItemAddData());
                }
                Call<ApiResult> call = laundryRegistrationAPI.getItemAddReques(param, SettingLogic.getPreAccessToken(activity));

                SharedPreferencesUtil.removeSharedPreference(activity, PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY);
                try {
                    Response<ApiResult> response = call.execute();

                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            ResultCode[0] = response.body().getResultCode();

                            Logger.d(TAG, "response.body().getResultCode() : " + response.body().getResultCode());
                            Logger.d(TAG, "response.body().getMessage() : " + response.body().getMessage());
                            Logger.d(TAG, "response.body().getData() : " + response.body().getData());

                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return ResultCode[0];
    }

}