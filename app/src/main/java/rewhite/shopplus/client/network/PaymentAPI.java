package rewhite.shopplus.client.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rewhite.shopplus.client.constant.NetConstant;
import rewhite.shopplus.client.manager.ManagerGetPaymentInfoList;
import rewhite.shopplus.client.manager.ManagerStorePaymentCancel;
import rewhite.shopplus.client.model.ApiResult;
import rewhite.shopplus.client.model.GetPaymentInfoModel;
import rewhite.shopplus.data.dto.ItemPaymentStoreOrderItem;

/**
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
interface PaymentAPI {

    public HttpLoggingInterceptor LOGGING = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    public OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .connectTimeout(NetConstant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(NetConstant.WRITE_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(NetConstant.READ_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(LOGGING)
            .build();

    public Retrofit AUTH_SERVER = new Retrofit.Builder()
            //프로토콜 TEST / LIVE CHECK
            .baseUrl(ServerApis.TEST_PROTOCOL + ServerApis.TESTBED_AUTHURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OK_HTTP_CLIENT)
            .build();

    @POST("/Payment/GetStorePaymentCancelAvailList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //결제취소 가능한 회원 결제내역
    Call<ManagerStorePaymentCancel> getStorePaymentCancelAvailList(@Query("storeUserID") long storeUserID, @Header("accesstoken") String authorization);


    @POST("/Payment/ChargeStoreUserPrePaid")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

        //가맹점 선충전금 충전(현금)
    Call<ApiResult> getManagerChargeStoreUserPrePaid(@Query("storeUserID") long storeUserID, @Query("paymentType") int paymentType, @Query("paymentPrice") int paymentPrice, @Header("accesstoken") String authorization);

    @POST("/Payment/ChargeStoreUserPrePaid")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

        //가맹점 선충전금 충전(카드)
    Call<ApiResult> getManagerCardChargeStoreUserPrePaid(@Query("storeUserID") long storeUserID, @Query("paymentType") int paymentType, @Query("paymentPrice") int paymentPrice,
                                                         @Query("TID") String TID, @Query("CATInfo") String CATInfo, @Query("cardName") String cardName, @Query("cardNo") String cardNo,
                                                         @Query("cardDivide") int cardDivide, @Header("accesstoken") String authorization);


    @POST("/Payment/PaymentStoreOrderItem")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

        //가맹점 주문 품목 결제(현금)
    Call<ItemPaymentStoreOrderItem> getPaymentCashStoreOrderItem(@Query("storeUserID") long storeUserID, @Query("orderItemIDs") String orderItemIDs, @Query("orderItemCount") int orderItemCount,
                                                                 @Query("cashPayPrice") int cashPayPrice, @Query("cardPayPrice") int cardPayPrice, @Query("usePrePaid") int usePrePaid, @Query("useMileage") int useMileage,
                                                                 @Query("IsReceipt") String IsReceipt, @Header("accesstoken") String authorization);

    @POST("/Payment/PaymentStoreOrderItem")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

        //가맹점 주문 품목 결제(카드)
    Call<ItemPaymentStoreOrderItem> getPaymentStoreOrderItem(@Query("storeUserID") long storeUserID, @Query("orderItemIDs") String orderItemIDs, @Query("orderItemCount") int orderItemCount,
                                             @Query("cashPayPrice") int cashPayPrice, @Query("cardPayPrice") int cardPayPrice, @Query("usePrePaid") int usePrePaid, @Query("useMileage") int useMileage,
                                             @Query("TID") String TID, @Query("CATInfo") String CATInfo, @Query("cardName") String cardName,
                                             @Query("cardNo") String cardNo, @Query("cardDivide") int cardDivide, @Header("accesstoken") String authorization);

    @POST("/Payment/PaymentStoreOrderItem")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
        //가맹점 주문 품목 결제(현금 + 카드)
    Call<ItemPaymentStoreOrderItem> getPaymentOrderItem(@Query("storeUserID") long storeUserID, @Query("orderItemIDs") String orderItemIDs, @Query("orderItemCount") int orderItemCount,
                                        @Query("cashPayPrice") int cashPayPrice, @Query("cardPayPrice") int cardPayPrice, @Query("usePrePaid") int usePrePaid, @Query("useMileage") int useMileage,
                                        @Query("IsReceipt") String IsReceipt, @Query("receiptID") String receiptID, @Query("TID") String TID, @Query("CATInfo") String CATInfo, @Query("cardName") String cardName,
                                        @Query("cardNo") String cardNo, @Query("cardDivide") int cardDivide, @Header("accesstoken") String authorization);

    @POST("/Payment/GetPaymentInfoList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

        //결제 상세 정보 리스트 조회
    Call<ManagerGetPaymentInfoList> getPaymentInfoList(@Query("orderItemID") long orderItemID, @Header("accesstoken") String authorization);

    @POST("/Payment/GetPaymentInfo")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

        //결제 상세 정보 리스트 조회
    Call<GetPaymentInfoModel> GetPaymentInfo(@Query("paymentID") long paymentID, @Header("accesstoken") String authorization);


    @POST("Order/CancelOrderItem")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

        //품목 취소
    Call<ApiResult> getCancelOrderItem(@Query("orderItemIDs") String orderItemIDs, @Query("orderItemCount") long orderItemCount, @Header("accesstoken") String authorization);


    @POST("/Order/OutOrderItem")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

        //품목 출고
    Call<Void> getOutOrderItem(@Query("orderItemIDs") String orderItemIDs, @Query("orderItemCount") long orderItemCount, @Header("accesstoken") String authorization);

    @POST("/Payment/CancelStorePayment")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

        //결제 취소
    Call<ApiResult> getCancelStorePayment(@Query("paymentID") long paymentID, @Header("accesstoken") String authorization);

    @POST("/Payment/RegCashReceiptID")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })

        //결제 취소
    Call<ApiResult> getRegCashReceiptID(@Query("paymentID") long paymentID, @Query("receiptID") String receiptID, @Query("receiptPrice") int receiptPrice, @Header("accesstoken") String authorization);


}
