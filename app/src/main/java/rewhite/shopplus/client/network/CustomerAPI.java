package rewhite.shopplus.client.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rewhite.shopplus.client.constant.NetConstant;
import rewhite.shopplus.client.manager.ManagerCutomerModel;
import rewhite.shopplus.client.manager.ManagerFaqCateforyList;
import rewhite.shopplus.client.manager.ManagerFaqList;

/**
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
public interface CustomerAPI {

    public static final HttpLoggingInterceptor LOGGING = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);

    public static final OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .connectTimeout(NetConstant.CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(NetConstant.WRITE_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(NetConstant.READ_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor(LOGGING)
            .build();

    public static final Retrofit AUTH_SERVER = new Retrofit.Builder()
            //프로토콜 TEST / LIVE CHECK
            .baseUrl(ServerApis.TEST_PROTOCOL + ServerApis.TESTBED_AUTHURL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(OK_HTTP_CLIENT)
            .build();

    //공지사항 리스트
    @POST("/Customer/NoticeList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ManagerCutomerModel> getNoticeList(@Query("page") int page, @Query("block") int block, @Header("accesstoken") String authorization);


    //FAQ 카테고리 리스트
    @POST("/Customer/FaqCategoryList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ManagerFaqCateforyList> getFaqCategoryList(@Query("deviceInformation") String deviceInformation);

    //FAQ 리스트
    @POST("/Customer/FaqList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ManagerFaqList> getFaqList(@Query("faqCategoryID") int faqCategoryID, @Query("page") int page, @Query("block") int block, @Query("deviceInformation") String deviceInformation);


    //FAQ 리스트
    @POST("/Customer/FaqList")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<ManagerFaqList> getFaqList(@Query("faqCategoryID") int faqCategoryID, @Query("searchText") String searchText, @Query("page") int page, @Query("block") int block, @Query("deviceInformation") String deviceInformation);

    //상담신청
    @POST("/Customer/RequsetCounsel")
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
    })
    Call<Void> getRequsetCounsel(@Query("storeID") long storeID, @Query("ReqStoreName") String ReqStoreName, @Query("ReqPhone") String ReqPhone, @Query("ReqContext") String ReqContext, @Query("deviceInformation") String deviceInformation);
}
