package rewhite.shopplus.client.network;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import rewhite.shopplus.client.manager.ManagerGetMileageHistory;
import rewhite.shopplus.client.manager.ManagerGetPrePaidChargeHistory;
import rewhite.shopplus.client.manager.ManagerGetPrePaidUseHistory;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.manager.ManagerStoreUserList;
import rewhite.shopplus.client.model.ApiResult;
import rewhite.shopplus.client.model.StoreUserInfoItemList;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;

public class StoreUserApiManager extends NetworkRequest {

    private static final String TAG = "StoreUserApiManager";
    private static WaitCounter mWait = null;

    /**
     * 가맹점 회원 정보
     *
     * @param activity view
     * @return
     */
    public static void getStoreUserInfo(AppCompatActivity activity, long storeUserID) {
        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    StoreUserAPI userAPI = StoreUserAPI.AUTH_SERVER.create(StoreUserAPI.class);
                    Call<StoreUserInfoItemList> call = userAPI.getUserInfo(storeUserID, SettingLogic.getPreAccessToken(activity));

                    Response<StoreUserInfoItemList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- Response Success ---------------------->");
                                ArrayList<StoreUserInfoItemList> arrayList = new ArrayList<>();
                                arrayList.add(response.body());
                                ManagerStoreUserInfo.getmInstance().setStoreUserInfo(arrayList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
    }

    /**
     * 가맹점 회원 리스트
     *
     * @param activity    View
     * @param searchPhone userPhonNumber
     * @return
     */
    public static ManagerStoreUserList getStoreUserList(AppCompatActivity activity, String searchName, final String searchPhone) {
        final ManagerStoreUserList[] storeUserList = {new ManagerStoreUserList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    StoreUserAPI userAPI = StoreUserAPI.AUTH_SERVER.create(StoreUserAPI.class);
                    Call<ManagerStoreUserList> call = userAPI.getStoreUserList(searchName, searchPhone, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerStoreUserList> response = call.execute();

                    int responseCode = response.code();
                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }

                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                ManagerStoreUserList.getmInstance().setStoreUserInfo(response.body().storeUserInfoItemLists);
                                storeUserList[0] = response.body();
                            }
                            break;
                        default:
                            Log.d(TAG, "-------- ManagerStoreUserList Request Error --------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return storeUserList[0];
    }

    /**
     * 가맹점 회원정보 등록
     *
     * @param activity
     * @param storeUserGrade 가맹점회원등급(1,2,3)
     * @param userName       회원명
     * @param address        주소
     * @param userPhone      휴대폰(010-xxxx-xxxx)
     * @param gender         성별(M:남, F:여, N:입력안함)
     * @param birthDay       생일(YYYY-MM-DD)
     * @param anniversary    기념일(MM-DD)
     * @param isNoticeAgree  정보성 알림 수신동의 여부(Y/N)
     * @param isPromoteArgee 홍보성 알림 수신동의 여부(Y/N)
     */
    public static String getStoreUserRegister(AppCompatActivity activity, String storeUserGrade, String userName, String address, String userPhone, String gender, String birthDay, String anniversary, String isNoticeAgree, String isPromoteArgee) {

        final StoreUserAPI storeUserAPI = StoreUserAPI.AUTH_SERVER.create(StoreUserAPI.class);
        ApiResult apiResult = new ApiResult();

        final String[] ResultCode = new String[1];

        String finalUserName = userName;
        String finalUserPhone = userPhone;
        String finalAddress = address;
        String finalBirthDay = birthDay;
        String finalAnniversary = anniversary;

        Thread workingThread = new Thread() {
            public void run() {

                Call<ApiResult> call = storeUserAPI.getStoreUserRegister(storeUserGrade, finalUserName, finalAddress, finalUserPhone, gender, finalBirthDay, finalAnniversary, isNoticeAgree, isPromoteArgee, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {
                        case 200:
                            ResultCode[0] = response.body().getResultCode();
                            break;

                        case 401:
                        case 500:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody().toString());
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.body());
                            ResultCode[0] = "F0001";
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }

    /**
     * 가맹점 회원정보 수정
     *
     * @param activity
     * @param storeUserID    가맹점회원ID
     * @param storeUserGrade 가맹점회원등급(1,2,3)
     * @param userName       회원명
     * @param address        주소
     * @param userPhone      회원휴대폰(010-xxxx-xxxx)
     * @param gender         성별(M:남, F:여, N:입력안함)
     * @param birthDay       생일(YYYY-MM-DD)
     * @param anniversary    기념일(MM-DD)
     * @param isNoticeAgree  정보성 알림 수신동의여부
     * @param isPromoteArgee 홍보성 알림 수신동의여부
     */
    public static String getStoreUserUpdate(AppCompatActivity activity, long storeUserID, String storeUserGrade, String userName, String address,
                                            String userPhone, String gender, String memo, String birthDay, String anniversary, String isNoticeAgree, String isPromoteArgee) {
        final StoreUserAPI storeUserAPI = StoreUserAPI.AUTH_SERVER.create(StoreUserAPI.class);
        final String[] ResultCode = new String[1];

        Thread workingThread = new Thread() {
            public void run() {
                Call<ApiResult> call = storeUserAPI.getStoreUserUpdate(storeUserID, storeUserGrade, userName, address, userPhone, gender, memo, birthDay, anniversary, isNoticeAgree, isPromoteArgee, SettingLogic.getPreAccessToken(activity));

                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {
                        case 200:
                            ResultCode[0] = response.body().getResultCode();

                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }

    /**
     * 가맹점 회원 마일리지 내역조회
     *
     * @param activity view
     * @return
     */
    public static ManagerGetMileageHistory getMileageHistory(AppCompatActivity activity, long storeUserID) {
        final ManagerGetMileageHistory[] managerGetMileageHistory = {new ManagerGetMileageHistory()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    StoreUserAPI userAPI = StoreUserAPI.AUTH_SERVER.create(StoreUserAPI.class);
                    Call<ManagerGetMileageHistory> call = userAPI.getMileageHistory(storeUserID, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerGetMileageHistory> response = call.execute();

                    int responseCode = response.code();
                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }

                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                ManagerGetMileageHistory.getmInstance().setmileageHistory(response.body().mMileageHistory);
                            }
                            break;
                        default:
                            Log.d(TAG, "-------- ManagerStoreUserList Request Error --------> " + response.errorBody());
                    }


                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerGetMileageHistory[0];
    }

    /**
     * 가맹점 회원 선충전금 충전내역
     *
     * @param activity view
     * @return
     */
    public static ManagerGetPrePaidChargeHistory getPrePaidChargeHistor(AppCompatActivity activity, long storeUserID) {
        final ManagerGetPrePaidChargeHistory[] managerGetMileageHistory = {new ManagerGetPrePaidChargeHistory()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    StoreUserAPI userAPI = StoreUserAPI.AUTH_SERVER.create(StoreUserAPI.class);
                    Call<ManagerGetPrePaidChargeHistory> call = userAPI.getPrePaidChargeHistory(storeUserID, SettingLogic.getPreAccessToken(activity));
                    Response<ManagerGetPrePaidChargeHistory> response = call.execute();

                    int responseCode = response.code();
                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }

                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                ManagerGetPrePaidChargeHistory.getmInstance().setManagerGetPrePaidUseHistory(response.body().getPrePaidUseHistory);
                            }
                            break;
                        default:
                            Log.d(TAG, "-------- ManagerStoreUserList Request Error --------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerGetMileageHistory[0];
    }

    /**
     * 가맹점 회원 선충전금 사용내역
     *
     * @param activity view
     * @return
     */
    public static ManagerGetPrePaidUseHistory getPrePaidUseHistory(AppCompatActivity activity, long storeUserID) {
        final ManagerGetPrePaidUseHistory[] managerGetMileageHistory = {new ManagerGetPrePaidUseHistory()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    StoreUserAPI userAPI = StoreUserAPI.AUTH_SERVER.create(StoreUserAPI.class);
                    Call<ManagerGetPrePaidUseHistory> call = userAPI.getStoreUserList(storeUserID, SettingLogic.getPreAccessToken(activity));
                    Response<ManagerGetPrePaidUseHistory> response = call.execute();

                    int responseCode = response.code();
                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }

                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                ManagerGetPrePaidUseHistory.getmInstance().setGetPrePaidUseHistory(response.body().getPrePaidUseHistory);
                            }
                            break;
                        default:
                            Log.d(TAG, "-------- ManagerStoreUserList Request Error --------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerGetMileageHistory[0];
    }
}