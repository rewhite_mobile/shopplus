package rewhite.shopplus.client.network;

import android.support.v7.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Response;
import rewhite.shopplus.client.manager.ManagerCardPaymentManageList;
import rewhite.shopplus.client.manager.ManagerNonPayOrderItemManageList;
import rewhite.shopplus.client.manager.ManagerNonPayUserManageList;
import rewhite.shopplus.client.manager.ManagerNotOutOrderItemManageList;
import rewhite.shopplus.client.manager.ManagerNotOutUserManageList;
import rewhite.shopplus.client.manager.ManagerPrepaidManageList;
import rewhite.shopplus.client.manager.ManagerUserManageList;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;

/**
 * 운영관리 API Manager Class
 */
public class OperationsManagement extends NetworkRequest {
    private static final String TAG = "OperationsManagement";

    /**
     * 회원 조회
     *
     * @param activity
     * @param userGrade 회원등급
     * @param startDate 조회 시작일(주문일)
     * @param endDate   조회 종료일(주문일)
     * @return
     */
    public static ManagerUserManageList getManagerUserManageList(AppCompatActivity activity, String userGrade, String startDate, String endDate) {
        final ManagerUserManageList[] managerUserManageList = {new ManagerUserManageList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {

                    OperationsManagementAPI operationsManagementAPI = OperationsManagementAPI.AUTH_SERVER.create(OperationsManagementAPI.class);
                    Call<ManagerUserManageList> call = operationsManagementAPI.getManagerUserManageList(userGrade, startDate, endDate, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerUserManageList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- ManagerGetPaymentInfoList Response Success ---------------------->");
                                ManagerUserManageList.getmInstance().setUserManageList(response.body().userManageList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerUserManageList[0];
    }


    /**
     * 미수 회원 조회
     *
     * @param activity
     * @param searchPhone 휴대폰 뒷자리
     * @param userName    회원명
     * @return
     */
    public static ManagerNonPayUserManageList getManagerUserManageList(AppCompatActivity activity, String searchPhone, String userName) {
        final ManagerNonPayUserManageList[] managerNonPayUserManageList = {new ManagerNonPayUserManageList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {

                    Logger.d(TAG, "searchPhone : " + searchPhone);
                    Logger.d(TAG, "userName : " + userName);
                    OperationsManagementAPI operationsManagementAPI = OperationsManagementAPI.AUTH_SERVER.create(OperationsManagementAPI.class);
                    Call<ManagerNonPayUserManageList> call = operationsManagementAPI.getNonPayUserManageList(searchPhone, userName, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerNonPayUserManageList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- ManagerGetPaymentInfoList Response Success ---------------------->");
                                ManagerNonPayUserManageList.getmInstance().setUserManageList(response.body().nonPayUserManageList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerNonPayUserManageList[0];
    }

    /**
     * 미수 품목 조회
     *
     * @param activity
     * @param searchType 검색타입(NOTOUT: 미출고, OUTFIN: 출고)
     * @return
     */
    public static ManagerNonPayOrderItemManageList getNonPayOrderItemManageList(AppCompatActivity activity, String searchType) {
        final ManagerNonPayOrderItemManageList[] managerNonPayUserManageList = {new ManagerNonPayOrderItemManageList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    Logger.d(TAG, "searchType : " + searchType);
                    OperationsManagementAPI operationsManagementAPI = OperationsManagementAPI.AUTH_SERVER.create(OperationsManagementAPI.class);
                    Call<ManagerNonPayOrderItemManageList> call = operationsManagementAPI.getNonPayOrderItemManageList(searchType, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerNonPayOrderItemManageList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- ManagerGetPaymentInfoList Response Success ---------------------->");
                                ManagerNonPayOrderItemManageList.getmInstance().setUserManageList(response.body().NonPayOrderItemManageList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerNonPayUserManageList[0];
    }

    /**
     * 미출고 회원 조회
     *
     * @param activity
     * @param searchPhone 휴대폰 뒷자리
     * @param userName    회원명
     * @return
     */
    public static ManagerNotOutUserManageList getNotOutUserManageList(AppCompatActivity activity, String searchPhone, String userName) {
        final ManagerNotOutUserManageList[] notOutUserManageList = {new ManagerNotOutUserManageList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    OperationsManagementAPI operationsManagementAPI = OperationsManagementAPI.AUTH_SERVER.create(OperationsManagementAPI.class);
                    Call<ManagerNotOutUserManageList> call = operationsManagementAPI.getNotOutUserManageList(searchPhone, userName, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerNotOutUserManageList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- ManagerGetPaymentInfoList Response Success ---------------------->");
                                ManagerNotOutUserManageList.getmInstance().setUserManageList(response.body().userManageList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return notOutUserManageList[0];
    }

    /**
     * 미출고 품목 조회
     *
     * @param activity
     * @param stayDay    체류일(n일 이상)
     * @param startDate  조회 시작일(주문일)
     * @param endDate    조회 종료일(주문일)
     * @param statusType 상태(ENTER:접수완료, WASHFIN:세탁완료)
     * @return
     */
    public static ManagerNotOutOrderItemManageList getNotOutOrderItemManageList(AppCompatActivity activity, int stayDay, String startDate, String endDate, String statusType) {
        final ManagerNotOutOrderItemManageList[] notOutUserManageList = {new ManagerNotOutOrderItemManageList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    OperationsManagementAPI operationsManagementAPI = OperationsManagementAPI.AUTH_SERVER.create(OperationsManagementAPI.class);
                    Call<ManagerNotOutOrderItemManageList> call = operationsManagementAPI.getNotOutOrderItemManageList(stayDay, startDate, endDate, statusType, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerNotOutOrderItemManageList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- ManagerGetPaymentInfoList Response Success ---------------------->");
                                ManagerNotOutOrderItemManageList.getmInstance().setNotOutOrderItemManageList(response.body().NotOutOrderItemManageList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return notOutUserManageList[0];
    }

    /**
     * 카드 결제 조회
     *
     * @param activity
     * @param searchPhone 휴대폰 뒷자리
     * @param userName    회원명
     * @param startDate   조회 시작일(결제일)
     * @param endDatem    조회 종료일(결제일)
     * @param status      상태(10: 정상승인, 90: 승인취소)
     * @return
     */
    public static ManagerCardPaymentManageList getCardPaymentManageList(AppCompatActivity activity, String searchPhone, String userName, String startDate, String endDatem, int status) {
        final ManagerCardPaymentManageList[] CardPaymentManageList = {new ManagerCardPaymentManageList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    OperationsManagementAPI operationsManagementAPI = OperationsManagementAPI.AUTH_SERVER.create(OperationsManagementAPI.class);
                    Call<ManagerCardPaymentManageList> call = operationsManagementAPI.getCardPaymentManageList(searchPhone, userName, startDate, endDatem, status, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerCardPaymentManageList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- ManagerGetPaymentInfoList Response Success ---------------------->");
                                ManagerCardPaymentManageList.getmInstance().setUserManageList(response.body().NonPayOrderItemManageList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return CardPaymentManageList[0];
    }


    /**
     * 선충전금조회
     *
     * @param activity
     * @param searchPhone 휴대폰 뒷자리
     * @param userName    회원명
     * @param startDate   조회 시작일(결제일)
     * @param endDate     조회 종료일(결제일)
     * @param status      상태(10: 정상승인, 90: 승인취소)
     * @return
     */
    public static ManagerPrepaidManageList getManagerPrepaidManageList(AppCompatActivity activity, String searchPhone, String userName, String startDate, String endDate, int status) {
        final ManagerPrepaidManageList[] managerPrepaidManageList = {new ManagerPrepaidManageList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    OperationsManagementAPI operationsManagementAPI = OperationsManagementAPI.AUTH_SERVER.create(OperationsManagementAPI.class);
                    Call<ManagerPrepaidManageList> call = operationsManagementAPI.gePrepaidManageList(searchPhone, userName, startDate, endDate, status, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerPrepaidManageList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- ManagerGetPaymentInfoList Response Success ---------------------->");
                                ManagerPrepaidManageList.getmInstance().setNotOutOrderItemManageList(response.body().NotPrepaidManageList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerPrepaidManageList[0];
    }
}