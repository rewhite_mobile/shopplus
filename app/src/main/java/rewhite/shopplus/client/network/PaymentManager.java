package rewhite.shopplus.client.network;

import android.support.v7.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;
import rewhite.shopplus.client.manager.ManagerGetPaymentInfo;
import rewhite.shopplus.client.manager.ManagerGetPaymentInfoList;
import rewhite.shopplus.client.manager.ManagerItemPaymentStoreOrder;
import rewhite.shopplus.client.manager.ManagerStorePaymentCancel;
import rewhite.shopplus.client.model.ApiResult;
import rewhite.shopplus.client.model.GetPaymentInfoModel;
import rewhite.shopplus.data.dto.ItemPaymentStoreOrderItem;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;

public class PaymentManager extends NetworkRequest {
    private static final String TAG = "PaymentManager";

    private static String apiStatus = null;


    /**
     * 결제 상세 정보 리스트 조회
     *
     * @param activity    activityView
     * @param orderItemID 주문품목ID
     * @param page        페이지 번호
     * @param block       페이지 크기
     * @return
     */
    public static ManagerGetPaymentInfoList getPaymentInfoList(AppCompatActivity activity, long orderItemID, int page, int block) {
        final ManagerGetPaymentInfoList[] getPaymentInfoLists = {new ManagerGetPaymentInfoList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);
                    Call<ManagerGetPaymentInfoList> call = paymentAPI.getPaymentInfoList(orderItemID, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerGetPaymentInfoList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- ManagerGetPaymentInfoList Response Success ---------------------->");
                                ManagerGetPaymentInfoList.getmInstance().setGetOrderList(response.body().canGetOrderList);
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return getPaymentInfoLists[0];
    }

    /**
     * 결제 상세 정보 조회
     *
     * @param activity  activityView
     * @param paymentID 결제ID
     * @return
     */
    public static void getPaymentInfo(AppCompatActivity activity, long paymentID) {
        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);
                    Call<GetPaymentInfoModel> call = paymentAPI.GetPaymentInfo(paymentID, SettingLogic.getPreAccessToken(activity));

                    Response<GetPaymentInfoModel> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- ManagerGetPaymentInfoList Response Success ---------------------->");
                                ArrayList<GetPaymentInfoModel> arrayList = new ArrayList<>();
                                arrayList.add(response.body());
                                ManagerGetPaymentInfo.getmInstance().setPaymentInfo(arrayList);

                                Logger.d(TAG, "1111111 :" + ManagerGetPaymentInfo.getmInstance().getPaymentInfo().get(0).getData().getPaymentDate());
                                Logger.d(TAG, "2222222 :" + ManagerGetPaymentInfo.getmInstance().getPaymentInfo().get(0).getData().getPaymentDivideDesc());
                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
    }


    /**
     * 가맹점 선충전금 충전(현금) REQUEST
     *
     * @param storeUserID  가맹점 회원ID
     * @param paymentType  결제타입(1:현금)
     * @param paymentPrice 결제금액
     */
    public static String getManagerChargeStoreUserPrePaid(AppCompatActivity activity, long storeUserID, int paymentType, int paymentPrice) {
        final PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);
        final String[] ResultCode = new String[1];

        Thread workingThread = new Thread() {
            public void run() {

                Call<ApiResult> call = paymentAPI.getManagerChargeStoreUserPrePaid(storeUserID, paymentType, paymentPrice, SettingLogic.getPreAccessToken(activity));
                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            ResultCode[0] = response.body().getResultCode();

                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }

    /**
     * 가맹점 선충전금 충전 REQUEST
     *
     * @param storeUserID  가맹점 회원ID
     * @param paymentType  결제타입(2:카드)
     * @param paymentPrice 결제금액
     * @param TID          카드결제시, 승인번호
     * @param CATInfo      카드결제시,단말기정보
     * @param cardName     카드결제시,카드명
     * @param cardNo       카드결제시,카드번호
     * @param cardDivide   카드결제시, 할부개월수(0:일시불)
     */
    public static String getManagerCardChargeStoreUserPrePaid(AppCompatActivity activity, long storeUserID, int paymentType, int paymentPrice, String TID, String CATInfo, String cardName, String cardNo, int cardDivide) {
        final PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);
        final String[] ResultCode = new String[1];

        Thread workingThread = new Thread() {
            public void run() {

                Call<ApiResult> call = paymentAPI.getManagerCardChargeStoreUserPrePaid(storeUserID, paymentType, paymentPrice, TID, CATInfo, cardName, cardNo, cardDivide, SettingLogic.getPreAccessToken(activity));
                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            ResultCode[0] = response.body().getResultCode();
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }

    /**
     * 품목취소
     *
     * @param orderItemIDs   주문품목ID(구분자'|')
     * @param orderItemCount 주문품목 개수
     */
    public static String getCancelOrderItem(AppCompatActivity activity, String orderItemIDs, int orderItemCount) {
        final PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);
        ArrayList<ApiResult> apiResult = new ArrayList<>();
        final String[] ResultCode = new String[1];

        Thread workingThread = new Thread() {
            public void run() {

                Call<ApiResult> call = paymentAPI.getCancelOrderItem(orderItemIDs, orderItemCount, SettingLogic.getPreAccessToken(activity));
                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            ResultCode[0] = response.body().getResultCode();
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }

    /**
     * 결제취소
     *
     * @param paymentID 결제ID
     */
    public static String getCancelStorePayment(AppCompatActivity activity, long paymentID) {
        final PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);
        ArrayList<ApiResult> apiResult = new ArrayList<>();
        final String[] ResultCode = new String[1];

        Thread workingThread = new Thread() {
            public void run() {

                Call<ApiResult> call = paymentAPI.getCancelStorePayment(paymentID, SettingLogic.getPreAccessToken(activity));
                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            ResultCode[0] = response.body().getResultCode();
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }

    /**
     * 품목 출고
     *
     * @param orderItemIDs   주문품목ID(구분자'|')
     * @param orderItemCount 주문품목 개수
     */
    public static void getOutOrderItem(AppCompatActivity activity, String orderItemIDs, int orderItemCount) {
        final PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);

        Thread workingThread = new Thread() {
            public void run() {

                Call<Void> call = paymentAPI.getOutOrderItem(orderItemIDs, orderItemCount, SettingLogic.getPreAccessToken(activity));
                try {
                    Response<Void> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {

                        case 200:
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 취소가능한 결제내역
     *
     * @param activity
     * @param storeUserID
     * @return
     */
    public static ManagerStorePaymentCancel getPaymentCancelAvailList(AppCompatActivity activity, long storeUserID) {
        final ManagerStorePaymentCancel[] managerStorePaymentCancel = {new ManagerStorePaymentCancel()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);
                    Call<ManagerStorePaymentCancel> call = paymentAPI.getStorePaymentCancelAvailList(storeUserID, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerStorePaymentCancel> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "<----------------------Response is null!!! ---------------------->");
                        return;
                    }
                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Logger.d(TAG, "<---------------------- getPaymentCancelAvailList Response Success ---------------------->");
                                ManagerStorePaymentCancel.getmInstance().setPaymentCancelAvailList(response.body().storePaymentCancelAvailList);

                                Logger.d(TAG, "ManagerStorePaymentCancel : " + ManagerStorePaymentCancel.getmInstance().getPaymentCancelAvailList().size());

                            }
                            break;
                        default:
                            Logger.d(TAG, "<----------------------Response Error!!!----------------------> " + response.errorBody());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerStorePaymentCancel[0];
    }

    /**
     * 가맹점 주문 품목 결제 (현금)
     *
     * @param orderItemIDs   주문 품목 ID (구분자'|')
     * @param orderItemCount 주문 품목 개수
     * @param cashPayPrice   현금 결제금액
     * @param usePrePaid     사용 선충전금
     * @param useMileage     사용 마일리지
     * @param IsReceipt      현금결제시, 현금영수증발행여부(Y/N)
     */
    public static String getCashPaymentStoreOrderItem(AppCompatActivity activity, String orderItemIDs, int orderItemCount, int cashPayPrice, int cardPayPrice, int usePrePaid, int useMileage, String IsReceipt) {

        final PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);
        ArrayList<ItemPaymentStoreOrderItem> apiResult = new ArrayList<>();

        final String[] ResultCode = new String[1];
        Thread workingThread = new Thread() {
            public void run() {

                Call<ItemPaymentStoreOrderItem> call = paymentAPI.getPaymentCashStoreOrderItem(Long.valueOf(SettingLogic.getStoreUserId(activity)), orderItemIDs, orderItemCount, cashPayPrice, cardPayPrice, usePrePaid, useMileage, IsReceipt, SettingLogic.getPreAccessToken(activity));
                try {
                    Response<ItemPaymentStoreOrderItem> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {
                        case 200:
                            ResultCode[0] = response.body().getResultCode();

                            apiResult.add(response.body());
                            ManagerItemPaymentStoreOrder.getmInstance().setItemAdditionalData(apiResult);

                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            apiStatus = "0";
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            apiStatus = "1";
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            apiStatus = "2";
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }

    /**
     * 가맹점 주문 품목 결제(카드)
     *
     * @param activity
     * @param orderItemIDs   주문 품목 ID (구분자'|')
     * @param orderItemCount 주문 품목 개수
     * @param cashPayPrice   현금 결제금액
     * @param cardPayPrice   카드 결제금액
     * @param usePrePaid     사용 선충전금
     * @param useMileage     사용 마일리지
     * @param TID            카드결제시, 승인번호
     * @param CATInfo        카드결제시, 단말기 정보
     * @param cardName       카드결제시, 카드명
     * @param cardNo         카드결제시, 카드번호
     * @param cardDivide     카드결제시, 할부 개월수(0:일시불)
     * @return
     */
    public static String getCardPaymentStoreOrderItem(AppCompatActivity activity, String orderItemIDs, int orderItemCount, int cashPayPrice, int cardPayPrice, int usePrePaid, int useMileage, String TID, String CATInfo, String cardName, String cardNo, int cardDivide) {
        final PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);
        ArrayList<ItemPaymentStoreOrderItem> apiResult = new ArrayList<>();

        final String[] ResultCode = new String[1];
        Thread workingThread = new Thread() {
            public void run() {

                Call<ItemPaymentStoreOrderItem> call = paymentAPI.getPaymentStoreOrderItem(Long.valueOf(SettingLogic.getStoreUserId(activity)), orderItemIDs, orderItemCount, cashPayPrice, cardPayPrice, usePrePaid, useMileage, TID, CATInfo, cardName, cardNo, cardDivide, SettingLogic.getPreAccessToken(activity));
                try {
                    Response<ItemPaymentStoreOrderItem> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {
                        case 200:
                            ResultCode[0] = response.body().getResultCode();

                            apiResult.add(response.body());
                            ManagerItemPaymentStoreOrder.getmInstance().setItemAdditionalData(apiResult);
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            apiStatus = "0";
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            apiStatus = "1";
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            apiStatus = "2";
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }


    /**
     * 현금영수증 발행 번호 등록
     * @param activity
     * @param paymentID    결제ID
     * @param receiptID    현금영수증 발행번호
     * @param receiptPrice 현금영수증 발행금액
     * @return
     */
    public static String getRegCashReceiptID(AppCompatActivity activity, long paymentID, String receiptID, int receiptPrice) {
        final PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);


        final String[] ResultCode = new String[1];
        Thread workingThread = new Thread() {
            public void run() {

                Logger.d(TAG, "paymentID : " + paymentID);
                Logger.d(TAG, "receiptID : " + receiptID);
                Logger.d(TAG, "receiptPrice : " + receiptPrice);
                Call<ApiResult> call = paymentAPI.getRegCashReceiptID(paymentID, receiptID, receiptPrice, SettingLogic.getPreAccessToken(activity));
                try {
                    Response<ApiResult> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {
                        case 200:
                            ResultCode[0] = response.body().getResultCode();

                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            apiStatus = "0";
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            apiStatus = "1";
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            apiStatus = "2";
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }

    /**
     * 가맹점 주문 품목 결제(현금 + 카드)
     *
     * @param activity
     * @param orderItemIDs   주문 품목 ID (구분자'|')
     * @param orderItemCount 주문 품목 개수
     * @param cashPayPrice   현금 결제금액
     * @param IsReceipt      현금영수증등록여부
     * @param receiptID      현금영수증 승인번호
     * @param cardPayPrice   카드 결제금액
     * @param usePrePaid     사용 선충전금
     * @param useMileage     사용 마일리지
     * @param TID            카드결제시, 승인번호
     * @param CATInfo        카드결제시, 단말기 정보
     * @param cardName       카드결제시, 카드명
     * @param cardNo         카드결제시, 카드번호
     * @param cardDivide     카드결제시, 할부 개월수(0:일시불)
     * @return
     */
    public static String getPaymentStoreOrderItem(AppCompatActivity activity, String orderItemIDs, int orderItemCount, int cashPayPrice, int cardPayPrice, int usePrePaid, int useMileage, String IsReceipt, String receiptID, String TID, String CATInfo, String cardName, String cardNo, int cardDivide) {
        final PaymentAPI paymentAPI = PaymentAPI.AUTH_SERVER.create(PaymentAPI.class);
        ArrayList<ItemPaymentStoreOrderItem> apiResult = new ArrayList<>();

        final String[] ResultCode = new String[1];
        Thread workingThread = new Thread() {
            public void run() {
                Call<ItemPaymentStoreOrderItem> call = paymentAPI.getPaymentOrderItem(Long.valueOf(SettingLogic.getStoreUserId(activity)), orderItemIDs, orderItemCount, cashPayPrice, cardPayPrice, usePrePaid, useMileage, IsReceipt, receiptID, TID, CATInfo, cardName, cardNo, cardDivide, SettingLogic.getPreAccessToken(activity));
                try {
                    Response<ItemPaymentStoreOrderItem> response = call.execute();
                    int responseCode = response.code();

                    switch (responseCode) {
                        case 200:
                            ResultCode[0] = response.body().getResultCode();

                            apiResult.add(response.body());
                            ManagerItemPaymentStoreOrder.getmInstance().setItemAdditionalData(apiResult);
                            Logger.d(TAG, "-------- Item Add Response Success --------");
                            apiStatus = "0";
                            break;

                        case 401:
                            Logger.d(TAG, "-------- Item Add  Error --------> " + response.errorBody());
                            apiStatus = "1";
                            break;

                        case 500:
                            Logger.d(TAG, "서버에러");
                            apiStatus = "2";
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        workingThread.start();
        try {
            workingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ResultCode[0];
    }
}
