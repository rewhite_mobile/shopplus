package rewhite.shopplus.client.network;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Response;
import rewhite.shopplus.client.manager.ManagerCancelAvailOrderList;
import rewhite.shopplus.client.manager.ManagerGetSelectOrderItemList;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.manager.ManagerGetVisitOrderNeedpay;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;

import static rewhite.shopplus.client.network.NetworkRequest.execute;

public class VisitReceptionManager {
    private static final String TAG = "VisitReceptionManager";

    /**
     * 방문 주문 접수
     *
     * @param activity  view
     * @param serchType StoreUseID
     * @return
     */
    public static ManagerGetVisitOrder getGetVisitOrder(AppCompatActivity activity, final String serchType, String storeUserID, String page, String block) {
        final ManagerGetVisitOrder[] storeUserInfoItemList = {new ManagerGetVisitOrder()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    VisitReceptionAPI visitReceptionAPI = VisitReceptionAPI.AUTH_SERVER.create(VisitReceptionAPI.class);
                    Call<ManagerGetVisitOrder> call = visitReceptionAPI.getGetVisitOrder(serchType, storeUserID, null,null, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerGetVisitOrder> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }

                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Log.d(TAG, "-------- Response Success --------> " + response.errorBody());
                                storeUserInfoItemList[0] = response.body();
                                ManagerGetVisitOrder.getmInstance().setGetVisitOrder(response.body().itemGetVisitOrder);
                            }
                            break;

                        default:
                            Log.d(TAG, "-------- Response Error --------> " + response.errorBody());
                            break;
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return storeUserInfoItemList[0];
    }


    /**
     * 방문 주문 접수 (OUTFIN*NEEDPAY:출고미수)
     *
     * @param activity  view
     * @return
     */
    public static ManagerGetVisitOrderNeedpay getGetVisitOrderNeedpay(AppCompatActivity activity, String storeUserID, String page, String block) {
        final ManagerGetVisitOrderNeedpay[] storeUserInfoItemList = {new ManagerGetVisitOrderNeedpay()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    VisitReceptionAPI visitReceptionAPI = VisitReceptionAPI.AUTH_SERVER.create(VisitReceptionAPI.class);
                    Call<ManagerGetVisitOrderNeedpay> call = visitReceptionAPI.getGetVisitOrderNeed("OUTFIN*NEEDPAY", storeUserID, "1","200000", SettingLogic.getPreAccessToken(activity));

                    Response<ManagerGetVisitOrderNeedpay> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }

                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Log.d(TAG, "-------- Response Success --------> " + response.errorBody());
                                storeUserInfoItemList[0] = response.body();
                                ManagerGetVisitOrderNeedpay.getmInstance().setGetVisitOrderNeedPay(response.body().itemGetVisitOrderNeedPay);
                            }
                            break;

                        default:
                            Log.d(TAG, "-------- Response Error --------> " + response.errorBody());
                            break;
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return storeUserInfoItemList[0];
    }

    /**
     * 주문 품목 조회(for 주문조회)
     * @param activity
     * @param startDate   조회 시작일(yyyy-MM-dd)
     * @param endDate     조회 종료일(yyyy-MM-dd)
     * @param orderStatus 주문상태(ENTER:접수완료, WASHFIN:세탁완료, OUT:출고완료, CNL:취소)
     * @param userGrade   회원등급(회원구분)
     * @param searchPhone 핸드폰 뒷자리(4자리)
     * @param userName    회원명
     * @param tagNo       택번호
     * @param page        페이지번호
     * @param block       페이지크기
     * @return
     */
    public static ManagerGetVisitOrder getGetVisit(AppCompatActivity activity, final String startDate, final String endDate, final String orderStatus, final String userGrade,
                                                   final String searchPhone, final String userName, final String tagNo, final int page, final int block) {
        final ManagerGetVisitOrder[] managerGetOrderLists = {new ManagerGetVisitOrder()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    VisitReceptionAPI visitReceptionAPI = VisitReceptionAPI.AUTH_SERVER.create(VisitReceptionAPI.class);
                    Call<ManagerGetVisitOrder> call = visitReceptionAPI.getGetOrderList(startDate, endDate, orderStatus, userGrade, searchPhone, userName, tagNo, page, block, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerGetVisitOrder> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }

                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Log.d(TAG, "-------- Response Success --------> " + response.errorBody());
                                ManagerGetVisitOrder.getmInstance().setGetVisitOrder(response.body().itemGetVisitOrder);
                                managerGetOrderLists[0] = response.body();
                            }
                            break;

                        default:
                            Log.d(TAG, "-------- Response Error --------> " + response.errorBody());
                            break;
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerGetOrderLists[0];
    }

    /**
     * 취소 가능한 주문 품목 조회(for 품목취소)
     *
     * @param activity
     * @param storeUserID
     * @return
     */
    public static ManagerCancelAvailOrderList getCancelAvailorder(AppCompatActivity activity, long storeUserID) {
        final ManagerCancelAvailOrderList[] managerCancelAvailOrder = {new ManagerCancelAvailOrderList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    VisitReceptionAPI visitReceptionAPI = VisitReceptionAPI.AUTH_SERVER.create(VisitReceptionAPI.class);
                    Call<ManagerCancelAvailOrderList> call = visitReceptionAPI.getCancelAvailorder(storeUserID, SettingLogic.getPreAccessToken(activity));

                    Response<ManagerCancelAvailOrderList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }

                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Log.d(TAG, "-------- Response Success --------> " + response.errorBody());
                                managerCancelAvailOrder[0] = response.body();
                            }
                            break;

                        default:
                            Log.d(TAG, "-------- Response Error --------> " + response.errorBody());
                            break;
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerCancelAvailOrder[0];
    }

    /**
     * 주문 품목 조회(다수의 주문품목ID)
     *
     * @param activity
     * @param orderItemIDs  주문품목ID (구분자'|')
     * @return
     */
    public static ManagerGetSelectOrderItemList getSelectOrderItemList(AppCompatActivity activity, String orderItemIDs) {
        final ManagerGetSelectOrderItemList[] managerGetSelectOrderItemLists = {new ManagerGetSelectOrderItemList()};

        execute(activity, new Runnable() {
            @Override
            public void run() {
                try {
                    VisitReceptionAPI visitReceptionAPI = VisitReceptionAPI.AUTH_SERVER.create(VisitReceptionAPI.class);
                    Call<ManagerGetSelectOrderItemList> call = visitReceptionAPI.GetSelectOrderItemList(orderItemIDs, "90DA9AD0B7064F10982D1531F4156075");

                    Response<ManagerGetSelectOrderItemList> response = call.execute();

                    int responseCode = response.code();

                    if (response == null) {
                        Logger.d(TAG, "UserInfoRequest ---------------------- response is null!!!");
                        return;
                    }

                    switch (responseCode) {
                        case 200:
                        case 201:
                            if (response != null && response.isSuccessful() && response.body() != null) {
                                Log.d(TAG, "-------- Response Success --------> " + response.errorBody());
                                managerGetSelectOrderItemLists[0] = response.body();
                            }
                            break;

                        default:
                            Log.d(TAG, "-------- Response Error --------> " + response.errorBody());
                            break;
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
        return managerGetSelectOrderItemLists[0];
    }
}
