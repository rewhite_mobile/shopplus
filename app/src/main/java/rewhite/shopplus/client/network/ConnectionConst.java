package rewhite.shopplus.client.network;

public class ConnectionConst {
    public static final String AGENT_PROPERTY_NAME = "http.agent";
    public static final String AGENT_CHECK_NAME = "||" + "app::";
    public static final String AGENT_CHECK_SEPARATOR = "||";

}
