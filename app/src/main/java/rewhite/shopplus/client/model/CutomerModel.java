package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

public class CutomerModel {
    private static final String TAG = "CutomerModel";
    @SerializedName("NoticeID")
    private int NoticeID;// 공지ID
    @SerializedName("NoticeType")
    private int NoticeType;// 공지타입(1:유저, 2:가맹점, 3:샵플러스)
    @SerializedName("NoticeTypeDesc")
    private String NoticeTypeDesc;// 공지타입 명
    @SerializedName("NoticeTitle")
    private String NoticeTitle;// 공지 제목
    @SerializedName("NoticeContent")
    private String NoticeContent;// 공지 내용
    @SerializedName("IsUse")
    private String IsUse;// 사용여부
    @SerializedName("RegDate")
    private String RegDate;// 등록일
    @SerializedName("UpdDate")
    private String UpdDate;// 수정일
    @SerializedName("WebViewUrl")
    private String WebViewUrl;// 웹뷰 URL

    public int getNoticeID() {
        return NoticeID;
    }

    public int getNoticeType() {
        return NoticeType;
    }

    public String getNoticeTypeDesc() {
        return NoticeTypeDesc;
    }

    public String getNoticeTitle() {
        return NoticeTitle;
    }

    public String getNoticeContent() {
        return NoticeContent;
    }

    public String getIsUse() {
        return IsUse;
    }

    public String getRegDate() {
        return RegDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }

    public String getWebViewUrl() {
        return WebViewUrl;
    }
}
