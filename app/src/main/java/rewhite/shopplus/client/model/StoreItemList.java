package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 * 품목등록 세탁요금 / 수선요금 / 추가금액(기술료) / 부속품
 */
public class StoreItemList {
    private static final String TAG = "StoreItemList";

    @SerializedName("StoreItemID")
    private int storeItemID;    //상품ID
    @SerializedName("StoreID")
    private int storeID;        //가맹점ID
    @SerializedName("ClassID")
    private int classID;        //클래스ID
    @SerializedName("ItemName")
    private String itemName;    //상품명
    @SerializedName("ItemOrder")
    private int itemOrder;      //상품순번
    @SerializedName("ItemVisitPrice")
    private int itemVisitPrice;      //방문품목가격(일반)
    @SerializedName("ItemVisitPrice2")
    private int itemVisitPrice_a;    //방문품목가격(명품)
    @SerializedName("ItemVisitPrice3")
    private int itemVisitPrice_b;    //방문품목가격(아동)
    @SerializedName("ItemPickupPrice")
    private int itemPickupPrice;      //수거품목가격(일반)
    @SerializedName("ItemPickupPrice2")
    private int itemPickupPrice_a;    //수거품목가격(명품)
    @SerializedName("ItemPickupPrice3")
    private int itemPickupPrice_b;    //수거품목가격(아동)
    @SerializedName("IsUse")
    private String isUse;             //사용여부
    @SerializedName("RegDate")
    private String regDate;           //등록일
    @SerializedName("UpdDate")
    private String updDate;           //수정일

    private boolean isChecked;


    public int getStoreItemID() {
        return storeItemID;
    }

    public int getStoreID() {
        return storeID;
    }

    public int getClassID() {
        return classID;
    }

    public String getItemName() {
        return itemName;
    }

    public int getItemOrder() {
        return itemOrder;
    }

    public int getItemVisitPrice() {
        return itemVisitPrice;
    }

    public int getItemVisitPrice_a() {
        return itemVisitPrice_a;
    }

    public int getItemVisitPrice_b() {
        return itemVisitPrice_b;
    }

    public int getItemPickupPrice() {
        return itemPickupPrice;
    }

    public int getItemPickupPrice_a() {
        return itemPickupPrice_a;
    }

    public int getItemPickupPrice_b() {
        return itemPickupPrice_b;
    }

    public String getIsUse() {
        return isUse;
    }

    public String getRegDate() {
        return regDate;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isChecked() {
        return isChecked;
    }
}
