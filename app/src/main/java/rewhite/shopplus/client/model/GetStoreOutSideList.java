package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 * 외주업체 리스트
 */
public class GetStoreOutSideList {

    @SerializedName("StoreOutSideID")
    private int StoreOutSideID;   //가맹점외주업체ID
    @SerializedName("StoreID")
    private int StoreID;          //가맹점ID
    @SerializedName("OutSideName")
    private String OutSideName;   //외주명
    @SerializedName("ProcessDay")
    private int ProcessDay;       //처리기간
    @SerializedName("ItemGroupID")
    private int ItemGroupID;      //취급 품목그룹ID
    @SerializedName("ItemGroupName")
    private String ItemGroupName; //취급 품목그룹명
    @SerializedName("IsUse")
    private String IsUse;         //사용여부(Y/N)
    @SerializedName("RegDate")
    private String RegDate;       //등록일
    @SerializedName("UpdDate")
    private String UpdDate;       //수정일


    public int getStoreOutSideID() {
        return StoreOutSideID;
    }

    public int getStoreID() {
        return StoreID;
    }

    public String getOutSideName() {
        return OutSideName;
    }

    public int getProcessDay() {
        return ProcessDay;
    }

    public int getItemGroupID() {
        return ItemGroupID;
    }

    public String getItemGroupName() {
        return ItemGroupName;
    }

    public String getIsUse() {
        return IsUse;
    }

    public String getRegDate() {
        return RegDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }
}
