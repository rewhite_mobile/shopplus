package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 * 가입신청 지역(시/도)조회 model
 */
public class AreaDistrictListModel {

    @SerializedName("AreaID")
    private int AreaID;//  지역ID
    @SerializedName("AreaType")
    private int AreaType;//지역타입 (1:시/도, 2:시/구/군, 3:구)
    @SerializedName("SiDo")
    private String SiDo;// 시/도
    @SerializedName("SiGuGun")
    private String SiGuGun;// 시/구/군
    @SerializedName("Gu")
    private String Gu;// 구
    @SerializedName("ParentAreaID")
    private int ParentAreaID;// 부모 지역ID


    public int getAreaID() {
        return AreaID;
    }

    public int getAreaType() {
        return AreaType;
    }

    public String getSiDo() {
        return SiDo;
    }

    public String getSiGuGun() {
        return SiGuGun;
    }

    public String getGu() {
        return Gu;
    }

    public int getParentAreaID() {
        return ParentAreaID;
    }
}
