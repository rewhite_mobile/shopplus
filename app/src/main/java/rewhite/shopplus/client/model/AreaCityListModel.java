package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 * 가입신청 지역(시/도)조회 model
 */
public class AreaCityListModel {

    @SerializedName("AreaID")
    private int AreaID;//지역ID
    @SerializedName("SiDo")
    private String SiDo;// 시/도

    public int getAreaID() {
        return AreaID;
    }

    public String getSiDo() {
        return SiDo;
    }
}
