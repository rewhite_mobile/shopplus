package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

public class GetStoreDeviceInfo {
    private static final String TAG = "GetStoreDeviceInfo";

    @SerializedName("Data")
    GetStoreDeviceInfo.Data Data;

    public GetStoreDeviceInfo.Data getData() {
        return Data;
    }

    public static class Data {
        @SerializedName("StoreID")
        private String StoreID; //가맹점ID
        @SerializedName("PrintCount")
        private int PrintCount; //인쇄 매수
        @SerializedName("IsPrintBalance")
        private String IsPrintBalance; //회원 잔액 출력 여부
        @SerializedName("IsPrintDamageNotice")
        private String IsPrintDamageNotice; //손해배상기준 출력 여부
        @SerializedName("DamageNotice")
        private String DamageNotice; //손해배상기준 인쇄 문구
        @SerializedName("IsPrintStorageNotice")
        private String IsPrintStorageNotice; //세탁물 보관료 안내 출력 여부
        @SerializedName("StorageNotice")
        private String StorageNotice; //세탁물 보관료 안내 문구
        @SerializedName("IsPrintStoreNotice")
        private String IsPrintStoreNotice; //세탁소 안내 출력 여부
        @SerializedName("StoreNotice")
        private String StoreNotice; //세탁소 안내 문구
        @SerializedName("RegDate")
        private String RegDate; //등록일
        @SerializedName("UpdDate")
        private String UpdDate; //수정일

        public String getStoreID() {
            return StoreID;
        }

        public int getPrintCount() {
            return PrintCount;
        }

        public String getIsPrintBalance() {
            return IsPrintBalance;
        }

        public String getIsPrintDamageNotice() {
            return IsPrintDamageNotice;
        }

        public String getDamageNotice() {
            return DamageNotice;
        }

        public String getIsPrintStorageNotice() {
            return IsPrintStorageNotice;
        }

        public String getStorageNotice() {
            return StorageNotice;
        }

        public String getIsPrintStoreNotice() {
            return IsPrintStoreNotice;
        }

        public String getStoreNotice() {
            return StoreNotice;
        }

        public String getRegDate() {
            return RegDate;
        }

        public String getUpdDate() {
            return UpdDate;
        }
    }
}
