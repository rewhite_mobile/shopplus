package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

public class MemoItemList {

    @SerializedName("MemoID")
    private int memoID;          //메모ID
    @SerializedName("StoreID")
    private int storeID;         //가맹점ID
    @SerializedName("MemoOrder")
    private int memoOrder;       //메모순번
    @SerializedName("MemoType")
    private int memoType;        //메모TYPE
    @SerializedName("MemoTypeDesc")
    private String memoTypeDesc; //메모타입명
    @SerializedName("Memo")
    private String memo;         //메모
    @SerializedName("IsUse")
    private String isUse;

    public int getMemoID() {
        return memoID;
    }

    public int getStoreID() {
        return storeID;
    }

    public int getMemoOrder() {
        return memoOrder;
    }

    public int getMemoType() {
        return memoType;
    }

    public String getMemoTypeDesc() {
        return memoTypeDesc;
    }

    public String getMemo() {
        return memo;
    }

    public String getIsUse() {
        return isUse;
    }
}
