package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * 매장관리 정보조회 Item
 */
public class GetStoreManageInfo {
    private static final String TAG = "GetStoreManageInfo";

    @SerializedName("Data")
    Data Data;

    public GetStoreManageInfo.Data getData() {
        return Data;
    }

    public static class Data {
        @SerializedName("StoreID")
        private int StoreID;                   //가맹점ID
        @SerializedName("StoreGreetingText")
        private String StoreGreetingText;      //매장인사말
        @SerializedName("TagNoType")
        private int TagNoType;                 //택번호 타입
        @SerializedName("AvailPaymentType")
        private String AvailPaymentType;       //지원결제방식
        @SerializedName("IsGiveBonusPrePaid")
        private String IsGiveBonusPrePaid;     // 선충전금 보너스 지급 여부
        @SerializedName("CashBonusPrePaidRate")
        private Double CashBonusPrePaidRate;   //현금결제 선충전금 보너스 지급율
        @SerializedName("CardBonusPrePaidRate")
        private Double CardBonusPrePaidRate;   // 카드결제 선충전금 보너스 지급율
        @SerializedName("IsPrePaidCashReceipt")
        private String IsPrePaidCashReceipt;   //선충전금 현금영주증 발급여부
        @SerializedName("IsSaveMileage")
        private String IsSaveMileage;          //마일리지 적립여부
        @SerializedName("CashMileageRate")
        private Double CashMileageRate;        //현금결제 마일리지 적립율
        @SerializedName("CardMileageRate")
        private Double CardMileageRate;        //카드결제 마일리지 적립율
        @SerializedName("MileageUseMinPrice")
        private int MileageUseMinPrice;        //마일리지 최소 사용금액
        @SerializedName("ClosingWeekType")
        private int ClosingWeekType;           //정기휴무 타입(1:매주, 2:격주(1,3주), 3:격주(2,4주))
        @SerializedName("ClosingWeek")
        private String ClosingWeek;            //정기휴무(ex:1111111)
        @SerializedName("ClosingDays")
        private ArrayList<String> ClosingDays;      //특정휴무일
        @SerializedName("ServiceStartTime")
        private String ServiceStartTime;       //영업시작시간(평일)
        @SerializedName("ServiceEndTime")
        private String ServiceEndTime;         //영업종료시간(평일)
        @SerializedName("ServiceStartTime_Sat")
        private String ServiceStartTime_Sat;   // 영업시작시간(토요일)
        @SerializedName("ServiceEndTime_Sat")
        private String ServiceEndTime_Sat;     //영업종료시간(토요일)
        @SerializedName("ServiceStartTime_Sun")
        private String ServiceStartTime_Sun;   //영업시작시간(일요일)
        @SerializedName("ServiceEndTime_Sun")
        private String ServiceEndTime_Sun;     //영업종료시간(일요일)
        @SerializedName("WashingCostDay")
        private int WashingCostDay;            //세탁소요일
        @SerializedName("RegDate")
        private String RegDate;                //등록일
        @SerializedName("UpdDate")
        private String UpdDate;                //수정일

        public int getStoreID() { return StoreID; }

        public String getStoreGreetingText() { return StoreGreetingText; }

        public int getTagNoType() { return TagNoType; }

        public String getAvailPaymentType() { return AvailPaymentType; }

        public String getIsGiveBonusPrePaid() { return IsGiveBonusPrePaid; }

        public Double getCashBonusPrePaidRate() { return CashBonusPrePaidRate; }

        public Double getCardBonusPrePaidRate() { return CardBonusPrePaidRate; }

        public String getIsPrePaidCashReceipt() {
            return IsPrePaidCashReceipt;
        }

        public String getIsSaveMileage() {
            return IsSaveMileage;
        }

        public Double getCashMileageRate() { return CashMileageRate; }

        public Double getCardMileageRate() { return CardMileageRate; }

        public int getClosingWeekType() {
            return ClosingWeekType;
        }

        public String getClosingWeek() {
            return ClosingWeek;
        }

        public ArrayList<String> getClosingDays() { return ClosingDays; }

        public int getMileageUseMinPrice() { return MileageUseMinPrice; }

        public String getServiceStartTime() {
            return ServiceStartTime;
        }

        public String getServiceEndTime() {
            return ServiceEndTime;
        }

        public String getServiceStartTime_Sat() {
            return ServiceStartTime_Sat;
        }

        public String getServiceEndTime_Sat() {
            return ServiceEndTime_Sat;
        }

        public String getServiceStartTime_Sun() {
            return ServiceStartTime_Sun;
        }

        public String getServiceEndTime_Sun() {
            return ServiceEndTime_Sun;
        }

        public int getWashingCostDay() {
            return WashingCostDay;
        }

        public String getRegDate() {
            return RegDate;
        }

        public String getUpdDate() {
            return UpdDate;
        }

    }
}
