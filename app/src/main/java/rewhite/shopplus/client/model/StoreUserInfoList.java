package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

public class StoreUserInfoList {
    private static final String TAG = "StoreUserInfoList";

    @SerializedName("UserType")
    private String UserType;         //회원 타입(S,SR)
    @SerializedName("StoreUserID")
    private int storeUserId;         //가맹점 회원ID
    @SerializedName("StoreID")
    private int storeId;             //가맹점ID
    @SerializedName("UserID")
    private int userId;              //회원ID
    @SerializedName("UserName")
    private String userName;         //회원명
    @SerializedName("MembershipCardNo")
    private String memberShipCardNo; //멤버쉽카드번호
    @SerializedName("LinkDate")
    private String linkData;         //멤버십 연결일
    @SerializedName("StoreUserGrade")
    private String storeUserGrade;   //가맹점 회원 등급(1,2,3)
    @SerializedName("_UserAddress")
    private String userAddress;      //주소1(읽기전용,복호화)
    @SerializedName("_UserPhone")
    private String userPhone;        //가맹점 회원 휴대폰 번호(읽기전용, 복호화)
    @SerializedName("PhoneSearch")
    private String userPhoneSearch;  //검색용 휴대폰 뒷자리
    @SerializedName("Memo")
    private String memo;             //메모
    @SerializedName("Gender")
    private String gender;           //성별
    @SerializedName("BirthDay")
    private String birthday;         //생일(yyyy-MM-dd)
    @SerializedName("Anniversary")
    private String anniversaty;      //기념일(MM-dd)
    @SerializedName("StoreUserStatus")
    private String storeUserStatus;  //가맹점 회원 상태(1:정상, 9:탈퇴)
    @SerializedName("_StoreUserStatus")
    private String storeUserStatus_type; //가맹점 회원 상태
    @SerializedName("IsNoticeAgree")
    private String isNoticeAgree;    //정보성 알림 수신 동의 여부
    @SerializedName("IsPromoteAgree")
    private String isPromoteAgree;   //광고성 알림 수신 동의여부
    @SerializedName("TotalPrePaid")
    private int TotalPrePaid;     //사용가능한 선충전금 총합
    @SerializedName("RealPrePaid")
    private int RealPrePaid;      //사용가능한 선충전금
    @SerializedName("BonusPrePaid")
    private int BonusPrePaid;     //사용가능한 추가적립(보너스) 선충전금
    @SerializedName("Mileage")
    private int Mileage;          //마일리지
    @SerializedName("ExMileage")
    private int ExMileage;        //적립 예정된 마일리지
    @SerializedName("RegDate")
    private String RegDate;       //등록일
    @SerializedName("UpdDate")
    private String UpdDate;       //수정일

    //Message
    @SerializedName("Message")
    private String message;

    //ResultCode
    @SerializedName("ResultCode")
    private String resultCode;

    //Count
    @SerializedName("Count")
    private int count;

    //TotalCount;
    @SerializedName("TotalCount")
    private int totalCount;

    public int getStoreUserId() {
        return storeUserId;
    }

    public int getStoreId() {
        return storeId;
    }

    public int getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserType() {
        return UserType;
    }

    public String getMemberShipCardNo() {
        return memberShipCardNo;
    }

    public String getLinkData() {
        return linkData;
    }

    public String getStoreUserGrade() {
        return storeUserGrade;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public String getUserPhoneSearch() {
        return userPhoneSearch;
    }

    public String getMemo() {
        return memo;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getAnniversaty() {
        return anniversaty;
    }

    public String getStoreUserStatus() {
        return storeUserStatus;
    }

    public String getStoreUserStatus_type() {
        return storeUserStatus_type;
    }

    public String getIsNoticeAgree() {
        return isNoticeAgree;
    }

    public String getIsPromoteAgree() {
        return isPromoteAgree;
    }

    public int getTotalPrePaid() {
        return TotalPrePaid;
    }

    public int getRealPrePaid() {
        return RealPrePaid;
    }

    public int getBonusPrePaid() {
        return BonusPrePaid;
    }

    public int getMileage() {
        return Mileage;
    }

    public int getExMileage() {
        return ExMileage;
    }

    public String getRegDate() {
        return RegDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }

    public String getMessage() {
        return message;
    }

    public String getResultCode() {
        return resultCode;
    }

    public int getCount() {
        return count;
    }

    public int getTotalCount() {
        return totalCount;
    }
}
