package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 *  FAQ 카테고리 리스트 Model class
 */
public class FaqCategoryListModel {

    @SerializedName("FaqCategoryID")
    private int FaqCategoryID;// FAQ 카테고리ID
    @SerializedName("FaqCategoryType")
    private int FaqCategoryType;// FAQ 카테고리 타입(1:유저, 2:가맹점, 3:샵플러스)
    @SerializedName("FaqCategoryTypeDesc")
    private String FaqCategoryTypeDesc;// FAQ 카테고리 타입 명
    @SerializedName("FaqCategoryOrder")
    private int FaqCategoryOrder;// 노출순번
    @SerializedName("FaqCategoryName")
    private String FaqCategoryName;// FAQ 카테고리 명
    @SerializedName("IsUse")
    private String IsUse;// 사용여부
    @SerializedName("RegDate")
    private String RegDate;// 등록일
    @SerializedName("UpdDate")
    private String UpdDate;// 수정일

    public int getFaqCategoryID() {
        return FaqCategoryID;
    }

    public int getFaqCategoryType() {
        return FaqCategoryType;
    }

    public String getFaqCategoryTypeDesc() {
        return FaqCategoryTypeDesc;
    }

    public int getFaqCategoryOrder() {
        return FaqCategoryOrder;
    }

    public String getFaqCategoryName() {
        return FaqCategoryName;
    }

    public String getIsUse() {
        return IsUse;
    }

    public String getRegDate() {
        return RegDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }
}
