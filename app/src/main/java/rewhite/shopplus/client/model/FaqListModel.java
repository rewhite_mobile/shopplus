package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

public class FaqListModel {

    @SerializedName("FaqID")
    private int FaqID;// FAQ ID
    @SerializedName("FaqOrder")
    private int FaqOrder;// 노출순번
    @SerializedName("FaqTitle")
    private String FaqTitle;// 제목
    @SerializedName("FaqContent")
    private String FaqContent;// 내용
    @SerializedName("IsUse")
    private String IsUse;// 사용여부
    @SerializedName("RegDate")
    private String RegDate;// 등록일
    @SerializedName("UpdDate")
    private String UpdDate;// 수정일
    @SerializedName("WebViewUrl")
    private String WebViewUrl;// 웹뷰 URL
    @SerializedName("FaqCategoryID")
    private int FaqCategoryID;// FAQ 카테고리ID
    @SerializedName("FaqCategoryType")
    private int FaqCategoryType;// FAQ 카테고리 타입(1:유저, 2:가맹점, 3:샵플러스)
    @SerializedName("FaqCategoryTypeDesc")
    private String FaqCategoryTypeDesc;// FAQ 카테고리 타입 명
    @SerializedName("FaqCategoryOrder")
    private int FaqCategoryOrder;// 노출순번
    @SerializedName("FaqCategoryName")
    private String FaqCategoryName;// FAQ 카테고리 명

    public int getFaqID() {
        return FaqID;
    }

    public int getFaqOrder() {
        return FaqOrder;
    }

    public String getFaqTitle() {
        return FaqTitle;
    }

    public String getFaqContent() {
        return FaqContent;
    }

    public String getIsUse() {
        return IsUse;
    }

    public String getRegDate() {
        return RegDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }

    public String getWebViewUrl() {
        return WebViewUrl;
    }

    public int getFaqCategoryID() {
        return FaqCategoryID;
    }

    public int getFaqCategoryType() {
        return FaqCategoryType;
    }

    public String getFaqCategoryTypeDesc() {
        return FaqCategoryTypeDesc;
    }

    public int getFaqCategoryOrder() {
        return FaqCategoryOrder;
    }

    public String getFaqCategoryName() {
        return FaqCategoryName;
    }
}
