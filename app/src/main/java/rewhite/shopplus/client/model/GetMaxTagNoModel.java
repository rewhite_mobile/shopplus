package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

public class GetMaxTagNoModel {
    private static final String TAG = "GetMaxTagNoModel";

    @SerializedName("Data")
    GetMaxTagNoModel.Data Data;

    public GetMaxTagNoModel.Data getData() {
        return Data;
    }

    public static class Data {
        @SerializedName("TagNo")
        private String TagNo; // 택번호
        @SerializedName("TagColor")
        private String TagColor; // 택컬러

        public String getTagNo() {
            return TagNo;
        }

        public String getTagColor() {
            return TagColor;
        }
    }
}
