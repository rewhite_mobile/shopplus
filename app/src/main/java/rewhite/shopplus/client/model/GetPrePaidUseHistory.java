package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetPrePaidUseHistory {
    private static final String TAG = "GetPrePaidUseHistory";

    @SerializedName("ChangeType")
    private int ChangeType;          //변경타입
    @SerializedName("ChangeTypeDesc")
    private String ChangeTypeDesc;   //변경타입명
    @SerializedName("ApplyDate")
    private String ApplyDate;        //충전/사용일
    @SerializedName("PaymentPrice")
    private int PaymentPrice;        //결제금액
    @SerializedName("Amount")
    private int Amount;              //충전/사용 금액
    @SerializedName("PaymentID")
    private int PaymentID;           //결제ID (충전원)
    @SerializedName("PaymentDetails")
    private ArrayList<PaymentDetails> PaymentDetails; //결제상세 정보

    public int getChangeType() {
        return ChangeType;
    }

    public String getChangeTypeDesc() {
        return ChangeTypeDesc;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public int getPaymentPrice() {
        return PaymentPrice;
    }

    public int getAmount() {
        return Amount;
    }

    public int getPaymentID() {
        return PaymentID;
    }

    public ArrayList<PaymentDetails> getPaymentDetails() {
        return PaymentDetails;
    }

    public class PaymentDetails {
        @SerializedName("PaymemtDetailID")
        private int PaymemtDetailID; //결제상세ID
        @SerializedName("PaymentID")
        private int PaymentID;       //결제ID
        @SerializedName("PaymentType")
        private int PaymentType;     //결제타입(1:현금,2:카드,3:선충전금,4:마일리지,5:쿠폰,10:온라인)
        @SerializedName("PaymentTypeDesc")
        private String PaymentTypeDesc; //결제 타입 명
        @SerializedName("IsReceipt")
        private String IsReceipt;       //영수증발급여부(Y/N)
        @SerializedName("PGID")
        private int PGID;            //PG ID
        @SerializedName("TID")
        private String TID;             //승인번호
        @SerializedName("CATInfo")
        private String CATInfo;         //카드 단말기 정보
        @SerializedName("CardName")
        private String CardName;        //결제 카드명
        @SerializedName("CardNo")
        private String CardNo;          //카드번호
        @SerializedName("CardDivide")
        private int CardDivide;      //할부개월수
        @SerializedName("PayYMD")
        private String PayYMD;          //원거래일자
        @SerializedName("Price")
        private int Price;           //결제금액
        @SerializedName("RegDate")
        private String RegDate;         //등록일
        @SerializedName("UpdDate")
        private String UpdDate;         //수정일

        public int getPaymemtDetailID() {
            return PaymemtDetailID;
        }

        public int getPaymentID() {
            return PaymentID;
        }

        public int getPaymentType() {
            return PaymentType;
        }

        public String getPaymentTypeDesc() {
            return PaymentTypeDesc;
        }

        public String getIsReceipt() {
            return IsReceipt;
        }

        public int getPGID() {
            return PGID;
        }

        public String getTID() {
            return TID;
        }

        public String getCATInfo() {
            return CATInfo;
        }

        public String getCardName() {
            return CardName;
        }

        public String getCardNo() {
            return CardNo;
        }

        public int getCardDivide() {
            return CardDivide;
        }

        public String getPayYMD() {
            return PayYMD;
        }

        public int getPrice() {
            return Price;
        }

        public String getRegDate() {
            return RegDate;
        }

        public String getUpdDate() {
            return UpdDate;
        }
    }
}