package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 * 계약정보 조회 Model
 */
public class GetStoreContractInfoModel {

    @SerializedName("Data")
    Data Data;

    public Data getData() {
        return Data;
    }

    public static class Data {
        @SerializedName("StoreID")
        private int StoreID;// 가맹점ID
        @SerializedName("ShareCode")
        private String ShareCode;// 추천코드
        @SerializedName("ApplyDate")
        private String ApplyDate;// 적용일
        @SerializedName("ContractType")
        private String ContractType;// 계약타입
        @SerializedName("StoreName")
        private String StoreName;// 가맹점명
        @SerializedName("ContractManagerName")
        private String ContractManagerName;//영업담당자 이름
        @SerializedName("_ContractManagerPhone")
        private String _ContractManagerPhone;//영업담당자 연락처
        @SerializedName("MasterName")
        private String MasterName;// 가맹점 대표자 성명
        @SerializedName("_MasterPhone")
        private String _MasterPhone;// 대표자 연락처
        @SerializedName("_MasterEmail")
        private String _MasterEmail;// 대표자 이메일
        @SerializedName("_StoreAddress1")
        private String _StoreAddress1;// 가맹점 주소
        @SerializedName("_StoreAddress2")
        private String _StoreAddress2;// 가맹점 상세주소
        @SerializedName("_StoreZipCode")
        private String _StoreZipCode;// 가맹점 우편번호
        @SerializedName("_BusinessRegistrationNumber")
        private String _BusinessRegistrationNumber;
        @SerializedName("_AccountBankNumber")
        private String _AccountBankNumber;// 은행 계좌번호
        @SerializedName("AccountBankName")
        private String AccountBankName;// 은행명
        @SerializedName("_AccountHolder")
        private String _AccountHolder;// 예금주
        @SerializedName("CalculateCycle")
        private int CalculateCycle;// 월 정산주기(1,2)
        @SerializedName("RegDate")
        private String RegDate;// 등록일
        @SerializedName("AdminId")
        private int AdminId;// 관리자ID


        public int getStoreID() {
            return StoreID;
        }

        public String getShareCode() {
            return ShareCode;
        }

        public String getApplyDate() {
            return ApplyDate;
        }

        public String getContractType() {
            return ContractType;
        }

        public String getContractManagerName() {
            return ContractManagerName;
        }

        public String get_ContractManagerPhone() {
            return _ContractManagerPhone;
        }

        public String getStoreName() {
            return StoreName;
        }

        public String getMasterName() {
            return MasterName;
        }

        public String get_MasterPhone() {
            return _MasterPhone;
        }

        public String get_MasterEmail() {
            return _MasterEmail;
        }

        public String get_StoreAddress1() {
            return _StoreAddress1;
        }

        public String get_StoreAddress2() {
            return _StoreAddress2;
        }

        public String get_StoreZipCode() {
            return _StoreZipCode;
        }

        public String get_BusinessRegistrationNumber() {
            return _BusinessRegistrationNumber;
        }

        public String get_AccountBankNumber() {
            return _AccountBankNumber;
        }

        public String getAccountBankName() {
            return AccountBankName;
        }

        public String get_AccountHolder() {
            return _AccountHolder;
        }

        public int getCalculateCycle() {
            return CalculateCycle;
        }

        public String getRegDate() {
            return RegDate;
        }

        public int getAdminId() {
            return AdminId;
        }
    }
}
