package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

public class StoreLoginModel {
    @SerializedName("Message")
    String Message;
    @SerializedName("Data")
    StoreLoginModel.Data Data;
    @SerializedName("ResultCode")
    String ResultCode;

    public String getResultCode() {
        return ResultCode;
    }

    public String getMessage() {
        return Message;
    }

    public StoreLoginModel.Data getData() {
        return Data;
    }

    public static class Data {
        @SerializedName("StaffID")
        private int StaffID; //직원ID
        @SerializedName("StaffName")
        private String StaffName; //직원명
        @SerializedName("StaffPhone")
        private String StaffPhone; //직원연락처
        @SerializedName("IsUse")
        private String IsUse; // 사용여부
        @SerializedName("LoginID")
        private String LoginID; // 로그인 ID (Key, 중복체크 필요)
        @SerializedName("AccessToken")
        private String AccessToken; //액세스토큰
        @SerializedName("AccessTokenExpiredDate")
        private String AccessTokenExpiredDate; // 액세스토큰 만료일
        @SerializedName("StoreID")
        private int StoreID; // 가맹점ID
        @SerializedName("StoreGrade")
        private String StoreGrade; // 가맹점등급
        @SerializedName("StoreType")
        private String StoreType; //가맹점타입(1:모바일, 2:방문, 3:모바일+방문)
        @SerializedName("StoreCategory")
        private int StoreCategory; // 1 : 세탁, 4 : 파트너(세탁)
        @SerializedName("StoreName")
        private String StoreName; //가맹점명
        @SerializedName("StoreDisplayName")
        private String StoreDisplayName; //노출 가맹점명
        @SerializedName("IsPickupDelivery")
        private String IsPickupDelivery; //수거/배달 여부
        @SerializedName("StoreTelephone")
        private String StoreTelephone; //가맹점 전화번호
        @SerializedName("StoreVirtualPhone")
        private String StoreVirtualPhone; //가맹점 가상 전화번호
        @SerializedName("StorePaymentMethod")
        private String StorePaymentMethod; //매장 소유 결제 기기 - 모바일용
        @SerializedName("StoreLicense")
        private String StoreLicense; //라이센스 보유 사항 - 모바일용 (json text)
        @SerializedName("ShareCode")
        private String ShareCode; //가맹점 공유코드
        @SerializedName("StoreImage")
        private String StoreImage; //가맹점이미지(구분자 '|')
        @SerializedName("IsTermination")
        private String IsTermination; //서비스 여부 (리스트 노출 X, 주문 X) - 모바일 전용
        @SerializedName("IsService")
        private String IsService; // 서비스 여부 (리스트 노출 X, 주문 X) - 모바일 전용
        @SerializedName("IsSuspend")
        private String IsSuspend; //일시중지 여부 (리스트 노출 O, 주문 X) - 모바일 전용
        @SerializedName("StoreServiceAreaText")
        private String StoreServiceAreaText; //서비스지역 설명
        @SerializedName("Longitude")
        private Number Longitude; //경도
        @SerializedName("Latitude")
        private Number Latitude; //위도
        @SerializedName("AreaZone1")
        private int AreaZone1; //통계 지역1
        @SerializedName("AreaZone2")
        private int AreaZone2; //통계 지역2
        @SerializedName("AreaZone3")
        private int AreaZone3; //통계 지역3
        @SerializedName("AreaZoneStore")
        private int AreaZoneStore; //통계 지역 가맹점
        @SerializedName("IsPrinter")
        private String IsPrinter; //영수증 프린터 설치 여부 - 모바일용 (GS 연동시 필요)
        @SerializedName("StoreRateScore")
        private Number StoreRateScore; // (number, optional): 가맹점 평균 평점
        @SerializedName("RegDate")
        private String RegDate;// 등록일
        @SerializedName("UpdDate")
        private String UpdDate;// 수정일


        public int getStaffID() {
            return StaffID;
        }

        public String getStaffName() {
            return StaffName;
        }

        public String getStaffPhone() {
            return StaffPhone;
        }

        public String getIsUse() {
            return IsUse;
        }

        public String getLoginID() {
            return LoginID;
        }

        public String getAccessToken() {
            return AccessToken;
        }

        public String getAccessTokenExpiredDate() {
            return AccessTokenExpiredDate;
        }

        public int getStoreID() {
            return StoreID;
        }

        public String getStoreGrade() {
            return StoreGrade;
        }

        public String getStoreType() {
            return StoreType;
        }

        public int getStoreCategory() {
            return StoreCategory;
        }

        public String getStoreName() {
            return StoreName;
        }

        public String getStoreDisplayName() {
            return StoreDisplayName;
        }

        public String getIsPickupDelivery() {
            return IsPickupDelivery;
        }

        public String getStoreTelephone() {
            return StoreTelephone;
        }

        public String getStoreVirtualPhone() {
            return StoreVirtualPhone;
        }

        public String getStorePaymentMethod() {
            return StorePaymentMethod;
        }

        public String getStoreLicense() {
            return StoreLicense;
        }

        public String getShareCode() {
            return ShareCode;
        }

        public String getStoreImage() {
            return StoreImage;
        }

        public String getIsTermination() {
            return IsTermination;
        }

        public String getIsService() {
            return IsService;
        }

        public String getIsSuspend() {
            return IsSuspend;
        }

        public String getStoreServiceAreaText() {
            return StoreServiceAreaText;
        }

        public Number getLongitude() {
            return Longitude;
        }

        public Number getLatitude() {
            return Latitude;
        }

        public int getAreaZone1() {
            return AreaZone1;
        }

        public int getAreaZone2() {
            return AreaZone2;
        }

        public int getAreaZone3() {
            return AreaZone3;
        }

        public int getAreaZoneStore() {
            return AreaZoneStore;
        }

        public String getIsPrinter() {
            return IsPrinter;
        }

        public Number getStoreRateScore() {
            return StoreRateScore;
        }

        public String getRegDate() {
            return RegDate;
        }

        public String getUpdDate() {
            return UpdDate;
        }
    }
}
