package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordModel {

    @SerializedName("ResultCode")
    private String ResultCode;// 결과 코드
    @SerializedName("Message")
    private String Message; //결과 메시지
    @SerializedName("Count")
    private int Count; //레코드 수
    @SerializedName("TotalCount")
    private int TotalCount; //총 레코드 수
    @SerializedName("PageBlock")
    private int PageBlock; //페이지 크기
    @SerializedName("LastPage")
    private int LastPage; //마지막 페이지 번호
    @SerializedName("Data")
    private int Data; //제네릭형 데이터

    public void setResultCode(String resultCode) {
        ResultCode = resultCode;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public void setCount(int count) {
        Count = count;
    }

    public void setTotalCount(int totalCount) {
        TotalCount = totalCount;
    }

    public void setPageBlock(int pageBlock) {
        PageBlock = pageBlock;
    }

    public void setLastPage(int lastPage) {
        LastPage = lastPage;
    }

    public void setData(int data) {
        Data = data;
    }

    public String getResultCode() {
        return ResultCode;
    }

    public String getMessage() {
        return Message;
    }

    public int getCount() {
        return Count;
    }

    public int getTotalCount() {
        return TotalCount;
    }

    public int getPageBlock() {
        return PageBlock;
    }

    public int getLastPage() {
        return LastPage;
    }

    public int getData() {
        return Data;
    }
}
