package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * 월별 매출 현황 (기본기간:최근3개월) ItemClass
 */
public class GetMonthSalesStatList {
    private static final String TAG = "GetMonthSalesStatList";

    @SerializedName("Data")
    GetStoreManageInfo.Data Data;

    public GetStoreManageInfo.Data getData() {
        return Data;
    }

    public static class Data {
        @SerializedName("PeriodType")
        private int PeriodType;              //기간 타입(1:일, 2:월)
        @SerializedName("PeriodTypeDesc")
        private String PeriodTypeDesc;       //기간 타입 명
        @SerializedName("StartDate")
        private String StartDate;            //조회 시작일
        @SerializedName("EndDate")
        private String EndDate;              //조회 종료일
        @SerializedName("TotalEnterCount")
        private int TotalEnterCount;         //총 접수(입고) 수
        @SerializedName("TotalOutCount")
        private int TotalOutCount;           //총 출고 수
        @SerializedName("TotalSale")
        private int TotalSale;               //총 매출액
        @SerializedName("DayAverageSale")
        private int DayAverageSale;          //일 평균 매출액
        @SerializedName("MonthAverageSale")
        private int MonthAverageSale;        //월 평균 매출액
        @SerializedName("Record")
        private ArrayList<SalesStat> Record; //레코드
    }

    private class SalesStat {
        @SerializedName("Base_Date")
        private String Base_Date;    //기준일
        @SerializedName("Enter_Count")
        private int Enter_Count;     //접수(입고) 수
        @SerializedName("Out_Count")
        private int Out_Count;       //출고 수
        @SerializedName("Normal_Count")
        private int Normal_Count;    //정상 결제건 수
        @SerializedName("Cancel_Count")
        private int Cancel_Count;    //결제 취소건 수
        @SerializedName("Total_Sales")
        private int Total_Sales;     //총 매출
        @SerializedName("Cash_Sales")
        private int Cash_Sales;      //현금 매출
        @SerializedName("Card_Sales")
        private int Card_Sales;      //카드 매출
        @SerializedName("Prepaid_Sales")
        private int Prepaid_Sales;   //선충전금 매출
    }
}