package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 * 세탁물 취급설정 Item
 */
public class GetHandleItemList {

    @SerializedName("StoreID")
    private int StoreID;               //가맹점ID
    @SerializedName("ItemGroupID")
    private int ItemGroupID;           //품목그룹ID
    @SerializedName("ItemGroupType")
    private int ItemGroupType;         //품목그룹타입
    @SerializedName("ItemGroupTypeDesc")
    private String ItemGroupTypeDesc;  //품목그룹타입명
    @SerializedName("ItemGroupName")
    private String ItemGroupName;      //품목그룹명(취급품목명)
    @SerializedName("IsUse")
    private String IsUse;              //취급여부(Y/N)
    @SerializedName("RegDate")
    private String RegDate;            //등록일
    @SerializedName("UpdDate")
    private String UpdDate;            //수정일

    public int getStoreID() {
        return StoreID;
    }

    public int getItemGroupID() {
        return ItemGroupID;
    }

    public int getItemGroupType() {
        return ItemGroupType;
    }

    public String getItemGroupTypeDesc() {
        return ItemGroupTypeDesc;
    }

    public String getItemGroupName() {
        return ItemGroupName;
    }

    public String getIsUse() {
        return IsUse;
    }

    public String getRegDate() {
        return RegDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }

    public void setIsUse(String isUse) {
        IsUse = isUse;
    }
}
