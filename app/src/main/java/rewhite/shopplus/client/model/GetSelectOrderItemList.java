package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * 주문품목조회 DataModel
 */
public class GetSelectOrderItemList {

    @SerializedName("StoreUserID")
    private int StoreUserID; //가맹점 회원ID
    @SerializedName("StoreID")
    private int StoreID; //가맹점ID
    @SerializedName("UserID")
    private int UserID; //회원ID
    @SerializedName("UserName")
    private String UserName; //회원명
    @SerializedName("StoreUserGrade")
    private String StoreUserGrade; //가맹점 회원 등급(1,2,3)
    @SerializedName("_UserAddress")
    private String _UserAddress; //주소1(읽기전용,복호화)
    @SerializedName("_UserPhone")
    private String _UserPhone; //가맹점 회원 휴대폰 번호(읽기전용, 복호화)
    @SerializedName("OrderID")
    private int OrderID; //주문ID
    @SerializedName("OrderItemID")
    private int OrderItemID; //주문상품ID
    @SerializedName("TagNo")
    private String TagNo; //Tag번호
    @SerializedName("TagColor")
    private String TagColor;//Tag 컬러
    @SerializedName("StoreItemID")
    private int StoreItemID; //가맹점 상품ID
    @SerializedName("StoreItemName")
    private String StoreItemName; //가맹점 상품명
    @SerializedName("ItemGrade")
    private int ItemGrade; //상품등급(1:일반,2:명품,3:아동)
    @SerializedName("ItemGradeDesc")
    private String ItemGradeDesc; //상풍등급명
    @SerializedName("StoreItemPrice")
    private int StoreItemPrice; //상품 금액
    @SerializedName("EnterDate")
    private String EnterDate; //입고일
    @SerializedName("WashFinWillDate")
    private String WashFinWillDate; //세탁완료예정일
    @SerializedName("WashFinDate")
    private String WashFinDate; //세탁완료일
    @SerializedName("OutDate")
    private String OutDate; //출고일
    @SerializedName("IsOutside")
    private String IsOutside; //외부세탁여부
    @SerializedName("IsDelivery")
    private String IsDelivery; //배달여부
    @SerializedName("PickupID")
    private int PickupID; //수거ID
    @SerializedName("DeliveryID")
    private int DeliveryID; //배송ID
    @SerializedName("PhotoURL")
    private String PhotoURL; //첨부사진 (구분자'|')
    @SerializedName("WashType")
    private int WashType; //세탁방법 : 1:드라이+다림질, 2:물세탁+다림질, 3:다림질
    @SerializedName("WashTypeDesc")
    private String WashTypeDesc; //세탁방법명
    @SerializedName("FreeType")
    private int FreeType; //기타옵션 : 1:무료서비스, 2:재세탁, 3:세탁진행중단
    @SerializedName("FreeTypeDesc")
    private String FreeTypeDesc; //기타옵션명
    @SerializedName("ItemColor")
    private String ItemColor; //색상(#FFFFFF)
    @SerializedName("OrderItemPrice")
    private int OrderItemPrice; //주문 금액
    @SerializedName("OrderItemMemo")
    private String OrderItemMemo; //메모(구분자'|')
    @SerializedName("OrderItemStatus")
    private int OrderItemStatus; //상태
    @SerializedName("OrderItemStatusDesc")
    private String OrderItemStatusDesc; //상태명
    @SerializedName("Badges")
    private ArrayList<String> Badges; //주문 정보 뱃지
    @SerializedName("PayPrice")
    private int PayPrice; //기결제금액
    @SerializedName("NonPayPrice")
    private int NonPayPrice; //미결제금액(미수금)
    @SerializedName("Options")
    private ArrayList<OrderItemOption> Options; //주문상품옵션

    public int getStoreUserID() {
        return StoreUserID;
    }

    public int getStoreID() {
        return StoreID;
    }

    public int getUserID() {
        return UserID;
    }

    public String getUserName() {
        return UserName;
    }

    public String getStoreUserGrade() {
        return StoreUserGrade;
    }

    public String get_UserAddress() {
        return _UserAddress;
    }

    public String get_UserPhone() {
        return _UserPhone;
    }

    public int getOrderID() {
        return OrderID;
    }

    public int getOrderItemID() {
        return OrderItemID;
    }

    public String getTagNo() {
        return TagNo;
    }

    public String getTagColor() {
        return TagColor;
    }

    public int getStoreItemID() {
        return StoreItemID;
    }

    public String getStoreItemName() {
        return StoreItemName;
    }

    public int getItemGrade() {
        return ItemGrade;
    }

    public String getItemGradeDesc() {
        return ItemGradeDesc;
    }

    public int getStoreItemPrice() {
        return StoreItemPrice;
    }

    public String getEnterDate() {
        return EnterDate;
    }

    public String getWashFinWillDate() {
        return WashFinWillDate;
    }

    public String getWashFinDate() {
        return WashFinDate;
    }

    public String getOutDate() {
        return OutDate;
    }

    public String getIsOutside() {
        return IsOutside;
    }

    public String getIsDelivery() {
        return IsDelivery;
    }

    public int getPickupID() {
        return PickupID;
    }

    public int getDeliveryID() {
        return DeliveryID;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public int getWashType() {
        return WashType;
    }

    public String getWashTypeDesc() {
        return WashTypeDesc;
    }

    public int getFreeType() {
        return FreeType;
    }

    public String getFreeTypeDesc() {
        return FreeTypeDesc;
    }

    public String getItemColor() {
        return ItemColor;
    }

    public int getOrderItemPrice() {
        return OrderItemPrice;
    }

    public String getOrderItemMemo() {
        return OrderItemMemo;
    }

    public int getOrderItemStatus() {
        return OrderItemStatus;
    }

    public String getOrderItemStatusDesc() {
        return OrderItemStatusDesc;
    }

    public ArrayList<String> getBadges() {
        return Badges;
    }

    public int getPayPrice() {
        return PayPrice;
    }

    public int getNonPayPrice() {
        return NonPayPrice;
    }

    public ArrayList<OrderItemOption> getOptions() {
        return Options;
    }

    public static class OrderItemOption {
        @SerializedName("OrderItemID")
        private int OrderItemID;//주문상품ID
        @SerializedName("OptionType")
        private int OptionType;//옵션 상품 타입:추가기술, 부속품, 수선
        @SerializedName("OptionTypeDesc")
        private String OptionTypeDesc; //옵션타입명
        @SerializedName("OptionItemID")
        private int OptionItemID; //옵션 상품ID
        @SerializedName("OptionItemName")
        private String OptionItemName; // 옵션상품명
        @SerializedName("OptionItemGrade")
        private int OptionItemGrade; //옵션 상품 등급
        @SerializedName("OptionPrice")
        private int OptionPrice; //옵션금액
        @SerializedName("IsUse")
        private String IsUse; //사용여부(Y,N)
        @SerializedName("RegDate")
        private String RegDate; //등록일
        @SerializedName("UpdDate")
        private String UpdDate; //수정일

        public int getOrderItemID() {
            return OrderItemID;
        }

        public int getOptionType() {
            return OptionType;
        }

        public String getOptionTypeDesc() {
            return OptionTypeDesc;
        }

        public int getOptionItemID() {
            return OptionItemID;
        }

        public String getOptionItemName() {
            return OptionItemName;
        }

        public int getOptionItemGrade() {
            return OptionItemGrade;
        }

        public int getOptionPrice() {
            return OptionPrice;
        }

        public String getIsUse() {
            return IsUse;
        }

        public String getRegDate() {
            return RegDate;
        }

        public String getUpdDate() {
            return UpdDate;
        }
    }
}
