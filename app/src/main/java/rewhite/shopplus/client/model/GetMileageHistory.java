package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

public class GetMileageHistory {

    @SerializedName("ChangeType")
    private int ChangeType; //변경 타입
    @SerializedName("ChangeTypeDesc")
    private String ChangeTypeDesc; //변경 타입명(적립정보)
    @SerializedName("PaymentID")
    private int PaymentID; //결제ID (적립발생원)
    @SerializedName("ApplyDate")
    private String ApplyDate; //적립(예정)/사용일
    @SerializedName("Amount")
    private int Amount; //적립/사용액(+/-)

    public int getChangeType() {
        return ChangeType;
    }

    public String getChangeTypeDesc() {
        return ChangeTypeDesc;
    }

    public int getPaymentID() {
        return PaymentID;
    }

    public String getApplyDate() {
        return ApplyDate;
    }

    public int getAmount() {
        return Amount;
    }
}
