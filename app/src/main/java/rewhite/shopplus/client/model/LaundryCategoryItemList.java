package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 * 품목등록 카테고리
 */
public class LaundryCategoryItemList {

    @SerializedName("CategoryID")
    private int categoryId;
    @SerializedName("CategoryType")
    private int CategoryType;
    @SerializedName("CategoryName")
    private String CategoryName;
    @SerializedName("CategoryOrder")
    private int CategoryOrder;
    @SerializedName("IsUse")
    private String IsUse;

    public int getCategoryId() {
        return categoryId;
    }

    public int getCategoryType() {
        return CategoryType;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public int getCategoryOrder() {
        return CategoryOrder;
    }

    public String getIsUse() {
        return IsUse;
    }
}
