package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 * 운영관리 회원 조회
 */
public class UserManageList {

    @SerializedName("UserType")
    private String UserType;            //회원 타입(S,SR)
    @SerializedName("UserType")
    private int StoreUserID;            //가맹점 회원ID
    @SerializedName("UserType")
    private int StoreID;                //가맹점ID
    @SerializedName("UserType")
    private int UserID;                 //회원ID
    @SerializedName("UserType")
    private String UserName;            //회원명
    @SerializedName("UserType")
    private String MembershipCardNo;    //멤버쉽카드번호
    @SerializedName("UserType")
    private String LinkDate;            //멤버십 연결일
    @SerializedName("UserType")
    private String StoreUserGrade;      //가맹점 회원 등급(1,2,3)
    @SerializedName("UserType")
    private String _UserAddress;        //주소1(읽기전용,복호화)
    @SerializedName("UserType")
    private String _UserPhone;          //가맹점 회원 휴대폰 번호(읽기전용, 복호화)
    @SerializedName("UserType")
    private String PhoneSearch;         //검색용 휴대폰 뒷자리
    @SerializedName("UserType")
    private String Memo;                //메모
    @SerializedName("UserType")
    private String Gender;              //성별
    @SerializedName("UserType")
    private String BirthDay;            //생일(yyyy-MM-dd)
    @SerializedName("UserType")
    private String Anniversary;         //기념일(MM-dd)
    @SerializedName("UserType")
    private int StoreUserStatus;        //가맹점 회원 상태(1:정상, 9:탈퇴)
    @SerializedName("UserType")
    private String _StoreUserStatus;    //가맹점 회원 상태
    @SerializedName("UserType")
    private String IsNoticeAgree;       //정보성 알림 수신 동의 여부
    @SerializedName("UserType")
    private String IsPromoteAgree;      //광고성 알림 수신 동의여부
    @SerializedName("UserType")
    private int TotalPrePaid;           //사용가능한 선충전금 총합
    @SerializedName("UserType")
    private int RealPrePaid;            //사용가능한 선충전금
    @SerializedName("UserType")
    private int BonusPrePaid;           //사용가능한 추가적립(보너스) 선충전금
    @SerializedName("UserType")
    private int Mileage;                //마일리지
    @SerializedName("UserType")
    private int ExMileage;              //적립 예정된 마일리지
    @SerializedName("UserType")
    private String RecentOrderDate;     //최근 주문일
    @SerializedName("UserType")
    private int TotalOrderCount;        //전체 주문 수
    @SerializedName("UserType")
    private int TotalOrderPrice;        //누적주문금액
    @SerializedName("UserType")
    private int TotalPayPrice;          //누적결제금액
    @SerializedName("UserType")
    private int NonPayPrice;            //미수금
    @SerializedName("UserType")
    private int NotOutOrderCount;       //미출고 주문 수
    @SerializedName("UserType")
    private int OutOrderCount;          //출고완료 주문 수
    @SerializedName("UserType")
    private int WashFinOrderCount;      //세탁완료 주문 수
    @SerializedName("UserType")
    private int OutNonPayOrderCount;    //출고 미수 주문 수
    @SerializedName("UserType")
    private int OutNonPayPrice;         //출고 미수금
    @SerializedName("UserType")
    private int NotOutNonPayOrderCount; //미출고 미수 주문수
    @SerializedName("UserType")
    private int NotOutNonPayPrice;      //미출고 미수금
    @SerializedName("UserType")
    private String RegDate;             //등록일
    @SerializedName("UserType")
    private String UpdDate;             //수정일


    public String getUserType() {
        return UserType;
    }

    public int getStoreUserID() {
        return StoreUserID;
    }

    public int getStoreID() {
        return StoreID;
    }

    public int getUserID() {
        return UserID;
    }

    public String getUserName() {
        return UserName;
    }

    public String getMembershipCardNo() {
        return MembershipCardNo;
    }

    public String getLinkDate() {
        return LinkDate;
    }

    public String getStoreUserGrade() {
        return StoreUserGrade;
    }

    public String get_UserAddress() {
        return _UserAddress;
    }

    public String get_UserPhone() {
        return _UserPhone;
    }

    public String getPhoneSearch() {
        return PhoneSearch;
    }

    public String getMemo() {
        return Memo;
    }

    public String getGender() {
        return Gender;
    }

    public String getBirthDay() {
        return BirthDay;
    }

    public String getAnniversary() {
        return Anniversary;
    }

    public int getStoreUserStatus() {
        return StoreUserStatus;
    }

    public String get_StoreUserStatus() {
        return _StoreUserStatus;
    }

    public String getIsNoticeAgree() {
        return IsNoticeAgree;
    }

    public String getIsPromoteAgree() {
        return IsPromoteAgree;
    }

    public int getTotalPrePaid() {
        return TotalPrePaid;
    }

    public int getRealPrePaid() {
        return RealPrePaid;
    }

    public int getBonusPrePaid() {
        return BonusPrePaid;
    }

    public int getMileage() {
        return Mileage;
    }

    public int getExMileage() {
        return ExMileage;
    }

    public String getRecentOrderDate() {
        return RecentOrderDate;
    }

    public int getTotalOrderCount() {
        return TotalOrderCount;
    }

    public int getTotalOrderPrice() {
        return TotalOrderPrice;
    }

    public int getTotalPayPrice() {
        return TotalPayPrice;
    }

    public int getNonPayPrice() {
        return NonPayPrice;
    }

    public int getNotOutOrderCount() {
        return NotOutOrderCount;
    }

    public int getOutOrderCount() {
        return OutOrderCount;
    }

    public int getWashFinOrderCount() {
        return WashFinOrderCount;
    }

    public int getOutNonPayOrderCount() {
        return OutNonPayOrderCount;
    }

    public int getOutNonPayPrice() {
        return OutNonPayPrice;
    }

    public int getNotOutNonPayOrderCount() {
        return NotOutNonPayOrderCount;
    }

    public int getNotOutNonPayPrice() {
        return NotOutNonPayPrice;
    }

    public String getRegDate() {
        return RegDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }
}
