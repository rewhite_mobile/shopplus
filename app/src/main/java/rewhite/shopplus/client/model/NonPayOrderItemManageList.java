package rewhite.shopplus.client.model;

import java.util.ArrayList;

public class NonPayOrderItemManageList {

    private int OrderID;                         //주문ID
    private int OrderItemID;                     //주문상품ID
    private String TagNo;                        //Tag번호
    private String TagColor;                     //Tag 컬러
    private int StoreItemID;                     //가맹점 상품ID
    private String StoreItemName;                //가맹점 상품명
    private int ItemGrade;                       //상품등급(1:일반,2:명품,3:아동)
    private String ItemGradeDesc;                //상풍등급명
    private int StoreItemPrice;                  //상품 금액
    private String EnterDate;                    //입고일
    private String WashFinWillDate;              //세탁완료예정일
    private String WashFinDate;                  //세탁완료일
    private String OutDate;                      //출고일
    private String IsOutside;                    //외부세탁여부
    private String IsDelivery;                   //배달여부
    private int PickupID;                        //수거ID
    private int DeliveryID;                      //배송ID
    private String PhotoURL;                     //첨부사진 (구분자'|')
    private int WashType;                        //세탁방법 : 1:드라이+다림질, 2:물세탁+다림질, 3:다림질
    private String WashTypeDesc;                 //세탁방법명
    private int FreeType;                        //기타옵션 : 1:무료서비스, 2:재세탁, 3:세탁진행중단
    private String FreeTypeDesc;                 //기타옵션명
    private String ItemColor;                    //색상(#FFFFFF)
    private int OrderItemPrice;                  //주문 금액
    private String OrderItemMemo;                //메모(구분자'|')
    private int OrderItemStatus;                 //상태
    private String OrderItemStatusDesc;          //상태명
    private String RegDate;                      //등록일
    private String UpdDate;                      //수정일
    private ArrayList<String> Badges;            //주문 정보 뱃지
    private int PayPrice;                        //기결제금액
    private int NonPayPrice;                     //미결제금액(미수금)
    private ArrayList<OrderItemOption> Options;  //주문상품옵션


    private class OrderItemOption {
        private String OrderItemID;              //주문상품ID
        private String OptionType;               //옵션 상품 타입:추가기술, 부속품, 수선
        private String OptionTypeDesc;           //옵션타입명
        private String OptionItemID;             //옵션 상품ID
        private String OptionItemName;           //옵션상품명
        private String OptionItemGrade;          //옵션 상품 등급
        private String OptionPrice;              //옵션금액
        private String IsUse;                    //사용여부(Y,N)
        private String RegDate;                  //등록일
        private String UpdDate;                  //수정일
    }
}
