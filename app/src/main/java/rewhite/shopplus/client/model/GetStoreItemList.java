package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 * 품목 리스트 조회(매장관리용) Item
 */
public class GetStoreItemList {
    @SerializedName("StoreID")
    private int StoreID;             //가맹점ID
    @SerializedName("CategoryID")
    private int CategoryID;          //카테고리ID
    @SerializedName("CategoryName")
    private String CategoryName;     //카테고리 명
    @SerializedName("ClassID")
    private int ClassID;             //클래스(중분류)ID
    @SerializedName("ClassName")
    private String ClassName;        //클래스(중분류) 명
    @SerializedName("StoreItemID")
    private int StoreItemID;         //상품ID
    @SerializedName("ItemType")
    private int ItemType;            //상품 타입 (1:기본, 2:커스텀(추가))
    @SerializedName("ItemName")
    private String ItemName;         //상품명
    @SerializedName("ItemOrder")
    private int ItemOrder;           //상품순번
    @SerializedName("ItemVisitPrice")
    private int ItemVisitPrice;      //방문품목가격(일반)
    @SerializedName("ItemVisitPrice2")
    private int ItemVisitPrice2;     //방문품목가격(명품)
    @SerializedName("ItemVisitPrice3")
    private int ItemVisitPrice3;     //방문품목가격(아동)
    @SerializedName("ItemPickupPrice")
    private int ItemPickupPrice;     //수거품목가격(일반)
    @SerializedName("ItemPickupPrice2")
    private int ItemPickupPrice2;    //수거품목가격(명품)
    @SerializedName("ItemPickupPrice3")
    private int ItemPickupPrice3;    //수거품목가격(아동)
    @SerializedName("IsUse")
    private String IsUse;            //사용여부
    @SerializedName("RegDate")
    private String RegDate;          //등록일
    @SerializedName("UpdDate")
    private String UpdDate;          //수정일


    public int getStoreID() {
        return StoreID;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public int getClassID() {
        return ClassID;
    }

    public String getClassName() {
        return ClassName;
    }

    public int getStoreItemID() {
        return StoreItemID;
    }

    public String getItemName() {
        return ItemName;
    }

    public int getItemOrder() {
        return ItemOrder;
    }

    public int getItemType() {
        return ItemType;
    }

    public int getItemVisitPrice() {
        return ItemVisitPrice;
    }

    public int getItemVisitPrice2() {
        return ItemVisitPrice2;
    }

    public int getItemVisitPrice3() {
        return ItemVisitPrice3;
    }

    public int getItemPickupPrice() {
        return ItemPickupPrice;
    }

    public int getItemPickupPrice2() {
        return ItemPickupPrice2;
    }

    public int getItemPickupPrice3() {
        return ItemPickupPrice3;
    }

    public String getIsUse() {
        return IsUse;
    }

    public String getRegDate() {
        return RegDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }
}
