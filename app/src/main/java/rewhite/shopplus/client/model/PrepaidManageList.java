package rewhite.shopplus.client.model;

import java.util.ArrayList;

public class PrepaidManageList {
    private int PaymentID;             //결제ID
    private int PaymentStatus;         //결제 상태
    private String PaymentStatusDesc;  //승인구분
    private String PaymentDate;        //결제/취소일자
    private int PaymentPrice;          //결제(취소)금액
    private int ChargePrice;           //충전(취소)금액
    private ArrayList<String> Badges;  //결제 정보 뱃지
    private ArrayList<PaymentDetail> PaymentDetails;  //결제 상세 정보
    private String UserType;           //회원 타입(S,SR)
    private int StoreUserID;           //가맹점 회원ID
    private int StoreID;               //가맹점ID
    private int UserID;                //회원ID
    private String UserName;           //회원명
    private String MembershipCardNo;   //멤버쉽카드번호
    private String LinkDate;           //멤버십 연결일
    private String StoreUserGrade;     //가맹점 회원 등급(1,2,3)
    private String _UserAddress;       //주소1(읽기전용,복호화)
    private String _UserPhone;         //가맹점 회원 휴대폰 번호(읽기전용, 복호화)
    private String PhoneSearch;        //검색용 휴대폰 뒷자리
    private String Memo;               //메모
    private String Gender;             //성별
    private String BirthDay;           //생일(yyyy-MM-dd)
    private String Anniversary;        //기념일(MM-dd)
    private int StoreUserStatus;       //가맹점 회원 상태(1:정상, 9:탈퇴)
    private String _StoreUserStatus;   //가맹점 회원 상태
    private String IsNoticeAgree;      //정보성 알림 수신 동의 여부
    private String IsPromoteAgree;     //광고성 알림 수신 동의여부
    private int TotalPrePaid;          //사용가능한 선충전금 총합
    private int RealPrePaid;           //사용가능한 선충전금
    private int BonusPrePaid;          //사용가능한 추가적립(보너스) 선충전금
    private int Mileage;               //마일리지
    private int ExMileage;             //적립 예정된 마일리지
    private String RecentOrderDate;    //최근 주문일
    private int TotalOrderCount;       //전체 주문 수
    private int TotalOrderPrice;       //누적주문금액
    private int TotalPayPrice;         //누적결제금액
    private int NonPayPrice;           //미수금
    private int NotOutOrderCount;      //미출고 주문 수
    private int OutOrderCount;         //출고완료 주문 수
    private int WashFinOrderCount;     //세탁완료 주문 수
    private int OutNonPayOrderCount;   //출고 미수 주문 수
    private int OutNonPayPrice;        //출고 미수금
    private int NotOutNonPayOrderCount; //미출고 미수 주문수
    private String NotOutNonPayPrice;  //미출고 미수금
    private String RegDate;            //등록일
    private String UpdDate;            //수정일

    private class PaymentDetail {
        private int PaymemtDetailID;    //결제상세ID
        private int PaymentID;          //결제ID
        private int PaymentType;        //결제타입(1:현금,2:카드,3:선충전금,4:마일리지,5:쿠폰,10:온라인)
        private String PaymentTypeDesc; //결제 타입 명
        private String IsReceipt;       //영수증발급여부(Y/
        private int PGID;               //PG ID
        private String TID;             //승인번호
        private String CATInfo;         //카드 단말기 정보
        private String CardName;        //결제 카드명
        private String CardNo;          //카드번호
        private int CardDivide;         //할부개월수
        private String PayYMD;          //원거래일자
        private int Price;              //결제금액
        private String RegDate;         //등록일
        private String UpdDate;         //수정일
    }
}