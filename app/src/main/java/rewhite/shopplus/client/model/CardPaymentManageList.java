package rewhite.shopplus.client.model;

public class CardPaymentManageList {

    private String PaymentID;               //결제ID
    private String PaymentStatus;           //결제 상태
    private String PaymentStatusDesc;       //승인구분
    private String PaymentDate;             //결제일자(승인일자)
    private String CardName;                //카드사명
    private String PaymentPrice;            //결제금액
    private String UserType;                //회원 타입(S,SR)
    private String StoreUserID;             //가맹점 회원ID
    private String StoreID;                 //가맹점ID
    private String UserID;                  //회원ID
    private String UserName;                //회원명
    private String MembershipCardNo;        //멤버쉽카드번호
    private String LinkDate;                //멤버십 연결일
    private String StoreUserGrade;          //가맹점 회원 등급(1,2,3)
    private String _UserAddress;            //주소1(읽기전용,복호화)
    private String _UserPhone;              //가맹점 회원 휴대폰 번호(읽기전용, 복호화)
    private String PhoneSearch;             //검색용 휴대폰 뒷자리
    private String Memo;                    //메모
    private String Gender;                  //성별
    private String BirthDay;                //생일(yyyy-MM-dd)
    private String Anniversary;             //기념일(MM-dd)
    private String StoreUserStatus;         //가맹점 회원 상태(1:정상, 9:탈퇴)
    private String _StoreUserStatus;        //가맹점 회원 상태
    private String IsNoticeAgree;           //정보성 알림 수신 동의 여부
    private String IsPromoteAgree;          //광고성 알림 수신 동의여부
    private String TotalPrePaid;            //사용가능한 선충전금 총합
    private String RealPrePaid;             //사용가능한 선충전금
    private String BonusPrePaid;            //사용가능한 추가적립(보너스) 선충전금
    private String Mileage;                 //마일리지
    private String ExMileage;               //적립 예정된 마일리지
    private String RecentOrderDate;         //최근 주문일
    private String TotalOrderCount;         //전체 주문 수
    private String TotalOrderPrice;         //누적주문금액
    private String TotalPayPrice;           //누적결제금액
    private String NonPayPrice;             //미수금
    private String NotOutOrderCount;        //미출고 주문 수
    private String OutOrderCount;           //출고완료 주문 수
    private String WashFinOrderCount;       //세탁완료 주문 수
    private String OutNonPayOrderCount;     //출고 미수 주문 수
    private String OutNonPayPrice;          //출고 미수금
    private String NotOutNonPayOrderCount;  //미출고 미수 주문수
    private String NotOutNonPayPrice;       //미출고 미수금
    private String RegDate;                 //등록일
    private String UpdDate;                 //수정일

    public String getPaymentID() {
        return PaymentID;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public String getPaymentStatusDesc() {
        return PaymentStatusDesc;
    }

    public String getPaymentDate() {
        return PaymentDate;
    }

    public String getCardName() {
        return CardName;
    }

    public String getPaymentPrice() {
        return PaymentPrice;
    }

    public String getUserType() {
        return UserType;
    }

    public String getStoreUserID() {
        return StoreUserID;
    }

    public String getStoreID() {
        return StoreID;
    }

    public String getUserID() {
        return UserID;
    }

    public String getUserName() {
        return UserName;
    }

    public String getMembershipCardNo() {
        return MembershipCardNo;
    }

    public String getLinkDate() {
        return LinkDate;
    }

    public String getStoreUserGrade() {
        return StoreUserGrade;
    }

    public String get_UserAddress() {
        return _UserAddress;
    }

    public String get_UserPhone() {
        return _UserPhone;
    }

    public String getPhoneSearch() {
        return PhoneSearch;
    }

    public String getMemo() {
        return Memo;
    }

    public String getGender() {
        return Gender;
    }

    public String getBirthDay() {
        return BirthDay;
    }

    public String getAnniversary() {
        return Anniversary;
    }

    public String getStoreUserStatus() {
        return StoreUserStatus;
    }

    public String get_StoreUserStatus() {
        return _StoreUserStatus;
    }

    public String getIsNoticeAgree() {
        return IsNoticeAgree;
    }

    public String getIsPromoteAgree() {
        return IsPromoteAgree;
    }

    public String getTotalPrePaid() {
        return TotalPrePaid;
    }

    public String getRealPrePaid() {
        return RealPrePaid;
    }

    public String getBonusPrePaid() {
        return BonusPrePaid;
    }

    public String getMileage() {
        return Mileage;
    }

    public String getExMileage() {
        return ExMileage;
    }

    public String getRecentOrderDate() {
        return RecentOrderDate;
    }

    public String getTotalOrderCount() {
        return TotalOrderCount;
    }

    public String getTotalOrderPrice() {
        return TotalOrderPrice;
    }

    public String getTotalPayPrice() {
        return TotalPayPrice;
    }

    public String getNonPayPrice() {
        return NonPayPrice;
    }

    public String getNotOutOrderCount() {
        return NotOutOrderCount;
    }

    public String getOutOrderCount() {
        return OutOrderCount;
    }

    public String getWashFinOrderCount() {
        return WashFinOrderCount;
    }

    public String getOutNonPayOrderCount() {
        return OutNonPayOrderCount;
    }

    public String getOutNonPayPrice() {
        return OutNonPayPrice;
    }

    public String getNotOutNonPayOrderCount() {
        return NotOutNonPayOrderCount;
    }

    public String getNotOutNonPayPrice() {
        return NotOutNonPayPrice;
    }

    public String getRegDate() {
        return RegDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }
}
