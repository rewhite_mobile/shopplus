package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

public class GetMainSalesInfo {
    private static final String TAG = "GetMainSalesInfo";

    @SerializedName("Data")
    GetMainSalesInfo.Data Data;

    public GetMainSalesInfo.Data getData() {
        return Data;
    }

    public static class Data {
        @SerializedName("Today_EnterCount")
        private int Today_EnterCount;     //오늘 접수건 수
        @SerializedName("Today_OutCount")
        private int Today_OutCount;       //오늘 출고건 수
        @SerializedName("Today_Sales")
        private int Today_Sales;          //오늘 매출
        @SerializedName("YesterDay_EnterCount")
        private int YesterDay_EnterCount; //어제 접수건 수
        @SerializedName("YesterDay_OutCount")
        private int YesterDay_OutCount;   //어제 출고건 수
        @SerializedName("YesterDay_Sales")
        private int YesterDay_Sales;      //어제 매출
        @SerializedName("Month_Sales")
        private int Month_Sales;          //월 매출
        @SerializedName("Day_Average_Sales")
        private double Day_Average_Sales;    //일 평균 매출


        public int getToday_EnterCount() {
            return Today_EnterCount;
        }

        public int getToday_OutCount() {
            return Today_OutCount;
        }

        public int getToday_Sales() {
            return Today_Sales;
        }

        public int getYesterDay_EnterCount() {
            return YesterDay_EnterCount;
        }

        public int getYesterDay_OutCount() {
            return YesterDay_OutCount;
        }

        public int getYesterDay_Sales() {
            return YesterDay_Sales;
        }

        public int getMonth_Sales() {
            return Month_Sales;
        }

        public double getDay_Average_Sales() {
            return Day_Average_Sales;
        }
    }
}
