package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * 일별 매출현황(기본기간:이번달) ItemClass
 */
public class GetDaySalesStatList {
    private static final String TAG = "GetDaySalesStatList";

    @SerializedName("Data")
    GetDaySalesStatList.Data Data;

    public GetDaySalesStatList.Data getData() {
        return Data;
    }

    public static class Data {
        @SerializedName("PeriodType")
        private int PeriodType;              //기간 타입(1:일, 2:월)
        @SerializedName("PeriodTypeDesc")
        private String PeriodTypeDesc;       //기간 타입 명
        @SerializedName("StartDate")
        private String StartDate;            //조회 시작일
        @SerializedName("EndDate")
        private String EndDate;              //조회 종료일
        @SerializedName("TotalEnterCount")
        private int TotalEnterCount;         //총 접수(입고) 수
        @SerializedName("TotalOutCount")
        private int TotalOutCount;           //총 출고 수
        @SerializedName("TotalSale")
        private int TotalSale;               //총 매출액
        @SerializedName("DayAverageSale")
        private int DayAverageSale;          //일 평균 매출액
        @SerializedName("MonthAverageSale")
        private int MonthAverageSale;        //월 평균 매출액
        @SerializedName("Record")
        private ArrayList<SalesStat> Record; //레코드

        public int getPeriodType() {
            return PeriodType;
        }

        public String getPeriodTypeDesc() {
            return PeriodTypeDesc;
        }

        public String getStartDate() {
            return StartDate;
        }

        public String getEndDate() {
            return EndDate;
        }

        public int getTotalEnterCount() {
            return TotalEnterCount;
        }

        public int getTotalOutCount() {
            return TotalOutCount;
        }

        public int getTotalSale() {
            return TotalSale;
        }

        public int getDayAverageSale() {
            return DayAverageSale;
        }

        public int getMonthAverageSale() {
            return MonthAverageSale;
        }

        public ArrayList<SalesStat> getRecord() {
            return Record;
        }
    }

    public static class SalesStat {
        @SerializedName("Base_Date")
        private String Base_Date;    //기준일
        @SerializedName("Enter_Count")
        private int Enter_Count;     //접수(입고) 수
        @SerializedName("Out_Count")
        private int Out_Count;       //출고 수
        @SerializedName("Normal_Count")
        private int Normal_Count;    //정상 결제건 수
        @SerializedName("Cancel_Count")
        private int Cancel_Count;    //결제 취소건 수
        @SerializedName("Total_Sales")
        private int Total_Sales;     //총 매출
        @SerializedName("Cash_Sales")
        private int Cash_Sales;      //현금 매출
        @SerializedName("Card_Sales")
        private int Card_Sales;      //카드 매출
        @SerializedName("Prepaid_Sales")
        private int Prepaid_Sales;   //선충전금 매출

        public String getBase_Date() {
            return Base_Date;
        }

        public int getEnter_Count() {
            return Enter_Count;
        }

        public int getOut_Count() {
            return Out_Count;
        }

        public int getNormal_Count() {
            return Normal_Count;
        }

        public int getCancel_Count() {
            return Cancel_Count;
        }

        public int getTotal_Sales() {
            return Total_Sales;
        }

        public int getCash_Sales() {
            return Cash_Sales;
        }

        public int getCard_Sales() {
            return Card_Sales;
        }

        public int getPrepaid_Sales() {
            return Prepaid_Sales;
        }
    }
}