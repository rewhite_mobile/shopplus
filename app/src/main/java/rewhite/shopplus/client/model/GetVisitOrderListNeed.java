package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetVisitOrderListNeed {

    @SerializedName("UserID")
    private int UserID;                         //회원ID
    @SerializedName("UserName")
    private String UserName;                    //회원명
    @SerializedName("_UserAddress")
    private String _UserAddress;//주소1(읽기전용,복호화)
    @SerializedName("_UserPhone")
    private String _UserPhone;//가맹점 회원 휴대폰 번호(읽기전용, 복호화)
    @SerializedName("OrderID")
    private int orderId;                        //주문 ID
    @SerializedName("OrderItemID")
    private int orderItemId;                    ///주문상품 ID
    @SerializedName("TagNo")
    private String tagNo;                       //Tag번호
    @SerializedName("TagColor")
    private String tagColor;                    //Tag색상
    @SerializedName("StoreItemID")
    private int storeItemId;                    //가맹점 상품 ID
    @SerializedName("StoreItemName")
    private String storeItemName;               //가맹점 상품명
    @SerializedName("ItemGrade")
    private int itemGrade;                      //상품등급(1:일반,2:명품,3:아동)
    @SerializedName("ItemGradeDesc")
    private String itemGradeDesc;               //상품등급명
    @SerializedName("StoreItemPrice")
    private int storeItemPrice;                 //삼품 금액
    @SerializedName("EnterDate")
    private String enterDate;                   //입고일
    @SerializedName("WashFinWillDate")
    private String WashFinWillDate;             //세탁완료예정일
    @SerializedName("WashFinDate")
    private String washFinDate;                 //세탁완료일
    @SerializedName("OutDate")
    private String outDate;                     //출고일
    @SerializedName("IsOutside")
    private String IsOutside;                   //외부세탁여부
    @SerializedName("PickupID")
    private int pickupID;                       //수거 ID
    @SerializedName("DeliveryID")
    private int deliveryID;                     //배송 ID
    @SerializedName("PhotoURL")
    private String PhotoUrl;                    //첨부사진 (구분자'|') ,
    @SerializedName("WashType")
    private int washType;                       //세탁방법 : 1:드라이+다림질, 2:물세탁+다림질, 3:다림질
    @SerializedName("WashTypeDesc")
    private String washTypeDesc;                //세탁방법명
    @SerializedName("FreeType")
    private int freeType;                       //기타옵션 1:무료서비스, 2:재세탁, 3:세탁진행중단
    @SerializedName("FreeTypeDesc")
    private String freeTypeDesc;                //기타옵션명
    @SerializedName("ItemColor")
    private String itemColor;                   //색상
    @SerializedName("OrderItemPrice")
    private int orderItemPrice;                 //주문금액
    @SerializedName("OrderItemMemo")
    private String orderItemMemo;               //메모(구문자|)
    @SerializedName("OrderItemStatus")
    private int orderItemStatus;                //상태
    @SerializedName("OrderItemStatusDesc")
    private String OrderItemStatusDesc;         //상태명
    @SerializedName("Badges")
    private ArrayList<String> badges;           //주문 정보 뱃지
    @SerializedName("RegDate")
    private String regDate;                     //등록일 (접수일)
    @SerializedName("UpdDate")
    private String updDate;                     //수정일
    @SerializedName("Options")
    private ArrayList<OrderItemOption> options; //주문상품옵션
    @SerializedName("PayPrice")
    private int payPrice;                       //기결제금액
    @SerializedName("NonPayPrice")
    private int nonPayPrice;                    //미결제금액(미수금)


    public int getUserID() {
        return UserID;
    }

    public String getUserName() {
        return UserName;
    }

    public String get_UserAddress() {
        return _UserAddress;
    }

    public String get_UserPhone() {
        return _UserPhone;
    }

    public String getWashFinWillDate() {
        return WashFinWillDate;
    }

    public String getOrderItemStatusDesc() {
        return OrderItemStatusDesc;
    }

    public ArrayList<String> getBadges() {
        return badges;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getTagNo() {
        return tagNo;
    }

    public void setTagNo(String tagNo) {
        this.tagNo = tagNo;
    }

    public String getTagColor() {
        return tagColor;
    }

    public void setTagColor(String tagColor) {
        this.tagColor = tagColor;
    }

    public int getStoreItemId() {
        return storeItemId;
    }

    public void setStoreItemId(int storeItemId) {
        this.storeItemId = storeItemId;
    }

    public String getStoreItemName() {
        return storeItemName;
    }

    public void setStoreItemName(String storeItemName) {
        this.storeItemName = storeItemName;
    }

    public int getItemGrade() {
        return itemGrade;
    }

    public void setItemGrade(int itemGrade) {
        this.itemGrade = itemGrade;
    }

    public String getItemGradeDesc() {
        return itemGradeDesc;
    }

    public void setItemGradeDesc(String itemGradeDesc) {
        this.itemGradeDesc = itemGradeDesc;
    }

    public int getStoreItemPrice() {
        return storeItemPrice;
    }

    public void setStoreItemPrice(int storeItemPrice) {
        this.storeItemPrice = storeItemPrice;
    }

    public String getEnterDate() {
        return enterDate;
    }

    public void setEnterDate(String enterDate) {
        this.enterDate = enterDate;
    }

    public String getWashFinDate() {
        return washFinDate;
    }

    public void setWashFinDate(String washFinDate) {
        this.washFinDate = washFinDate;
    }

    public String getOutDate() {
        return outDate;
    }

    public void setOutDate(String outDate) {
        this.outDate = outDate;
    }

    public String getIsOutside() {
        return IsOutside;
    }

    public void setIsOutside(String isOutside) {
        IsOutside = isOutside;
    }

    public int getPickupID() {
        return pickupID;
    }

    public void setPickupID(int pickupID) {
        this.pickupID = pickupID;
    }

    public int getDeliveryID() {
        return deliveryID;
    }

    public void setDeliveryID(int deliveryID) {
        this.deliveryID = deliveryID;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        PhotoUrl = photoUrl;
    }

    public int getWashType() {
        return washType;
    }

    public void setWashType(int washType) {
        this.washType = washType;
    }

    public String getWashTypeDesc() {
        return washTypeDesc;
    }

    public void setWashTypeDesc(String washTypeDesc) {
        this.washTypeDesc = washTypeDesc;
    }

    public int getFreeType() {
        return freeType;
    }

    public void setFreeType(int freeType) {
        this.freeType = freeType;
    }

    public String getFreeTypeDesc() {
        return freeTypeDesc;
    }

    public void setFreeTypeDesc(String freeTypeDesc) {
        this.freeTypeDesc = freeTypeDesc;
    }

    public String getItemColor() {
        return itemColor;
    }

    public void setItemColor(String itemColor) {
        this.itemColor = itemColor;
    }

    public int getOrderItemPrice() {
        return orderItemPrice;
    }

    public void setOrderItemPrice(int orderItemPrice) {
        this.orderItemPrice = orderItemPrice;
    }

    public String getOrderItemMemo() {
        return orderItemMemo;
    }

    public void setOrderItemMemo(String orderItemMemo) {
        this.orderItemMemo = orderItemMemo;
    }

    public int getOrderItemStatus() {
        return orderItemStatus;
    }

    public void setOrderItemStatus(int orderItemStatus) {
        this.orderItemStatus = orderItemStatus;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public ArrayList<OrderItemOption> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<OrderItemOption> options) {
        this.options = options;
    }

    public int getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(int payPrice) {
        this.payPrice = payPrice;
    }

    public int getNonPayPrice() {
        return nonPayPrice;
    }

    public void setNonPayPrice(int nonPayPrice) {
        this.nonPayPrice = nonPayPrice;
    }
    public class OrderItemOption {

        @SerializedName("OrderItemID")
        private int OrderItemID;         //(주문상품ID
        @SerializedName("OptionType")
        private int OptionType;          //옵션 상품 타입:추가기술, 부속품, 수선
        @SerializedName("OptionTypeDesc")
        private String OptionTypeDesc;   //옵션타입명
        @SerializedName("OptionItemID")
        private int OptionItemID;        //옵션 상품ID
        @SerializedName("OptionItemName")
        private String OptionItemName;   //옵션상품명
        @SerializedName("OptionItemGrade")
        private int OptionItemGrade;     //옵션 상품 등급
        @SerializedName("OptionPrice")
        private int OptionPrice;         //옵션금액
        @SerializedName("IsUse")
        private String IsUse;            //사용여부(Y,N)
        @SerializedName("RegDate")
        private String RegDate;          //등록일
        @SerializedName("UpdDate")
        private String UpdDate;          //수정일


        public int getOrderItemID() {
            return OrderItemID;
        }

        public void setOrderItemID(int orderItemID) {
            OrderItemID = orderItemID;
        }

        public int getOptionType() {
            return OptionType;
        }

        public void setOptionType(int optionType) {
            OptionType = optionType;
        }

        public String getOptionTypeDesc() {
            return OptionTypeDesc;
        }

        public void setOptionTypeDesc(String optionTypeDesc) {
            OptionTypeDesc = optionTypeDesc;
        }

        public int getOptionItemID() {
            return OptionItemID;
        }

        public void setOptionItemID(int optionItemID) {
            OptionItemID = optionItemID;
        }

        public String getOptionItemName() {
            return OptionItemName;
        }

        public void setOptionItemName(String optionItemName) {
            OptionItemName = optionItemName;
        }

        public int getOptionItemGrade() {
            return OptionItemGrade;
        }

        public void setOptionItemGrade(int optionItemGrade) {
            OptionItemGrade = optionItemGrade;
        }

        public int getOptionPrice() {
            return OptionPrice;
        }

        public void setOptionPrice(int optionPrice) {
            OptionPrice = optionPrice;
        }

        public String getIsUse() {
            return IsUse;
        }

        public void setIsUse(String isUse) {
            IsUse = isUse;
        }

        public String getRegDate() {
            return RegDate;
        }

        public void setRegDate(String regDate) {
            RegDate = regDate;
        }

        public String getUpdDate() {
            return UpdDate;
        }

        public void setUpdDate(String updDate) {
            UpdDate = updDate;
        }
    }

}
