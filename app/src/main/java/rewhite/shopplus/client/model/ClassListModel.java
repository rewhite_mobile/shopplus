package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

/**
 * 품목 중분류 리스트
 */
public class ClassListModel {

    @SerializedName("ClassID")
    private int ClassID; //상품 클래스ID
    @SerializedName("CategoryID")
    private int CategoryID; //카테고리ID
    @SerializedName("ClassName")
    private String ClassName; //클래스명
    @SerializedName("ClassOrder")
    private int ClassOrder; //클래스순번
    @SerializedName("IsUse")
    private String IsUse; //사용여부
    @SerializedName("RegDate")
    private String RegDate; //등록일
    @SerializedName("UpdDate")
    private String UpdDate; //수정일


    public int getClassID() {
        return ClassID;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public String getClassName() {
        return ClassName;
    }

    public int getClassOrder() {
        return ClassOrder;
    }

    public String getIsUse() {
        return IsUse;
    }

    public String getRegDate() {
        return RegDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }
}
