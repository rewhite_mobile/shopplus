package rewhite.shopplus.client.model;

/**
 * 방문접수-전체 data class
 */
public class MemberAllVisitsItemList {

    private String selectNumber;       //택번호
    private String item;               //품목
    private String Detail;             //상세내용
    private String Receipt;            //접수일
    private String release;            //출고일
    private String condition;          //상태
    private String Price;              //금액
    private String Receivables;        //미수금
    private String BillingInformation; //결제정보

    public String getSelectNumber() {
        return selectNumber;
    }

    public void setSelectNumber(String selectNumber) {
        this.selectNumber = selectNumber;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public String getReceipt() {
        return Receipt;
    }

    public void setReceipt(String receipt) {
        Receipt = receipt;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getReceivables() {
        return Receivables;
    }

    public void setReceivables(String receivables) {
        Receivables = receivables;
    }

    public String getBillingInformation() {
        return BillingInformation;
    }

    public void setBillingInformation(String billingInformation) {
        BillingInformation = billingInformation;
    }
}
