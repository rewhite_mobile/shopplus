package rewhite.shopplus.client.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * 취소 가능한 주문 품목 조회(for 품목취소) item
 */
public class CancelAvailOrder {

    @SerializedName("OrderID")
    private int OrderID;            //주문ID
    @SerializedName("OrderItemID")
    private int OrderItemID;        //주문상품ID
    @SerializedName("TagNo")
    private String TagNo;           //Tag번호
    @SerializedName("TagColor")
    private String TagColor;        //Tag 컬러
    @SerializedName("StoreItemID")
    private int StoreItemID;        //가맹점 상품ID
    @SerializedName("StoreItemName")
    private String StoreItemName;   //가맹점 상품명
    @SerializedName("ItemGrade")
    private int ItemGrade;          //상품등급(1:일반,2:명품,3:아동)
    @SerializedName("ItemGradeDesc")
    private String ItemGradeDesc;   //상풍등급명
    @SerializedName("StoreItemPrice")
    private int StoreItemPrice;     //상품 금액
    @SerializedName("EnterDate")
    private String EnterDate;       //입고일
    @SerializedName("WashFinWillDate")
    private String WashFinWillDate; //세탁완료예정일
    @SerializedName("WashFinDate")
    private String WashFinDate;     //세탁완료일
    @SerializedName("OutDate")
    private String OutDate;         //출고일
    @SerializedName("IsOutside")
    private String IsOutside;       //외부세탁여부
    @SerializedName("IsDelivery")
    private String IsDelivery;      //배달여부
    @SerializedName("PickupID")
    private int PickupID;           //수거ID
    @SerializedName("DeliveryID")
    private int DeliveryID;         //배송ID
    @SerializedName("PhotoURL")
    private String PhotoURL;        //첨부사진 (구분자'|')
    @SerializedName("WashType")
    private int WashType;           //세탁방법 : 1:드라이+다림질, 2:물세탁+다림질, 3:다림질
    @SerializedName("WashTypeDesc")
    private String WashTypeDesc;    //세탁방법명
    @SerializedName("FreeType")
    private int FreeType;           //기타옵션 : 1:무료서비스, 2:재세탁, 3:세탁진행중단
    @SerializedName("FreeTypeDesc")
    private String FreeTypeDesc;    //기타옵션명
    @SerializedName("ItemColor")
    private String ItemColor;       //색상(#FFFFFF)
    @SerializedName("OrderItemPrice")
    private int OrderItemPrice;     //주문 금액
    @SerializedName("OrderItemMemo")
    private String OrderItemMemo;   //메모(구분자'|')
    @SerializedName("OrderItemStatus")
    private int OrderItemStatus;    //상태
    @SerializedName("OrderItemStatusDesc")
    private String OrderItemStatusDesc; //상태명
    @SerializedName("RegDate")
    private String RegDate;             //등록일
    @SerializedName("UpdDate")
    private String UpdDate;             //수정일
    @SerializedName("Badges")
    private ArrayList<String> Badges;   //주문 정보 뱃지
    @SerializedName("PayPrice")
    private int PayPrice;               //기결제금액
    @SerializedName("NonPayPrice")
    private int NonPayPrice;                    //미결제금액(미수금)
    @SerializedName("Options")
    private ArrayList<OrderItemOption> Options; //주문상품옵션

    public class OrderItemOption {
        @SerializedName("OrderItemID")
        private int OrderItemID;        //주문상품ID
        @SerializedName("OptionType")
        private int OptionType;         //옵션 상품 타입:추가기술, 부속품, 수선
        @SerializedName("OptionTypeDesc")
        private String OptionTypeDesc;  //옵션타입명
        @SerializedName("OptionItemID")
        private int OptionItemID;       //옵션 상품ID
        @SerializedName("OptionItemName")
        private String OptionItemName;  //옵션상품명
        @SerializedName("OptionItemGrade")
        private int OptionItemGrade;    //옵션 상품 등급
        @SerializedName("OptionPrice")
        private int OptionPrice;        //옵션금액
        @SerializedName("IsUse")
        private String IsUse;           //사용여부(Y,N)
        @SerializedName("RegDate")
        private String RegDate;         //등록일
        @SerializedName("UpdDate")
        private String UpdDate;         //수정일
    }

    public ArrayList<String> getBadges() {
        return Badges;
    }

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public int getOrderItemID() {
        return OrderItemID;
    }

    public void setOrderItemID(int orderItemID) {
        OrderItemID = orderItemID;
    }

    public String getTagNo() {
        return TagNo;
    }

    public void setTagNo(String tagNo) {
        TagNo = tagNo;
    }

    public String getTagColor() {
        return TagColor;
    }

    public void setTagColor(String tagColor) {
        TagColor = tagColor;
    }

    public int getStoreItemID() {
        return StoreItemID;
    }

    public void setStoreItemID(int storeItemID) {
        StoreItemID = storeItemID;
    }

    public String getStoreItemName() {
        return StoreItemName;
    }

    public void setStoreItemName(String storeItemName) {
        StoreItemName = storeItemName;
    }

    public int getItemGrade() {
        return ItemGrade;
    }

    public void setItemGrade(int itemGrade) {
        ItemGrade = itemGrade;
    }

    public String getItemGradeDesc() {
        return ItemGradeDesc;
    }

    public void setItemGradeDesc(String itemGradeDesc) {
        ItemGradeDesc = itemGradeDesc;
    }

    public int getStoreItemPrice() {
        return StoreItemPrice;
    }

    public void setStoreItemPrice(int storeItemPrice) {
        StoreItemPrice = storeItemPrice;
    }

    public String getEnterDate() {
        return EnterDate;
    }

    public void setEnterDate(String enterDate) {
        EnterDate = enterDate;
    }

    public String getWashFinWillDate() {
        return WashFinWillDate;
    }

    public void setWashFinWillDate(String washFinWillDate) {
        WashFinWillDate = washFinWillDate;
    }

    public String getWashFinDate() {
        return WashFinDate;
    }

    public void setWashFinDate(String washFinDate) {
        WashFinDate = washFinDate;
    }

    public String getOutDate() {
        return OutDate;
    }

    public void setOutDate(String outDate) {
        OutDate = outDate;
    }

    public String getIsOutside() {
        return IsOutside;
    }

    public void setIsOutside(String isOutside) {
        IsOutside = isOutside;
    }

    public String getIsDelivery() {
        return IsDelivery;
    }

    public void setIsDelivery(String isDelivery) {
        IsDelivery = isDelivery;
    }

    public int getPickupID() {
        return PickupID;
    }

    public void setPickupID(int pickupID) {
        PickupID = pickupID;
    }

    public int getDeliveryID() {
        return DeliveryID;
    }

    public void setDeliveryID(int deliveryID) {
        DeliveryID = deliveryID;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public int getWashType() {
        return WashType;
    }

    public void setWashType(int washType) {
        WashType = washType;
    }

    public String getWashTypeDesc() {
        return WashTypeDesc;
    }

    public void setWashTypeDesc(String washTypeDesc) {
        WashTypeDesc = washTypeDesc;
    }

    public int getFreeType() {
        return FreeType;
    }

    public void setFreeType(int freeType) {
        FreeType = freeType;
    }

    public String getFreeTypeDesc() {
        return FreeTypeDesc;
    }

    public void setFreeTypeDesc(String freeTypeDesc) {
        FreeTypeDesc = freeTypeDesc;
    }

    public String getItemColor() {
        return ItemColor;
    }

    public void setItemColor(String itemColor) {
        ItemColor = itemColor;
    }

    public int getOrderItemPrice() {
        return OrderItemPrice;
    }

    public void setOrderItemPrice(int orderItemPrice) {
        OrderItemPrice = orderItemPrice;
    }

    public String getOrderItemMemo() {
        return OrderItemMemo;
    }

    public void setOrderItemMemo(String orderItemMemo) {
        OrderItemMemo = orderItemMemo;
    }

    public int getOrderItemStatus() {
        return OrderItemStatus;
    }

    public void setOrderItemStatus(int orderItemStatus) {
        OrderItemStatus = orderItemStatus;
    }

    public String getOrderItemStatusDesc() {
        return OrderItemStatusDesc;
    }

    public void setOrderItemStatusDesc(String orderItemStatusDesc) {
        OrderItemStatusDesc = orderItemStatusDesc;
    }

    public String getRegDate() {
        return RegDate;
    }

    public void setRegDate(String regDate) {
        RegDate = regDate;
    }

    public String getUpdDate() {
        return UpdDate;
    }

    public void setUpdDate(String updDate) {
        UpdDate = updDate;
    }

    public ArrayList<OrderItemOption> getOptions() {
        return Options;
    }

    public void setOptions(ArrayList<OrderItemOption> options) {
        Options = options;
    }

    public int getPayPrice() {
        return PayPrice;
    }

    public void setPayPrice(int payPrice) {
        PayPrice = payPrice;
    }

    public int getNonPayPrice() {
        return NonPayPrice;
    }

    public void setNonPayPrice(int nonPayPrice) {
        NonPayPrice = nonPayPrice;
    }
}
