package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemImageData;

public class ManagerImageData {
    private static ArrayList<ItemImageData> itemImageData = new ArrayList<>();

    public void setImageData(ArrayList<ItemImageData> dates) {
        itemImageData = dates;
    }

    public ArrayList<ItemImageData> getImageData() {
        return itemImageData;
    }

    private static ManagerImageData mInstance = null;

    public static ManagerImageData getInstance() {
        if (mInstance == null) {
            synchronized (ManagerImageData.class) {
                if (mInstance == null) {
                    mInstance = new ManagerImageData();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
