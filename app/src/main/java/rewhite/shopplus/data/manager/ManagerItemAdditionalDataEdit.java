package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemAdditionalDataEdit;

/**
 * 품목수정 리스트 관리 ManagerClass
 */
public class ManagerItemAdditionalDataEdit {
    private static ArrayList<ItemAdditionalDataEdit> itemAdditionalData = new ArrayList<>();

    public void setItemAdditionalData(ArrayList<ItemAdditionalDataEdit> dates) {
        itemAdditionalData = dates;
    }

    public ArrayList<ItemAdditionalDataEdit> getItemAdditionalData() {
        return itemAdditionalData;
    }

    public void setReairFreeDelete(){
        ArrayList<ItemAdditionalDataEdit> itemRepairFree = ManagerItemAdditionalDataEdit.getInstance().getItemAdditionalData();
        itemRepairFree.clear();
    }

    private static ManagerItemAdditionalDataEdit mInstance = null;

    public static ManagerItemAdditionalDataEdit getInstance() {
        if (mInstance == null) {
            synchronized (ManagerMemo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerItemAdditionalDataEdit();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
