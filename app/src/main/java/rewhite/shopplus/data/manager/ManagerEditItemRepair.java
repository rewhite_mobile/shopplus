package rewhite.shopplus.data.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.data.dto.ItemEditItemRepair;

public class ManagerEditItemRepair {
    private static final String TAG = "ManagerEditItemRepair";

    @SerializedName("Data")
    public List<ItemEditItemRepair> managerItemRepair = new ArrayList<>();

    public List<ItemEditItemRepair> getManagerItemRepair() {
        return managerItemRepair;
    }

    public void setManagerItemRepair(List<ItemEditItemRepair> itemRepair) {
        this.managerItemRepair = itemRepair;
    }

    private static ManagerEditItemRepair mInstance = null;

    public static ManagerEditItemRepair getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerEditItemRepair.class) {
                if (mInstance == null) {
                    mInstance = new ManagerEditItemRepair();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
