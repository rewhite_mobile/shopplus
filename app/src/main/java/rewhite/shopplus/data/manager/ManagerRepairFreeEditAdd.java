package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemRepairFree;

/**
 * 수선요금 수정
 */
public class ManagerRepairFreeEditAdd {
    private static ArrayList<ItemRepairFree> newsStandDates = new ArrayList<>();

    public void setRepairFeeDates(ArrayList<ItemRepairFree> dates) {
        newsStandDates = dates;
    }

    public ArrayList<ItemRepairFree> getRepairFreeDates() {
        return newsStandDates;
    }

    public void setReairFreeDelete(){
        ArrayList<ItemRepairFree> itemRepairFree = ManagerRepairFreeEditAdd.getInstance().getRepairFreeDates();
        itemRepairFree.clear();
    }


    private static ManagerRepairFreeEditAdd mInstance = null;

    public static ManagerRepairFreeEditAdd getInstance() {
        if (mInstance == null) {
            synchronized (ManagerRepairFreeEditAdd.class) {
                if (mInstance == null) {
                    mInstance = new ManagerRepairFreeEditAdd();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
