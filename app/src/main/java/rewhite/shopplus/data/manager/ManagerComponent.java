package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemAdditionalData;

/**
 * 품목등록 부속품 Manager
 */
public class ManagerComponent {
    private static ArrayList<ItemAdditionalData.ItemComponentEdit> itemComponent = new ArrayList<>();

    public void setComponent(ArrayList<ItemAdditionalData.ItemComponentEdit> dates) {
        itemComponent = dates;
    }

    public ArrayList<ItemAdditionalData.ItemComponentEdit> getTComponent() {
        return itemComponent;
    }

    public void setItemDelete(){
        ArrayList<ItemAdditionalData.ItemComponentEdit> itemRepairFrees = ManagerComponent.getInstance().getTComponent();
        itemRepairFrees.clear();
    }

    private static ManagerComponent mInstance = null;

    public static ManagerComponent getInstance() {
        if (mInstance == null) {
            synchronized (ManagerComponent.class) {
                if (mInstance == null) {
                    mInstance = new ManagerComponent();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
