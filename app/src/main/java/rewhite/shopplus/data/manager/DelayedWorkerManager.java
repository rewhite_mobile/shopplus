package rewhite.shopplus.data.manager;

import java.util.ArrayList;
import java.util.List;

/**
 * 특정 조건이 만족될때까지 작업을 미루어 처리할 수 있는 매니저
 */
public class DelayedWorkerManager {
    private boolean mWorked;
    private List<Runnable> mDelayedWorkerList = new ArrayList<>();

    public void run() {
        mWorked = true;

        for (Runnable worker : mDelayedWorkerList) {
            worker.run();
        }
        mDelayedWorkerList.clear();
    }

    public void work(Runnable worker) {
        if (mWorked) {
            worker.run();
        } else {
            mDelayedWorkerList.add(worker);
        }
    }

    public int getSize() {
        return mDelayedWorkerList.size();
    }

    public boolean isWorked() {
        return mWorked;
    }
}
