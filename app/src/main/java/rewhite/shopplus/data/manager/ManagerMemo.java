package rewhite.shopplus.data.manager;

import java.util.ArrayList;

public class ManagerMemo {
    private static ArrayList<String> itemManagerMemo = new ArrayList<>();

    public void setTManagerMemo(ArrayList<String> dates) {
        itemManagerMemo = dates;
    }

    public ArrayList<String> getManagerMemo() {
        return itemManagerMemo;
    }

    public void setItemDelete (){
       ArrayList<String> Memo = ManagerMemo.getInstance().getManagerMemo();
       Memo.clear();
    }


    private static ManagerMemo mInstance = null;

    public static ManagerMemo getInstance() {
        if (mInstance == null) {
            synchronized (ManagerMemo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerMemo();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
