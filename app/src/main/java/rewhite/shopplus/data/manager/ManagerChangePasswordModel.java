package rewhite.shopplus.data.manager;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.ChangePasswordModel;
import rewhite.shopplus.data.dto.ItemEditItemRepair;

/**
 * 패스워드 변경 Request Data
 */
public class ManagerChangePasswordModel {
    private static final String TAG = "ManagerChangePasswordModel";

    public List<ChangePasswordModel> managerItemRepair = new ArrayList<>();

    public List<ChangePasswordModel> getManagerItemRepair() {
        return managerItemRepair;
    }

    public void ManagerChangePasswordModel(List<ChangePasswordModel> itemRepair) {
        this.managerItemRepair = itemRepair;
    }

    private static ManagerChangePasswordModel mInstance = null;

    public static ManagerChangePasswordModel getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerChangePasswordModel.class) {
                if (mInstance == null) {
                    mInstance = new ManagerChangePasswordModel();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
