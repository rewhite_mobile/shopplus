package rewhite.shopplus.data.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetVisitOrderList;

public class ManagerGetVisitOrderItem {
    private static final String TAG = "ManagerGetVisitOrderItem";

    @SerializedName("Data")
    public List<GetVisitOrderList> itemGetVisitOrder = new ArrayList<>();

    public List<GetVisitOrderList> getGetVisitOrder() {
        return itemGetVisitOrder;
    }

    public void setGetVisitOrder(List<GetVisitOrderList> storeUserInfoItemList) {
        this.itemGetVisitOrder = storeUserInfoItemList;
    }

    private static ManagerGetVisitOrderItem mInstance = null;

    public static ManagerGetVisitOrderItem getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerGetVisitOrderItem.class) {
                if (mInstance == null) {
                    mInstance = new ManagerGetVisitOrderItem();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
