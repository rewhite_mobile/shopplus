package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemAdditionalDataEditType;

/**
 *  품목수정 API ManagerClass
 */
public class ManagerItemAdditionalDataEditType {
    private static ArrayList<ItemAdditionalDataEditType> itemAdditionalDataType = new ArrayList<>();

    public void setItemAdditionalData(ArrayList<ItemAdditionalDataEditType> dates) {
        itemAdditionalDataType = dates;
    }

    public ArrayList<ItemAdditionalDataEditType> getItemAdditionalData() {
        return itemAdditionalDataType;
    }

    public void setItemDelete(){
        ArrayList<ItemAdditionalDataEditType> itemRepairFrees = ManagerItemAdditionalDataEditType.getInstance().getItemAdditionalData();
        itemRepairFrees.clear();
    }
    private static ManagerItemAdditionalDataEditType mInstance = null;

    public static ManagerItemAdditionalDataEditType getInstance() {
        if (mInstance == null) {
            synchronized (ManagerMemo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerItemAdditionalDataEditType();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
