package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemAdditionalData;

public class ManagerRepairFree {
    private static ArrayList<ItemAdditionalData.ItemRepairFreeEdit> newsStandDates = new ArrayList<>();

    public void setRepairFeeDates(ArrayList<ItemAdditionalData.ItemRepairFreeEdit> dates) {
        newsStandDates = dates;
    }

    public ArrayList<ItemAdditionalData.ItemRepairFreeEdit> getRepairFreeDates() {
        return newsStandDates;
    }

    public void setReairFreeDelete(){
        ArrayList<ItemAdditionalData.ItemRepairFreeEdit> itemRepairFree = ManagerRepairFree.getInstance().getRepairFreeDates();
        itemRepairFree.clear();
    }


    private static ManagerRepairFree mInstance = null;

    public static ManagerRepairFree getInstance() {
        if (mInstance == null) {
            synchronized (ManagerRepairFree.class) {
                if (mInstance == null) {
                    mInstance = new ManagerRepairFree();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
