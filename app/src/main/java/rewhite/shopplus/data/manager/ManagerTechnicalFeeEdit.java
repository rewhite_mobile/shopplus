package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemTechnical;

public class ManagerTechnicalFeeEdit {
    private static ArrayList<ItemTechnical> itemRepairFrees = new ArrayList<>();

    public void setTechnicalFee(ArrayList<ItemTechnical> dates) {
        itemRepairFrees = dates;
    }

    public ArrayList<ItemTechnical> getTechnicalFee() {
        return itemRepairFrees;
    }

    public void setItemDelete(){
        ArrayList<ItemTechnical> itemRepairFrees = ManagerTechnicalFeeEdit.getInstance().getTechnicalFee();
        itemRepairFrees.clear();
    }

    private static ManagerTechnicalFeeEdit mInstance = null;

    public static ManagerTechnicalFeeEdit getInstance() {
        if (mInstance == null) {
            synchronized (ManagerTechnicalFeeEdit.class) {
                if (mInstance == null) {
                    mInstance = new ManagerTechnicalFeeEdit();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
