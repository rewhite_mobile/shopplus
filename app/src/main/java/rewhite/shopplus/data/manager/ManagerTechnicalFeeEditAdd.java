package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemTechnical;

public class ManagerTechnicalFeeEditAdd {
    private static ArrayList<ItemTechnical> itemRepairFrees = new ArrayList<>();

    public void setTechnicalFee(ArrayList<ItemTechnical> dates) {
        itemRepairFrees = dates;
    }

    public ArrayList<ItemTechnical> getTechnicalFee() {
        return itemRepairFrees;
    }

    public void setItemDelete(){
        ArrayList<ItemTechnical> itemRepairFrees = ManagerTechnicalFeeEditAdd.getInstance().getTechnicalFee();
        itemRepairFrees.clear();
    }

    private static ManagerTechnicalFeeEditAdd mInstance = null;

    public static ManagerTechnicalFeeEditAdd getInstance() {
        if (mInstance == null) {
            synchronized (ManagerTechnicalFeeEditAdd.class) {
                if (mInstance == null) {
                    mInstance = new ManagerTechnicalFeeEditAdd();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
