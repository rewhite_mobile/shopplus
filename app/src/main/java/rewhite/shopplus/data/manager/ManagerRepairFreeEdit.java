package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemRepairFree;

/**
 * 수선요금 수정
 */
public class ManagerRepairFreeEdit {
    private static ArrayList<ItemRepairFree> newsStandDates = new ArrayList<>();

    public void setRepairFeeDates(ArrayList<ItemRepairFree> dates) {
        newsStandDates = dates;
    }

    public ArrayList<ItemRepairFree> getRepairFreeDates() {
        return newsStandDates;
    }

    public void setReairFreeDelete(){
        ArrayList<ItemRepairFree> itemRepairFree = ManagerRepairFreeEdit.getInstance().getRepairFreeDates();
        itemRepairFree.clear();
    }


    private static ManagerRepairFreeEdit mInstance = null;

    public static ManagerRepairFreeEdit getInstance() {
        if (mInstance == null) {
            synchronized (ManagerRepairFreeEdit.class) {
                if (mInstance == null) {
                    mInstance = new ManagerRepairFreeEdit();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
