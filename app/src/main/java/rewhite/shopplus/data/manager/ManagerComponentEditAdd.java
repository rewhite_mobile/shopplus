package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemComponent;

/**
 * 품목등록 부속품 Manager
 */
public class ManagerComponentEditAdd {
    private static ArrayList<ItemComponent> itemComponent = new ArrayList<>();

    public void setComponent(ArrayList<ItemComponent> dates) {
        itemComponent = dates;
    }

    public ArrayList<ItemComponent> getTComponent() {
        return itemComponent;
    }

    public void setItemDelete(){
        ArrayList<ItemComponent> itemRepairFrees = ManagerComponentEditAdd.getInstance().getTComponent();
        itemRepairFrees.clear();
    }

    private static ManagerComponentEditAdd mInstance = null;

    public static ManagerComponentEditAdd getInstance() {
        if (mInstance == null) {
            synchronized (ManagerComponentEditAdd.class) {
                if (mInstance == null) {
                    mInstance = new ManagerComponentEditAdd();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
