package rewhite.shopplus.data.manager;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.CancelAvailOrder;

public class ManagerItemCancle {
    private static List<CancelAvailOrder> itemCancle = new ArrayList<>();

    public void setitemCancle(List<CancelAvailOrder> dates) {
        itemCancle = dates;
    }

    public List<CancelAvailOrder> getitemCancle() {
        return itemCancle;
    }

    public void setItemDelete (){
        List<CancelAvailOrder> itemCancle = ManagerItemCancle.getInstance().getitemCancle();
        itemCancle.clear();
    }


    private static ManagerItemCancle mInstance = null;

    public static ManagerItemCancle getInstance() {
        if (mInstance == null) {
            synchronized (ManagerItemCancle.class) {
                if (mInstance == null) {
                    mInstance = new ManagerItemCancle();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
