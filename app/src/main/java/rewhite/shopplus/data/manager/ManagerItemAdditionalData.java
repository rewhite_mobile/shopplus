package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemAdditionalData;

/**
 * 품목등록 리스트 관리 ManagerClass
 */
public class ManagerItemAdditionalData {
    private static ArrayList<ItemAdditionalData> itemAdditionalData = new ArrayList<>();

    public void setItemAdditionalData(ArrayList<ItemAdditionalData> dates) {
        itemAdditionalData = dates;
    }

    public ArrayList<ItemAdditionalData> getItemAdditionalData() {
        return itemAdditionalData;
    }

    public void setReairFreeDelete(){
        ArrayList<ItemAdditionalData> itemRepairFree = ManagerItemAdditionalData.getInstance().getItemAdditionalData();
        itemRepairFree.clear();
    }

    private static ManagerItemAdditionalData mInstance = null;

    public static ManagerItemAdditionalData getInstance() {
        if (mInstance == null) {
            synchronized (ManagerMemo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerItemAdditionalData();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
