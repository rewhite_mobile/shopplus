package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemAdditionalDataType;

/**
 *  품목등록 API ManagerClass
 */
public class ManagerItemAdditionalDataType {
    private static ArrayList<ItemAdditionalDataType> itemAdditionalDataType = new ArrayList<>();

    public void setItemAdditionalData(ArrayList<ItemAdditionalDataType> dates) {
        itemAdditionalDataType = dates;
    }

    public ArrayList<ItemAdditionalDataType> getItemAdditionalData() {
        return itemAdditionalDataType;
    }

    public void setItemDelete(){
        ArrayList<ItemAdditionalDataType> itemRepairFrees = ManagerItemAdditionalDataType.getInstance().getItemAdditionalData();
        itemRepairFrees.clear();
    }
    private static ManagerItemAdditionalDataType mInstance = null;

    public static ManagerItemAdditionalDataType getInstance() {
        if (mInstance == null) {
            synchronized (ManagerMemo.class) {
                if (mInstance == null) {
                    mInstance = new ManagerItemAdditionalDataType();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
