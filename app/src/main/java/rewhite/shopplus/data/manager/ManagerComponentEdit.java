package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemComponent;

/**
 * 품목등록 부속품 Manager
 */
public class ManagerComponentEdit {
    private static ArrayList<ItemComponent> itemComponent = new ArrayList<>();

    public void setComponent(ArrayList<ItemComponent> dates) {
        itemComponent = dates;
    }

    public ArrayList<ItemComponent> getTComponent() {
        return itemComponent;
    }

    public void setItemDelete(){
        ArrayList<ItemComponent> itemRepairFrees = ManagerComponentEdit.getInstance().getTComponent();
        itemRepairFrees.clear();
    }

    private static ManagerComponentEdit mInstance = null;

    public static ManagerComponentEdit getInstance() {
        if (mInstance == null) {
            synchronized (ManagerComponentEdit.class) {
                if (mInstance == null) {
                    mInstance = new ManagerComponentEdit();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
