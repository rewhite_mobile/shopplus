package rewhite.shopplus.data.manager;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetVisitOrderList;

public class ManagerItemRegistrationAdd {
    private static final String TAG = "ManagerItemRegistrationAdd";

    @SerializedName("Data")
    public List<GetVisitOrderList> itemGetVisitOrder = new ArrayList<>();

    public List<GetVisitOrderList> getGetVisitOrder() {
        return itemGetVisitOrder;
    }

    public void setGetVisitOrder(List<GetVisitOrderList> storeUserInfoItemList) {
        this.itemGetVisitOrder = storeUserInfoItemList;
    }

    private static ManagerItemRegistrationAdd mInstance = null;

    public static ManagerItemRegistrationAdd getmInstance() {
        if (mInstance == null) {
            synchronized (ManagerItemRegistrationAdd.class) {
                if (mInstance == null) {
                    mInstance = new ManagerItemRegistrationAdd();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
