package rewhite.shopplus.data.manager;

import java.util.ArrayList;

import rewhite.shopplus.data.dto.ItemAdditionalData;

public class ManagerTechnicalFee {
    private static ArrayList<ItemAdditionalData.ItemTechnicalEdit> itemRepairFrees = new ArrayList<>();

    public void setTechnicalFee(ArrayList<ItemAdditionalData.ItemTechnicalEdit> dates) {
        itemRepairFrees = dates;
    }

    public ArrayList<ItemAdditionalData.ItemTechnicalEdit> getTechnicalFee() {
        return itemRepairFrees;
    }

    public void setItemDelete(){
        ArrayList<ItemAdditionalData.ItemTechnicalEdit> itemRepairFrees = ManagerTechnicalFee.getInstance().getTechnicalFee();
        itemRepairFrees.clear();
    }

    private static ManagerTechnicalFee mInstance = null;

    public static ManagerTechnicalFee getInstance() {
        if (mInstance == null) {
            synchronized (ManagerTechnicalFee.class) {
                if (mInstance == null) {
                    mInstance = new ManagerTechnicalFee();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
