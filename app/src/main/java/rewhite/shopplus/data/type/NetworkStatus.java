package rewhite.shopplus.data.type;

/**
 * 네트워크 연결 상태.
 * 와이파이, 모바일, 미연결
 */
public enum NetworkStatus {

    /**
     * 와이파이
     */
    WIFI(0),

    /**
     * 모바일
     */
    MOBILE(1),

    /**
     * 미연결
     */
    NOT_CONNECT(2);

    private final int networkStatus;

    NetworkStatus(int networkStatus) {
        this.networkStatus = networkStatus;
    }

    public int type() {
        return networkStatus;
    }

    public static NetworkStatus parse(int status) {
        NetworkStatus type;

        switch (status) {
            case 0:
                type = NetworkStatus.WIFI;
                break;
            case 1:
                type = NetworkStatus.MOBILE;
                break;
            case 2:
            default:
                type = NetworkStatus.NOT_CONNECT;
                break;
        }

        return type;
    }
}