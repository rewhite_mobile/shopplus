package rewhite.shopplus.data.type;

public enum TodayWaterDeliveryType {

    /**
     * 모바일
     */

    MOBILE(0),

    /**
     * 플레이스
     */

    PLACE(1),

    /**
     * GS25
     */
    GS(2),

    /**
     * 방문
     */
    VISIT(3);

    private final int todayWaterDeliveryType;

    TodayWaterDeliveryType(int todayWaterDeliveryType) {
        this.todayWaterDeliveryType = todayWaterDeliveryType;
    }

    public static TodayWaterDeliveryType parse(int type) {
        TodayWaterDeliveryType toType;

        switch (type) {
            case 0:
                toType = TodayWaterDeliveryType.MOBILE;
                break;
            case 1:
                toType = TodayWaterDeliveryType.PLACE;
                break;
            case 2:
                toType = TodayWaterDeliveryType.GS;
                break;
            case 3:
            default:
                toType = TodayWaterDeliveryType.VISIT;
                break;
        }
        return toType;
    }
}