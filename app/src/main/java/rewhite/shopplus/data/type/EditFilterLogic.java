package rewhite.shopplus.data.type;

import android.content.Context;

import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.util.SharedPreferencesUtil;

public class EditFilterLogic {

    public EditFilterLogic(){

    }

    /**
     * 사용자 로그인 아이디
     * @param context
     * @param userId
     */
    public static void setUserId(Context context, String userId){
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.NAME_USER_ID, PrefConstant.KEY_EDIT_FILTER, userId);
    }

    public static String getUserId(Context context){
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.NAME_USER_ID, PrefConstant.KEY_EDIT_FILTER);
    }

    /**
     * 사용자 로그인 패스워드
     * @param context
     * @param userPass
     */
    public static void setUserPassWord(Context context, String userPass){
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.NAME_USER_PASSWORD, PrefConstant.KEY_EDIT_FILTER, userPass);
    }

    public static String getUserPassWord(Context context){
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.NAME_USER_PASSWORD, PrefConstant.KEY_EDIT_FILTER);
    }

    /**
     * 세탁소 이름
     * @param context
     * @param naundryName
     */
    public static void setNaundryName(Context context, String naundryName){
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.NAME_NAUNDRY_NAME, PrefConstant.KEY_EDIT_FILTER, naundryName);
    }

    public static String getNaundryName(Context context){
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.NAME_NAUNDRY_NAME, PrefConstant.KEY_EDIT_FILTER);
    }

    /**
     * 사용자 핸드폰번호
     * @param context
     * @param phonNumber
     */
    public static void setUserPhonNumber(Context context, String phonNumber){
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.NAME_PHON_NUMBER, PrefConstant.KEY_EDIT_FILTER, phonNumber);
    }

    public static String getUserPhonNumber(Context context){
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.NAME_PHON_NUMBER, PrefConstant.KEY_EDIT_FILTER);
    }
}
