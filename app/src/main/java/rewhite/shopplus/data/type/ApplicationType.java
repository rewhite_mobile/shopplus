package rewhite.shopplus.data.type;


public enum ApplicationType{
    FULL("full"), PLAY("play"), PLUS("plus");

    private String str;

    ApplicationType(String str) {
        this.str = str;
    }

    public static ApplicationType parse(String value) {
        ApplicationType result = null;

        for(ApplicationType type : ApplicationType.values()) {
            if(type.toString().equals(value)) {
                result = type;
                break;
            }
        }

        return result;
    }

    @Override
    public String toString() {
        return str;
    }

    public String getType() {
        return str;
    }

//	/**
//	 * 웹뷰 접속 초기화시에 파라미터로 사용한다.
//	 */
//	public String toUrlParam() {
//		StringBuilder result = new StringBuilder("apptype=");
//
//		switch (this) {
//		case FULL:
//			result.append(FULL);
//			break;
//		case PLAY :
//			result.append(PLAY);
//			break;
//		default:
//			break;
//		}
//
//		return result.toString();
//	}


}
