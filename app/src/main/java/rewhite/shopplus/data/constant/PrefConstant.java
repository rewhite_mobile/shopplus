package rewhite.shopplus.data.constant;

/**
 * SharedPreferenceUtil Key Value 정의 class
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
public class PrefConstant {

    /**
     * 로그인
     */
    //DeviceToken
    public static final String KEY_PREFS_FILE = "KEY_PREFS_FILE";
    public static final String NAME_PREFS_DEVICE_ID = "NAME_PREFS_DEVICE_ID";

    //GCM TOKEN
    public static final String KEY_GCM_TOKEN = "pref_key_gcm_token";
    public static final String NAME_GCM_TOKEN = "pref_name_gcm_token";

    //AccessToken
    public static final String KEY_ACCESS_TOKEN = "pref_key_access_token";
    public static final String NAME_ACCESS_TOKEN = "pref_name_access_token";

    //자동로그인
    public static final String KEY_AUTO_LOGIN = "pref_key_auto_login";
    public static final String NAME_AUTO_LOGIN = "pref_name_auto_login";

    //입력 Filter key
    public static final String KEY_EDIT_FILTER = "pref_key_filter";
    public static final String NAME_USER_ID = "pref_name_user_id";
    public static final String NAME_USER_PASSWORD = "pref_name_user_password";
    public static final String NAME_NAUNDRY_NAME = "pref_name_naundry_name";
    public static final String NAME_PHON_NUMBER = "pref_name_phon_number";

    //택번호 Key
    public static final String KEY_START_TAC_NUMBER = "pref_key_start_tac_number";
    public static final String NAME_START_TAC_NUMBER = "pref_name_start_tac_number";
    public static final String NAME_START_TAC_NUMBER_POSITION = "pref_name_start_tac_number_position";
    public static final String NAME_START_TAC_TYPE = "pref_start_tac_type";

    //StoreUserID
    public static final String KEY_STOREUSER_ID = "pref_key_storeuser_id";
    public static final String NAME_STOREUSER_ID = "pref_name_storeuser_id";

    //List item add
    public static final String KEY_ITEM_CLICK = "pref_key_item_click";
    public static final String NAME_ITEM_CLICK = "pref_name_item_click";

    //품목등록 총 결제 금액
    public static final String KEY_TOTAL_MOENY = "pref_key_total_moeny";
    public static final String NAME_TOTAL_MOENY = "pref_name_total_money";

    //    //직접입력한 메모
    public static final String KEY_EDITE_MEMO = "pref_key_edite_memo";
    public static final String NAME_EDITE_MEMO = "pref_namew_edite_memo";

    //고객센터 공지사항/자주하는질문
    public static final String KEY_SERVICE_CENTER_CHANGE = "pref_key_service_center_change";
    public static final String NAME_SERVICE_CENTER_CHANGE = "pref_name_service_center_change";

    /**
     * 영업시간 설정
     */
    //평일 오전 영업시간
    public static final String KEY_BUSINESS_HOURS_WEEKDAYS_MORNING = "pref_key_business_hours_weekdays_morning";
    public static final String NAME_BUSINESS_HOURS_WEEKDAYS_MORNING = "pref_name_business_hours_weekdays_morning";

    //평일 오후 영업시간
    public static final String KEY_BUSINESS_HOURS_WEEKDAYS_AFTERNOON = "pref_key_business_hours_weekdays_afternoon";
    public static final String NAME_BUSINESS_HOURS_WEEKDAYS_AFTERNOON = "pref_name_business_hours_weekdays_afternoon";

    //토요일 오전 영업시간
    public static final String KEY_SATURDAY_BUSINESS_MORNING = "prfe_key_saturday_business_morning";
    public static final String NAME_SATURDAY_BUSINESS_MORNING = "prfe_name_saturday_business_morning";

    //토요일 오후 영업시간
    public static final String KEY_SATURDAY_BUSINESS_AFTERNOON = "prfe_key_saturday_business_afternoon";
    public static final String NAME_SATURDAY_BUSINESS_AFTERNOON = "prfe_name_saturday_business_afternoon";

    //일요일 오전 영업시간
    public static final String KEY_SUNDAY_BUSINESS_MORNING = "prfe_key_sunday_business_morning";
    public static final String NAME_SUNDAY_BUSINESS_MORNING = "prfe_name_sunday_business_morning";

    //일요일 오후 영업시간
    public static final String KEY_SUNDAY_BUSINESS_AFTERNOON = "prfe_name_sunday_business_afternoon";
    public static final String NAME_SUNDAY_BUSINESS_AFTERNOON = "prfe_name_sunday_business_afternoon";

    /**
     * 자주쓰는 메뉴
     */
    //문자보내기
    public static final String KEY_SEND_TEXT = "pref_key_send_text";
    public static final String NAME_SEND_TEXT = "pref_name_send_text";

    //월별 매출현황
    public static final String KEY_MONTHLY_SALES_STATUS = "pref_key_monthly_sales_status";
    public static final String NAME_MONEHLY_SALES_STATUS = "pref_name_monthly_sales_status";

    //일별 매출현황
    public static final String KEY_DAILY_SALES_STATUS = "pref_key_daily_sales_status";
    public static final String NAME_DAILY_SALES_STATUS = "pref_name_daily_sales_status";

    //모바일 매출현황
    public static final String KEY_MOBILE_SALES_STATUS = "pref_key_mobile_sales_status";
    public static final String NAME_MOBILE_SALES_STATUS = "pref_name_mobile_salse_status";

    //방문접수 매출현황
    public static final String KEY_VISITED_RECEPTION_SALES = "pref_key_visited_reception_sales";
    public static final String NAME_VISITED_RECEPTION_SALES = "pref_key_visited_reception_salse";

    //세탁물 자동출고
    public static final String KEY_AUTOMATIC_WAREHOUESE_OF_LAUNDRY = "pref_key_automatic_warhouese_of_laundry";
    public static final String NAME_AUTOMATIC_WAREHOUESE_OF_LAUNDRY = "pref_name_automatic_warhouese_of_laundry";

    //세탁물 자동 세탁완료
    public static final String KEY_LAUNDRY_AUTOMATIC_LAUNDRY = "pref_key_laundry_automatic_laundry";
    public static final String NAME_LAUNDRY_AUTOMATIC_LAUNDRY = "pref_name_laundry_automatic_laundry";

    //마일리지 관리
    public static final String KEY_MILEAGE_MANAGEMENT = "pref_key_maileage_management";
    public static final String NAME_MILEAGE_MANAGEMENT = "pref_name_maileage_menagment";

    //선충전금 관리
    public static final String KEY_PRE_CHARGE_MANAGEMENT = "pref_key_pre_charge_management";
    public static final String NAME_PRE_CHARGE_MANAGEMENT = "pref_name_pre_charge_management";

    //미수금 관리
    public static final String KEY_ACCOUNTS_RECEIVABLE_MANAGEMENT = "pref_key_accounts_receivable_management";
    public static final String NAME_ACCOUNTS_RECEIVABLE_MANAGEMNT = "pref_name_accounts_receivable_management";

    //미출고 세탁물현황
    public static final String KEY_STATUS_OF_UNLOADED_LAUNDRY = "pref_key_status_of_unloaded_laundry";
    public static final String NAME_STATUS_OF_UNLOADED_LAUNDRY = "pref_name_status_of_unloaded_laundry";


    /**
     * 환경설정 데이터
     */

    //FirstData 연결 성공 체크 로직
    public static final String KEY_CONNECTION_STATUS = "key_connection_status";
    public static final String NAME_CONNECTION_STATUS = "name_connection_status";

    //하드웨어 모델 이름
    public static final String KEY_MODEL_NAME = "pref_key_model_name";
    public static final String NAME_MODEL_NAME = "pref_name_model_name";

    //하드웨어 시리얼 번호
    public static final String KEY_SERIAL_NUMBER = "pref_key_serial_number";
    public static final String NAME_SERIAL_NUMBER = "pref_name_serial_number";

    //CAT ID
    public static final String KEY_CAT_ID = "pref_key_cat_id";
    public static final String NAME_CAT_ID = "pref_name_cat_id";

    //Port
    public static final String KEY_PORT = "pref_key_port";
    public static final String NAME_PORT = "pref_name_port";


    /**
     * 상담신청 지역 선택
     */
    public static final String KEY_AREA = "pref_key_area";
    public static final String NAME_AREA_ID = "pref_name_area_id";
    public static final String NAME_AREA_DOSI = "pref_area_dosi";

    /**
     * 상담신청 구 선택
     */
    public static final String KEY_CITY = "pref_key_city";
    public static final String NAME_CITY_ID = "pref_name_city_id";
    public static final String NAME_CITY = "pref_name_city";

    /**
     * 패스워드 6개월 경과 안내 팝업
     */
    public static final String KEY_PASSWORD_CHANGE_GUIDE = "pref_key_password_change_guide";
    public static final String NAME_PASSWORD_CHANGE_GUIDE = "pref_name_password_change_guid";
}
