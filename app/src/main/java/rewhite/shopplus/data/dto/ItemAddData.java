package rewhite.shopplus.data.dto;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ItemAddData {
    @SerializedName("TagNo")
    private String TagNo;         // 택번호
    @SerializedName("TagColor")
    private int TagColor;         // 택색상
    @SerializedName("StoreItemID")
    private int StoreItemID;      // 가맹점 상품ID
    @SerializedName("ItemGrade")
    private int ItemGrade;        // 상품 등급 (1:일반,2:명품,3:아동)
    @SerializedName("IsOutside")
    private String IsOutside;     // 외부세탁여부
    @SerializedName("IsDelivery")
    private String IsDelivery;    // 배송여부
    @SerializedName("PhotoURL")
    private String PhotoURL;      // 첨부사진 (구분자'|')
    @SerializedName("WashType")
    private int WashType;         // 세탁 방법 : 1:드라이+다림질, 2:물세탁+다림질, 3:다림질
    @SerializedName("FreeType")
    private int FreeType;         // 기타옵션 : 1:무료서비스, 2:재세탁, 3:세탁진행중단
    @SerializedName("OrderItemMemo")
    private String OrderItemMemo; // 메모
    @SerializedName("ItemColor")
    private String ItemColor;     // 색상
    @SerializedName("OrderItemPrice")
    private int OrderItemPrice;   // 주문 상품 가격(상품가+옵션가 or 입력가)
    @SerializedName("Options")
    private ArrayList<OrderItemOptionInput> Options; // 추가 옵션

    public String getTacNumber() {
        return TagNo;
    }

    public void setTacNumber(String tacNumber) {
        this.TagNo = tacNumber;
    }

    public int getTacColor() {
        return TagColor;
    }

    public void setTacColor(int tacColor) {
        this.TagColor = tacColor;
    }

    public int getStoreItemID() {
        return StoreItemID;
    }

    public void setStoreItemID(int storeItemID) {
        this.StoreItemID = storeItemID;
    }

    public int getItemGrade() {
        return ItemGrade;
    }

    public void setItemGrade(int itemGrade) {
        this.ItemGrade = itemGrade;
    }

    public String getIsOutside() {
        return IsOutside;
    }

    public void setIsOutside(String isOutside) {
        this.IsOutside = isOutside;
    }

    public String getIsDelicery() {
        return IsDelivery;
    }

    public void setIsDelicery(String isDelicery) {
        this.IsDelivery = isDelicery;
    }

    public String getPhotoUrl() {
        return PhotoURL;
    }

    public void setPhotoUrl(String photoUrl) {
        this.PhotoURL = photoUrl;
    }

    public int getWashType() {
        return WashType;
    }

    public void setWashType(int washType) {
        this.WashType = washType;
    }

    public int getFreeType() {
        return FreeType;
    }

    public void setFreeType(int freeType) {
        this.FreeType = freeType;
    }

    public String getOrderItemMemo() {
        return OrderItemMemo;
    }

    public void setOrderItemMemo(String orderItemMemo) {
        this.OrderItemMemo = orderItemMemo;
    }

    public String getItemColor() {
        return ItemColor;
    }

    public void setItemColor(String itemColor) {
        this.ItemColor = itemColor;
    }

    public int getOrderItemPrice() {
        return OrderItemPrice;
    }

    public void setOrderItemPrice(int orderItemPrice) {
        this.OrderItemPrice = orderItemPrice;
    }

    public ArrayList<OrderItemOptionInput> getItemOption() {
        return Options;
    }

    public void setItemOption(ArrayList<OrderItemOptionInput> itemOption) {
        this.Options = itemOption;
    }


    /**
     * 품목등록 OptionType
     */
    public static class OrderItemOptionInput {
        @SerializedName("OptionType")
        private int OptionType;
        @SerializedName("OptionItemID")
        private int OptionItemID;

        public int getOptionType() {
            return OptionType;
        }

        public void setOptionType(int optionType) {
            this.OptionType = optionType;
        }

        public int getOptionItemId() {
            return OptionItemID;
        }

        public void setOptionItemId(int optionItemId) {
            this.OptionItemID = optionItemId;
        }
    }
}
