package rewhite.shopplus.data.dto;

import java.util.ArrayList;

public class ItemAdditionalDataEditType {
    private static final String TAG = "ItemAdditionalDataType";

    private ArrayList<ItemAddDataEdit> Items;      //아이템 추가

    public ArrayList<ItemAddDataEdit> getItemAddData() {
        return Items;
    }

    public void setItemAddData(ArrayList<ItemAddDataEdit> itemAddData) {
        this.Items = itemAddData;
    }
}
