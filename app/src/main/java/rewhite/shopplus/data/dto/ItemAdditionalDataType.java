package rewhite.shopplus.data.dto;

import java.util.ArrayList;

public class ItemAdditionalDataType {
    private static final String TAG = "ItemAdditionalDataType";

    private ArrayList<ItemAddData> Items;      //아이템 추가

    public ArrayList<ItemAddData> getItemAddData() {
        return Items;
    }

    public void setItemAddData(ArrayList<ItemAddData> itemAddData) {
        this.Items = itemAddData;
    }
}
