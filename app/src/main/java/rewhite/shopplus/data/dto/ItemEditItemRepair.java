package rewhite.shopplus.data.dto;

public class ItemEditItemRepair {
    private String orderItemName;
    private String orderItemAmount;
    private int orderItemID;
    private String type;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrderItemAmount() {
        return orderItemAmount;
    }

    public void setOrderItemAmount(String orderItemAmount) {
        this.orderItemAmount = orderItemAmount;
    }

    public int getOrderItemID() {
        return orderItemID;
    }

    public void setOrderItemID(int orderItemID) {
        this.orderItemID = orderItemID;
    }

    public String getOrderItemName() {
        return orderItemName;
    }

    public void setOrderItemName(String orderItemName) {
        this.orderItemName = orderItemName;
    }
}
