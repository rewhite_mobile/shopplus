package rewhite.shopplus.data.dto;

public class ItemTechnical {
    private String titleName;       //제목
    private String amount;          //금액
    private String OptionItemID;    //옵션상품 ID


    public String getOptionItemID() {
        return OptionItemID;
    }

    public void setOptionItemID(String optionItemID) {
        OptionItemID = optionItemID;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}
