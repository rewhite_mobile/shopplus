package rewhite.shopplus.data.dto;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.client.model.GetVisitOrderList;

/**
 * 방문접수(전체) 아이템 선택 ManagerClass
 */
public class ItemGetVisitOrderList {
    private static final String TAG = "ItemGetVisitOrderList";

    public List<GetVisitOrderList> itemGetVisitOrder = new ArrayList<>();

    public List<GetVisitOrderList> getGetVisitOrder() {
        return itemGetVisitOrder;
    }

    public void setGetVisitOrder(List<GetVisitOrderList> storeUserInfoItemList) {
        this.itemGetVisitOrder = storeUserInfoItemList;
    }

    private static ItemGetVisitOrderList mInstance = null;

    public static ItemGetVisitOrderList getmInstance() {
        if (mInstance == null) {
            synchronized (ItemGetVisitOrderList.class) {
                if (mInstance == null) {
                    mInstance = new ItemGetVisitOrderList();
                    return mInstance;
                }
            }
        }
        return mInstance;
    }
}
