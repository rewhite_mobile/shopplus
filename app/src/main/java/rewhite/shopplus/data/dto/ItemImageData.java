package rewhite.shopplus.data.dto;

public class ItemImageData {

    private String ImageFirstData;
    private String ImageSecondData;
    private String ImageThirdData;


    public String getImageFirstData() {
        return ImageFirstData;
    }

    public void setImageFirstData(String imageFirstData) {
        ImageFirstData = imageFirstData;
    }

    public String getImageSecondData() {
        return ImageSecondData;
    }

    public void setImageSecondData(String imageSecondData) {
        ImageSecondData = imageSecondData;
    }

    public String getImageThirdData() {
        return ImageThirdData;
    }

    public void setImageThirdData(String imageThirdData) {
        ImageThirdData = imageThirdData;
    }
}
