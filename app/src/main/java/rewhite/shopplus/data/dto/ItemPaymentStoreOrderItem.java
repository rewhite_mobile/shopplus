package rewhite.shopplus.data.dto;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import rewhite.shopplus.client.model.GetPaymentInfoModel;

public class ItemPaymentStoreOrderItem {

    @SerializedName("ResultCode")
    private String ResultCode; //결과 코드

    @SerializedName("Data")
    GetPaymentInfoModel.Data Data;

    public GetPaymentInfoModel.Data getData() {
        return Data;
    }

    public String getResultCode() {
        return ResultCode;
    }

    public static class Data {
        @SerializedName("PaymentID")
        private int PaymentID;            //결제ID
        @SerializedName("StoreID")
        private int StoreID;              //가맹점ID
        @SerializedName("StoreUserID")
        private int StoreUserID;          //가맹점 회원ID
        @SerializedName("UserID")
        private int UserID;               //회원ID
        @SerializedName("PaymentDivide")
        private int PaymentDivide;        //결제구분(1:주문결제, 2:선충전금충전)
        @SerializedName("PaymentDivideDesc")
        private String PaymentDivideDesc; //결제 구분 명
        @SerializedName("PaymentPrice")
        private int PaymentPrice;         //금액
        @SerializedName("PaymentStatus")
        private int PaymentStatus;         //결제 상태(10:정상, 90:취소)
        @SerializedName("PaymentStatusDesc")
        private String PaymentStatusDesc;  //결제 상태명
        @SerializedName("PaymentMemo")
        private String PaymentMemo;        //메모
        @SerializedName("PaymentDate")
        private String PaymentDate;        //결제일
        @SerializedName("CancelDate")
        private String CancelDate;         //취소일
        @SerializedName("SaveMileage")
        private int SaveMileage;           //적립마일리지
        @SerializedName("UseAvailDate")
        private String UseAvailDate;       //마일리지 적립일(예정일)
        @SerializedName("Badges")
        private ArrayList<String> Badges;  //결제 정보 뱃지
        @SerializedName("exReceiptPrice")
        private int exReceiptPrice;        // 현금영수증 발급 예상금액
        @SerializedName("ReceiptPrice")
        private int ReceiptPrice;          //현금영수증 발급 금액
        @SerializedName("ReceiptID")
        private String ReceiptID;          // 현금영수증 발급번호
        @SerializedName("PaymentDetails")
        private ArrayList<GetPaymentInfoModel.PaymentDetail> PaymentDetails; //결제 상세 정보
        @SerializedName("OrderItems")
        private ArrayList<GetPaymentInfoModel.OrderItem> OrderItems; //주문 품목 정보


        public int getExReceiptPrice() {
            return exReceiptPrice;
        }

        public String getReceiptID() {
            return ReceiptID;
        }

        public int getPaymentID() {
            return PaymentID;
        }

        public int getStoreID() {
            return StoreID;
        }

        public int getStoreUserID() {
            return StoreUserID;
        }

        public int getUserID() {
            return UserID;
        }

        public int getPaymentDivide() {
            return PaymentDivide;
        }

        public String getPaymentDivideDesc() {
            return PaymentDivideDesc;
        }

        public int getPaymentPrice() {
            return PaymentPrice;
        }

        public int getPaymentStatus() {
            return PaymentStatus;
        }

        public String getPaymentStatusDesc() {
            return PaymentStatusDesc;
        }

        public String getPaymentMemo() {
            return PaymentMemo;
        }

        public String getPaymentDate() {
            return PaymentDate;
        }

        public String getCancelDate() {
            return CancelDate;
        }

        public int getSaveMileage() {
            return SaveMileage;
        }

        public String getUseAvailDate() {
            return UseAvailDate;
        }

        public ArrayList<String> getBadges() {
            return Badges;
        }

        public int getReceiptPrice() {
            return ReceiptPrice;
        }

        public ArrayList<GetPaymentInfoModel.PaymentDetail> getPaymentDetails() {
            return PaymentDetails;
        }

        public ArrayList<GetPaymentInfoModel.OrderItem> getOrderItems() {
            return OrderItems;
        }
    }

    public static class PaymentDetail {
        @SerializedName("PaymemtDetailID")
        private int PaymemtDetailID;     //결제상세ID
        @SerializedName("PaymentID")
        private int PaymentID;           //결제ID
        @SerializedName("PaymentType")
        private int PaymentType;         //결제타입(1:현금,2:카드,3:선충전금,4:마일리지,5:쿠폰,10:온라인)
        @SerializedName("PaymentTypeDesc")
        private String PaymentTypeDesc;  //결제 타입 명
        @SerializedName("IsReceipt")
        private String IsReceipt;        //영수증발급여부(Y/N)
        @SerializedName("PGID")
        private int PGID;                //PG ID
        @SerializedName("TID")
        private String TID;              //승인번호
        @SerializedName("CATInfo")
        private String CATInfo;          //카드 단말기 정보
        @SerializedName("CardName")
        private String CardName;         //결제 카드명
        @SerializedName("CardNo")
        private String CardNo;           //카드번호
        @SerializedName("CardDivide")
        private int CardDivide;          //할부개월수
        @SerializedName("PayYMD")
        private String PayYMD;           //원거래일자
        @SerializedName("Price")
        private int Price;               //결제금액
        @SerializedName("RegDate")
        private String RegDate;          //등록일
        @SerializedName("UpdDate")
        private String UpdDate;          //수정일

        public int getPaymemtDetailID() {
            return PaymemtDetailID;
        }

        public int getPaymentID() {
            return PaymentID;
        }

        public int getPaymentType() {
            return PaymentType;
        }

        public String getPaymentTypeDesc() {
            return PaymentTypeDesc;
        }

        public String getIsReceipt() {
            return IsReceipt;
        }

        public int getPGID() {
            return PGID;
        }

        public String getTID() {
            return TID;
        }

        public String getCATInfo() {
            return CATInfo;
        }

        public String getCardName() {
            return CardName;
        }

        public String getCardNo() {
            return CardNo;
        }

        public int getCardDivide() {
            return CardDivide;
        }

        public String getPayYMD() {
            return PayYMD;
        }

        public int getPrice() {
            return Price;
        }

        public String getRegDate() {
            return RegDate;
        }

        public String getUpdDate() {
            return UpdDate;
        }
    }

    public static class OrderItem {
        @SerializedName("StoreUserID")
        private int StoreUserID;        //가맹점 회원ID
        @SerializedName("StoreID")
        private int StoreID;            //가맹점ID
        @SerializedName("UserID")
        private int UserID;             //회원ID
        @SerializedName("UserName")
        private String UserName;        //회원명
        @SerializedName("StoreUserGrade")
        private String StoreUserGrade;  //가맹점 회원 등급(1,2,3)
        @SerializedName("_UserAddress")
        private String _UserAddress;    //주소1(읽기전용,복호화)
        @SerializedName("_UserPhone")
        private String _UserPhone;      //가맹점 회원 휴대폰 번호(읽기전용, 복호화)
        @SerializedName("OrderID")
        private int OrderID;            //주문ID
        @SerializedName("OrderItemID")
        private int OrderItemID;        //주문상품ID
        @SerializedName("TagNo")
        private String TagNo;           //Tag번호
        @SerializedName("TagColor")
        private String TagColor;        //Tag 컬러
        @SerializedName("StoreItemID")
        private int StoreItemID;        //가맹점 상품ID
        @SerializedName("StoreItemName")
        private String StoreItemName;   //가맹점 상품명
        @SerializedName("ItemGrade")
        private int ItemGrade;          //상품등급(1:일반,2:명품,3:아동)
        @SerializedName("ItemGradeDesc")
        private String ItemGradeDesc;   //상풍등급명
        @SerializedName("StoreItemPrice")
        private int StoreItemPrice;     //상품 금액
        @SerializedName("EnterDate")
        private String EnterDate;       //입고일
        @SerializedName("WashFinWillDate")
        private String WashFinWillDate; //세탁완료예정일
        @SerializedName("WashFinDate")
        private String WashFinDate;     //세탁완료일
        @SerializedName("OutDate")
        private String OutDate;         //출고일
        @SerializedName("IsOutside")
        private String IsOutside;       //외부세탁여부
        @SerializedName("IsDelivery")
        private String IsDelivery;      //배달여부
        @SerializedName("PickupID")
        private int PickupID;           //수거ID
        @SerializedName("DeliveryID")
        private int DeliveryID;         //배송ID
        @SerializedName("PhotoURL")
        private String PhotoURL;        //첨부사진 (구분자'|')
        @SerializedName("WashType")
        private int WashType;           //세탁방법 : 1:드라이+다림질, 2:물세탁+다림질, 3:다림질
        @SerializedName("WashTypeDesc")
        private String WashTypeDesc;    //세탁방법명
        @SerializedName("FreeType")
        private int FreeType;           //기타옵션 : 1:무료서비스, 2:재세탁, 3:세탁진행중단
        @SerializedName("FreeTypeDesc")
        private String FreeTypeDesc;    //기타옵션명
        @SerializedName("ItemColor")
        private String ItemColor;       //색상(#FFFFFF)
        @SerializedName("OrderItemPrice")
        private int OrderItemPrice;     //주문 금액
        @SerializedName("OrderItemMemo")
        private String OrderItemMemo;   //메모(구분자'|')
        @SerializedName("OrderItemStatus")
        private int OrderItemStatus;    //상태
        @SerializedName("OrderItemStatusDesc")
        private String OrderItemStatusDesc; //상태명
        @SerializedName("Badges")
        private ArrayList<String> Badges;   //주문 정보 뱃지
        @SerializedName("PayPrice")
        private String PayPrice;            //기결제금액
        @SerializedName("NonPayPrice")
        private String NonPayPrice;         //미결제금액(미수금)
        @SerializedName("Options")
        private ArrayList<GetPaymentInfoModel.OrderItemOption> Options;//주문상품옵션

        public int getStoreUserID() {
            return StoreUserID;
        }

        public int getStoreID() {
            return StoreID;
        }

        public int getUserID() {
            return UserID;
        }

        public String getUserName() {
            return UserName;
        }

        public String getStoreUserGrade() {
            return StoreUserGrade;
        }

        public String get_UserAddress() {
            return _UserAddress;
        }

        public String get_UserPhone() {
            return _UserPhone;
        }

        public int getOrderID() {
            return OrderID;
        }

        public int getOrderItemID() {
            return OrderItemID;
        }

        public String getTagNo() {
            return TagNo;
        }

        public String getTagColor() {
            return TagColor;
        }

        public int getStoreItemID() {
            return StoreItemID;
        }

        public String getStoreItemName() {
            return StoreItemName;
        }

        public int getItemGrade() {
            return ItemGrade;
        }

        public String getItemGradeDesc() {
            return ItemGradeDesc;
        }

        public int getStoreItemPrice() {
            return StoreItemPrice;
        }

        public String getEnterDate() {
            return EnterDate;
        }

        public String getWashFinWillDate() {
            return WashFinWillDate;
        }

        public String getWashFinDate() {
            return WashFinDate;
        }

        public String getOutDate() {
            return OutDate;
        }

        public String getIsOutside() {
            return IsOutside;
        }

        public String getIsDelivery() {
            return IsDelivery;
        }

        public int getPickupID() {
            return PickupID;
        }

        public int getDeliveryID() {
            return DeliveryID;
        }

        public String getPhotoURL() {
            return PhotoURL;
        }

        public int getWashType() {
            return WashType;
        }

        public String getWashTypeDesc() {
            return WashTypeDesc;
        }

        public int getFreeType() {
            return FreeType;
        }

        public String getFreeTypeDesc() {
            return FreeTypeDesc;
        }

        public String getItemColor() {
            return ItemColor;
        }

        public int getOrderItemPrice() {
            return OrderItemPrice;
        }

        public String getOrderItemMemo() {
            return OrderItemMemo;
        }

        public int getOrderItemStatus() {
            return OrderItemStatus;
        }

        public String getOrderItemStatusDesc() {
            return OrderItemStatusDesc;
        }

        public ArrayList<String> getBadges() {
            return Badges;
        }

        public String getPayPrice() {
            return PayPrice;
        }

        public String getNonPayPrice() {
            return NonPayPrice;
        }

        public ArrayList<GetPaymentInfoModel.OrderItemOption> getOptions() {
            return Options;
        }
    }

    public static class OrderItemOption {
        @SerializedName("OrderItemID")
        private int OrderItemID;        //주문상품ID
        @SerializedName("OptionType")
        private int OptionType;         //옵션 상품 타입:추가기술, 부속품, 수선
        @SerializedName("OptionTypeDesc")
        private String OptionTypeDesc;  //옵션타입명
        @SerializedName("OptionItemID")
        private int OptionItemID;       //옵션 상품ID
        @SerializedName("OptionItemName")
        private String OptionItemName;  //옵션상품명
        @SerializedName("OptionItemGrade")
        private int OptionItemGrade;    //옵션 상품 등급
        @SerializedName("OptionPrice")
        private int OptionPrice;        //옵션금액
        @SerializedName("IsUse")
        private String IsUse;           //사용여부(Y,N)
        @SerializedName("RegDate")
        private String RegDate;         //등록일
        @SerializedName("UpdDate")
        private String UpdDate;         //수정일

        public int getOrderItemID() {
            return OrderItemID;
        }

        public int getOptionType() {
            return OptionType;
        }

        public String getOptionTypeDesc() {
            return OptionTypeDesc;
        }

        public int getOptionItemID() {
            return OptionItemID;
        }

        public String getOptionItemName() {
            return OptionItemName;
        }

        public int getOptionItemGrade() {
            return OptionItemGrade;
        }

        public int getOptionPrice() {
            return OptionPrice;
        }

        public String getIsUse() {
            return IsUse;
        }

        public String getRegDate() {
            return RegDate;
        }

        public String getUpdDate() {
            return UpdDate;
        }
    }
}
