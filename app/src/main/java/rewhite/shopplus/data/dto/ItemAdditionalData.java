package rewhite.shopplus.data.dto;

import java.util.ArrayList;

public class ItemAdditionalData {
    private static final String TAG = "ItemAdditionalData";

    private String itemName;                         // 상품 Name
    private String UserId;                           // 사용자ID

    private String tacNumber;                        // 택번호
    private int tacColor;                            // 택컬러
    private int storeItemID;                         // 가맹점 상품ID
    private int laundryMoney;                        // 가맹점 상품 money
    private String storeItemName;                    // 가맹점 상품Name
    private int ItemGrade;                           // 상품등급
    private String IsOutside;                        // 외부세탁여부
    private String IsDelivery;                       // 배송여부
    private String photoUri;                         // 첨부사진(구분자 |)
    private int howToWash;                           // 세탁방법
    private int freeType;                            // 기타옵션
    private String memoData;                         // 메모
    private int tagCount;                            // tagCount
    private int rgbColor;                            // 색상
    private String totalMoney;                       // 주문금액
    private ArrayList<ItemAdditionalData.ItemRepairFreeEdit> optionTypeRepairFee;   //추가옵션 수선 Name
    private ArrayList<ItemAdditionalData.ItemTechnicalEdit> optionTechnicalFree;   //추가옵션 기술 Name
    private ArrayList<ItemAdditionalData.ItemComponentEdit>  optionTypeComponent;   //추가옵션 부속품 Name

    private ArrayList<OrderItemOptionInput> Options; // 추가 옵션


    public int getLaundryMoney() {
        return laundryMoney;
    }

    public void setLaundryMoney(int laundryMoney) {
        this.laundryMoney = laundryMoney;
    }

    public int getTagCount() {
        return tagCount;
    }

    public void setTagCount(int tagCount) {
        this.tagCount = tagCount;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public int getStoreItemID() {
        return storeItemID;
    }

    public void setStoreItemID(int storeItemID) {
        this.storeItemID = storeItemID;
    }

    public int getItemGrade() {
        return ItemGrade;
    }

    public void setItemGrade(int itemGrade) {
        ItemGrade = itemGrade;
    }

    public String getIsOutside() {
        return IsOutside;
    }

    public void setIsOutside(String isOutside) {
        IsOutside = isOutside;
    }

    public String getIsDelivery() {
        return IsDelivery;
    }

    public void setIsDelivery(String isDelivery) {
        IsDelivery = isDelivery;
    }

    public int getFreeType() {
        return freeType;
    }

    public void setFreeType(int freeType) {
        this.freeType = freeType;
    }

    public ArrayList<OrderItemOptionInput> getOptions() {
        return Options;
    }

    public void setOptions(ArrayList<OrderItemOptionInput> options) {
        Options = options;
    }

    public int getTacColor() {
        return tacColor;
    }

    public void setTacColor(int tacColor) {
        this.tacColor = tacColor;
    }

    public String getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(String totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getTacNumber() {
        return tacNumber;
    }

    public void setTacNumber(String tacNumber) {
        this.tacNumber = tacNumber;
    }

    public int getHowToWash() {
        return howToWash;
    }

    public void setHowToWash(int howToWash) {
        this.howToWash = howToWash;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }

    public int getRgbColor() {
        return rgbColor;
    }

    public void setRgbColor(int rgbColor) {
        this.rgbColor = rgbColor;
    }

    public String getMemoData() {
        return memoData;
    }

    public void setMemoData(String memoData) {
        this.memoData = memoData;
    }

    public String getStoreItemName() {
        return storeItemName;
    }

    public void setStoreItemName(String storeItemName) {
        this.storeItemName = storeItemName;
    }

    public ArrayList<ItemAdditionalData.ItemRepairFreeEdit> getOptionTypeRepairFee() {
        return optionTypeRepairFee;
    }

    public void setOptionTypeRepairFee(ArrayList<ItemAdditionalData.ItemRepairFreeEdit> optionTypeRepairFee) {
        this.optionTypeRepairFee = optionTypeRepairFee;
    }

    public ArrayList<ItemAdditionalData.ItemTechnicalEdit> getOptionTechnicalFree() {
        return optionTechnicalFree;
    }

    public void setOptionTechnicalFree(ArrayList<ItemTechnicalEdit> optionTechnicalFree) {
        this.optionTechnicalFree = optionTechnicalFree;
    }

    public ArrayList<ItemComponentEdit> getOptionTypeComponent() {
        return optionTypeComponent;
    }

    public void setOptionTypeComponent(ArrayList<ItemComponentEdit> optionTypeComponent) {
        this.optionTypeComponent = optionTypeComponent;
    }

    public static class ItemRepairFreeEdit {
        private String titleName;       //제목
        private String amount;          //금액
        private String OptionItemID;    //옵션상품 ID


        public String getOptionItemID() {
            return OptionItemID;
        }

        public void setOptionItemID(String optionItemID) {
            OptionItemID = optionItemID;
        }

        public String getTitleName() {
            return titleName;
        }

        public void setTitleName(String titleName) {
            this.titleName = titleName;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }

    public static class ItemTechnicalEdit {

        private String titleName;       //제목
        private String amount;          //금액
        private String OptionItemID;    //옵션상품 ID


        public String getOptionItemID() {
            return OptionItemID;
        }

        public void setOptionItemID(String optionItemID) {
            OptionItemID = optionItemID;
        }

        public String getTitleName() {
            return titleName;
        }

        public void setTitleName(String titleName) {
            this.titleName = titleName;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }


    public static class ItemComponentEdit {

        private String titleName;       //제목
        private String amount;          //금액
        private String OptionItemID;    //옵션상품 ID


        public String getOptionItemID() {
            return OptionItemID;
        }

        public void setOptionItemID(String optionItemID) {
            OptionItemID = optionItemID;
        }

        public String getTitleName() {
            return titleName;
        }

        public void setTitleName(String titleName) {
            this.titleName = titleName;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }
    }

    /**
     * 품목등록 OptionType
     */
    public static class OrderItemOptionInput {
        private int OptionType;     //옵션타입(1:추가기술, 2:부속품, 3:수선)
        private int OptionItemID;   //옵션상품ID

        public int getOptionType() {
            return OptionType;
        }

        public void setOptionType(int optionType) {
            this.OptionType = optionType;
        }

        public int getOptionItemId() {
            return OptionItemID;
        }

        public void setOptionItemId(int optionItemId) {
            this.OptionItemID = optionItemId;
        }
    }
}
