package rewhite.shopplus.data.dto;

public class ItemOrderDetails {
    private static final String TAG = "ItemOrderDetails";

    private String itemName;
    private int itemAmount;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(int itemAmount) {
        this.itemAmount = itemAmount;
    }
}
