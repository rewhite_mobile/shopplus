package rewhite.shopplus.common.listener;

/**
 * 백키 이벤트 리스너를 등록 및 삭제할 수 있는 옵저버
 */
public interface OnBackPressedObserver {
    void addOnBackPressedListener(OnBackPressedListener listener);
    void removeOnBackPressedListener(OnBackPressedListener listener);
}
