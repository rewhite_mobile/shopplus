package rewhite.shopplus.common.listener;

import android.view.View;
import android.view.ViewGroup;

/**
 * 주어진 컨테이너에 추가할 View를 생성할 수 있는 Generator
 */
public interface ViewGenerator {
    View generate(ViewGroup container);
}
