package rewhite.shopplus.common.listener;

import rewhite.shopplus.common.local.StateChecker;

/**
 * 단말 상태를 병렬로 조회하는 경우 상태 조회가 완료되었을 때 전달하는 인터페이스
 */
public interface ChainStateCheckListener extends StateChecker.StateCheckListener {
    void onStateCheckFinished();
}