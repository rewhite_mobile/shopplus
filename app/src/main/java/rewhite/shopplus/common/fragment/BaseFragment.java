package rewhite.shopplus.common.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import java.lang.reflect.Field;

import rewhite.shopplus.common.listener.OnBackPressedListener;
import rewhite.shopplus.common.listener.OnBackPressedObserver;

/**
 * Rewhite_Shpp+ 앱에서 구현하는 모든 Fragment들의 추상 Fragment
 * DialogFragment를 상속받는다.
 */
public class BaseFragment extends DialogFragment {
    private FragmentService mFragmentService;
    private OnBackPressedObserver mOnBackPressedObserver;
    private OnDialogDismissListener mListener;

    public interface OnDialogDismissListener {
        void onDismiss(BaseFragment fragment);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if(mListener != null) {
            mListener.onDismiss(BaseFragment.this);
        }
    }

    public void setOnDialogDismissListener(OnDialogDismissListener listener) {
        mListener = listener;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    public boolean isVanished() {
        return (isRemoving() || isDetached() || getContext() == null);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mFragmentService = (FragmentService)context;
        } catch( ClassCastException ex ) {
            mFragmentService = new FragmentService() {
                @Override
                public void replaceFragment(Fragment fragment) {
                    Log.e("FragmentService", "Attached context is not FragmentService");
                }
                @Override
                public void addFragment(Fragment fragment) {
                    Log.e("FragmentService", "Attached context is not FragmentService");
                }
                @Override
                public void finishFragment() {
                    Log.e("FragmentService", "Attached context is not FragmentService");
                }
            };
        }

        try {
            mOnBackPressedObserver = (OnBackPressedObserver)context;
        } catch( Exception ex ) {
            mOnBackPressedObserver = new OnBackPressedObserver() {
                @Override
                public void addOnBackPressedListener(OnBackPressedListener listener) {

                }

                @Override
                public void removeOnBackPressedListener(OnBackPressedListener listener) {

                }
            };
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    protected void addOnBackPressedListener(OnBackPressedListener listener) {
        if(mOnBackPressedObserver != null){
            mOnBackPressedObserver.addOnBackPressedListener(listener);
        }
    }

    protected void removeOnBackPressedListener(OnBackPressedListener listener) {
        if(mOnBackPressedObserver != null){
            mOnBackPressedObserver.removeOnBackPressedListener(listener);
        }
    }

    protected void addFragment(Fragment fragment) {
        mFragmentService.addFragment(fragment);
    }

    protected void replaceFragment(Fragment fragment) {
        mFragmentService.replaceFragment(fragment);
    }

    protected void finishFragment() {
        mFragmentService.finishFragment();
    }
}
