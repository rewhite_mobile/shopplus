package rewhite.shopplus.common.fragment;

import android.support.v4.app.Fragment;

/**
 * Fragment의 생성, 종료를 처리하기 위한 인터페이스
 */
public interface FragmentService {
    void replaceFragment(Fragment fragment);
    void addFragment(Fragment fragment);
    void finishFragment();
}