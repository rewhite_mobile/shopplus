package rewhite.shopplus.common.local;

import android.os.AsyncTask;
import android.text.TextUtils;

/**
 * 단말 상태를 비동기로 조회하기 위한 클래스
 */
public abstract class StateChecker {
    protected AsyncChecker mChecker;
    protected StateCheckListener mListener;

    private boolean mSuccess;
    private boolean mCancelRequested;

    public interface StateCheckListener {
        void onCheckStart(StateChecker checker);
        void onChecked(StateChecker checker);
        void onCheckCancelled(StateChecker checker);
    }

    public abstract void startCheck();

    public void stopCheck() {
        mCancelRequested = true;
        if(isChecking()) {
            mChecker.cancel(true);
        }
    }

    public boolean isChecking() {
        return mChecker != null && mChecker.getStatus() != AsyncTask.Status.FINISHED;
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    public void setStateCheckListener(StateCheckListener listener) {
        mListener = listener;
    }

    protected void check() {
        mCancelRequested = false;
//        Utility.executeTask(mChecker, "");
    }

    protected abstract class AsyncChecker extends AsyncTask<String, Void, String> {
        private StateChecker checker;

        protected abstract String doInBackground(String... params);

        public AsyncChecker(StateChecker checker) {
            this.checker = checker;
        }

        @Override
        protected void onPreExecute() {
            if(mListener != null) {
                mListener.onCheckStart(checker);
            }
        }

        @Override
        protected void onPostExecute(String s) {
            mSuccess = !TextUtils.isEmpty(s);
            if(mListener != null) {
                if(checker.mCancelRequested) {
                    mListener.onCheckCancelled(checker);
                } else {
                    mListener.onChecked(checker);
                }
            }
            checker.mCancelRequested = false;
        }

        @Override
        protected void onCancelled(String s) {
            if(mListener != null) {
                mListener.onCheckCancelled(checker);
            }
        }
    }
}
