package rewhite.shopplus.common.local;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.common.listener.ChainStateCheckListener;

/**
 * 단말 상태를 병렬로 조회하기 위한 Chain 클래스
 */
public class StateCheckChain {
    private int count;
    private ChainStateCheckListener listener;
    private List<StateChecker> list = new ArrayList<>();

    private StateChecker.StateCheckListener slistener = new StateChecker.StateCheckListener() {
        @Override
        public void onCheckStart(StateChecker checker) {
            listener.onCheckStart(checker);
        }

        @Override
        public void onChecked(StateChecker checker) {
            listener.onChecked(checker);
            if(--count == 0) {
                listener.onStateCheckFinished();
            }
        }

        @Override
        public void onCheckCancelled(StateChecker checker) {
            listener.onCheckCancelled(checker);
            if(--count == 0) {
                listener.onStateCheckFinished();
            }
        }
    };

    public StateCheckChain(ChainStateCheckListener listener) {
        this.listener = listener;
    }

    public StateCheckChain addChecker(StateChecker checker) {
        checker.setStateCheckListener(slistener);
        list.add(checker);
        count = list.size();
        return this;
    }

    public void check() {
        for(StateChecker checker : list) {
            checker.startCheck();
        }
    }

    public void cancel() {
        for(StateChecker checker : list) {
            checker.stopCheck();
        }
        count = 0;
        list.clear();
    }
}
