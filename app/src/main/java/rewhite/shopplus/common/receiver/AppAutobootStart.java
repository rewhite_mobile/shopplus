package rewhite.shopplus.common.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import rewhite.shopplus.activity.ActivityLogin;

/**
 * 디바이스 재실행 및 종료후 자동으로 Shop+ App실행 가능하도록 설정
        */
public class AppAutobootStart extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if (action.equals("android.intent.action.BOOT_COMPLETED")) {
            Intent i = new Intent(context, ActivityLogin.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }
}
