package rewhite.shopplus.common.popup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import rewhite.shopplus.R;
import rewhite.shopplus.common.ValueWorker;
import rewhite.shopplus.util.Logger;

/**
 * 확인, 취소 버튼을 가지고 있는 선택 팝업
 */
public class ConfirmPopup extends AlertPopup {
    private OnCancelClickListener mOnCancelClickListener;
    private Button mCancelButton;

    public ConfirmPopup(Context context) {
        super(context);
    }

    public ConfirmPopup setOnCancelClickListener(OnCancelClickListener listener) {
        mOnCancelClickListener = listener;
        return this;
    }

    public ConfirmPopup setCancelButtonText(int resId) {
        mWorkerManager.work(new ValueWorker<Integer>(resId) {
            @Override
            public void run() {
                mCancelButton.setText(getValue());
            }
        });
        return this;
    }

    public ConfirmPopup setCancelButtonText(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                mCancelButton.setText(getValue());
            }
        });
        return this;
    }

    public ConfirmPopup setConfirmButtonText(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                mConfirmButton.setText(getValue());
            }
        });
        return this;
    }

    @Override
    protected void setButtons(ViewGroup container) {
        container.removeViewAt(container.getChildCount() - 1);
        View v = LayoutInflater.from(container.getContext()).inflate(R.layout.view_bottom_horizontal_two_buttons, container, false);
        mCancelButton = (Button)v.findViewById(R.id.left_button);
        mCancelButton.setText(R.string.cancel);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();

                if(mOnCancelClickListener != null) {
                    sendJsResultCancel();
                    mOnCancelClickListener.onCancelClick();
                }
            }
        });
        mConfirmButton = (Button)v.findViewById(R.id.right_button);
        mConfirmButton.setText(R.string.confirm);
        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hide();

                if( mOnConfirmClickListener != null ) {
                    sendJsResultConfirm();
                    mOnConfirmClickListener.onConfirmClick();
                }
            }
        });
        container.addView(v);
    }

    @Override
    public void sendJsResult() {
        Logger.v("AlertPopup","## Alet sendJsResult[sendJsResultCancel]");
        sendJsResultCancel();
    }

    public interface OnCancelClickListener {
        void onCancelClick();
    }
}
