package rewhite.shopplus.common.popup;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.common.listener.ViewGenerator;

/**
 * 화면의 상단 및 하단으로부터 밀려 내려오거나 올라오는 애니메이션이 가능한 팝업 컨테이너
 */
public class PeekPopup extends Popup {
    private FrameLayout mTopContainer;
    private FrameLayout mBottomContainer;

    private boolean mViewCreated = false;
    private List<Runnable> mWorkerListOnViewCreated = new ArrayList<>();

    private AnimationProxy mTopAnimationProxy = new AnimationProxy() {
        @Override
        public ViewGroup getContainer() {
            return mTopContainer;
        }

        @Override
        public int getMargin(ViewGroup.MarginLayoutParams params) {
            return params.topMargin;
        }

        @Override
        public void setMargin(ViewGroup.MarginLayoutParams params, int margin) {
            params.topMargin = margin;
        }
    };

    private AnimationProxy mBottomAnimationProxy = new AnimationProxy() {
        @Override
        public ViewGroup getContainer() {
            return mBottomContainer;
        }

        @Override
        public int getMargin(ViewGroup.MarginLayoutParams params) {
            return params.bottomMargin;
        }

        @Override
        public void setMargin(ViewGroup.MarginLayoutParams params, int margin) {
            params.bottomMargin = margin;
        }
    };

    public PeekPopup(Context context) {
        super(context);
    }

    @Override
    protected View getContentView(LayoutInflater inflater, ViewGroup container) {
        View v = inflater.inflate(R.layout.dialog_peek, container, false);

        mTopContainer = (FrameLayout) v.findViewById(R.id.peek_popup_top_container);
        mBottomContainer = (FrameLayout) v.findViewById(R.id.peek_popup_bottom_container);

        for (Runnable worker : mWorkerListOnViewCreated) {
            worker.run();
        }

        if (mTopContainer.getChildCount() > 0) {
            mTopContainer.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View view,
                                           int left, int top, int right, int bottom,
                                           int oldLeft, int oldTop, int oldRight, int oldBottom
                ) {
                    showAnimation(mTopAnimationProxy, bottom - top);

                    mTopContainer.removeOnLayoutChangeListener(this);
                }
            });
        }

        if (mBottomContainer.getChildCount() > 0) {
            mBottomContainer.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View view,
                                           int left, int top, int right, int bottom,
                                           int oldLeft, int oldTop, int oldRight, int oldBottom
                ) {
                    showAnimation(mBottomAnimationProxy, bottom - top);

                    mBottomContainer.removeOnLayoutChangeListener(this);
                }
            });
        }

        mViewCreated = true;

        return v;
    }

    @Override
    public void hide() {
        AnimatorSet set = new AnimatorSet();
        List<Animator> animators = new ArrayList<>();

        if (mTopContainer.getChildCount() > 0) {
            animators.add(hideAnimation(mTopAnimationProxy));
        }

        if (mBottomContainer.getChildCount() > 0) {
            animators.add(hideAnimation(mBottomAnimationProxy));
        }

        set.playTogether(animators);
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                PeekPopup.super.hide();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });

        set.start();
    }

    public void setTopContent(ViewGenerator generator) {
        setContent(new ContainerGiver() {
            @Override
            public ViewGroup getContainer() {
                return mTopContainer;
            }
        }, generator);
    }

    public void setBottomContent(ViewGenerator generator) {
        setContent(new ContainerGiver() {
            @Override
            public ViewGroup getContainer() {
                return mBottomContainer;
            }
        }, generator);
    }

    private void showAnimation(final AnimationProxy proxy, int height) {
        Animator animator = startAnimation(proxy, height, true);
        animator.start();
    }

    private Animator hideAnimation(AnimationProxy proxy) {
        ViewGroup container = proxy.getContainer();

        return startAnimation(proxy, container.getHeight(), false);
    }

    private Animator startAnimation(final AnimationProxy proxy, int height, boolean start) {
        final ViewGroup container = proxy.getContainer();

        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) container.getLayoutParams();
        proxy.setMargin(params, height);
        container.setLayoutParams(params);

        int s = start ? proxy.getMargin(params) : 0;
        int e = start ? 0 : proxy.getMargin(params);

        ValueAnimator animator = ValueAnimator.ofInt(s * -1, e * -1);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) container.getLayoutParams();
                proxy.setMargin(params, (int) valueAnimator.getAnimatedValue());
                container.setLayoutParams(params);
            }
        });

        animator.setDuration(100);

        float degree = start ? 1.0f : 0;

        ValueAnimator dim = ValueAnimator.ofFloat(1.0f - degree, degree);
        dim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator va) {
                View v = getPopupContainer();
                v.setAlpha((float) va.getAnimatedValue());
            }
        });

        dim.setDuration(100);

        AnimatorSet set = new AnimatorSet();
        set.playTogether(animator, dim);

        return set;
    }

    private interface AnimationProxy {
        ViewGroup getContainer();

        int getMargin(ViewGroup.MarginLayoutParams params);

        void setMargin(ViewGroup.MarginLayoutParams params, int margin);
    }

    private void setContent(final ContainerGiver giver, final ViewGenerator generator) {
        Runnable worker = new Runnable() {
            @Override
            public void run() {
                ViewGroup container = giver.getContainer();
                View v = generator.generate(container);

                container.removeAllViews();
                container.addView(v);
            }
        };

        if (mViewCreated) {
            worker.run();
        } else {
            mWorkerListOnViewCreated.add(worker);
        }
    }

    private interface ContainerGiver {
        ViewGroup getContainer();
    }
}
