package rewhite.shopplus.common.popup;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.common.ValueWorker;
import rewhite.shopplus.common.listener.ViewGenerator;
import rewhite.shopplus.data.manager.DelayedWorkerManager;
import rewhite.shopplus.util.Utility;

/**
 * 화면 상단으로 밀려 내려오는 타이틀이 포함된 팝업 컨테이너
 */
public abstract class TopTitlePopup extends PeekPopup {
    private ViewGroup mRootContainer;
    private TextView mTitleText;

    private DelayedWorkerManager mWorkerManager;

    public TopTitlePopup(Context context) {
        super(context);

        mWorkerManager = new DelayedWorkerManager();

        setTopContent(new ViewGenerator() {
            @Override
            public View generate(ViewGroup container) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                View v = inflater.inflate(R.layout.popup_title_peek, container, false);

                mRootContainer = (ViewGroup)v.findViewById(R.id.popup_title_peek_root_container);

                if( checkTransparentStatus() ) {
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams)mRootContainer.getLayoutParams();
                    params.topMargin += Utility.getStatusBarHeight(getContext());

                    mRootContainer.setLayoutParams(params);
                }

                ViewGroup contentContainer = (ViewGroup)v.findViewById(R.id.popup_title_peek_content_container);
                contentContainer.addView(getTopContentView(contentContainer));

                v.findViewById(R.id.popup_title_peek_close_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hide();
                    }
                });

                mTitleText = (TextView)v.findViewById(R.id.popup_title_peek_title_text);

                mWorkerManager.run();

                return v;
            }
        });
    }

    public void setTitle(int resId) {
        mWorkerManager.work(new ValueWorker<Integer>(resId) {
            @Override
            public void run() {
                mTitleText.setText(getValue());
            }
        });
    }

    public void setTitle(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                mTitleText.setText(getValue());
            }
        });
    }

    protected abstract View getTopContentView(ViewGroup container);
}
