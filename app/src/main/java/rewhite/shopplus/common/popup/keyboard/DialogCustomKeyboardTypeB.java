package rewhite.shopplus.common.popup.keyboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

import rewhite.shopplus.R;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.util.CustomKeyboard;
import rewhite.shopplus.util.SharedPreferencesUtil;

/**
 * 세탁요금 변경 View
 */
public class DialogCustomKeyboardTypeB extends AppCompatActivity {
    private static final String TAG = "ActivityCustomTacKeyboard";
    private static final String BEFORE_CHANGE_MONEY = "before_change_money";
    private DecimalFormat decimalFormat = new DecimalFormat("#,###");

    private CustomKeyboard mCustomKeyboard;
    private EditText etChange;
    private String result = "";

    private String mMoneyAmount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_custom_keyboard_type_b);
        View view = findViewById(R.id.rootView);

        Intent intent = getIntent();
        String beforChangeMoney = intent.getExtras().getString(BEFORE_CHANGE_MONEY);

        setContentData(view, beforChangeMoney);
    }

    private void setContentData(View view, String beforChangeMoney) {
        LinearLayout llClose = (LinearLayout) findViewById(R.id.ll_close);
        Button btnAgree = (Button) findViewById(R.id.btn_agree);
        TextView tacNumber = (TextView) findViewById(R.id.tv_tak_number);

        etChange = (EditText) findViewById(R.id.et_settac_number);
        tacNumber.setText(beforChangeMoney);

        btnAgree.setOnClickListener(btnClickFinish);
        llClose.setOnClickListener(btnClickFinish);
        etChange.addTextChangedListener(textWatcher);
        mCustomKeyboard = new CustomKeyboard(this, view, R.id.keyboard_view, R.xml.custom_keyboard);
        mCustomKeyboard.registerEditText(view, R.id.et_settac_number);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!TextUtils.isEmpty(s.toString()) && !s.toString().equals(result)) {
                result = decimalFormat.format(Double.parseDouble(s.toString().replaceAll(",", "")));
                etChange.setText(result);
                etChange.setSelection(result.length());
                mMoneyAmount = s.toString();
            }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private View.OnClickListener btnClickFinish = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            if (id == R.id.ll_close) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(DialogCustomKeyboardTypeB.this);
                thirdConfirmPopup.setTitle("팝업종료");
                thirdConfirmPopup.setContent("세탁요금 변경 종료할까요?");
                thirdConfirmPopup.setButtonText("예");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        finish();
                    }
                });
                thirdConfirmPopup.show();
            } else if (id == R.id.btn_agree) {
                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(DialogCustomKeyboardTypeB.this);
                thirdAlertPopup.setTitle("알립 팝업");
                thirdAlertPopup.setContent("저장이 완료되었습니다.");
                thirdAlertPopup.setButtonText("확인");
                thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        SharedPreferencesUtil.putSharedPreference(getApplicationContext(), PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY, mMoneyAmount);
                        finish();
                    }
                });
                thirdAlertPopup.show();
            }
        }
    };
}