package rewhite.shopplus.common.popup.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.model.GetStoreManageInfo;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.model.StoreUserInfoItemList;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.client.network.PaymentManager;
import rewhite.shopplus.client.network.StoreUserApiManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.PaymenrWaitCounter;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.util.hardware.FirstHardware;
import rewhite.shopplus.view.adapter.AdapterInstallment;

/**
 * 선충전금 View
 */
public class ActivityPreFillGold extends AppCompatActivity {
    private static final String TAG = "ActivityPreFillGold";
    private static final String USER_STORE_ID = "user_store_id";    // UserStoreId
    private static int userStoreId;

    private List<StoreUserInfoItemList> storeUserInfoItemList;
    private List<GetVisitOrderList> getVisitOrderList;

    private boolean paymentSelect = true;       //true 현금 / false 카드
    private RecyclerView rvInstallment;
    private LinearLayout llInstallment, llClose;

    private Button btnOneThousandWon, btnThreeThousandWon, btnFiveThousandWon, btnTenThousandWon, btnThirtyThousandWon, btnFiftyThousandWon, btnInitializeAmount;
    private Button btnCash, btnCard, btnLumpSum, btnInstallment, btnMakePayment;

    private TextView tvMoneyCount, tvPaymentAmount, tvAmountToBeAccumulated, tvTotalChargeAmount;
    private TextView tvUserName, tvPreFilledGold, tvReserves, tvUserPhone, tvUserAddress, tvCardAccumulate, tvCashAccumulate;

    private int moneyCount;
    private Double cardMileageRate, cashMileageRate;

    private WaitCounter mWait;
    private PaymenrWaitCounter mPaymentWait;
    private View view = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_charging);
        view = findViewById(R.id.rootView);

        Intent intent = getIntent();
        userStoreId = intent.getExtras().getInt(USER_STORE_ID);

        mWait = new WaitCounter(this);
        mPaymentWait = new PaymenrWaitCounter(ActivityPreFillGold.this);
        init();
    }

    /**
     * 데이터 바인딩 init
     */
    private void init() {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    StoreUserApiManager.getStoreUserInfo(ActivityPreFillGold.this, Long.valueOf(userStoreId));
                    EnviromentManager.getManagerStoreManageInfo(ActivityPreFillGold.this);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                            storeUserInfoItemList = ManagerStoreUserInfo.getmInstance().getStoreUserInfo();
                            setLayout();
                            setInstallmentAdapter();
                            setTextView();
                            setClickListnner();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * LayoutSetting View
     */
    private void setLayout() {
        rvInstallment = (RecyclerView) findViewById(R.id.rv_installment);
        llClose = (LinearLayout) findViewById(R.id.ll_close);
        llInstallment = (LinearLayout) findViewById(R.id.ll_installment);

        tvMoneyCount = (TextView) findViewById(R.id.tv_money_count);
        tvPaymentAmount = (TextView) findViewById(R.id.tv_payment_amount);
        tvTotalChargeAmount = (TextView) findViewById(R.id.total_charge_amount);

        tvUserName = (TextView) findViewById(R.id.tv_user_name);
        tvPreFilledGold = (TextView) findViewById(R.id.tv_pre_filled_gold);
        tvReserves = (TextView) findViewById(R.id.tv_reserves);
        tvUserPhone = (TextView) findViewById(R.id.tv_user_phone);
        tvUserAddress = (TextView) findViewById(R.id.tv_user_address);
        tvCardAccumulate = (TextView) findViewById(R.id.tv_card_accumulate);
        tvCashAccumulate = (TextView) findViewById(R.id.tv_cash_accumulate);
        tvAmountToBeAccumulated = (TextView) findViewById(R.id.tv_amount_to_be_accumulated);

        btnCash = (Button) findViewById(R.id.btn_cash);
        btnCard = (Button) findViewById(R.id.btn_card);
        btnLumpSum = (Button) findViewById(R.id.btn_lump_sum);
        btnInstallment = (Button) findViewById(R.id.btn_installment);
        btnOneThousandWon = (Button) findViewById(R.id.btn_one_thousand_won);
        btnThreeThousandWon = (Button) findViewById(R.id.btn_three_thousand_won);
        btnFiveThousandWon = (Button) findViewById(R.id.btn_five_thousand_won);
        btnTenThousandWon = (Button) findViewById(R.id.btn_ten_thousand_won);
        btnThirtyThousandWon = (Button) findViewById(R.id.btn_thirty_thousand_won);
        btnFiftyThousandWon = (Button) findViewById(R.id.btn_fifty_thousand_won);
        btnInitializeAmount = (Button) findViewById(R.id.btn_initialize_amount);
        btnMakePayment = (Button) findViewById(R.id.btn_payment_use);
    }

    /**
     * Setting TextView Logic
     */
    private void setTextView() {
        tvUserName.setText(storeUserInfoItemList.get(0).getData().getUserName());
        tvUserPhone.setText(storeUserInfoItemList.get(0).getData().get_UserPhone());
        if (storeUserInfoItemList.get(0).getData().get_UserAddress() == null || TextUtils.isEmpty(storeUserInfoItemList.get(0).getData().get_UserAddress())) {
            tvUserAddress.setVisibility(View.GONE);
        } else {
            tvUserAddress.setText(storeUserInfoItemList.get(0).getData().get_UserAddress());
            tvUserAddress.setVisibility(View.VISIBLE);
        }
        tvPreFilledGold.setText(String.valueOf(storeUserInfoItemList.get(0).getData().getRealPrePaid()) + getResources().getString(R.string.won));
        tvReserves.setText(String.valueOf(storeUserInfoItemList.get(0).getData().getBonusPrePaid()) + getResources().getString(R.string.won));
        tvCardAccumulate.setText(""+ cardMileageRate + "%");
        tvCashAccumulate.setText(""+ cashMileageRate + "%");
    }

    /**
     * 선충전금 다이얼로그 View Setting
     */
    private void setClickListnner() {
        btnCash.setOnClickListener(btnMoneyCountListener);
        btnCard.setOnClickListener(btnMoneyCountListener);
        btnLumpSum.setOnClickListener(btnMoneyCountListener);
        btnInstallment.setOnClickListener(btnMoneyCountListener);

        btnOneThousandWon.setOnTouchListener(btnOnTouchListener);
        btnThreeThousandWon.setOnTouchListener(btnOnTouchListener);
        btnFiveThousandWon.setOnTouchListener(btnOnTouchListener);
        btnTenThousandWon.setOnTouchListener(btnOnTouchListener);
        btnThirtyThousandWon.setOnTouchListener(btnOnTouchListener);
        btnFiftyThousandWon.setOnTouchListener(btnOnTouchListener);

        llClose.setOnClickListener(btnClickListener);
        btnInitializeAmount.setOnClickListener(btnClickListener);

        btnMakePayment.setOnClickListener(btnMoneyCountListener);
    }

    /**
     * 할부 선택 Adapter SettingView
     */
    private void setInstallmentAdapter() {
        List<GetStoreManageInfo> manageInfos = ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList();
        cardMileageRate = manageInfos.get(0).getData().getCardBonusPrePaidRate();
        cashMileageRate = manageInfos.get(0).getData().getCashBonusPrePaidRate();

        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(11, StaggeredGridLayoutManager.VERTICAL);
        rvInstallment.setLayoutManager(layoutManager);
        AdapterInstallment adapterInstallment = new AdapterInstallment(getApplicationContext(), mOnAdapterListner);

        ArrayList<String> arrayListNumber = new ArrayList<>();
        for (int i = 2; i <= 12; i++) {
            arrayListNumber.add(String.valueOf(i));
        }
        adapterInstallment.addItem(arrayListNumber);
        rvInstallment.setAdapter(adapterInstallment);
    }

    private View.OnClickListener btnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ll_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityPreFillGold.this);
                    thirdConfirmPopup.setTitle("종료팝업");
                    thirdConfirmPopup.setContent("팝업을 종료할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setCancelButtonText("아니오");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdConfirmPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                            finish();
                        }
                    });

                    thirdConfirmPopup.show();
                    break;

                case R.id.btn_initialize_amount:
                    moneyCount = 0;
                    tvMoneyCount.setText("0");
                    tvPaymentAmount.setText("0" + getResources().getString(R.string.won));
                    tvAmountToBeAccumulated.setText("0" + getResources().getString(R.string.won));
                    tvTotalChargeAmount.setText("0" + getResources().getString(R.string.won));
                    break;
            }
        }
    };


    /**
     * 현금 & 카드 UI변경 로직 Listenener
     */
    private View.OnTouchListener btnOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int id = v.getId();
            switch (event.getAction()) {

                case MotionEvent.ACTION_DOWN:
                    switch (id) {
                        case R.id.btn_one_thousand_won:
                            btnOneThousandWon.setBackgroundResource(R.drawable.a_button_type_2);
                            btnOneThousandWon.setTextColor(getResources().getColor(R.color.Wite_color));
                            break;

                        case R.id.btn_three_thousand_won:
                            btnThreeThousandWon.setBackgroundResource(R.drawable.a_button_type_2);
                            btnThreeThousandWon.setTextColor(getResources().getColor(R.color.Wite_color));
                            break;

                        case R.id.btn_five_thousand_won:
                            btnFiveThousandWon.setBackgroundResource(R.drawable.a_button_type_2);
                            btnFiveThousandWon.setTextColor(getResources().getColor(R.color.Wite_color));
                            break;

                        case R.id.btn_ten_thousand_won:
                            btnTenThousandWon.setBackgroundResource(R.drawable.a_button_type_2);
                            btnTenThousandWon.setTextColor(getResources().getColor(R.color.Wite_color));
                            break;

                        case R.id.btn_thirty_thousand_won:
                            btnThirtyThousandWon.setBackgroundResource(R.drawable.a_button_type_2);
                            btnThirtyThousandWon.setTextColor(getResources().getColor(R.color.Wite_color));
                            break;

                        case R.id.btn_fifty_thousand_won:
                            btnFiftyThousandWon.setBackgroundResource(R.drawable.a_button_type_2);
                            btnFiftyThousandWon.setTextColor(getResources().getColor(R.color.Wite_color));
                            break;
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    switch (id) {
                        case R.id.btn_one_thousand_won:
                            moneyCount += 1000;
                            tvMoneyCount.setText(DataFormatUtil.moneyFormatToWon(moneyCount));
                            btnOneThousandWon.setBackgroundResource(R.drawable.a_button_type_3);
                            btnOneThousandWon.setTextColor(getResources().getColor(R.color.color_43425c));
                            setMileageRate();
                            break;

                        case R.id.btn_three_thousand_won:
                            moneyCount += 3000;
                            tvMoneyCount.setText(DataFormatUtil.moneyFormatToWon(moneyCount));
                            btnThreeThousandWon.setBackgroundResource(R.drawable.a_button_type_3);
                            btnThreeThousandWon.setTextColor(getResources().getColor(R.color.color_43425c));
                            setMileageRate();
                            break;

                        case R.id.btn_five_thousand_won:
                            moneyCount += 5000;
                            tvMoneyCount.setText(DataFormatUtil.moneyFormatToWon(moneyCount));
                            btnFiveThousandWon.setBackgroundResource(R.drawable.a_button_type_3);
                            btnFiveThousandWon.setTextColor(getResources().getColor(R.color.color_43425c));
                            setMileageRate();
                            break;

                        case R.id.btn_ten_thousand_won:
                            moneyCount += 10000;
                            tvMoneyCount.setText(DataFormatUtil.moneyFormatToWon(moneyCount));
                            btnTenThousandWon.setBackgroundResource(R.drawable.a_button_type_3);
                            btnTenThousandWon.setTextColor(getResources().getColor(R.color.color_43425c));
                            setMileageRate();
                            break;

                        case R.id.btn_thirty_thousand_won:
                            moneyCount += 30000;
                            tvMoneyCount.setText(DataFormatUtil.moneyFormatToWon(moneyCount));
                            btnThirtyThousandWon.setBackgroundResource(R.drawable.a_button_type_3);
                            btnThirtyThousandWon.setTextColor(getResources().getColor(R.color.color_43425c));
                            setMileageRate();
                            break;

                        case R.id.btn_fifty_thousand_won:
                            moneyCount += 50000;
                            tvMoneyCount.setText(DataFormatUtil.moneyFormatToWon(moneyCount));
                            btnFiftyThousandWon.setBackgroundResource(R.drawable.a_button_type_3);
                            btnFiftyThousandWon.setTextColor(getResources().getColor(R.color.color_43425c));
                            setMileageRate();
                            break;
                    }
                    break;
            }
            return false;
        }
    };

    /**
     * 선충전금 다이얼로그 ClickListener
     */
    private View.OnClickListener btnMoneyCountListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.btn_cash:
                    paymentSelect = true;
                    btnCash.setTextColor(getResources().getColor(R.color.Wite_color));
                    btnCash.setBackgroundResource(R.drawable.a_button_type_2);

                    btnCard.setTextColor(getResources().getColor(R.color.color_43425c));
                    btnCard.setBackgroundResource(R.drawable.a_button_type_3);
                    rvInstallment.setVisibility(View.GONE);
                    llInstallment.setVisibility(View.GONE);
                    break;
                case R.id.btn_card:
                    paymentSelect = false;

                    btnCard.setTextColor(getResources().getColor(R.color.Wite_color));
                    btnCard.setBackgroundResource(R.drawable.a_button_type_2);

                    btnCash.setTextColor(getResources().getColor(R.color.color_43425c));
                    btnCash.setBackgroundResource(R.drawable.a_button_type_3);
                    rvInstallment.setVisibility(View.VISIBLE);
                    llInstallment.setVisibility(View.VISIBLE);

                    break;
                case R.id.btn_lump_sum:
                    rvInstallment.setVisibility(View.GONE);
                    btnLumpSum.setTextColor(getResources().getColor(R.color.Wite_color));
                    btnLumpSum.setBackgroundResource(R.drawable.a_button_type_2);

                    btnInstallment.setTextColor(getResources().getColor(R.color.color_43425c));
                    btnInstallment.setBackgroundResource(R.drawable.a_button_type_3);

                    break;
                case R.id.btn_installment:
                    rvInstallment.setVisibility(View.VISIBLE);
                    btnInstallment.setTextColor(getResources().getColor(R.color.Wite_color));
                    btnInstallment.setBackgroundResource(R.drawable.a_button_type_2);

                    btnLumpSum.setTextColor(getResources().getColor(R.color.color_43425c));
                    btnLumpSum.setBackgroundResource(R.drawable.a_button_type_3);

                    break;
                case R.id.btn_payment_use:
                    if (moneyCount == 0) {
                        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityPreFillGold.this);
                        thirdAlertPopup.setTitle("선충전금 충전 오류");
                        thirdAlertPopup.setContent("충전금액을 입력해주세요.");
                        thirdAlertPopup.setButtonText("확인");
                        thirdAlertPopup.show();
                    } else {
                        ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityPreFillGold.this);
                        thirdConfirmPopup.setTitle("현금충전 안내");
                        if (paymentSelect) {
                            thirdConfirmPopup.setContent("현금충전을 진행하시겠습니까?");
                            thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    setPayment();
                                }
                            });
                            thirdConfirmPopup.show();
                        } else {
                            thirdConfirmPopup.setTitle("카드충전 안내");
                            thirdConfirmPopup.setContent("카드충전을 진행하시겠습니까?");
                            thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    setCardPayment();
                                }
                            });
                            thirdConfirmPopup.show();
                        }
                        break;
                    }
            }
        }
    };

    /**
     * 카드 결제 로직 및 예외처리 로직
     */
    private void setCardPayment() {
        try {
            mPaymentWait.show();
            mPaymentWait.setMessage("카드 결재 진행중...");
            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityPreFillGold.this);//FirstData 결제 로직

            new Thread(new Runnable() {
                public void run() {
                    String requestCode = FirstHardware.getPreFilledCardPrint(ActivityPreFillGold.this, moneyCount, tvPaymentAmount.getText().toString(), tvAmountToBeAccumulated.getText().toString(), tvTotalChargeAmount.getText().toString());
                    runOnUiThread(new Runnable() {
                        public void run() {
                            mPaymentWait.dismiss();
                            if (requestCode.equals("0")) {
                                thirdAlertPopup.setTitle("결제완료");
                                thirdAlertPopup.setContent("결제가 완료되었어요.");
                                thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                    @Override
                                    public void onConfirmClick() {
                                        finish();
                                    }
                                });
                                thirdAlertPopup.show();
                            } else {
                                mPaymentWait.dismiss();
                                thirdAlertPopup.setTitle("결제실패");
                                thirdAlertPopup.setContent("카드단말기에서 응답이 없어요.\n" + "다시 시도해주세요.");
                                thirdAlertPopup.show();
                            }
                        }
                    });
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 서버 및 실제 VAN결제 진행 로직
     */
    private void setPayment() {
        mWait.show();
        final String[] ResultCode = new String[1];
        new Thread(new Runnable() {
            public void run() {
                ResultCode[0] = PaymentManager.getManagerChargeStoreUserPrePaid(ActivityPreFillGold.this, storeUserInfoItemList.get(0).getData().getStoreUserID(), 1, moneyCount);
                runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        if (ResultCode[0].equals("S0000")) {
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityPreFillGold.this);
                            thirdAlertPopup.setTitle("충전완료 안내");
                            thirdAlertPopup.setContent("현금 충전이 완료 되었습니다.");
                            thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    rewhite.shopplus.util.hardware.FirstHardware.getPreFilledGoldPrint(tvPaymentAmount.getText().toString(), tvAmountToBeAccumulated.getText().toString(), tvTotalChargeAmount.getText().toString());
                                    finish();
                                }
                            });
                            thirdAlertPopup.show();
                        } else {
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityPreFillGold.this);
                            thirdAlertPopup.setTitle("충전오류 안내");
                            thirdAlertPopup.setContent("충전 실패 하였습니다.\n 잠시후 다시시도 해주세요.");
                            thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    finish();
                                }
                            });
                            thirdAlertPopup.show();
                        }
                    }
                });
            }
        }).start();
    }

    /**
     * 마일리지 계산 로직
     */
    private void setMileageRate() {
        tvPaymentAmount.setText(DataFormatUtil.moneyFormatToWon(moneyCount) + getResources().getString(R.string.won));
        if (paymentSelect) {
            tvAmountToBeAccumulated.setText(DataFormatUtil.moneyFormatToWon(Math.round(moneyCount * cashMileageRate / 100)) + getResources().getString(R.string.won));
            tvTotalChargeAmount.setText(DataFormatUtil.moneyFormatToWon(moneyCount + Math.round(moneyCount * cashMileageRate / 100)) + getResources().getString(R.string.won));
        } else {
            tvAmountToBeAccumulated.setText(DataFormatUtil.moneyFormatToWon(Math.round(moneyCount * cardMileageRate / 100)) + getResources().getString(R.string.won));
            tvTotalChargeAmount.setText(DataFormatUtil.moneyFormatToWon(moneyCount + Math.round(moneyCount * cardMileageRate / 100)) + getResources().getString(R.string.won));
        }
    }

    /**
     * 할부 선택 Adapter InterFace
     */
    private AdapterInstallment.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterInstallment.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {
        }
    };
}
