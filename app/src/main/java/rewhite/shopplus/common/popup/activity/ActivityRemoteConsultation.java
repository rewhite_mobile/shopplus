package rewhite.shopplus.common.popup.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerAuthStoreLogin;
import rewhite.shopplus.client.network.CustomerManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;


public class ActivityRemoteConsultation extends AppCompatActivity {
    private static final String TAG = "ActivityRemoteConsultation";
    private WaitCounter mWait;
    private Button btnServiceAgree;

    private TextView tvBusinessName;
    private EditText etPhonnumber, etConsultationRequestContents;

    private WebView webViewContent;

    private boolean mIsChecked;
    private CheckBox cbAcceptTerms;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_activity_remote_consultation);
        mWait = new WaitCounter(this);

        setLayout();
    }

    private void setLayout() {
        tvBusinessName = (TextView) findViewById(R.id.tv_business_name);

        etPhonnumber = (EditText) findViewById(R.id.et_phonnumber);
        etConsultationRequestContents = (EditText) findViewById(R.id.et_consultation_request_contents);

        webViewContent = (WebView) findViewById(R.id.web_view);

        btnServiceAgree = (Button) findViewById(R.id.btn_service_agree);
        LinearLayout llCheckAcceptTerms = (LinearLayout) findViewById(R.id.ll_check_accept_terms);
        cbAcceptTerms = (CheckBox) findViewById(R.id.cb_accept_terms);
        LinearLayout llClose = (LinearLayout) findViewById(R.id.ll_close);

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityRemoteConsultation.this);
                thirdConfirmPopup.setTitle("종료 팝업");
                thirdConfirmPopup.setButtonText("예");
                thirdConfirmPopup.setCancelButtonText("아니오");
                thirdConfirmPopup.setContent("원격상담신청을 종료할까요?");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        finish();
                    }
                });
                thirdConfirmPopup.show();
            }
        });
        cbAcceptTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Logger.d(TAG, "isChecked : " + isChecked);
                mIsChecked = isChecked;
            }
        });

        btnServiceAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityRemoteConsultation.this);
                if (etPhonnumber.getText().toString().equals("") || TextUtils.isEmpty(etPhonnumber.getText().toString())) {
                    thirdAlertPopup.setTitle("입력오류");
                    thirdAlertPopup.setContent("핸드폰 번호를 입력해주세요.");
                    thirdAlertPopup.show();
                } else if (!mIsChecked) {
                    thirdAlertPopup.setTitle("입력오류");
                    thirdAlertPopup.setContent("약관 동의를 해주세요.");
                    thirdAlertPopup.show();
                } else {
                    requestConsultation();
                }
            }
        });
        setTextView();
    }

    private void setTextView() {
        tvBusinessName.setText(ManagerAuthStoreLogin.getmInstance().getmanagerAuthStoreLogin().get(0).getData().getStoreName());
        etPhonnumber.setText(ManagerAuthStoreLogin.getmInstance().getmanagerAuthStoreLogin().get(0).getData().getStoreTelephone());
    }

    private void requestConsultation() {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    CustomerManager.getRequsetCounsel(ActivityRemoteConsultation.this, ManagerAuthStoreLogin.getmInstance().getmanagerAuthStoreLogin().get(0).getData().getStoreID(),
                            ManagerAuthStoreLogin.getmInstance().getmanagerAuthStoreLogin().get(0).getData().getStoreName(), etPhonnumber.getText().toString(), etConsultationRequestContents.getText().toString());
                    runOnUiThread(new Runnable() {
                        public void run() {
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityRemoteConsultation.this);
                            thirdAlertPopup.setTitle("접수완료");
                            thirdAlertPopup.setContent("상담신청 접수 완료되었어요.\n" + "접수 순서대로 연락드릴 예정입니다.");
                            thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    finish();
                                }
                            });
                            thirdAlertPopup.show();
                            mWait.dismiss();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
