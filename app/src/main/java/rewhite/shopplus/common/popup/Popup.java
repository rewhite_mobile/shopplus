package rewhite.shopplus.common.popup;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JsResult;

import java.util.Timer;
import java.util.TimerTask;

import rewhite.shopplus.R;
import rewhite.shopplus.common.ValueWorker;
import rewhite.shopplus.common.activity.TransparentActivity;
import rewhite.shopplus.common.listener.OnBackPressedListener;
import rewhite.shopplus.data.manager.DelayedWorkerManager;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.ScreenLighter;

/**
 * 모든 팝업의 추상 클래스
 */
public abstract class Popup {
    private Context mContext;
    private Dialog mDialog;
    private ViewGroup mContainer;
    private Options mOptions;
    private boolean mTransparentStatusBar;
    private boolean mCanBackPressed = true;

    private ScreenLighter mScreenLighter;
    private boolean mLighterOn;

    private DelayedWorkerManager mDialogWorkerManager = new DelayedWorkerManager();

    private OnBackPressedListener mOnBackPressedListener = new OnBackPressedListener() {
        @Override
        public boolean onBackPressed() {
            if( isShowing() && mCanBackPressed) {
                hide();
                return true;
            } else {
                return false;
            }
        }
    };


    public Popup(Context context) {
        mContext = context;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        mContainer = (ViewGroup)inflater.inflate(R.layout.dialog_base, null);
    }

    private Dialog createDialog() {
        PopupDialog dialog = new PopupDialog(mContext, R.style.Popup, this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(mContainer);
        dialog.setCancelable(false);
        dialog.setOnBackPressedListener(mOnBackPressedListener);

        if( checkTransparentStatus() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ) {
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        mScreenLighter = new ScreenLighter(dialog.getWindow());

        return dialog;
    }

    public void setCanBackPressed(boolean canBackPressed) {
        mCanBackPressed = canBackPressed;
    }

    protected boolean checkTransparentStatus() {
        return mTransparentStatusBar || mContext instanceof TransparentActivity && ((TransparentActivity)mContext).getStatusBarHeight() > 0;
    }

    public void setScreenLighterEnabled(boolean b) {
        mLighterOn = b;
    }

    public void setTransparentStatusBar(boolean b) {
        mTransparentStatusBar = b;
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void show() {
        Logger.i("Popup", "show() return");
        if(mContext == null || !(mContext instanceof AppCompatActivity) || ((AppCompatActivity)mContext).isFinishing()){
            return;
        }
        Logger.i("Popup", "show()");

        if( mDialog == null ) {
            mDialog = createDialog();
            mDialogWorkerManager.run();
        }

        if( mContainer.getChildCount() <= 0 ) {
            mContainer.addView(getContentView(LayoutInflater.from(mDialog.getContext()), mContainer));
        }

        if( !mDialog.isShowing() ) {
            if( mOptions != null ) {
                if( mOptions.mTimeoutMilliseconds > 0 ) {
                    startTimeout(mOptions.mTimeoutMilliseconds);
                }
            }

            if( mLighterOn ) {
                mScreenLighter.enlighten();
            }

            mDialog.show();
        }
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        mDialogWorkerManager.work(new ValueWorker<DialogInterface.OnDismissListener>(listener) {
            @Override
            public void run() {
                mDialog.setOnDismissListener(getValue());
            }
        });
    }

    public void setOptions(Options options) {
        mOptions = options;
    }

    public void show(Options options) {
        mOptions = options;

        show();
    }

    public void hide() {
        if( mDialog != null && mDialog.isShowing() ) {
            mDialog.dismiss();
            mDialog = null;

            if( mLighterOn ) {
                mScreenLighter.restore();
            }
        }
    }

    public Context getContext() {
        return mContext;
    }

    public ViewGroup getPopupContainer() {
        return mContainer;
    }

    public Dialog getDialog() {
        return mDialog;
    }

    private void startTimeout(long ms) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                hide();
            }
        }, ms);
    }

    private JsResult mResult = null;
    public void setJsResult(JsResult result) {
        mResult = result;
    }

    public void sendJsResultConfirm(){
        if(mResult != null){
            mResult.confirm();
            mResult = null;
        }
    }

    public void sendJsResultCancel(){
        if(mResult != null){
            mResult.cancel();
            mResult = null;
        }
    }


    public void sendJsResult(){

    }

    private static class PopupDialog extends Dialog {
        private OnBackPressedListener mOnBackPressedListener;
        private Popup mDlg;

        public PopupDialog(Context context) {
            super(context);
        }

        public PopupDialog(Context context, int style, Popup popDlg) {
            super(context, style);
            mDlg = popDlg;
        }

        @Override
        public void onBackPressed() {
            if( mOnBackPressedListener != null && mOnBackPressedListener.onBackPressed() ) {
                return;
            }

            super.onBackPressed();
        }

        public void setOnBackPressedListener(OnBackPressedListener listener) {
            mOnBackPressedListener = listener;
        }
    }

    protected abstract View getContentView(LayoutInflater inflater, ViewGroup container);

    public static class Options {
        private long mTimeoutMilliseconds = 0;

        public Options timeout(long ms) {
            mTimeoutMilliseconds = ms;
            return this;
        }
    }
}