package rewhite.shopplus.common.popup.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import rewhite.shopplus.R;
import rewhite.shopplus.client.network.AuthManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.util.ServerErrPopup;
import rewhite.shopplus.util.WaitCounter;

/**
 * ID / PW 찾기 View
 */
public class ActivityIdPwChange extends AppCompatActivity {
    private static final String TAG = "ActivityIdPwChange";
    private WaitCounter mWait;

    private EditText etIdMobilePhoneNumber, etPwMobilePhoneNumber;

    private Button btnIdMobilePhoneNumberSend, btnPwMobilePhoneNumberSend;
    private ImageView imgClose;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_id_pw_change);
        mWait = new WaitCounter(this);

        setLayout();
    }

    /**
     * 레이아웃 셋팅
     */
    private void setLayout() {
        etIdMobilePhoneNumber = (EditText) findViewById(R.id.et_id_mobile_phone_number);
        //핸드폰 번호 입력 변환
        etIdMobilePhoneNumber.setInputType(android.text.InputType.TYPE_CLASS_PHONE);
        etIdMobilePhoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        etPwMobilePhoneNumber = (EditText) findViewById(R.id.et_pw_mobile_phone_number);
        //핸드폰 번호 입력 변환
        etPwMobilePhoneNumber.setInputType(android.text.InputType.TYPE_CLASS_PHONE);
        etPwMobilePhoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        btnIdMobilePhoneNumberSend = (Button) findViewById(R.id.btn_id_mobile_phone_number_send);
        btnPwMobilePhoneNumberSend = (Button) findViewById(R.id.btn_pw_mobile_phone_number_send);
        imgClose = (ImageView) findViewById(R.id.img_close);

        btnIdMobilePhoneNumberSend.setOnClickListener(btnOnClickListner);
        btnPwMobilePhoneNumberSend.setOnClickListener(btnOnClickListner);
        imgClose.setOnClickListener(btnOnClickListner);
    }

    private View.OnClickListener btnOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityIdPwChange.this);
            thirdAlertPopup.setTitle("오류팝업");
            switch (id) {
                case R.id.btn_id_mobile_phone_number_send:
                    if (etIdMobilePhoneNumber.getText().toString().replace("-", "") == null || TextUtils.isEmpty(etIdMobilePhoneNumber.getText().toString().replace("-", ""))
                            || etIdMobilePhoneNumber.getText().toString().replace("-", "").equals("") || etIdMobilePhoneNumber.getText().toString().replace("-", "").length() == 0) {
                        thirdAlertPopup.setContent("올바른 휴대폰번호를 입력하세요. ");
                        thirdAlertPopup.show();
                    } else if (etIdMobilePhoneNumber.getText().toString().replace("-", "").length() != 11) {
                        setIdTextView();
                        thirdAlertPopup.setContent("올바른 휴대폰번호를 입력하세요.");
                        thirdAlertPopup.show();
                    } else {
                        idMobileNUmberSend();
                    }
                    break;

                case R.id.btn_pw_mobile_phone_number_send:
                    if (etPwMobilePhoneNumber.getText().toString().replace("-", "") == null || TextUtils.isEmpty(etPwMobilePhoneNumber.getText().toString().replace("-", ""))
                            || etPwMobilePhoneNumber.getText().toString().replace("-", "").equals("") || etPwMobilePhoneNumber.getText().toString().replace("-", "").length() == 0) {
                        thirdAlertPopup.setContent("휴대폰번호를 입력해주세요.");
                        thirdAlertPopup.show();
                    } else if (etPwMobilePhoneNumber.getText().toString().replace("-", "").length() != 11) {
                        setPwTextView();
                        thirdAlertPopup.setContent("휴대폰번호를 입력해주세요.");
                        thirdAlertPopup.show();
                    } else {
                        pwMobileNUmberSend();
                    }
                    break;

                case R.id.img_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityIdPwChange.this);
                    thirdConfirmPopup.setTitle("팝업종료");
                    thirdConfirmPopup.setContent("아이디/비밀번호 찾기 \n팝업을 종료할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setCancelButtonText("아니요");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            finish();
                        }
                    });
                    thirdConfirmPopup.show();
                    break;
            }
        }
    };

    /**
     * 아이디 찾기 휴대폰 번호 발송
     */
    private void idMobileNUmberSend() {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    String resultCode = AuthManager.getgetFindLoginID(ActivityIdPwChange.this, etIdMobilePhoneNumber.getText().toString().replace("-", ""));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (resultCode.contains("F")) {
                                setIdTextView();
                                ServerErrPopup.idPwSerch(ActivityIdPwChange.this, resultCode);
                            } else {
                                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityIdPwChange.this);
                                thirdAlertPopup.setTitle("전송완료");
                                thirdAlertPopup.setContent("아이디 전송되었어요.");
                                thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                    @Override
                                    public void onConfirmClick() {
                                        finish();
                                    }
                                });
                                thirdAlertPopup.show();
                            }
                            mWait.dismiss();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * 비밀번호 찾기 휴대폰 번호 발송
     */
    private void pwMobileNUmberSend() {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    String resultCode = AuthManager.getFindLoginPassword(ActivityIdPwChange.this, etPwMobilePhoneNumber.getText().toString().replace("-", ""));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (resultCode.contains("F")) {
                                setPwTextView();
                                ServerErrPopup.idPwSerch(ActivityIdPwChange.this, resultCode);
                            } else {
                                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityIdPwChange.this);
                                thirdAlertPopup.setTitle("전송완료");
                                thirdAlertPopup.setContent("패스워드 전송되었어요.");
                                thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                    @Override
                                    public void onConfirmClick() {
                                        finish();
                                    }
                                });
                                thirdAlertPopup.show();
                            }
                            mWait.dismiss();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    /**
     * ID 입력 데이터 초기화
     */
    private void setIdTextView(){
        etIdMobilePhoneNumber.setText("");
    }

    /**
     * PW 입력 데이터 초기화
     */
    private void setPwTextView(){
        etPwMobilePhoneNumber.setText("");
    }
}
