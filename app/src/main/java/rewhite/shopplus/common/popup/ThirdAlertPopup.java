package rewhite.shopplus.common.popup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.common.ValueWorker;
import rewhite.shopplus.data.manager.DelayedWorkerManager;

/**
 * 확인 버튼만 존재하는 알림 팝업
 */
public class ThirdAlertPopup extends Popup {
    protected OnConfirmClickListener mOnConfirmClickListener;

    protected TextView mTitleText;
    protected TextView mContentText;
    protected Button mConfirmButton;

    protected TextView popup_alert_content_text_top;
    protected TextView popup_alert_content_text_bottom;
    protected LinearLayout third_popup_alert_line;
    protected TextView popup_alert_content_text_sub;

    protected ImageView iv_popup_error;

    protected DelayedWorkerManager mWorkerManager;

    public ThirdAlertPopup(Context context) {
        super(context);
        mWorkerManager = new DelayedWorkerManager();
    }

    @Override
    protected View getContentView(LayoutInflater inflater, ViewGroup container) {
        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.dialog_third_alert, container, false);

        setButtons(v);
        mTitleText = (TextView) v.findViewById(R.id.popup_alert_title_text);
        mContentText = (TextView) v.findViewById(R.id.popup_alert_content_text);

        popup_alert_content_text_top = (TextView) v.findViewById(R.id.popup_alert_content_text_top);
        popup_alert_content_text_bottom = (TextView) v.findViewById(R.id.popup_alert_content_text_bottom);
        third_popup_alert_line = (LinearLayout) v.findViewById(R.id.third_popup_alert_line);
        popup_alert_content_text_sub = (TextView) v.findViewById(R.id.popup_alert_content_text_sub);

        iv_popup_error = (ImageView) v.findViewById(R.id.iv_popup_error);

        mWorkerManager.run();

        return v;
    }

    public ThirdAlertPopup setOnConfirmClickListener(OnConfirmClickListener listener) {
        mOnConfirmClickListener = listener;
        return this;
    }

    public ThirdAlertPopup setTitle(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                mTitleText.setText(getValue());
            }
        });

        return this;
    }

    public ThirdAlertPopup setTitle(int resId) {
        mWorkerManager.work(new ValueWorker<Integer>(resId) {
            @Override
            public void run() {
                mTitleText.setText(getValue());
            }
        });

        return this;
    }

    public ThirdAlertPopup setContent(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                mContentText.setText(getValue());
            }
        });

        return this;
    }

    public ThirdAlertPopup setContent(int resId) {
        mWorkerManager.work(new ValueWorker<Integer>(resId) {
            @Override
            public void run() {
                mContentText.setText(getValue());
            }
        });

        return this;
    }

    public ThirdAlertPopup setButtonText(int resId) {
        mWorkerManager.work(new ValueWorker<Integer>(resId) {
            @Override
            public void run() {
                mConfirmButton.setText(getValue());
            }
        });
        return this;
    }

    public ThirdAlertPopup setButtonText(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                mConfirmButton.setText(getValue());
            }
        });
        return this;
    }

    protected void setButtons(ViewGroup container) {
        mConfirmButton = (Button) container.findViewById(R.id.popup_alert_confirm_button);
        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide();

                if (mOnConfirmClickListener != null) {
                    mOnConfirmClickListener.onConfirmClick();
                }
            }
        });

    }

    public interface OnConfirmClickListener {
        void onConfirmClick();
    }

    public ThirdAlertPopup setTextTop(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                popup_alert_content_text_top.setVisibility(View.VISIBLE);
                popup_alert_content_text_top.setText(getValue());
            }
        });
        return this;
    }

    public ThirdAlertPopup setTextBottom(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                popup_alert_content_text_bottom.setVisibility(View.VISIBLE);
                popup_alert_content_text_bottom.setText(getValue());
            }
        });
        return this;
    }

    public ThirdAlertPopup setTextSub(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                third_popup_alert_line.setVisibility(View.VISIBLE);
                popup_alert_content_text_sub.setVisibility(View.VISIBLE);
                popup_alert_content_text_sub.setText(getValue());
            }
        });
        return this;
    }

    public ThirdAlertPopup setVisibility(boolean b) {
        mWorkerManager.work(new ValueWorker<Boolean>(b) {
            @Override
            public void run() {
                if (true) {
                    iv_popup_error.setVisibility(View.VISIBLE);
                }
            }
        });
        return this;
    }

}
