package rewhite.shopplus.common.popup.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerStorePaymentCancel;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.model.StorePaymentCancelAvailList;
import rewhite.shopplus.client.network.PaymentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.PaymenrWaitCounter;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.util.hardware.FirstHardware;
import rewhite.shopplus.view.adapter.AdapterPaymentCancle;

/**
 * 결제취소 DialogClass
 */
public class ActivityCancelPayment extends AppCompatActivity {
    private static final String TAG = "ActivityCancelPayment";

    private ListView lvItemCancle;
    private WaitCounter mWait;
    private PaymenrWaitCounter mPaymentWait;

    private TextView tvSubTitleUserName;
    private LinearLayout llItemCancle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_payment);

        mWait = new WaitCounter(ActivityCancelPayment.this);
        mPaymentWait = new PaymenrWaitCounter(ActivityCancelPayment.this);
        init();
        FirstHardware.init(this);
    }

    /**
     * 데이터 init
     */
    private void init() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                PaymentManager.getPaymentCancelAvailList(ActivityCancelPayment.this, Integer.valueOf(SettingLogic.getStoreUserId(getApplicationContext())));
                runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        setLayout();
                        List<StorePaymentCancelAvailList> paymentCancelAvailList = ManagerStorePaymentCancel.getmInstance().getPaymentCancelAvailList();

                        if (paymentCancelAvailList.size() == 0) {
                            llItemCancle.setVisibility(View.VISIBLE);
                            lvItemCancle.setVisibility(View.GONE);
                        } else {
                            AdapterPaymentCancle adapterItemCancle = new AdapterPaymentCancle(getApplicationContext(), canclePayment);

                            adapterItemCancle.addItem(paymentCancelAvailList);
                            lvItemCancle.setAdapter(adapterItemCancle);
                            lvItemCancle.setVisibility(View.VISIBLE);
                            llItemCancle.setVisibility(View.GONE);
                        }
                    }
                });
            }
        }).start();
    }

    /**
     * 레이아웃 셋팅 view
     */
    private void setLayout() {
        lvItemCancle = (ListView) findViewById(R.id.lv_payment_cancle);
        LinearLayout llCancle = (LinearLayout) findViewById(R.id.ll_close);
        llItemCancle = (LinearLayout) findViewById(R.id.ll_item_cancle);
        tvSubTitleUserName = (TextView) findViewById(R.id.tv_sub_title_user_name);
        tvSubTitleUserName.setText(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName());

        llCancle.setOnClickListener(btnOnClickListener);
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.ll_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityCancelPayment.this);
                    thirdConfirmPopup.setTitle("종료팝업");
                    thirdConfirmPopup.setContent("팝업을 종료할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setCancelButtonText("아니오");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            finish();
                        }
                    });
                    thirdConfirmPopup.show();
                    break;
            }
        }
    };

    /**
     * 결제 취소 로직
     */
    public interface canclePayment {
        // 현금 / 카드 결제취소
        void cancleId(int paymentID, int paymentType, String TID, String PayYMD, int paymentPrice);

        // 현금 + 카드 결제 취소
        void canclePayCard(int paymentID, String TID, String ReceiptID, String PayYMD, int paymentPrice);
    }

    private canclePayment canclePayment = new canclePayment() {
        @Override
        public void cancleId(int paymentID, int paymentType, String receiptID, String PayYMD, int paymentPrice) {
            final String[] requestCode = {null};//FirstData 결제 로직
            final String[] apiResponseCode = {null};

            new Thread(new Runnable() {
                public void run() {
                    try {
                        if (paymentType == 1) {
                            //현금 결제 취소
                            apiResponseCode[0] = PaymentManager.getCancelStorePayment(ActivityCancelPayment.this, Long.valueOf(paymentID));

                            if(receiptID.equals("") || TextUtils.isEmpty(receiptID)){
                                requestCode[0] = FirstHardware.getPaymentCancle(ActivityCancelPayment.this, receiptID, PayYMD, paymentPrice);
                            }

                        } else if (paymentType == 2) {
                            //카드 결제 취소
                            requestCode[0] = FirstHardware.payCashReceiptCancel(ActivityCancelPayment.this, receiptID, PayYMD, paymentPrice);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityCancelPayment.this);
                                if (apiResponseCode[0].equals("S0000")) {
                                    thirdAlertPopup.setTitle("결제취소 팝업");
                                    thirdAlertPopup.setContent("결제가 취소되었어요.");
                                    thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                        @Override
                                        public void onConfirmClick() {
                                            finish();
                                        }
                                    });
                                    thirdAlertPopup.show();
                                } else {
                                    thirdAlertPopup.setTitle("결제취소정보 전송오류");
                                    thirdAlertPopup.setContent("결제취소정보 전송중 오류 발생했어요. \n 고객센터로 문의해주세요.");
                                    thirdAlertPopup.show();
                                }
                            } catch (Exception err) {
                                err.printStackTrace();
                            }
                        }
                    });
                }
            }).start();
        }

        @Override
        public void canclePayCard(int paymentID, String TID, String receiptID, String PayYMD, int paymentPrice) {
            Logger.d(TAG, "TID : " + TID);
            Logger.d(TAG, "receiptID : " + receiptID);

            final String[] requestCardCode = {null};//FirstData 결제 로직
            final String[] requestPayCode = {null};//FirstData 결제 로직
            final String[] apiResponseCode = {null};

            new Thread(new Runnable() {
                public void run() {
                    try {
                        //현금영수증 취소
                        requestPayCode[0] = FirstHardware.payCashReceiptCancel(ActivityCancelPayment.this, receiptID, PayYMD, paymentPrice);
                        //카드결제 취소
                        requestCardCode[0] = FirstHardware.getPaymentCancle(ActivityCancelPayment.this, TID, PayYMD, paymentPrice);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (requestPayCode[0].equals("0") || requestCardCode[0].equals("0")) {
                        apiResponseCode[0] = PaymentManager.getCancelStorePayment(ActivityCancelPayment.this, Long.valueOf(paymentID));
                    }
                    runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityCancelPayment.this);
                                if (apiResponseCode[0].equals("S0000")) {
                                    thirdAlertPopup.setTitle("결제취소 팝업");
                                    thirdAlertPopup.setContent("결제가 취소되었어요.");
                                    thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                        @Override
                                        public void onConfirmClick() {
                                            finish();
                                        }
                                    });
                                    thirdAlertPopup.show();
                                } else {
                                    thirdAlertPopup.setTitle("결제취소정보 전송오류");
                                    thirdAlertPopup.setContent("결제취소정보 전송중 오류 발생했어요. \n 고객센터로 문의해주세요.");
                                    thirdAlertPopup.show();
                                }
                            } catch (Exception err) {
                                err.printStackTrace();
                            }
                        }
                    });
                }
            }).start();
        }
    };
}
