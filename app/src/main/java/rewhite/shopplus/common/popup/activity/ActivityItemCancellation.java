package rewhite.shopplus.common.popup.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerCancelAvailOrderList;
import rewhite.shopplus.client.model.CancelAvailOrder;
import rewhite.shopplus.client.network.PaymentManager;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.data.manager.ManagerItemCancle;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterItemCancle;

/**
 * 품목취소 Dialog Class
 */
public class ActivityItemCancellation extends AppCompatActivity {
    private static final String TAG = "ActivityItemCancellation";

    private ListView lvItemCancle;
    private WaitCounter mWait;
    private static final String USER_STORE_ID = "user_store_id";
    private static int userStoreId;
    private LinearLayout llItemCancle;

    private CheckBox cbAllCheck;
    private List<CancelAvailOrder> cancelAvailOrders = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_cancellation);

        userStoreId = getIntent().getExtras().getInt(USER_STORE_ID);
        mWait = new WaitCounter(this);

        init();
    }

    /**
     * 데이터 init
     */
    private void init() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                ManagerCancelAvailOrderList managerCancelAvailOrderList = VisitReceptionManager.getCancelAvailorder(ActivityItemCancellation.this, userStoreId);
                runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        cancelAvailOrders = managerCancelAvailOrderList.getcancelAvailOrder();
                        setLayout();
                    }
                });
            }
        }).start();
    }

    /**
     * layout setting view
     */
    private void setLayout() {
        lvItemCancle = (ListView) findViewById(R.id.lv_item_cancle);
        llItemCancle = (LinearLayout) findViewById(R.id.ll_item_cancle);

        CheckBox cbAllCheck = (CheckBox) findViewById(R.id.cb_all_check);
        LinearLayout llCancle = (LinearLayout) findViewById(R.id.ll_close);
        Button btnItemCancle = (Button) findViewById(R.id.btn_item_cancle);

        cbAllCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setAdapter(isChecked);
            }
        });

        btnItemCancle.setOnClickListener(btnOnClickListener);
        llCancle.setOnClickListener(btnOnClickListener);

        setItemNoneView();
        setAdapter(false);
    }

    /**
     * 품목취소 데이터가 존재 하지않을 경우 View setting
     */
    private void setItemNoneView() {
        if (cancelAvailOrders.size() == 0) {
            llItemCancle.setVisibility(View.VISIBLE);
            lvItemCancle.setVisibility(View.GONE);
        } else {
            llItemCancle.setVisibility(View.GONE);
            lvItemCancle.setVisibility(View.VISIBLE);
        }

    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.ll_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityItemCancellation.this);
                    thirdConfirmPopup.setTitle("종료팝업");
                    thirdConfirmPopup.setContent("팝업을 종료할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setCancelButtonText("아니오");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            finish();
                        }
                    });
                    thirdConfirmPopup.show();
                    break;

                case R.id.btn_item_cancle:
                    List<CancelAvailOrder> cancelAvailOrders = ManagerItemCancle.getInstance().getitemCancle();

                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityItemCancellation.this);
                    StringBuffer cancleData = new StringBuffer();
                    for (int i = 0; i < cancelAvailOrders.size(); i++) {
                        if (i == 0) {
                            cancleData.append(cancelAvailOrders.get(i).getOrderItemID());
                        } else {
                            cancleData.append("|");
                            cancleData.append(cancelAvailOrders.get(i).getOrderItemID());
                        }
                    }

                    if (cancelAvailOrders.size() == 0) {
                        thirdAlertPopup.setTitle("품목취소 팝업");
                        thirdAlertPopup.setContent("품목을 선택해주세요.");
                        thirdAlertPopup.show();
                    } else {

                        mWait.show();
                        new Thread(new Runnable() {
                            public void run() {
                                String resultCode = PaymentManager.getCancelOrderItem(ActivityItemCancellation.this, cancleData.toString(), cancelAvailOrders.size());

                                Logger.d(TAG, "resultCode : " + resultCode);
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        mWait.dismiss();
                                        if (resultCode.equals("S0000")) {
                                            thirdAlertPopup.setTitle("품목취소 팝업");
                                            thirdAlertPopup.setContent("품목이 취소되었어요.");
                                            thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                                @Override
                                                public void onConfirmClick() {
                                                    finish();
                                                }
                                            });
                                            thirdAlertPopup.show();
                                        } else {

                                        }
                                    }
                                });
                            }
                        }).start();

                    }
                    break;
            }
        }
    };

    public interface itemCancle {
        void setSelectItem(CancelAvailOrder cancelAvailOrder);

        void setUnSelectItem(CancelAvailOrder cancelAvailOrder);
    }

    private itemCancle itemCancle = new itemCancle() {

        @Override
        public void setSelectItem(CancelAvailOrder cancelAvailOrder) {
            List<CancelAvailOrder> managerCancelAvailOrders = new ArrayList<>();
            managerCancelAvailOrders.add(cancelAvailOrder);

            ManagerItemCancle.getInstance().setitemCancle(managerCancelAvailOrders);
        }

        @Override
        public void setUnSelectItem(CancelAvailOrder cancelAvailOrder) {
            List<CancelAvailOrder> cancelAvailOrders = ManagerItemCancle.getInstance().getitemCancle();

            for (CancelAvailOrder cancelAvail : cancelAvailOrders) {
                if (cancelAvail.getStoreItemName().equals(cancelAvailOrders.get(0).getStoreItemName())) {
                    cancelAvailOrders.remove(cancelAvail);
                }
            }
            ManagerItemCancle.getInstance().setitemCancle(cancelAvailOrders);
        }
    };

    /**
     * 품목 취소 데이터 Adapter setting
     */
    private void setAdapter(boolean isCheckedStatus) {
        AdapterItemCancle adapterItemCancle = new AdapterItemCancle(getApplicationContext(), isCheckedStatus, itemCancle);

        adapterItemCancle.addItem(cancelAvailOrders);
        lvItemCancle.setAdapter(adapterItemCancle);
        adapterItemCancle.notifyDataSetChanged();
    }
}
