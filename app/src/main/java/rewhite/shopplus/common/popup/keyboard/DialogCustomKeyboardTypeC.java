package rewhite.shopplus.common.popup.keyboard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.model.GetStoreManageInfo;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.logic.BackPressCloseHandler;
import rewhite.shopplus.logic.BusProvider;
import rewhite.shopplus.util.CustomKeyboard;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.WaitCounter;

/**
 * 마일리지 설정 View
 */
public class DialogCustomKeyboardTypeC extends AppCompatActivity {
    private static final String TAG = "DialogCustomKeyboardTypeC";

    private EditText etCashAccrual, etCardRate, etMinimalUse;
    private InputMethodManager imm;
    private View view = null;
    private String earning = null;
    private CustomKeyboard mCustomKeyboard;

    private WaitCounter mWait;
    private List<GetStoreManageInfo> getStoreManageInfos = new ArrayList<>();

    private RadioGroup rgAccumulate;
    private RadioButton rbAccumulate, rbEarnest;
    private View viewMinUsePayment, viewCard, viewCash;

    private BackPressCloseHandler backPressCloseHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_custom_keyboard_type_c);
        view = findViewById(R.id.rootView);
        mWait = new WaitCounter(this);
        backPressCloseHandler = new BackPressCloseHandler(this);

        getStoreManageInfos = ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList();

        setLayout();
        setRadioSelectSetting();
    }

    /**
     * 레이아웃 셋팅
     */
    private void setLayout() {
        DecimalFormat df = new DecimalFormat("#.##");

        LinearLayout llClose = (LinearLayout) findViewById(R.id.ll_close);
        Button btnAgree = (Button) findViewById(R.id.btn_agree);
        TextView tacNumber = (TextView) findViewById(R.id.tv_tak_number);

        viewMinUsePayment = (View) findViewById(R.id.view_min_use_payment);
        viewCard = (View) findViewById(R.id.view_card_c);
        viewCash = (View) findViewById(R.id.view_cash_c);

        etCashAccrual = (EditText) findViewById(R.id.et_cash_accrual);
        etCardRate = (EditText) findViewById(R.id.et_card_rate);
        etMinimalUse = (EditText) findViewById(R.id.et_minimal_use);

        rgAccumulate = (RadioGroup) findViewById(R.id.rg_accumulate);
        rbAccumulate = (RadioButton) findViewById(R.id.rb_accumulate);
        rbEarnest = (RadioButton) findViewById(R.id.rb_earnest);

        btnAgree.setOnClickListener(btnClickFinish);
        llClose.setOnClickListener(btnClickFinish);

        viewMinUsePayment.setOnClickListener(setOnClickListener);
        viewCard.setOnClickListener(setOnClickListener);
        viewCash.setOnClickListener(setOnClickListener);

        rgAccumulate.setOnCheckedChangeListener(onCheckedListener);
        mCustomKeyboard = new CustomKeyboard(DialogCustomKeyboardTypeC.this, view, R.id.keyboard_view, R.xml.custom_keyboard_type3);
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        etCashAccrual.setHint(String.valueOf(df.format(getStoreManageInfos.get(0).getData().getCashMileageRate())));
        etCardRate.setHint(String.valueOf(df.format(getStoreManageInfos.get(0).getData().getCardMileageRate())));
        etMinimalUse.setHint(DataFormatUtil.moneyFormatToWon(getStoreManageInfos.get(0).getData().getMileageUseMinPrice()));

        mCustomKeyboard.registerEditText(view, R.id.et_cash_accrual);
        etCashAccrual.setBackgroundResource(R.drawable.a_box_round_custom_type_1);
        etCardRate.setBackgroundResource(R.drawable.a_button_type_3);
        etMinimalUse.setBackgroundResource(R.drawable.a_button_type_3);
    }

    /**
     * 라디오 버튼 초기 설정 View
     */
    private void setRadioSelectSetting() {
        if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getIsSaveMileage().equals("Y")) {
            rbAccumulate.setTextColor(getResources().getColor(R.color.Wite_color));
            rbEarnest.setTextColor(getResources().getColor(R.color.color_43425c));

            rbAccumulate.setBackgroundResource(R.drawable.a_button_type_2);
            rbEarnest.setBackgroundResource(R.drawable.a_button_type_3);
        } else {
            rbEarnest.setTextColor(getResources().getColor(R.color.Wite_color));
            rbAccumulate.setTextColor(getResources().getColor(R.color.color_43425c));

            rbEarnest.setBackgroundResource(R.drawable.a_button_type_2);
            rbAccumulate.setBackgroundResource(R.drawable.a_button_type_3);
        }
    }

    /**
     * 적립 여부 ClickListener View Setting
     */
    private RadioGroup.OnCheckedChangeListener onCheckedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbAccumulate.isChecked()) {
                rbAccumulate.setTextColor(getResources().getColor(R.color.Wite_color));
                rbEarnest.setTextColor(getResources().getColor(R.color.color_43425c));

                rbAccumulate.setBackgroundResource(R.drawable.a_button_type_2);
                rbEarnest.setBackgroundResource(R.drawable.a_button_type_3);
            } else if (rbEarnest.isChecked()) {
                rbEarnest.setTextColor(getResources().getColor(R.color.Wite_color));
                rbAccumulate.setTextColor(getResources().getColor(R.color.color_43425c));

                rbEarnest.setBackgroundResource(R.drawable.a_button_type_2);
                rbAccumulate.setBackgroundResource(R.drawable.a_button_type_3);
            }
        }
    };

    /**
     * 카드 / 현금 / 마일리지 최소금액 입력 EditText Setting View
     */
    private View.OnClickListener setOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                //현금 마일리지 적립률
                case R.id.view_cash_c:
                    etCashAccrual.setRawInputType(InputType.TYPE_CLASS_TEXT);
                    etCashAccrual.setSelection(etCashAccrual.length());
                    etCashAccrual.requestFocus();
                    etCashAccrual.setTextIsSelectable(true);

                    mCustomKeyboard.registerEditText(view, R.id.et_cash_accrual);

                    etCashAccrual.setBackgroundResource(R.drawable.a_box_round_custom_type_1);
                    etCardRate.setBackgroundResource(R.drawable.a_button_type_3);
                    etMinimalUse.setBackgroundResource(R.drawable.a_button_type_3);
                    break;
                //카드 마일리지 적립률
                case R.id.view_card_c:
                    etCardRate.setRawInputType(InputType.TYPE_CLASS_TEXT);
                    etCardRate.setSelection(etCardRate.length());
                    etCardRate.requestFocus();
                    etCardRate.setTextIsSelectable(true);

                    mCustomKeyboard.registerEditText(view, R.id.et_card_rate);

                    etCardRate.setBackgroundResource(R.drawable.a_box_round_custom_type_1);
                    etCashAccrual.setBackgroundResource(R.drawable.a_button_type_3);
                    etMinimalUse.setBackgroundResource(R.drawable.a_button_type_3);
                    break;

                //마일리지 최소 금액 단위
                case R.id.view_min_use_payment:
                    etMinimalUse.setRawInputType(InputType.TYPE_CLASS_TEXT);
                    etMinimalUse.setSelection(etMinimalUse.length());
                    etMinimalUse.requestFocus();
                    etMinimalUse.setTextIsSelectable(true);

                    mCustomKeyboard.registerEditText(view, R.id.et_minimal_use);

                    etMinimalUse.setBackgroundResource(R.drawable.a_box_round_custom_type_1);
                    etCardRate.setBackgroundResource(R.drawable.a_button_type_3);
                    etCashAccrual.setBackgroundResource(R.drawable.a_button_type_3);
                    break;
            }
        }
    };

    /**
     * 적립 확인 버튼
     * <p>
     * - 예외처리 로직 추가.
     * - Custom Dialog 로직 추가.
     */
    private View.OnClickListener btnClickFinish = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            EnviromentManager.getManagerStoreManageInfo(DialogCustomKeyboardTypeC.this);
            BusProvider.getInstance().post(1); // Click 이벤트 던지기 위한 로직

            if (id == R.id.ll_close) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(DialogCustomKeyboardTypeC.this);
                thirdConfirmPopup.setTitle("팝업종료");
                thirdConfirmPopup.setContent("변경하신 내용은 삭제됩니다. \n 팝업을 종료할까요?");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        finish();
                    }
                });
                thirdConfirmPopup.show();

            } else if (id == R.id.btn_agree) {
                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(DialogCustomKeyboardTypeC.this);
                thirdAlertPopup.setTitle("알림");

                if (rbAccumulate.isChecked()) {
                    earning = "Y";
                } else {
                    earning = "N";
                }
                if (etCashAccrual.getText().toString().equals("") || TextUtils.isEmpty(etCashAccrual.getText().toString()) || etCashAccrual.getText().toString() == null) {
                    thirdAlertPopup.setContent("현금적립율를 \n" + "입력해주세요. ");
                    thirdAlertPopup.show();
                } else if (etCardRate.getText().toString().equals("") || TextUtils.isEmpty(etCardRate.getText().toString()) || etCardRate.getText().toString() == null) {
                    thirdAlertPopup.setContent("카드적립율를 \n" + "입력해주세요. ");
                    thirdAlertPopup.show();
                } else if (etMinimalUse.getText().toString().equals("") || TextUtils.isEmpty(etMinimalUse.getText().toString()) || Integer.valueOf(etMinimalUse.getText().toString()) < 1000) {
                    thirdAlertPopup.setContent("최소사용금액은 1000원 단위로\n" + "설정해주세요. ");
                    thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            etMinimalUse.setText("0");  //입력 데이터 값 초기화
                        }
                    });
                    thirdAlertPopup.show();
                } else {
                    String resultCode;
                    resultCode = EnviromentManager.getUpdateMileageManage(DialogCustomKeyboardTypeC.this, earning, Double.valueOf(etCashAccrual.getText().toString()), Double.valueOf(etCardRate.getText().toString()), Integer.valueOf(etMinimalUse.getText().toString()));

                    if (resultCode.equals("S0000")) {
                        thirdAlertPopup.setContent("저장이 완료 되었습니다.");
                        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                            @Override
                            public void onConfirmClick() {
                                finish();
                            }
                        });
                        thirdAlertPopup.show();
                    } else {
                        thirdAlertPopup.setContent("일시적인 오류가 뱔생했어요. \n 잠시후 다시 시도해 주세요.");
                        thirdAlertPopup.show();
                    }
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }
}