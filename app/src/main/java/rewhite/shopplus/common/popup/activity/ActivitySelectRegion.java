package rewhite.shopplus.common.popup.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerAreaCity;
import rewhite.shopplus.client.manager.ManagerAreaDistrict;
import rewhite.shopplus.client.model.AreaCityListModel;
import rewhite.shopplus.client.model.AreaDistrictListModel;
import rewhite.shopplus.client.network.AuthManager;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.util.SharedPreferencesUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterSingUpLocal;
import rewhite.shopplus.view.adapter.AdapterSingUpLocalArea;

public class ActivitySelectRegion extends AppCompatActivity {
    private static final String TAG = "ActivitySelectRegion";
    private static final String AREA_CITY_KEY = "area_city_key";
    private ManagerAreaCity managerAreaCity;
    private ManagerAreaDistrict managerAreaDistrict;
    private WaitCounter mWait;

    private int areaCityIndex, mParnetAreaID;
    private TextView tvTitle;
    private ListView lvlocal;

    private LinearLayout llClose;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_activity_select_region);
        mWait = new WaitCounter(this);

        setLayout();
        init();
        areaCityIndex = getIntent().getExtras().getInt(AREA_CITY_KEY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (areaCityIndex == 2) {
            tvTitle.setText(SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_AREA, PrefConstant.NAME_AREA_DOSI) + "선택");
        } else {
            tvTitle.setText("시/도 선택");
        }
    }

    private void setLayout() {
        tvTitle = (TextView) findViewById(R.id.tv_title);
        lvlocal = (ListView) findViewById(R.id.lv_local);
        llClose = (LinearLayout) findViewById(R.id.ll_close);
        llClose.setOnClickListener(btnOnClickListener);
    }

    private void init() {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    if (areaCityIndex == 1) {
                        managerAreaCity = AuthManager.getAreaCityList(ActivitySelectRegion.this);
                    } else {
                        managerAreaDistrict = AuthManager.getAreaDistrictList(ActivitySelectRegion.this, SharedPreferencesUtil.getIntSharedPreference(getApplicationContext(), PrefConstant.KEY_AREA, PrefConstant.NAME_AREA_ID));
                    }
                    runOnUiThread(new Runnable() {
                        public void run() {

                            if (areaCityIndex == 1) {
                                List<AreaCityListModel> areaCityListModels = managerAreaCity.getManagerAreaCity();

                                AdapterSingUpLocal adapterSingUpLocal = new AdapterSingUpLocal(getApplicationContext(), parnetArea);
                                adapterSingUpLocal.addItem(areaCityListModels);
                                lvlocal.setAdapter(adapterSingUpLocal);
                            } else {
                                List<AreaDistrictListModel> areaCityListModels = managerAreaDistrict.getAreaDistrictListModel();

                                AdapterSingUpLocalArea adapterSingUpLocalArea = new AdapterSingUpLocalArea(getApplicationContext(), parnetArea);

                                adapterSingUpLocalArea.addItem(areaCityListModels);
                                lvlocal.setAdapter(adapterSingUpLocalArea);
                            }
                            mWait.dismiss();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    public interface parnetArea {
        void parentAreaId(int parnetAreaID);
    }

    private parnetArea parnetArea = new parnetArea() {
        @Override
        public void parentAreaId(int parnetAreaID) {
            finish();
        }
    };


    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.ll_close:
                    finish();
                    break;
            }
        }
    };
}
