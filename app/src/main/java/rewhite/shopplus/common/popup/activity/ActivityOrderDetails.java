package rewhite.shopplus.common.popup.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityEditItem;
import rewhite.shopplus.client.manager.ManagerOrderItemList;
import rewhite.shopplus.client.model.GetOrderItemList;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.data.dto.ItemOrderDetails;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.util.hardware.FirstHardware;
import rewhite.shopplus.view.adapter.AdapterComponent;
import rewhite.shopplus.view.adapter.AdapterItemMemo;
import rewhite.shopplus.view.adapter.AdapterOderDetalis;
import rewhite.shopplus.view.adapter.AdapterOrderDetailComponent;
import rewhite.shopplus.view.adapter.AdapterOrderDetailRepair;
import rewhite.shopplus.view.adapter.AdapterOrderDetailTechnical;

import static rewhite.shopplus.util.CommonUtil.setListViewHeightBasedOnChildren;

/**
 * 주문 상세보기
 */
public class ActivityOrderDetails extends AppCompatActivity {
    private static final String TAG = "ActivityOrderDetails";
    private static final String TAC_NUMBER = "tac_number";
    private static final String ITEM_ORDER_ID = "item_order_id";  //주문ID
    private static final String ORDER_ID = "order_id";  //주문ID

    private List<GetOrderItemList> getOrderItemLists;
    private RecyclerView recycleViewMemo;

    private WaitCounter mWait;

    private int mPosition = 0;
    private int itemOrderID;
    private TextView tvUserName, tvPhoneNumber, tvAddress, tvLaundryCompleteDate;
    private TextView tvTotalItemAmountWon, tvWashTypeDesc;

    private ImageView ivItemTacColor, ivColor, ivClose;
    private TextView tvItemTacNumber, tvLaundryStatus, tvCategoryName, tvItemAmount;
    private Button btnItemEdit, btnItemReception;
    private ListView lvRepairFee, lvRegistation, lvViewComponent, lvTechnicalFee;

    private RelativeLayout rlRepairFree, rlTechnicalFee, rlMemo, rlViewComponent;
    private RecyclerView rvhowToWash, rvTechnicalFee;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        mWait = new WaitCounter(this);

        String name = FirstHardware.initStatus(ActivityOrderDetails.this);

        Logger.d(TAG, "name : " + name);
        itemOrderID = getIntent().getExtras().getInt(ITEM_ORDER_ID);

        init();
    }

    /**
     * 데이터 바인딩
     */
    private void init() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                ManagerOrderItemList managerOrderItemList = LaundryRegistrationManager.getManagerOrderItemList(ActivityOrderDetails.this, itemOrderID);
                runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        AdapterOderDetalis adapterOderDetalis = new AdapterOderDetalis(getApplicationContext(), orderItemDetail);
                        lvRegistation = (ListView) findViewById(R.id.lv_registation);
                        getOrderItemLists = managerOrderItemList.getManagerAreaCity();
                        adapterOderDetalis.ItemAdd(getOrderItemLists);
                        lvRegistation.setAdapter(adapterOderDetalis);
                        setLayout();
                    }
                });
            }
        }).start();
    }

    /**
     * LayoutSetting View
     */
    private void setLayout() {
        tvUserName = (TextView) findViewById(R.id.tv_user_name);
        tvPhoneNumber = (TextView) findViewById(R.id.tv_phone_number);
        tvAddress = (TextView) findViewById(R.id.tv_address);
        tvLaundryCompleteDate = (TextView) findViewById(R.id.tv_laundry_complete_date);
        tvTotalItemAmountWon = (TextView) findViewById(R.id.tv_total_item_amount_won);
        tvWashTypeDesc = (TextView) findViewById(R.id.tv_wash_type_desc);

        tvItemTacNumber = (TextView) findViewById(R.id.tv_item_tac_number);
        ivItemTacColor = (ImageView) findViewById(R.id.iv_item_tac_color);
        tvLaundryStatus = (TextView) findViewById(R.id.tv_laundry_status);
        tvCategoryName = (TextView) findViewById(R.id.tv_category_name);
        tvItemAmount = (TextView) findViewById(R.id.tv_item_amount);
        lvViewComponent = (ListView) findViewById(R.id.lv_view_component);
        lvRepairFee = (ListView) findViewById(R.id.lv_repair_fee);
        lvTechnicalFee = (ListView) findViewById(R.id.lv_technical_fee);
        ivColor = (ImageView) findViewById(R.id.iv_color);

        rlRepairFree = (RelativeLayout) findViewById(R.id.rl_repair_free);
        rlTechnicalFee = (RelativeLayout) findViewById(R.id.rl_technical_fee);
        rlMemo = (RelativeLayout) findViewById(R.id.rl_memo);
        rlViewComponent = (RelativeLayout) findViewById(R.id.rl_view_component);
        recycleViewMemo = (RecyclerView) findViewById(R.id.recycle_view_memo);

        ivClose = (ImageView) findViewById(R.id.iv_close);
        btnItemEdit = (Button) findViewById(R.id.btn_item_edit);
        btnItemReception = (Button) findViewById(R.id.btn_item_reception);
        setTextView();
        setItemTextView();
        setOnClickListener();
    }

    /**
     * TextView Setting
     */
    private void setTextView() {
        int totalAmount = 0;

        tvUserName.setText(getOrderItemLists.get(mPosition).getUserName());
        tvPhoneNumber.setText(getOrderItemLists.get(mPosition).get_UserPhone());
        tvAddress.setText(getOrderItemLists.get(mPosition).get_UserAddress());

        if (getOrderItemLists.get(mPosition).getFreeTypeDesc() != null || !TextUtils.isEmpty(getOrderItemLists.get(mPosition).getFreeTypeDesc())) {
            tvLaundryStatus.setVisibility(View.VISIBLE);
            tvLaundryStatus.setText(getOrderItemLists.get(mPosition).getFreeTypeDesc());
        } else {
            tvLaundryStatus.setVisibility(View.GONE);
        }

        for (GetOrderItemList getVisitOrderList : getOrderItemLists) {
            totalAmount += getVisitOrderList.getOrderItemPrice();
        }

        tvWashTypeDesc.setText(getOrderItemLists.get(mPosition).getWashTypeDesc());
        tvTotalItemAmountWon.setText(DataFormatUtil.moneyFormatToWon(totalAmount) + "원");
        try {
            tvLaundryCompleteDate.setText(DataFormatUtil.setDateMothFormat(getOrderItemLists.get(mPosition).getWashFinWillDate()) +
                    "(" + DataFormatUtil.getDayWeek(DataFormatUtil.setDatePaymentFormat4(getOrderItemLists.get(mPosition).getWashFinWillDate())) + ")");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * detailTextView Setting
     */
    private void setItemTextView() {
        try {
            int idx = getOrderItemLists.get(mPosition).getTagNo().indexOf("|");
            if (idx == 0) {
                tvItemTacNumber.setText(CommonUtil.setTagFormater(getOrderItemLists.get(mPosition).getTagNo()));
            } else if (getOrderItemLists.get(mPosition).getTagNo().length() >= 6 && getOrderItemLists.get(mPosition).getTagNo().length() < 9) {
                tvItemTacNumber.setText(CommonUtil.setFormater(getOrderItemLists.get(mPosition).getTagNo()));
            } else if (getOrderItemLists.get(mPosition).getTagNo().contains("-")) {
                tvItemTacNumber.setText(CommonUtil.setTagFormaterChange(getOrderItemLists.get(mPosition).getTagNo()));
            }
        } catch (Exception err) {
            err.printStackTrace();
        }

        CommonUtil.getTacColor(getApplication(), Integer.valueOf(getOrderItemLists.get(mPosition).getTagColor()), ivItemTacColor);
        tvCategoryName.setText(getOrderItemLists.get(mPosition).getStoreItemName() + " > " + getOrderItemLists.get(mPosition).getItemGradeDesc());
        CommonUtil.setColorView(Integer.valueOf(getOrderItemLists.get(mPosition).getTagColor()), ivColor);

        getLvRepairFee();
    }

    private void setOnClickListener() {
        ivClose.setOnClickListener(btnItemOnClickListener);
        btnItemEdit.setOnClickListener(btnItemOnClickListener);
        btnItemReception.setOnClickListener(btnItemOnClickListener);
    }

    private View.OnClickListener btnItemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.iv_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityOrderDetails.this);
                    thirdConfirmPopup.setTitle("종료팝업");
                    thirdConfirmPopup.setContent("팝업을 종료할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setCancelButtonText("아니오");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            finish();
                        }
                    });
                    thirdConfirmPopup.show();
                    break;

                case R.id.btn_item_edit:
                    Intent intent = new Intent(getApplicationContext(), ActivityEditItem.class);
                    intent.putExtra(ORDER_ID, getOrderItemLists.get(mPosition).getOrderID());
                    startActivity(intent);
                    finish();
                    break;

                case R.id.btn_item_reception:
                    String resultCode = FirstHardware.getReissueOfReceiptType(getOrderItemLists);
                    try {
                        if (resultCode.equals("0")) {
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityOrderDetails.this);
                            thirdAlertPopup.setTitle("완료팝업");
                            thirdAlertPopup.setContent("접수증 출력이 완료되었어요.");
                            thirdAlertPopup.setButtonText("확인");
                            thirdAlertPopup.show();

                        } else if (resultCode.equals("-100")) {
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityOrderDetails.this);
                            thirdAlertPopup.setTitle("오류팝업");
                            thirdAlertPopup.setContent("단말기 연결을 확인해주세요.");
                            thirdAlertPopup.setButtonText("확인");
                            thirdAlertPopup.show();
                        }
                    } catch (Exception err) {
                        err.printStackTrace();
                    }
                    break;
            }
        }
    };

    /**
     * 수선요금 AdapterView Setting
     */
    private void getLvRepairFee() {
        ArrayList<ItemOrderDetails> itemOrderDetailsComponent = new ArrayList<>();
        ArrayList<ItemOrderDetails> itemOrderDetailsRepairFree = new ArrayList<>();
        ArrayList<ItemOrderDetails> itemOrderDetailsTechnicalFee = new ArrayList<>();

        for (GetOrderItemList.OrderItemOption orderItemOption : getOrderItemLists.get(mPosition).getOptions()) {
            //기술요금
            if (orderItemOption.getOptionType() == 1) {
                rlTechnicalFee.setVisibility(View.VISIBLE);

                ItemOrderDetails itemOrderDetail = new ItemOrderDetails();
                itemOrderDetail.setItemAmount(orderItemOption.getOptionPrice());
                itemOrderDetail.setItemName(orderItemOption.getOptionItemName());
                itemOrderDetailsTechnicalFee.add(itemOrderDetail);

                AdapterOrderDetailTechnical adapterOrderDetailTechnical = new AdapterOrderDetailTechnical(getApplicationContext());
                adapterOrderDetailTechnical.addItem(itemOrderDetailsTechnicalFee);
                lvTechnicalFee.setAdapter(adapterOrderDetailTechnical);
                setListViewHeightBasedOnChildren(lvTechnicalFee); //동적 높이 조절
            }

            //부속품
            if (orderItemOption.getOptionType() == 3) {
                rlViewComponent.setVisibility(View.VISIBLE);

                ItemOrderDetails itemOrderDetail = new ItemOrderDetails();
                itemOrderDetail.setItemAmount(orderItemOption.getOptionPrice());
                itemOrderDetail.setItemName(orderItemOption.getOptionItemName());
                itemOrderDetailsComponent.add(itemOrderDetail);

                AdapterOrderDetailComponent adapterOrderDetailComponent = new AdapterOrderDetailComponent(getApplicationContext());
                adapterOrderDetailComponent.addItem(itemOrderDetailsComponent);
                lvViewComponent.setAdapter(adapterOrderDetailComponent);
                setListViewHeightBasedOnChildren(lvViewComponent); //동적 높이 조절

            }

            //수선
            if (orderItemOption.getOptionType() == 2) {
                rlRepairFree.setVisibility(View.VISIBLE);

                ItemOrderDetails itemOrderDetail = new ItemOrderDetails();
                itemOrderDetail.setItemAmount(orderItemOption.getOptionPrice());
                itemOrderDetail.setItemName(orderItemOption.getOptionItemName());
                itemOrderDetailsRepairFree.add(itemOrderDetail);

                AdapterOrderDetailRepair adapterOrderDetailRepair = new AdapterOrderDetailRepair(getApplicationContext());
                adapterOrderDetailRepair.addItem(itemOrderDetailsRepairFree);
                lvRepairFee.setAdapter(adapterOrderDetailRepair);
                setListViewHeightBasedOnChildren(lvRepairFee); //동적 높이 조절

            }
        }

        if (!TextUtils.isEmpty(getOrderItemLists.get(mPosition).getOrderItemMemo()) || getOrderItemLists.get(mPosition).getOrderItemMemo() != null) {
            rlMemo.setVisibility(View.VISIBLE);

            ArrayList<String> memoDataList = new ArrayList<>();
            String[] array = getOrderItemLists.get(mPosition).getOrderItemMemo().split("\\|");
            for (String meno : array) {
                memoDataList.add(meno);
            }

            RecyclerView.LayoutManager layoutManager;

            layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            recycleViewMemo.setLayoutManager(layoutManager);

            AdapterItemMemo adapterMemo = new AdapterItemMemo(getApplicationContext(), mOnAdapterListners);
            adapterMemo.addItem(memoDataList);
            recycleViewMemo.setAdapter(adapterMemo);
        } else {
            rlMemo.setVisibility(View.GONE);
        }
    }


    public interface orderItemDetail {
        void getItemDetail(int position);
    }

    public orderItemDetail orderItemDetail = new orderItemDetail() {
        @Override
        public void getItemDetail(int poistion) {
            mPosition = poistion;
            setLayout();
        }
    };

    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListners = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
}


