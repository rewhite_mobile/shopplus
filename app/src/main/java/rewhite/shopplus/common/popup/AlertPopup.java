package rewhite.shopplus.common.popup;

import android.content.Context;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.common.ValueWorker;
import rewhite.shopplus.data.manager.DelayedWorkerManager;
import rewhite.shopplus.util.Logger;

/**
 * 확인 버튼만 존재하는 알림 팝업
 */
public class AlertPopup extends Popup {
    protected OnConfirmClickListener mOnConfirmClickListener;

    protected TextView mTitleText;
    protected TextView mContentText;
    protected Button mConfirmButton;

    protected DelayedWorkerManager mWorkerManager;

    public AlertPopup(Context context) {
        super(context);

        mWorkerManager = new DelayedWorkerManager();
    }

    @Override
    protected View getContentView(LayoutInflater inflater, ViewGroup container) {
        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.dialog_alert, container, false);

        setButtons(v);
        mTitleText = (TextView) v.findViewById(R.id.popup_alert_title_text);
        mContentText = (TextView) v.findViewById(R.id.popup_alert_content_text);

        mWorkerManager.run();

        return v;
    }

    public AlertPopup setOnConfirmClickListener(OnConfirmClickListener listener) {
        mOnConfirmClickListener = listener;
        return this;
    }

    public AlertPopup setTitle(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                mTitleText.setText(getValue());
            }
        });

        return this;
    }

    public AlertPopup setTitle(int resId) {
        mWorkerManager.work(new ValueWorker<Integer>(resId) {
            @Override
            public void run() {
                mTitleText.setText(getValue());
            }
        });

        return this;
    }

    public AlertPopup setContent(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                mContentText.setText(getValue());
            }
        });

        return this;
    }

    public AlertPopup setContent(Spanned text) {
        mWorkerManager.work(new ValueWorker<Spanned>(text) {
            @Override
            public void run() {
                mContentText.setText(getValue(), TextView.BufferType.SPANNABLE);
            }
        });

        return this;
    }

    public AlertPopup setContent(int resId) {
        mWorkerManager.work(new ValueWorker<Integer>(resId) {
            @Override
            public void run() {
                mContentText.setText(getValue());
            }
        });

        return this;
    }

    public AlertPopup setButtonText(int resId) {
        mWorkerManager.work(new ValueWorker<Integer>(resId) {
            @Override
            public void run() {
                mConfirmButton.setText(getValue());
            }
        });
        return this;
    }

    public AlertPopup setButtonText(String text) {
        mWorkerManager.work(new ValueWorker<String>(text) {
            @Override
            public void run() {
                mConfirmButton.setText(getValue());
            }
        });
        return this;
    }

    protected void setButtons(ViewGroup container) {
        mConfirmButton = (Button) container.findViewById(R.id.popup_alert_confirm_button);
        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide();

                if (mOnConfirmClickListener != null) {
                    sendJsResultConfirm();
                    mOnConfirmClickListener.onConfirmClick();
                }
            }
        });
    }

    @Override
    public void sendJsResult() {
        Logger.v("AlertPopup", "## Alet sendJsResult[sendJsResultConfirm]");
        sendJsResultConfirm();
    }

    public interface OnConfirmClickListener {
        void onConfirmClick();
    }

}