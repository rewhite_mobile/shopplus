package rewhite.shopplus.common.popup.keyboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.CustomKeyboard;
import rewhite.shopplus.util.SharedPreferencesUtil;
import rewhite.shopplus.view.adapter.AdapterTacColor;

public class DialogCustomKeyboardTypeA extends AppCompatActivity {
    private static final String TAG = "DialogCustomKeyboardTypeA";
    private static final String TAC_NUMVER = "tac_numver";
    private static final int MAX_LENGTH_TAG_4 = 4;
    private static final int MAX_LENGTH_TAG_5 = 5;
    private static final int MAX_LENGTH_TAG_6 = 6;
    private static final int MAX_LENGTH_TAG_8 = 8;
    private static final int MAX_LENGTH_TAG_10 = 10;

    private CustomKeyboard mCustomKeyboard;
    private EditText etChange;
    private String tacNumberData;

    private int testType = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_tac_number);
        View view = findViewById(R.id.rootView);

        Intent intent = getIntent();
        tacNumberData = intent.getExtras().getString(TAC_NUMVER);

        setContentData(view);
    }

    private void setContentData(View view) {
        ArrayList<String> colorName = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager;

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_color);
        LinearLayout llClose = (LinearLayout) findViewById(R.id.ll_close);
        Button btnAgree = (Button) findViewById(R.id.btn_agree);
        TextView tacNumber = (TextView) findViewById(R.id.tv_tak_number);

        etChange = (EditText) findViewById(R.id.et_settac_number);
        etChange.setSelection(etChange.length());

        tacNumber.setText(tacNumberData);

        layoutManager = new StaggeredGridLayoutManager(6, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        AdapterTacColor adapterTaColor = new AdapterTacColor(getApplicationContext(), mOnAdapterListner);
        //TODO 임의 데이터 SETTING
        for (int i = 0; i <= 19; i++) {
            colorName.add("TEST DATA");
        }

        adapterTaColor.addItem(colorName);
        recyclerView.setAdapter(adapterTaColor);

        btnAgree.setOnClickListener(btnClickFinish);
        llClose.setOnClickListener(btnClickFinish);
        etChange.addTextChangedListener(textWatcher);

        mCustomKeyboard = new CustomKeyboard(this, view, R.id.keyboard_view, R.xml.custom_keyboard);
        mCustomKeyboard.registerEditText(view, R.id.et_settac_number);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String tagNumberEnd = null;
            testType = ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType();
            if (testType == 1) {
                if (start == 3) {
                    etChange.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_5)});
                    String name = CommonUtil.getTagFormmater(s.toString().substring(0, 1).toString());

                    etChange.setText(name + s.toString().substring(1, s.toString().length()));
                    etChange.setSelection(etChange.getText().toString().length());

                } else if (start == 4) {
                    etChange.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_5)});
                    tagNumberEnd = s.toString().substring(0, 2);

                    if (!tagNumberEnd.equals("①0")) {
                        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(DialogCustomKeyboardTypeA.this);
                        thirdAlertPopup.setTitle("알림");
                        thirdAlertPopup.setContent("정상적인 택번호가 아닙니다.");

                        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                            @Override
                            public void onConfirmClick() {
                                etChange.setText("");
                                return;
                            }
                        });
                        thirdAlertPopup.show();
                    } else {
                        String name = CommonUtil.getTagFormmater((tagNumberEnd.toString()));

                        etChange.setText(s.toString().replace(tagNumberEnd, name));
                        etChange.setSelection(etChange.getText().toString().length());
                    }
                }
            } else if (testType == 2) {
                etChange.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_8)});
                if (start == 4) {
                    String name = CommonUtil.getTagFormmater(s.toString().substring(1, 2).toString());
                    StringBuffer stringBuffer = new StringBuffer(s.toString());

                    stringBuffer.delete(1, 2);
                    stringBuffer.insert(1, name);

                    String testName = stringBuffer.toString();

                    etChange.setText(testName);
                    etChange.setSelection(etChange.getText().toString().length());

                } else if (start == 5) {
                    String deleteNmae = CommonUtil.getSubTagFormmater(s.toString().substring(1, 2).toString());

                    String name = CommonUtil.getTagFormmater(s.toString().substring(2, 3).toString());
                    StringBuffer stringBuffer = new StringBuffer(s.toString());

                    stringBuffer.delete(1, 2);
                    stringBuffer.insert(1, deleteNmae);
                    stringBuffer.delete(2, 3);
                    stringBuffer.insert(2, name);

                    String testName = stringBuffer.toString();
                    etChange.setText(testName + s.toString());
                    etChange.setSelection(etChange.getText().toString().length());

                } else if (start == 6) {
                    String deleteNmae = CommonUtil.getSubTagFormmater(s.toString().substring(2, 3).toString());

                    String name = CommonUtil.getTagFormmater(s.toString().substring(3, 4).toString());
                    StringBuffer stringBuffer = new StringBuffer(s.toString());

                    stringBuffer.delete(2, 3);
                    stringBuffer.insert(2, deleteNmae);
                    stringBuffer.delete(3, 4);
                    stringBuffer.insert(3, name);

                    String testName = stringBuffer.toString();
                    etChange.setText(testName + s.toString());
                    etChange.setSelection(etChange.getText().toString().length());

                }
            } else if (testType == 3) {
                etChange.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_10)});
                String name = null;
                String nameTo = null;
                if (start == 4) {
                    name = s.toString().substring(0, 1);
                    nameTo = s.toString().substring(1, 5);

                    etChange.setText(name + "-" + nameTo);
                    etChange.setSelection(etChange.getText().toString().length());
                } else if (start == 6) {
                    StringBuffer stringBuffer = new StringBuffer(s.toString());

                    stringBuffer.delete(1, 2);
                    stringBuffer.insert(2, "-");

                    etChange.setText(stringBuffer.toString());
                    etChange.setSelection(etChange.getText().toString().length());
                } else if (start == 7) {
                    StringBuffer stringBuffer = new StringBuffer(s.toString());

                    stringBuffer.delete(2, 3);
                    stringBuffer.insert(3, "-");

                    etChange.setText(stringBuffer.toString());
                    etChange.setSelection(etChange.getText().toString().length());
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private View.OnClickListener btnClickFinish = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            if (id == R.id.ll_close) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(DialogCustomKeyboardTypeA.this);
                thirdConfirmPopup.setTitle("팝업종료");
                thirdConfirmPopup.setContent("택번호 변경을 종료할까요?");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        finish();
                    }
                });
                thirdConfirmPopup.show();
            } else if (id == R.id.btn_agree) {
                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(DialogCustomKeyboardTypeA.this);
                thirdAlertPopup.setTitle("알림");
                if (testType == 0) {
                    thirdAlertPopup.setContent("변경할 택번호를 입력해주세요.");
                    thirdAlertPopup.show();
                } else if (testType == 1) {
                    if (etChange.getText().toString().length() < 4 || etChange.getText().toString().length() > 5) {
                        thirdAlertPopup.setContent("택번호를 확인해주세요.");
                    } else {
                        thirdAlertPopup.setContent("택번호 변경되었어요.");
                        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                            @Override
                            public void onConfirmClick() {
                                SharedPreferencesUtil.putSharedPreference(getApplicationContext(), PrefConstant.KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_NUMBER, etChange.getText().toString());
                                finish();
                            }
                        });
                    }
                    thirdAlertPopup.show();
                } else if (testType == 2) {
                    if (etChange.getText().toString().length() < 5 || etChange.getText().toString().length() > 8) {
                        thirdAlertPopup.setContent("택번호를 확인해주세요.");
                    } else {
                        thirdAlertPopup.setContent("택번호 변경되었어요.");
                        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                            @Override
                            public void onConfirmClick() {
                                SharedPreferencesUtil.putSharedPreference(getApplicationContext(), PrefConstant.KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_NUMBER, etChange.getText().toString());
                                finish();
                            }
                        });
                    }
                    thirdAlertPopup.show();
                } else if (testType == 3) {
                    if (etChange.getText().toString().length() < 6 || etChange.getText().toString().length() > 11) {
                        thirdAlertPopup.setContent("택번호를 확인해주세요.");
                    } else {
                        thirdAlertPopup.setContent("택번호 변경되었어요.");
                        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                            @Override
                            public void onConfirmClick() {
                                SharedPreferencesUtil.putSharedPreference(getApplicationContext(), PrefConstant.KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_NUMBER, etChange.getText().toString());
                                finish();
                            }
                        });
                    }
                    thirdAlertPopup.show();
                }
            }
        }
    };

    private AdapterTacColor.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterTacColor.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {
        }
    };
}