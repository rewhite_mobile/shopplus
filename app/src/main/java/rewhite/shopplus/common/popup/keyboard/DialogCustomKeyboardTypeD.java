package rewhite.shopplus.common.popup.keyboard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.model.GetStoreManageInfo;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.logic.BackPressCloseHandler;
import rewhite.shopplus.logic.BusProvider;
import rewhite.shopplus.util.CustomKeyboard;
import rewhite.shopplus.util.WaitCounter;

/**
 * 선충전금 설정 View
 */
public class DialogCustomKeyboardTypeD extends AppCompatActivity {
    private static final String TAG = "ActivityCustomTacKeyboard";

    private CustomKeyboard mCustomKeyboard;
    private EditText etCashAccrualRate, etCardAccrualRate;
    private InputMethodManager imm;
    private View view = null;
    private WaitCounter mWait;

    private RadioGroup rgAccumulate, rgIssued;
    private RadioButton rbAccumulate, rbUnAccumulate, rbIssued, rbNotIssue;
    private List<GetStoreManageInfo> getStoreManageInfos = new ArrayList<>();

    private String isGiveBonusPrePaid, isPrePaidCashReceipt;

    private BackPressCloseHandler backPressCloseHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_custom_keyboard_type_d);
        view = findViewById(R.id.rootView);
        mWait = new WaitCounter(this);
        backPressCloseHandler = new BackPressCloseHandler(this);

        getStoreManageInfos = ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList();

        setContentData();
        setRadioSelectSetting();
    }

    private void setContentData() {
        DecimalFormat df = new DecimalFormat("#.##");

        LinearLayout llClose = (LinearLayout) findViewById(R.id.ll_close);
        Button btnAgree = (Button) findViewById(R.id.btn_agree);
        TextView tacNumber = (TextView) findViewById(R.id.tv_tak_number);

        etCashAccrualRate = (EditText) findViewById(R.id.et_cash_accrual_rate);
        etCardAccrualRate = (EditText) findViewById(R.id.et_card_accrual_rate);

        rgAccumulate = (RadioGroup) findViewById(R.id.rg_accumulate);
        rgIssued = (RadioGroup) findViewById(R.id.rg_issued);

        rbAccumulate = (RadioButton) findViewById(R.id.rb_accumulate);
        rbUnAccumulate = (RadioButton) findViewById(R.id.rb_un_accumulate);
        rbIssued = (RadioButton) findViewById(R.id.rb_issued);
        rbNotIssue = (RadioButton) findViewById(R.id.rb_not_issue);

        View viewCard = (View) findViewById(R.id.view_card);
        View viewCash = (View) findViewById(R.id.view_cash);

        btnAgree.setOnClickListener(btnClickFinish);
        llClose.setOnClickListener(btnClickFinish);

        mCustomKeyboard = new CustomKeyboard(DialogCustomKeyboardTypeD.this, view, R.id.keyboard_view, R.xml.custom_keyboard_type3);
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        mCustomKeyboard.registerEditText(view, R.id.et_cash_accrual_rate);
        etCashAccrualRate.setHint(String.valueOf(df.format(getStoreManageInfos.get(0).getData().getCardBonusPrePaidRate())));
        etCardAccrualRate.setHint(String.valueOf(df.format(getStoreManageInfos.get(0).getData().getCashBonusPrePaidRate())));

        etCashAccrualRate.setBackgroundResource(R.drawable.a_box_round_custom_type_1);
        etCardAccrualRate.setBackgroundResource(R.drawable.a_button_type_3);

        viewCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etCashAccrualRate.setRawInputType(InputType.TYPE_CLASS_TEXT);
                etCashAccrualRate.setSelection(etCashAccrualRate.length());
                etCashAccrualRate.requestFocus();
                etCashAccrualRate.setTextIsSelectable(true);

                mCustomKeyboard.registerEditText(view, R.id.et_cash_accrual_rate);

                etCashAccrualRate.setBackgroundResource(R.drawable.a_box_round_custom_type_1);
                etCardAccrualRate.setBackgroundResource(R.drawable.a_button_type_3);
            }
        });

        viewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etCardAccrualRate.setRawInputType(InputType.TYPE_CLASS_TEXT);
                etCardAccrualRate.setSelection(etCardAccrualRate.length());
                etCardAccrualRate.requestFocus();
                etCardAccrualRate.setTextIsSelectable(true);

                mCustomKeyboard.registerEditText(view, R.id.et_card_accrual_rate);

                etCashAccrualRate.setBackgroundResource(R.drawable.a_button_type_3);
                etCardAccrualRate.setBackgroundResource(R.drawable.a_box_round_custom_type_1);
            }
        });

        rgAccumulate.setOnCheckedChangeListener(radioGroupClickListener);
        rgIssued.setOnCheckedChangeListener(radioIssuedGroupClickListener);
    }

    /**
     * 라디오 버튼 초기 설정 View
     */
    private void setRadioSelectSetting() {
        if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getIsPrePaidCashReceipt().equals("Y")) {
            isPrePaidCashReceipt = "Y";
            rbIssued.setTextColor(getResources().getColor(R.color.Wite_color));
            rbNotIssue.setTextColor(getResources().getColor(R.color.color_43425c));

            rbIssued.setBackgroundResource(R.drawable.a_button_type_2);
            rbNotIssue.setBackgroundResource(R.drawable.a_button_type_3);
        } else {
            isPrePaidCashReceipt = "N";
            rbNotIssue.setTextColor(getResources().getColor(R.color.Wite_color));
            rbIssued.setTextColor(getResources().getColor(R.color.color_43425c));

            rbNotIssue.setBackgroundResource(R.drawable.a_button_type_2);
            rbIssued.setBackgroundResource(R.drawable.a_button_type_3);
        }

        if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getIsGiveBonusPrePaid().equals("Y")) {
            isGiveBonusPrePaid = "Y";
            rbAccumulate.setTextColor(getResources().getColor(R.color.Wite_color));
            rbUnAccumulate.setTextColor(getResources().getColor(R.color.color_43425c));

            rbAccumulate.setBackgroundResource(R.drawable.a_button_type_2);
            rbUnAccumulate.setBackgroundResource(R.drawable.a_button_type_3);
        } else {
            isGiveBonusPrePaid = "N";
            rbUnAccumulate.setTextColor(getResources().getColor(R.color.Wite_color));
            rbAccumulate.setTextColor(getResources().getColor(R.color.color_43425c));

            rbAccumulate.setBackgroundResource(R.drawable.a_button_type_3);
            rbUnAccumulate.setBackgroundResource(R.drawable.a_button_type_2);
        }
    }

    private RadioGroup.OnCheckedChangeListener radioGroupClickListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbAccumulate.isChecked()) {
                isGiveBonusPrePaid = "Y";
                rbAccumulate.setTextColor(getResources().getColor(R.color.Wite_color));
                rbUnAccumulate.setTextColor(getResources().getColor(R.color.color_43425c));

                rbAccumulate.setBackgroundResource(R.drawable.a_button_type_2);
                rbUnAccumulate.setBackgroundResource(R.drawable.a_button_type_3);
            } else if (rbUnAccumulate.isChecked()) {
                isGiveBonusPrePaid = "N";
                rbUnAccumulate.setTextColor(getResources().getColor(R.color.Wite_color));
                rbAccumulate.setTextColor(getResources().getColor(R.color.color_43425c));

                rbAccumulate.setBackgroundResource(R.drawable.a_button_type_3);
                rbUnAccumulate.setBackgroundResource(R.drawable.a_button_type_2);
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener radioIssuedGroupClickListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbIssued.isChecked()) {
                isPrePaidCashReceipt = "Y";
                rbIssued.setTextColor(getResources().getColor(R.color.Wite_color));
                rbNotIssue.setTextColor(getResources().getColor(R.color.color_43425c));

                rbIssued.setBackgroundResource(R.drawable.a_button_type_2);
                rbNotIssue.setBackgroundResource(R.drawable.a_button_type_3);
            } else if (rbNotIssue.isChecked()) {
                isPrePaidCashReceipt = "N";
                rbNotIssue.setTextColor(getResources().getColor(R.color.Wite_color));
                rbIssued.setTextColor(getResources().getColor(R.color.color_43425c));

                rbNotIssue.setBackgroundResource(R.drawable.a_button_type_2);
                rbIssued.setBackgroundResource(R.drawable.a_button_type_3);
            }
        }
    };

    private View.OnClickListener btnClickFinish = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            BusProvider.getInstance().post(1); // Click 이벤트 던지기 위한 로직
            if (id == R.id.ll_close) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(DialogCustomKeyboardTypeD.this);
                thirdConfirmPopup.setTitle("팝업종료");
                thirdConfirmPopup.setContent("변경하신 내용은 삭제됩니다. \n 팝업을 종료할까요?");
                thirdConfirmPopup.setButtonText("예");
                thirdConfirmPopup.setCancelButtonText("아니요");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        finish();
                    }
                });
                thirdConfirmPopup.show();

            } else if (id == R.id.btn_agree) {
                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(DialogCustomKeyboardTypeD.this);
                thirdAlertPopup.setTitle("알림");

                if (etCashAccrualRate.getText().toString() == null || TextUtils.isEmpty(etCashAccrualRate.getText().toString()) == true || etCashAccrualRate.getText().toString().equals("")) {
                    thirdAlertPopup.setContent("현금적립률을 입력해주세요.");
                } else if (etCardAccrualRate.getText().toString() == null || TextUtils.isEmpty(etCardAccrualRate.getText().toString()) == true || etCardAccrualRate.getText().toString().equals("")) {
                    thirdAlertPopup.setContent("카드적립률을 입력해주세요.");
                } else {
                    String resultCode;
                    resultCode = EnviromentManager.getUpdatePrePaidManage(DialogCustomKeyboardTypeD.this, isGiveBonusPrePaid, Double.valueOf(etCashAccrualRate.getText().toString()), Double.valueOf(etCardAccrualRate.getText().toString()), isPrePaidCashReceipt);

                    if (resultCode.equals("S0000")) {
                        thirdAlertPopup.setTitle("알림");
                        thirdAlertPopup.setContent("저장이 완료되었어요.");
                        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                            @Override
                            public void onConfirmClick() {
                                mWait.dismiss();
                                finish();
                            }
                        });
                    } else {
                        thirdAlertPopup.setTitle("알림");
                        thirdAlertPopup.setContent("일시적인 오류가 뱔생했어요. \n 잠시후 다시 시도해 주세요.");
                    }
                }
                thirdAlertPopup.show();
            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}