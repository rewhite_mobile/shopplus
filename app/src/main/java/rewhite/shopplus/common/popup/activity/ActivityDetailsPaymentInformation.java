package rewhite.shopplus.common.popup.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetPaymentInfoList;
import rewhite.shopplus.client.model.GetPaymentInfoItem;
import rewhite.shopplus.client.model.ManagerGetPaymentInfoModel;
import rewhite.shopplus.client.network.PaymentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.util.hardware.FirstHardware;
import rewhite.shopplus.view.adapter.AdapterDetailPaymentInfo;

/**
 * 결제내역상세 Dialog View
 */
public class ActivityDetailsPaymentInformation extends AppCompatActivity {
    private static final String TAG = "ActivityDetailsPaymentInformation";

    private List<ManagerGetPaymentInfoModel> getPaymentInfoItem;
    private static final String orderItemIdType = "OrderItemID";    //주문ID
    private int indexPosition = 0;

    private int oderItemId;

    private ListView lvItemCancle;
    private TextView tvPaymentDate, tvAmountPayment, tvMileageDate, tvAccumulateMileagePayment, tvPrefilledGold;
    private TextView tvMileagePayment, tvCashPayment, tvTotalAmountPayment;
    private TextView tvCardName, tvCardInfo, tvCardDivide, tvPayYmd, tvPrice, tvCashReceipts;

    private LinearLayout llBillingInformation;
    private Button btnReceipt;

    private WaitCounter mWait;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_payment_infomation);

        oderItemId = getIntent().getExtras().getInt(orderItemIdType);

        mWait = new WaitCounter(this);
        init(oderItemId);
    }

    /**
     * 데이터 init
     */
    private void init(int oderItemId) {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                PaymentManager.getPaymentInfoList(ActivityDetailsPaymentInformation.this, Long.valueOf(oderItemId), 0, 0);   //결제정보 리스트

                runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        setLayout();

                        getPaymentInfoItem = ManagerGetPaymentInfoList.getmInstance().getGetOrderList();

                        ArrayList<GetPaymentInfoItem.OrderItem> orderItems = getPaymentInfoItem.get(0).getOrderItems();

                        AdapterDetailPaymentInfo adapterDetailPaymentInfo = new AdapterDetailPaymentInfo(getApplicationContext(), getPaymentInfo);
                        adapterDetailPaymentInfo.addItem(orderItems);
                        lvItemCancle.setAdapter(adapterDetailPaymentInfo);
                        setTextView(0);
                    }
                });
            }
        }).start();
    }

    /**
     * 레이아웃 셋팅
     */
    private void setLayout() {
        lvItemCancle = (ListView) findViewById(R.id.lv_payment_cancle);
        LinearLayout llCancle = (LinearLayout) findViewById(R.id.ll_close);

        btnReceipt = (Button) findViewById(R.id.btn_receipt);
        tvPaymentDate = (TextView) findViewById(R.id.tv_payment_date);
        tvAmountPayment = (TextView) findViewById(R.id.tv_amount_payment);
        tvMileageDate = (TextView) findViewById(R.id.tv_mileage_date);
        tvAccumulateMileagePayment = (TextView) findViewById(R.id.tv_accumulate_mileage_payment);
        tvPrefilledGold = (TextView) findViewById(R.id.tv_prefilled_gold);

        tvMileagePayment = (TextView) findViewById(R.id.tv_mileage_payment);
        tvCashPayment = (TextView) findViewById(R.id.tv_cash_payment);
        tvTotalAmountPayment = (TextView) findViewById(R.id.tv_total_amount_payment);

        llBillingInformation = (LinearLayout) findViewById(R.id.ll_billing_information);
        tvCardName = (TextView) findViewById(R.id.tv_card_name);
        tvCardInfo = (TextView) findViewById(R.id.tv_card_info);
        tvCardDivide = (TextView) findViewById(R.id.tv_card_divide);
        tvPayYmd = (TextView) findViewById(R.id.tv_pay_ymd);
        tvPrice = (TextView) findViewById(R.id.tv_price);

        tvCashReceipts = (TextView) findViewById(R.id.tv_cash_receipts);
        llCancle.setOnClickListener(btnOnClickListener);
        btnReceipt.setOnClickListener(btnOnClickListener);
    }

    /**
     * @param position
     */
    private void setTextView(int position) {
        try {
            tvPaymentDate.setText("" + DataFormatUtil.setDatePaymentFormat(getPaymentInfoItem.get(0).getPaymentDate()));
            tvMileageDate.setText("" + DataFormatUtil.setDatePaymentFormat3(getPaymentInfoItem.get(0).getUseAvailDate()));

            tvAmountPayment.setText("" + DataFormatUtil.moneyFormatToWon(getPaymentInfoItem.get(0).getPaymentPrice()) + "원");
            tvAccumulateMileagePayment.setText("" + DataFormatUtil.moneyFormatToWon(getPaymentInfoItem.get(0).getSaveMileage()));


            for (GetPaymentInfoItem.PaymentDetail paymentDetail : getPaymentInfoItem.get(0).getPaymentDetails()) {
                if (paymentDetail.getPaymentType() == 3) {
                    tvPrefilledGold.setText("" + DataFormatUtil.moneyFormatToWon(paymentDetail.getPrice()));
                    tvCardName.setVisibility(View.GONE);
                    tvCardInfo.setVisibility(View.GONE);
                    tvCardDivide.setVisibility(View.GONE);
                    tvPayYmd.setVisibility(View.GONE);
                    tvPrice.setVisibility(View.GONE);

                } else if (paymentDetail.getPaymentType() == 4) {
                    tvMileagePayment.setText("" + DataFormatUtil.moneyFormatToWon(paymentDetail.getPrice()));
                    tvCardName.setVisibility(View.GONE);
                    tvCardInfo.setVisibility(View.GONE);
                    tvCardDivide.setVisibility(View.GONE);
                    tvPayYmd.setVisibility(View.GONE);
                    tvPrice.setVisibility(View.GONE);

                } else if (paymentDetail.getPaymentType() == 1) {
                    tvCashPayment.setText("" + DataFormatUtil.moneyFormatToWon(paymentDetail.getPrice()));
                    tvCardName.setVisibility(View.GONE);
                    tvCardInfo.setVisibility(View.GONE);
                    tvCardDivide.setVisibility(View.GONE);
                    tvPayYmd.setVisibility(View.GONE);
                    tvPrice.setVisibility(View.GONE);

                    if (getPaymentInfoItem.get(0).getPaymentDetails().get(position).getIsReceipt().equals("Y")) {
                        tvCashReceipts.setVisibility(View.VISIBLE);
                    } else {
                        tvCashReceipts.setVisibility(View.GONE);
                    }
                } else if (paymentDetail.getPaymentType() == 2) {
                    tvCardName.setText(getPaymentInfoItem.get(0).getPaymentDetails().get(position).getCardName() + "(");
                    tvCardInfo.setText(getPaymentInfoItem.get(0).getPaymentDetails().get(position).getCardNo() + ")");
                    if (getPaymentInfoItem.get(0).getPaymentDetails().get(position).getCardDivide() == 0) {
                        tvCardDivide.setText("일시불");
                    } else {
                        tvCardDivide.setText("할부 (" + getPaymentInfoItem.get(0).getPaymentDetails().get(position).getCardDivide() + " 개월)");
                    }
                    tvPayYmd.setText("" + DataFormatUtil.getDateHyphen(getPaymentInfoItem.get(0).getPaymentDetails().get(position).getPayYMD()));
                    tvPrice.setText("" + DataFormatUtil.moneyFormatToWon(paymentDetail.getPrice()) + " 원");

                    llBillingInformation.setVisibility(View.VISIBLE);
                    tvPayYmd.setVisibility(View.VISIBLE);
                    tvPrice.setVisibility(View.VISIBLE);
                }
            }
            tvTotalAmountPayment.setText("" + DataFormatUtil.moneyFormatToWon(getPaymentInfoItem.get(0).getPaymentPrice()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Dialog 종료
     */
    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.ll_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityDetailsPaymentInformation.this);
                    thirdConfirmPopup.setTitle("종료 팝업");
                    thirdConfirmPopup.setContent("팝업을 종료할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setCancelButtonText("아니오");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            finish();
                        }
                    });

                    thirdConfirmPopup.show();
                    break;

                case R.id.btn_receipt:
                    FirstHardware.getReissueOfReceipt(getPaymentInfoItem);

                    break;
            }
        }
    };

    public interface getPaymentInfo {
        void paymentInfo(int oderItemId);
    }

    private getPaymentInfo getPaymentInfo = new getPaymentInfo() {
        @Override
        public void paymentInfo(int oderItemId) {
            init(oderItemId);
        }
    };
}