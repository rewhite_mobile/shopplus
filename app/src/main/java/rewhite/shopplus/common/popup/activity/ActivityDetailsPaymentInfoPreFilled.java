package rewhite.shopplus.common.popup.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetPaymentInfo;
import rewhite.shopplus.client.model.GetPaymentInfoModel;
import rewhite.shopplus.client.network.PaymentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.util.hardware.FirstHardware;

/**
 * 결제내역상세 (선충전금) Dialog View
 */
public class ActivityDetailsPaymentInfoPreFilled extends AppCompatActivity {
    private static final String TAG = "ActivityDetailsPaymentInfoPreFilled";
    private static final String PAYMENY_ID = "payment_id";    //주문ID

    private int paymentId;

    private WaitCounter mWait;
    private TextView tvPaymentDate, tvPaymentAmount, tvPaymentType, tvPrice, tvPrefilledGold, tvNematodesReserve;
    private LinearLayout llClose;
    private List<GetPaymentInfoModel> paymentInfoItems;
    private Button btnReissueReceipts;
    private TextView tvPaymentName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_paymentinfo_pre_filled);
        Intent intent = getIntent();

        paymentId = intent.getExtras().getInt(PAYMENY_ID);
        mWait = new WaitCounter(this);

        init();
    }

    /**
     * 데이터 init
     */
    private void init() {
        try {
            new Thread(new Runnable() {
                public void run() {
                    PaymentManager.getPaymentInfo(ActivityDetailsPaymentInfoPreFilled.this, Long.valueOf(paymentId));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            paymentInfoItems = ManagerGetPaymentInfo.getmInstance().getPaymentInfo();
                            setLayout();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * layout View
     */
    private void setLayout() {
        llClose = (LinearLayout) findViewById(R.id.ll_close);
        tvPaymentDate = (TextView) findViewById(R.id.tv_payment_date);
        tvPaymentAmount = (TextView) findViewById(R.id.tv_payment_amount);
        tvPaymentType = (TextView) findViewById(R.id.tv_payment_type);
        tvPrice = (TextView) findViewById(R.id.tv_price);
        tvPrefilledGold = (TextView) findViewById(R.id.tv_prefilled_gold);
        tvNematodesReserve = (TextView) findViewById(R.id.tv_nematodes_reserve);
        btnReissueReceipts = (Button) findViewById(R.id.btn_reissue_receipts);
        tvPaymentName = (TextView) findViewById(R.id.tv_payment_name);

        btnReissueReceipts.setOnClickListener(btnOnClickListener);
        llClose.setOnClickListener(btnOnClickListener);

        setTextView();
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.ll_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityDetailsPaymentInfoPreFilled.this);
                    thirdConfirmPopup.setTitle("종료팝업");
                    thirdConfirmPopup.setContent("팝업을 종료할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setCancelButtonText("아니오");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            finish();
                        }
                    });
                    thirdConfirmPopup.show();
                    break;

                case R.id.btn_reissue_receipts:
                    if (paymentInfoItems.get(0).getData().getPaymentDetails().get(0).getPaymentTypeDesc().equals("현금")) {
                        FirstHardware.getPreFilledGoldPrint(String.valueOf((paymentInfoItems.get(0).getData().getPaymentPrice())), String.valueOf(paymentInfoItems.get(0).getData().getSaveMileage())
                                , String.valueOf(paymentInfoItems.get(0).getData().getPaymentPrice() + paymentInfoItems.get(0).getData().getSaveMileage()));
                    } else {
                        FirstHardware.getReissueReceiptsCardPrint(paymentInfoItems);
                    }

                    break;
            }
        }
    };

    /**
     * Text View Setting
     */
    private void setTextView() {
        try {
            tvPaymentDate.setText(DataFormatUtil.setDatePaymentFormat2(paymentInfoItems.get(0).getData().getPaymentDate() + " (" + DataFormatUtil.getDayWeek(DataFormatUtil.setDateWeekFormat(paymentInfoItems.get(0).getData().getPaymentDate())) + ") "
                    + paymentInfoItems.get(0).getData().getPaymentDate()));

            tvPaymentAmount.setText(DataFormatUtil.moneyFormatToWon(paymentInfoItems.get(0).getData().getPaymentPrice()));
            tvPaymentType.setText(paymentInfoItems.get(0).getData().getPaymentDetails().get(0).getPaymentTypeDesc());
            tvPrice.setText(DataFormatUtil.moneyFormatToWon(paymentInfoItems.get(0).getData().getPaymentDetails().get(0).getPrice()));
            tvPrefilledGold.setText(DataFormatUtil.moneyFormatToWon(paymentInfoItems.get(0).getData().getPaymentDetails().get(0).getPrice()));
            tvNematodesReserve.setText(DataFormatUtil.moneyFormatToWon(Math.round((paymentInfoItems.get(0).getData().getSaveMileage()))));

            if(paymentInfoItems.get(0).getData().getPaymentDivide() == 1){
                tvPaymentName.setText("주문결제");
            } else {
                tvPaymentName.setText("선충전금 충전");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
