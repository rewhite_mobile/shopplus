package rewhite.shopplus.common.popup.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.network.StoreUserApiManager;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.CustomViewPager;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterMemberHistory;

/**
 * 회원 내역조회 (마일리지/선충전금) dialog
 */
public class ActivityMemberHistory extends AppCompatActivity {
    private static final String TAG = "ActivityMemberHistory";
    private static final String USER_STORE_ID = "user_store_id";
    private static int userStoreId;

    private LinearLayout llClose;
    private Button btnMileage, btnPreFilledGold;
    private CustomViewPager viewPager;
    private WaitCounter mWait;

    private TextView tvMemberName, tvMileagePayment, tvExpectedMileage, tvPreFilledGold;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_history);

        mWait = new WaitCounter(this);

        userStoreId = getIntent().getExtras().getInt(USER_STORE_ID);
        init();
    }

    private void init() {

        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    //회원 마일리지
                    StoreUserApiManager.getMileageHistory(ActivityMemberHistory.this, Integer.valueOf(SettingLogic.getStoreUserId(ActivityMemberHistory.this)));

                    //회원 선충전 내역
                    StoreUserApiManager.getPrePaidChargeHistor(ActivityMemberHistory.this, Integer.valueOf(SettingLogic.getStoreUserId(ActivityMemberHistory.this)));

                    //회원 선충전금 사용 내역
                    StoreUserApiManager.getPrePaidUseHistory(ActivityMemberHistory.this, Integer.valueOf(SettingLogic.getStoreUserId(ActivityMemberHistory.this)));
                    runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                            setLayout();
                            setViewPagerAdapter();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * LayoutView Setting
     */
    private void setLayout() {
        viewPager = (CustomViewPager) findViewById(R.id.view_pager);

        btnMileage = (Button) findViewById(R.id.btn_mileage);
        btnPreFilledGold = (Button) findViewById(R.id.btn_pre_filled_gold);

        tvMemberName = (TextView) findViewById(R.id.tv_member_name);
        tvMileagePayment = (TextView)findViewById(R.id.tv_mileage_payment);
        tvExpectedMileage = (TextView) findViewById(R.id.tv_expected_mileage);
        tvPreFilledGold = (TextView) findViewById(R.id.tv_pre_filled_gold);

        llClose = (LinearLayout) findViewById(R.id.ll_close);

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityMemberHistory.this);
                thirdConfirmPopup.setTitle("종료 팝업");
                thirdConfirmPopup.setContent("팝업을 종료할까요?");
                thirdConfirmPopup.setButtonText("예");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdConfirmPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        finish();
                    }
                });
                thirdConfirmPopup.setCancelButtonText("아니오");
                thirdConfirmPopup.show();
            }
        });
        setTextView();
    }


    private void setTextView() {
        tvMemberName.setText(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName());
        tvMileagePayment.setText(DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getMileage()) + " 원");
        tvExpectedMileage.setText("(적립예정 " + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getExMileage()) + " 원)");
        tvPreFilledGold.setText(DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getTotalPrePaid()) + " 원");
    }

    /**
     * ViewPager Setting
     */
    private void setViewPagerAdapter() {
        btnMileage.setOnClickListener(movePageListener);
        btnMileage.setTag(0);
        btnPreFilledGold.setOnClickListener(movePageListener);
        btnPreFilledGold.setTag(1);

        viewPager.setAdapter(new AdapterMemberHistory(getSupportFragmentManager(), this, userStoreId));
        viewPager.setCurrentItem(0);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            viewPager.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    /**
     * 카테고리 View 변경
     *
     * @param tag
     */
    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                btnMileage.setTextColor(getResources().getColor(R.color.Wite_color));
                btnMileage.setBackgroundResource(R.drawable.a_button_type_2);

                btnPreFilledGold.setTextColor(getResources().getColor(R.color.color_43425c));
                btnPreFilledGold.setBackgroundResource(R.drawable.a_button_type_3);
                break;

            case 1:
                btnMileage.setTextColor(getResources().getColor(R.color.color_43425c));
                btnMileage.setBackgroundResource(R.drawable.a_button_type_3);

                btnPreFilledGold.setTextColor(getResources().getColor(R.color.Wite_color));
                btnPreFilledGold.setBackgroundResource(R.drawable.a_button_type_2);
                break;
        }
    }
}