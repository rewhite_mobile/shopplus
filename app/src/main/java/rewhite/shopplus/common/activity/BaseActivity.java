package rewhite.shopplus.common.activity;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import rewhite.shopplus.common.listener.ChainStateCheckListener;
import rewhite.shopplus.common.listener.OnBackPressedListener;
import rewhite.shopplus.common.listener.OnBackPressedObserver;
import rewhite.shopplus.common.local.StateCheckChain;
import rewhite.shopplus.common.local.StateChecker;
import rewhite.shopplus.common.popup.AlertPopup;

/**
 * Created by idea on 2016-05-20.
 */
public class BaseActivity extends AppCompatActivity implements OnBackPressedObserver {
    private AppCompatActivity mActivity;
    private List<OnBackPressedListener> mOnBackPressedListener;
    private static Typeface typeface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        typeface = Typeface.createFromAsset(getAssets(), "nanum_square_otf_light.otf");

        setGlobalFont(getWindow().getDecorView());

        checkDeviceStates();
        mActivity = this;
        mOnBackPressedListener = new CopyOnWriteArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mTopEventManager.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        mTopEventManager.hide();
    }

    private StateCheckChain mStateChain;
    private List<StateChecker> mCheckers = new ArrayList<>();

    private void checkDeviceStates() {
        mStateChain = new StateCheckChain(new ChainStateCheckListener() {
            @Override
            public void onStateCheckFinished() {
                for (StateChecker checker : mCheckers) {
                    if (!checker.isSuccess()) {
                        return;
                    }
                }

            }

            @Override
            public void onCheckStart(StateChecker checker) {
            }

            @Override
            public void onChecked(StateChecker checker) {
                mCheckers.add(checker);
            }

            @Override
            public void onCheckCancelled(StateChecker checker) {
            }
        });
    }

    private void setGlobalFont(View view) {
        if (view != null) {
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                int vgCnt = viewGroup.getChildCount();
                for (int i = 0; i < vgCnt; i++) {
                    View v = viewGroup.getChildAt(i);
                    if (v instanceof TextView) {
                        ((TextView) v).setTypeface(typeface);
                    }
                    setGlobalFont(v);
                }
            }
        }
    }

    private void showErrorPopup(final String message) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AlertPopup popup = new AlertPopup(mActivity);
                popup.setContent(message);
                popup.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        mActivity.finish();
                    }
                });
                popup.show();
            }
        }, 100);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("clip_watchdog", true);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mOnBackPressedListener != null) {
            mOnBackPressedListener.clear();
            mOnBackPressedListener = null;
        }

        mActivity = null;
    }

    public boolean isBackPressed() {
        for (OnBackPressedListener listener : mOnBackPressedListener) {
            if (listener.onBackPressed()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        for (OnBackPressedListener listener : mOnBackPressedListener) {
            if (listener.onBackPressed()) {
                return;
            }
        }

        super.onBackPressed();
    }

    protected AppCompatActivity getActivity() {
        return mActivity;
    }

    @Override
    public void addOnBackPressedListener(OnBackPressedListener listener) {
        if (mOnBackPressedListener != null) {
            mOnBackPressedListener.add(listener);
        }
    }

    @Override
    public void removeOnBackPressedListener(OnBackPressedListener listener) {
        if (mOnBackPressedListener != null) {
            mOnBackPressedListener.remove(listener);
        }
    }
}