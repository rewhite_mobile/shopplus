package rewhite.shopplus.common.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import rewhite.shopplus.util.Utility;

/**
 * 상단 상태바 영역을 투명하게 만들어서 앱 영역으로 사용할 수 있는 액티비티
 */
public class TransparentActivity extends BaseActivity {
    private int mStatusBarHeight = 0;
    private boolean mTransparency;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT ) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            mTransparency = true;
        }

        if( mTransparency ) {
            mStatusBarHeight = Utility.getStatusBarHeight(this);
        }
    }

    protected void addStatusBarMargin(View v) {
        ViewGroup.LayoutParams params = v.getLayoutParams();

        if( params instanceof ViewGroup.MarginLayoutParams ) {
            ViewGroup.MarginLayoutParams mp = (ViewGroup.MarginLayoutParams)params;
            mp.topMargin += mStatusBarHeight;

            v.setLayoutParams(mp);
        }
    }

    public int getStatusBarHeight() {
        return mStatusBarHeight;
    }

    public void setStatusBarBackgroundColor(int color) {
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && mTransparency ) {
            getWindow().setStatusBarColor(color);
        }
    }
}
