package rewhite.shopplus.common;

/**
 * Runnable에 임의의 값을 전달할 수 있는 러너블
 */
public abstract class ValueWorker<T> implements Runnable {
    private T mValue;

    public ValueWorker(T value) {
        mValue = value;
    }

    protected T getValue() {
        return mValue;
    }
}
