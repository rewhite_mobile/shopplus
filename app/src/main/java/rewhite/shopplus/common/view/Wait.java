package rewhite.shopplus.common.view;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import rewhite.shopplus.R;

/**
 * 사용자의 입력을 방지하도록 프로시브 애니메이션이 처리된 다이얼로그
 */
public class Wait extends Dialog implements DialogInterface.OnCancelListener {
    private WaitCancelListener mListener;

    public interface WaitCancelListener {
        void onWaitCancel(Wait dlg);
    }

    @SuppressLint("ResourceType")
    public Wait(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.view_wait_progress);

        setProgress(ContextCompat.getDrawable(context, R.anim.animation));

        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setCancelable(false);
    }

    public static Wait display(Context context) {
        return display(context, null);
    }

    public static Wait display(Context context, WaitCancelListener listener) {
        try {
            Wait wait = new Wait(context);
            wait.setCancelListener(listener);
            wait.show();
            return wait;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Wait displayWithWait(Context context, Wait wait, WaitCancelListener listener) {
        return wait != null ? wait : Wait.display(context, listener);
    }

    public static Wait displayWithWait(Context context, Wait wait) {
        return displayWithWait(context, wait, null);
    }

    public static void hide(Wait wait) {
        try {
            wait.dismiss();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void setMessage(int resId) {
        setMessage(getContext().getString(resId));
    }

    public void setMessage(String text) {
        TextView msgView = (TextView)findViewById(R.id.text_loading);
        msgView.setText(text);
        if(!TextUtils.isEmpty(text)) {
            msgView.setVisibility(View.VISIBLE);
        } else {
            msgView.setVisibility(View.GONE);
        }
    }

    public void setMessageVisibility(int visibility) {
        findViewById(R.id.text_loading).setVisibility(visibility);
    }

    public void setProgressDrawable(Drawable d) {
        setProgress(d);
    }

    public void setProgressDrawable(int drawableResId) {
        setProgressDrawable(ContextCompat.getDrawable(getContext(), drawableResId));
    }

    public void setCancelListener(WaitCancelListener listener) {
        this.mListener = listener;
        if(listener != null) {
            setCancelable(true);
            setOnCancelListener(this);
        } else {
            setCancelable(false);
            setOnCancelListener(null);
        }
    }

    public boolean isCancelable() {
        return mListener != null;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        mListener.onWaitCancel((Wait) dialog);
    }

    private void setProgress(Drawable d) {
        ImageView progressbar = (ImageView) findViewById(R.id.img_loading);
        progressbar.setBackground(d);
        if(progressbar.getBackground() instanceof AnimationDrawable) {
            ((AnimationDrawable) progressbar.getBackground()).start();
        }
    }
}
