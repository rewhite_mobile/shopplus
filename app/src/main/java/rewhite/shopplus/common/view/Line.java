package rewhite.shopplus.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

import rewhite.shopplus.R;

/**
 * 두께와 색상을 미리 정의해서 사용성을 높인 View
 */
public class Line extends View {
    public static final int TYPE_01 = 0;
    public static final int TYPE_02 = 1;
    public static final int TYPE_03 = 2;
    public static final int TYPE_04 = 3;
    public static final int TYPE_05 = 4;
    public static final int TYPE_06 = 5;
    public static final int TYPE_07 = 6;
    public static final int TYPE_08 = 7;
    public static final int TYPE_09 = 8;
    public static final int TYPE_10 = 9;
    public static final int TYPE_11 = 10;

    private static final Data[] sData = new Data[] {
            new Data(1, Color.WHITE),
            new Data(1, Color.rgb(0xda, 0xda, 0xda)),
            new Data(2, Color.rgb(0x2e, 0x2e, 0x2e)),
            new Data(1, Color.rgb(0xe8, 0xe8, 0xe8)),
            new Data(1, Color.rgb(0xc9, 0xc9, 0xc9)),
            new Data(1, Color.rgb(0xe6, 0xe6, 0xe6)),
            new Data(1, Color.BLACK),
            new Data(1, Color.rgb(0x8d, 0x8d, 0x8d)),
            new Data(1, Color.rgb(0xd6, 0x3b, 0x3b)),
            new Data(1, Color.rgb(0xf3, 0xf3, 0xf2)),
            new Data(6, Color.rgb(0xe6, 0xe6, 0xe6))
    };

    private Data mCurrentData;

    public Line(Context context) {
        this(context, null);
    }

    public Line(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Line(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mCurrentData = sData[0];

        if( attrs != null ) {
            TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.Line);
            int type = arr.getInt(R.styleable.Line_type, 0);

            arr.recycle();

            if( 0 <= type && type < sData.length ) {
                mCurrentData = sData[type];
            }

            setBackgroundColor(mCurrentData.color);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSpecMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);

        if( widthSpecMode == MeasureSpec.EXACTLY ) {
//            setMeasuredDimension(widthSpecSize, Utility.toPixel(getContext(), mCurrentData.height));
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public void setLine(int height, int color) {
        mCurrentData = new Data(height, color);
        setBackgroundColor(color);
        invalidate();
    }

    public void setLineType(int type) {
        if( type >= 0 && type < sData.length ) {
            mCurrentData = sData[type];
            setBackgroundColor(mCurrentData.color);
            invalidate();
        }
    }

    private static class Data {
        public int height;
        public int color;

        public Data(int height, int color) {
            this.height = height;
            this.color = color;
        }
    }
}
