package rewhite.shopplus.logic;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;


public class TextFontNanumSquareOTFR extends AppCompatTextView {

    public TextFontNanumSquareOTFR(Context context) {
        super(context);
        setTpye(context);
    }

    public TextFontNanumSquareOTFR(Context context, AttributeSet attrs){
        super(context);
        setTpye(context);
    }

    public TextFontNanumSquareOTFR(Context context, AttributeSet attrs, int defSyleAttr){
        super(context);
        setTpye(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TextFontNanumSquareOTFR(Context context, AttributeSet attrs, int defSyleAttr, int defStyleRes){
        super(context);
        setTpye(context);
    }

    private void setTpye(Context context){
        //asset에 폰트 복사
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "nanum_square_otf_light.ttf"));
    }
}
