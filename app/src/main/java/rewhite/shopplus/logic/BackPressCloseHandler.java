package rewhite.shopplus.logic;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;

public class BackPressCloseHandler {

    private long backKeyPressedTime = 0;
    private Toast toast;

    private AppCompatActivity activity;

    public BackPressCloseHandler(AppCompatActivity context) {
        this.activity = context;
    }

    public void onBackPressed() {
        if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            showGuide();
            return;
        }
        if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
            activity.finish();
            toast.cancel();
        }
    }

    public void showGuide() {
        ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(activity);
        thirdConfirmPopup.setTitle("안내 팝업");
        thirdConfirmPopup.setContent("앱을 종료할까요?");
        thirdConfirmPopup.setButtonText("예");
        thirdConfirmPopup.setCancelButtonText("아니요");
        thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
            @Override
            public void onConfirmClick() {
                activity.moveTaskToBack(true);

                activity.finish();

                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
        thirdConfirmPopup.show();
    }
}
