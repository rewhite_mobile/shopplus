package rewhite.shopplus.logic;

import android.content.Context;

import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.util.SharedPreferencesUtil;

/**
 * 설정 관련 설정 로직 클래스
 * 해당 내용이 변경 될시 반드시!! 대응 코드 필요 수정시
 * 작업자 : 정재훈 확인 요망.
 */
public class SettingLogic {

    private static final String TAG = "SettingLogic";

    public SettingLogic() {

    }

    /**
     * 자동 로그인
     */
    public static void setAutoLogin(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_AUTO_LOGIN, PrefConstant.NAME_AUTO_LOGIN, status);
    }

    public static boolean getAutoLogin(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_AUTO_LOGIN, PrefConstant.NAME_AUTO_LOGIN, false);
    }


    /**
     * 로그인 AccessToken
     */
    public static void setPreAccessToken(Context context, String deviceId){
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_ACCESS_TOKEN, PrefConstant.NAME_ACCESS_TOKEN, deviceId);
    }
    public static String getPreAccessToken(Context context){
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_ACCESS_TOKEN, PrefConstant.NAME_ACCESS_TOKEN);
    }

    /**
     * GCM AccessToken
     */
    public static void setGCMAccessToken(Context context, String deviceId){
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_GCM_TOKEN, PrefConstant.NAME_GCM_TOKEN, deviceId);
    }
    public static String getGCMAccessToken(Context context){
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_GCM_TOKEN, PrefConstant.NAME_GCM_TOKEN);
    }

    /**
     * 로그인 DeviceToken
     */
    public static void setPrefsDeviceId(Context context, String deviceId){
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_PREFS_FILE, PrefConstant.NAME_PREFS_DEVICE_ID, deviceId);
    }

    public static String getPrefsDeviceId(Context context){
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_PREFS_FILE, PrefConstant.NAME_PREFS_DEVICE_ID);
    }

    /**
     * User StoreID
     */
    public static void setStoreUserId(Context context, String storeID){
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_STOREUSER_ID, PrefConstant.NAME_STOREUSER_ID, storeID);
    }

    public static String getStoreUserId(Context context){
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_STOREUSER_ID, PrefConstant.NAME_STOREUSER_ID);
    }

    /**
     * 시작택 번호 변경
     *
     * @param context
     * @param startTacNumber
     */
    public static void setItemStartTacNumver(Context context, String startTacNumber) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_NUMBER, startTacNumber);
    }

    public static String getItemStartTacNumver(Context context) {
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_NUMBER);
    }

    /**
     * 자주쓰는 메뉴 -> 문자보내기
     */
    public static void setSendText(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_SEND_TEXT, PrefConstant.NAME_SEND_TEXT, status);
    }

    public static boolean getSendText(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_SEND_TEXT, PrefConstant.NAME_SEND_TEXT, false);
    }

    /**
     * 자주쓰는 메뉴 -> 월별 매출현황
     */
    public static void setMonthlySalesStatus(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_MONTHLY_SALES_STATUS, PrefConstant.NAME_MONEHLY_SALES_STATUS, status);
    }

    public boolean getMonthlySalesStatus(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_MONTHLY_SALES_STATUS, PrefConstant.NAME_MONEHLY_SALES_STATUS, false);
    }

    /**
     * 자주쓰는 메뉴 -> 일별 매출현황
     */
    public static void setDailySalesStatus(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_DAILY_SALES_STATUS, PrefConstant.NAME_DAILY_SALES_STATUS, status);
    }

    public boolean getDailySalesStatus(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_DAILY_SALES_STATUS, PrefConstant.NAME_DAILY_SALES_STATUS, false);
    }

    /**
     * 자주쓰는 메뉴 -> 모바일 매출현황
     */
    public static void setMobileSalseStatus(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_MOBILE_SALES_STATUS, PrefConstant.NAME_MOBILE_SALES_STATUS, status);
    }

    public boolean getMobileSalseStatus(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_MOBILE_SALES_STATUS, PrefConstant.NAME_MOBILE_SALES_STATUS, false);
    }

    /**
     * 자주쓰는 메뉴 -> 방문접수 매출현황
     */
    public static void setVisitedReceptionSales(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_VISITED_RECEPTION_SALES, PrefConstant.NAME_VISITED_RECEPTION_SALES, status);
    }

    public boolean getVisitedReceptionSales(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_VISITED_RECEPTION_SALES, PrefConstant.NAME_VISITED_RECEPTION_SALES, false);
    }

    /**
     * 자주쓰는 메뉴 -> 세탁물 자동출고
     */
    public static void setAutomaticWarehoueseLaundry(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_AUTOMATIC_WAREHOUESE_OF_LAUNDRY, PrefConstant.NAME_AUTOMATIC_WAREHOUESE_OF_LAUNDRY, status);
    }

    public boolean getAutomaticWarehoueseLaundry(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_AUTOMATIC_WAREHOUESE_OF_LAUNDRY, PrefConstant.NAME_AUTOMATIC_WAREHOUESE_OF_LAUNDRY, false);
    }

    /**
     * 자주쓰는 메뉴 -> 세탁물 자동 세탁완료
     */
    public static void setLaundryAutomaticLaundry(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_LAUNDRY_AUTOMATIC_LAUNDRY, PrefConstant.NAME_LAUNDRY_AUTOMATIC_LAUNDRY, status);
    }

    public boolean getLaundryAutomaticLaundry(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_LAUNDRY_AUTOMATIC_LAUNDRY, PrefConstant.NAME_LAUNDRY_AUTOMATIC_LAUNDRY, false);
    }

    /**
     * 자주쓰는 메뉴 -> 마일리지 관리
     */
    public static void setMileageManagement(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_MILEAGE_MANAGEMENT, PrefConstant.NAME_MILEAGE_MANAGEMENT, status);
    }

    public boolean getMileageManagement(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_MILEAGE_MANAGEMENT, PrefConstant.NAME_MILEAGE_MANAGEMENT, false);
    }

    /**
     * 자주쓰는 메뉴 -> 선충전금 관리
     */
    public static void setPreChargeManaement(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_PRE_CHARGE_MANAGEMENT, PrefConstant.NAME_PRE_CHARGE_MANAGEMENT, status);
    }

    public boolean getPreChargeManaementt(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_PRE_CHARGE_MANAGEMENT, PrefConstant.NAME_PRE_CHARGE_MANAGEMENT, false);
    }

    /**
     * 자주쓰는 메뉴 -> 미수금 관리
     */
    public static void setAccountReceivableManagement(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_ACCOUNTS_RECEIVABLE_MANAGEMENT, PrefConstant.NAME_ACCOUNTS_RECEIVABLE_MANAGEMNT, status);
    }

    public boolean getAccountReceivableManagement(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_ACCOUNTS_RECEIVABLE_MANAGEMENT, PrefConstant.NAME_ACCOUNTS_RECEIVABLE_MANAGEMNT, false);
    }

    /**
     * 자주쓰는 메뉴 -> 미출고 세탁물현황
     */
    public static void setStatusUnLoadedLaundry(Context context, boolean status) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_STATUS_OF_UNLOADED_LAUNDRY, PrefConstant.NAME_STATUS_OF_UNLOADED_LAUNDRY, status);
    }

    public boolean getStatusUnLoadedLaundry(Context context) {
        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_STATUS_OF_UNLOADED_LAUNDRY, PrefConstant.NAME_STATUS_OF_UNLOADED_LAUNDRY, false);
    }

    /**
     * 평일 오전 영업시간
     * @param context
     * @param date
     */
    public static void setBusinessHoursWeekday(Context context, String date) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_BUSINESS_HOURS_WEEKDAYS_MORNING, PrefConstant.NAME_BUSINESS_HOURS_WEEKDAYS_MORNING, date);
    }

    public static String getBusinessHoursWeekday(Context context) {
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_BUSINESS_HOURS_WEEKDAYS_MORNING, PrefConstant.NAME_BUSINESS_HOURS_WEEKDAYS_MORNING);
    }


    /**
     * 평일 오후 영업시간
     * @param context
     * @param date
     */
    public static void setBusinessHoursWeekdayAfternoon(Context context, String date) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_BUSINESS_HOURS_WEEKDAYS_AFTERNOON, PrefConstant.NAME_BUSINESS_HOURS_WEEKDAYS_AFTERNOON, date);
    }

    public static String getBusinessHoursWeekdayAfternoon(Context context) {
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_BUSINESS_HOURS_WEEKDAYS_AFTERNOON, PrefConstant.NAME_BUSINESS_HOURS_WEEKDAYS_AFTERNOON);
    }

    /**
     * 토요일 오전 영업시간
     * @param context
     * @param date
     */
    public static void setBusinessHoursSaturdayMorning(Context context, String date) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_SATURDAY_BUSINESS_MORNING, PrefConstant.NAME_SATURDAY_BUSINESS_MORNING, date);
    }

    public static String getBusinessHoursSaturdayMorning(Context context) {
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_SATURDAY_BUSINESS_MORNING, PrefConstant.NAME_SATURDAY_BUSINESS_MORNING);
    }

    /**
     * 토요일 오후 영업시간
     * @param context
     * @param date
     */
    public static void setBusinessHoursSaturdayAfternoon(Context context, String date) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_SATURDAY_BUSINESS_AFTERNOON, PrefConstant.NAME_SATURDAY_BUSINESS_AFTERNOON, date);
    }

    public static String getBusinessHoursSaturdayAfternoon(Context context) {
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_SATURDAY_BUSINESS_AFTERNOON, PrefConstant.NAME_SATURDAY_BUSINESS_AFTERNOON);
    }

    /**
     * 일요일 오전 영업시간
     * @param context
     * @param date
     */
    public static void setBusinessHoursSundayMorning(Context context, String date) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_SUNDAY_BUSINESS_MORNING, PrefConstant.NAME_SUNDAY_BUSINESS_MORNING, date);
    }

    public static String getBusinessHoursSundayMorning(Context context) {
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_SUNDAY_BUSINESS_MORNING, PrefConstant.NAME_SUNDAY_BUSINESS_MORNING);
    }

    /**
     * 일요일 오후 영업시간
     * @param context
     * @param date
     */
    public static void setBusinessHoursSundayAfternoon(Context context, String date) {
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_SUNDAY_BUSINESS_AFTERNOON, PrefConstant.NAME_SUNDAY_BUSINESS_AFTERNOON, date);
    }

    public static String getBusinessHoursSundayAfternoon(Context context) {
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_SUNDAY_BUSINESS_AFTERNOON, PrefConstant.NAME_SUNDAY_BUSINESS_AFTERNOON);
    }

//    /**
//     * 고객센터 공지사항/자주묻는질문
//     * @param context
//     * @param status
//     */
//    public static void setServiceCenterChange(Context context, boolean status){
//        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_SERVICE_CENTER_CHANGE, PrefConstant.NAME_SERVICE_CENTER_CHANGE, status);
//    }
//
//    public static boolean getServiceCenterChange(Context context){
//        return SharedPreferencesUtil.getBooleanSharedPreference(context, PrefConstant.KEY_SERVICE_CENTER_CHANGE, PrefConstant.NAME_SERVICE_CENTER_CHANGE, false);
//    }

    /**
     * 패스워드 6개월 경과 변경 안내
     * @param context
     * @param status
     */
    public static void setPasswordChangeGuide(Context context, String status){
        SharedPreferencesUtil.putSharedPreference(context, PrefConstant.KEY_PASSWORD_CHANGE_GUIDE, PrefConstant.NAME_PASSWORD_CHANGE_GUIDE, status);
    }

    public static String getPasswordChangeGuide(Context context){
        return SharedPreferencesUtil.getStringSharedPreference(context, PrefConstant.KEY_PASSWORD_CHANGE_GUIDE, PrefConstant.NAME_PASSWORD_CHANGE_GUIDE);

    }

}