package rewhite.shopplus.web;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;

import rewhite.shopplus.activity.ShopPlusApplication;
import rewhite.shopplus.client.network.ConnectionConst;

public class DWebView extends WebView {
    private static final String TAG = "DWebView";

    public DWebView(Context context) {
        super(context);
        settings();
    }

    public DWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @SuppressLint({ "NewApi", "SetJavaScriptEnabled" })
    public void settings() {
        WebSettings settings = getSettings();

        String orginAgent = settings.getUserAgentString();
        StringBuffer sb = new StringBuffer(orginAgent);
        sb.append(ConnectionConst.AGENT_CHECK_NAME);
        sb.append(ShopPlusApplication.getAppType().getType());
        sb.append(ConnectionConst.AGENT_CHECK_SEPARATOR);
        sb.append(ShopPlusApplication.getAppVer());

        settings.setUserAgentString(sb.toString());

        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);

        settings.setAppCacheEnabled(false);
        settings.setPluginState(WebSettings.PluginState.ON);

        try {
            settings.setMediaPlaybackRequiresUserGesture(false);
        } catch(NoSuchMethodError e) {

        }

        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        settings.setLoadWithOverviewMode(true);
        settings.setSupportMultipleWindows(true);

        settings.setDomStorageEnabled(true);
        settings.setNeedInitialFocus(false);

        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
    }

}