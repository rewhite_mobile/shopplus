package rewhite.shopplus.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.view.adapter.AdapterSalesStatus;
import rewhite.shopplus.util.CustomViewPager;

/**
 * 매출현황 View
 */
public class ActivitySalesStatus extends AppCompatActivity {
    private static final String TAG = "ActivitySalesStatus";
    private CustomViewPager vpSalesStatus;

    private LinearLayout llMonthlySalesStatus;
    private ImageView ivIconMonthlySalesStatus;
    private TextView tvTabMonthlySalesStatus;

    private LinearLayout llDailySalesStatus;
    private ImageView ivIconDailySalesStatus;
    private TextView tvTabDailySalesStatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_status);
        setLayout();
        setViewPagerAdapter();
    }


    private void setLayout() {
        llMonthlySalesStatus = (LinearLayout) findViewById(R.id.ll_monthly_sales_status);
        ivIconMonthlySalesStatus = (ImageView) findViewById(R.id.iv_icon_monthly_sales_status);
        tvTabMonthlySalesStatus = (TextView) findViewById(R.id.tv_tab_monthly_sales_status);

        llDailySalesStatus = (LinearLayout) findViewById(R.id.ll_daily_sales_status);
        ivIconDailySalesStatus = (ImageView) findViewById(R.id.iv_icon_daily_sales_status);
        tvTabDailySalesStatus = (TextView) findViewById(R.id.tv_tab_daily_sales_status);

        vpSalesStatus = (CustomViewPager) findViewById(R.id.vp_sales_status);
    }

    private void setViewPagerAdapter() {
        vpSalesStatus.setAdapter(new AdapterSalesStatus(getSupportFragmentManager()));
        vpSalesStatus.setCurrentItem(0);
        setTapBackGround(0);

        llMonthlySalesStatus.setOnClickListener(movePageListener);
        llMonthlySalesStatus.setTag(0);
        llDailySalesStatus.setOnClickListener(movePageListener);
        llDailySalesStatus.setTag(1);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            vpSalesStatus.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    /**
     * 탭 UI 변경 분기처리 로직
     *
     * @param tag
     */
    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                llMonthlySalesStatus.setBackgroundResource(R.drawable.a_button_type_3);                // 월별매출현황
                ivIconMonthlySalesStatus.setBackgroundResource(R.drawable.btn_sales_press_00);
                tvTabMonthlySalesStatus.setTextColor(getResources().getColor(R.color.color_43425c));

                llDailySalesStatus.setBackgroundResource(R.drawable.a_button_type_2);
                ivIconDailySalesStatus.setBackgroundResource(R.drawable.btn_sales_normal_01);
                tvTabDailySalesStatus.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                break;

            case 1:
                llDailySalesStatus.setBackgroundResource(R.drawable.a_button_type_3);                  // 일별매출현황
                ivIconDailySalesStatus.setBackgroundResource(R.drawable.btn_sales_press_00);
                tvTabDailySalesStatus.setTextColor(getResources().getColor(R.color.color_43425c));

                llMonthlySalesStatus.setBackgroundResource(R.drawable.a_button_type_2);
                ivIconMonthlySalesStatus.setBackgroundResource(R.drawable.btn_sales_normal_01);
                tvTabMonthlySalesStatus.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                break;
        }
    }
}