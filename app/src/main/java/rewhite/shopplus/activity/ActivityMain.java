package rewhite.shopplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerStoreContractInfo;
import rewhite.shopplus.data.type.NetworkStatus;
import rewhite.shopplus.logic.BackPressCloseHandler;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.CustomViewPager;
import rewhite.shopplus.util.TimerHandler;
import rewhite.shopplus.view.adapter.AdapterMainPager;

import static rewhite.shopplus.activity.ShopPlusApplication.getCurrentActivity;

/**
 * Main 화면에서는 Fragment로 화면 View관리
 */
public class ActivityMain extends AppCompatActivity {
    private static final String TAG = "ActivityMain";

    private CustomViewPager vp;
    private TextView tvDate;
    private BackPressCloseHandler backPressCloseHandler;
    private LinearLayout llHome, llOrderInquiry, llCollectionManagement, llSalesStatus, llOperationsManagement, llCharacterManagement, llRewhiteMole;

    private LinearLayout llNotice;
    private ImageView ivIconHome, ivIconLnbOrder, ivIconLnbDelivery, ivIconLnbAccount, ivIconLnbManage, ivIconLnbSns, ivIconLnbShopping;
    private TextView tvTabHome, tvTabOrder, tvTabDelivery, tvTabAccount, tvTabManage, tvTabSns, tvTabShopping;
    private TextView tvLaundryTitleName;

    private LinearLayout llEnviroment, llServiceCenter;
    private Spinner sMember;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        backPressCloseHandler = new BackPressCloseHandler(this);
        checkNetWork();

        init();
    }

    private void init() {
        setLayout();
        setViewTimer();
        setViewPagerAdapter();
    }

    private void setLayout() {
        llEnviroment = (LinearLayout) findViewById(R.id.ll_enviroment);
        llServiceCenter = (LinearLayout) findViewById(R.id.ll_service_center);

        tvDate = (TextView) findViewById(R.id.tv_date);
        llHome = (LinearLayout) findViewById(R.id.ll_home);
        llOrderInquiry = (LinearLayout) findViewById(R.id.ll_order_inquiry);
        llCollectionManagement = (LinearLayout) findViewById(R.id.ll_collection_management);
        llSalesStatus = (LinearLayout) findViewById(R.id.ll_sales_status);
        llOperationsManagement = (LinearLayout) findViewById(R.id.ll_operations_management);
        llCharacterManagement = (LinearLayout) findViewById(R.id.ll_character_management);
        llRewhiteMole = (LinearLayout) findViewById(R.id.ll_rewhite_mole);
        llNotice = (LinearLayout) findViewById(R.id.ll_notice);

        ivIconHome = (ImageView) findViewById(R.id.iv_icon_home);
        ivIconLnbOrder = (ImageView) findViewById(R.id.iv_icon_lnb_order);
        ivIconLnbDelivery = (ImageView) findViewById(R.id.iv_icon_lnb_delivery);
        ivIconLnbAccount = (ImageView) findViewById(R.id.iv_icon_lnb_account);
        ivIconLnbManage = (ImageView) findViewById(R.id.iv_icon_lnb_manage);
        ivIconLnbSns = (ImageView) findViewById(R.id.iv_icon_lnb_sns);
        ivIconLnbShopping = (ImageView) findViewById(R.id.iv_icon_lnb_shopping);

        tvTabHome = (TextView) findViewById(R.id.tv_tab_home);
        tvTabOrder = (TextView) findViewById(R.id.tv_tab_order);
        tvTabDelivery = (TextView) findViewById(R.id.tv_tab_delivery);
        tvTabAccount = (TextView) findViewById(R.id.tv_tab_account);
        tvTabManage = (TextView) findViewById(R.id.tv_tab_manage);
        tvTabSns = (TextView) findViewById(R.id.tv_tab_sns);
        tvTabShopping = (TextView) findViewById(R.id.tv_tab_shopping);
        tvLaundryTitleName = (TextView) findViewById(R.id.tv_laundry_title_name);

        sMember = (Spinner) findViewById(R.id.s_member);
        vp = (CustomViewPager) findViewById(R.id.vp);

        setTextView();
        setOnClickView();
    }

    /**
     * TextView Setting
     */
    private void setTextView() {
        try {
            tvLaundryTitleName.setText(ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().getStoreName());
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * ClickView Setting
     */
    private void setOnClickView() {
        llEnviroment.setOnClickListener(btnOnClickListener);
        llServiceCenter.setOnClickListener(btnOnClickListener);
        llNotice.setOnClickListener(btnOnClickListener);
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            Intent intent = null;
            switch (id) {
                case R.id.ll_enviroment:
                    intent = new Intent(getApplicationContext(), ActivityEnviroment.class);
                    startActivity(intent);
                    break;

                case R.id.ll_service_center:
                    intent = new Intent(getApplicationContext(), ActivityServiceCenter.class);
                    startActivity(intent);
                    break;

                case R.id.ll_notice:
                    intent = new Intent(getApplicationContext(), ActivityNotice.class);
                    startActivity(intent);
                    break;
            }
        }
    };

    private void setSpinnerMember() {
        String[] arMember = getResources().getStringArray(R.array.member_views);
        ArrayList member = new ArrayList(Arrays.asList(arMember));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_sinner_custom_item, member);
        sMember.setAdapter(arrayAdapter);

        sMember.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setViewPagerAdapter() {
        vp.setAdapter(new AdapterMainPager(getSupportFragmentManager(), this));
        vp.setCurrentItem(0);
        vp.setOffscreenPageLimit(6);
        setTapBackGround(0);

        llHome.setOnClickListener(movePageListener);
        llHome.setTag(0);
        llOrderInquiry.setOnClickListener(movePageListener);
        llOrderInquiry.setTag(1);
        llCollectionManagement.setOnClickListener(movePageListener);
        llCollectionManagement.setTag(2);
        llSalesStatus.setOnClickListener(movePageListener);
        llSalesStatus.setTag(3);
        llOperationsManagement.setOnClickListener(movePageListener);
        llOperationsManagement.setTag(4);
        //TODO 2차
//        llCharacterManagement.setOnClickListener(movePageListener);
//        llCharacterManagement.setTag(5);
//        llRewhiteMole.setOnClickListener(movePageListener);
//        llRewhiteMole.setTag(6);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            vp.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    private void setViewTimer() {
        new TimerHandler(tvDate, "MM/dd(E)", TimerHandler.UP_A_DAY).start();
    }

    /**
     * 탭 UI 변경 분기처리 로직
     *
     * @param tag
     */
    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                setTabHome();                 // 홈
                break;

            case 1:
                setTabOrderInquiry();         // 주문조회
                break;

            case 2:
                setTabCollectionManagement(); // 수거배송관리
                break;

            case 3:
                setTabSalesStatus();          // 매출현황
                break;

            case 4:
                setTabOperationsManagement(); // 운영관리
                break;

            case 5:
                setTabCharacterManagement();  // 문자관리
                break;

            case 6:
//                setTabRewhiteMole();          // 리화이트몰
                break;

            default:
                break;
        }
    }

    /**
     * HOME Click Ui logic
     */
    private void setTabHome() {
        llHome.setBackgroundColor(getResources().getColor(R.color.transparent));

        llOrderInquiry.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCollectionManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llSalesStatus.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOperationsManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCharacterManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llRewhiteMole.setBackgroundColor(getResources().getColor(R.color.color_43425c));

        ivIconHome.setImageResource(R.drawable.home_sel);
        ivIconLnbOrder.setImageResource(R.drawable.clipboard);
        ivIconLnbDelivery.setImageResource(R.drawable.icon_package);
        ivIconLnbAccount.setImageResource(R.drawable.bar_chart);
        ivIconLnbManage.setImageResource(R.drawable.file_text);
        ivIconLnbSns.setImageResource(R.drawable.message_circle);
        ivIconLnbShopping.setImageResource(R.drawable.mall);

        tvTabHome.setTextColor(getResources().getColor(R.color.color_43425c));
        tvTabOrder.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabDelivery.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabAccount.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabManage.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabSns.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabShopping.setTextColor(getResources().getColor(R.color.color_c7cfe8));
    }

    /**
     * OrderInquiry Click Ui logic
     */
    private void setTabOrderInquiry() {
        llOrderInquiry.setBackgroundColor(getResources().getColor(R.color.transparent));

        llHome.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCollectionManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llSalesStatus.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOperationsManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCharacterManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llRewhiteMole.setBackgroundColor(getResources().getColor(R.color.color_43425c));


        ivIconLnbOrder.setImageResource(R.drawable.clipboard_sel);
        ivIconHome.setImageResource(R.drawable.home);
        ivIconLnbDelivery.setImageResource(R.drawable.icon_package);
        ivIconLnbAccount.setImageResource(R.drawable.bar_chart);
        ivIconLnbManage.setImageResource(R.drawable.file_text);
        ivIconLnbSns.setImageResource(R.drawable.message_circle);
        ivIconLnbShopping.setImageResource(R.drawable.mall);

        tvTabOrder.setTextColor(getResources().getColor(R.color.color_43425c));
        tvTabHome.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabDelivery.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabAccount.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabManage.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabSns.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabShopping.setTextColor(getResources().getColor(R.color.color_c7cfe8));
    }

    /**
     * CollectionManagement Click Ui logic
     */
    private void setTabCollectionManagement() {
        llCollectionManagement.setBackgroundColor(getResources().getColor(R.color.transparent));

        llHome.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOrderInquiry.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llSalesStatus.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOperationsManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCharacterManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llRewhiteMole.setBackgroundColor(getResources().getColor(R.color.color_43425c));

        ivIconLnbDelivery.setImageResource(R.drawable.package_sel);
        ivIconHome.setImageResource(R.drawable.home);
        ivIconLnbOrder.setImageResource(R.drawable.clipboard);
        ivIconLnbAccount.setImageResource(R.drawable.bar_chart);
        ivIconLnbManage.setImageResource(R.drawable.file_text);
        ivIconLnbSns.setImageResource(R.drawable.message_circle);
        ivIconLnbShopping.setImageResource(R.drawable.mall);

        tvTabDelivery.setTextColor(getResources().getColor(R.color.color_43425c));
        tvTabHome.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabOrder.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabAccount.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabManage.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabSns.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabShopping.setTextColor(getResources().getColor(R.color.color_c7cfe8));
    }

    /**
     * SalesStatus Click Ui logic
     */
    private void setTabSalesStatus() {
        llSalesStatus.setBackgroundColor(getResources().getColor(R.color.transparent));

        llHome.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOrderInquiry.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCollectionManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOperationsManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCharacterManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llRewhiteMole.setBackgroundColor(getResources().getColor(R.color.color_43425c));

        ivIconLnbAccount.setImageResource(R.drawable.bar_chart_sel);
        ivIconHome.setImageResource(R.drawable.home);
        ivIconLnbOrder.setImageResource(R.drawable.clipboard);
        ivIconLnbDelivery.setImageResource(R.drawable.icon_package);
        ivIconLnbManage.setImageResource(R.drawable.file_text);
        ivIconLnbSns.setImageResource(R.drawable.message_circle);
        ivIconLnbShopping.setImageResource(R.drawable.mall);

        tvTabAccount.setTextColor(getResources().getColor(R.color.color_43425c));
        tvTabHome.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabOrder.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabDelivery.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabManage.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabSns.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabShopping.setTextColor(getResources().getColor(R.color.color_c7cfe8));
    }

    /**
     * TabOperationsManagement Click Ui logic
     */
    private void setTabOperationsManagement() {
        llOperationsManagement.setBackgroundColor(getResources().getColor(R.color.transparent));

        llHome.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOrderInquiry.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llSalesStatus.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCollectionManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCharacterManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llRewhiteMole.setBackgroundColor(getResources().getColor(R.color.color_43425c));

        ivIconLnbManage.setImageResource(R.drawable.file_text_sel);
        ivIconHome.setImageResource(R.drawable.home);
        ivIconLnbOrder.setImageResource(R.drawable.clipboard);
        ivIconLnbDelivery.setImageResource(R.drawable.icon_package);
        ivIconLnbAccount.setImageResource(R.drawable.bar_chart);
        ivIconLnbSns.setImageResource(R.drawable.message_circle);
        ivIconLnbShopping.setImageResource(R.drawable.mall);

        tvTabManage.setTextColor(getResources().getColor(R.color.color_43425c));
        tvTabHome.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabOrder.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabDelivery.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabAccount.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabSns.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabShopping.setTextColor(getResources().getColor(R.color.color_c7cfe8));
    }

    /**
     * CharacterManagement Click Ui logic
     */
    private void setTabCharacterManagement() {
        llCharacterManagement.setBackgroundColor(getResources().getColor(R.color.transparent));

        llHome.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOrderInquiry.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llSalesStatus.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOperationsManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCollectionManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llRewhiteMole.setBackgroundColor(getResources().getColor(R.color.color_43425c));

        ivIconLnbSns.setImageResource(R.drawable.message_circle_sel);
        ivIconHome.setImageResource(R.drawable.home);
        ivIconLnbOrder.setImageResource(R.drawable.clipboard);
        ivIconLnbDelivery.setImageResource(R.drawable.icon_package);
        ivIconLnbAccount.setImageResource(R.drawable.bar_chart);
        ivIconLnbManage.setImageResource(R.drawable.file_text);
        ivIconLnbShopping.setImageResource(R.drawable.mall);

        tvTabSns.setTextColor(getResources().getColor(R.color.color_43425c));
        tvTabHome.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabOrder.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabDelivery.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabAccount.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabManage.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabShopping.setTextColor(getResources().getColor(R.color.color_c7cfe8));
    }

    /**
     * RewhiteMole Click Ui logic
     */
    private void setTabRewhiteMole() {
        llRewhiteMole.setBackgroundColor(getResources().getColor(R.color.transparent));

        llHome.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOrderInquiry.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llSalesStatus.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llOperationsManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCollectionManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));
        llCharacterManagement.setBackgroundColor(getResources().getColor(R.color.color_43425c));

        ivIconLnbShopping.setImageResource(R.drawable.mall_sel);
        ivIconHome.setImageResource(R.drawable.home);
        ivIconLnbOrder.setImageResource(R.drawable.clipboard);
        ivIconLnbDelivery.setImageResource(R.drawable.icon_package);
        ivIconLnbAccount.setImageResource(R.drawable.bar_chart);
        ivIconLnbManage.setImageResource(R.drawable.file_text);
        ivIconLnbSns.setImageResource(R.drawable.message_circle);

        tvTabShopping.setTextColor(getResources().getColor(R.color.color_43425c));
        tvTabHome.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabOrder.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabDelivery.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabAccount.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabManage.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        tvTabSns.setTextColor(getResources().getColor(R.color.color_c7cfe8));
    }

    /**
     * 네트워크 상태 CheckLogic
     */
    private void checkNetWork() {
        try {
            NetworkStatus networkStatus = ShopPlusApplication.getCurrnetNetworkStatus();
            if (!networkStatus.equals(NetworkStatus.WIFI)) {
                CommonUtil.netWorkErrPoup(this);
            }
            getCurrentActivity();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        backPressCloseHandler.onBackPressed();
    }
}
