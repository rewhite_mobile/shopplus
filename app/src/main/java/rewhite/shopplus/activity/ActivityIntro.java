package rewhite.shopplus.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.firebase.iid.FirebaseInstanceId;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerAuthStoreLogin;
import rewhite.shopplus.client.network.AuthManager;
import rewhite.shopplus.common.activity.BaseActivity;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;

/**
 * intro class
 */
public class ActivityIntro extends BaseActivity {
    private static final String TAG = "ActivityIntro";

    private String DeviceToken;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        Logger.setAllLogEnable(getApplicationContext(), getResources().getBoolean(R.bool.log_enable));

        //GCM DeviceToken 생성로직
        FirebaseInstanceId.getInstance().getToken();
        setLayout();
    }

    /**
     * layout Setting
     */
    private void setLayout() {
        setIntroAnimation();

        ImageView imgLodingIcon = (ImageView) findViewById(R.id.img_intro_logo);
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.progress_animation_loding);
        imgLodingIcon.setAnimation(anim);
    }

    /**
     * intro icon animation & data init
     */
    private void setIntroAnimation() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    if (SettingLogic.getPreAccessToken(getApplicationContext()) != null || !SettingLogic.getPreAccessToken(getApplicationContext()).equals("") || !TextUtils.isEmpty(SettingLogic.getPreAccessToken(getApplicationContext()))) {
                        AuthManager.CheckAccessToken(ActivityIntro.this, SettingLogic.getPreAccessToken(getApplicationContext()));
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    public void run() {
                        //자동로그인 설정
                        if (isFirstApp() == true) {
                            finish();
                            return;
                        }
                        if (ManagerAuthStoreLogin.getmInstance().getmanagerAuthStoreLogin().get(0).getResultCode().equals("S0000") ||
                                !SettingLogic.getPreAccessToken(getApplicationContext()).equals("") || !TextUtils.isEmpty(SettingLogic.getPreAccessToken(getApplicationContext()))) {
                            if (SettingLogic.getAutoLogin(getApplicationContext())) {
                                if (!TextUtils.isEmpty(SettingLogic.getGCMAccessToken(getApplicationContext())) && SettingLogic.getAutoLogin(getApplicationContext()) == true) {

                                    Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
                                    intent.addCategory(Intent.CATEGORY_HOME);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    //기존 계정 로그인)
                                    if (SettingLogic.getGCMAccessToken(getApplicationContext()) != null || !TextUtils.isEmpty(SettingLogic.getGCMAccessToken(getApplicationContext()))) {
                                        Intent intent = new Intent(getApplicationContext(), ActivityExistingAccountLogin.class);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                                        intent.addCategory(Intent.CATEGORY_HOME);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            } else {
                                //기존 계정 로그인
                                if (SettingLogic.getGCMAccessToken(getApplicationContext()) != null || !TextUtils.isEmpty(SettingLogic.getGCMAccessToken(getApplicationContext()))) {
                                    Intent intent = new Intent(getApplicationContext(), ActivityExistingAccountLogin.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                                    intent.addCategory(Intent.CATEGORY_HOME);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        } else {
                            Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
            }
        }).start();
    }

    /**
     * 최초 실행 여부 판단하는 구문 로직
     */
    private boolean isFirstApp() {
        SharedPreferences pref = getSharedPreferences("isFirst", Activity.MODE_PRIVATE);
        boolean first = pref.getBoolean("isFirst", false);
        if (first == false) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("isFirst", true);
            editor.commit();

            //AccessToken 데이터 저장
            AuthManager.CheckAccessToken(ActivityIntro.this, SettingLogic.getGCMAccessToken(getApplicationContext()));

            Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

            return true;
        } else {
            Log.d("Is first Time?", "not first");
            return false;
        }
    }
}