package rewhite.shopplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.manager.ManagerPaymentVisitOrder;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.model.GetStoreManageInfo;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.model.StoreUserInfoItemList;
import rewhite.shopplus.client.network.PaymentManager;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.data.dto.ItemGetVisitOrderList;
import rewhite.shopplus.data.manager.ManagerItemRegistrationAdd;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.CustomKeyboard;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.PaymenrWaitCounter;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.util.hardware.FirstHardware;
import rewhite.shopplus.view.adapter.AdapterInstallment;
import rewhite.shopplus.view.adapter.AdapterPaymentDelivery;

/**
 * 결제하기 공통 Activity Class
 */
public class ActivityPayment extends AppCompatActivity {
    private static final String TAG = "ActivityPayment";
    private static final String paymentType = "1";         // 결제하기 Type;
    private int paymentStatusType = 0;  //결제 Type (현금, 카드 , 현금 + 카드)
    private int keybordType = 0;        //키보드 입력값 Type에 따른 입력
    private ManagerGetVisitOrder ManagerGetVisitOrder;
    private PaymenrWaitCounter mPaymentWait;

    private LinearLayout llPaymentCount, llCashPayment, llCardPayment;
    private LinearLayout llInstallment, llCashReceipts, llReceivables;

    private List<StoreUserInfoItemList> userInfoItemList;
    private List<GetVisitOrderList> mGetVisitOrderLists = new ArrayList<>();
    private List<GetStoreManageInfo> getStoreManageInfo;
    private String requestCode;
    private int allSelectPayment;
    private Button btnCash, btnCard, btnCardCash, btnPaymentAgree;
    private Button btnMakePayment, btnLaterMakePayment, btnFilledGoldAllUse, btnMileagePaymentAllUse;

    private String beforeText;
    private int normalPayment, payingCreditCard;
    private ImageView imgClose;
    private RadioGroup rgCashReceipts, rgSelectInstallment;
    private RadioButton rbPublish, rbNoPublish;
    private RadioButton rbLumpSum, rbInstallment;

    private ListView lvPaymentDelivery;
    private String cashReceipts = "Y";
    private TextView tvPaymentTitleType, tvPaymenrUserName, tvPaymentUserPhoneNumber, tvPaymentUserAddress, tvSalePayment;
    private TextView tvPreFilledGoldEt, tvMileagePaymentEt, tvCashPayment, tvCardPayment, tvPreFilledGold, tvMileagePayment;
    private TextView tvTotalCount, tvItemTotalPayment, tvLaundryCharge, tvDiscountedAmount, tvPaymentAmountPaid, tvMileageWon;

    private TextView tvLaundryData, tvLaundry;
    private TextView tvPayAmountOutstanding, tvAmountOutstanding, tvReceivables, tvReceivablesPay;
    private EditText etCashPayment;

    private RecyclerView rvInstallment;
    private String orderItemId;


    private int paymentTypeData, mCheckCount, mTotalPayment, selectPayment;
    private int preFilledGold, mileagePaymentAllUse, preFilledGoldet, mileagePaymentAllUseet;
    private WaitCounter mWait;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        View view = findViewById(R.id.rootView);

        mWait = new WaitCounter(this);
        mPaymentWait = new PaymenrWaitCounter(ActivityPayment.this);

        Intent intent = getIntent();
        paymentTypeData = intent.getExtras().getInt(paymentType);
        init(view);
    }

    private void init(View view) {
        FirstHardware.init(this);

        userInfoItemList = ManagerStoreUserInfo.getmInstance().getStoreUserInfo();
        getStoreManageInfo = ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList();

        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    if (paymentTypeData == 3 || paymentTypeData == 1) {
                        ManagerGetVisitOrder = VisitReceptionManager.getGetVisitOrder(ActivityPayment.this, "NEEDPAY", SettingLogic.getStoreUserId(getApplicationContext()), "", "");
                    }
                    ActivityPayment.this.runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();

                            setLayout(view);
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * Layout Setting View
     *
     * @param view
     */
    private void setLayout(View view) {
        btnCard = (Button) findViewById(R.id.btn_card);
        btnCash = (Button) findViewById(R.id.btn_cash);
        btnCardCash = (Button) findViewById(R.id.btn_card_cash);
        btnPaymentAgree = (Button) findViewById(R.id.btn_payment_agree);

        llInstallment = (LinearLayout) findViewById(R.id.ll_installment);
        llCashReceipts = (LinearLayout) findViewById(R.id.ll_cash_receipts);
        llReceivables = (LinearLayout) findViewById(R.id.ll_receivables);
        llCashPayment = (LinearLayout) findViewById(R.id.ll_cash_payment);
        llCardPayment = (LinearLayout) findViewById(R.id.ll_card_payment);

        tvLaundryData = (TextView) findViewById(R.id.tv_laundry_data);
        tvLaundry = (TextView) findViewById(R.id.tv_laundry);

        imgClose = (ImageView) findViewById(R.id.img_close);
        llPaymentCount = (LinearLayout) findViewById(R.id.ll_payment_count);
        etCashPayment = (EditText) findViewById(R.id.et_cash_payment);
        tvPreFilledGoldEt = (TextView) findViewById(R.id.tv_pre_filled_gold_et);
        tvMileagePaymentEt = (TextView) findViewById(R.id.tv_mileage_payment_et);
        tvCashPayment = (TextView) findViewById(R.id.tv_cash_payment);
        tvCardPayment = (TextView) findViewById(R.id.tv_card_payment);
        tvPreFilledGold = (TextView) findViewById(R.id.tv_pre_filled_gold);
        tvMileagePayment = (TextView) findViewById(R.id.tv_mileage_payment);
        tvPaymenrUserName = (TextView) findViewById(R.id.tv_paymenr_user_name);
        tvPaymentUserPhoneNumber = (TextView) findViewById(R.id.tv_payment_user_phone_number);
        tvPaymentUserAddress = (TextView) findViewById(R.id.tv_payment_user_address);
        tvSalePayment = (TextView) findViewById(R.id.tv_sale_payment);
        tvTotalCount = (TextView) findViewById(R.id.tv_total_count);
        tvItemTotalPayment = (TextView) findViewById(R.id.tv_item_total_payment);
        tvLaundryCharge = (TextView) findViewById(R.id.tv_laundry_charge);
        tvDiscountedAmount = (TextView) findViewById(R.id.tv_discounted_amount);
        tvPaymentAmountPaid = (TextView) findViewById(R.id.tv_payment_amount_paid);
        tvMileageWon = (TextView) findViewById(R.id.tv_mileage_won);

        rgCashReceipts = (RadioGroup) findViewById(R.id.rg_cash_receipts);
        rgSelectInstallment = (RadioGroup) findViewById(R.id.rg_select_installment);

        rbPublish = (RadioButton) findViewById(R.id.rb_publish);
        rbNoPublish = (RadioButton) findViewById(R.id.rb_no_publish);
        rbLumpSum = (RadioButton) findViewById(R.id.rb_lump_sum);
        rbInstallment = (RadioButton) findViewById(R.id.rb_installment);

        rvInstallment = (RecyclerView) findViewById(R.id.rv_installment);

        lvPaymentDelivery = (ListView) findViewById(R.id.lv_payment_delivery);
        btnMakePayment = (Button) findViewById(R.id.btn_make_payment);
        btnLaterMakePayment = (Button) findViewById(R.id.btn_later_make_payment);
        btnFilledGoldAllUse = (Button) findViewById(R.id.btn_filled_gold_all_use);
        btnMileagePaymentAllUse = (Button) findViewById(R.id.btn_mileage_payment_all_use);

        tvPaymentTitleType = (TextView) findViewById(R.id.tv_payment_title_type);

        tvPayAmountOutstanding = (TextView) findViewById(R.id.tv_pay_amount_outstanding);
        tvAmountOutstanding = (TextView) findViewById(R.id.tv_amount_outstanding);
        tvReceivables = (TextView) findViewById(R.id.tv_receivables);
        tvReceivablesPay = (TextView) findViewById(R.id.tv_receivables_pay);

        setListViewContextType();
        setClickListener();
        setInstallmentAdapter();
        setCustomKeyBoard(view);
        setTextView();
    }

    /**
     * 리스트 View Type 설정
     */
    private void setListViewContextType() {
        if (paymentTypeData == 0) {
            setPaymentAdapter();        //결제하기
        } else if (paymentTypeData == 2) {
            setDeliveryCompleted();     //출고완료
        } else if (paymentTypeData == 3) {
            setReceivingReceivables();  //미수금수령
        } else if (paymentTypeData == 1) {
            setPaymentCompleted();      //접수완료
        }
    }

    /**
     * 초기 회원정보 및 결제 정보 TextSetting
     */
    private void setTextView() {
        int payPrice = 0;
        for (GetVisitOrderList getVisitOrderList : ItemGetVisitOrderList.getmInstance().getGetVisitOrder()) {
            payPrice += getVisitOrderList.getNonPayPrice();
        }
        tvPayAmountOutstanding.setText(DataFormatUtil.moneyFormatToWon(payPrice) + " 원");

        tvPaymenrUserName.setText(userInfoItemList.get(0).getData().getUserName());        // 회원이름
        tvPaymentUserPhoneNumber.setText(userInfoItemList.get(0).getData().get_UserPhone()); // 회원휴대번호
        tvPaymentUserAddress.setText(userInfoItemList.get(0).getData().get_UserAddress());     // 회원주소

        //선충전금
        if (userInfoItemList.get(0).getData().getRealPrePaid() == 0) {
            tvPreFilledGoldEt.setBackgroundResource(R.drawable.a_button_type_3);
            tvPreFilledGoldEt.setTextColor(getResources().getColor(R.color.color_dddddd));

            btnFilledGoldAllUse.setBackgroundResource(R.color.color_ebebeb);
            btnFilledGoldAllUse.setTextColor(getResources().getColor(R.color.color_dddddd));
        } else {
            btnFilledGoldAllUse.setOnClickListener(btnOnClickListener);
            tvPreFilledGoldEt.setOnClickListener(onClickChangeListener);

            tvPreFilledGold.setText(DataFormatUtil.moneyFormatToWon(userInfoItemList.get(0).getData().getRealPrePaid()) + "원 보유");
            tvPreFilledGoldEt.setBackgroundResource(R.drawable.a_button_type_3);
            tvPreFilledGoldEt.setTextColor(getResources().getColor(R.color.color_43425c));

            btnFilledGoldAllUse.setBackgroundResource(R.drawable.a_button_type_2);
            btnFilledGoldAllUse.setTextColor(getResources().getColor(R.color.Wite_color));
        }

        //마일리지
        if (userInfoItemList.get(0).getData().getMileage() == 0) {
            btnMileagePaymentAllUse.setBackgroundResource(R.color.color_ebebeb);
            btnMileagePaymentAllUse.setTextColor(getResources().getColor(R.color.color_dddddd));

            tvMileagePaymentEt.setBackgroundResource(R.drawable.a_button_type_3);
            tvMileagePaymentEt.setTextColor(getResources().getColor(R.color.color_dddddd));
            tvMileagePayment.setText(DataFormatUtil.moneyFormatToWon(userInfoItemList.get(0).getData().getMileage()) + "원 보유");
        } else {
            btnMileagePaymentAllUse.setOnClickListener(btnOnClickListener);
            tvMileagePaymentEt.setOnClickListener(onClickChangeListener);

            btnMileagePaymentAllUse.setBackgroundResource(R.drawable.a_button_type_2);
            btnMileagePaymentAllUse.setTextColor(getResources().getColor(R.color.Wite_color));

            tvMileagePaymentEt.setBackgroundResource(R.drawable.a_button_type_3);
            tvMileagePaymentEt.setTextColor(getResources().getColor(R.color.color_43425c));
            tvMileagePayment.setText(DataFormatUtil.moneyFormatToWon(userInfoItemList.get(0).getData().getMileage()) + "원 보유");
        }

        preFilledGold = userInfoItemList.get(0).getData().getRealPrePaid();
        mileagePaymentAllUse = userInfoItemList.get(0).getData().getMileage();
    }

    /**
     * 미수금 수령하기 View
     */
    private void setReceivingReceivables() {
        int nonPayPrice = 0;
        int orderItemPrice = 0;

        llReceivables.setVisibility(View.VISIBLE);

        List<GetVisitOrderList> visitOrderLists = ManagerGetVisitOrder.getGetVisitOrder();

        for (GetVisitOrderList getVisitOrderList : visitOrderLists) {
            nonPayPrice += getVisitOrderList.getNonPayPrice();
            orderItemPrice += getVisitOrderList.getOrderItemPrice();
        }

        AdapterPaymentDelivery adapterPaymentDelivery = new AdapterPaymentDelivery(getApplicationContext(), paymentAmount);
        adapterPaymentDelivery.addItem(visitOrderLists);
        lvPaymentDelivery.setAdapter(adapterPaymentDelivery);

        tvReceivables.setText("미수금수령하기");
        tvReceivablesPay.setText(DataFormatUtil.moneyFormatToWon(orderItemPrice) + " 원");

        tvAmountOutstanding.setText("총 주문금액 ");
        tvPayAmountOutstanding.setText(DataFormatUtil.moneyFormatToWon(orderItemPrice) + " 원");
    }

    /**
     * 접수완료
     */
    private void setPaymentCompleted() {
        int nonPayPrice = 0;
        int orderItemPrice = 0;

        llReceivables.setVisibility(View.VISIBLE);
        tvPaymentTitleType.setText("접수완료");

        List<GetVisitOrderList> visitOrderLists = ManagerItemRegistrationAdd.getmInstance().getGetVisitOrder();

        for (GetVisitOrderList getVisitOrderList : visitOrderLists) {
            nonPayPrice += getVisitOrderList.getNonPayPrice();
            orderItemPrice += getVisitOrderList.getOrderItemPrice();

            try {
                tvLaundryData.setText(DataFormatUtil.setDateMothFormat(getVisitOrderList.getWashFinWillDate()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        AdapterPaymentDelivery adapterPaymentDelivery = new AdapterPaymentDelivery(getApplicationContext(), paymentAmount);
        adapterPaymentDelivery.addItem(visitOrderLists);
        lvPaymentDelivery.setAdapter(adapterPaymentDelivery);
        tvLaundry.setVisibility(View.VISIBLE);
        tvLaundryData.setVisibility(View.VISIBLE);

        tvReceivables.setText("총 주문금액");
        tvReceivablesPay.setText(DataFormatUtil.moneyFormatToWon(orderItemPrice) + " 원");

        tvAmountOutstanding.setText("미수금");
        tvPayAmountOutstanding.setText(DataFormatUtil.moneyFormatToWon(nonPayPrice) + " 원");
    }

    /**
     * 출고완료 View
     */
    private void setDeliveryCompleted() {
        int nonPayPrice = 0;
        int orderItemPrice = 0;

        llReceivables.setVisibility(View.VISIBLE);
        tvPaymentTitleType.setText("출고완료");

        List<GetVisitOrderList> visitOrderLists = ItemGetVisitOrderList.getmInstance().getGetVisitOrder();

        for (GetVisitOrderList getVisitOrderList : visitOrderLists) {
            nonPayPrice += getVisitOrderList.getNonPayPrice();
            orderItemPrice += getVisitOrderList.getOrderItemPrice();
        }

        AdapterPaymentDelivery adapterPaymentDelivery = new AdapterPaymentDelivery(getApplicationContext(), paymentAmount);
        adapterPaymentDelivery.addItem(visitOrderLists);
        lvPaymentDelivery.setAdapter(adapterPaymentDelivery);

        tvReceivables.setText("총 주문금액");
        tvReceivablesPay.setText(DataFormatUtil.moneyFormatToWon(nonPayPrice) + " 원");

        tvAmountOutstanding.setText("미수금");
        tvPayAmountOutstanding.setText(DataFormatUtil.moneyFormatToWon(orderItemPrice) + " 원");
    }

    /**
     * 결제하기 PaymentView
     */
    private void setPaymentAdapter() {
        tvPaymentTitleType.setText("결제하기");

        List<GetVisitOrderList> visitOrderLists = ItemGetVisitOrderList.getmInstance().getGetVisitOrder();

        AdapterPaymentDelivery adapterPaymentDelivery = new AdapterPaymentDelivery(getApplicationContext(), paymentAmount);
        adapterPaymentDelivery.addItem(visitOrderLists);
        lvPaymentDelivery.setAdapter(adapterPaymentDelivery);
    }

    /**
     * CustomKeyBoard reset
     *
     * @param view
     */
    private void setCustomKeyBoard(View view) {
        CustomKeyboard mCustomKeyboard = new CustomKeyboard(this, view, R.id.keyboardview, R.xml.custom_keyboard_type1);
        mCustomKeyboard.registerEditText(view, R.id.et_cash_payment);
    }

    private void setClickListener() {
        btnCash.setOnClickListener(btnOnClickListener);
        btnCard.setOnClickListener(btnOnClickListener);
        btnCardCash.setOnClickListener(btnOnClickListener);
        btnPaymentAgree.setOnClickListener(btnOnClickListener);
        btnMakePayment.setOnClickListener(btnOnClickListener);
        btnLaterMakePayment.setOnClickListener(btnOnClickListener);
        tvCashPayment.setOnClickListener(onClickChangeListener);
        tvCardPayment.setOnClickListener(onClickChangeListener);
        imgClose.setOnClickListener(onClickChangeListener);

        rgCashReceipts.setOnCheckedChangeListener(publishCheckListener);
        rgSelectInstallment.setOnCheckedChangeListener(installmentCheckListener);

        etCashPayment.addTextChangedListener(onTextWatcherListener);
    }

    private TextWatcher onTextWatcherListener = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            beforeText = s.toString().replaceAll("^0+", "");

            Logger.d(TAG, "beforeText : " + beforeText);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //선충전금 입력 Type
            if (keybordType == 0) {
                try {
                    if (paymentStatusType == 2) {
                        tvCashPayment.setText("0");
                        tvCardPayment.setText("0");
                    } else {
                        if (s.toString().length() > 0) {
                            if (Integer.parseInt(s.toString()) > preFilledGold) {
                                etCashPayment.setText(beforeText.replaceAll("^0+", ""));
                            } else {
                                preFilledGoldet = Integer.valueOf(s.toString());

                                normalPayment = selectPayment - (preFilledGoldet + mileagePaymentAllUseet);
                                payingCreditCard = selectPayment - (preFilledGoldet + mileagePaymentAllUseet);

                                tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(normalPayment));
                                tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(payingCreditCard));
                                tvPreFilledGoldEt.setText(DataFormatUtil.moneyFormatToWon(preFilledGoldet));
                            }
                            getPaymentAmount();
                        } else if (s.toString().length() == 0) {
                            normalPayment = selectPayment;
                            payingCreditCard = selectPayment;
                        }
                    }
                } catch (NumberFormatException err) {
                    err.printStackTrace();
                }

                //마일리지 입력 Type
            } else if (keybordType == 1) {
                try {
                    if (s.toString().length() > 0) {
                        Logger.d(TAG, "Integer.parseInt(s.toString())  : " + Integer.parseInt(s.toString()));
                        Logger.d(TAG, "mileagePaymentAllUseet : " + mileagePaymentAllUseet
                        );
                        if (Integer.parseInt(s.toString()) >= mileagePaymentAllUse) {
                            etCashPayment.setText(beforeText.replaceAll("^0+", ""));
                        } else {
                            mileagePaymentAllUseet = Integer.valueOf(s.toString());

                            normalPayment = selectPayment - (preFilledGoldet + mileagePaymentAllUseet);
                            payingCreditCard = selectPayment - (preFilledGoldet + mileagePaymentAllUseet);

                            tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(normalPayment));
                            tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(payingCreditCard));
                            Logger.d(TAG, "mileagePaymentAllUseet : " + mileagePaymentAllUseet);
                            tvMileagePaymentEt.setText(DataFormatUtil.moneyFormatToWon(mileagePaymentAllUseet));
                        }
                    } else if (s.toString().length() == 0) {
                        normalPayment = selectPayment;
                        payingCreditCard = selectPayment;
                    }
                    getPaymentAmount();
                } catch (NumberFormatException err) {
                    err.printStackTrace();
                }
                //현금결제 입력 Type
            } else if (keybordType == 2) {
                try {
                    if (s.toString().length() > 0) {
                        if (Integer.parseInt(s.toString()) > mTotalPayment) {
                            etCashPayment.setText(beforeText.replaceAll("^0+", ""));
                        } else {
                            if (s.toString().equals("") || s.toString().length() == 0) {
                                normalPayment = 0;
                            } else {
                                normalPayment = Integer.valueOf(s.toString());
                            }

                            //초과된 입력불가 하도록 예외처리.
                            if (normalPayment >= mTotalPayment) {
                                tvCashPayment.setText(String.valueOf(mTotalPayment));
                            } else {
                                tvCashPayment.setText(s.toString().replaceAll("^0+", ""));
                            }

                            //현금 + 카드 일 경우 예외 처리.
                            payingCreditCard = mTotalPayment - normalPayment;
                            if (payingCreditCard < 0) {
                                tvCardPayment.setText(String.valueOf(0));
                            } else {
                                tvCardPayment.setText(String.valueOf(payingCreditCard));
                            }
                        }
                    }
                } catch (NumberFormatException err) {
                    err.printStackTrace();
                }
                //카드결제 입력 Type
            } else if (keybordType == 3) {
                try {
                    if (s.toString().length() > 0) {
                        if (Integer.parseInt(s.toString()) > mTotalPayment) {
                            etCashPayment.setText(beforeText.replaceAll("^0+", ""));
                        } else {
                            if (s.toString().equals("") || s.toString().length() == 0) {
                                payingCreditCard = 0;
                            } else {
                                payingCreditCard = Integer.valueOf(s.toString());
                            }

                            //초과된 입력불가 하도록 예외처리.
                            if (payingCreditCard >= mTotalPayment) {
                                tvCardPayment.setText(String.valueOf(mTotalPayment));
                            } else {
                                tvCardPayment.setText(s.toString().replaceAll("^0+", ""));
                            }

                            //현금 + 카드 일 경우 예외 처리.
                            normalPayment = mTotalPayment - payingCreditCard;
                            if (normalPayment < 0) {
                                tvCashPayment.setText(String.valueOf(0));
                            } else {
                                tvCashPayment.setText(String.valueOf(normalPayment));
                            }
                        }
                    }
                } catch (NumberFormatException err) {
                    err.printStackTrace();
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    /**
     * 결제 예정금액 계산 로직
     */
    private void getPaymentAmount() {
        try {
            tvSalePayment.setText(DataFormatUtil.moneyFormatToWon((mileagePaymentAllUseet + preFilledGoldet)));
            tvDiscountedAmount.setText(DataFormatUtil.moneyFormatToWon(preFilledGoldet + mileagePaymentAllUseet) + "원");

            tvPaymentAmountPaid.setText(DataFormatUtil.moneyFormatToWon(mTotalPayment - (preFilledGoldet + mileagePaymentAllUseet)));
            tvMileageWon.setText(String.valueOf(Math.round((mTotalPayment - (preFilledGoldet + mileagePaymentAllUseet)) * getStoreManageInfo.get(0).getData().getCashMileageRate() / 100)) + "원");

        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * 카드 할부 AdapterView
     */
    private void setInstallmentAdapter() {
        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(11, StaggeredGridLayoutManager.VERTICAL);
        rvInstallment.setLayoutManager(layoutManager);
        AdapterInstallment adapterInstallment = new AdapterInstallment(getApplicationContext(), mOnAdapterListner);

        ArrayList<String> arrayListNumber = new ArrayList<>();
        for (int i = 2; i <= 12; i++) {
            arrayListNumber.add(String.valueOf(i));
        }

        adapterInstallment.addItem(arrayListNumber);
        rvInstallment.setAdapter(adapterInstallment);
    }

    private View.OnClickListener onClickChangeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.tv_pre_filled_gold_et:
                    llPaymentCount.setVisibility(View.VISIBLE);
                    tvPreFilledGoldEt.setBackgroundResource(R.drawable.a_box_round_custom_type_1);

                    tvMileagePaymentEt.setBackgroundResource(R.drawable.a_button_type_3);
                    tvCashPayment.setBackgroundResource(R.drawable.a_button_type_3);
                    tvCardPayment.setBackgroundResource(R.drawable.a_button_type_3);
                    keybordType = 0;
                    etCashPayment.setText("0");

                    break;
                case R.id.tv_mileage_payment_et:
                    llPaymentCount.setVisibility(View.VISIBLE);
                    tvMileagePaymentEt.setBackgroundResource(R.drawable.a_box_round_custom_type_1);

                    tvPreFilledGoldEt.setBackgroundResource(R.drawable.a_button_type_3);
                    tvCashPayment.setBackgroundResource(R.drawable.a_button_type_3);
                    tvCardPayment.setBackgroundResource(R.drawable.a_button_type_3);
                    keybordType = 1;
                    etCashPayment.setText("0");
                    break;

                case R.id.tv_cash_payment:
                    if (paymentStatusType == 2) {
                        llPaymentCount.setVisibility(View.VISIBLE);
                    }
                    tvCashPayment.setBackgroundResource(R.drawable.a_box_round_custom_type_1);

                    tvPreFilledGoldEt.setBackgroundResource(R.drawable.a_button_type_3);
                    tvCardPayment.setBackgroundResource(R.drawable.a_button_type_3);
                    tvMileagePaymentEt.setBackgroundResource(R.drawable.a_button_type_3);
                    keybordType = 2;
                    break;

                case R.id.tv_card_payment:
                    if (paymentStatusType == 2) {
                        llPaymentCount.setVisibility(View.VISIBLE);
                    }
                    tvCardPayment.setBackgroundResource(R.drawable.a_box_round_custom_type_1);

                    tvPreFilledGoldEt.setBackgroundResource(R.drawable.a_button_type_3);
                    tvCashPayment.setBackgroundResource(R.drawable.a_button_type_3);
                    tvMileagePaymentEt.setBackgroundResource(R.drawable.a_button_type_3);
                    keybordType = 3;
                    break;

                case R.id.img_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityPayment.this);
                    thirdConfirmPopup.setTitle("종료팝업");
                    thirdConfirmPopup.setContent("팝업을 종료할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setOnCancelClickListener(new ThirdConfirmPopup.OnCancelClickListener() {
                        @Override
                        public void onCancelClick() {
                            finish();
                        }
                    });
                    thirdConfirmPopup.setCancelButtonText("아니오");
                    thirdConfirmPopup.show();
                    break;
            }
        }
    };

    /**
     * 현금 영수증 발행 View
     */
    private RadioGroup.OnCheckedChangeListener publishCheckListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == R.id.rb_publish) {
                cashReceipts = "Y";
                rbPublish.setTextColor(getResources().getColor(R.color.Wite_color));
                rbPublish.setBackgroundResource(R.drawable.a_button_type_2);

                rbNoPublish.setTextColor(getResources().getColor(R.color.color_43425c));
                rbNoPublish.setBackgroundResource(R.drawable.a_button_type_3);
            } else if (checkedId == R.id.rb_no_publish) {
                cashReceipts = "N";
                rbNoPublish.setTextColor(getResources().getColor(R.color.Wite_color));
                rbNoPublish.setBackgroundResource(R.drawable.a_button_type_2);

                rbPublish.setTextColor(getResources().getColor(R.color.color_43425c));
                rbPublish.setBackgroundResource(R.drawable.a_button_type_3);
            }
        }
    };

    /**
     * 할부선택 View
     */
    private RadioGroup.OnCheckedChangeListener installmentCheckListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == R.id.rb_lump_sum) {
                rvInstallment.setVisibility(View.GONE); //일시불

                rbLumpSum.setTextColor(getResources().getColor(R.color.Wite_color));
                rbLumpSum.setBackgroundResource(R.drawable.a_button_type_2);

                rbInstallment.setTextColor(getResources().getColor(R.color.color_43425c));
                rbInstallment.setBackgroundResource(R.drawable.a_button_type_3);
            } else if (checkedId == R.id.rb_installment) {
                rvInstallment.setVisibility(View.VISIBLE);  //할부

                rbInstallment.setTextColor(getResources().getColor(R.color.Wite_color));
                rbInstallment.setBackgroundResource(R.drawable.a_button_type_2);

                rbLumpSum.setTextColor(getResources().getColor(R.color.color_43425c));
                rbLumpSum.setBackgroundResource(R.drawable.a_button_type_3);
            }
        }
    };

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityPayment.this);

            switch (id) {
                case R.id.btn_cash:
                    //선충전금 및 마일리지 예외처리 로직
                    if (preFilledGoldet != 0 || preFilledGoldet > 0 && mileagePaymentAllUseet != 0 || mileagePaymentAllUseet > 0) {
                        tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(selectPayment - (mileagePaymentAllUseet + preFilledGoldet)));
                    } else if (preFilledGoldet != 0 || preFilledGoldet > 0) {
                        tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(selectPayment - preFilledGoldet));
                    } else if (mileagePaymentAllUseet != 0 || mileagePaymentAllUseet > 0) {
                        tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(selectPayment - mileagePaymentAllUseet));
                    } else {
                        tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(normalPayment));
                    }

                    paymentStatusType = 0;
                    btnCash.setTextColor(getResources().getColor(R.color.Wite_color));
                    btnCash.setBackgroundResource(R.drawable.a_button_type_2);

                    btnCard.setTextColor(getResources().getColor(R.color.color_43425c));
                    btnCard.setBackgroundResource(R.drawable.a_button_type_3);
                    btnCardCash.setTextColor(getResources().getColor(R.color.color_43425c));
                    btnCardCash.setBackgroundResource(R.drawable.a_button_type_3);

                    rvInstallment.setVisibility(View.GONE);
                    llInstallment.setVisibility(View.GONE);
                    llCashReceipts.setVisibility(View.VISIBLE);
                    llCashPayment.setVisibility(View.VISIBLE);
                    llCardPayment.setVisibility(View.GONE);
                    break;

                case R.id.btn_card:
                    //선충전금 및 마일리지 예외처리 로직
                    if (preFilledGoldet != 0 || preFilledGoldet > 0 && mileagePaymentAllUseet != 0 || mileagePaymentAllUseet > 0) {
                        tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(selectPayment - (mileagePaymentAllUseet + preFilledGoldet)));
                    } else if (preFilledGoldet != 0 || preFilledGoldet > 0) {
                        tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(selectPayment - preFilledGoldet));
                    } else if (mileagePaymentAllUseet != 0 || mileagePaymentAllUseet > 0) {
                        tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(selectPayment - mileagePaymentAllUseet));
                    } else {
                        tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(normalPayment));
                    }

                    paymentStatusType = 1;
                    btnCard.setTextColor(getResources().getColor(R.color.Wite_color));
                    btnCard.setBackgroundResource(R.drawable.a_button_type_2);

                    btnCash.setTextColor(getResources().getColor(R.color.color_43425c));
                    btnCash.setBackgroundResource(R.drawable.a_button_type_3);
                    btnCardCash.setTextColor(getResources().getColor(R.color.color_43425c));
                    btnCardCash.setBackgroundResource(R.drawable.a_button_type_3);

                    llInstallment.setVisibility(View.VISIBLE);
                    llCashReceipts.setVisibility(View.GONE);
                    llCashPayment.setVisibility(View.GONE);
                    llCardPayment.setVisibility(View.VISIBLE);
                    break;

                case R.id.btn_card_cash:    //현금 + 카드 결제
                    paymentStatusType = 2;
                    //현금 + 카드 일경우 예외 결제금액 초기화
                    tvCashPayment.setText("0");
                    tvCardPayment.setText("0");

                    btnCardCash.setTextColor(getResources().getColor(R.color.Wite_color));
                    btnCardCash.setBackgroundResource(R.drawable.a_button_type_2);

                    btnCash.setTextColor(getResources().getColor(R.color.color_43425c));
                    btnCash.setBackgroundResource(R.drawable.a_button_type_3);
                    btnCard.setTextColor(getResources().getColor(R.color.color_43425c));
                    btnCard.setBackgroundResource(R.drawable.a_button_type_3);
                    llInstallment.setVisibility(View.VISIBLE);
                    llCashReceipts.setVisibility(View.VISIBLE);
                    llCashPayment.setVisibility(View.VISIBLE);
                    llCardPayment.setVisibility(View.VISIBLE);
                    break;

                case R.id.btn_payment_agree:
                    llPaymentCount.setVisibility(View.GONE);
                    break;

                case R.id.btn_filled_gold_all_use:      //선충전금 전체사용
                    if (mGetVisitOrderLists.size() != 0) {
                        if ((mTotalPayment - preFilledGold) <= 0) {
                            btnMileagePaymentAllUse.setEnabled(false);
                            tvMileagePaymentEt.setEnabled(false);

                            btnMileagePaymentAllUse.setBackgroundResource(R.color.color_ebebeb);
                            btnMileagePaymentAllUse.setTextColor(getResources().getColor(R.color.color_dddddd));

                            tvMileagePaymentEt.setBackgroundResource(R.color.Wite_color);
                            tvMileagePaymentEt.setTextColor(getResources().getColor(R.color.color_dddddd));
                        }
                        setPreFilledGol();
                    } else {
                        thirdAlertPopup.setTitle("알림");
                        thirdAlertPopup.setContent("선택된 결제 목록이 없어요.\n 결제한 리스트를 선택해주세요.");
                        thirdAlertPopup.show();
                    }

                    break;

                case R.id.btn_mileage_payment_all_use:  //마일리지 전체사용
                    if (mGetVisitOrderLists.size() != 0) {
                        if ((mTotalPayment - mileagePaymentAllUse) <= 0) {
                            btnFilledGoldAllUse.setEnabled(false);
                            tvPreFilledGoldEt.setEnabled(false);
                            btnFilledGoldAllUse.setBackgroundResource(R.color.color_ebebeb);
                            btnFilledGoldAllUse.setTextColor(getResources().getColor(R.color.color_dddddd));

                            tvPreFilledGoldEt.setBackgroundResource(R.color.Wite_color);
                            tvPreFilledGoldEt.setTextColor(getResources().getColor(R.color.color_dddddd));
                        }
                        setMileagePayment();
                    } else {
                        thirdAlertPopup.setTitle("알림");
                        thirdAlertPopup.setContent("선택된 결제 목록이 없어요.\n 결제한 리스트를 선택해주세요.");
                        thirdAlertPopup.show();
                    }
                    break;

                case R.id.btn_make_payment:
                    final String[] status = {null};
                    //주문 품목 결제 API
                    if (mGetVisitOrderLists.size() == 0) {
                        thirdAlertPopup.setTitle("오류 알림");
                        thirdAlertPopup.setContent("결제 품목을 선택해주세요.");
                        thirdAlertPopup.show();
                    } else {
                        if (paymentStatusType == 0) {
                            //현금결제
                            ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityPayment.this);
                            thirdConfirmPopup.setTitle("현금결제 안내");
                            thirdConfirmPopup.setContent("현금결제를 진행하시겠습니까?");
                            thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    setPayment();
                                }
                            });
                            thirdConfirmPopup.show();
                        } else if (paymentStatusType == 1) {
                            //카드결제
                            ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityPayment.this);
                            thirdConfirmPopup.setTitle("카드결제 안내");
                            thirdConfirmPopup.setContent("카드결제글 진행하시겠습니까?");
                            thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    setPaymentCard();
                                }
                            });
                            thirdConfirmPopup.show();
                        } else if (paymentStatusType == 2) {
                            if (normalPayment == 0) {
                                setPaymentCard();
                            } else if (payingCreditCard == 0) {
                                setPayment();
                            } else {
                                setCompoundPayment();
                            }
                        }
                    }
                    break;
                case R.id.btn_later_make_payment:
                    break;
            }
        }
    };

    /**
     * 선충전금 (전체사용) 예외처리로직
     */
    private void setPreFilledGol() {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityPayment.this);

        if (userInfoItemList.get(0).getData().getRealPrePaid() == 0) {
            thirdAlertPopup.setTitle("알림");
            thirdAlertPopup.setContent("선충전금 잔액이 없어요.");
            thirdAlertPopup.show();
        } else {
            try {
                if (userInfoItemList.get(0).getData().getRealPrePaid() < mTotalPayment) {
                    preFilledGoldet = userInfoItemList.get(0).getData().getRealPrePaid();
                    int payMent = mTotalPayment - (userInfoItemList.get(0).getData().getRealPrePaid() + mileagePaymentAllUseet);

                    allSelectPayment = payMent;
                    if (paymentStatusType == 2) {
                        tvCardPayment.setText("0");
                        tvCashPayment.setText("0");
                    } else {
                        tvPreFilledGoldEt.setText(DataFormatUtil.moneyFormatToWon(userInfoItemList.get(0).getData().getRealPrePaid()));
                        tvDiscountedAmount.setText(DataFormatUtil.moneyFormatToWon(userInfoItemList.get(0).getData().getRealPrePaid()));

                        tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(payMent));
                        tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(payMent));
                    }
                } else if (userInfoItemList.get(0).getData().getRealPrePaid() >= mTotalPayment) {
                    preFilledGoldet = mTotalPayment;

                    tvCardPayment.setText("0");
                    tvCashPayment.setText("0");
                    tvPreFilledGoldEt.setText(DataFormatUtil.moneyFormatToWon(mTotalPayment));
                }
                getPaymentAmount();
            } catch (NumberFormatException err) {
                err.printStackTrace();
            }
        }
    }

    /**
     * 마일리지 (전체사용) 예외처리로직
     */
    private void setMileagePayment() {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityPayment.this);
        thirdAlertPopup.setTitle("알림");

        if (mileagePaymentAllUse == 0) {
            thirdAlertPopup.setContent("마일리지 잔액이 없어요.");
            thirdAlertPopup.show();

        } else if (mileagePaymentAllUse > 0) {
            mileagePaymentAllUseet = mileagePaymentAllUse;
            //결제금액 < 마일리지 최소금액
            if (mTotalPayment < ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice()) {
                thirdAlertPopup.setContent("마일리지는" + ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice() + "원이상의 주문부터 \n 마일리지로 결제할 수 있어요.");
                thirdAlertPopup.show();

                //결제 금액 > 마일리지
            } else if (mTotalPayment > ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice()) {

                //마일리지 < 마일리지 최소금액
                if (mileagePaymentAllUse < ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice()) {
                    thirdAlertPopup.setContent("마일리지는" + ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice() + "원부터 사용할 수 있어요.");
                    thirdAlertPopup.show();

                    //마일리지 > 마일리지 최소금액
                } else if (mileagePaymentAllUse > ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice()) {

                    //마일리지 잔액 <= 결제금액
                    if (mileagePaymentAllUse <= mTotalPayment) {
                        mileagePaymentAllUseet = mileagePaymentAllUse;

                        int payMent = mTotalPayment - (mileagePaymentAllUse + preFilledGoldet);

                        allSelectPayment = payMent;

                        tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(payMent));
                        tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(payMent));

                        tvMileagePaymentEt.setText(DataFormatUtil.moneyFormatToWon(mileagePaymentAllUse));

                        // 마일리지 잔액 > 결제금액
                    } else if (mileagePaymentAllUse > mTotalPayment) {

                        mileagePaymentAllUseet = mTotalPayment;

                        tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(0));
                        tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(0));

                        tvMileagePaymentEt.setText(DataFormatUtil.moneyFormatToWon(mTotalPayment));
                    }

                    //마일리지 = 마일리지 최소금액
                } else if (mileagePaymentAllUse == ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice()) {
                    mileagePaymentAllUseet = ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice();
                    int payMent = mTotalPayment - (mileagePaymentAllUse + preFilledGoldet);

                    allSelectPayment = payMent;
                    tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(payMent));
                    tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(payMent));

                    tvMileagePaymentEt.setText(DataFormatUtil.moneyFormatToWon(mileagePaymentAllUse));
                }

            } else if (mTotalPayment == ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice()) {
                //마일리지 < 마일리지 최소 사용금액
                if (mileagePaymentAllUse < ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice()) {
                    thirdAlertPopup.setContent("마일리지는" + ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice() + "원부터 사용할 수 있어요.");
                    thirdAlertPopup.show();
                    //마일리지 >= 마일리지 최소 사용금액
                } else if (mileagePaymentAllUse >= ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getMileageUseMinPrice()) {
                    int payMent = mTotalPayment - (mileagePaymentAllUse + preFilledGoldet);

                    allSelectPayment = payMent;
                    tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(payMent));
                    tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(payMent));

                    tvMileagePaymentEt.setText(DataFormatUtil.moneyFormatToWon(mTotalPayment));
                }
            }
        }
        getPaymentAmount();
    }

    /**
     * 현금 결제 로직 및 예외처리
     */
    private void setPayment() {
        try {
            final String[] HardWareRequestCode = new String[1];
            HardWareRequestCode[0] = "";

            mPaymentWait.show();
            mPaymentWait.setMessage("현금결제 진행중...");
            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityPayment.this);//FirstData 결제 로직

            new Thread(new Runnable() {
                public void run() {
                    try {
                        requestCode = PaymentManager.getCashPaymentStoreOrderItem(ActivityPayment.this, orderItemId, mCheckCount, selectPayment - (preFilledGoldet + mileagePaymentAllUseet), 0, preFilledGoldet, mileagePaymentAllUseet, cashReceipts);

                        if (cashReceipts.equals("Y")) {
                            try {
                                HardWareRequestCode[0] = FirstHardware.getPaymentDataPrint(ActivityPayment.this, selectPayment - (preFilledGoldet + mileagePaymentAllUseet), orderItemId, preFilledGoldet, mileagePaymentAllUseet, cashReceipts);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        public void run() {
                            mPaymentWait.dismiss();
                            if (requestCode.equals("S0000")) {
                                thirdAlertPopup.setTitle("결제완료");
                                thirdAlertPopup.setContent("결제가 완료되었어요.");
                                thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                    @Override
                                    public void onConfirmClick() {
                                        finish();
                                    }
                                });
                                thirdAlertPopup.show();
                            } else if (HardWareRequestCode[0].equals("")) {
                                mPaymentWait.dismiss();
                                thirdAlertPopup.setTitle("수신실패");
                                thirdAlertPopup.setContent("현금영수증 출력 실패했어요.\n" + "다시 시도해주세요. ");
                                thirdAlertPopup.show();
                            } else {
                                mPaymentWait.dismiss();
                                thirdAlertPopup.setTitle("수신실패");
                                thirdAlertPopup.setContent("결제가 실패했어요.\n" + "다시 시도해주세요. ");
                                thirdAlertPopup.show();
                            }
                        }
                    });
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 카드 결제 로직 및 예외처리
     */
    private void setPaymentCard() {
        try {
            mPaymentWait.show();
            mPaymentWait.setMessage("카드 결재 진행중...");
            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityPayment.this);//FirstData 결제 로직

            new Thread(new Runnable() {
                public void run() {
                    String requestCode = null;//FirstData 결제 로직
                    try {
                        requestCode = FirstHardware.getCardPaymentDataPrint(ActivityPayment.this, selectPayment - (preFilledGoldet + mileagePaymentAllUseet), orderItemId, mCheckCount, preFilledGoldet, mileagePaymentAllUseet);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String finalRequestCode = requestCode;
                    runOnUiThread(new Runnable() {
                        public void run() {
                            mPaymentWait.dismiss();
                            if (finalRequestCode.equals("S0000")) {
                                thirdAlertPopup.setTitle("결제완료");
                                thirdAlertPopup.setContent("결제가 완료되었어요.");
                                thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                    @Override
                                    public void onConfirmClick() {
                                        if (paymentStatusType == 2) {
                                            setPayment();
                                        } else {
                                            finish();
                                        }
                                    }
                                });
                                thirdAlertPopup.show();
                            } else {
                                mPaymentWait.dismiss();
                                thirdAlertPopup.setTitle("결제실패");
                                thirdAlertPopup.setContent("카드단말기에서 응답이 없어요.\n" + "다시 시도해주세요.");
                                thirdAlertPopup.show();
                            }
                        }
                    });
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * (현금 + 카드) 결제 연동로직
     */
    private void setCompoundPayment() {
        try {
            final String[] requestCode = {null};//FirstData 결제 로직

            mPaymentWait.show();
            mPaymentWait.setMessage("결재 진행중...");
            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityPayment.this);//FirstData 결제 로직

            new Thread(new Runnable() {
                public void run() {
                    try {
                        requestCode[0] = FirstHardware.getCompoundPayment(ActivityPayment.this, normalPayment - (preFilledGoldet + mileagePaymentAllUseet),
                                payingCreditCard - (preFilledGoldet + mileagePaymentAllUseet), orderItemId, mCheckCount, preFilledGoldet, mileagePaymentAllUseet, cashReceipts);

                        runOnUiThread(new Runnable() {
                            public void run() {
                                mPaymentWait.dismiss();
                                if (requestCode[0].equals("S0000")) {
                                    thirdAlertPopup.setTitle("결제완료");
                                    thirdAlertPopup.setContent("결제가 완료되었어요.");
                                    thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                        @Override
                                        public void onConfirmClick() {
                                            finish();
                                        }
                                    });
                                    thirdAlertPopup.show();
                                } else {
                                    mPaymentWait.dismiss();
                                    thirdAlertPopup.setTitle("결제실패");
                                    thirdAlertPopup.setContent("카드단말기에서 응답이 없어요.\n" + "다시 시도해주세요.");
                                    thirdAlertPopup.show();
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface paymentAmount {
        void itemPaymner(int totalPayment, int checkCount);

        void itemCheckSelect(GetVisitOrderList getVisitOrderLists);

        void itemCheckUnSelect(GetVisitOrderList getVisitOrderLists);

    }

    private paymentAmount paymentAmount = new paymentAmount() {
        @Override
        public void itemPaymner(int totalPayment, int checkCount) {
            if (paymentStatusType != 2) {
                tvCashPayment.setText(DataFormatUtil.moneyFormatToWon(totalPayment));
                tvCardPayment.setText(DataFormatUtil.moneyFormatToWon(totalPayment));
            }

            payingCreditCard = totalPayment;
            normalPayment = totalPayment;
            mTotalPayment = totalPayment;
            selectPayment = totalPayment;
            mCheckCount = checkCount;

            tvItemTotalPayment.setText(DataFormatUtil.moneyFormatToWon(totalPayment) + "원");
            tvLaundryCharge.setText(DataFormatUtil.moneyFormatToWon(totalPayment) + "원");
            tvTotalCount.setText(String.valueOf(mCheckCount) + " 건");

            btnMileagePaymentAllUse.setEnabled(true);
            tvMileagePaymentEt.setEnabled(true);

            btnMileagePaymentAllUse.setBackgroundResource(R.drawable.a_button_type_2);
            btnMileagePaymentAllUse.setTextColor(getResources().getColor(R.color.Wite_color));

            tvMileagePaymentEt.setBackgroundResource(R.drawable.a_button_type_3);
            tvMileagePaymentEt.setTextColor(getResources().getColor(R.color.color_43425c));


            btnFilledGoldAllUse.setEnabled(true);
            tvPreFilledGoldEt.setEnabled(true);
            btnFilledGoldAllUse.setBackgroundResource(R.drawable.a_button_type_2);
            btnFilledGoldAllUse.setTextColor(getResources().getColor(R.color.Wite_color));

            tvPreFilledGoldEt.setBackgroundResource(R.drawable.a_button_type_3);
            tvPreFilledGoldEt.setTextColor(getResources().getColor(R.color.color_43425c));
        }

        @Override
        public void itemCheckSelect(GetVisitOrderList getVisitOrderLists) {
            mGetVisitOrderLists.add(getVisitOrderLists);

            ArrayList<String> name = new ArrayList<>();
            for (GetVisitOrderList getVisitOrderList : mGetVisitOrderLists) {
                name.add(String.valueOf(getVisitOrderList.getOrderItemId()));
            }
            StringBuffer orderItem = new StringBuffer();
            for (int i = 0; i < name.size(); i++) {
                if (i == 0) {
                    orderItem.append(name.get(i));
                } else {
                    orderItem.append("|");
                    orderItem.append(name.get(i));
                }
            }

            orderItemId = orderItem.toString();

            ManagerPaymentVisitOrder.getmInstance().setGetVisitOrder(mGetVisitOrderLists);
        }

        @Override
        public void itemCheckUnSelect(GetVisitOrderList getVisitOrderLists) {
            List<GetVisitOrderList> visitOrderLists = ManagerPaymentVisitOrder.getmInstance().getGetVisitOrder();
            try {
                for (GetVisitOrderList orderList : visitOrderLists) {
                    if (orderList.getStoreItemName().equals(getVisitOrderLists.getStoreItemName())) {
                        visitOrderLists.remove(orderList);
                    }
                }

                if (visitOrderLists.size() == 0) {
                    if (mTotalPayment == 0) {
                        selectReset();
                    }
                }

                ManagerPaymentVisitOrder.getmInstance().setGetVisitOrder(visitOrderLists);
            } catch (Exception err) {
            }
        }
    };

    /**
     * View Data 초기화
     */
    private void selectReset() {
        preFilledGoldet = 0;
        mileagePaymentAllUseet = 0;

        tvPreFilledGoldEt.setText("0");
        tvMileagePaymentEt.setText("0");
        tvSalePayment.setText("0");
        tvPaymentAmountPaid.setText("0");

        btnMileagePaymentAllUse.setEnabled(true);
        tvMileagePaymentEt.setEnabled(true);

        btnMileagePaymentAllUse.setBackgroundResource(R.drawable.a_button_type_2);
        btnMileagePaymentAllUse.setTextColor(getResources().getColor(R.color.Wite_color));

        tvMileagePaymentEt.setBackgroundResource(R.drawable.a_button_type_3);
        tvMileagePaymentEt.setTextColor(getResources().getColor(R.color.color_43425c));


        btnFilledGoldAllUse.setEnabled(true);
        tvPreFilledGoldEt.setEnabled(true);
        btnFilledGoldAllUse.setBackgroundResource(R.drawable.a_button_type_2);
        btnFilledGoldAllUse.setTextColor(getResources().getColor(R.color.Wite_color));

        tvPreFilledGoldEt.setBackgroundResource(R.drawable.a_button_type_3);
        tvPreFilledGoldEt.setTextColor(getResources().getColor(R.color.color_43425c));
    }

    /**
     * 할부 선택 Adapter InterFace
     */
    private AdapterInstallment.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterInstallment.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {

        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {
        }
    };
}
