package rewhite.shopplus.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.client.network.AuthManager;
import rewhite.shopplus.common.activity.BaseActivity;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.common.popup.activity.ActivitySelectRegion;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.data.type.NetworkStatus;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.SharedPreferencesUtil;
import rewhite.shopplus.util.WaitCounter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static rewhite.shopplus.activity.ShopPlusApplication.getCurrentActivity;

/**
 * 상담 신청하기 신청서 작업 VIEW
 */
public class ActivitySingUp extends BaseActivity {
    private static final String TAG = "ActivitySingUp";
    private static final String AREA_CITY_KEY = "area_city_key";
    private WaitCounter mWait;

    private Button btnApply;
    private LinearLayout llClose;
    private ImageView imgAgree;
    private EditText etName, etPhone;
    private RelativeLayout llSite, llLocal;
    private RadioButton rbYes, rbNo;

    private TextView tvSite, tvLocal;
    private boolean imgStatus;
    private String isDelivery = "N";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_up);
        mWait = new WaitCounter(this);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_AREA, PrefConstant.NAME_AREA_DOSI) != null
                || !TextUtils.isEmpty(SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_AREA, PrefConstant.NAME_AREA_DOSI))) {
            tvSite.setText(SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_AREA, PrefConstant.NAME_AREA_DOSI));
        } else {
            tvSite.setText("시/도 선택");
        }
        if (SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_CITY, PrefConstant.NAME_CITY) != null
                || !TextUtils.isEmpty(SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_CITY, PrefConstant.NAME_CITY))) {
            tvLocal.setText(SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_CITY, PrefConstant.NAME_CITY));
        } else {
            tvLocal.setText("시/군/도 선택");
        }

    }

    private void init() {
        checkNetWork();
        setLayout();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * 레이아웃 셋팅 View
     */
    private void setLayout() {
        llClose = (LinearLayout) findViewById(R.id.ll_close);
        etName = (EditText) findViewById(R.id.et_name);
        etPhone = (EditText) findViewById(R.id.et_phone);
        //핸드폰 번호 입력 변환
        etPhone.setInputType(android.text.InputType.TYPE_CLASS_PHONE);
        etPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        imgAgree = (ImageView) findViewById(R.id.img_agree);

        rbYes = (RadioButton) findViewById(R.id.rb_yes);
        rbNo = (RadioButton) findViewById(R.id.rb_no);

        btnApply = (Button) findViewById(R.id.btn_apply);

        llSite = (RelativeLayout) findViewById(R.id.ll_site);
        llLocal = (RelativeLayout) findViewById(R.id.ll_local);

        tvSite = (TextView) findViewById(R.id.tv_site);
        tvLocal = (TextView) findViewById(R.id.tv_local);
        setClickView();
    }

    /**
     * Click Listener Setting
     */
    private void setClickView() {
        llSite.setOnClickListener(clickListener);
        llLocal.setOnClickListener(clickListener);

        llClose.setOnClickListener(clickListener);
        imgAgree.setOnClickListener(clickListener);
        btnApply.setOnClickListener(clickListener);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            Intent intent = null;

            switch (id) {
                case R.id.ll_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivitySingUp.this);
                    thirdConfirmPopup.setTitle("종료 팝업");
                    thirdConfirmPopup.setContent("샵플러스 상담신청을 종료할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setCancelButtonText("아니요");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            finish();
                        }
                    });
                    thirdConfirmPopup.show();

                    break;
                case R.id.ll_site:
                    intent = new Intent(getApplicationContext(), ActivitySelectRegion.class);
                    intent.putExtra(AREA_CITY_KEY, 1);
                    startActivity(intent);
                    break;

                case R.id.ll_local:
                    intent = new Intent(getApplicationContext(), ActivitySelectRegion.class);
                    intent.putExtra(AREA_CITY_KEY, 2);
                    startActivity(intent);
                    break;

                case R.id.img_agree:
                    imgStatus = !imgStatus;
                    setAgreeStatus(imgStatus, imgAgree);
                    break;

                case R.id.btn_apply:
                    setApply();
                    break;
            }
        }
    };

    /**
     * Check Status 변경 처리
     *
     * @param status
     * @param img
     */
    private void setAgreeStatus(boolean status, ImageView img) {
        if (status) {
            isDelivery = "Y";
            img.setBackgroundResource(R.drawable.ico_login_checkon_02);
        } else {
            isDelivery = "N";
            img.setBackgroundResource(R.drawable.ico_login_checkoff_01);
        }
    }

    /**
     * 시청하기
     */
    private void setApply() {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(this);
        thirdAlertPopup.setTitle("알림");
        if (TextUtils.isEmpty(etName.getText()) || etName.getText() == null) {
            thirdAlertPopup.setContent("세탁소명을 입력하세요.");
        } else if (TextUtils.isEmpty(etPhone.getText()) || etPhone.getText() == null) {
            thirdAlertPopup.setContent("휴대폰번호를 입력하세요.");
        } else if (!rbYes.isChecked() && !rbNo.isChecked()) {
            thirdAlertPopup.setContent("수거배송 여부를 선택해주세요.");
        } else if (isDelivery.equals("N")) {
            thirdAlertPopup.setContent("개인정보 수집 및 이용 동의 약관을 동의해주세요.");
        } else {

            try {
                mWait.show();
                new Thread(new Runnable() {
                    public void run() {
                        AuthManager.getRequestJoinStore(ActivitySingUp.this, etName.getText().toString(), etPhone.getText().toString(),
                                SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_AREA, PrefConstant.NAME_AREA_DOSI),
                                SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_CITY, PrefConstant.NAME_CITY), isDelivery);
                        runOnUiThread(new Runnable() {
                            public void run() {
                                mWait.dismiss();
                                thirdAlertPopup.setContent("샵플러스 상담신청이 완료되었어요.\n 리화이트에서 연락드릴게요.");
                                thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                    @Override
                                    public void onConfirmClick() {
                                        SharedPreferencesUtil.removeSharedPreference(getApplicationContext(), PrefConstant.KEY_AREA, PrefConstant.NAME_AREA_DOSI);
                                        SharedPreferencesUtil.removeSharedPreference(getApplicationContext(), PrefConstant.KEY_CITY, PrefConstant.NAME_CITY);
                                        finish();
                                    }
                                });
                            }
                        });
                    }
                }).start();
            } catch (Exception err) {
                err.printStackTrace();
            }
        }
        thirdAlertPopup.show();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    /**
     * 네트워크 상태 CheckLogic
     */
    private void checkNetWork() {
        NetworkStatus networkStatus = ShopPlusApplication.getCurrnetNetworkStatus();
        if (!networkStatus.equals(NetworkStatus.WIFI)) {
            CommonUtil.netWorkErrPoup(this);
        }
        getCurrentActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //화면 종료시 데이터 삭제
        SharedPreferencesUtil.removeSharedPreference(getApplicationContext(), PrefConstant.KEY_AREA, PrefConstant.NAME_AREA_DOSI);
        SharedPreferencesUtil.removeSharedPreference(getApplicationContext(), PrefConstant.KEY_CITY, PrefConstant.NAME_CITY);
    }
}