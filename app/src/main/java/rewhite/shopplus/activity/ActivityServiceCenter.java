package rewhite.shopplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.common.popup.activity.ActivityRemoteConsultation;
import rewhite.shopplus.fragment.main.FragmentAskedQuestions;
import rewhite.shopplus.fragment.main.FragmentNotice;
import rewhite.shopplus.util.WaitCounter;

/**
 * 고객센터
 */
public class ActivityServiceCenter extends AppCompatActivity {
    private static final String TAG = "ActivityServiceCenter";

    private WaitCounter mWait;

    private boolean mIsServiceCenterStatus = true;

    private ImageView ivFrequentlyAskedIcon;
    private TextView tvFrequentlyAskedAuestions;
    private TextView tvSubFrequentlyAsked;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_center);
        mWait = new WaitCounter(this);
        setLayout();
//        init();
    }

//    private void init() {
//        try {
//            mWait.show();
//            new Thread(new Runnable() {
//                public void run() {
//
//                    runOnUiThread(new Runnable() {
//                        public void run() {
//
//                            mWait.dismiss();
//                        }
//                    });
//                }
//            }).start();
//        } catch (Exception err) {
//            err.printStackTrace();
//        }
//    }

    /**
     * 레이아웃 SettingView
     */
    private void setLayout() {
        LinearLayout llNoticeChange = (LinearLayout) findViewById(R.id.ll_notice_change);
        LinearLayout llCustomerServiceLink = (LinearLayout) findViewById(R.id.ll_customer_service_link);

        ImageView imgClose = (ImageView) findViewById(R.id.img_close);
        ivFrequentlyAskedIcon = (ImageView) findViewById(R.id.iv_frequently_asked_icon);
        tvFrequentlyAskedAuestions = (TextView) findViewById(R.id.tv_frequently_asked_auestions);
        tvSubFrequentlyAsked = (TextView) findViewById(R.id.tv_sub_frequently_asked);

        imgClose.setOnClickListener(btnClickListener);
        llCustomerServiceLink.setOnClickListener(btnClickListener);
        llNoticeChange.setOnClickListener(btnClickListener);

//        mIsServiceCenterStatus = SettingLogic.getServiceCenterChange(getApplicationContext());
        setImageStatus(ivFrequentlyAskedIcon, tvFrequentlyAskedAuestions, tvSubFrequentlyAsked, mIsServiceCenterStatus);
    }

    /**
     * 고객센터 View
     *
     * @param ivImg
     * @param tvFrequentlyAskedAuestions
     * @param tvSubFrequentlyAsked
     * @param status
     */
    private void setImageStatus(ImageView ivImg, TextView tvFrequentlyAskedAuestions, TextView tvSubFrequentlyAsked, boolean status) {
        if (status) {
            ivImg.setBackgroundResource(R.drawable.ico_customerservice_speechbubble_03); //공지사항
            tvFrequentlyAskedAuestions.setText("자주 묻는 질문");
            tvSubFrequentlyAsked.setText("자주 묻는 질문을 확인해보세요.");

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            FragmentNotice fragmentNotice = new FragmentNotice();
            fragmentTransaction.replace(R.id.fragment_service_center, fragmentNotice);
            fragmentTransaction.commit();

        } else {
            ivImg.setBackgroundResource(R.drawable.ico_customerservice_loudspeaker_04); //자주하는 질문
            tvFrequentlyAskedAuestions.setText(getResources().getString(R.string.service_center_notice));
            tvSubFrequentlyAsked.setText("샵플러스 다양한 소식을\n 공지사항에서 확인해보세요.");

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            FragmentAskedQuestions fragmentAskedQuestions = new FragmentAskedQuestions();
            fragmentTransaction.replace(R.id.fragment_service_center, fragmentAskedQuestions);
            fragmentTransaction.commit();
        }
    }

    private View.OnClickListener btnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.ll_notice_change:
                    mIsServiceCenterStatus = !mIsServiceCenterStatus;

                    setImageStatus(ivFrequentlyAskedIcon, tvFrequentlyAskedAuestions, tvSubFrequentlyAsked, mIsServiceCenterStatus);
//                    SettingLogic.setServiceCenterChange(getApplicationContext(), mIsServiceCenterStatus);
                    break;

                case R.id.ll_customer_service_link:
                    Intent intent = new Intent(getApplicationContext(), ActivityRemoteConsultation.class);
                    startActivity(intent);
                    break;

                case R.id.img_close:
                    finish();
                    break;
            }
        }
    };

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
