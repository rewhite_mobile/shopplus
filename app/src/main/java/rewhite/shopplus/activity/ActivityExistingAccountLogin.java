package rewhite.shopplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.client.network.AuthManager;
import rewhite.shopplus.client.network.CustomerManager;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.logic.SettingLogic;

/**
 * 기존 계정 로그인 View
 */
public class ActivityExistingAccountLogin extends AppCompatActivity {
    private static final String TAG = "ActivityExistingAccountLogin";
    private Button btnExistingAccount, btnNewAccount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existing_account_login);
        setLayout();
    }


    private void setLayout() {
        TextView login = (TextView) findViewById(R.id.login);
        btnExistingAccount = (Button) findViewById(R.id.btn_existing_account);
        btnNewAccount = (Button) findViewById(R.id.btn_new_account);

        btnNewAccount.setOnClickListener(btnOnClickListener);
        btnExistingAccount.setOnClickListener(btnOnClickListener);
    }


    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            final Intent[] intent = {null};
            switch (id) {
                case R.id.btn_existing_account:
                    if (SettingLogic.getPreAccessToken(getApplicationContext()) != null || !TextUtils.isEmpty(SettingLogic.getPreAccessToken(getApplicationContext()))) {
                        AuthManager.CheckAccessToken(ActivityExistingAccountLogin.this, SettingLogic.getPreAccessToken(getApplicationContext()));

                        CustomerManager.getFaqCateforyListe(ActivityExistingAccountLogin.this);
                        //환경설정 데이터 init
                        EnviromentManager.getStoreDeviceInfo(ActivityExistingAccountLogin.this);
                        //환경설정 매장관리 정보 데이터 init
                        EnviromentManager.getManagerStoreManageInfo(ActivityExistingAccountLogin.this);
                        //가맹점 정보
                        EnviromentManager.getStoreContractInfo(ActivityExistingAccountLogin.this);

                        intent[0] = new Intent(getApplicationContext(), ActivityMain.class);
                        intent[0].setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent[0].addCategory(Intent.CATEGORY_HOME);
                        startActivity(intent[0]);
                    } else {
                        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityExistingAccountLogin.this);
                        thirdAlertPopup.setTitle("알림");
                        thirdAlertPopup.setContent("오류로 인해 로그아웃 처리 되었어요. \n 다시 한번 로그인 해주세요.");
                        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                            @Override
                            public void onConfirmClick() {
                                intent[0] = new Intent(getApplicationContext(), ActivityLogin.class);
                                intent[0].setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent[0].addCategory(Intent.CATEGORY_HOME);
                                startActivity(intent[0]);
                            }
                        });
                        thirdAlertPopup.show();
                    }
                    break;

                case R.id.btn_new_account:
                    intent[0] = new Intent(getApplicationContext(), ActivityLogin.class);
                    intent[0].setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent[0].addCategory(Intent.CATEGORY_HOME);
                    startActivity(intent[0]);
                    break;
            }
        }
    };
}