package rewhite.shopplus.activity;

import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatActivity;
import android.webkit.CookieSyncManager;

import rewhite.shopplus.R;
import rewhite.shopplus.client.network.NetworkChangeReceiver;
import rewhite.shopplus.data.type.ApplicationType;
import rewhite.shopplus.data.type.NetworkStatus;
import rewhite.shopplus.logic.DeviceUuidFactoryLogic;
import rewhite.shopplus.logic.UpdateCheckLogic;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.NetworkUtils;

/**
 * 샵 플러스 어플리케이션 클래스
 */
public class ShopPlusApplication extends MultiDexApplication {
    private static final String TAG = "WebhardApplication";
    private static ApplicationType mAppType;
    private static String mAppver;

    private NetworkChangeReceiver mNetworkReceiver;
    private static NetworkStatus mCurrentNetworkStatus;

    private static volatile AppCompatActivity currentActivity = null;

    private static String mSecretKey;
    private static int REGSTEREDVERSION;

    @Override
    public void onCreate() {
        super.onCreate();
        //로그 활성화 세팅
        Logger.setAllLogEnable(getApplicationContext(), getResources().getBoolean(R.bool.log_enable));
        //deviceInformation
        new DeviceUuidFactoryLogic(getApplicationContext());

        if (Build.VERSION.SDK_INT < 21) {
            CookieSyncManager.createInstance(this);
        }

        init();
        initAsyncTask();

        //네트워크 실시간으로 체크해 상태 값 확인 하기 위한 로직
        if (currentActivity != null) {
            mCurrentNetworkStatus = NetworkUtils.getNetworkStatus(currentActivity);
        }

        mNetworkReceiver = new NetworkChangeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mNetworkReceiver, filter);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Logger.d(TAG, "onTerminate()");
        if (mNetworkReceiver != null)
            unregisterReceiver(mNetworkReceiver);
    }

    private void init() {
        //데이터베이스 설정
//        initDatabase();
//        updateGcmStatus();
    }

    private void initAsyncTask() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            try {
                Class.forName("android.os.AsyncTask");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static AppCompatActivity getCurrentActivity() {
        return currentActivity;
    }

    public static void setCurrentActivity(AppCompatActivity currentActivity) {
        ShopPlusApplication.currentActivity = currentActivity;
    }


    /**
     * SHOP+ 버전코드 이전 유저를 대상으로 신규 앱 설치 유저 및 업데이트 필요한 유저 관리해 강제 업데이트 및 자동 업데이트 처리.
     */
    private void updateGcmStatus() {
        Logger.d(TAG, "updateGcmStatus");
        String store_version = UpdateCheckLogic.getMarketVersion(getPackageName());

        try {
            String device_version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

            if (store_version.compareTo(device_version) > 0) {
                // 업데이트 필요
                UpdateCheckLogic.getMarketVersionFast("rewhite.shopplus");
            } else {
                // 업데이트 불필요

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static ApplicationType getAppType() {
        return mAppType;
    }

    public static String getAppVer() {
        return mAppver;
    }

    public static void setSecretKey(String key) {
        mSecretKey = key;
    }

    public static String getSecretKey() {
        return mSecretKey;
    }

    public static NetworkStatus getCurrnetNetworkStatus() {
        return mCurrentNetworkStatus;
    }

    public static void setCurrentNetworkStatus(NetworkStatus status) {
        mCurrentNetworkStatus = status;
    }

    /**
     * Database 사용할경우 처리 하기위해 셋팅만 해놓은 상태
     */
//    private void initDatabase() {
//        DBConnector conn = DBConnector.getInstance();
//        if (!conn.isOpen()) {
//            DBOpenHelper dbHelper = new DBOpenHelper(getApplicationContext(), DBConnector.DatabaseName, DBConnector.DatabaseVersion);
//            try {
//                conn.setDatabase(dbHelper.openDatabase());
//            } catch (Exception e) {
//                Logger.e(TAG, "initDatabase failed.", e);
//                ToastUtil.showMakeText(getApplicationContext(), R.string.db_init_err, Toast.LENGTH_LONG);
//            }
//        }
//    }
}
