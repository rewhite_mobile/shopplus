package rewhite.shopplus.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.firebase.iid.FirebaseInstanceId;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerAuthStoreLogin;
import rewhite.shopplus.client.model.StoreLoginModel;
import rewhite.shopplus.client.network.AuthManager;
import rewhite.shopplus.client.network.CustomerManager;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.common.activity.BaseActivity;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.common.popup.activity.ActivityIdPwChange;
import rewhite.shopplus.data.type.NetworkStatus;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.util.hardware.FirstHardware;
import rewhite.shopplus.view.adapter.ViewPagerAdapter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static rewhite.shopplus.activity.ShopPlusApplication.getCurrentActivity;

/**
 * 로그인 화면 구성 view
 */
public class ActivityLogin extends BaseActivity {
    private static final String TAG = "ActivityLogin";
    private static final String USER_LOGIN = "USER_LOGIN"; //사용자 로그인 KEY
    private static final String USER_ID = "USER_ID"; //사용자 아이디
    private static final String USER_PASSWORD = "USER_PASSWORD"; //사용자 패스워드

    private CheckBox chAutoLogin;
    private WaitCounter mWait;

    private LinearLayout viewConsultation, llAutoLogin;

    private EditText tvId, tvPassword;

    private Button btnLogin, btnIdPwChange;
    private ViewPager myPager = null;
    private Typeface typeFace;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        checkNetWork();

        FirstHardware.init(getActivity());

        setWaitCount();
        setLayout();
        setViewPagerAdapter();
    }

    /**
     * Progress Loding Setting
     */
    private void setWaitCount() {
        mWait = new WaitCounter(this);
    }

    /**
     * ViewPager Timer for Auto Sliding
     */
    private void setViewPagerAdapter() {
        final int noofsize = 3;
        final int[] count = {0};

        ViewPagerAdapter adapter = new ViewPagerAdapter(this, noofsize);
        myPager = (ViewPager) findViewById(R.id.viewPager);
        myPager.setAdapter(adapter);
        myPager.setCurrentItem(0);

        // Timer for auto sliding
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (count[0] <= noofsize) {
                            myPager.setCurrentItem(count[0]);
                            count[0]++;
                        } else {
                            count[0] = 0;
                            myPager.setCurrentItem(count[0]);
                        }
                    }
                });
            }
        }, 500, 3000);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    /**
     * LayoutSetting View
     */
    private void setLayout() {
        mWait.show();
        viewConsultation = (LinearLayout) findViewById(R.id.view_consultation);
        llAutoLogin = (LinearLayout) findViewById(R.id.ll_auto_login);
        chAutoLogin = (CheckBox) findViewById(R.id.ch_auto_login);

        tvId = (EditText) findViewById(R.id.tv_id);
        tvPassword = (EditText) findViewById(R.id.tv_password);

        btnLogin = (Button) findViewById(R.id.btn_login);
        btnIdPwChange = (Button) findViewById(R.id.btn_id_pw_change);
        chAutoLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Logger.d(TAG, "true");
                    SettingLogic.setAutoLogin(getApplicationContext(), true);
                } else {
                    Logger.d(TAG, "false");
                    SettingLogic.setAutoLogin(getApplicationContext(), false);
                }
            }
        });

        tvId.setOnClickListener(clickListener);
        tvPassword.setOnClickListener(clickListener);
        viewConsultation.setOnClickListener(clickListener);
        llAutoLogin.setOnClickListener(clickListener);
        btnLogin.setOnClickListener(clickListener);
        btnIdPwChange.setOnClickListener(clickListener);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            Intent intent = null;
            switch (id) {
                case R.id.view_consultation:
                    intent = new Intent(getApplicationContext(), ActivitySingUp.class);
                    startActivity(intent);
                    break;

                case R.id.btn_login:
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityLogin.this);

                    if (tvId.getText().toString().equals("") || tvId.getText().toString() == null || TextUtils.isEmpty(tvId.getText().toString())) {
                        thirdAlertPopup.setTitle("계정 입력 오류");
                        thirdAlertPopup.setContent("아이디, 비밀번호를\n" + "모두 입력하세요.");
                        thirdAlertPopup.show();
                    } else if (tvPassword.getText().toString().equals("") || tvPassword.getText().toString() == null || TextUtils.isEmpty(tvPassword.getText().toString())) {
                        thirdAlertPopup.setTitle("계정 입력 오류");
                        thirdAlertPopup.setContent("아이디, 비밀번호를\n" + "모두 입력하세요.");
                        thirdAlertPopup.show();
                    } else {
                        uplusInitialize();
                    }
                    break;
                case R.id.btn_id_pw_change:
                    intent = new Intent(getApplicationContext(), ActivityIdPwChange.class);
                    startActivity(intent);
            }
        }
    };

    /**
     * 로그인 Initialize 처리 로직
     */
    private void uplusInitialize() {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityLogin.this);
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    AuthManager.getStoreLogin(ActivityLogin.this, tvId.getText().toString(), tvPassword.getText().toString(), FirebaseInstanceId.getInstance().getToken());
                    runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                            List<StoreLoginModel> storeLoginModel = ManagerAuthStoreLogin.getmInstance().getmanagerAuthStoreLogin();
//                            try {
                            if (storeLoginModel.get(0).getResultCode().contains("F")) {
                                CommonUtil.loginErrPopup(ActivityLogin.this, storeLoginModel.get(0).getResultCode());
                            } else if (storeLoginModel.get(0).getResultCode().equals("S0000")) {
                                SettingLogic.setGCMAccessToken(ActivityLogin.this, FirebaseInstanceId.getInstance().getToken());

                                SettingLogic.setPreAccessToken(getApplicationContext(), storeLoginModel.get(0).getData().getAccessToken());

                                CustomerManager.getFaqCateforyListe(ActivityLogin.this);
                                //환경설정 데이터 init
                                EnviromentManager.getStoreDeviceInfo(ActivityLogin.this);
                                //환경설정 매장관리 정보 데이터 init
                                EnviromentManager.getManagerStoreManageInfo(ActivityLogin.this);
                                //가맹점 정보
                                EnviromentManager.getStoreContractInfo(ActivityLogin.this);

                                Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                startActivity(intent);
                            } else if (storeLoginModel.get(0).getResultCode().equals("S1001")) {

                            }
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private void setPasswordGuide() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        Calendar cal = Calendar.getInstance();

        ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityLogin.this);
        thirdConfirmPopup.setTitle("계정 입력 오류");
        thirdConfirmPopup.setContent("비밀번호 변경 후 6개월이 경과되\n" + "었어요. 지금 변경하시겠어요?");
        thirdConfirmPopup.setButtonText("예");
        thirdConfirmPopup.setCancelButtonText("한달간보지않기");
        thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
            @Override
            public void onConfirmClick() {

            }
        });

        thirdConfirmPopup.setOnCancelClickListener(new ThirdConfirmPopup.OnCancelClickListener() {
            @Override
            public void onCancelClick() {
                SettingLogic.setPasswordChangeGuide(getApplicationContext(), sdf.format(cal.getTime()));
            }
        });
    }

    /**
     * 네트워크 상태 CheckLogic
     */
    private void checkNetWork() {
        NetworkStatus networkStatus = ShopPlusApplication.getCurrnetNetworkStatus();
        if (!networkStatus.equals(NetworkStatus.WIFI)) {
            CommonUtil.netWorkErrPoup(this);
        }
        getCurrentActivity();
    }

    @Override
    protected void onResume() {
        mWait.dismiss();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}