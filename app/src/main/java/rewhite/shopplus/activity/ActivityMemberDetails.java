package rewhite.shopplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.model.StoreUserInfoItemList;
import rewhite.shopplus.client.network.PaymentManager;
import rewhite.shopplus.client.network.StoreUserApiManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.common.popup.activity.ActivityCancelPayment;
import rewhite.shopplus.common.popup.activity.ActivityItemCancellation;
import rewhite.shopplus.common.popup.activity.ActivityMemberHistory;
import rewhite.shopplus.common.popup.activity.ActivityPreFillGold;
import rewhite.shopplus.data.dto.ItemGetVisitOrderList;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.CustomViewPager;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.util.hardware.FirstHardware;
import rewhite.shopplus.view.adapter.AdapterMemberDetailsViewPager;

/**
 * 회원상세 VIEW
 */
public class ActivityMemberDetails extends AppCompatActivity {
    private static final String TAG = "ActivityMemberDetails";
    private static final String USER_STORE_ID = "user_store_id";
    private static final String paymentType = "1";  // 결제하기 Type;
    private static final String MEMBER_DETAIL = "member_detail";  // 회원정보수정 Type;
    private static int userStoreId;

    private RelativeLayout rlVisitReception;
    private LinearLayout llMemmberPrint;
    private List<StoreUserInfoItemList> storeUserInfoItemList;

    private TextView tvEditMembershipInformation, tvMembershipCard, tvViewCoupons;
    private static TextView tvNonPayPrice;
    private TextView tvRealPrePaid;
    private TextView tvMileage;
    private TextView tvVisitReception;
    private TextView tvUserName, tvUserPhoneNumber, tvUserAddress;
    private TextView tvUnreleasedCount, tvLaundryCompleteCount, tvAttemptedDeliveryCount, tvAttemptedReleaseCount, tvTotalCount;
    private Button btnCharging, btnHistory;

    private CustomViewPager viewPager;
    private ImageView imgClose;

    private static TextView tvPayment;
    private TextView tvNonpayPrice;
    private static TextView tvNonPayPrice1;
    private static TextView tvTotalPayment;
    private TextView tvMemoData;

    private static TextView tvSelectCount;

    private WaitCounter mWait;
    private Button btnNextPage, btnItemCancle, btnPaymentCancle, btnReceiving, btnItemRelease, btnMakePayment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_details);

        Intent intent = getIntent();
        userStoreId = intent.getExtras().getInt(USER_STORE_ID);
        FirstHardware.init(this);
        mWait = new WaitCounter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    /**
     * init 회원정보 바인딩
     */
    private void init() {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    StoreUserApiManager.getStoreUserInfo(ActivityMemberDetails.this, Long.valueOf(userStoreId));
                    ActivityMemberDetails.this.runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                            storeUserInfoItemList = ManagerStoreUserInfo.getmInstance().getStoreUserInfo();
                            setLayout();
                            setTextView();
                            setViewPagerAdapter();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * Layout Setting
     */
    private void setLayout() {
        //TODO 2차오픈 추가 예정.
//        rlLaundryPickupDelivery = (RelativeLayout) findViewById(R.id.rl_laundry_pickup_delivery);
//        rlRewhitePickupDelivery = (RelativeLayout) findViewById(R.id.rl_rewhite_pickup_delivery);
//        tvLaundryPickupDelivery = (TextView) findViewById(R.id.tv_laundry_pickup_delivery);
//        tvRewhitePickupDelivery = (TextView) findViewById(R.id.tv_rewhite_pickup_delicery);

        tvSelectCount = (TextView) findViewById(R.id.tv_select_count);

        tvNonPayPrice1 = (TextView) findViewById(R.id.tv_nonpay_price);
        tvMemoData = (TextView) findViewById(R.id.tv_memo_data);
        tvUnreleasedCount = (TextView) findViewById(R.id.tv_unreleased_count);
        tvLaundryCompleteCount = (TextView) findViewById(R.id.tv_laundry_complete_count);
        tvAttemptedDeliveryCount = (TextView) findViewById(R.id.tv_attempted_delivery_count);
        tvAttemptedReleaseCount = (TextView) findViewById(R.id.tv_attempted_release_count);
        tvTotalCount = (TextView) findViewById(R.id.tv_total_count);

        tvEditMembershipInformation = (TextView) findViewById(R.id.tv_edit_membership_information);
        tvMembershipCard = (TextView) findViewById(R.id.tv_membership_card);
        tvViewCoupons = (TextView) findViewById(R.id.tv_view_coupons);

        rlVisitReception = (RelativeLayout) findViewById(R.id.rl_visit_reception);
        tvVisitReception = (TextView) findViewById(R.id.tv_visit_reception);
        llMemmberPrint = (LinearLayout) findViewById(R.id.ll_memmber_print);

        btnReceiving = (Button) findViewById(R.id.btn_receiving);
        btnPaymentCancle = (Button) findViewById(R.id.btn_payment_cancle);
        btnHistory = (Button) findViewById(R.id.btn_history);
        btnItemRelease = (Button) findViewById(R.id.btn_item_release);
        btnMakePayment = (Button) findViewById(R.id.btn_member_make_payment);
        btnItemCancle = (Button) findViewById(R.id.btn_item_cancle);
        btnNextPage = (Button) findViewById(R.id.btn_next);
        btnCharging = (Button) findViewById(R.id.btn_charging);

        tvNonPayPrice = (TextView) findViewById(R.id.tv_non_pay_price);
        tvRealPrePaid = (TextView) findViewById(R.id.tv_real_pre_paid);
        tvMileage = (TextView) findViewById(R.id.tv_mileage);
        tvTotalPayment = (TextView) findViewById(R.id.tv_total_payment);
        tvPayment = (TextView) findViewById(R.id.tv_payment);
        tvUserName = (TextView) findViewById(R.id.tv_user_name);
        tvUserPhoneNumber = (TextView) findViewById(R.id.tv_user_phoneNumber);
        tvUserAddress = (TextView) findViewById(R.id.tv_user_address);

        imgClose = (ImageView) findViewById(R.id.img_close);
        viewPager = (CustomViewPager) findViewById(R.id.view_pager);

        setClickListener();
    }

    /**
     * Set ClickListener
     */
    private void setClickListener() {
        tvEditMembershipInformation.setOnClickListener(btnClickListener);
        tvMembershipCard.setOnClickListener(btnClickListener);
        tvViewCoupons.setOnClickListener(btnClickListener);

        llMemmberPrint.setOnClickListener(btnClickListener);
        btnMakePayment.setOnClickListener(btnClickListener);
        btnCharging.setOnClickListener(btnClickListener);
        btnItemCancle.setOnClickListener(btnClickListener);
        btnPaymentCancle.setOnClickListener(btnClickListener);

        btnItemRelease.setOnClickListener(btnClickListener);
        btnNextPage.setOnClickListener(btnClickListener);
        imgClose.setOnClickListener(btnClickListener);
        btnHistory.setOnClickListener(btnClickListener);
        btnReceiving.setOnClickListener(btnClickListener);
    }

    /**
     * Text Setting
     */
    private void setTextView() {
        try {
            tvUserName.setText(storeUserInfoItemList.get(0).getData().getUserName());
            tvUserPhoneNumber.setText(storeUserInfoItemList.get(0).getData().get_UserPhone());
            tvUserAddress.setText(storeUserInfoItemList.get(0).getData().get_UserAddress());
            tvMemoData.setText(storeUserInfoItemList.get(0).getData().getMemo());
            tvTotalCount.setText(String.valueOf(storeUserInfoItemList.get(0).getData().getTotalOrderCount()) + "건");
            tvUnreleasedCount.setText(String.valueOf(storeUserInfoItemList.get(0).getData().getNotOutOrderCount()) + "건");             // 미출고
            tvLaundryCompleteCount.setText(String.valueOf(storeUserInfoItemList.get(0).getData().getWashFinOrderCount()) + "건");       // 세탁완료
            tvAttemptedDeliveryCount.setText(String.valueOf(storeUserInfoItemList.get(0).getData().getOutNonPayPrice()) + "건");        // 출고미수금
            tvAttemptedReleaseCount.setText(String.valueOf(storeUserInfoItemList.get(0).getData().getNotOutNonPayOrderCount()) + "건"); // 미출고 미수 주문수
            tvNonPayPrice.setText(DataFormatUtil.moneyFormatToWon(storeUserInfoItemList.get(0).getData().getNonPayPrice()));            //미수금
            tvRealPrePaid.setText(DataFormatUtil.moneyFormatToWon(storeUserInfoItemList.get(0).getData().getTotalPrePaid()));           //충전
            tvMileage.setText(DataFormatUtil.moneyFormatToWon(storeUserInfoItemList.get(0).getData().getMileage()));                    //마일리지
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private View.OnClickListener btnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            Intent intent = null;
            switch (id) {
                case R.id.btn_next:
                    intent = new Intent(ActivityMemberDetails.this, ActivityItemRegistration.class);          //품목등록 DialogViewClass
                    intent.putExtra(USER_STORE_ID, userStoreId);
                    startActivity(intent);
                    break;

                case R.id.btn_charging:
                    intent = new Intent(ActivityMemberDetails.this, ActivityPreFillGold.class);        //선충전금 결제 DialogViewClass
                    intent.putExtra(USER_STORE_ID, userStoreId);
                    startActivity(intent);
                    break;

                case R.id.btn_member_make_payment:
                    intent = new Intent(ActivityMemberDetails.this, ActivityPayment.class);            //결제하기(결제)공통 ActivityClass
                    intent.putExtra(paymentType, 0);
                    startActivity(intent);
                    break;

                case R.id.btn_history:
                    intent = new Intent(ActivityMemberDetails.this, ActivityMemberHistory.class);      //회원 내역조회 (마일리지/선충전금) dialog
                    intent.putExtra(USER_STORE_ID, userStoreId);
                    startActivity(intent);
                    break;

                case R.id.btn_item_cancle:
                    intent = new Intent(ActivityMemberDetails.this, ActivityItemCancellation.class);   //품목취소 DialogViewClass
                    intent.putExtra(USER_STORE_ID, userStoreId);
                    startActivity(intent);
                    break;

                case R.id.btn_payment_cancle:
                    intent = new Intent(ActivityMemberDetails.this, ActivityCancelPayment.class);      //결제취소 DialogViewClass
                    intent.putExtra(USER_STORE_ID, userStoreId);
                    startActivity(intent);
                    break;

                case R.id.img_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityMemberDetails.this);
                    thirdConfirmPopup.setTitle("회원상세정보 팝업종료");
                    thirdConfirmPopup.setContent("회원상세정보 화면을 종료 하시겠습니까?");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            finish();
                        }
                    });
                    thirdConfirmPopup.show();
                    break;

                case R.id.btn_receiving:
                    if (storeUserInfoItemList.get(0).getData().getNonPayPrice() == 0) {
                        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityMemberDetails.this);

                        thirdAlertPopup.setTitle("알림");
                        thirdAlertPopup.setContent("현재 미수금이 존재하지않습니다.");
                        thirdAlertPopup.setButtonText("확인");
                        thirdAlertPopup.show();
                    } else {
                        intent = new Intent(ActivityMemberDetails.this, ActivityPayment.class);            //결제하기(결제)공통 ActivityClass
                        intent.putExtra(paymentType, 3);
                        startActivity(intent);
                    }
                    break;

                case R.id.btn_item_release:
                    List<GetVisitOrderList> getVisitOrder = ItemGetVisitOrderList.getmInstance().getGetVisitOrder();
                    if (getVisitOrder.size() == 0) {
                        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityMemberDetails.this);
                        thirdAlertPopup.setTitle("오류팝업");
                        thirdAlertPopup.setContent("출고 가능한 품목이 없어요.\n" + "품목을 다시 선택해주세요. ");
                        thirdAlertPopup.show();
                    } else {
                        StringBuffer cancleData = new StringBuffer();

                        for (int i = 0; i < getVisitOrder.size(); i++) {
                            if (i == 0) {
                                cancleData.append(getVisitOrder.get(i).getOrderItemId());
                            } else {
                                cancleData.append("|");
                                cancleData.append(getVisitOrder.get(i).getOrderItemId());
                            }
                        }
                        try {
                            mWait.show();
                            new Thread(new Runnable() {
                                public void run() {
                                    PaymentManager.getOutOrderItem(ActivityMemberDetails.this, cancleData.toString(), getVisitOrder.size());
                                    ActivityMemberDetails.this.runOnUiThread(new Runnable() {
                                        public void run() {
                                            mWait.dismiss();
                                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityMemberDetails.this);
                                            thirdAlertPopup.setTitle("출고완료");
                                            thirdAlertPopup.setContent("출고가 완료되었어요.");
                                            thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                                @Override
                                                public void onConfirmClick() {
                                                    //TODO 체크
                                                    Intent intent = new Intent(getApplicationContext(), ActivityPayment.class);
                                                    intent.putExtra(paymentType, 2);
                                                    startActivity(intent);
                                                }
                                            });
                                            thirdAlertPopup.show();
                                        }
                                    });
                                }
                            }).start();
                        } catch (Exception err) {
                            err.printStackTrace();
                        }
                    }
                    break;

                case R.id.ll_memmber_print:
                    FirstHardware.getMemberInfoPrint();
                    break;

                case R.id.tv_edit_membership_information:
                    intent = new Intent(getApplicationContext(), ActivityNewMemberRegistration.class);
                    intent.putExtra(MEMBER_DETAIL, "2");
                    startActivity(intent);
                    break;
                case R.id.tv_membership_card:
                case R.id.tv_view_coupons:
                    CommonUtil.servicePreparationPopup(ActivityMemberDetails.this);
                    break;
            }
        }
    };

    private void setViewPagerAdapter() {
        viewPager.setAdapter(new AdapterMemberDetailsViewPager(getSupportFragmentManager(), this));
        viewPager.setCurrentItem(0);
        setTapBackGround(0);

        rlVisitReception.setOnClickListener(movePageListener);
        rlVisitReception.setTag(0);
        //TODO 2차오픈 추가 예정.
//        rlLaundryPickupDelivery.setOnClickListener(movePageListener);
//        rlLaundryPickupDelivery.setTag(1);
//        rlRewhitePickupDelivery.setOnClickListener(movePageListener);
//        rlRewhitePickupDelivery.setTag(2);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            viewPager.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                rlVisitReception.setBackgroundResource(R.drawable.a_icon_button_round);
                //TODO 2차오픈 추가 예정.
//                rlLaundryPickupDelivery.setBackgroundResource(R.drawable.a_button_type_3);
//                rlRewhitePickupDelivery.setBackgroundResource(R.drawable.a_button_type_3);

                tvVisitReception.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                //TODO 2차오픈 추가 예정.
//                tvLaundryPickupDelivery.setTextColor(getResources().getColor(R.color.color_9b9b9b));
//                tvRewhitePickupDelivery.setTextColor(getResources().getColor(R.color.color_9b9b9b));
                break;
            //TODO 2차오픈 추가 예정.
//            case 1:
////                rlLaundryPickupDelivery.setBackgroundResource(R.drawable.a_icon_button_round);
//                rlVisitReception.setBackgroundResource(R.drawable.a_button_type_3);
////                rlRewhitePickupDelivery.setBackgroundResource(R.drawable.a_button_type_3);
//
//                tvLaundryPickupDelivery.setTextColor(getResources().getColor(R.color.color_c7cfe8));
//                tvVisitReception.setTextColor(getResources().getColor(R.color.color_9b9b9b));
//                tvRewhitePickupDelivery.setTextColor(getResources().getColor(R.color.color_9b9b9b));
//                break;
//            case 2:
//                rlRewhitePickupDelivery.setBackgroundResource(R.drawable.a_icon_button_round);
//                rlVisitReception.setBackgroundResource(R.drawable.a_button_type_3);
//                rlLaundryPickupDelivery.setBackgroundResource(R.drawable.a_button_type_3);
//
//                tvRewhitePickupDelivery.setTextColor(getResources().getColor(R.color.color_c7cfe8));
//                tvLaundryPickupDelivery.setTextColor(getResources().getColor(R.color.color_9b9b9b));
//                tvVisitReception.setTextColor(getResources().getColor(R.color.color_9b9b9b));
//                break;
        }
    }

    public static void CountMoney(int count, int NonPayMent, int payMent) {
        int TotalPay = 0;

        TotalPay = NonPayMent - payMent;

        tvSelectCount.setText(DataFormatUtil.moneyFormatToWon(count));
        tvNonPayPrice1.setText(DataFormatUtil.moneyFormatToWon(NonPayMent));
        tvTotalPayment.setText(DataFormatUtil.moneyFormatToWon(payMent));

        tvPayment.setText(DataFormatUtil.moneyFormatToWon(TotalPay));

        Logger.d(TAG, "count : " + count);
        Logger.d(TAG, "payMent : " + payMent);
        Logger.d(TAG, "NonPayMent : " + NonPayMent);
        Logger.d(TAG, "TotalPay : " + TotalPay);
    }
}