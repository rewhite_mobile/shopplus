package rewhite.shopplus.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.data.type.NetworkStatus;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.CustomViewPager;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.enviroment.AdapterEnviroment;

import static rewhite.shopplus.activity.ShopPlusApplication.getCurrentActivity;

/**
 * 환경설정 View
 */
public class ActivityEnviroment extends AppCompatActivity {
    private static final String TAG = "ActivityEnviroment";
    private WaitCounter mWait;
    private CustomViewPager viewPager;

    private LinearLayout llSalesSetup, llSetRatetable, llAppSettings, llEquipmentSetup;
    private ImageView ivSalesSetup, ivSetRatetable, ivAppSettings, ivEquipmentSetup;
    private TextView tvSalesSetup, tvSetRatetable, tvAppSettings, tvEquipmentSetup;
    private ImageView imgCloose;
    private AdapterEnviroment adapterEnviroment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviroment);
        checkNetWork();

        mWait = new WaitCounter(this);

        init();
    }

    private void init() {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    EnviromentManager.getManagerStoreManageInfo(ActivityEnviroment.this);
                    ActivityEnviroment.this.runOnUiThread(new Runnable() {
                        public void run() {
                            setLayout();
                            setViewPagerAdapter();
                            checkNetWork();
                            mWait.dismiss();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private void setLayout() {
        viewPager = (CustomViewPager) findViewById(R.id.custom_view_pager);

        llSalesSetup = (LinearLayout) findViewById(R.id.ll_sales_setup);
        llSetRatetable = (LinearLayout) findViewById(R.id.ll_set_ratetable);
        llAppSettings = (LinearLayout) findViewById(R.id.ll_app_settings);
        llEquipmentSetup = (LinearLayout) findViewById(R.id.ll_equipment_setup);

        imgCloose = (ImageView) findViewById(R.id.img_close);
        ivSalesSetup = (ImageView) findViewById(R.id.iv_sales_setup);
        ivSetRatetable = (ImageView) findViewById(R.id.iv_set_ratetable);
        ivAppSettings = (ImageView) findViewById(R.id.iv_app_settings);
        ivEquipmentSetup = (ImageView) findViewById(R.id.iv_equipment_setup);

        tvSalesSetup = (TextView) findViewById(R.id.tv_sales_setup);
        tvSetRatetable = (TextView) findViewById(R.id.tv_set_ratetable);
        tvAppSettings = (TextView) findViewById(R.id.tv_app_settings);
        tvEquipmentSetup = (TextView) findViewById(R.id.tv_equipment_setup);

        imgCloose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityEnviroment.this);
                thirdConfirmPopup.setTitle("팝업종료");
                thirdConfirmPopup.setContent("환경설정을 종료할까요?");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        finish();
                    }
                });
                thirdConfirmPopup.show();
            }
        });
    }

    private void setViewPagerAdapter() {
        adapterEnviroment = new AdapterEnviroment(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapterEnviroment);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(3);
        llSalesSetup.setOnClickListener(movePageListener);
        llSalesSetup.setTag(0);
        llSetRatetable.setOnClickListener(movePageListener);
        llSetRatetable.setTag(1);
        llAppSettings.setOnClickListener(movePageListener);
        llAppSettings.setTag(2);
        llEquipmentSetup.setOnClickListener(movePageListener);
        llEquipmentSetup.setTag(3);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            viewPager.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    private void setTapBackGround(int tag) {

        switch (tag) {
            case 0:
                llSalesSetup.setBackgroundResource(R.color.Wite_color);
                llSetRatetable.setBackgroundResource(R.color.color_43425c);
                llAppSettings.setBackgroundResource(R.color.color_43425c);
                llEquipmentSetup.setBackgroundResource(R.color.color_43425c);

                ivSalesSetup.setBackgroundResource(R.drawable.btn_setting_normal_00);
                ivSetRatetable.setBackgroundResource(R.drawable.btn_setting_press_01);
                ivAppSettings.setBackgroundResource(R.drawable.ico_settings_price_list_normal);
                ivEquipmentSetup.setBackgroundResource(R.drawable.btn_setting_press_03);

                tvSalesSetup.setTextColor(getResources().getColor(R.color.color_43425c));
                tvSetRatetable.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                tvAppSettings.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                tvEquipmentSetup.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                break;

            case 1:
                llSetRatetable.setBackgroundResource(R.color.Wite_color);
                llSalesSetup.setBackgroundResource(R.color.color_43425c);
                llAppSettings.setBackgroundResource(R.color.color_43425c);
                llEquipmentSetup.setBackgroundResource(R.color.color_43425c);

                ivSetRatetable.setBackgroundResource(R.drawable.btn_setting_normal_01);
                ivSalesSetup.setBackgroundResource(R.drawable.btn_setting_press_00);
                ivAppSettings.setBackgroundResource(R.drawable.ico_settings_price_list_normal);
                ivEquipmentSetup.setBackgroundResource(R.drawable.btn_setting_press_03);

                tvSetRatetable.setTextColor(getResources().getColor(R.color.color_43425c));
                tvSalesSetup.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                tvAppSettings.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                tvEquipmentSetup.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                break;

            case 2:
                llAppSettings.setBackgroundResource(R.color.Wite_color);
                llSetRatetable.setBackgroundResource(R.color.color_43425c);
                llSalesSetup.setBackgroundResource(R.color.color_43425c);
                llEquipmentSetup.setBackgroundResource(R.color.color_43425c);

                ivAppSettings.setBackgroundResource(R.drawable.ico_settings_price_list_press);
                ivSetRatetable.setBackgroundResource(R.drawable.btn_setting_press_01);
                ivSalesSetup.setBackgroundResource(R.drawable.btn_setting_press_00);
                ivEquipmentSetup.setBackgroundResource(R.drawable.btn_setting_press_03);

                tvAppSettings.setTextColor(getResources().getColor(R.color.color_43425c));
                tvSetRatetable.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                tvSalesSetup.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                tvEquipmentSetup.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                break;

            case 3:
                llEquipmentSetup.setBackgroundResource(R.color.Wite_color);
                llAppSettings.setBackgroundResource(R.color.color_43425c);
                llSetRatetable.setBackgroundResource(R.color.color_43425c);
                llSalesSetup.setBackgroundResource(R.color.color_43425c);

                ivEquipmentSetup.setBackgroundResource(R.drawable.btn_setting_normal_03);
                ivAppSettings.setBackgroundResource(R.drawable.ico_settings_price_list_normal);
                ivSetRatetable.setBackgroundResource(R.drawable.btn_setting_press_01);
                ivSalesSetup.setBackgroundResource(R.drawable.btn_setting_press_00);

                tvEquipmentSetup.setTextColor(getResources().getColor(R.color.color_43425c));
                tvAppSettings.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                tvSetRatetable.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                tvSalesSetup.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                break;
        }
    }
    /**
     * 네트워크 상태 CheckLogic
     */
    private void checkNetWork() {
        NetworkStatus networkStatus = ShopPlusApplication.getCurrnetNetworkStatus();
        if (!networkStatus.equals(NetworkStatus.WIFI)) {
            CommonUtil.netWorkErrPoup(this);
        }
        getCurrentActivity();
    }
}