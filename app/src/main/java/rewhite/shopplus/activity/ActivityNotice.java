package rewhite.shopplus.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.view.adapter.AdapterNotice;

/**
 * 알림 View
 */
public class ActivityNotice extends AppCompatActivity {
    private static final String TAG = "ActivityNotice";

    private LinearLayout llDataExistence;
    private LinearLayout llNone;

    private ListView lvNotice;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);

        init();
    }

    private void init() {

        setLayout();
        setAdapter();
    }

    /**
     * layout View Setting
     */
    private void setLayout() {
        llDataExistence = (LinearLayout) findViewById(R.id.ll_data_existence);
        llNone = (LinearLayout) findViewById(R.id.ll_none);
        lvNotice = (ListView) findViewById(R.id.lv_notice);

    }

    /**
     * List Adapter Setting
     */
    private void setAdapter() {
        ArrayList<String> testList = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            testList.add("" + i);
        }

        AdapterNotice adapterNotice = new AdapterNotice(getApplicationContext());
        adapterNotice.ItemAdd(testList);

        lvNotice.setAdapter(adapterNotice);
    }
}
