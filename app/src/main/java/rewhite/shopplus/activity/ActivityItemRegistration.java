package rewhite.shopplus.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.manager.ManagerMaxTagModel;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.model.StoreUserInfoItemList;
import rewhite.shopplus.client.network.AuthManager;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.client.network.StoreUserApiManager;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.keyboard.DialogCustomKeyboardTypeA;
import rewhite.shopplus.common.popup.keyboard.DialogCustomKeyboardTypeB;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.data.dto.ItemAddData;
import rewhite.shopplus.data.dto.ItemAdditionalData;
import rewhite.shopplus.data.dto.ItemAdditionalDataType;
import rewhite.shopplus.data.manager.ManagerComponent;
import rewhite.shopplus.data.manager.ManagerItemAdditionalData;
import rewhite.shopplus.data.manager.ManagerItemAdditionalDataType;
import rewhite.shopplus.data.manager.ManagerItemRegistrationAdd;
import rewhite.shopplus.data.manager.ManagerMemo;
import rewhite.shopplus.data.manager.ManagerRepairFree;
import rewhite.shopplus.data.manager.ManagerTechnicalFee;
import rewhite.shopplus.fragment.sub.FragmentCategory;
import rewhite.shopplus.fragment.sub.FragmentColor;
import rewhite.shopplus.fragment.sub.FragmentComponent;
import rewhite.shopplus.fragment.sub.FragmentHowToWash;
import rewhite.shopplus.fragment.sub.FragmentLaundryCharge;
import rewhite.shopplus.fragment.sub.FragmentMemo;
import rewhite.shopplus.fragment.sub.FragmentPhoto;
import rewhite.shopplus.fragment.sub.FragmentRepairFee;
import rewhite.shopplus.fragment.sub.FragmentTechnicalFee;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.SharedPreferencesUtil;
import rewhite.shopplus.util.ToastUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterComponent;
import rewhite.shopplus.view.adapter.AdapterItemComponent;
import rewhite.shopplus.view.adapter.AdapterItemMemo;
import rewhite.shopplus.view.adapter.AdapterItemRegistration;
import rewhite.shopplus.view.adapter.AdapterItemRepair;
import rewhite.shopplus.view.adapter.AdapterItemTechnicalFee;

import static rewhite.shopplus.data.constant.PrefConstant.KEY_START_TAC_NUMBER;
import static rewhite.shopplus.util.CommonUtil.setListViewHeightBasedOnChildren;

/**
 * 품목등록 DialogViewClass
 */
public class ActivityItemRegistration extends AppCompatActivity {
    private static final String TAG = "ActivityItemRegistration";
    private static final String USER_STORE_ID = "user_store_id";                //사용자 key
    private static final String TAC_NUMVER = "tac_numver";                      //택번호
    private static final String BEFORE_CHANGE_MONEY = "before_change_money";    //총 주문 금액
    private static final String paymentType = "1";         // 결제하기 Type;
    private static int userStoreId = 0;         //사용자 StoreID
    private int mItemGrade = 0;         //상품등급 (1:일반,2:명품,3:아동)
    private int StoreItemID;
    private int howtoWashType = 0;  //서버 데이터 전송 세탁방법 Type
    private int otherOptions = 0;   //서버 데이터 전송 기타옵션 Type
    private int laundryMoney, positionColor, totalMoeny;
    private String mType = "Y";
    private ArrayList<String> itemAdditionalDataEdit = new ArrayList<>();

    private int mPosition;  //수정 선택된 리스트 축력하기 위한 position
    private int paymentAmount = 0;
    private int mCategoryId;
    private String mLaundryCharge = "";
    private String memoTitle = "";
    private String mItemName = "";
    private String mTagNumber = "";
    private String mTotalMoney = "0";
    private List<StoreUserInfoItemList> storeUserInfoItemList;

    private ArrayList<ItemAdditionalData.ItemRepairFreeEdit> listRepairFee = new ArrayList<>();
    private ArrayList<ItemAdditionalData.ItemTechnicalEdit> listTechnicalFree = new ArrayList<>();
    private ArrayList<ItemAdditionalData.ItemComponentEdit> listComponent = new ArrayList<>();

    private ArrayList<String> repairAmount = new ArrayList<>();
    private ArrayList<String> technicalFreeAmount = new ArrayList<>();
    private ArrayList<String> componentAmount = new ArrayList<>();
    private StringBuffer listMemo = new StringBuffer();

    private CheckBox checkStopLaundry, checkReWashing, checkLaundryFree;

    private ImageView ivColor, imgTagColor;

    private Button btnTechnicaFeeAdd, btnRepairFeeAdd, btnPhoto, btnMemo, btnComponent, btnColor, btnTotalMoneyEdit;
    private Button btnReceipt, btnTagChange, btnAddItem;

    private TextView tvItem, tvHowToWash, tvLaundryCharge, tvLaundryMoney;
    private TextView tvUserName, tvUserPhoneNumber, tvUserAddrres, tvTacNumber;
    private TextView tvTotalMoney, tvItemListTotalMoeny, tvItemCount;

    private LinearLayout llLaundryCharge, llHowToWash, llDetail, llColor, llView;
    private RelativeLayout rlRepairFree, rlTechnicalFree, rlComponent, rlMemo;

    private ListView lvTechnicalFee, lvViewComponent, lvRepairFee, lvRegistation;

    private RecyclerView recycleViewMemo;
    private WaitCounter mWait;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_registration);

        mWait = new WaitCounter(this);

        Intent intent = getIntent();
        userStoreId = intent.getExtras().getInt(USER_STORE_ID);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setLayout();
        //Tag값 데이터
        String changeTacNumber = SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_NUMBER);
        //Tag컬러 값 데이터
        int tagPosition = SharedPreferencesUtil.getIntSharedPreference(getApplicationContext(), KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_NUMBER_POSITION);

        if (!TextUtils.isEmpty(changeTacNumber) || changeTacNumber != null) {
            tvTacNumber.setText(changeTacNumber);
            CommonUtil.getTacColor(getApplicationContext(), tagPosition, imgTagColor);
        }
        try {
            //총 금액 변경 값 데이터
            tvTotalMoney.setText(SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY));
            totalMoeny = Integer.valueOf(SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY));
        } catch (Exception err) {
            err.printStackTrace();
        }
        SharedPreferencesUtil.removeSharedPreference(getApplicationContext(), PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY);

        Logger.d(TAG, "aMount : " + SharedPreferencesUtil.getIntSharedPreference(getApplicationContext(), PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY));
        //품목추가 데이터 초기화
        ManagerItemAdditionalDataType.getInstance().setItemDelete();
    }

    /**
     * SETTING INIT
     */
    private void init() {
        //회원정보 바인딩
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                LaundryRegistrationManager.getManagerMaxTagModel(ActivityItemRegistration.this);
                StoreUserApiManager.getStoreUserInfo(ActivityItemRegistration.this, Long.valueOf(userStoreId));
                LaundryRegistrationManager.getManagerStoreUserList(ActivityItemRegistration.this, 1);
                EnviromentManager.getManagerStoreManageInfo(ActivityItemRegistration.this);

                runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        storeUserInfoItemList = ManagerStoreUserInfo.getmInstance().getStoreUserInfo();
                        setLayout();
                        setTextView();
                        setItemReset();

                    }
                });
            }
        }).start();
    }

    /**
     * LAYOUT SETTING
     */
    private void setLayout() {
        btnAddItem = (Button) findViewById(R.id.btn_add_item);
        tvTotalMoney = (TextView) findViewById(R.id.tv_total_money);

        btnTagChange = (Button) findViewById(R.id.btn_tag_change);
        tvItemListTotalMoeny = (TextView) findViewById(R.id.tv_item_list_total_moeny);
        tvLaundryMoney = (TextView) findViewById(R.id.tv_laundry_money);
        tvItemCount = (TextView) findViewById(R.id.tv_item_count);
        rlMemo = (RelativeLayout) findViewById(R.id.rl_memo);
        llLaundryCharge = (LinearLayout) findViewById(R.id.ll_laundry_charge);
        llHowToWash = (LinearLayout) findViewById(R.id.ll_how_to_wash);
        llDetail = (LinearLayout) findViewById(R.id.ll_detail);
        llColor = (LinearLayout) findViewById(R.id.ll_color);
        llView = (LinearLayout) findViewById(R.id.ll_view);

        rlRepairFree = (RelativeLayout) findViewById(R.id.rl_repair_free);
        rlTechnicalFree = (RelativeLayout) findViewById(R.id.rl_technical_free);
        rlComponent = (RelativeLayout) findViewById(R.id.rl_component);

        imgTagColor = (ImageView) findViewById(R.id.img_tag_color);
        lvRegistation = (ListView) findViewById(R.id.lv_registation);
        btnTechnicaFeeAdd = (Button) findViewById(R.id.btn_technical_fee_add);
        btnRepairFeeAdd = (Button) findViewById(R.id.btn_repair_fee_add);
        btnPhoto = (Button) findViewById(R.id.btn_photo_add);
        btnMemo = (Button) findViewById(R.id.btn_memo_add);
        btnComponent = (Button) findViewById(R.id.btn_component_add);
        btnColor = (Button) findViewById(R.id.btn_color_add);
        btnTotalMoneyEdit = (Button) findViewById(R.id.btn_total_money_edit);

        tvItem = (TextView) findViewById(R.id.tv_item);
        tvLaundryCharge = (TextView) findViewById(R.id.tv_laundry_charge);
        tvHowToWash = (TextView) findViewById(R.id.tv_how_to_wash);

        ivColor = (ImageView) findViewById(R.id.iv_color);
        tvUserName = (TextView) findViewById(R.id.tv_user_name);
        tvUserPhoneNumber = (TextView) findViewById(R.id.tv_user_phoneNumber);
        tvUserAddrres = (TextView) findViewById(R.id.tv_user_address);

        tvTacNumber = (TextView) findViewById(R.id.tv_tac_number);

        lvViewComponent = (ListView) findViewById(R.id.lv_view_component);
        lvRepairFee = (ListView) findViewById(R.id.lv_repair_fee);
        lvTechnicalFee = (ListView) findViewById(R.id.lv_technical_fee);
        recycleViewMemo = (RecyclerView) findViewById(R.id.recycle_view_memo);

        checkStopLaundry = (CheckBox) findViewById(R.id.ch_stop_laundry);
        checkReWashing = (CheckBox) findViewById(R.id.ch_re_washing);
        checkLaundryFree = (CheckBox) findViewById(R.id.ch_laundry_free);

        btnReceipt = (Button) findViewById(R.id.btn_receipt);
        setClickAction();
    }

    /**
     * Text Setting
     */
    private void setTextView() {
        int tagType = SharedPreferencesUtil.getIntSharedPreference(getApplicationContext(), PrefConstant.KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_TYPE);
        tvUserName.setText(storeUserInfoItemList.get(0).getData().getUserName());
        tvUserPhoneNumber.setText(storeUserInfoItemList.get(0).getData().get_UserPhone());
        tvUserAddrres.setText(storeUserInfoItemList.get(0).getData().get_UserAddress());
        llView.setVisibility(View.VISIBLE);
        try {
            //초기 셋팅 택번호
            if (ManagerMaxTagModel.getmInstance().getManagerGetMaxTagNoModel().get(0).getData() == null) {
                if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
                    tvTacNumber.setText("①001");
                    mTagNumber = "①001";
                } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
                    tvTacNumber.setText("1⓪001");
                    mTagNumber = "1⓪001";
                } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
                    tvTacNumber.setText("0-0001");
                    mTagNumber = "0-0001";
                }
            } else {
                if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() != tagType) {
                    Logger.d(TAG, "1");
                    if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
                        tvTacNumber.setText("①001");
                        mTagNumber = "①001";
                    } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
                        tvTacNumber.setText("1⓪001");
                        mTagNumber = "1⓪001";
                    } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
                        tvTacNumber.setText("0-0001");
                        mTagNumber = "0-0001";
                    }
                } else {
                    try {
                        Logger.d(TAG, "2");
                        if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
                            Logger.d(TAG, "일제");
                            tvTacNumber.setText(CommonUtil.setTagFormaterChange(ManagerMaxTagModel.getmInstance().getManagerGetMaxTagNoModel().get(0).getData().getTagNo()));
                            mTagNumber = CommonUtil.setTagFormaterChange(ManagerMaxTagModel.getmInstance().getManagerGetMaxTagNoModel().get(0).getData().getTagNo());
                        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
                            Logger.d(TAG, "바코드");
                            tvTacNumber.setText(CommonUtil.setTagFormaterChangeType(ManagerMaxTagModel.getmInstance().getManagerGetMaxTagNoModel().get(0).getData().getTagNo()));
                            mTagNumber = CommonUtil.setTagFormaterChangeType(ManagerMaxTagModel.getmInstance().getManagerGetMaxTagNoModel().get(0).getData().getTagNo());
                        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
                            Logger.d(TAG, "기타");
                            tvTacNumber.setText(CommonUtil.setEtcTagFormaterChangeResponse(ManagerMaxTagModel.getmInstance().getManagerGetMaxTagNoModel().get(0).getData().getTagNo()));
                            mTagNumber = CommonUtil.setEtcTagFormaterChangeResponse(ManagerMaxTagModel.getmInstance().getManagerGetMaxTagNoModel().get(0).getData().getTagNo());
                        }
                    } catch (Exception err) {
                        err.printStackTrace();
                    }
                }
            }
        } catch (IndexOutOfBoundsException err) {
            err.printStackTrace();
        }
    }

    /**
     * Action Click Listener Setting
     */
    private void setClickAction() {
        btnTechnicaFeeAdd.setOnClickListener(viewOnClickListener);
        btnRepairFeeAdd.setOnClickListener(viewOnClickListener);
        btnPhoto.setOnClickListener(viewOnClickListener);
        btnMemo.setOnClickListener(viewOnClickListener);
        btnComponent.setOnClickListener(viewOnClickListener);
        btnColor.setOnClickListener(viewOnClickListener);
        btnTagChange.setOnClickListener(viewOnClickListener);

        tvItem.setOnClickListener(viewOnClickListener);
        tvLaundryCharge.setOnClickListener(viewOnClickListener);
        tvHowToWash.setOnClickListener(viewOnClickListener);
        tvTacNumber.setOnClickListener(viewOnClickListener);

        btnAddItem.setOnClickListener(viewOnClickListener);
        btnTotalMoneyEdit.setOnClickListener(viewOnClickListener);

        checkStopLaundry.setOnClickListener(viewOnClickListener);
        checkReWashing.setOnClickListener(viewOnClickListener);
        checkLaundryFree.setOnClickListener(viewOnClickListener);

        btnReceipt.setOnClickListener(viewOnClickListener);
    }

    private View.OnClickListener viewOnClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            int id = v.getId();
            final Intent[] intent = {null};

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (id) {
                case R.id.tv_item:                  //카테고리 품목 선택 FragmentView
                    FragmentCategory fragmentCategory = new FragmentCategory(OnArticleSelectedListener);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentCategory);
                    fragmentTransaction.commit();
                    break;
                case R.id.tv_laundry_charge:        //세탁요금 선택 FragmentView
                    FragmentLaundryCharge fragmentLaundryCharge = new FragmentLaundryCharge(OnArticleSelectedListener);
                    FragmentLaundryCharge.setLaundryName(tvItem.getText().toString(), mCategoryId);  //선택된 품목 문자넘김
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentLaundryCharge);
                    fragmentTransaction.commit();
                    break;
                case R.id.tv_how_to_wash:           //세탁방법 선택 FragmentView
                    FragmentHowToWash fragmentHowToWash = new FragmentHowToWash(OnArticleSelectedListener);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentHowToWash);
                    fragmentTransaction.commit();
                    break;
                case R.id.btn_repair_fee_add:       //수선요금 FragmentView
                    FragmentRepairFee fragmentRepairFee = new FragmentRepairFee(OnArticleSelectedListener);
                    fragmentRepairFee.setSeelctedItem(mPosition, mType);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentRepairFee);
                    fragmentTransaction.commit();
                    break;

                case R.id.btn_technical_fee_add:    //기술요금 FragmentView
                    FragmentTechnicalFee fragmentTechnicalFee = new FragmentTechnicalFee(OnArticleSelectedListener);
                    fragmentTechnicalFee.setSeelctedItem(mPosition, mType);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentTechnicalFee);
                    fragmentTransaction.commit();
                    break;

                case R.id.btn_photo_add:            //사진 FragmentView
                    FragmentPhoto fragmentPhoto = new FragmentPhoto();
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentPhoto);
                    fragmentTransaction.commit();

                    break;
                case R.id.btn_memo_add:             //메모 FragmentView
                    FragmentMemo fragmentMemo = new FragmentMemo(OnArticleSelectedListener);
                    fragmentMemo.setSeelctedItem(mPosition, mType);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentMemo);
                    fragmentTransaction.commit();

                    break;
                case R.id.btn_component_add:        //부속품 FragmentView
                    FragmentComponent fragmentComponent = new FragmentComponent(OnArticleSelectedListener);
                    fragmentComponent.setSeelctedItem(mPosition, mType);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentComponent);
                    fragmentTransaction.commit();

                    break;
                case R.id.btn_color_add:            //색상 FragmentView
                    FragmentColor fragmentColor = new FragmentColor(OnArticleSelectedListener);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentColor);
                    fragmentTransaction.commit();
                    break;

                case R.id.btn_tag_change:            //택번호 변경
                    intent[0] = new Intent(getApplicationContext(), DialogCustomKeyboardTypeA.class);
                    intent[0].putExtra(TAC_NUMVER, tvTacNumber.getText().toString());
                    startActivity(intent[0]);
                    break;

                case R.id.btn_add_item:     //품목추가
                    setItemAddData();
                    break;

                case R.id.btn_total_money_edit:
                    intent[0] = new Intent(getApplicationContext(), DialogCustomKeyboardTypeB.class);
                    intent[0].putExtra(BEFORE_CHANGE_MONEY, tvLaundryMoney.getText().toString());
                    startActivity(intent[0]);
                    break;

                case R.id.ch_stop_laundry:
                    if (((CheckBox) v).isChecked()) {
                        otherOptions = 0;
                        checkStopLaundry.setBackgroundResource(R.drawable.a_button_type_2);
                        checkStopLaundry.setTextColor(getResources().getColor(R.color.Wite_color));

                        checkReWashing.setBackgroundResource(R.drawable.a_button_type_3);
                        checkReWashing.setTextColor(getResources().getColor(R.color.color_43425c));

                        checkLaundryFree.setBackgroundResource(R.drawable.a_button_type_3);
                        checkLaundryFree.setTextColor(getResources().getColor(R.color.color_43425c));
                        tvLaundryMoney.setText(getResources().getString(R.string.number_zero));
                        tvTotalMoney.setText(getResources().getString(R.string.number_zero));
                    }
                    break;

                case R.id.ch_re_washing:
                    if (((CheckBox) v).isChecked()) {
                        otherOptions = 1;
                        checkStopLaundry.setBackgroundResource(R.drawable.a_button_type_3);
                        checkStopLaundry.setTextColor(getResources().getColor(R.color.color_43425c));

                        checkReWashing.setBackgroundResource(R.drawable.a_button_type_2);
                        checkReWashing.setTextColor(getResources().getColor(R.color.Wite_color));

                        checkLaundryFree.setBackgroundResource(R.drawable.a_button_type_3);
                        checkLaundryFree.setTextColor(getResources().getColor(R.color.color_43425c));
                        tvLaundryMoney.setText(getResources().getString(R.string.number_zero));
                        tvTotalMoney.setText(getResources().getString(R.string.number_zero));
                    }
                    break;

                case R.id.ch_laundry_free:
                    if (((CheckBox) v).isChecked()) {
                        otherOptions = 2;
                        checkStopLaundry.setBackgroundResource(R.drawable.a_button_type_3);
                        checkStopLaundry.setTextColor(getResources().getColor(R.color.color_43425c));

                        checkReWashing.setBackgroundResource(R.drawable.a_button_type_3);
                        checkReWashing.setTextColor(getResources().getColor(R.color.color_43425c));

                        checkLaundryFree.setBackgroundResource(R.drawable.a_button_type_2);
                        checkLaundryFree.setTextColor(getResources().getColor(R.color.Wite_color));
                        tvLaundryMoney.setText(getResources().getString(R.string.number_zero));
                        tvTotalMoney.setText(getResources().getString(R.string.number_zero));
                    }
                    break;


                case R.id.btn_receipt:
                    if (ManagerItemAdditionalData.getInstance().getItemAdditionalData().size() != 0) {
                        final int position = SharedPreferencesUtil.getIntSharedPreference(getApplicationContext(), KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_NUMBER_POSITION);
                        final Dialog dlg = new Dialog(ActivityItemRegistration.this);
                        dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dlg.setContentView(R.layout.dialog_custom);
                        dlg.show();

                        final Button okButton = (Button) dlg.findViewById(R.id.right_button);
                        final Button cancelButton = (Button) dlg.findViewById(R.id.left_button);

                        okButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                setServerRequestData();
                                dlg.dismiss();
                            }
                        });

                        cancelButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dlg.dismiss();
                            }
                        });

                    } else {
                        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityItemRegistration.this);
                        thirdAlertPopup.setTitle(getResources().getString(R.string.notice));
                        thirdAlertPopup.setContent(getResources().getString(R.string.dialog_item_registration_info));
                        thirdAlertPopup.setButtonText(getResources().getString(R.string.agree));
                        thirdAlertPopup.show();
                    }
                    break;
            }
        }
    };

    /**
     * 초기 View 셋팅
     */
    private void setContentView() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentCategory fragmentCategory = new FragmentCategory(OnArticleSelectedListener);
        fragmentTransaction.replace(R.id.fragment_item_registation, fragmentCategory);
        fragmentTransaction.commit();
    }

    /**
     * 품목추가 예외 처리
     */
    private boolean itemAddNullDataCheckDialog() {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(this);
        if (TextUtils.isEmpty(tvTacNumber.getText().toString())) {
            thirdAlertPopup.setTitle(getResources().getString(R.string.notice));
            thirdAlertPopup.setContent(getResources().getString(R.string.dialog_tag_select_number_info));
            thirdAlertPopup.setButtonText(getResources().getString(R.string.agree));
            thirdAlertPopup.show();
            return false;
        } else if (tvItem.getText().toString().equals(getResources().getString(R.string.dialog_item_select_info))) {
            thirdAlertPopup.setTitle(getResources().getString(R.string.notice));
            thirdAlertPopup.setContent(getResources().getString(R.string.dialog_item_select_info));
            thirdAlertPopup.setButtonText(getResources().getString(R.string.agree));
            thirdAlertPopup.show();
            return false;
        } else if (tvLaundryCharge.getText().toString().equals(getResources().getString(R.string.dialog_select_laundry_charge_info))) {
            thirdAlertPopup.setTitle(getResources().getString(R.string.notice));
            thirdAlertPopup.setContent(getResources().getString(R.string.dialog_select_laundry_charge_info));
            thirdAlertPopup.setButtonText(getResources().getString(R.string.agree));
            thirdAlertPopup.show();
            return false;
        } else if (tvHowToWash.getText().toString().equals(getResources().getString(R.string.dialog_select_washing_method_info))) {
            thirdAlertPopup.setTitle(getResources().getString(R.string.notice));
            thirdAlertPopup.setContent(getResources().getString(R.string.dialog_select_washing_method_info));
            thirdAlertPopup.setButtonText(getResources().getString(R.string.agree));
            thirdAlertPopup.show();
            return false;
        }
        return true;
    }

    /**
     * 품목 추가 리스트 추가
     */
    private void setItemAddData() {
        int tagCount = 0;
        ArrayList<ItemAdditionalData.OrderItemOptionInput> orderItemOptionInputs = new ArrayList<>();
        ArrayList<ItemAdditionalData.ItemRepairFreeEdit> ItemRepairFreeEdit = new ArrayList<>();        //수선요금
        ArrayList<ItemAdditionalData.ItemTechnicalEdit> ItemTechnicalEdit = new ArrayList<>();          //기술요금
        ArrayList<ItemAdditionalData.ItemComponentEdit> ItemComponentEdit = new ArrayList<>();          //부속품요금

        if (itemAddNullDataCheckDialog()) {
            ArrayList<ItemAdditionalData> itemAdditionalData = ManagerItemAdditionalData.getInstance().getItemAdditionalData();
            ItemAdditionalData additionalData = new ItemAdditionalData();
            ItemAdditionalData.ItemComponentEdit itemComponentEdit = new ItemAdditionalData.ItemComponentEdit();//부속품요금
            additionalData.setItemName(mItemName);

            if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
                additionalData.setTacNumber(CommonUtil.setTagFormaterChangeResponse(tvTacNumber.getText().toString()));        // 일제 택번호
            } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
                additionalData.setTacNumber(CommonUtil.setTagFormaterChangeTypeResponse(tvTacNumber.getText().toString()));    // 일제바코드 택번호
            } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
                additionalData.setTacNumber(tvTacNumber.getText().toString());// 기타 택번호
            }

            additionalData.setTacColor(SharedPreferencesUtil.getIntSharedPreference(getApplicationContext(), KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_NUMBER_POSITION)); // 택컬러
            additionalData.setStoreItemID(StoreItemID);                           // 가맹점 상품 ID
            additionalData.setStoreItemName(mLaundryCharge);                      // 가맹점 상품 NAME <-서버 데이터 저장안함.
            additionalData.setItemGrade(mItemGrade);                              // 상품등급
            additionalData.setLaundryMoney(laundryMoney);                         // 상품금액 <-서버 데이터 저장안함.
            additionalData.setIsOutside(null);                                    // 외부세탁여부
            additionalData.setIsDelivery(null);                                   // 배송여부
            additionalData.setPhotoUri(null);                                     // 첨부사진
            additionalData.setHowToWash(howtoWashType);                           // 세탁방법
            additionalData.setFreeType(otherOptions);                             // 기타옵션
            if (TextUtils.isEmpty(listMemo.toString()) || listMemo.toString().equals("") || listMemo.toString().length() == 0) {
                additionalData.setMemoData(null);                      // 메모
            } else {
                additionalData.setMemoData(listMemo.toString());                      // 메모
            }
            additionalData.setRgbColor(positionColor);                            // 색상
            additionalData.setTotalMoney(String.valueOf(totalMoeny));             // 주문금액
            //데이터 리스트 추가

            if (!TextUtils.isEmpty(listMemo.toString())) {
                tagCount += 1;
            }

            //수선 요금 ADD
            if (listRepairFee.size() != 0 || listRepairFee != null) {
                tagCount += 1;
                for (ItemAdditionalData.ItemRepairFreeEdit itemRepairFreeEdit1 : listRepairFee) {
                    ItemAdditionalData.ItemRepairFreeEdit itemRepairFreeEdit = new ItemAdditionalData.ItemRepairFreeEdit();//수선요금
                    itemRepairFreeEdit.setAmount(itemRepairFreeEdit1.getAmount());
                    itemRepairFreeEdit.setTitleName(itemRepairFreeEdit1.getTitleName());
                    itemRepairFreeEdit.setOptionItemID(itemRepairFreeEdit1.getOptionItemID());
                    ItemRepairFreeEdit.add(itemRepairFreeEdit);
                }

                for (ItemAdditionalData.ItemRepairFreeEdit itemRepairFree : listRepairFee) {
                    ItemAdditionalData.OrderItemOptionInput orderItemOptionInput = new ItemAdditionalData.OrderItemOptionInput();
                    orderItemOptionInput.setOptionItemId(Integer.valueOf(itemRepairFree.getOptionItemID()));
                    orderItemOptionInput.setOptionType(3);
                    orderItemOptionInputs.add(orderItemOptionInput);
                }
            }

            //추가기술 ADD
            try {
                if (listTechnicalFree.size() != 0 || listTechnicalFree != null) {
                    tagCount += 1;
                    for (ItemAdditionalData.ItemTechnicalEdit itemTechnicalEdit1 : listTechnicalFree) {
                        ItemAdditionalData.ItemTechnicalEdit itemTechnicalEdit = new ItemAdditionalData.ItemTechnicalEdit();//수선요금
                        itemTechnicalEdit.setAmount(itemTechnicalEdit1.getAmount());
                        itemTechnicalEdit.setTitleName(itemTechnicalEdit1.getTitleName());
                        itemTechnicalEdit.setOptionItemID(itemTechnicalEdit1.getOptionItemID());
                        ItemTechnicalEdit.add(itemTechnicalEdit);
                    }

                    for (ItemAdditionalData.ItemTechnicalEdit itemRepairFree : listTechnicalFree) {
                        ItemAdditionalData.OrderItemOptionInput orderItemOptionInput = new ItemAdditionalData.OrderItemOptionInput();
                        orderItemOptionInput.setOptionItemId(Integer.valueOf(itemRepairFree.getOptionItemID()));
                        orderItemOptionInput.setOptionType(1);
                        orderItemOptionInputs.add(orderItemOptionInput);
                    }
                }
            } catch (NumberFormatException err) {
                err.printStackTrace();
            }

            //부속품 ADD
            try {
                if (listComponent.size() != 0 || listComponent != null) {
                    tagCount += 1;
                    for (ItemAdditionalData.ItemComponentEdit itemComponentEdit1 : listComponent) {
                        ItemAdditionalData.ItemComponentEdit itemComponent = new ItemAdditionalData.ItemComponentEdit();//수선요금
                        itemComponent.setAmount(itemComponentEdit1.getAmount());
                        itemComponent.setTitleName(itemComponentEdit1.getTitleName());
                        itemComponent.setOptionItemID(itemComponentEdit1.getOptionItemID());
                        ItemComponentEdit.add(itemComponent);
                    }


                    for (ItemAdditionalData.ItemComponentEdit itemComponent : listComponent) {
                        ItemAdditionalData.OrderItemOptionInput orderItemOptionInput = new ItemAdditionalData.OrderItemOptionInput();
                        orderItemOptionInput.setOptionItemId(Integer.valueOf(itemComponent.getOptionItemID()));
                        orderItemOptionInput.setOptionType(2);
                        orderItemOptionInputs.add(orderItemOptionInput);
                    }
                }
            } catch (NumberFormatException err) {
                err.printStackTrace();
            }
            additionalData.setTagCount(tagCount);
            additionalData.setOptionTypeRepairFee(ItemRepairFreeEdit);          //수선
            additionalData.setOptionTechnicalFree(ItemTechnicalEdit);
            additionalData.setOptionTypeComponent(ItemComponentEdit);

            additionalData.setOptions(orderItemOptionInputs);                    // 추가옵션

            if (btnAddItem.getText().toString().equals("수정완료")) {
                itemAdditionalData.set(mPosition, additionalData);
                ManagerItemAdditionalData.getInstance().setItemAdditionalData(itemAdditionalData);

                //품목추가 ListView Add
                AdapterItemRegistration adapterItemRegistration = new AdapterItemRegistration(this, onClickItem);
                adapterItemRegistration.addItem(itemAdditionalData);
                lvRegistation.setAdapter(adapterItemRegistration);
                adapterItemRegistration.notifyDataSetChanged();
                mType = "Y";
            } else {
                itemAdditionalData.add(additionalData);
                ManagerItemAdditionalData.getInstance().setItemAdditionalData(itemAdditionalData);

                //품목추가 ListView Add
                AdapterItemRegistration adapterItemRegistration = new AdapterItemRegistration(this, onClickItem);
                adapterItemRegistration.addItem(itemAdditionalData);
                lvRegistation.setAdapter(adapterItemRegistration);
                adapterItemRegistration.notifyDataSetChanged();
            }

            SharedPreferencesUtil.removeSharedPreference(getApplicationContext(), PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY);
            setItemDataChange();
            setItemReset();
            setTacNumberView();
        }
    }

    /**
     * 서버 데이터 전송 메소드
     */
    private void setServerRequestData() {
        try {
            ArrayList<ItemAdditionalData> itemAdditionalData = ManagerItemAdditionalData.getInstance().getItemAdditionalData();
            ArrayList<ItemAdditionalDataType> itemAdditionalDataList = ManagerItemAdditionalDataType.getInstance().getItemAdditionalData();
            ArrayList<ItemAddData> itemAddDatas = new ArrayList<>();

            for (int j = 0; j < itemAdditionalData.size(); j++) {
                ArrayList<ItemAddData.OrderItemOptionInput> orderItemOptionInputs = new ArrayList<>();

                ItemAdditionalDataType itemAdditionalDataType = new ItemAdditionalDataType();
                ItemAddData itemAddData = new ItemAddData();
                //품목
                itemAddData.setTacNumber(itemAdditionalData.get(j).getTacNumber());                            //Tag번호
                itemAddData.setTacColor(itemAdditionalData.get(j).getTacColor());                              //Tag컬러
                itemAddData.setStoreItemID(itemAdditionalData.get(j).getStoreItemID());                        //가맹점 상품 ID
                itemAddData.setItemGrade(itemAdditionalData.get(j).getItemGrade());                            //상품등급
                itemAddData.setIsOutside(null);                                                                //외부세탁여부 2차 오픈 작업
                itemAddData.setIsDelicery(null);                                                               //배송여부 2차 오픈 작업
                itemAddData.setPhotoUrl(null);                                                                 //사진 URL 2차 오픈 작업
                itemAddData.setWashType(itemAdditionalData.get(j).getItemGrade());                             //세탁방법
                itemAddData.setFreeType(itemAdditionalData.get(j).getFreeType());                              //기타옵션
                itemAddData.setOrderItemMemo(itemAdditionalData.get(j).getMemoData());                         //메모

                itemAddData.setItemColor(String.valueOf(itemAdditionalData.get(j).getRgbColor()));             //색상

                itemAddData.setOrderItemPrice(Integer.valueOf(itemAdditionalData.get(j).getTotalMoney()));     //주문 상품가격

                if (itemAdditionalData.get(j).getOptions().size() != 0) {

                    for (int i = 0; i < itemAdditionalData.get(j).getOptions().size(); i++) {
                        ItemAddData.OrderItemOptionInput orderItemOptionInput = new ItemAddData.OrderItemOptionInput();

                        orderItemOptionInput.setOptionItemId(itemAdditionalData.get(j).getOptions().get(i).getOptionItemId());
                        orderItemOptionInput.setOptionType(itemAdditionalData.get(j).getOptions().get(i).getOptionType());
                        orderItemOptionInputs.add(orderItemOptionInput);
                        itemAddData.setItemOption(orderItemOptionInputs);
                    }
                } else if (itemAdditionalData.get(j).getOptions().size() == 0) {
                    itemAddData.setItemOption(orderItemOptionInputs);
                }

                itemAddDatas.add(itemAddData);
                itemAdditionalDataType.setItemAddData(itemAddDatas);
                itemAdditionalDataList.add(itemAdditionalDataType);
            }

            ManagerItemAdditionalDataType.getInstance().setItemAdditionalData(itemAdditionalDataList);
            SharedPreferencesUtil.putSharedPreference(getApplicationContext(), PrefConstant.KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_TYPE, ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType());

            new Thread(new Runnable() {
                public void run() {
                    String requestCode = AuthManager.setStoreItemAd(ActivityItemRegistration.this);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (requestCode.equals("S0000")) {
                                setPaymentDataIn();
                            } else {

                            }
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private void setPaymentDataIn() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                ManagerGetVisitOrder managerGetVisitOrder = VisitReceptionManager.getGetVisitOrder(ActivityItemRegistration.this, "NEEDPAY", SettingLogic.getStoreUserId(ActivityItemRegistration.this), "", "");
                runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        List<GetVisitOrderList> getVisitOrderList = managerGetVisitOrder.getGetVisitOrder();
                        List<GetVisitOrderList> getVisitOrderListData = new ArrayList<>();

                        for (GetVisitOrderList getVisitOrderList1 : getVisitOrderList) {
                            for (ItemAdditionalData itemAdditionalData1 : ManagerItemAdditionalData.getInstance().getItemAdditionalData()) {

                                if (getVisitOrderList1.getStoreItemId() == itemAdditionalData1.getStoreItemID() && getVisitOrderList1.getTagNo().equals(itemAdditionalData1.getTacNumber())) {
                                    getVisitOrderListData.add(getVisitOrderList1);
                                    ManagerItemRegistrationAdd.getmInstance().setGetVisitOrder(getVisitOrderListData);
                                }
                            }
                        }
                        Intent intent = new Intent(getApplicationContext(), ActivityPayment.class);
                        intent.putExtra(paymentType, 1);
                        startActivity(intent);
                        finish();

                        ManagerItemAdditionalData.getInstance().setReairFreeDelete();
                        SharedPreferencesUtil.removeSharedPreference(getApplicationContext(), PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY);
                    }
                });
            }
        }).start();
    }

    private void setTacNumberView() {
        if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
            tvTacNumber.setText(CommonUtil.setTagFormaterChange(CommonUtil.setTagFormaterChangeResponse(tvTacNumber.getText().toString())));
            mTagNumber = CommonUtil.setTagFormaterChange(CommonUtil.setTagFormaterChangeResponse(tvTacNumber.getText().toString()));
        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
            tvTacNumber.setText(CommonUtil.setTagFormaterChangeType(CommonUtil.setTagFormaterChangeTypeResponse(tvTacNumber.getText().toString())));
            mTagNumber = CommonUtil.setTagFormaterChangeType(CommonUtil.setTagFormaterChangeTypeResponse(tvTacNumber.getText().toString()));
        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
            tvTacNumber.setText(CommonUtil.setTagFormaterChangeResponse(tvTacNumber.getText().toString()));
            mTagNumber = CommonUtil.setTagFormaterChangeResponse(tvTacNumber.getText().toString());
        }
    }

    /**
     * dataChange
     */
    private void setItemDataChange() {
        int itemListTotalMoney = 0;

        if (ManagerItemAdditionalData.getInstance().getItemAdditionalData().size() != 0) {
            ArrayList<ItemAdditionalData> itemAdditionalData = ManagerItemAdditionalData.getInstance().getItemAdditionalData();

            if (itemAdditionalData.size() != 0) {
                for (int i = 0; i < itemAdditionalData.size(); i++) {
                    itemListTotalMoney += Integer.valueOf(itemAdditionalData.get(i).getTotalMoney());
                }
            }
            tvLaundryMoney.setText(getResources().getString(R.string.number_zero));
            tvTotalMoney.setText(getResources().getString(R.string.number_zero));
            SharedPreferencesUtil.putSharedPreference(getApplicationContext(), PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY, itemListTotalMoney);
            tvItemListTotalMoeny.setText(DataFormatUtil.moneyFormatToWon(itemListTotalMoney));
            tvItemCount.setText(String.valueOf(itemAdditionalData.size()));
        }
    }

    /**
     * 품목추가 데이터 초기화 처리
     */
    private void setItemReset() {
        ManagerRepairFree.getInstance().setReairFreeDelete();   //수선요금 초기화
        ManagerTechnicalFee.getInstance().setItemDelete();      //기술요금 초기화
        ManagerMemo.getInstance().setItemDelete();              //메모데이터 초기화
        ManagerComponent.getInstance().setItemDelete();         //부속품 초기화

        repairAmount.clear();
        technicalFreeAmount.clear();
        componentAmount.clear();

        setContentView();                                       //View 초기화

        tvItem.setText(getResources().getString(R.string.item_select_info));
        tvLaundryCharge.setText(getResources().getString(R.string.select_laundry_charge_info));
        tvHowToWash.setText(getResources().getString(R.string.select_washing_method_info));
        btnAddItem.setText(getResources().getString(R.string.item_add));

        btnColor.setText(getResources().getString(R.string.adding));
        btnMemo.setText(getResources().getString(R.string.adding));
        btnComponent.setText(getResources().getString(R.string.adding));
        btnRepairFeeAdd.setText(getResources().getString(R.string.adding));
        btnTechnicaFeeAdd.setText(getResources().getString(R.string.adding));
        tvTotalMoney.setText(getResources().getString(R.string.number_zero));
        setViewHinden(false);
    }

    /**
     * Fragment Select Item Data Setting
     */
    public interface OnArticleSelectedListener {
        public void onItemSelected(String name, int categoryId);              //품목명

        public void onLaundryCharge(String laundryCharge, int moneym, int storeItemID, int itemGrade);         //세탁요금

        public void onHowToWash(String HowToWash);                            //세탁방법

        public void onRepairFee(ArrayList<ItemAdditionalData.ItemRepairFreeEdit> RepairFee);         //수선요금

        public void onTechnicalFree(ArrayList<ItemAdditionalData.ItemTechnicalEdit> technicalFree); //기술요금

        public void onPhotoAdd(String photo);                                 //사진촬영

        public void onComponent(ArrayList<ItemAdditionalData.ItemComponentEdit> component);         //부속품

        public void onColor(int position);                                    //색상

        public void onMemo(ArrayList<String> memo);                           //메모
    }

    public OnArticleSelectedListener OnArticleSelectedListener = new OnArticleSelectedListener() {
        @Override
        public void onItemSelected(String name, int categoryId) {
            if (!TextUtils.isEmpty(name) || name != null) {
                llLaundryCharge.setVisibility(View.VISIBLE);
                mItemName = name;
                tvItem.setText(name);
                mCategoryId = categoryId;

                //화면 View 변경 (세탁요금)
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                FragmentLaundryCharge fragmentLaundryCharge = new FragmentLaundryCharge(OnArticleSelectedListener);
                FragmentLaundryCharge.setLaundryName(name, categoryId);  //선택된 품목 문자넘김
                fragmentTransaction.replace(R.id.fragment_item_registation, fragmentLaundryCharge);
                fragmentTransaction.commit();
            } else {
                ToastUtil.showMakeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT);
            }
        }

        @Override
        public void onLaundryCharge(String laundryCharge, int money, int storeItemID, int itemGrade) {
            if (!TextUtils.isEmpty(laundryCharge) || laundryCharge != null) {
                llHowToWash.setVisibility(View.VISIBLE);
                laundryMoney = money;       //금액
                mLaundryCharge = laundryCharge + getResources().getString(R.string.comma) + money;
                tvLaundryCharge.setText(laundryCharge + " " + money);
                StoreItemID = storeItemID;  //StoreIdtemID
                mItemGrade = itemGrade;

                //화면 View변경 (세탁방법)
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                FragmentHowToWash fragmentHowToWash = new FragmentHowToWash(OnArticleSelectedListener);
                fragmentTransaction.replace(R.id.fragment_item_registation, fragmentHowToWash);
                fragmentTransaction.commit();
            } else {
                ToastUtil.showMakeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT);
            }
            setTotalAmonet();
        }

        @Override
        public void onHowToWash(String HowToWash) {
            if (!TextUtils.isEmpty(HowToWash) || HowToWash != null) {
                llDetail.setVisibility(View.VISIBLE);
                tvHowToWash.setText(HowToWash);

                if (HowToWash.equals("드라이, 다림질")) {
                    howtoWashType = 0;
                } else if (HowToWash.equals("물세탁, 다림질")) {
                    howtoWashType = 1;
                } else if (HowToWash.equals("드라이만")) {
                    howtoWashType = 2;
                }
            } else {
                ToastUtil.showMakeText(getApplicationContext(), getResources().getString(R.string.please_try_again), Toast.LENGTH_SHORT);
            }
        }

        @Override
        public void onRepairFee(ArrayList<ItemAdditionalData.ItemRepairFreeEdit> RepairFee) {
            if (RepairFee.size() == 0) {
                btnRepairFeeAdd.setText(getResources().getString(R.string.adding));

                rlRepairFree.setVisibility(View.GONE);
                repairAmount.clear();
            } else {
                listRepairFee = RepairFee;
                btnRepairFeeAdd.setText(getResources().getString(R.string.editing));
                rlRepairFree.setVisibility(View.VISIBLE);

                repairAmount = new ArrayList<>();
                for (ItemAdditionalData.ItemRepairFreeEdit itemRepairFree : RepairFee) {
                    repairAmount.add(itemRepairFree.getAmount());
                }

                AdapterItemRepair adapterItemRepair = new AdapterItemRepair(getApplicationContext());
                adapterItemRepair.addItem(RepairFee);
                lvRepairFee.setAdapter(adapterItemRepair);
                setListViewHeightBasedOnChildren(lvRepairFee); //동적 높이 조절
            }
            setTotalAmonet();
        }

        @Override
        public void onTechnicalFree(ArrayList<ItemAdditionalData.ItemTechnicalEdit> technicalFree) {
            if (technicalFree.size() == 0) {
                btnTechnicaFeeAdd.setText(getResources().getString(R.string.adding));
                rlTechnicalFree.setVisibility(View.GONE);
                technicalFreeAmount.clear();
            } else {
                listTechnicalFree = technicalFree;

                btnTechnicaFeeAdd.setText(getResources().getString(R.string.editing));
                rlTechnicalFree.setVisibility(View.VISIBLE);
                technicalFreeAmount = new ArrayList<>();
                for (ItemAdditionalData.ItemTechnicalEdit itemRepairFree : technicalFree) {
                    technicalFreeAmount.add(itemRepairFree.getAmount());
                }

                AdapterItemTechnicalFee adapterItemTechnicalFee = new AdapterItemTechnicalFee(getApplicationContext());
                adapterItemTechnicalFee.addItem(technicalFree);
                lvTechnicalFee.setAdapter(adapterItemTechnicalFee);
                setListViewHeightBasedOnChildren(lvTechnicalFee); //동적 높이 조절
            }
            setTotalAmonet();
        }

        @Override
        public void onPhotoAdd(String photo) {
            btnPhoto.setText(getResources().getString(R.string.editing));
        }

        @Override
        public void onComponent(ArrayList<ItemAdditionalData.ItemComponentEdit> component) {
            if (component.size() == 0) {
                btnComponent.setText(getResources().getString(R.string.adding));
                rlComponent.setVisibility(View.GONE);
                componentAmount.clear();
            } else {
                listComponent = component;
                btnComponent.setText(getResources().getString(R.string.editing));
                rlComponent.setVisibility(View.VISIBLE);

                componentAmount = new ArrayList<>();
                for (ItemAdditionalData.ItemComponentEdit itemComponent : component) {
                    componentAmount.add(itemComponent.getAmount());
                }
                AdapterItemComponent adapterItemComponent = new AdapterItemComponent(getApplicationContext());
                adapterItemComponent.addItem(component);
                lvViewComponent.setAdapter(adapterItemComponent);
                setListViewHeightBasedOnChildren(lvViewComponent); //동적 높이 조절
            }
            setTotalAmonet();
        }

        @Override
        public void onColor(int position) {
            btnColor.setText(getResources().getString(R.string.editing));
            llColor.setVisibility(View.VISIBLE);
            CommonUtil.setColorView(position, ivColor);

            positionColor = position;
        }

        @Override
        public void onMemo(ArrayList<String> memo) {
            if (memo.size() == 0) {
                btnMemo.setText(getResources().getString(R.string.adding));
                rlMemo.setVisibility(View.GONE);
            } else {
                StringBuffer memoName = new StringBuffer();
                for (int i = 0; i < memo.size(); i++) {
                    if (i == 0) {
                        if (memo.get(i) != null || !TextUtils.isEmpty(memo.get(i))) {
                            memoName.append(memo.get(i));
                        }
                    } else {
                        memoName.append("|");
                        memoName.append(memo.get(i));
                    }
                }
                listMemo = memoName;
                Logger.d(TAG, "listMemo : " + listMemo.toString());
                RecyclerView.LayoutManager layoutManager;
                rlMemo.setVisibility(View.VISIBLE);
                recycleViewMemo.setHasFixedSize(true);

                layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
                recycleViewMemo.setLayoutManager(layoutManager);

                AdapterItemMemo adapterMemo = new AdapterItemMemo(getApplicationContext(), mOnAdapterListners);
                adapterMemo.addItem(memo);
                recycleViewMemo.setAdapter(adapterMemo);
            }
        }
    };

    /**
     * 총 세탁 금액 View
     */
    private void setTotalAmonet() {
        int repairTotalMoney = 0;
        int technicalTotalMoney = 0;
        int componentTotalMoney = 0;

        try {
            if (repairAmount.size() != 0) {
                for (int i = 0; i < repairAmount.size(); i++) {
                    repairTotalMoney += Integer.parseInt(repairAmount.get(i));
                }
            }
            if (technicalFreeAmount.size() != 0) {
                for (int i = 0; i < technicalFreeAmount.size(); i++) {
                    technicalTotalMoney += Integer.parseInt(technicalFreeAmount.get(i));
                }
            }
            if (componentAmount.size() != 0) {
                for (int i = 0; i < componentAmount.size(); i++) {
                    componentTotalMoney += Integer.parseInt(componentAmount.get(i));
                }
            }

            totalMoeny = laundryMoney + repairTotalMoney + technicalTotalMoney + componentTotalMoney;
            tvLaundryMoney.setText(DataFormatUtil.moneyFormatToWon(totalMoeny));
            tvTotalMoney.setText(tvLaundryMoney.getText().toString());
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListners = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

    public interface onClickItem {
        void onClickItem(int position);

        void setCopyMoney(int moenyCount, int itemCount);

        void setDelete(int moenyCount, int itemCount);
    }

    public ActivityItemRegistration.onClickItem onClickItem = new ActivityItemRegistration.onClickItem() {
        @Override
        public void onClickItem(int position) {
            try {
                mType = "N";
                mPosition = position;
                setViewHinden(true);

                ArrayList<ItemAdditionalData> itemAdditionalData = ManagerItemAdditionalData.getInstance().getItemAdditionalData();
                btnAddItem.setText(getResources().getString(R.string.modifications_completed));

                if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
                    tvTacNumber.setText(CommonUtil.setTagFormater(itemAdditionalData.get(position).getTacNumber()));
                } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
                    tvTacNumber.setText(CommonUtil.setFormater(itemAdditionalData.get(position).getTacNumber()));
                } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
                    tvTacNumber.setText(CommonUtil.setEtcTagFormaterChangeResponse(itemAdditionalData.get(position).getTacNumber()));
                }

                tvItem.setText(itemAdditionalData.get(position).getItemName());
                tvLaundryCharge.setText(itemAdditionalData.get(position).getStoreItemName());

                tvTotalMoney.setText(DataFormatUtil.moneyFormatToWon(Integer.valueOf(itemAdditionalData.get(position).getTotalMoney())));
                tvLaundryMoney.setText(DataFormatUtil.moneyFormatToWon(Integer.valueOf(itemAdditionalData.get(position).getTotalMoney())));

                if (itemAdditionalData.get(position).getHowToWash() == 1) {
                    tvHowToWash.setText("드라이+다림질");
                } else if (itemAdditionalData.get(position).getHowToWash() == 2) {
                    tvHowToWash.setText("물세탁+다림질");
                } else if (itemAdditionalData.get(position).getHowToWash() == 3) {
                    tvHowToWash.setText("다림질");
                }

                //수선요금 수정 VIEW
                if (itemAdditionalData.get(position).getOptionTypeRepairFee().size() > 0) {
                    btnRepairFeeAdd.setText(getResources().getString(R.string.editing));
                    rlRepairFree.setVisibility(View.VISIBLE);
                    listRepairFee = itemAdditionalData.get(position).getOptionTypeRepairFee();

                    repairAmount = new ArrayList<>();
                    for (ItemAdditionalData.ItemRepairFreeEdit itemRepairFree : itemAdditionalData.get(position).getOptionTypeRepairFee()) {
                        repairAmount.add(itemRepairFree.getAmount());
                    }

                    AdapterItemRepair adapterItemRepair = new AdapterItemRepair(getApplicationContext());
                    adapterItemRepair.addItem(itemAdditionalData.get(position).getOptionTypeRepairFee());
                    lvRepairFee.setAdapter(adapterItemRepair);
                    setListViewHeightBasedOnChildren(lvRepairFee); //동적 높이 조절
                } else {
                    rlRepairFree.setVisibility(View.GONE);
                    btnRepairFeeAdd.setText(getResources().getString(R.string.adding));
                    repairAmount.clear();
                }

                //기술요금 수정 VIEW
                if (itemAdditionalData.get(position).getOptionTechnicalFree().size() > 0) {
                    btnTechnicaFeeAdd.setText(getResources().getString(R.string.editing));
                    rlTechnicalFree.setVisibility(View.VISIBLE);
                    technicalFreeAmount = new ArrayList<>();
                    listTechnicalFree = itemAdditionalData.get(position).getOptionTechnicalFree();

                    technicalFreeAmount = new ArrayList<>();
                    for (ItemAdditionalData.ItemTechnicalEdit itemRepairFree : itemAdditionalData.get(position).getOptionTechnicalFree()) {
                        technicalFreeAmount.add(itemRepairFree.getAmount());
                    }

                    AdapterItemTechnicalFee adapterItemTechnicalFee = new AdapterItemTechnicalFee(getApplicationContext());
                    adapterItemTechnicalFee.addItem(itemAdditionalData.get(position).getOptionTechnicalFree());
                    lvTechnicalFee.setAdapter(adapterItemTechnicalFee);
                    setListViewHeightBasedOnChildren(lvTechnicalFee); //동적 높이 조절
                } else {
                    rlTechnicalFree.setVisibility(View.GONE);
                    btnTechnicaFeeAdd.setText(getResources().getString(R.string.adding));
                    technicalFreeAmount.clear();
                }

                //메모 수정 VIEW
                if (!TextUtils.isEmpty(itemAdditionalData.get(position).getMemoData()) || itemAdditionalData.get(position).getMemoData() != null || itemAdditionalData.get(position).getMemoData().equals("") || itemAdditionalData.get(position).getMemoData().length() != 0) {
                    rlMemo.setVisibility(View.VISIBLE);
                    recycleViewMemo.setHasFixedSize(true);
                    ArrayList<String> memoDataList = new ArrayList<>();

                    RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
                    recycleViewMemo.setLayoutManager(layoutManager);
                    AdapterItemMemo adapterMemo = new AdapterItemMemo(getApplicationContext(), mOnAdapterListners);
                    String[] array = itemAdditionalData.get(position).getMemoData().split("\\|");

                    for (String meno : array) {
                        memoDataList.add(meno);
                    }

                    StringBuffer memoName = new StringBuffer();
                    for (int i = 0; i < memoDataList.size(); i++) {
                        if (i == 0) {
                            if (memoDataList.get(i) != null || !TextUtils.isEmpty(memoDataList.get(i)) || !memoDataList.get(i).equals("")) {
                                memoName.append(memoDataList.get(i));
                            }
                        } else {
                            memoName.append("|");
                            memoName.append(memoDataList.get(i));
                        }

                        listMemo = memoName;
                        adapterMemo.addItem(memoDataList);
                        recycleViewMemo.setAdapter(adapterMemo);
                    }
                } else {
                    rlMemo.setVisibility(View.GONE);
                }

                //부속품요금 수정 VIEW
                if (itemAdditionalData.get(position).getOptionTypeComponent().size() > 0) {
                    btnComponent.setText(getResources().getString(R.string.editing));
                    rlComponent.setVisibility(View.VISIBLE);
                    listComponent = itemAdditionalData.get(position).getOptionTypeComponent();

                    componentAmount = new ArrayList<>();
                    for (ItemAdditionalData.ItemComponentEdit itemComponent : itemAdditionalData.get(position).getOptionTypeComponent()) {
                        componentAmount.add(itemComponent.getAmount());
                    }

                    AdapterItemComponent adapterItemComponent = new AdapterItemComponent(getApplicationContext());
                    adapterItemComponent.addItem(itemAdditionalData.get(position).getOptionTypeComponent());
                    lvViewComponent.setAdapter(adapterItemComponent);
                    setListViewHeightBasedOnChildren(lvViewComponent); //동적 높이 조절
                } else {
                    rlComponent.setVisibility(View.GONE);
                    btnComponent.setText(getResources().getString(R.string.adding));
                    componentAmount.clear();
                }

                if (itemAdditionalData.get(position).getFreeType() == 1) {
                    checkStopLaundry.setBackgroundResource(R.drawable.a_button_type_2);
                    checkStopLaundry.setTextColor(getResources().getColor(R.color.Wite_color));

                    checkReWashing.setBackgroundResource(R.drawable.a_button_type_3);
                    checkReWashing.setTextColor(getResources().getColor(R.color.color_43425c));

                    checkLaundryFree.setBackgroundResource(R.drawable.a_button_type_3);
                    checkLaundryFree.setTextColor(getResources().getColor(R.color.color_43425c));
                } else if (itemAdditionalData.get(position).getFreeType() == 2) {
                    checkStopLaundry.setBackgroundResource(R.drawable.a_button_type_3);
                    checkStopLaundry.setTextColor(getResources().getColor(R.color.color_43425c));

                    checkReWashing.setBackgroundResource(R.drawable.a_button_type_2);
                    checkReWashing.setTextColor(getResources().getColor(R.color.Wite_color));

                    checkLaundryFree.setBackgroundResource(R.drawable.a_button_type_3);
                    checkLaundryFree.setTextColor(getResources().getColor(R.color.color_43425c));
                } else if (itemAdditionalData.get(position).getFreeType() == 3) {
                    checkStopLaundry.setBackgroundResource(R.drawable.a_button_type_3);
                    checkStopLaundry.setTextColor(getResources().getColor(R.color.color_43425c));

                    checkReWashing.setBackgroundResource(R.drawable.a_button_type_3);
                    checkReWashing.setTextColor(getResources().getColor(R.color.color_43425c));

                    checkLaundryFree.setBackgroundResource(R.drawable.a_button_type_2);
                    checkLaundryFree.setTextColor(getResources().getColor(R.color.Wite_color));
                }
                CommonUtil.setColorView(itemAdditionalData.get(position).getRgbColor(), ivColor);
                setTotalAmonet();
            } catch (Exception err) {
                err.printStackTrace();
            }
        }

        @Override
        public void setCopyMoney(int moenyCount, int itemCount) {
            tvItemListTotalMoeny.setText(DataFormatUtil.moneyFormatToWon(moenyCount));
            tvItemCount.setText(String.valueOf(itemCount));
            setTacNumberView();
        }

        @Override
        public void setDelete(int moenyCount, int itemCount) {
            tvItemListTotalMoeny.setText(DataFormatUtil.moneyFormatToWon(moenyCount));
            tvItemCount.setText(String.valueOf(itemCount));

        }
    };

    /**
     * 상태에 따른 View Visible / Gone
     *
     * @param status
     */
    private void setViewHinden(boolean status) {
        if (status) {
            llLaundryCharge.setVisibility(View.VISIBLE);
            llHowToWash.setVisibility(View.VISIBLE);
            llDetail.setVisibility(View.VISIBLE);
            llColor.setVisibility(View.VISIBLE);

            btnTechnicaFeeAdd.setText("수정하기");
            btnRepairFeeAdd.setText("수정하기");
            btnPhoto.setText("수정하기");
            btnMemo.setText("수정하기");
            btnComponent.setText("수정하기");
            btnColor.setText("수정하기");

        } else {
            btnTechnicaFeeAdd.setText("추가하기");
            btnRepairFeeAdd.setText("추가하기");
            btnPhoto.setText("추가하기");
            btnMemo.setText("추가하기");
            btnComponent.setText("추가하기");
            btnColor.setText("추가하기");

            llLaundryCharge.setVisibility(View.GONE);
            llHowToWash.setVisibility(View.GONE);
            llDetail.setVisibility(View.GONE);
            rlRepairFree.setVisibility(View.GONE);
            rlTechnicalFree.setVisibility(View.GONE);
            rlComponent.setVisibility(View.GONE);
            llColor.setVisibility(View.GONE);
            rlMemo.setVisibility(View.GONE);
        }
    }

    /**
     * 입력 데이터 초기화
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}