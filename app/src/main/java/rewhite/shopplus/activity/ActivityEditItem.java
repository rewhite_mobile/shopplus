package rewhite.shopplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.manager.ManagerOrderItemList;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.model.GetOrderItemList;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.common.popup.keyboard.DialogCustomKeyboardTypeB;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.data.dto.ItemAddDataEdit;
import rewhite.shopplus.data.dto.ItemAdditionalDataEdit;
import rewhite.shopplus.data.dto.ItemAdditionalDataEditType;
import rewhite.shopplus.data.dto.ItemComponent;
import rewhite.shopplus.data.dto.ItemRepairFree;
import rewhite.shopplus.data.dto.ItemTechnical;
import rewhite.shopplus.data.manager.ManagerComponentEdit;
import rewhite.shopplus.data.manager.ManagerItemAdditionalDataEdit;
import rewhite.shopplus.data.manager.ManagerItemAdditionalDataEditType;
import rewhite.shopplus.data.manager.ManagerMemo;
import rewhite.shopplus.data.manager.ManagerRepairFreeEdit;
import rewhite.shopplus.data.manager.ManagerTechnicalFeeEdit;
import rewhite.shopplus.fragment.sub.FragmentEditItemCategory;
import rewhite.shopplus.fragment.sub.FragmentEditItemComponent;
import rewhite.shopplus.fragment.sub.FragmentEditItemHowToWash;
import rewhite.shopplus.fragment.sub.FragmentEditItemLaundryCharge;
import rewhite.shopplus.fragment.sub.FragmentEditItemMemo;
import rewhite.shopplus.fragment.sub.FragmentEditItemRepairFee;
import rewhite.shopplus.fragment.sub.FragmentEditItemTechnicalFee;
import rewhite.shopplus.fragment.sub.FragmentEditeItemColor;
import rewhite.shopplus.fragment.sub.FragmentLaundryCharge;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.SharedPreferencesUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterComponent;
import rewhite.shopplus.view.adapter.AdapterEditItem;
import rewhite.shopplus.view.adapter.AdapterItemComponentEdit;
import rewhite.shopplus.view.adapter.AdapterItemMemo;
import rewhite.shopplus.view.adapter.AdapterItemRepairEdit;
import rewhite.shopplus.view.adapter.AdapterItemTechnicalFeeEdit;

import static rewhite.shopplus.util.CommonUtil.setListViewHeightBasedOnChildren;

/**
 * 품목수정 Activity
 */
public class ActivityEditItem extends AppCompatActivity {
    private static final String TAG = "ActivityEditItem";
    private static final String ORDER_ID = "order_id";  //주문ID
    private static final String BEFORE_CHANGE_MONEY = "before_change_money";    //총 주문 금액

    private List<GetOrderItemList> orderItemList;

    private ArrayList<String> repairAmount = new ArrayList<>();
    private ArrayList<String> technicalFreeAmount = new ArrayList<>();
    private ArrayList<String> componentAmount = new ArrayList<>();
    private AdapterEditItem adapterEditItem;
    private StringBuffer listMemo = new StringBuffer();

    private int mTotalMoeny, totalMoeny;
    private int orderId = 0;
    private int mCategoryId;
    private int mPosition = 0;
    private int howtoWashType = 99;  //서버 데이터 전송 세탁방법 Type
    private int StoreItemID;
    private int mItemGrade = 99;         //상품등급 (1:일반,2:명품,3:아동)
    private int toTalMoneyCount = 0;
    private int mTotalAmount;
    private String mItemName = "";
    private WaitCounter mWait;

    private int positionColor = 99;
    private int laundryMoney;
    private ListView listView;
    private ListView lvRepairFee, lvViewComponent, lvTechnicalFee;
    private String mLaundryCharge = "";

    private String mSelected, mTagNumber;
    private TextView tvUserName, tvUserPhoneNumber, tvUserAddress;
    private TextView tvItemListTotalMoeny, tvItemCount;

    private TextView tvTacNumber, tvItem, tvLaundryCharge, tvHowToWash, tvTotalMoney, tvLaundryMoney, tvPayment;
    private RelativeLayout rlRepairFree, rlTechnicalFree, rlMemo, rlComponent;

    private CheckBox chStopLaundry, chReWashing, chLaundryFree;
    private ImageView ivColor, imgTagColor;

    private Button btnRepairFeeAdd, btnTechnicalFeeAdd, btnMemoAdd, btnComponentAdd, btnColorAdd, btnTotalMoneyEdit, btnEditAddItem, btnReceipt;
    private Button btnClose;
    private RecyclerView recycleViewMemo;

    private FrameLayout fragmentItemRegistation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);
        mWait = new WaitCounter(this);
        ManagerItemAdditionalDataEdit.getInstance().setReairFreeDelete();

        Intent intent = getIntent();

        orderId = intent.getExtras().getInt(ORDER_ID);
        init();
    }

    /**
     * 데이터 바인딩
     */
    private void init() {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    ManagerOrderItemList managerOrderItemList = LaundryRegistrationManager.getManagerOrderItemList(ActivityEditItem.this, orderId);
                    LaundryRegistrationManager.getManagerStoreUserList(ActivityEditItem.this, 1);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                            orderItemList = managerOrderItemList.getManagerAreaCity();

                            setLayout();

                            adapterEditItem = new AdapterEditItem(getApplicationContext(), selectPosition);
                            adapterEditItem.addItem(orderItemList);
                            listView.setAdapter(adapterEditItem);

                            setTextView();       // 텍스트 셋팅 View
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            mTotalMoeny = Integer.valueOf(SharedPreferencesUtil.getStringSharedPreference(getApplicationContext(), PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY));
            tvTotalMoney.setText(DataFormatUtil.moneyFormatToWon(mTotalMoeny) + "원");
        } catch (Exception err) {
            err.printStackTrace();
        }

        SharedPreferencesUtil.removeSharedPreference(getApplicationContext(), PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY);

    }

    /**
     * 레이아웃 셋팅 View
     */
    private void setLayout() {
        listView = (ListView) findViewById(R.id.lv_registation);
        tvUserName = (TextView) findViewById(R.id.tv_user_name);
        tvUserPhoneNumber = (TextView) findViewById(R.id.tv_user_phoneNumber);
        tvUserAddress = (TextView) findViewById(R.id.tv_user_address);
        tvItemListTotalMoeny = (TextView) findViewById(R.id.tv_item_list_total_moeny);
        tvItemCount = (TextView) findViewById(R.id.tv_item_count);

        tvTotalMoney = (TextView) findViewById(R.id.tv_total_money);
        tvLaundryMoney = (TextView) findViewById(R.id.tv_laundry_money);
        tvPayment = (TextView) findViewById(R.id.tv_payment);

        tvTacNumber = (TextView) findViewById(R.id.tv_tac_number);
        tvItem = (TextView) findViewById(R.id.tv_edit_item);
        tvLaundryCharge = (TextView) findViewById(R.id.tv_laundry_charge);
        tvHowToWash = (TextView) findViewById(R.id.tv_how_to_wash);

        lvRepairFee = (ListView) findViewById(R.id.lv_repair_fee);
        lvViewComponent = (ListView) findViewById(R.id.lv_view_component);
        lvTechnicalFee = (ListView) findViewById(R.id.lv_technical_fee);

        rlRepairFree = (RelativeLayout) findViewById(R.id.rl_repair_free);

        rlTechnicalFree = (RelativeLayout) findViewById(R.id.rl_technical_free);
        rlMemo = (RelativeLayout) findViewById(R.id.rl_memo);
        rlComponent = (RelativeLayout) findViewById(R.id.rl_component);

        chStopLaundry = (CheckBox) findViewById(R.id.ch_stop_laundry);
        chReWashing = (CheckBox) findViewById(R.id.ch_re_washing);
        chLaundryFree = (CheckBox) findViewById(R.id.ch_laundry_free);

        ivColor = (ImageView) findViewById(R.id.iv_color);
        imgTagColor = (ImageView) findViewById(R.id.img_tag_color);

        btnClose = (Button) findViewById(R.id.btn_close);
        btnRepairFeeAdd = (Button) findViewById(R.id.btn_repair_fee_add);
        btnTechnicalFeeAdd = (Button) findViewById(R.id.btn_technical_fee_add);
        btnMemoAdd = (Button) findViewById(R.id.btn_memo_add);
        btnComponentAdd = (Button) findViewById(R.id.btn_component_add);
        btnColorAdd = (Button) findViewById(R.id.btn_color_add);
        btnTotalMoneyEdit = (Button) findViewById(R.id.btn_total_money_edit);
        btnEditAddItem = (Button) findViewById(R.id.btn_add_item);
        btnReceipt = (Button) findViewById(R.id.btn_receipt);

        fragmentItemRegistation = (FrameLayout) findViewById(R.id.fragment_item_registation);
        recycleViewMemo = (RecyclerView) findViewById(R.id.recycle_view_memo);


        setItemdetailTextView(); // 품목수정하기 상세TextView Setting
        setClickView();          // Click View
        setAdapterView();        //수선 / 부속품 / 추가기술 Adapter 셋팅
        setInitialSetting();     //초기 셋팅
        setTotalAmonet();        //금액 셋팅
    }

    /**
     * 텍스트 셋팅 View
     */
    private void setTextView() {
        try {
            tvUserName.setText(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName());
            tvUserPhoneNumber.setText(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserPhone());
            tvUserAddress.setText(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserAddress());
            tvItemCount.setText(String.valueOf(orderItemList.size()));
            for (GetOrderItemList getOrderItemList : orderItemList) {
                toTalMoneyCount += getOrderItemList.getOrderItemPrice();
            }
            tvItemListTotalMoeny.setText(DataFormatUtil.moneyFormatToWon(toTalMoneyCount));
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * 품목수정하기 상세TextView Setting
     */
    private void setItemdetailTextView() {
        int idx = orderItemList.get(mPosition).getTagNo().indexOf("|");

        try {
            if (idx == 0) {
                tvTacNumber.setText(CommonUtil.setTagFormater(orderItemList.get(mPosition).getTagNo()));
                mTagNumber = CommonUtil.setTagFormater(orderItemList.get(mPosition).getTagNo());
            } else if (orderItemList.get(mPosition).getTagNo().length() >= 6 && orderItemList.get(mPosition).getTagNo().length() < 9) {
                tvTacNumber.setText(CommonUtil.setFormater(orderItemList.get(mPosition).getTagNo()));
                mTagNumber = CommonUtil.setTagFormater(orderItemList.get(mPosition).getTagNo());
            } else if (orderItemList.get(mPosition).getTagNo().contains("-")) {
                tvTacNumber.setText(CommonUtil.setTagFormaterChange(orderItemList.get(mPosition).getTagNo()));
                mTagNumber = CommonUtil.setTagFormater(orderItemList.get(mPosition).getTagNo());
            }
        } catch (Exception err) {
            err.printStackTrace();
        }
        tvItem.setText(orderItemList.get(mPosition).getStoreItemName());
        tvLaundryCharge.setText(orderItemList.get(mPosition).getItemGradeDesc() + "(" + getItemGrade() + ")" + DataFormatUtil.moneyFormatToWon(orderItemList.get(mPosition).getStoreItemPrice()) + "원");
        tvHowToWash.setText(getWashType());
        CommonUtil.setColorView(Integer.valueOf(orderItemList.get(mPosition).getItemColor()), ivColor);
        CommonUtil.setTacImgColor(Integer.valueOf(orderItemList.get(mPosition).getTagColor()), imgTagColor);

        if (orderItemList.get(mPosition).getPayPrice() >= 0) {
            mSelected = "N";
        } else {
            mSelected = "Y";
        }

        setOtherOptions();  //기타옵션 선택
    }


    private void setClickView() {
        //결제 안된경우
        if (orderItemList.get(0).getNonPayPrice() > 0) {
            tvItem.setOnClickListener(btnOnClickListener);
            tvLaundryCharge.setOnClickListener(btnOnClickListener);
            tvHowToWash.setOnClickListener(btnOnClickListener);
        }
        btnRepairFeeAdd.setOnClickListener(btnOnClickListener);
        btnTechnicalFeeAdd.setOnClickListener(btnOnClickListener);
        btnMemoAdd.setOnClickListener(btnOnClickListener);
        btnComponentAdd.setOnClickListener(btnOnClickListener);
        btnColorAdd.setOnClickListener(btnOnClickListener);
        btnTotalMoneyEdit.setOnClickListener(btnOnClickListener);
        btnEditAddItem.setOnClickListener(btnOnClickListener);
        btnReceipt.setOnClickListener(btnOnClickListener);
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            ArrayList<ItemRepairFree> RepairFee = new ArrayList<>();
            ArrayList<ItemTechnical> itemTechnicals = new ArrayList<>();
            ArrayList<ItemComponent> itemComponents = new ArrayList<>();

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            switch (id) {
                case R.id.tv_item:                   //카테고리 품목 선택 FragmentView
                    if (orderItemList.get(mPosition).getPayPrice() == 0) {
                        fragmentItemRegistation.setVisibility(View.VISIBLE);
                        FragmentEditItemCategory fragmentEditItemCategory = new FragmentEditItemCategory(OnArticleSelectedListener);
                        fragmentTransaction.replace(R.id.fragment_item_registation, fragmentEditItemCategory);
                        fragmentTransaction.commit();
                    }
                    break;

                case R.id.tv_laundry_charge:        //세탁요금 선택 FragmentView
                    if (orderItemList.get(mPosition).getPayPrice() == 0) {
                        fragmentItemRegistation.setVisibility(View.VISIBLE);
                        FragmentEditItemLaundryCharge fragmentEditItemLaundryCharge = new FragmentEditItemLaundryCharge(OnArticleSelectedListener);
                        FragmentLaundryCharge.setLaundryName(tvItem.getText().toString(), 1);  //선택된 품목 문자넘김
                        fragmentTransaction.replace(R.id.fragment_item_registation, fragmentEditItemLaundryCharge);
                        fragmentTransaction.commit();
                    }
                    break;
                case R.id.tv_how_to_wash:           //세탁방법 선택 FragmentView
                    if (orderItemList.get(mPosition).getPayPrice() == 0) {
                        fragmentItemRegistation.setVisibility(View.VISIBLE);
                        FragmentEditItemHowToWash fragmentEditItemHowToWash = new FragmentEditItemHowToWash(OnArticleSelectedListener);
                        fragmentTransaction.replace(R.id.fragment_item_registation, fragmentEditItemHowToWash);
                        fragmentTransaction.commit();
                    }
                    break;

                case R.id.btn_repair_fee_add:       //수선요금 FragmentView
                    for (GetOrderItemList.OrderItemOption orderItemOption : orderItemList.get(mPosition).getOptions()) {
                        if (orderItemOption.getOptionType() == 3) {

                            ItemRepairFree itemRepairFree = new ItemRepairFree();

                            itemRepairFree.setTitleName(orderItemOption.getOptionItemName());
                            itemRepairFree.setAmount(String.valueOf(orderItemOption.getOptionPrice()));
                            itemRepairFree.setOptionItemID(String.valueOf(orderItemOption.getOptionItemID()));
                            RepairFee.add(itemRepairFree);

                            ManagerRepairFreeEdit.getInstance().setRepairFeeDates(RepairFee);
                        }
                    }
                    fragmentItemRegistation.setVisibility(View.VISIBLE);

                    FragmentEditItemRepairFee fragmentRepairFee = new FragmentEditItemRepairFee(OnArticleSelectedListener);
                    fragmentRepairFee.setSelected(mSelected);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentRepairFee);
                    fragmentTransaction.commit();
                    break;

                case R.id.btn_technical_fee_add:    //기술요금 FragmentView
                    for (GetOrderItemList.OrderItemOption orderItemOption : orderItemList.get(mPosition).getOptions()) {
                        if (orderItemOption.getOptionType() == 1) {

                            ItemTechnical itemRepairFree = new ItemTechnical();

                            itemRepairFree.setTitleName(orderItemOption.getOptionItemName());
                            itemRepairFree.setAmount(String.valueOf(orderItemOption.getOptionPrice()));
                            itemRepairFree.setOptionItemID(String.valueOf(orderItemOption.getOptionItemID()));
                            itemTechnicals.add(itemRepairFree);

                            ManagerTechnicalFeeEdit.getInstance().setTechnicalFee(itemTechnicals);
                        }
                    }
                    fragmentItemRegistation.setVisibility(View.VISIBLE);
                    FragmentEditItemTechnicalFee fragmentEditItemTechnicalFee = new FragmentEditItemTechnicalFee(OnArticleSelectedListener);
                    fragmentEditItemTechnicalFee.setSelected(mSelected);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentEditItemTechnicalFee);
                    fragmentTransaction.commit();
                    break;

                case R.id.btn_memo_add:             //메모 FragmentView
                    if (!TextUtils.isEmpty(orderItemList.get(mPosition).getOrderItemMemo()) || orderItemList.get(mPosition).getOrderItemMemo() != null) {
                        ArrayList<String> memoDataList = new ArrayList<>();
                        String[] array = orderItemList.get(mPosition).getOrderItemMemo().split("\\|");
                        for (String meno : array) {
                            memoDataList.add(meno);
                        }
                        ManagerMemo.getInstance().setTManagerMemo(memoDataList);
                    }
                    fragmentItemRegistation.setVisibility(View.VISIBLE);
                    FragmentEditItemMemo fragmentEditItemMemo = new FragmentEditItemMemo(OnArticleSelectedListener);
                    fragmentEditItemMemo.setSeelctedItem(mSelected);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentEditItemMemo);
                    fragmentTransaction.commit();
                    break;

                case R.id.btn_component_add:        //부속품 FragmentView
                    for (GetOrderItemList.OrderItemOption orderItemOption : orderItemList.get(mPosition).getOptions()) {
                        if (orderItemOption.getOptionType() == 2) {

                            ItemComponent itemComponent = new ItemComponent();

                            itemComponent.setTitleName(orderItemOption.getOptionItemName());
                            itemComponent.setAmount(String.valueOf(orderItemOption.getOptionPrice()));
                            itemComponent.setOptionItemID(String.valueOf(orderItemOption.getOptionItemID()));

                            itemComponents.add(itemComponent);

                            ManagerComponentEdit.getInstance().setComponent(itemComponents);
                        }
                    }
                    fragmentItemRegistation.setVisibility(View.VISIBLE);
                    FragmentEditItemComponent fragmentEditItemComponent = new FragmentEditItemComponent(OnArticleSelectedListener);
                    fragmentEditItemComponent.setSelected(mSelected);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentEditItemComponent);
                    fragmentTransaction.commit();
                    break;

                case R.id.btn_color_add:            //색상 FragmentView
                    FragmentEditeItemColor fragmentEditeItemColor = new FragmentEditeItemColor(OnArticleSelectedListener);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentEditeItemColor);
                    fragmentTransaction.commit();
                    break;

                case R.id.btn_total_money_edit: //금액 수정 Dialog 출력 View
                    Intent intent = new Intent(getApplicationContext(), DialogCustomKeyboardTypeB.class);
                    intent.putExtra(BEFORE_CHANGE_MONEY, mTotalMoeny);
                    startActivity(intent);
                    break;

                case R.id.btn_add_item: //수정된 품목 등록
                    setItemEditAddData();
                    break;

                case R.id.btn_receipt:
                    setServerRequestData();
                    break;

                case R.id.btn_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityEditItem.this);
                    thirdConfirmPopup.setTitle("종료 팝업");
                    thirdConfirmPopup.setContent("품목수정을 종료할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setCancelButtonText("아니요");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            finish();
                        }
                    });
                    break;
            }
        }
    };

    /**
     * Fragment Select Item Data Setting
     */
    public interface OnArticleSelectedListener {
        public void onItemSelected(String name, int categoryId);              //품목명

        public void onLaundryCharge(String laundryCharge, int money, int storeItemID, int itemGrade);         //세탁요금

        public void onHowToWash(String HowToWash);                            //세탁방법

        public void onRepairFee(ArrayList<ItemRepairFree> RepairFee);         //수선요금

        public void onTechnicalFree(ArrayList<ItemTechnical> technicalFree); //기술요금

        public void onPhotoAdd(String photo);                                 //사진촬영

        public void onComponent(ArrayList<ItemComponent> component);          //부속품

        public void onColor(int position);                                    //색상

        public void onMemo(ArrayList<String> memo);                           //메모
    }

    public ActivityEditItem.OnArticleSelectedListener OnArticleSelectedListener = new ActivityEditItem.OnArticleSelectedListener() {

        @Override
        public void onItemSelected(String name, int categoryId) {
            mItemName = name;
            mCategoryId = categoryId;
        }

        @Override
        public void onLaundryCharge(String laundryCharge, int money, int storeItemID, int itemGrade) {
            laundryMoney = money;       //금액
            mLaundryCharge = laundryCharge + " " + money;
            tvLaundryCharge.setText(laundryCharge + " " + money);
            StoreItemID = storeItemID;  //StoreIdtemID
            mItemGrade = itemGrade;
        }

        @Override
        public void onHowToWash(String HowToWash) {
            if (!TextUtils.isEmpty(HowToWash) || HowToWash != null) {
                tvHowToWash.setText(HowToWash);
                if (HowToWash.equals("드라이+다림질")) {
                    howtoWashType = 0;
                } else if (HowToWash.equals("물세탁+다림질")) {
                    howtoWashType = 1;
                } else if (HowToWash.equals("드라이만")) {
                    howtoWashType = 2;
                }
            }
        }

        @Override
        public void onRepairFee(ArrayList<ItemRepairFree> repairFee) {
            if (repairFee.size() == 0) {
                rlRepairFree.setVisibility(View.GONE);
                repairAmount.clear();
            } else {
                repairAmount.clear();
                rlRepairFree.setVisibility(View.VISIBLE);

                for (ItemRepairFree itemRepairFree : repairFee) {
                    repairAmount.add(itemRepairFree.getAmount());
                }

                setTotalAmonet();
                AdapterItemRepairEdit adapterItemRepair = new AdapterItemRepairEdit(getApplicationContext());
                adapterItemRepair.addItem(repairFee);
                lvRepairFee.setAdapter(adapterItemRepair);
                setListViewHeightBasedOnChildren(lvRepairFee); //동적 높이 조절
            }
        }

        @Override
        public void onTechnicalFree(ArrayList<ItemTechnical> technicalFree) {
            if (technicalFree.size() == 0) {
                rlTechnicalFree.setVisibility(View.GONE);
                technicalFreeAmount.clear();
            } else {
                technicalFreeAmount.clear();
                rlTechnicalFree.setVisibility(View.VISIBLE);

                for (ItemTechnical itemTechnical : technicalFree) {
                    technicalFreeAmount.add(itemTechnical.getAmount());
                }
                setTotalAmonet();
                AdapterItemTechnicalFeeEdit adapterItemTechnicalFee = new AdapterItemTechnicalFeeEdit(getApplicationContext());
                adapterItemTechnicalFee.addItem(technicalFree);
                lvTechnicalFee.setAdapter(adapterItemTechnicalFee);
                setListViewHeightBasedOnChildren(lvTechnicalFee); //동적 높이 조절
            }
        }

        @Override
        public void onPhotoAdd(String photo) {

        }

        @Override
        public void onComponent(ArrayList<ItemComponent> component) {

            if (component.size() == 0) {
                rlComponent.setVisibility(View.GONE);
                componentAmount.clear();
            } else {
                componentAmount.clear();
                rlComponent.setVisibility(View.VISIBLE);
                for (ItemComponent itemComponent : component) {
                    componentAmount.add(itemComponent.getAmount());
                }

                setTotalAmonet();
                AdapterItemComponentEdit adapterItemComponent = new AdapterItemComponentEdit(getApplicationContext());
                adapterItemComponent.addItem(component);
                lvViewComponent.setAdapter(adapterItemComponent);
                setListViewHeightBasedOnChildren(lvViewComponent); //동적 높이 조절
            }
        }

        @Override
        public void onColor(int position) {
            CommonUtil.setColorView(position, ivColor);
            positionColor = position;
        }

        @Override
        public void onMemo(ArrayList<String> memo) {
            if (memo.size() == 0) {

            } else {
                StringBuffer memoName = new StringBuffer();
                for (int i = 0; i < memo.size(); i++) {
                    if (i == 0) {
                        if (memo.get(i) != null || !TextUtils.isEmpty(memo.get(i))) {
                            memoName.append(memo.get(i));
                        }
                    } else {
                        memoName.append("|");
                        memoName.append(memo.get(i));
                    }
                }
                listMemo = memoName;
                RecyclerView.LayoutManager layoutManager;
                rlMemo.setVisibility(View.VISIBLE);
                recycleViewMemo.setHasFixedSize(true);

                layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
                recycleViewMemo.setLayoutManager(layoutManager);

                AdapterItemMemo adapterMemo = new AdapterItemMemo(getApplicationContext(), mOnAdapterListners);
                adapterMemo.addItem(memo);
                recycleViewMemo.setAdapter(adapterMemo);
            }
        }
    };

    /**
     * 수선 / 부속품 / 추가기술 AdapterView Setting
     */
    private void setAdapterView() {
        ArrayList<ItemTechnical> ItemTechnical = new ArrayList<>();
        ArrayList<ItemComponent> itemComponents = new ArrayList<>();
        ArrayList<ItemRepairFree> RepairFee = new ArrayList<>();

        if (orderItemList.get(mPosition).getOptions().size() != 0) {
            for (GetOrderItemList.OrderItemOption orderItemOption : orderItemList.get(mPosition).getOptions()) {

                if (orderItemOption.getOptionType() != 1) {
                    rlTechnicalFree.setVisibility(View.GONE);
                }
                if (orderItemOption.getOptionType() != 2) {
                    rlComponent.setVisibility(View.GONE);
                }
                if (orderItemOption.getOptionType() != 3) {
                    rlRepairFree.setVisibility(View.GONE);
                }

                //추가기술
                if (orderItemOption.getOptionType() == 1) {
                    Logger.d(TAG, "추가기술 존재함");
                    rlTechnicalFree.setVisibility(View.VISIBLE);
                    ItemTechnical itemRepairFree = new ItemTechnical();

                    AdapterItemTechnicalFeeEdit adapterItemTechnicalFee = new AdapterItemTechnicalFeeEdit(getApplicationContext());

                    //아이템 추가
                    itemRepairFree.setTitleName(orderItemOption.getOptionItemName());
                    itemRepairFree.setOptionItemID(String.valueOf(orderItemOption.getOptionItemID()));
                    itemRepairFree.setAmount(String.valueOf(orderItemOption.getOptionPrice()));
                    ItemTechnical.add(itemRepairFree);

                    technicalFreeAmount.add(String.valueOf(orderItemOption.getOptionPrice()));

                    adapterItemTechnicalFee.addItem(ItemTechnical);
                    lvTechnicalFee.setAdapter(adapterItemTechnicalFee);
                    setListViewHeightBasedOnChildren(lvTechnicalFee); //동적 높이 조절

                    ManagerTechnicalFeeEdit.getInstance().setTechnicalFee(ItemTechnical);

                    //부속품
                } else if (orderItemOption.getOptionType() == 2) {
                    Logger.d(TAG, "부속품 존재함");
                    rlComponent.setVisibility(View.VISIBLE);

                    ItemComponent itemComponent = new ItemComponent();

                    AdapterItemComponentEdit adapterItemComponent = new AdapterItemComponentEdit(getApplicationContext());

                    itemComponent.setTitleName(orderItemOption.getOptionItemName());
                    itemComponent.setOptionItemID(String.valueOf(orderItemOption.getOptionItemID()));
                    itemComponent.setAmount(String.valueOf(orderItemOption.getOptionPrice()));
                    itemComponents.add(itemComponent);

                    componentAmount.add(String.valueOf(orderItemOption.getOptionPrice()));

                    adapterItemComponent.addItem(itemComponents);
                    lvViewComponent.setAdapter(adapterItemComponent);
                    setListViewHeightBasedOnChildren(lvViewComponent); //동적 높이 조절

                    ManagerComponentEdit.getInstance().setComponent(itemComponents); //품목등록시 선택된 ItemData Setting
                    //수선
                } else if (orderItemOption.getOptionType() == 3) {
                    Logger.d(TAG, "수선 존재함");
                    rlRepairFree.setVisibility(View.VISIBLE);
                    ItemRepairFree itemRepairFree1 = new ItemRepairFree();

                    AdapterItemRepairEdit adapterItemRepair = new AdapterItemRepairEdit(getApplicationContext());

                    //아이템 추가
                    itemRepairFree1.setTitleName(orderItemOption.getOptionItemName());
                    itemRepairFree1.setAmount(String.valueOf(orderItemOption.getOptionPrice()));
                    itemRepairFree1.setOptionItemID(String.valueOf(orderItemOption.getOptionItemID()));
                    RepairFee.add(itemRepairFree1);

                    repairAmount.add(String.valueOf(orderItemOption.getOptionPrice()));

                    adapterItemRepair.addItem(RepairFee);
                    lvRepairFee.setAdapter(adapterItemRepair);
                    setListViewHeightBasedOnChildren(lvRepairFee); //동적 높이 조절

                    ManagerRepairFreeEdit.getInstance().setRepairFeeDates(RepairFee); //품목등록시 선택된 ItemData Setting
                }
            }
        }

        rlMemo.setVisibility(View.VISIBLE);
        recycleViewMemo.setHasFixedSize(true);
        ArrayList<String> memoDataList = new ArrayList<>();

        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
        recycleViewMemo.setLayoutManager(layoutManager);
        AdapterItemMemo adapterMemo = new AdapterItemMemo(getApplicationContext(), mOnAdapterListners);

        if (!TextUtils.isEmpty(orderItemList.get(mPosition).getOrderItemMemo()) || orderItemList.get(mPosition).getOrderItemMemo() != null) {
            String[] array = orderItemList.get(mPosition).getOrderItemMemo().split("\\|");
            for (String meno : array) {
                memoDataList.add(meno);
            }
            adapterMemo.addItem(memoDataList);
            recycleViewMemo.setAdapter(adapterMemo);
        } else {
            rlMemo.setVisibility(View.GONE);
        }
    }


    /**
     * 총 세탁 금액 View
     */
    private void setTotalAmonet() {
        int repairTotalMoney = 0;
        int technicalTotalMoney = 0;
        int componentTotalMoney = 0;
        int tvPaymentCount = 0;
        int totalMoney;
        try {
            if (repairAmount.size() != 0) {
                for (int i = 0; i < repairAmount.size(); i++) {
                    repairTotalMoney += Integer.parseInt(repairAmount.get(i));
                }
            }
            if (technicalFreeAmount.size() != 0) {
                for (int i = 0; i < technicalFreeAmount.size(); i++) {
                    technicalTotalMoney += Integer.parseInt(technicalFreeAmount.get(i));
                }
            }
            if (componentAmount.size() != 0) {
                for (int i = 0; i < componentAmount.size(); i++) {
                    componentTotalMoney += Integer.parseInt(componentAmount.get(i));
                }
            }

            for (GetOrderItemList getOrderItemList : orderItemList) {
                tvPaymentCount += getOrderItemList.getOrderItemPrice();
            }

            totalMoney = orderItemList.get(mPosition).getStoreItemPrice() + repairTotalMoney + technicalTotalMoney + componentTotalMoney;
            mTotalMoeny = totalMoney;

            tvTotalMoney.setText(DataFormatUtil.moneyFormatToWon(totalMoney) + "원");
            tvLaundryMoney.setText(DataFormatUtil.moneyFormatToWon(orderItemList.get(mPosition).getStoreItemPrice()) + "원");
            tvPayment.setText(DataFormatUtil.moneyFormatToWon(tvPaymentCount) + "원");

        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * 기타옵션 SettingView
     */
    private void setOtherOptions() {
        if (orderItemList.get(mPosition).getFreeType() == 1) {
            chLaundryFree.setBackgroundResource(R.drawable.a_radio_button_type_2_like);
            chLaundryFree.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        } else if (orderItemList.get(mPosition).getFreeType() == 2) {
            chReWashing.setBackgroundResource(R.drawable.a_radio_button_type_2_like);
            chReWashing.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        } else if (orderItemList.get(mPosition).getFreeType() == 3) {
            chStopLaundry.setBackgroundResource(R.drawable.a_radio_button_type_2_like);
            chStopLaundry.setTextColor(getResources().getColor(R.color.color_c7cfe8));
        }
    }

    /**
     * 품목 세탁방법
     *
     * @return
     */
    private String getWashType() {
        String washType = null;

        if (orderItemList.get(mPosition).getWashType() == 1) {
            washType = "드라이+다림질";
        } else if (orderItemList.get(mPosition).getWashType() == 2) {
            washType = "물세탁+다림질";
        } else if (orderItemList.get(mPosition).getWashType() == 3) {
            washType = "다림질";
        }
        return washType;
    }

    /**
     * 품목 세탁요금
     *
     * @return
     */
    private String getItemGrade() {
        String itemGrade = null;
        if (orderItemList.get(mPosition).getItemGrade() == 1) {
            itemGrade = "일반";
            mItemGrade = 1;
        } else if (orderItemList.get(mPosition).getItemGrade() == 2) {
            itemGrade = "명품";
            mItemGrade = 2;
        } else if (orderItemList.get(mPosition).getItemGrade() == 3) {
            itemGrade = "아동";
            mItemGrade = 3;
        }
        return itemGrade;
    }


    /**
     * 서버 데이터 전송 메소드
     */
    private void setServerRequestData() {
        try {
            ArrayList<ItemAdditionalDataEdit> itemAdditionalDataEdits = ManagerItemAdditionalDataEdit.getInstance().getItemAdditionalData();
            ArrayList<ItemAdditionalDataEditType> itemAdditionalDataList = ManagerItemAdditionalDataEditType.getInstance().getItemAdditionalData();
            ArrayList<ItemAddDataEdit> itemAddDatas = new ArrayList<>();

            for (int j = 0; j < itemAdditionalDataEdits.size(); j++) {
                ArrayList<ItemAddDataEdit.OrderItemOptionInput> orderItemOptionInputs = new ArrayList<>();

                ItemAdditionalDataEditType itemAdditionalDataType = new ItemAdditionalDataEditType();
                ItemAddDataEdit itemAddData = new ItemAddDataEdit();
                //품목
                itemAddData.setTacNumber(itemAdditionalDataEdits.get(j).getTacNumber());                            //Tag번호
                itemAddData.setTacColor(itemAdditionalDataEdits.get(j).getTacColor());                              //Tag컬러
                itemAddData.setStoreItemID(itemAdditionalDataEdits.get(j).getStoreItemID());                        //가맹점 상품 ID
                itemAddData.setOrderItemID(itemAdditionalDataEdits.get(j).getOrderItemID());                        //상품주문 ID
                itemAddData.setItemGrade(itemAdditionalDataEdits.get(j).getItemGrade());                            //상품등급
                itemAddData.setIsOutside(null);                                                     //외부세탁여부 2차 오픈 작업
                itemAddData.setIsDelicery(null);                                                    //배송여부 2차 오픈 작업
                itemAddData.setPhotoUrl(null);                                                      //사진 URL 2차 오픈 작업
                itemAddData.setWashType(itemAdditionalDataEdits.get(j).getItemGrade());                             //세탁방법
                itemAddData.setFreeType(itemAdditionalDataEdits.get(j).getFreeType());                              //기타옵션
                itemAddData.setOrderItemMemo(itemAdditionalDataEdits.get(j).getMemoData());              //메모

                itemAddData.setItemColor(String.valueOf(itemAdditionalDataEdits.get(j).getRgbColor()));             //색상

                itemAddData.setOrderItemPrice(Integer.valueOf(itemAdditionalDataEdits.get(j).getTotalMoney()));     //주문 상품가격

                if (itemAdditionalDataEdits.get(j).getOptions().size() != 0) {

                    for (int i = 0; i < itemAdditionalDataEdits.get(j).getOptions().size(); i++) {
                        ItemAddDataEdit.OrderItemOptionInput orderItemOptionInput = new ItemAddDataEdit.OrderItemOptionInput();

                        orderItemOptionInput.setOptionItemId(itemAdditionalDataEdits.get(j).getOptions().get(i).getOptionItemId());
                        orderItemOptionInput.setOptionType(itemAdditionalDataEdits.get(j).getOptions().get(i).getOptionType());
                        orderItemOptionInputs.add(orderItemOptionInput);
                        itemAddData.setItemOption(orderItemOptionInputs);
                    }
                } else if (itemAdditionalDataEdits.get(j).getOptions().size() == 0) {
                    itemAddData.setItemOption(orderItemOptionInputs);
                }

                itemAddDatas.add(itemAddData);
                itemAdditionalDataType.setItemAddData(itemAddDatas);
                itemAdditionalDataList.add(itemAdditionalDataType);
            }
            ManagerItemAdditionalDataEditType.getInstance().setItemAdditionalData(itemAdditionalDataList);

            new Thread(new Runnable() {
                public void run() {
                    String requestCode = LaundryRegistrationManager.setUpdateVisitOrder(ActivityEditItem.this, orderId, mTotalAmount);
                    runOnUiThread(new Runnable() {
                        public void run() {
                            if (requestCode.equals("S0000")) {
                                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityEditItem.this);
                                thirdAlertPopup.setTitle("수정완료");
                                thirdAlertPopup.setContent("품목수정이 완료되었어요.");
                                thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                    @Override
                                    public void onConfirmClick() {
                                        finish();
                                    }
                                });
                                thirdAlertPopup.show();
                            }
                        }
                    });
                }
            }).start();

        } catch (Exception err) {
            err.printStackTrace();
        }
    }


    /**
     * 품목 수정 리스트 수정 등록
     */
    private void setItemEditAddData() {
        ArrayList<ItemAdditionalDataEdit.OrderItemOptionInput> orderItemOptionInputs = new ArrayList<>();
        ArrayList<ItemRepairFree> ItemRepairFreeEdit = new ArrayList<>();        //수선요금
        ArrayList<ItemTechnical> ItemTechnicalEdit = new ArrayList<>();          //기술요금
        ArrayList<ItemComponent> ItemComponentEdit = new ArrayList<>();          //부속품요금

        ArrayList<ItemAdditionalDataEdit> itemAdditionalData = ManagerItemAdditionalDataEdit.getInstance().getItemAdditionalData();
        ItemAdditionalDataEdit additionalData = new ItemAdditionalDataEdit();
        additionalData.setItemName(orderItemList.get(mPosition).getStoreItemName());

        Logger.d(TAG, "itemAdditionalData.get(mPosition).getTacNumber() : " + itemAdditionalData.get(mPosition).getTacNumber());
        Logger.d(TAG, "TacNumber() : " + mTagNumber);
        if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
            additionalData.setTacNumber(itemAdditionalData.get(mPosition).getTacNumber());        // 일제 택번호
        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
            additionalData.setTacNumber(itemAdditionalData.get(mPosition).getTacNumber());    // 일제바코드 택번호
        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
            additionalData.setTacNumber(itemAdditionalData.get(mPosition).getTacNumber());// 기타 택번호
        }

        additionalData.setOrderItemID(orderItemList.get(mPosition).getOrderItemID());           //주문 상품 ID
        additionalData.setTacColor(Integer.valueOf(orderItemList.get(mPosition).getTagColor())); // 택컬러
        additionalData.setStoreItemID(orderItemList.get(mPosition).getStoreItemID());    // 가맹점 상품 ID
        additionalData.setStoreItemName(mLaundryCharge);                                 // 가맹점 상품 NAME <-서버 데이터 저장안함.
        if (mItemGrade == 99) {
            additionalData.setItemGrade(orderItemList.get(mPosition).getItemGrade());    // 상품등급
        } else {
            additionalData.setItemGrade(mItemGrade);                                     // 상품등급
        }
        additionalData.setIsOutside(null);                                               // 외부세탁여부
        additionalData.setIsDelivery(null);                                              // 배송여부
        additionalData.setPhotoUri(null);                                                // 첨부사진
        if (howtoWashType == 99) {
            additionalData.setHowToWash(orderItemList.get(mPosition).getWashType());     // 세탁방법
        } else {
            additionalData.setHowToWash(howtoWashType);                                  // 세탁방법
        }
        additionalData.setFreeType(orderItemList.get(mPosition).getFreeType());          // 기타옵션

        if (listMemo.equals("") || TextUtils.isEmpty(listMemo)) {
            additionalData.setMemoData(orderItemList.get(mPosition).getOrderItemMemo()); // 메모
        } else {
            additionalData.setMemoData(listMemo.toString());                             // 메모
        }

        if (positionColor == 99) {
            additionalData.setRgbColor(Integer.valueOf(orderItemList.get(mPosition).getItemColor())); // 색상
        } else {
            additionalData.setRgbColor(positionColor);                                                // 색상
        }
        additionalData.setTotalMoney(String.valueOf(mTotalMoeny));                        // 주문금액
        //데이터 리스트 추가

        //수선 요금 ADD
        if (ManagerRepairFreeEdit.getInstance().getRepairFreeDates().size() != 0 || ManagerRepairFreeEdit.getInstance().getRepairFreeDates() != null) {
            for (ItemRepairFree itemRepairFree : ManagerRepairFreeEdit.getInstance().getRepairFreeDates()) {
                ItemAdditionalDataEdit.OrderItemOptionInput orderItemOptionInput = new ItemAdditionalDataEdit.OrderItemOptionInput();
                orderItemOptionInput.setOptionItemId(Integer.valueOf(itemRepairFree.getOptionItemID()));
                orderItemOptionInput.setOptionType(3);
                orderItemOptionInputs.add(orderItemOptionInput);
            }
        }

        //추가기술 ADD
        try {
            if (ManagerTechnicalFeeEdit.getInstance().getTechnicalFee().size() != 0 || ManagerTechnicalFeeEdit.getInstance().getTechnicalFee() != null) {
                for (ItemTechnical itemTechnical : ManagerTechnicalFeeEdit.getInstance().getTechnicalFee()) {
                    ItemAdditionalDataEdit.OrderItemOptionInput orderItemOptionInput = new ItemAdditionalDataEdit.OrderItemOptionInput();
                    orderItemOptionInput.setOptionItemId(Integer.valueOf(itemTechnical.getOptionItemID()));
                    orderItemOptionInput.setOptionType(1);
                    orderItemOptionInputs.add(orderItemOptionInput);
                }
            }
        } catch (NumberFormatException err) {
            err.printStackTrace();
        }

        //부속품 ADD
        try {
            if (ManagerComponentEdit.getInstance().getTComponent().size() != 0 || ManagerComponentEdit.getInstance().getTComponent() != null) {
                for (ItemComponent itemComponent : ManagerComponentEdit.getInstance().getTComponent()) {
                    ItemAdditionalDataEdit.OrderItemOptionInput orderItemOptionInput = new ItemAdditionalDataEdit.OrderItemOptionInput();
                    orderItemOptionInput.setOptionItemId(Integer.valueOf(itemComponent.getOptionItemID()));
                    orderItemOptionInput.setOptionType(2);
                    orderItemOptionInputs.add(orderItemOptionInput);
                }
            }
        } catch (NumberFormatException err) {
            err.printStackTrace();
        }
        additionalData.setOptionTypeRepairFee(ItemRepairFreeEdit);          //수선
        additionalData.setOptionTechnicalFree(ItemTechnicalEdit);
        additionalData.setOptionTypeComponent(ItemComponentEdit);

        additionalData.setOptions(orderItemOptionInputs);                    // 추가옵션

        setListTotalMoney();
        itemAdditionalData.set(mPosition, additionalData);
        ManagerItemAdditionalDataEdit.getInstance().setItemAdditionalData(itemAdditionalData);
    }

    private void setListTotalMoney() {
        int toTalMoneyCount = 0;
        orderItemList.get(mPosition).setOrderItemPrice(mTotalMoeny);
        orderItemList.get(mPosition).setNonPayPrice(mTotalMoeny);

        for (GetOrderItemList getOrderItemList : orderItemList) {
            toTalMoneyCount += getOrderItemList.getOrderItemPrice();
        }
        tvItemListTotalMoeny.setText(DataFormatUtil.moneyFormatToWon(toTalMoneyCount));

        mTotalAmount = toTalMoneyCount;
        adapterEditItem.notifyDataSetChanged();
    }

    /**
     * 초기 데이터 셋팅
     */
    private void setInitialSetting() {
        ArrayList<ItemAdditionalDataEdit> itemAdditionalData = ManagerItemAdditionalDataEdit.getInstance().getItemAdditionalData();
        for (GetOrderItemList getOrderItemList : orderItemList) {
            ItemAdditionalDataEdit additionalData = new ItemAdditionalDataEdit();
            ArrayList<ItemAdditionalDataEdit.OrderItemOptionInput> orderItemOptionInputs = new ArrayList<>();

            additionalData.setItemName(getOrderItemList.getStoreItemName());
            Logger.d(TAG, "getOrderItemList.getTagNo() : " + getOrderItemList.getTagNo());
            additionalData.setTacNumber(getOrderItemList.getTagNo());

            additionalData.setTacColor(Integer.valueOf(getOrderItemList.getTagColor()));     // 택컬러
            additionalData.setStoreItemID(getOrderItemList.getStoreItemID());                // 가맹점 상품 ID
            additionalData.setStoreItemName(getOrderItemList.getStoreItemName());            // 가맹점 상품 NAME <-서버 데이터 저장안함.
            additionalData.setItemGrade(getOrderItemList.getItemGrade());                    // 상품등급
            additionalData.setOrderItemID(getOrderItemList.getOrderItemID());                // 주문상품 ID
            additionalData.setIsOutside(null);                                               // 외부세탁여부
            additionalData.setIsDelivery(null);                                              // 배송여부
            additionalData.setPhotoUri(null);                                                // 첨부사진
            additionalData.setHowToWash(getOrderItemList.getWashType());                     // 세탁방법
            additionalData.setFreeType(getOrderItemList.getFreeType());                      // 기타옵션
            additionalData.setMemoData(getOrderItemList.getOrderItemMemo());                 // 메모
            additionalData.setRgbColor(Integer.valueOf(getOrderItemList.getItemColor()));    // 색상
            additionalData.setTotalMoney(String.valueOf(getOrderItemList.getNonPayPrice())); // 주문금액

            for (int i = 0; i < getOrderItemList.getOptions().size(); i++) {
                //수선 요금 ADD
                if (getOrderItemList.getOptions().get(i).getOptionType() == 3) {
                    ItemAdditionalDataEdit.OrderItemOptionInput orderItemOptionInput = new ItemAdditionalDataEdit.OrderItemOptionInput();
                    orderItemOptionInput.setOptionItemId(Integer.valueOf(getOrderItemList.getOptions().get(i).getOptionItemID()));
                    orderItemOptionInput.setOptionType(getOrderItemList.getOptions().get(i).getOptionType());
                    orderItemOptionInputs.add(orderItemOptionInput);
                }

                //추가기술 ADD
                if (getOrderItemList.getOptions().get(i).getOptionType() == 1) {
                    ItemAdditionalDataEdit.OrderItemOptionInput orderItemOptionInput = new ItemAdditionalDataEdit.OrderItemOptionInput();
                    orderItemOptionInput.setOptionItemId(Integer.valueOf(getOrderItemList.getOptions().get(i).getOptionItemID()));
                    orderItemOptionInput.setOptionType(getOrderItemList.getOptions().get(i).getOptionType());
                    orderItemOptionInputs.add(orderItemOptionInput);
                }

                //부속품 ADD
                if (getOrderItemList.getOptions().get(i).getOptionType() == 2) {
                    ItemAdditionalDataEdit.OrderItemOptionInput orderItemOptionInput = new ItemAdditionalDataEdit.OrderItemOptionInput();
                    orderItemOptionInput.setOptionItemId(Integer.valueOf(getOrderItemList.getOptions().get(i).getOptionItemID()));
                    orderItemOptionInput.setOptionType(getOrderItemList.getOptions().get(i).getOptionType());
                    orderItemOptionInputs.add(orderItemOptionInput);
                }
            }
            additionalData.setOptions(orderItemOptionInputs);                    // 추가옵션
            itemAdditionalData.add(additionalData);
        }
        ManagerItemAdditionalDataEdit.getInstance().setItemAdditionalData(itemAdditionalData);
    }


    public interface setAddPosition {
        void selectPosition(int position);

    }

    private setAddPosition selectPosition = new setAddPosition() {
        @Override
        public void selectPosition(int position) {
            mPosition = position;

            repairAmount.clear();
            technicalFreeAmount.clear();
            componentAmount.clear();

            fragmentItemRegistation.setVisibility(View.GONE);

            ManagerTechnicalFeeEdit.getInstance().setItemDelete();    //기술요금 초기화
            ManagerComponentEdit.getInstance().setItemDelete();       //부속품 초기화
            ManagerRepairFreeEdit.getInstance().setReairFreeDelete(); //수선요금 초기화

            setLayout();
        }
    };

    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListners = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
}