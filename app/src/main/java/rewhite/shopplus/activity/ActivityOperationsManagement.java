package rewhite.shopplus.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.view.adapter.AdapterOperationManagement;
import rewhite.shopplus.util.CustomViewPager;

/**
 * 운영관리 View
 */
public class ActivityOperationsManagement extends AppCompatActivity {
    private static final String TAG = "ActivityOperationsManagement";

    private CustomViewPager viewPager;

    private LinearLayout llMembershipManagement;
    private ImageView ivMembershipManagement;
    private TextView tvMembershipManagement;

    private LinearLayout llAcountsReceivableManagement;
    private ImageView ivAcountsReceivableManagement;
    private TextView tvAcountsReceivableManagement;

    private LinearLayout llNonIssueManagement;
    private ImageView ivNonIssueManagement;
    private TextView tvNonIssueManagement;

    private LinearLayout llCardPaymentManagement;
    private ImageView ivCardPaymentManagement;
    private TextView tvCardPaymentManagement;

    private LinearLayout llPreChargeManagement;
    private ImageView ivPreChargeManagement;
    private TextView tvPreChargeManagementt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operations_management);

        setLayout();
        setViewPagerAdapter();
    }

    private void setLayout() {
        llMembershipManagement = (LinearLayout) findViewById(R.id.ll_membership_management);
        ivMembershipManagement = (ImageView) findViewById(R.id.iv_membership_management);
        tvMembershipManagement = (TextView) findViewById(R.id.tv_membership_management);

        llAcountsReceivableManagement = (LinearLayout) findViewById(R.id.ll_accounts_receivable_management);
        ivAcountsReceivableManagement = (ImageView) findViewById(R.id.iv_accounts_receivable_management);
        tvAcountsReceivableManagement = (TextView) findViewById(R.id.tv_accounts_receivable_management);

        llNonIssueManagement = (LinearLayout) findViewById(R.id.ll_non_issue_management);
        ivNonIssueManagement = (ImageView) findViewById(R.id.iv_non_issue_management);
        tvNonIssueManagement = (TextView) findViewById(R.id.tv_non_issue_management);

        llCardPaymentManagement = (LinearLayout) findViewById(R.id.ll_card_payment_management);
        ivCardPaymentManagement = (ImageView) findViewById(R.id.iv_card_payment_management);
        tvCardPaymentManagement = (TextView) findViewById(R.id.tv_card_payment_management);

        llPreChargeManagement = (LinearLayout) findViewById(R.id.ll_pre_charge_management);
        ivPreChargeManagement = (ImageView) findViewById(R.id.iv_pre_charge_management);
        tvPreChargeManagementt = (TextView) findViewById(R.id.tv_pre_charge_management);

        viewPager = (CustomViewPager) findViewById(R.id.vp_sales_status);
    }


    private void setViewPagerAdapter() {
        viewPager.setAdapter(new AdapterOperationManagement(getSupportFragmentManager()));
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(4);
        setTapBackGround(0);

        llMembershipManagement.setOnClickListener(movePageListener);
        llMembershipManagement.setTag(0);
        llAcountsReceivableManagement.setOnClickListener(movePageListener);
        llAcountsReceivableManagement.setTag(1);
        llNonIssueManagement.setOnClickListener(movePageListener);
        llNonIssueManagement.setTag(2);
        llCardPaymentManagement.setOnClickListener(movePageListener);
        llCardPaymentManagement.setTag(3);
        llPreChargeManagement.setOnClickListener(movePageListener);
        llPreChargeManagement.setTag(4);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            viewPager.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    /**
     * 탭 UI 변경 분기처리 로직
     *
     * @param tag
     */
    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                setMembershipManagementSelect();
                break;
            case 1:
                setAcountsReceivableManagementSelect();
                break;
            case 2:
                setNonIssueManagementSelect();
                break;
            case 3:
                setCardPaymentManagementSelect();
                break;
            case 4:
                setPreChargeManagementSelect();
                break;
        }
    }

    private void setMembershipManagementSelect() {
        llMembershipManagement.setBackgroundResource(R.drawable.a_button_type_3);
        ivMembershipManagement.setBackgroundResource(R.drawable.btn_manage_press_00);
        tvMembershipManagement.setTextColor(getResources().getColor(R.color.color_43425c));

        llAcountsReceivableManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivAcountsReceivableManagement.setBackgroundResource(R.drawable.btn_manage_normal_01);
        tvAcountsReceivableManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llNonIssueManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivNonIssueManagement.setBackgroundResource(R.drawable.btn_manage_normal_02);
        tvNonIssueManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llCardPaymentManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivCardPaymentManagement.setBackgroundResource(R.drawable.btn_manage_normal_03);
        tvCardPaymentManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llPreChargeManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivPreChargeManagement.setBackgroundResource(R.drawable.btn_manage_normal_04);
        tvPreChargeManagementt.setTextColor(getResources().getColor(R.color.Wite_color));
    }

    private void setAcountsReceivableManagementSelect() {
        llAcountsReceivableManagement.setBackgroundResource(R.drawable.a_button_type_3);
        ivAcountsReceivableManagement.setBackgroundResource(R.drawable.btn_manage_press_01);
        tvAcountsReceivableManagement.setTextColor(getResources().getColor(R.color.color_43425c));

        llMembershipManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivMembershipManagement.setBackgroundResource(R.drawable.btn_manage_normal_00);
        tvMembershipManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llNonIssueManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivNonIssueManagement.setBackgroundResource(R.drawable.btn_manage_normal_02);
        tvNonIssueManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llCardPaymentManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivCardPaymentManagement.setBackgroundResource(R.drawable.btn_manage_normal_03);
        tvCardPaymentManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llPreChargeManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivPreChargeManagement.setBackgroundResource(R.drawable.btn_manage_normal_04);
        tvPreChargeManagementt.setTextColor(getResources().getColor(R.color.Wite_color));
    }

    private void setNonIssueManagementSelect() {
        llNonIssueManagement.setBackgroundResource(R.drawable.a_button_type_3);
        ivNonIssueManagement.setBackgroundResource(R.drawable.btn_manage_press_03);
        tvNonIssueManagement.setTextColor(getResources().getColor(R.color.color_43425c));

        llMembershipManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivMembershipManagement.setBackgroundResource(R.drawable.btn_manage_normal_00);
        tvMembershipManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llAcountsReceivableManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivAcountsReceivableManagement.setBackgroundResource(R.drawable.btn_manage_normal_01);
        tvAcountsReceivableManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llCardPaymentManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivCardPaymentManagement.setBackgroundResource(R.drawable.btn_manage_normal_03);
        tvCardPaymentManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llPreChargeManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivPreChargeManagement.setBackgroundResource(R.drawable.btn_manage_normal_04);
        tvPreChargeManagementt.setTextColor(getResources().getColor(R.color.Wite_color));
    }

    private void setCardPaymentManagementSelect() {
        llCardPaymentManagement.setBackgroundResource(R.drawable.a_button_type_3);
        ivCardPaymentManagement.setBackgroundResource(R.drawable.btn_manage_press_04);
        tvCardPaymentManagement.setTextColor(getResources().getColor(R.color.color_43425c));

        llAcountsReceivableManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivAcountsReceivableManagement.setBackgroundResource(R.drawable.btn_manage_normal_01);
        tvAcountsReceivableManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llMembershipManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivMembershipManagement.setBackgroundResource(R.drawable.btn_manage_normal_00);
        tvMembershipManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llNonIssueManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivNonIssueManagement.setBackgroundResource(R.drawable.btn_manage_normal_02);
        tvNonIssueManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llPreChargeManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivPreChargeManagement.setBackgroundResource(R.drawable.btn_manage_normal_04);
        tvPreChargeManagementt.setTextColor(getResources().getColor(R.color.Wite_color));
    }

    private void setPreChargeManagementSelect() {
        llPreChargeManagement.setBackgroundResource(R.drawable.a_button_type_3);
        ivPreChargeManagement.setBackgroundResource(R.drawable.btn_manage_press_05);
        tvPreChargeManagementt.setTextColor(getResources().getColor(R.color.color_43425c));

        llAcountsReceivableManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivAcountsReceivableManagement.setBackgroundResource(R.drawable.btn_manage_normal_01);
        tvAcountsReceivableManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llMembershipManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivMembershipManagement.setBackgroundResource(R.drawable.btn_manage_normal_00);
        tvMembershipManagement.setTextColor(getResources().getColor(R.color.Wite_color));

        llNonIssueManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivNonIssueManagement.setBackgroundResource(R.drawable.btn_manage_normal_02);
        tvNonIssueManagement.setTextColor(getResources().getColor(R.color.Wite_color));


        llCardPaymentManagement.setBackgroundResource(R.drawable.a_button_type_2);
        ivCardPaymentManagement.setBackgroundResource(R.drawable.btn_manage_normal_03);
        tvCardPaymentManagement.setTextColor(getResources().getColor(R.color.Wite_color));
    }
}
