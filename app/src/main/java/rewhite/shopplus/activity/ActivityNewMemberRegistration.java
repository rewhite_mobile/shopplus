package rewhite.shopplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.manager.ManagerStoreUserList;
import rewhite.shopplus.client.model.StoreUserInfoItemList;
import rewhite.shopplus.client.network.StoreUserApiManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.fragment.main.FragmentHome;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;

/**
 * 신규 회원 등록 View
 */
public class ActivityNewMemberRegistration extends AppCompatActivity {
    private static final String TAG = "ActivityNewMemberRegistration";
    private static final String MEMBER_DETAIL = "member_detail";  // 회원정보수정 Type;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private static final String USER_STORE_ID = "user_store_id";
    private String birthdayMonth = "";
    private String birthdayDay = "";
    private String birthdayYear = "";
    private List<StoreUserInfoItemList> storeUserInfoItemList;

    private Button btnMemberEnrollment;
    private String VIEW_TYPE;
    private EditText etPhonnumber, etNewMember, etAddress, etMemo;
    private RadioGroup rbGender;
    private CheckBox cbNoti, cbEvent;
    private String userName, address, userPhone, birthDay, anniversary;

    private RadioButton rbMen, rbWomon;
    private String isNoticeAgree = "Y";
    private String isPromoteArgee = "N";
    private String month = "";
    private String day = "";
    private String gender = "M";
    private EditText etWeddingAnniversary, etBirthday;
    private ImageView type1, type2, type3;
    private int memberRating = 1;

    private WaitCounter mWait;

    private TextView tvPopupTitle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_member_registration);
        mWait = new WaitCounter(this);

        Intent intent = getIntent();
        VIEW_TYPE = intent.getExtras().getString(MEMBER_DETAIL);

        setLayout();
    }

    private void setLayout() {
        etPhonnumber = (EditText) findViewById(R.id.et_phonnumber);
        etPhonnumber.setInputType(android.text.InputType.TYPE_CLASS_PHONE); // 먼저 EditText에 번호만 입력되도록 바꾼 뒤
        etPhonnumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher()); // 이렇게 리스너를 걸어주면

        etWeddingAnniversary = (EditText) findViewById(R.id.et_wedding_anniversary);

        etNewMember = (EditText) findViewById(R.id.et_new_member);
        etAddress = (EditText) findViewById(R.id.et_address);
        etMemo = (EditText) findViewById(R.id.et_memo);
        rbGender = (RadioGroup) findViewById(R.id.rb_gender);
        cbNoti = (CheckBox) findViewById(R.id.cb_noti);
        cbEvent = (CheckBox) findViewById(R.id.cb_event);

        rbMen = (RadioButton) findViewById(R.id.rb_men);
        rbWomon = (RadioButton) findViewById(R.id.rb_womon);

        LinearLayout llType1 = (LinearLayout) findViewById(R.id.ll_type1);
        LinearLayout llType2 = (LinearLayout) findViewById(R.id.ll_type2);
        LinearLayout llType3 = (LinearLayout) findViewById(R.id.ll_type3);
        LinearLayout llClose = (LinearLayout) findViewById(R.id.ll_close);
        btnMemberEnrollment = (Button) findViewById(R.id.btn_member_enrollment);

        type1 = (ImageView) findViewById(R.id.type_1);
        type2 = (ImageView) findViewById(R.id.type_2);
        type3 = (ImageView) findViewById(R.id.type_3);

        tvPopupTitle = (TextView) findViewById(R.id.tv_popup_title);
        etBirthday = (EditText) findViewById(R.id.et_birthday);

        btnMemberEnrollment.setOnClickListener(clickListener);
        llType1.setOnClickListener(clickListener);
        llType2.setOnClickListener(clickListener);
        llType3.setOnClickListener(clickListener);
        llClose.setOnClickListener(clickListener);

        rbGender.setOnCheckedChangeListener(onCheckedChangeListener);
        cbNoti.setOnCheckedChangeListener(checkBoxNotiChangeListener);
        cbEvent.setOnCheckedChangeListener(checkBoxEventChangeListener);

        //결혼기념일 데이터 포맷
        etWeddingAnniversary.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (start == 2) {
                        month = s.toString().substring(0, 2);
                    } else if (start == 3) {
                        day = s.toString().substring(2, 4);
                        etWeddingAnniversary.setText(month + "." + day);
                        etWeddingAnniversary.setSelection(etWeddingAnniversary.length());
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //생일 데이터 포맷
        etBirthday.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                StringBuffer stringBuffer = new StringBuffer(s.toString());

                if (start == 3) {
                    birthdayYear = stringBuffer.toString() + ".";

                    etBirthday.setText(birthdayYear);
                    etBirthday.setSelection(etBirthday.length());
                } else if (start == 7) {
                    birthdayMonth = s.toString().substring(5, 7) + ".";

                    etBirthday.setText(birthdayYear + birthdayMonth);
                    etBirthday.setSelection(etBirthday.length());

                } else if (start == 10) {
                    birthdayDay = s.toString().substring(8, 10);

                    etBirthday.setText(birthdayYear + birthdayMonth + birthdayDay);
                    etBirthday.setSelection(etBirthday.length());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        setTextView();
    }

    private void setTextView() {
        storeUserInfoItemList = ManagerStoreUserInfo.getmInstance().getStoreUserInfo();

        if (VIEW_TYPE.equals("2")) {
            etPhonnumber.setText(storeUserInfoItemList.get(0).getData().get_UserPhone());
            etNewMember.setText(storeUserInfoItemList.get(0).getData().getUserName());
            etAddress.setText(storeUserInfoItemList.get(0).getData().get_UserAddress());
            etAddress.setText(storeUserInfoItemList.get(0).getData().get_UserAddress());
            etWeddingAnniversary.setText(storeUserInfoItemList.get(0).getData().getAnniversary());
            etMemo.setText(storeUserInfoItemList.get(0).getData().getMemo());
            etBirthday.setText(storeUserInfoItemList.get(0).getData().getBirthDay());

            if (storeUserInfoItemList.get(0).getData().getIsNoticeAgree().equals("Y")) {
                cbNoti.setChecked(true);
            } else {
                cbNoti.setChecked(false);
            }

            if (storeUserInfoItemList.get(0).getData().getIsPromoteAgree().equals("Y")) {
                cbEvent.setChecked(true);
            } else {
                cbEvent.setChecked(false);
            }

            if (storeUserInfoItemList.get(0).getData().getStoreUserGrade().contains("1")) {
                type1.setVisibility(View.VISIBLE);

                type2.setVisibility(View.GONE);
                type3.setVisibility(View.GONE);

            } else if (storeUserInfoItemList.get(0).getData().getStoreUserGrade().contains("2")) {
                type2.setVisibility(View.VISIBLE);

                type1.setVisibility(View.GONE);
                type3.setVisibility(View.GONE);
            } else if (storeUserInfoItemList.get(0).getData().getStoreUserGrade().contains("3")) {
                type3.setVisibility(View.VISIBLE);

                type1.setVisibility(View.GONE);
                type2.setVisibility(View.GONE);

            }

            if (storeUserInfoItemList.get(0).getData().getGender().equals("F")) {
                rbWomon.setChecked(true);
                rbMen.setChecked(false);
            } else {
                rbWomon.setChecked(false);
                rbMen.setChecked(true);
            }

            tvPopupTitle.setText("회원정보 수정");
            btnMemberEnrollment.setText("회원수정");
        } else {
            tvPopupTitle.setText("회원신규 등록");
            btnMemberEnrollment.setText("회원등록");
        }
    }

    private CompoundButton.OnCheckedChangeListener checkBoxNotiChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                isNoticeAgree = "Y";
            } else {
                isNoticeAgree = "N";
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener checkBoxEventChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                isPromoteArgee = "Y";
            } else {
                isPromoteArgee = "N";
            }
        }
    };

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.btn_member_enrollment:
                    if (etNewMember.getText().toString().isEmpty() && etAddress.getText().toString().isEmpty() && etPhonnumber.getText().toString().isEmpty()) {
                        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityNewMemberRegistration.this);

                        thirdAlertPopup.setTitle("오류 팝업");
                        thirdAlertPopup.setContent("회원정보를 입력해주세요.");
                        thirdAlertPopup.setButtonText("확인");
                        thirdAlertPopup.show();
                    } else {
                        if (VIEW_TYPE.equals("1")) {
                            try {
                                mWait.show();
                                new Thread(new Runnable() {
                                    public void run() {
                                        userName = etNewMember.getText().toString();
                                        address = etAddress.getText().toString();
                                        userPhone = etPhonnumber.getText().toString();
                                        birthDay = etBirthday.getText().toString();
                                        anniversary = etWeddingAnniversary.getText().toString();
                                        setNullpointCheckLogic();

                                        String ResultCode = StoreUserApiManager.getStoreUserRegister(ActivityNewMemberRegistration.this, String.valueOf(memberRating), userName, address, userPhone,
                                                gender, birthDay, anniversary, isNoticeAgree, isPromoteArgee);
                                        runOnUiThread(new Runnable() {
                                            public void run() {
                                                mWait.dismiss();
                                                if (userPhone == null || TextUtils.isEmpty(userPhone)) {
                                                    userPhone = "0000";
                                                }
                                                ManagerStoreUserList storeUserInfo = StoreUserApiManager.getStoreUserList(ActivityNewMemberRegistration.this, null, userPhone);
                                                if (ResultCode.equals("F0001")) {
                                                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityNewMemberRegistration.this);
                                                    thirdAlertPopup.setTitle("오류팝업");
                                                    thirdAlertPopup.setContent("회원정보를 입력해주세요.");
                                                    thirdAlertPopup.setButtonText("확인");
                                                    thirdAlertPopup.show();
                                                } else {
                                                    if (storeUserInfo.getStoreUserInfo().size() >= 1) {
                                                        ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityNewMemberRegistration.this);
                                                        thirdConfirmPopup.setTitle("핸드폰 중복오류");
                                                        thirdConfirmPopup.setContent("이미 가입된 휴대폰번호입니다.\n" + "회원정보를 확인할까요? ");
                                                        thirdConfirmPopup.setButtonText("예");
                                                        thirdConfirmPopup.setCancelButtonText("아니오");
                                                        thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                                            @Override
                                                            public void onConfirmClick() {
                                                                if (storeUserInfo.getStoreUserInfo().size() == 1) {
                                                                    Intent intent = new Intent(getApplicationContext(), ActivityMemberDetails.class);
                                                                    intent.putExtra(USER_STORE_ID, storeUserInfo.getStoreUserInfo().get(0).getStoreUserId());
                                                                    SettingLogic.setStoreUserId(ActivityNewMemberRegistration.this, String.valueOf(storeUserInfo.getStoreUserInfo().get(0).getStoreUserId()));
                                                                    startActivity(intent);
                                                                    finish();
                                                                } else {
                                                                    finish();
                                                                    FragmentHome.getMemberSearchList(storeUserInfo);
                                                                }
                                                            }
                                                        });
                                                        thirdConfirmPopup.show();
                                                    } else {
                                                        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityNewMemberRegistration.this);
                                                        thirdAlertPopup.setTitle(" 회원등록 완료 팝업");
                                                        thirdAlertPopup.setContent("등록이 완료되었습니다.");
                                                        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                                            @Override
                                                            public void onConfirmClick() {
//                                                                if (userPhone == null || TextUtils.isEmpty(userPhone)) {
//                                                                    userPhone = "010-0000-0000";
//                                                                }
//                                                                ManagerStoreUserList storeUserInfo = StoreUserApiManager.getStoreUserList(ActivityNewMemberRegistration.this, userName, userPhone);
                                                                if (storeUserInfo.getStoreUserInfo().size() == 1) {
                                                                    Intent intent = new Intent(getApplicationContext(), ActivityMemberDetails.class);
                                                                    intent.putExtra(USER_STORE_ID, storeUserInfo.getStoreUserInfo().get(0).getStoreUserId());
                                                                    SettingLogic.setStoreUserId(ActivityNewMemberRegistration.this, String.valueOf(storeUserInfo.getStoreUserInfo().get(0).getStoreUserId()));
                                                                    startActivity(intent);
                                                                    finish();
                                                                } else {
                                                                    finish();
                                                                    FragmentHome.getMemberSearchList(storeUserInfo);
                                                                }

                                                            }
                                                        });
                                                        thirdAlertPopup.show();
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }).start();
                            } catch (Exception err) {
                                err.printStackTrace();
                            }
                        } else if (VIEW_TYPE.equals("2")) {
                            try {
                                mWait.show();
                                new Thread(new Runnable() {
                                    public void run() {
                                        String resultCode = StoreUserApiManager.getStoreUserUpdate(ActivityNewMemberRegistration.this, storeUserInfoItemList.get(0).getData().getStoreUserID(), String.valueOf(memberRating), etNewMember.getText().toString()
                                                , etAddress.getText().toString(), etPhonnumber.getText().toString(), gender, etMemo.getText().toString(), etBirthday.getText().toString(), etWeddingAnniversary.getText().toString(), isNoticeAgree, isPromoteArgee);
                                        runOnUiThread(new Runnable() {
                                            public void run() {
                                                mWait.dismiss();
                                                if (resultCode.equals("S0000")) {
                                                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(ActivityNewMemberRegistration.this);
                                                    thirdAlertPopup.setTitle("완료팝업");
                                                    thirdAlertPopup.setContent("입력한 내용이 수정 되었습니다.");
                                                    thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                                        @Override
                                                        public void onConfirmClick() {
                                                            finish();
                                                        }
                                                    });
                                                    thirdAlertPopup.show();
                                                } else if (resultCode.equals("E0000")) {
                                                    Logger.d(TAG, "회원정보 수정 실패");
                                                }
                                            }
                                        });
                                    }
                                }).start();
                            } catch (Exception err) {
                                err.printStackTrace();
                            }
                        }
                    }
                    break;

                case R.id.ll_type1:
                    memberRating = 1;
                    type1.setVisibility(View.VISIBLE);

                    type2.setVisibility(View.GONE);
                    type3.setVisibility(View.GONE);
                    break;

                case R.id.ll_type2:
                    memberRating = 2;
                    type2.setVisibility(View.VISIBLE);

                    type1.setVisibility(View.GONE);
                    type3.setVisibility(View.GONE);
                    break;

                case R.id.ll_type3:
                    memberRating = 3;
                    type3.setVisibility(View.VISIBLE);

                    type1.setVisibility(View.GONE);
                    type2.setVisibility(View.GONE);
                    break;

                case R.id.ll_close:
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(ActivityNewMemberRegistration.this);
                    thirdConfirmPopup.setTitle("취소팝업");
                    thirdConfirmPopup.setContent("입력한 내용이 모두 삭제됩니다.\n" + "등록을 취소하시겠어요? ");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {
                            finish();
                        }
                    });

                    thirdConfirmPopup.show();
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            switch (checkedId) {

                case R.id.rb_men:
                    gender = "M";
                    break;

                case R.id.rb_womon:
                    gender = "F";
                    break;
            }
        }
    };

    private void setNullpointCheckLogic() {
        if (userName.equals("") || TextUtils.isEmpty(userName)) {
            userName = null;
        }
        if (address.equals("") || TextUtils.isEmpty(address)) {
            address = null;
        }

        if (userPhone.equals("") || TextUtils.isEmpty(userPhone)) {
            userPhone = null;
        }

        if (birthDay.equals("") || TextUtils.isEmpty(birthDay)) {
            birthDay = null;
        }

        if (anniversary.equals("") || TextUtils.isEmpty(anniversary)) {
            anniversary = null;
        }
    }

}
