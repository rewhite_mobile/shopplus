package rewhite.shopplus.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import rewhite.shopplus.fragment.main.FragementCollection;
import rewhite.shopplus.fragment.sub.FragmentDelivery;
import rewhite.shopplus.util.Logger;

/**
 * MainActivity 오늘의 수거 배송 SettingViewPagerAdater
 */
public class AdapterTodayPager extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterMainPager";

    public AdapterTodayPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Logger.d(TAG, "position : " + position);
        switch (position) {
            case 0:
                return new FragementCollection();// 수거
            case 1:
                return new FragmentDelivery();// 배송
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}