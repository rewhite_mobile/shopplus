package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.MemoItemList;
import rewhite.shopplus.data.manager.ManagerItemAdditionalData;
import rewhite.shopplus.data.manager.ManagerMemo;
import rewhite.shopplus.fragment.sub.FragmentMemo;

public class AdapterMemoEdit extends RecyclerView.Adapter {
    private static final String TAG = "AdapterMemoEdit";
    private Context mContext;

    private int selectedPosition = -1;
    private List<MemoItemList> mMemoData;

    private AdapterComponent.CCViewHolder.OnAdapterListner mLisner;
    private FragmentMemo.onCompletItem mListner;
    private int mPosition;
    private String mType;

    public AdapterMemoEdit(Context context, FragmentMemo.onCompletItem listner, AdapterComponent.CCViewHolder.OnAdapterListner lisner, int position, String type) {
        this.mContext = context;
        this.mListner = listner;
        this.mLisner = lisner;
        this.mPosition = position;
        this.mType = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_memo, parent, false);
        RecyclerView.ViewHolder holder = new AdapterComponent.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        MemoItemList memoItemList = mMemoData.get(position);

        ArrayList<String> memoDatas = new ArrayList<>();
        CheckBox checkBoxBg = (CheckBox) holder.itemView.findViewById(R.id.checkbox_bg);
        final TextView tvItemRegistration = (TextView) holder.itemView.findViewById(R.id.tv_item_registration);
        tvItemRegistration.setText(memoItemList.getMemo());

        ArrayList<String> itemEditItemRepairs = ManagerMemo.getInstance().getManagerMemo();

        for (String memoData : itemEditItemRepairs) {
            if (memoData.equals(mMemoData.get(position).getMemo())) {
                checkBoxBg.setBackgroundResource(R.drawable.a_button_type_2);
                tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                memoDatas.add(memoData);
                ManagerMemo.getInstance().setTManagerMemo(memoDatas);
            }
        }

        checkBoxBg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    mListner.setSelectItem(tvItemRegistration.getText().toString());
                } else {
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
                    mListner.setUnSelectItem(tvItemRegistration.getText().toString());
                }
            }
        });
    }

    /**
     * 이전 선택된 Item View Check
     *
     * @param tvItemRegistration
     */
    private void viewSelecitionPrevious(CheckBox checkBoxBg, TextView tvItemRegistration, String memoName) {
        try {
            ArrayList<String> memoDataList = new ArrayList<>();
            if (ManagerItemAdditionalData.getInstance().getItemAdditionalData().get(mPosition).getMemoData() != null) {
                String memoData = ManagerItemAdditionalData.getInstance().getItemAdditionalData().get(mPosition).getMemoData();

                String[] array1 = memoData.split("\\|");
                for (String meno : array1) {
                    memoDataList.add(meno);

                    for (String memoDatas : memoDataList) {
                        if (memoDatas.equals(memoName)) {
                            checkBoxBg.setChecked(true);
                            ManagerMemo.getInstance().setTManagerMemo(memoDataList);
                            tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                        }
                    }
                }
            }
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mMemoData == null ? 0 : mMemoData.size();
    }

    public void addItem(List<MemoItemList> memoData) {
        mMemoData = memoData;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterComponent.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterComponent.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}