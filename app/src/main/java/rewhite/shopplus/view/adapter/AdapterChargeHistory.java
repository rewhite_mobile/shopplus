package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetPrePaidUseHistory;
import rewhite.shopplus.util.DataFormatUtil;

public class AdapterChargeHistory extends BaseAdapter {
    private static final String TAG = "AdapterChargeHistory";

    private List<GetPrePaidUseHistory> mList = new ArrayList<>();
    private Context mContext;

    public AdapterChargeHistory(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        GetPrePaidUseHistory getPrePaidUseHistory = mList.get(position);
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_adapter_charge_history, parent, false);
        }

        TextView tvDateUse = (TextView) v.findViewById(R.id.tv_date_use);
        TextView tvUsageInformation = (TextView) v.findViewById(R.id.tv_usage_information);
        TextView tvAmountUsed = (TextView) v.findViewById(R.id.tv_amount_used);

        try {
            tvDateUse.setText(DataFormatUtil.setDateMothFormat(getPrePaidUseHistory.getApplyDate()));
            tvUsageInformation.setText(getPrePaidUseHistory.getChangeTypeDesc());
            tvAmountUsed.setText(DataFormatUtil.moneyFormatToWon(getPrePaidUseHistory.getAmount()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return v;
    }

    public void addItem(List<GetPrePaidUseHistory> item) {
        mList = item;
    }
}
