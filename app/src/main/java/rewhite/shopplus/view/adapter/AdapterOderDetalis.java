package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetOrderItemList;
import rewhite.shopplus.common.popup.activity.ActivityOrderDetails;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;

public class AdapterOderDetalis extends BaseAdapter {
    private static final String TAG = "AdapterOderDetalis";

    private Context mContext;
    private List<GetOrderItemList> mItemList;

    private ActivityOrderDetails.orderItemDetail mListner;
    public AdapterOderDetalis(Context context, ActivityOrderDetails.orderItemDetail listner) {
        this.mContext = context;
        this.mListner = listner;
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_oder_detalis, parent, false);
        }
        GetOrderItemList getOrderItemList = mItemList.get(position);

        TextView tvTagNumber = (TextView) v.findViewById(R.id.tv_tag_number);
        ImageView imgTacColor = (ImageView) v.findViewById(R.id.img_tac_color);
        TextView tvItemName = (TextView) v.findViewById(R.id.tv_item_name);
        TextView tvReceptionDate = (TextView) v.findViewById(R.id.tv_reception_date);
        TextView tvShipDate = (TextView) v.findViewById(R.id.tv_ship_date);
        TextView tvPaymentAmount = (TextView) v.findViewById(R.id.tv_payment_amount);

        try {
            int idx = getOrderItemList.getTagNo().indexOf("|");
            if (idx == 0) {
                tvTagNumber.setText(CommonUtil.setTagFormater(getOrderItemList.getTagNo()));
            } else if (getOrderItemList.getTagNo().length() >= 6 && getOrderItemList.getTagNo().length() < 9) {
                tvTagNumber.setText(CommonUtil.setFormater(getOrderItemList.getTagNo()));
            } else if (getOrderItemList.getTagNo().contains("-")) {
                tvTagNumber.setText(CommonUtil.setTagFormaterChange(getOrderItemList.getTagNo()));
            }
        } catch (Exception err) {
            err.printStackTrace();
        }

        CommonUtil.getTacColor(mContext, Integer.valueOf(getOrderItemList.getTagColor()), imgTacColor);
        tvItemName.setText(getOrderItemList.getStoreItemName());
        try {
            tvReceptionDate.setText(DataFormatUtil.setDateItemCancelFormat(getOrderItemList.getEnterDate()));
            if (!TextUtils.isEmpty(getOrderItemList.getOutDate()) || getOrderItemList.getOutDate() != null) {
                tvShipDate.setText(DataFormatUtil.setDateItemCancelFormat(getOrderItemList.getOutDate()));
            } else {
                tvShipDate.setText("-");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Logger.d(TAG, "getOrderItemList.getPayPrice() : " + getOrderItemList.getOrderItemPrice());
        tvPaymentAmount.setText(DataFormatUtil.moneyFormatToWon(getOrderItemList.getOrderItemPrice()));


        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListner.getItemDetail(position);
            }
        });
        return v;
    }


    public void ItemAdd(List<GetOrderItemList> item) {
        mItemList = item;
    }
}
