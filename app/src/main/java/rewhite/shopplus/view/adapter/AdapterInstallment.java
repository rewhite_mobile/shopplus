package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;

public class AdapterInstallment extends RecyclerView.Adapter {
    private static final String TAG = "AdapterInstallment";
    private Context mContext;
    private int selectedPosition = 0;
    private List<String> mListItems = new ArrayList<>();
    private AdapterInstallment.CCViewHolder.OnAdapterListner mLisner;

    public AdapterInstallment(Context context, AdapterInstallment.CCViewHolder.OnAdapterListner lisner) {
        this.mContext = context;
        this.mLisner = lisner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_installment, parent, false);
        RecyclerView.ViewHolder holder = new AdapterInstallment.CCViewHolder(v, mLisner);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        String installmentNumber = mListItems.get(position);

        TextView tvNumber = (TextView) holder.itemView.findViewById(R.id.tv_number);
        setSelectedPosition(position, tvNumber);

        tvNumber.setText(installmentNumber.toString());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                mLisner.onClick(Integer.valueOf(mListItems.get(position)));
                notifyDataSetChanged();
            }
        });
    }
    /**
     * 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setSelectedPosition(int position, TextView tvNumber) {
        if (selectedPosition == position) {
            tvNumber.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            tvNumber.setBackgroundResource(R.drawable.a_button_type_2);
        } else {
            tvNumber.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
            tvNumber.setBackgroundResource(R.drawable.a_button_type_3);
        }
    }

    @Override
    public int getItemCount() {
        return mListItems == null ? 0 : mListItems.size();
    }

    public void addItem(List<String> listItems) {
        mListItems = listItems;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterInstallment.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterInstallment.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}