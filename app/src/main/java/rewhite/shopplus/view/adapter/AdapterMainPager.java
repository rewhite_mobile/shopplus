package rewhite.shopplus.view.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import rewhite.shopplus.fragment.main.FragmentCharacterManagement;
import rewhite.shopplus.fragment.main.FragmentCollectionManagement;
import rewhite.shopplus.fragment.main.FragmentHome;
import rewhite.shopplus.fragment.main.FragmentOperationsManagement;
import rewhite.shopplus.fragment.main.FragmentOrderInquiry;
import rewhite.shopplus.fragment.main.FragmentRewhiteMall;
import rewhite.shopplus.fragment.main.FragmentSalesStatus;

/**
 * MainActivity ViewPager Fragment SettingAdapter
 */
public class AdapterMainPager extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterMainPager";
    private AppCompatActivity mActivity;

    public AdapterMainPager(FragmentManager fm, AppCompatActivity activity) {
        super(fm);
        this.mActivity = activity;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentHome(mActivity);// Home
            case 1:
                return new FragmentOrderInquiry();// 주문조회
            case 2:
                return new FragmentCollectionManagement();// 수거배송관리
            case 3:
                return new FragmentOperationsManagement();// 매출현황
            case 4:
                return new FragmentSalesStatus();// 운영관리
            case 5:
                return new FragmentCharacterManagement();// 문자관리
            case 6:
                return new FragmentRewhiteMall();// 리화이트몰
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 7;
    }
}