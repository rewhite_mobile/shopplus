package rewhite.shopplus.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import rewhite.shopplus.fragment.sub.FragmentAcountsReceivableManagement;
import rewhite.shopplus.fragment.sub.FragmentCardPaymentManagement;
import rewhite.shopplus.fragment.sub.FragmentMembershipManagement;
import rewhite.shopplus.fragment.sub.FragmentNonIssueManagement;
import rewhite.shopplus.fragment.sub.FragmentPreChargeManagement;

public class AdapterOperationManagement extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterOperationManagement";

    public AdapterOperationManagement(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentMembershipManagement();        //회원관리
            case 1:
                return new FragmentAcountsReceivableManagement(); //미수금관리
            case 2:
                return new FragmentNonIssueManagement();          //미출고관리
            case 3:
                return new FragmentCardPaymentManagement();       //카드결제관리
            case 4:
                return new FragmentPreChargeManagement();         //선충전금관리
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 5;
    }
}