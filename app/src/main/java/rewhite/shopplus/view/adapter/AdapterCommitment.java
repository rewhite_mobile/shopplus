package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.UserOperationModel;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 운영관리 -> 미수 품목 관리 ViewAdapter
 */
public class AdapterCommitment extends BaseAdapter {
    private static final String TAG = "AdapterCommitment";
    private static final String ORDER_ITEM_ID = "OrderItemID";    //주문 아이템 ID
    private static final String ORDER_ID = "order_id";  //주문ID
    private Context mContext;

    private List<UserOperationModel> mGetVisitOrderList;
    private UserOperationModel userManageList;

    public AdapterCommitment(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mGetVisitOrderList.size();
    }

    @Override
    public Object getItem(int position) {
        return mGetVisitOrderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        userManageList = mGetVisitOrderList.get(position);
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_commit_ment, parent, false);
        }
        setLayout(v, userManageList);
        return v;
    }

    /**
     * Setting Layout View
     *
     * @param v
     */
    private void setLayout(View v, UserOperationModel userManageList) {
        TextView tvMemberName = (TextView) v.findViewById(R.id.tv_member_name);
        TextView tvMemberPhoneNumber = (TextView) v.findViewById(R.id.tv_member_phone_number);
        TextView tvMemberAddress = (TextView) v.findViewById(R.id.tv_member_address);
        TextView tvTagNumber = (TextView) v.findViewById(R.id.tv_tag_number);
        ImageView imgTacColor = (ImageView) v.findViewById(R.id.img_tac_color);

        TextView tvItemName = (TextView) v.findViewById(R.id.tv_item_name);
        TextView tvTagTypeA = (TextView) v.findViewById(R.id.tv_tag_type_a);
        TextView tvTagTypeE = (TextView) v.findViewById(R.id.tv_tag_type_e);
        TextView tvRegDate = (TextView) v.findViewById(R.id.tv_reg_date);
        TextView tvOutDate = (TextView) v.findViewById(R.id.tv_out_date);
        TextView tvOrderItemStatusDesc = (TextView) v.findViewById(R.id.tv_order_item_status_desc);
        TextView tvOrderItemPrice = (TextView) v.findViewById(R.id.tv_order_item_price);
        TextView tvNonPayPrice = (TextView) v.findViewById(R.id.tv_non_pay_price);

        tvMemberName.setText(userManageList.getUserName());
        tvMemberPhoneNumber.setText(userManageList.get_UserPhone());
        tvMemberAddress.setText(userManageList.get_UserAddress());

        try {
            int idx = userManageList.getTagNo().indexOf("|");
            if (idx == 0) {
                tvTagNumber.setText(CommonUtil.setTagFormater(userManageList.getTagNo()));
            } else if (userManageList.getTagNo().length() >= 6 && userManageList.getTagNo().length() < 9) {
                tvTagNumber.setText(CommonUtil.setFormater(userManageList.getTagNo()));
            } else if (userManageList.getTagNo().contains("-")) {
                tvTagNumber.setText(CommonUtil.setTagFormaterChange(userManageList.getTagNo()));
            }
        } catch (StringIndexOutOfBoundsException err) {
            err.printStackTrace();
        }

        CommonUtil.setTacImgColor(Integer.valueOf(userManageList.getTagColor()), imgTacColor);
        tvItemName.setText(userManageList.getStoreItemName());
        tvTagTypeA.setText("수선");
        tvTagTypeE.setText("+" + userManageList.getOptions().size());

        try {
            tvRegDate.setText(DataFormatUtil.setDateItemCancelFormat(userManageList.getEnterDate()));
            if (!TextUtils.isEmpty(userManageList.getOutDate()) || userManageList.getOutDate() != null) {
                tvOutDate.setText(DataFormatUtil.setDateItemCancelFormat(userManageList.getOutDate()));
            } else {
                tvOutDate.setText("-");
            }
            tvOrderItemStatusDesc.setText(DataFormatUtil.setDateItemCancelFormat(userManageList.getOrderItemStatusDesc()));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception err) {
            err.printStackTrace();
        }

        try {
            tvOrderItemPrice.setText(DataFormatUtil.moneyFormatToWon(userManageList.getStoreItemPrice()));
            tvNonPayPrice.setText(DataFormatUtil.moneyFormatToWon(userManageList.getNonPayPrice()));
        } catch (NullPointerException err) {
            err.printStackTrace();
        }


    }


    public void addItem(List<UserOperationModel> getVisitOrderLists) {
        mGetVisitOrderList = getVisitOrderLists;
    }

}