package rewhite.shopplus.view.adapter.enviroment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.fragment.enviroment.FragmentOpeningHours;

/**
 * 정기휴무일 Adapter
 */
public class AdapterWeek extends RecyclerView.Adapter {
    private static final String TAG = "AdapterWeek";
    private Context mContext;

    private AdapterWeek.CCViewHolder.OnAdapterListner mLisner;
    private ArrayList<String> mWeek;
    private String[] array;

    private String monday = "0", tuesday = "0", wednesday = "0", thursday = "0", friday = "0", saturday = "0", sunday = "0";
    private FragmentOpeningHours.onCompletItem mOnCompletItem;
    private String mSelectItemCheck;

    public AdapterWeek(Context context, AdapterWeek.CCViewHolder.OnAdapterListner lisner, FragmentOpeningHours.onCompletItem onCompletItem, String selectItemCheck) {
        this.mContext = context;
        this.mLisner = lisner;
        this.mOnCompletItem = onCompletItem;
        this.mSelectItemCheck = selectItemCheck;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_week, parent, false);
        RecyclerView.ViewHolder holder = new AdapterWeek.CCViewHolder(v, mLisner);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        CheckBox checkBox = (CheckBox) holder.itemView.findViewById(R.id.cb_sunday);
        checkBox.setText(mWeek.get(position).toString());

        array = mSelectItemCheck.split("\\,");

        for (String test : array) {
            if (test.contains("월") && position == 0) {
                checkBox.setBackgroundResource(R.drawable.a_button_type_2);
                checkBox.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            } else if (test.contains("화") && position == 1) {
                checkBox.setBackgroundResource(R.drawable.a_button_type_2);
                checkBox.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            } else if (test.contains("수") && position == 2) {
                checkBox.setBackgroundResource(R.drawable.a_button_type_2);
                checkBox.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            } else if (test.contains("목") && position == 3) {
                checkBox.setBackgroundResource(R.drawable.a_button_type_2);
                checkBox.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            } else if (test.contains("금")&& position == 4) {
                checkBox.setBackgroundResource(R.drawable.a_button_type_2);
                checkBox.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            } else if (test.contains("토")&& position == 5) {
                checkBox.setBackgroundResource(R.drawable.a_button_type_2);
                checkBox.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            } else if (test.contains("일")&& position == 6) {
                checkBox.setBackgroundResource(R.drawable.a_button_type_2);
                checkBox.setTextColor(mContext.getResources().getColor(R.color.Wite_color));

            }
        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkBox.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    setSelectClick(position);
                    mOnCompletItem.setSelectItem(monday + tuesday + wednesday + thursday + friday + saturday + sunday);
                } else {
                    checkBox.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
                    setUnSelectClick(position);
                    mOnCompletItem.setSelectItem(monday + tuesday + wednesday + thursday + friday + saturday + sunday);
                }
            }
        });
    }

    /**
     * 체크 선택
     *
     * @param position
     */
    private void setSelectClick(int position) {
        switch (position) {
            case 0:
                monday = "1";
                break;
            case 1:
                tuesday = "1";
                break;
            case 2:
                wednesday = "1";
                break;
            case 3:
                thursday = "1";
                break;
            case 4:
                friday = "1";
                break;
            case 5:
                saturday = "1";
                break;
            case 6:
                sunday = "1";
                break;
        }
    }

    /**
     * 체크 해지
     *
     * @param position
     */
    private void setUnSelectClick(int position) {
        switch (position) {
            case 0:
                monday = "0";
                break;
            case 1:
                tuesday = "0";
                break;
            case 2:
                wednesday = "0";
                break;
            case 3:
                thursday = "0";
                break;
            case 4:
                friday = "0";
                break;
            case 5:
                saturday = "0";
                break;
            case 6:
                sunday = "0";
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mWeek.size();
    }

    public void itemAdd(ArrayList<String> week) {
        mWeek = week;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterWeek.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterWeek.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
