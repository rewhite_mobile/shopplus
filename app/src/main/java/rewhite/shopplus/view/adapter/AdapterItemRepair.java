package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.data.dto.ItemAdditionalData;
import rewhite.shopplus.data.dto.ItemEditItemRepair;
import rewhite.shopplus.data.manager.ManagerEditItemRepair;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 추가된 수선 ListAdpater View
 */
public class AdapterItemRepair extends BaseAdapter {
    private static final String TAG = "AdapterItemRepair";

    private Context mContext;

    private ArrayList<ItemAdditionalData.ItemRepairFreeEdit> mAllVisitsReceiveds;

    private  ArrayList<ItemAdditionalData.OrderItemOptionInput> mallVisitsReceiveds;
    public AdapterItemRepair(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mAllVisitsReceiveds.size();
    }

    @Override
    public Object getItem(int position) {
        return mAllVisitsReceiveds.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ItemAdditionalData.ItemRepairFreeEdit itemComponent = mAllVisitsReceiveds.get(position);

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_adapter_repair, parent, false);

            TextView tvTitleName = (TextView) v.findViewById(R.id.tv_title_name);
            TextView tvAmount = (TextView) v.findViewById(R.id.tv_amount);

            tvTitleName.setText(itemComponent.getTitleName());
            tvAmount.setText(DataFormatUtil.moneyFormatToWon(Integer.valueOf(itemComponent.getAmount())) + "원");

            ArrayList<ItemEditItemRepair> itemRepairs = new ArrayList<>();
            ItemEditItemRepair ItemEditItemRepair = new ItemEditItemRepair();

            ItemEditItemRepair.setOrderItemName(itemComponent.getTitleName());
            ItemEditItemRepair.setOrderItemAmount(String.valueOf(itemComponent.getAmount()));
            itemRepairs.add(ItemEditItemRepair);
            ManagerEditItemRepair.getmInstance().setManagerItemRepair(itemRepairs);
        }

        return v;
    }

    public void addItem(ArrayList<ItemAdditionalData.ItemRepairFreeEdit> allVisitsReceiveds) {
        mAllVisitsReceiveds = allVisitsReceiveds;
    }
}
