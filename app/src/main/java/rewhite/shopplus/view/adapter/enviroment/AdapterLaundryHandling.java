package rewhite.shopplus.view.adapter.enviroment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetHandleItemList;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.fragment.enviroment.FragmentLaundryHandling;

/**
 * 세탁물 취급설정 Adapter
 */
public class AdapterLaundryHandling extends BaseAdapter {
    private static final String TAG = "AdapterLaundryHandling";
    private Context mContext;

    private List<GetHandleItemList> mItemList;
    private RadioButton btnTreatment;
    private RadioButton btnUntreatment;
    private RadioGroup radioGroup;

    private FragmentLaundryHandling.onCheckedClick mListener;

    public AdapterLaundryHandling(Context context, FragmentLaundryHandling.onCheckedClick listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_laundry_handling, parent, false);

        GetHandleItemList getHandleItemList = mItemList.get(position);
        TextView tvItemName = (TextView) view.findViewById(R.id.tv_item_name);
        TextView tvItemOperating = (TextView) view.findViewById(R.id.tv_item_operating);

        radioGroup = (RadioGroup) view.findViewById(R.id.rg_handling_treatment);
        btnTreatment = (RadioButton) view.findViewById(R.id.rb_handling_treatment);
        btnUntreatment = (RadioButton) view.findViewById(R.id.rb_handling_untreatment);
        radioGroup.setTag(position);

        if (getHandleItemList.getIsUse().equals("Y")) {
            btnTreatment.setBackgroundResource(R.drawable.a_button_type_2);
            btnUntreatment.setBackgroundResource(R.drawable.a_button_type_3);

            btnTreatment.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            btnUntreatment.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
        } else {
            btnUntreatment.setBackgroundResource(R.drawable.a_button_type_2);
            btnTreatment.setBackgroundResource(R.drawable.a_button_type_3);

            btnUntreatment.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            btnTreatment.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
        }

        tvItemName.setText(getHandleItemList.getItemGroupTypeDesc());
        tvItemOperating.setText(getHandleItemList.getItemGroupName());

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                boolean isChecked = (checkedId == R.id.rb_handling_treatment);

                if (isChecked) {
                    EnviromentManager.getSetHandleItem(mContext, mItemList.get(position).getItemGroupID(), "Y");
                } else {
                    EnviromentManager.getSetHandleItem(mContext, mItemList.get(position).getItemGroupID(), "N");
                }
                mListener.onChecked(1);
            }
        });
        return view;
    }

    public void addItem(List<GetHandleItemList> itemList) {
        mItemList = itemList;
    }
}