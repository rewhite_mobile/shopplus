package rewhite.shopplus.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import rewhite.shopplus.fragment.sub.FragmentChargeHistory;
import rewhite.shopplus.fragment.sub.FragmentUsageHistory;

/**
 * 선충전금 충전내역 Adapter
 */
public class AdapterPreFilledGold extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterPreFilledGold";

    private AppCompatActivity mActivity;
    private int mUserStoreId;

    public AdapterPreFilledGold(FragmentManager fm, AppCompatActivity activity, int userStoreId) {
        super(fm);
        this.mActivity = activity;
        this.mUserStoreId = userStoreId;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentUsageHistory(mUserStoreId);  //충전내역
            case 1:
                return new FragmentChargeHistory(mActivity, mUserStoreId); //사용내역
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
