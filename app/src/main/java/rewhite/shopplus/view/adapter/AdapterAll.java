package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityEditItem;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.common.popup.activity.ActivityDetailsPaymentInformation;
import rewhite.shopplus.fragment.sub.FragmentAll;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 방문접수 전체
 */
public class AdapterAll extends BaseAdapter {
    private static final String TAG = "AdapterAll";
    private static final String ORDER_ITEM_ID = "OrderItemID";    //주문 아이템 ID
    private static final String ORDER_ID = "order_id";  //주문ID
    private Context mContext;

    private List<GetVisitOrderList> mGetVisitOrderList;
    private GetVisitOrderList getVisitOrderList;

    private int selectPosition;
    private boolean mAllCheckStatus;
    private FragmentAll.OnAdapterListner mListener;
    private ImageView ivPaymentInfo;

    private TextView tvTagNumber;
    private TextView tvItemName;
    private TextView tvRegDate;
    private TextView tvOutDate;
    private TextView tvOrderItemStatusDesc;
    private TextView tvOrderItemPrice;
    private TextView tvNonPayPrice;
    private RecyclerView recyclerView;
    private View lineView;

    private boolean mPaymentStatus;
    private ImageView imgTacColor;

    public AdapterAll(Context context, boolean allCheckStatus, FragmentAll.OnAdapterListner listner) {
        this.mContext = context;
        this.mAllCheckStatus = allCheckStatus;
        this.mListener = listner;
    }

    @Override
    public int getCount() {
        return mGetVisitOrderList.size();
    }

    @Override
    public Object getItem(int position) {
        return mGetVisitOrderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        selectPosition = position;
        getVisitOrderList = mGetVisitOrderList.get(position);
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_member_all_visits, parent, false);
        }
        setLayout(v, position, getVisitOrderList);
        return v;
    }

    /**
     * Setting Layout View
     *
     * @param v
     */
    private void setLayout(View v, int position, GetVisitOrderList getVisitOrderList) {
        tvTagNumber = (TextView) v.findViewById(R.id.tv_tag_number);
        tvItemName = (TextView) v.findViewById(R.id.tv_item_name);
        tvRegDate = (TextView) v.findViewById(R.id.tv_reg_date);
        tvOutDate = (TextView) v.findViewById(R.id.tv_out_date);
        tvOrderItemStatusDesc = (TextView) v.findViewById(R.id.tv_order_item_status_desc);
        tvOrderItemPrice = (TextView) v.findViewById(R.id.tv_order_item_price);
        tvNonPayPrice = (TextView) v.findViewById(R.id.tv_non_pay_price);
        recyclerView = (RecyclerView) v.findViewById(R.id.rv_tag);
        imgTacColor = (ImageView) v.findViewById(R.id.img_tac_color);
        ivPaymentInfo = (ImageView) v.findViewById(R.id.iv_payment_info);
        lineView = (View) v.findViewById(R.id.line_view);
        LinearLayout llItem = (LinearLayout) v.findViewById(R.id.ll_item);
        LinearLayout llBillingInformation = (LinearLayout) v.findViewById(R.id.ll_billing_nformation);
        CheckBox chSelect = (CheckBox) v.findViewById(R.id.ch_select);

        //미수금 X && 출고 o 되었을 경우 View
        if (getVisitOrderList.getPayPrice() == 0) {
            lineView.setVisibility(View.GONE);
            llBillingInformation.setVisibility(View.GONE);
        } else {
            lineView.setVisibility(View.VISIBLE);
            llBillingInformation.setVisibility(View.VISIBLE);
        }

        setTextView(v, position);
        setTagAdapter(v);

        if (getVisitOrderList.getPayPrice() != 0 && getVisitOrderList.getOrderItemStatusDesc().equals("출고완료") || getVisitOrderList.getOrderItemStatusDesc().equals("취소(유저)")) {
            chSelect.setBackgroundResource(R.drawable.btn_ltemdetails_checkbox_disabled);
        } else {
            chSelect.setBackgroundResource(R.drawable.a_check_box_type_2);

            chSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        mListener.setSelectItem(mGetVisitOrderList.get(position), mGetVisitOrderList.get(position).getNonPayPrice(), mGetVisitOrderList.get(position).getPayPrice());
                    } else {
                        mListener.setUnSelectItem(mGetVisitOrderList.get(position), mGetVisitOrderList.get(position).getNonPayPrice(), mGetVisitOrderList.get(position).getPayPrice());
                    }

                    if (mAllCheckStatus == true) {
                        if (isChecked == false) {
                            mListener.onAllCategoryItem(false);
                        }
                    }
                }
            });
        }

        if (mAllCheckStatus == true) {
            chSelect.setChecked(mAllCheckStatus);
            mListener.onAllCategory(mGetVisitOrderList);
        } else {
            mListener.unAllCategory(mGetVisitOrderList);
        }

        llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityEditItem.class);
                intent.putExtra(ORDER_ID, getVisitOrderList.getOrderId());
                mContext.startActivity(intent);
            }
        });

        llBillingInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ActivityDetailsPaymentInformation.class);
                intent.putExtra(ORDER_ITEM_ID, getVisitOrderList.getOrderItemId());
                mContext.startActivity(intent);
            }
        });
    }

    private void setTagAdapter(View view) {
        ArrayList<String> testCode = new ArrayList<>();
        for (int i = 0; i < getVisitOrderList.getBadges().size(); i++) {
            testCode.add(getVisitOrderList.getBadges().get(i).toString());
        }

        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(6, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        AdapterCommonTag adapterCommonTag = new AdapterCommonTag(mContext, mOnAdapterListner);
        adapterCommonTag.addItem(testCode);
        recyclerView.setAdapter(adapterCommonTag);
    }

    /**
     * TextView Setting
     *
     * @param view
     * @param position
     */
    private void setTextView(View view, int position) {
        CommonUtil.getTacColor(mContext, Integer.valueOf(mGetVisitOrderList.get(position).getTagColor()), imgTacColor);
        try {
            int idx = mGetVisitOrderList.get(position).getTagNo().indexOf("|");
            if (idx == 0) {
                tvTagNumber.setText(CommonUtil.setTagFormater(mGetVisitOrderList.get(position).getTagNo()));
            } else if (mGetVisitOrderList.get(position).getTagNo().length() >= 6 && mGetVisitOrderList.get(position).getTagNo().length() < 9) {
                tvTagNumber.setText(CommonUtil.setFormater(mGetVisitOrderList.get(position).getTagNo()));
            } else if (mGetVisitOrderList.get(position).getTagNo().contains("-")) {
                tvTagNumber.setText(CommonUtil.setTagFormaterChange(mGetVisitOrderList.get(position).getTagNo()));
            }

            tvItemName.setText(mGetVisitOrderList.get(position).getStoreItemName());

            tvRegDate.setText(DataFormatUtil.setDateFormat(mGetVisitOrderList.get(position).getEnterDate()));
            if (mGetVisitOrderList.get(position).getOutDate() != null || !TextUtils.isEmpty(mGetVisitOrderList.get(position).getOutDate())) {
                tvOutDate.setText(DataFormatUtil.setDateFormat(mGetVisitOrderList.get(position).getOutDate()));
            } else {
                tvOutDate.setText("-");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException err) {
            err.printStackTrace();
        }
        tvOrderItemStatusDesc.setText(mGetVisitOrderList.get(position).getOrderItemStatusDesc());

        if (mGetVisitOrderList.get(position).getPayPrice() == 0) {
            tvOrderItemPrice.setText("-");
        } else {
            tvOrderItemPrice.setText(DataFormatUtil.moneyFormatToWon(mGetVisitOrderList.get(position).getPayPrice()));
        }

        if (mGetVisitOrderList.get(position).getNonPayPrice() == 0) {
            tvNonPayPrice.setText("-");
        } else {
            tvNonPayPrice.setText(DataFormatUtil.moneyFormatToWon(mGetVisitOrderList.get(position).getNonPayPrice()));
        }
    }


    public void addItem(List<GetVisitOrderList> getVisitOrderLists) {
        mGetVisitOrderList = getVisitOrderLists;
    }

    private AdapterCommonTag.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterCommonTag.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
}