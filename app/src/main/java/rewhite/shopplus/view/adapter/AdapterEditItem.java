package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityEditItem;
import rewhite.shopplus.client.model.GetOrderItemList;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 품목수정 Adapter
 */
public class AdapterEditItem extends BaseAdapter {
    private static final String TAG = "AdapterEditItem";
    private Context mContext;
    private List<GetOrderItemList> mOrderItemList;

    private TextView tvTacNumber, tvItem;
    private ImageView imgTacColor;
    private TextView tvTagName, tvTagName2;
    private TextView tvPaymentAmount, tvPaymentAttemptAmount, tvPaymentStaet;

    private LinearLayout llCancle;
    private RecyclerView recyclerView;

    private ActivityEditItem.setAddPosition mListner;

    public AdapterEditItem(Context context, ActivityEditItem.setAddPosition lisner) {
        this.mContext = context;
        this.mListner = lisner;
    }

    @Override
    public int getCount() {
        return mOrderItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mOrderItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = vi.inflate(R.layout.item_edit_item, parent, false);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListner.selectPosition(position);
            }
        });
        setLayout(view);
        setTextView(position);

        return view;
    }

    /**
     * 레이아웃 셋팅 View
     *
     * @param view
     */
    private void setLayout(View view) {
        tvTacNumber = (TextView) view.findViewById(R.id.tv_tac_number);
        tvItem = (TextView) view.findViewById(R.id.tv_item);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_tag);
        tvTagName = (TextView) view.findViewById(R.id.tv_tag_name);
        tvTagName2 = (TextView) view.findViewById(R.id.tv_tag_name2);
        tvPaymentStaet = (TextView) view.findViewById(R.id.tv_payment_staet);
        tvPaymentAmount = (TextView) view.findViewById(R.id.tv_payment_amount);
        tvPaymentAttemptAmount = (TextView) view.findViewById(R.id.tv_payment_attempt_amount);

        imgTacColor = (ImageView) view.findViewById(R.id.img_tac_color);
        llCancle = (LinearLayout) view.findViewById(R.id.ll_cancle);
    }

    /**
     * TextView Settting
     *
     * @param position
     */
    private void setTextView(int position) {

        int idx = mOrderItemList.get(position).getTagNo().indexOf("|");
        if (idx == 0) {
            tvTacNumber.setText(CommonUtil.setTagFormater(mOrderItemList.get(position).getTagNo()));
        } else if (mOrderItemList.get(position).getTagNo().length() >= 6 && mOrderItemList.get(position).getTagNo().length() < 9) {
            tvTacNumber.setText(CommonUtil.setFormater(mOrderItemList.get(position).getTagNo()));
        } else if (mOrderItemList.get(position).getTagNo().contains("-")) {
            tvTacNumber.setText(CommonUtil.setTagFormaterChange(mOrderItemList.get(position).getTagNo()));
        }

        CommonUtil.setTacImgColor(Integer.valueOf(mOrderItemList.get(0).getTagColor()), imgTacColor);

        tvItem.setText(mOrderItemList.get(position).getStoreItemName());

        tvPaymentStaet.setText(mOrderItemList.get(position).getOrderItemStatusDesc());
        tvPaymentAmount.setText(DataFormatUtil.moneyFormatToWon(mOrderItemList.get(position).getOrderItemPrice()));
        tvPaymentAttemptAmount.setText(DataFormatUtil.moneyFormatToWon(mOrderItemList.get(position).getNonPayPrice()));
        tvTagName.setText(mOrderItemList.get(position).getBadges().get(0).toString());
        tvTagName2.setText("+" + mOrderItemList.get(position).getBadges().size());

        if (mOrderItemList.get(position).getPayPrice() != 0 || !TextUtils.isEmpty(mOrderItemList.get(position).getOutDate()) || !mOrderItemList.get(position).getOrderItemStatusDesc().equals("접수완료")) {
            llCancle.setVisibility(View.VISIBLE);

            llCancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(mContext);
                    thirdConfirmPopup.setTitle("취소 팝업");
                    thirdConfirmPopup.setContent("해당품목의 주문을 취소할까요?");
                    thirdConfirmPopup.setButtonText("예");
                    thirdConfirmPopup.setCancelButtonText("아니오");
                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                        @Override
                        public void onConfirmClick() {

                        }
                    });
                    thirdConfirmPopup.show();
                }
            });
        } else {
            llCancle.setVisibility(View.INVISIBLE);
        }
    }

    public void addItem(List<GetOrderItemList> orderItem) {
        mOrderItemList = orderItem;
    }

    private AdapterCommonTag.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterCommonTag.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
}
