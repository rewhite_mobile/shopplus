package rewhite.shopplus.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import rewhite.shopplus.fragment.sub.FragmentDailySalesStatus;
import rewhite.shopplus.fragment.sub.FragmentMonthlySalesStatus;

public class AdapterSalesStatus extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterSalesStatus";

    public AdapterSalesStatus(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentMonthlySalesStatus(); // 월별매출현황
            case 1:
                return new FragmentDailySalesStatus();   //일별매출현황
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}