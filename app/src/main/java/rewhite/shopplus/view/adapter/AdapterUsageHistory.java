package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetPrePaidUseHistory;
import rewhite.shopplus.util.DataFormatUtil;

public class AdapterUsageHistory extends BaseAdapter {
    private static final String TAG = "AdapterUsageHistory";

    private List<GetPrePaidUseHistory> mList = new ArrayList<>();
    private Context mContext;

    public AdapterUsageHistory(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        GetPrePaidUseHistory getPrePaidUseHistory = mList.get(position);
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_adapter_usage_history, parent, false);
        }
        TextView tvHistoryDate = (TextView) v.findViewById(R.id.tv_history_date);
        TextView tvCharge = (TextView) v.findViewById(R.id.tv_charge);
        TextView tvCard = (TextView) v.findViewById(R.id.tv_card);
        TextView tvCardInstallment = (TextView) v.findViewById(R.id.tv_card_installment);
        TextView tvInstallmentMonth = (TextView) v.findViewById(R.id.tv_installment_month);
        TextView tvPaymentCancle = (TextView) v.findViewById(R.id.tv_payment_cancle);
        TextView tvAgreeCancle = (TextView) v.findViewById(R.id.tv_agree_cancle);

        try {
            tvHistoryDate.setText(DataFormatUtil.setDateMothFormat(getPrePaidUseHistory.getApplyDate()));
            tvCharge.setText(getPrePaidUseHistory.getChangeTypeDesc());
            tvCard.setText(getPrePaidUseHistory.getPaymentDetails().get(0).getPaymentTypeDesc());

            if (getPrePaidUseHistory.getPaymentDetails().get(0).getCardDivide() > 1) {
                tvInstallmentMonth.setText("할부");
                tvCardInstallment.setText(getPrePaidUseHistory.getPaymentDetails().get(0).getCardDivide());

                tvInstallmentMonth.setVisibility(View.VISIBLE);
                tvCardInstallment.setVisibility(View.VISIBLE);
            } else {
                tvInstallmentMonth.setVisibility(View.GONE);
                tvCardInstallment.setVisibility(View.GONE);
            }

            tvPaymentCancle.setText(DataFormatUtil.moneyFormatToWon(getPrePaidUseHistory.getPaymentPrice()) + " 원");
            tvAgreeCancle.setText(DataFormatUtil.moneyFormatToWon(getPrePaidUseHistory.getAmount()) + " 원");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (getPrePaidUseHistory.getChangeTypeDesc().equals("충전")) {
            tvCharge.setTextColor(mContext.getResources().getColor(R.color.color_ffa800));
        } else {
            tvCharge.setTextColor(mContext.getResources().getColor(R.color.color_ff2500));
        }

        return v;
    }

    public void addItem(List<GetPrePaidUseHistory> item) {
        mList = item;
    }
}