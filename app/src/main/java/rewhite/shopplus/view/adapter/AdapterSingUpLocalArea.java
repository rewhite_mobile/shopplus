package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.AreaDistrictListModel;
import rewhite.shopplus.common.popup.activity.ActivitySelectRegion;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.util.SharedPreferencesUtil;

public class AdapterSingUpLocalArea extends BaseAdapter {
    private static final String TAG = "AdapterSingUpLocal";
    private Context mContext;
    private List<AreaDistrictListModel> mItem;
    private ActivitySelectRegion.parnetArea mLIstener;


    public AdapterSingUpLocalArea(Context context, ActivitySelectRegion.parnetArea lIstener) {
        this.mContext = context;
        this.mLIstener = lIstener;
    }

    @Override
    public int getCount() {
        return mItem.size();
    }

    @Override
    public Object getItem(int position) {
        return mItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        AreaDistrictListModel areaDistrictListModel = mItem.get(position);

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_text_view, parent, false);
        }

        TextView tvLocal = (TextView) v.findViewById(R.id.tv_local);
        tvLocal.setText(areaDistrictListModel.getSiGuGun());

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLIstener.parentAreaId(areaDistrictListModel.getAreaID());

                SharedPreferencesUtil.putSharedPreference(mContext, PrefConstant.KEY_CITY, PrefConstant.NAME_CITY_ID, areaDistrictListModel.getAreaID());
                SharedPreferencesUtil.putSharedPreference(mContext, PrefConstant.KEY_CITY, PrefConstant.NAME_CITY, areaDistrictListModel.getSiGuGun());
            }
        });
        return v;
    }

    public void addItem(List<AreaDistrictListModel> item) {
        mItem = item;
    }
}
