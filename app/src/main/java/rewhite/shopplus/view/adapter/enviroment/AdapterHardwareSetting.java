package rewhite.shopplus.view.adapter.enviroment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import rewhite.shopplus.fragment.enviroment.FragmentHardwareSettings;
import rewhite.shopplus.fragment.enviroment.FragmentPaymentTerminals;
import rewhite.shopplus.fragment.enviroment.FragmentPrintSettings;

public class AdapterHardwareSetting extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterHardwareSetting";

    private AppCompatActivity mActivity;
    public AdapterHardwareSetting(FragmentManager fm, AppCompatActivity activity) {
        super(fm);
        this.mActivity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentPrintSettings(mActivity);    //인쇄설정
            case 1:
                return new FragmentHardwareSettings(mActivity); //하드웨어 설정
            case 2:
                return new FragmentPaymentTerminals(); //결제단말기 설정
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}