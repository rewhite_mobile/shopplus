package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.fragment.sub.FragmentLaundryCharge;
import rewhite.shopplus.util.SharedPreferencesUtil;

/**
 * 세탁요금표 선택 AdapterView
 */
public class AdapterLaundryCharge extends RecyclerView.Adapter {
    private static final String TAG = "AdapterLaundryCharge";
    private Context mContext;

    private AdapterComponent.CCViewHolder.OnAdapterListner mLisner;
    private FragmentLaundryCharge.onCompletItem mListner;

    private List<StoreItemList> storeItemLists;
    private ArrayList<Boolean> positionArray;

    private int RepairSelectedPosition = 0;
    private int basicSelectedPosition = 0;
    private int LuxurySelectedPosition = 0;
    private int ChildSelectedPosition = 0;

    private TextView tvItemRegistration;
    private CheckBox tvItemBasic;
    private CheckBox tvItemLuxury;
    private CheckBox tvItemChild;
    private TextView tvItemRepair;

    private int mClickType = 0;

    public AdapterLaundryCharge(Context context, FragmentLaundryCharge.onCompletItem listner, AdapterComponent.CCViewHolder.OnAdapterListner lisner) {
        this.mContext = context;
        this.mListner = listner;
        this.mLisner = lisner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_charge, parent, false);
        RecyclerView.ViewHolder holder = new AdapterComponent.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        positionArray = new ArrayList<Boolean>(storeItemLists.size());
        for (int i = 0; i < storeItemLists.size(); i++) {
            positionArray.add(false);
        }
        final boolean[] isChecked = {false};

        final StoreItemList storeItemList = storeItemLists.get(position);
        final LinearLayout llRootView = (LinearLayout) holder.itemView.findViewById(R.id.root_view);

        tvItemRegistration = (TextView) holder.itemView.findViewById(R.id.tv_item_registration);
        tvItemBasic = (CheckBox) holder.itemView.findViewById(R.id.tv_item_basic);
        tvItemLuxury = (CheckBox) holder.itemView.findViewById(R.id.tv_item_luxury);
        tvItemChild = (CheckBox) holder.itemView.findViewById(R.id.tv_item_child);
        tvItemRepair = (TextView) holder.itemView.findViewById(R.id.tv_item_repair);

        tvItemRegistration.setText(storeItemList.getItemName());
        tvItemBasic.setText(String.valueOf(storeItemList.getItemVisitPrice()));
        tvItemLuxury.setText(String.valueOf(storeItemList.getItemVisitPrice_a()));
        tvItemChild.setText(String.valueOf(storeItemList.getItemVisitPrice_b()));
        tvItemRepair.setText(mContext.getResources().getString(R.string.distance));

        mClickType = SharedPreferencesUtil.getIntSharedPreference(mContext, "1", "1");

        if (mClickType == 0) {
            tvItemBasic.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
            tvItemBasic.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_3));

            tvItemLuxury.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
            tvItemLuxury.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_3));

            tvItemRepair.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
            tvItemRepair.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_3));

            tvItemRepair.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
            tvItemRepair.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_3));
        } else if (mClickType == 1) {
            setBasicSelectedPosition(position);
        } else if (mClickType == 2) {
            setLuxurySelectedPosition(position);
        } else if (mClickType == 3) {
            setChildSelectedPosition(position);    //아동
        } else if (mClickType == 4) {
            setSelectedPosition(position);        //수선

        }
        tvItemBasic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isChecked[0] = true;
                mClickType = 1;
                basicSelectedPosition = position;
                SharedPreferencesUtil.putSharedPreference(mContext, "LaundrySelect", "LaundrySelect", 1);
                SharedPreferencesUtil.putSharedPreference(mContext, "basicSelectedPosition", "basicSelectedPosition", basicSelectedPosition);
                mListner.setSelectItem(storeItemLists.get(position).getItemName(), storeItemLists.get(position).getItemVisitPrice(), storeItemLists.get(position).getStoreItemID(),1);

                notifyDataSetChanged();
            }
        });


        tvItemLuxury.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isChecked[0] = true;
                mClickType = 2;
                LuxurySelectedPosition = position;
                SharedPreferencesUtil.putSharedPreference(mContext, "LaundrySelect", "LaundrySelect", 2);
                SharedPreferencesUtil.putSharedPreference(mContext, "LuxurySelectedPosition", "LuxurySelectedPosition", LuxurySelectedPosition);

                mListner.setSelectItem(storeItemLists.get(position).getItemName(), storeItemLists.get(position).getItemVisitPrice_a(), storeItemLists.get(position).getStoreItemID(),2);

                notifyDataSetChanged();
            }
        });

        tvItemChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isChecked[0] = true;
                mClickType = 3;
                ChildSelectedPosition = position;
                SharedPreferencesUtil.putSharedPreference(mContext, "LaundrySelect", "LaundrySelect", 3);
                SharedPreferencesUtil.putSharedPreference(mContext, "ChildSelectedPosition", "ChildSelectedPosition", ChildSelectedPosition);

                mListner.setSelectItem(storeItemLists.get(position).getItemName(), storeItemLists.get(position).getItemVisitPrice_b(), storeItemLists.get(position).getStoreItemID(),3);

                notifyDataSetChanged();
            }
        });

        tvItemRepair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isChecked[0] = true;
                mClickType = 4;
                RepairSelectedPosition = position;
                SharedPreferencesUtil.putSharedPreference(mContext, "LaundrySelect", "LaundrySelect", 4);
                SharedPreferencesUtil.putSharedPreference(mContext, "RepairSelectedPosition", "RepairSelectedPosition", RepairSelectedPosition);

                mListner.setSelectItem(storeItemLists.get(position).getItemName(), 0, storeItemLists.get(position).getStoreItemID(),4);
                notifyDataSetChanged();
            }
        });
    }

    /**
     * 세탁요금 (기본) 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setBasicSelectedPosition(int position) {
        if (SharedPreferencesUtil.getIntSharedPreference(mContext, "basicSelectedPosition", "basicSelectedPosition") == position) {
            tvItemBasic.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            tvItemBasic.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_2));
        }
    }

    /**
     * 세탁요금 (명품/고급) 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setLuxurySelectedPosition(int position) {
        if (SharedPreferencesUtil.getIntSharedPreference(mContext, "LuxurySelectedPosition", "LuxurySelectedPosition") == position) {
            tvItemLuxury.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            tvItemLuxury.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_2));
        }
    }

    /**
     * 세탁요금 (아동) 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setChildSelectedPosition(int position) {
        if (SharedPreferencesUtil.getIntSharedPreference(mContext, "ChildSelectedPosition", "ChildSelectedPosition") == position) {
            tvItemChild.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            tvItemChild.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_2));
        }
    }

    /**
     * 세탁요금 (수선) 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setSelectedPosition(int position) {
        if (SharedPreferencesUtil.getIntSharedPreference(mContext, "RepairSelectedPosition", "RepairSelectedPosition") == position) {
            tvItemRepair.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            tvItemRepair.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_2));
        }
    }

    @Override
    public int getItemCount() {
        return storeItemLists == null ? 0 : storeItemLists.size();
    }

    public void addItem(List<StoreItemList> storeItemList) {
        storeItemLists = storeItemList;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterComponent.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterComponent.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
