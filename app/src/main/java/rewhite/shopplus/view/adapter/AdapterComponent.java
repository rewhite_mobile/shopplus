package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.data.manager.ManagerComponent;
import rewhite.shopplus.data.manager.ManagerItemAdditionalData;
import rewhite.shopplus.data.dto.ItemAdditionalData;
import rewhite.shopplus.fragment.sub.FragmentComponent;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 부속품 RecyclerView List
 */
public class AdapterComponent extends RecyclerView.Adapter {
    private static final String TAG = "AdapterComponent";
    private Context mContext;

    private List<StoreItemList> mListItems = new ArrayList<>();
    private CCViewHolder.OnAdapterListner mLisner;
    private FragmentComponent.onCompletItem mListner;
    private int mPosition;
    private String mType;

    public AdapterComponent(Context context, FragmentComponent.onCompletItem listner, CCViewHolder.OnAdapterListner lisner, int position, String type) {
        this.mContext = context;
        this.mListner = listner;
        this.mLisner = lisner;
        this.mPosition = position;
        this.mType = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_component, parent, false);
        RecyclerView.ViewHolder holder = new CCViewHolder(v, mLisner);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final StoreItemList storeItemList = mListItems.get(position);

        CheckBox checkBoxBg = (CheckBox) holder.itemView.findViewById(R.id.checkbox_bg);
        final TextView tvItemRegistration = (TextView) holder.itemView.findViewById(R.id.tv_item_registration);
        final TextView tvWon = (TextView) holder.itemView.findViewById(R.id.tv_won);

        tvItemRegistration.setText(storeItemList.getItemName());
        tvWon.setText(DataFormatUtil.moneyFormatToWon(storeItemList.getItemVisitPrice()));

        if (mType.equals("N")) {
            viewSelecitionPrevious(storeItemList.getItemName(), checkBoxBg, tvItemRegistration, tvWon);
        } else {
            viewSelecition(storeItemList.getItemName(), checkBoxBg, tvItemRegistration, tvWon);
        }

        checkBoxBg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    mListner.setSelectItem(mListItems.get(position).getItemName(), String.valueOf(mListItems.get(position).getItemVisitPrice()), mListItems.get(position).getStoreItemID());

                } else {
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.color_0fc5dc));
                    mListner.setUnSelectItem(mListItems.get(position).getItemName(),  String.valueOf(mListItems.get(position).getItemVisitPrice()));
                }
            }
        });
    }

    /**
     * 이전 선택된 Item View Check
     *
     * @param itemName
     * @param tvItemRegistration
     * @param tvWon
     */
    private void viewSelecition(String itemName, CheckBox checkBoxBg, TextView tvItemRegistration, TextView tvWon) {

        ArrayList<ItemAdditionalData.ItemComponentEdit> itemComponentList = ManagerComponent.getInstance().getTComponent();

        if (itemComponentList.size() != 0) {
            for (ItemAdditionalData.ItemComponentEdit itemRepairFreeEdit : new ArrayList<>(itemComponentList)) {
                if (itemRepairFreeEdit.getTitleName().equals(itemName)) {
                    checkBoxBg.setChecked(true);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                }
            }
        }
    }

    /**
     * 이전 선택된 Item View Check
     *
     * @param itemName
     * @param tvItemRegistration
     * @param tvWon
     */
    private void viewSelecitionPrevious(String itemName, CheckBox checkBoxBg, TextView tvItemRegistration, TextView tvWon) {
        try {
            if (ManagerItemAdditionalData.getInstance().getItemAdditionalData().get(mPosition).getOptionTypeComponent() != null) {
                ArrayList<ItemAdditionalData.ItemComponentEdit> itemComponentList = ManagerItemAdditionalData.getInstance().getItemAdditionalData().get(mPosition).getOptionTypeComponent();
                ArrayList<ItemAdditionalData.ItemComponentEdit> itemComponentList1 = ManagerComponent.getInstance().getTComponent();

                if (itemComponentList.size() != 0) {
                    for (ItemAdditionalData.ItemComponentEdit itemComponentEdit : new ArrayList<>(itemComponentList)) {
                        ItemAdditionalData.ItemComponentEdit itemComponent = new ItemAdditionalData.ItemComponentEdit();
                        if (itemComponentEdit.getTitleName().equals(itemName)) {

                            itemComponent.setTitleName(itemComponentEdit.getTitleName());
                            itemComponent.setAmount(itemComponentEdit.getAmount());
                            itemComponent.setOptionItemID(itemComponentEdit.getOptionItemID());
                            itemComponentList1.add(itemComponent);

                            ManagerComponent.getInstance().setComponent(itemComponentList1);

                            checkBoxBg.setChecked(true);
                            tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                            tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                        }
                    }
                }
            }
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mListItems == null ? 0 : mListItems.size();
    }

    public void addItem(List<StoreItemList> listItems) {
        mListItems = listItems;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private OnAdapterListner listner;

        public CCViewHolder(View view, CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
