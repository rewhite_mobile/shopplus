package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityPayment;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 결제하기 Adapter
 */
public class AdapterPaymentDelivery extends BaseAdapter {
    private static final String TAG = "AdapterPaymentDelivery";

    private Context mContext;
    private List<GetVisitOrderList> mVisitOrderLists;
    private ActivityPayment.paymentAmount mLisner;
    private int plusPrice, checkCount;

    public AdapterPaymentDelivery(Context context, ActivityPayment.paymentAmount lisner) {
        this.mContext = context;
        this.mLisner = lisner;
    }

    @Override
    public int getCount() {
        return mVisitOrderLists.size();
    }

    @Override
    public Object getItem(int position) {
        return mVisitOrderLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_payment_delivery, parent, false);
        }

        CheckBox chSelect = (CheckBox) view.findViewById(R.id.ch_select);

        TextView tvTagNumber = (TextView) view.findViewById(R.id.tv_tag_number);
        ImageView imgTacColor = (ImageView) view.findViewById(R.id.img_tac_color);
        TextView tvItemName = (TextView) view.findViewById(R.id.tv_item_name);
        TextView tvReceptionDate = (TextView) view.findViewById(R.id.tv_reception_date);
        TextView tvShipDate = (TextView) view.findViewById(R.id.tv_ship_date);
        TextView tvPaymentAmount = (TextView) view.findViewById(R.id.tv_payment_amount);
        TextView tvReceivables = (TextView) view.findViewById(R.id.tv_receivables);

        int idx = mVisitOrderLists.get(position).getTagNo().indexOf("|");

        if (idx == 0) {
            tvTagNumber.setText(CommonUtil.setTagFormater(mVisitOrderLists.get(position).getTagNo()));
        } else if (mVisitOrderLists.get(position).getTagNo().length() >= 6 && mVisitOrderLists.get(position).getTagNo().length() < 9) {
            tvTagNumber.setText(CommonUtil.setFormater(mVisitOrderLists.get(position).getTagNo()));
        } else if (mVisitOrderLists.get(position).getTagNo().contains("-")) {
            tvTagNumber.setText(CommonUtil.setTagFormaterChange(mVisitOrderLists.get(position).getTagNo()));
        }
        CommonUtil.getTacColor(mContext, Integer.valueOf(mVisitOrderLists.get(position).getTagColor()), imgTacColor);

        tvItemName.setText(mVisitOrderLists.get(position).getStoreItemName());


//        try {
//            tvReceptionDate.setText(DataFormatUtil.setDateFormat(mVisitOrderLists.get(position).getRegDate()));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        try {
            if (mVisitOrderLists.get(position).getOutDate() == null || TextUtils.isEmpty(mVisitOrderLists.get(position).getOutDate())) {
                tvShipDate.setVisibility(View.INVISIBLE);
            } else {
                tvShipDate.setVisibility(View.VISIBLE);
                tvShipDate.setText(DataFormatUtil.setDateFormat(mVisitOrderLists.get(position).getOutDate()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        tvPaymentAmount.setText(DataFormatUtil.moneyFormatToWon(mVisitOrderLists.get(position).getPayPrice()));
        tvReceivables.setText(DataFormatUtil.moneyFormatToWon(mVisitOrderLists.get(position).getNonPayPrice()));

        chSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    plusPrice += mVisitOrderLists.get(position).getOrderItemPrice();
                    checkCount += 1;

                    mLisner.itemPaymner(plusPrice, checkCount);
                    mLisner.itemCheckSelect(mVisitOrderLists.get(position));
                } else {
                    plusPrice -= mVisitOrderLists.get(position).getOrderItemPrice();
                    checkCount -= 1;

                    mLisner.itemPaymner(plusPrice, checkCount);
                    mLisner.itemCheckUnSelect(mVisitOrderLists.get(position));
                }
            }
        });
        return view;
    }

    public void addItem(List<GetVisitOrderList> visitOrderLists) {
        mVisitOrderLists = visitOrderLists;
    }
}
