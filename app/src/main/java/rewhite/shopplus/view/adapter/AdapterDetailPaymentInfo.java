package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetPaymentInfoItem;
import rewhite.shopplus.common.popup.activity.ActivityDetailsPaymentInformation;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;

public class AdapterDetailPaymentInfo extends BaseAdapter {
    private static final String TAG = "AdapterDetailPaymentInfo";

    private List<GetPaymentInfoItem.OrderItem> mList = new ArrayList<>();
    private GetPaymentInfoItem.OrderItem orderItem;
    private ActivityDetailsPaymentInformation.getPaymentInfo mListner;

    private Context mContext;
    private TextView tvTagNumber;
    private TextView tvItemName;
    private TextView tvItemPayment;

    private ImageView ivColor;

    public AdapterDetailPaymentInfo(Context context, ActivityDetailsPaymentInformation.getPaymentInfo listner) {
        this.mContext = context;
        this.mListner = listner;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_adapter_detail_payment, parent, false);
        }
        setLayout(v, position);
        return v;
    }

    private void setLayout(View v, int position) {
        ivColor = (ImageView) v.findViewById(R.id.iv_color);
        tvTagNumber = (TextView) v.findViewById(R.id.tv_tag_number);
        tvItemName = (TextView) v.findViewById(R.id.tv_item_name);
        tvItemPayment = (TextView) v.findViewById(R.id.tv_item_payment);
        setTextView(position);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListner.paymentInfo(mList.get(position).getOrderItemID());
            }
        });
    }

    /**
     * TextView Setting
     */
    private void setTextView(int position) {
        int idx = mList.get(0).getTagNo().indexOf("|");
        if (idx == 0) {
            tvTagNumber.setText(CommonUtil.setTagFormater(mList.get(0).getTagNo()));
        } else if (mList.get(0).getTagNo().length() >= 6 && mList.get(0).getTagNo().length() < 9) {
            tvTagNumber.setText(CommonUtil.setFormater(mList.get(0).getTagNo()));
        } else if (mList.get(0).getTagNo().contains("-")) {
            tvTagNumber.setText(CommonUtil.setTagFormaterChange(mList.get(0).getTagNo()));
        }


        tvTagNumber.setText(mList.get(position).getTagNo());
        tvItemName.setText(mList.get(position).getStoreItemName());
        tvItemPayment.setText(DataFormatUtil.moneyFormatToWon((mList.get(position).getPayPrice())));
    }

    /**
     * 아이템 리스트 ADD
     *
     * @param item
     */
    public void addItem(List<GetPaymentInfoItem.OrderItem> item) {
        mList = item;
    }
}
