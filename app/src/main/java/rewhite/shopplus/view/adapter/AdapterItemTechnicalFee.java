package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.data.dto.ItemAdditionalData;
import rewhite.shopplus.util.DataFormatUtil;

public class AdapterItemTechnicalFee extends BaseAdapter {
    private static final String TAG = "AdapterAll";

    private Context mContext;

    private ArrayList<ItemAdditionalData.ItemTechnicalEdit> mAllVisitsReceiveds;

    public AdapterItemTechnicalFee(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mAllVisitsReceiveds.size();
    }

    @Override
    public Object getItem(int position) {
        return mAllVisitsReceiveds.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ItemAdditionalData.ItemTechnicalEdit itemTechnical = mAllVisitsReceiveds.get(position);
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_adapter_repair, parent, false);

            TextView tvTitleName = (TextView) v.findViewById(R.id.tv_title_name);
            TextView tvAmount = (TextView) v.findViewById(R.id.tv_amount);

            tvTitleName.setText(itemTechnical.getTitleName());
            tvAmount.setText(DataFormatUtil.moneyFormatToWon(Integer.valueOf(itemTechnical.getAmount())) + "원");
        }

        return v;
    }

    public void addItem(ArrayList<ItemAdditionalData.ItemTechnicalEdit> allVisitsReceiveds) {
        mAllVisitsReceiveds = allVisitsReceiveds;
    }
}
