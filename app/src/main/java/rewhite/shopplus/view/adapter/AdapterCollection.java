package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.common.naver.NMapViewer;

/**
 * Main 오늘의 수거배달 수거 View
 */
public class AdapterCollection extends BaseAdapter {
    private static final String TAG = "AdapterCollection";

    private ArrayList<String> mItems;
    private Context mContext;

    public AdapterCollection(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_collection, parent, false);
        }
        setLayout(convertView);
        return convertView;
    }

    private void setLayout(View view) {
        LinearLayout llMap = (LinearLayout) view.findViewById(R.id.ll_map);
        LinearLayout llDetails = (LinearLayout) view.findViewById(R.id.ll_details);

        llMap.setOnClickListener(btnOnClickListener);
        llDetails.setOnClickListener(btnOnClickListener);
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.ll_map:       //네이버 지도
                    Intent intent = new Intent(mContext, NMapViewer.class);
                    mContext.startActivity(intent);
                    break;

                case R.id.ll_details:   //자세히 보기

                    break;
            }
        }
    };

    public void addItem(ArrayList<String> item) {
        mItems = item;
    }
}