package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.fragment.sub.FragmentColor;
import rewhite.shopplus.util.CommonUtil;

public class AdapterFragmentColor extends RecyclerView.Adapter {
    private static final String TAG = "AdapterCategory";
    private Context mContext;

    private int mPosition;
    private int selectedPosition = -1;

    private List<String> mLaundryCategoryList;

    private AdapterComponent.CCViewHolder.OnAdapterListner mLisner;
    private FragmentColor.onCompletItem mListner;

    public AdapterFragmentColor(Context context, FragmentColor.onCompletItem listner, AdapterComponent.CCViewHolder.OnAdapterListner lisner) {
        this.mContext = context;
        this.mListner = listner;
        this.mLisner = lisner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_color, parent, false);
        RecyclerView.ViewHolder holder = new AdapterComponent.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final RelativeLayout imgBg = (RelativeLayout) holder.itemView.findViewById(R.id.img_bg);
        ImageView ivCheck = (ImageView) holder.itemView.findViewById(R.id.iv_check);

        CommonUtil.setColorView(position, imgBg);
        setSelectedPosition(position, ivCheck, imgBg);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                mListner.setSelectItem(position);
                notifyDataSetChanged();
            }
        });
    }


    /**
     * 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     * @param ivCheck  해당 amount
     * @param imgBg    해당 background
     */
    private void setSelectedPosition(int position, ImageView ivCheck, RelativeLayout imgBg) {
        if (selectedPosition == position) {
            ivCheck.setVisibility(View.VISIBLE);
        } else {
            ivCheck.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mLaundryCategoryList == null ? 0 : mLaundryCategoryList.size();
    }

    public void addItem(List<String> laundryCategoryList) {
        mLaundryCategoryList = laundryCategoryList;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterComponent.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterComponent.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
