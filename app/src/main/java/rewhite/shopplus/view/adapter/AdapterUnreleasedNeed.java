package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetVisitOrderListNeed;
import rewhite.shopplus.util.DataFormatUtil;

public class AdapterUnreleasedNeed extends BaseAdapter {
    private static final String TAG = "AdapterUnreleasedNeed";

    private List<GetVisitOrderListNeed> mList = new ArrayList<>();
    private GetVisitOrderListNeed getVisitOrderList;

    private Context mContext;
    private TextView tvMemberName, tvMemberPhoneNumber, tvMemberAddress;

    private TextView tvItemName, tvShipDate, tvPrice, tvAccruedAmount;

    public AdapterUnreleasedNeed(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        getVisitOrderList = mList.get(position);
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_unreleased, parent, false);
        }

        tvMemberName = (TextView) v.findViewById(R.id.tv_member_name);
        tvMemberPhoneNumber = (TextView) v.findViewById(R.id.tv_member_phone_number);
        tvMemberAddress = (TextView) v.findViewById(R.id.tv_member_address);

        tvItemName = (TextView) v.findViewById(R.id.tv_item_name);
        tvShipDate = (TextView) v.findViewById(R.id.tv_ship_date);
        tvPrice = (TextView) v.findViewById(R.id.tv_price);
        tvAccruedAmount = (TextView) v.findViewById(R.id.tv_accrued_amount);

        setTextView(getVisitOrderList, position);
        return v;
    }

    private void setTextView(GetVisitOrderListNeed getVisitOrderList, int position) {
        tvMemberName.setText(getVisitOrderList.getUserName());
        tvMemberPhoneNumber.setText(getVisitOrderList.get_UserPhone());
        tvMemberAddress.setText(getVisitOrderList.get_UserAddress());

        tvItemName.setText(getVisitOrderList.getStoreItemName());
        try {
            tvShipDate.setText(DataFormatUtil.setDateItemCancelFormat(getVisitOrderList.getEnterDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        tvPrice.setText(DataFormatUtil.moneyFormatToWon(getVisitOrderList.getStoreItemPrice()));
        tvAccruedAmount.setText(DataFormatUtil.moneyFormatToWon(getVisitOrderList.getNonPayPrice()));
    }

    public void addItem(List<GetVisitOrderListNeed> item) {
        mList = item;
    }
}
