package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.data.manager.ManagerTechnicalFeeEdit;
import rewhite.shopplus.data.dto.ItemTechnical;
import rewhite.shopplus.fragment.sub.FragmentTechnicalFee;

/**
 * 품목등록 - >기술 수정 Adapter
 */
public class AdapterTechnicalFeeEdit extends RecyclerView.Adapter {
    private static final String TAG = "AdapterTechnicalFee";
    private Context mContext;
    private int selectedPosition = -1;

    private List<StoreItemList> mLaundryCategoryList;

    private AdapterComponent.CCViewHolder.OnAdapterListner mLisner;
    private FragmentTechnicalFee.onCompletItem mListner;
    private String mSelected;

    public AdapterTechnicalFeeEdit(Context context, FragmentTechnicalFee.onCompletItem listner, AdapterComponent.CCViewHolder.OnAdapterListner lisner, String type) {
        this.mContext = context;
        this.mListner = listner;
        this.mLisner = lisner;
        this.mSelected = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_technical_fee, parent, false);
        RecyclerView.ViewHolder holder = new AdapterComponent.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final StoreItemList storeItemList = mLaundryCategoryList.get(position);
        ArrayList<ItemTechnical> itemTechnicals = new ArrayList<>();

        CheckBox checkBoxBg = (CheckBox) holder.itemView.findViewById(R.id.checkbox_bg);
        final TextView tvItemRegistration = (TextView) holder.itemView.findViewById(R.id.tv_item_registration);
        final TextView tvWon = (TextView) holder.itemView.findViewById(R.id.tv_won);

        tvItemRegistration.setText(storeItemList.getItemName());
        tvWon.setText(String.valueOf(storeItemList.getItemVisitPrice()));

        List<ItemTechnical> itemEditItemRepairs = ManagerTechnicalFeeEdit.getInstance().getTechnicalFee();

        if (mSelected.equals("N")) {
            for (ItemTechnical itemEditItemRepair : itemEditItemRepairs) {
                if (itemEditItemRepair.getTitleName().equals(storeItemList.getItemName())) {
                    checkBoxBg.setEnabled(false);
                    checkBoxBg.setBackgroundResource(R.color.color_ebebeb);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.color_0fc5dc));
                }
            }
        } else {
            for (ItemTechnical itemEditItemRepair : itemEditItemRepairs) {
                if (itemEditItemRepair.getTitleName().equals(storeItemList.getItemName())) {
                    ItemTechnical itemEditItem = new ItemTechnical();

                    checkBoxBg.setBackgroundResource(R.drawable.a_button_type_2);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));

                    itemEditItem.setTitleName(itemEditItemRepair.getTitleName());
                    itemEditItem.setAmount(itemEditItemRepair.getAmount());
                    itemEditItem.setOptionItemID(itemEditItemRepair.getOptionItemID());

                    itemTechnicals.add(itemEditItem);
                    ManagerTechnicalFeeEdit.getInstance().setTechnicalFee(itemTechnicals);
                }
            }
        }

        checkBoxBg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    mListner.setSelectItem(mLaundryCategoryList.get(position).getItemName(), tvWon.getText().toString(), mLaundryCategoryList.get(position).getStoreItemID());
                } else {
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.color_0fc5dc));
                    mListner.setUnSelectItem(mLaundryCategoryList.get(position).getItemName(), tvWon.getText().toString());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mLaundryCategoryList == null ? 0 : mLaundryCategoryList.size();
    }

    public void addItem(List<StoreItemList> laundryCategoryList) {
        mLaundryCategoryList = laundryCategoryList;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterComponent.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterComponent.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}