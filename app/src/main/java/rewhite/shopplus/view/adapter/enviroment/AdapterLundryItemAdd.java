package rewhite.shopplus.view.adapter.enviroment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.LaundryCategoryItemList;
import rewhite.shopplus.util.Logger;

public class AdapterLundryItemAdd extends RecyclerView.Adapter {
    private static final String TAG = "AdapterLundryItemAdd";
    private Context mContext;

    private int selectedPosition = 0;
    private List<LaundryCategoryItemList> mItemList;
    private AdapterLundryItemAdd.CCViewHolder.OnAdapterListner mLisner;
    private int mType;

    public AdapterLundryItemAdd(Context context, AdapterLundryItemAdd.CCViewHolder.OnAdapterListner lisner, int type) {
        this.mContext = context;
        this.mLisner = lisner;
        this.mType = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_laundry_add, parent, false);
        RecyclerView.ViewHolder holder = new AdapterLundryItemAdd.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        LaundryCategoryItemList laundryCategoryItemList = mItemList.get(position);
        TextView tvItemRegistration = (TextView) holder.itemView.findViewById(R.id.tv_item_registration);
        tvItemRegistration.setText(laundryCategoryItemList.getCategoryName());

        if (position == 0) {
            tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            tvItemRegistration.setBackgroundResource(R.drawable.a_button_type_2);
        }

        if (mType == 0) {
            setSelectedPosition(position, tvItemRegistration);
            tvItemRegistration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLisner.onClick(laundryCategoryItemList.getCategoryId());
                    selectedPosition = position;
                    notifyDataSetChanged();
                }
            });
        } else {
            tvItemRegistration.setBackgroundResource(R.color.color_ebebeb);
            tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
        }
    }

    /**
     * 선택된 아이템 리스트 UI 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setSelectedPosition(int position, TextView view) {
        if (selectedPosition == position) {
            view.setBackgroundResource(R.drawable.a_button_type_2);
            view.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else {
            view.setBackgroundResource(R.drawable.a_button_type_3);
            view.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
        }
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    public void addItem(List<LaundryCategoryItemList> itemList) {
        mItemList = itemList;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterLundryItemAdd.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterLundryItemAdd.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
