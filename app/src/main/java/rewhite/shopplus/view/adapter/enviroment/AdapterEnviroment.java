package rewhite.shopplus.view.adapter.enviroment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import rewhite.shopplus.fragment.enviroment.FragmentAppSetting;
import rewhite.shopplus.fragment.enviroment.FragmentEquipmentSetup;
import rewhite.shopplus.fragment.enviroment.FragmentSalesSetup;
import rewhite.shopplus.fragment.enviroment.FragmentSetRatetable;

public class AdapterEnviroment extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterEnviroment";
    private AppCompatActivity mActivity;

    public AdapterEnviroment(FragmentManager fm, AppCompatActivity activity) {
        super(fm);
        this.mActivity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentSalesSetup(mActivity);    //영업설정
            case 1:
                return new FragmentSetRatetable(mActivity);  //요금표설정
            case 2:
                return new FragmentAppSetting(mActivity);    //앱 설정
            case 3:
                return new FragmentEquipmentSetup(mActivity);//장비설정
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
