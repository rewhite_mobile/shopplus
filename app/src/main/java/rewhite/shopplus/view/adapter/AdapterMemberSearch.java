package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.StoreUserInfoList;
import rewhite.shopplus.fragment.main.FragmentHome.onContentItemClickListener;

public class AdapterMemberSearch extends BaseAdapter {
    private static final String TAG = "SiteListAdapter";

    private Context mContext;
    private onContentItemClickListener mListener;
    private CustomViewHolder holder;

    private List<StoreUserInfoList> mListSite = new ArrayList<>();
    private StoreUserInfoList storeUserInfoItemList;

    public AdapterMemberSearch(Context context, onContentItemClickListener listener) {
        mContext = context;
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mListSite.size();
    }

    @Override
    public Object getItem(int position) {
        return mListSite.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        holder = new CustomViewHolder();
        storeUserInfoItemList = mListSite.get(position);

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.item_adapter_member_search, parent, false);
            convertView.setTag(holder);
        } else {
            holder = (CustomViewHolder) convertView.getTag();
        }

        setLayout(convertView);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(mListSite.get(position).getStoreUserId());
            }
        });
        return convertView;

    }

    private void setLayout(View v) {
        holder.tvUserName = (TextView) v.findViewById(R.id.tv_user_name);
        holder.tvUserPhoneNumber = (TextView) v.findViewById(R.id.tv_user_phoneNumber);
        holder.tvUserAddress = (TextView) v.findViewById(R.id.tv_user_address);
        holder.imgMemberType = (ImageView) v.findViewById(R.id.img_member_type);


        if (storeUserInfoItemList.getUserType().equals("S")) {
            holder.imgMemberType.setImageResource(R.drawable.ico_itemregistration_meberrating_01);
        } else if (storeUserInfoItemList.getUserType().equals("SR")) {
            holder.imgMemberType.setImageResource(R.drawable.ico_itemregistration_meberrating_00);

        }
        holder.tvUserName.setText(storeUserInfoItemList.getUserName());
        holder.tvUserPhoneNumber.setText(storeUserInfoItemList.getUserPhone());
        holder.tvUserAddress.setText(storeUserInfoItemList.getUserAddress());

    }

    public class CustomViewHolder {
        public TextView tvUserName;
        public TextView tvUserPhoneNumber;
        public TextView tvUserAddress;
        public ImageView imgMemberType;

    }

    public void addItem(List<StoreUserInfoList> listSite) {
        mListSite = listSite;
    }
}