package rewhite.shopplus.view.adapter.enviroment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetStoreItemList;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.fragment.enviroment.FragmentAccessories;

/**
 * 부속품 ListView
 */
public class AdapterDetailListData extends BaseAdapter {
    private static final String TAG = "AdapterDetailListData";
    private Context mContext;

    private int selectedPosition = -1;
    private List<GetStoreItemList> mItemList;
    private FragmentAccessories.existingCharge mListener;

    private RadioGroup rgRepairTreatment;
    private RadioButton rbRepairTreatment, rbRepairUntreatment;

    public AdapterDetailListData(Context context, FragmentAccessories.existingCharge listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    public void addItem(List<GetStoreItemList> itemList) {
        mItemList = itemList;
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repair_fee, parent, false);
        CustomViewHolder holder = new CustomViewHolder();

        GetStoreItemList getStoreItemList = mItemList.get(position);
        holder.tvItemName = (TextView) convertView.findViewById(R.id.tv_repair_item_name);
        holder.tvLuxuryRate = (TextView) convertView.findViewById(R.id.tv_luxury_rate);

        holder.rgRepairTreatment = (RadioGroup) convertView.findViewById(R.id.rg_repair_treatment);
        holder.rbRepairTreatment = (RadioButton) convertView.findViewById(R.id.rb_repair_treatment);
        holder.rbRepairUntreatment = (RadioButton) convertView.findViewById(R.id.rb_repair_untreatment);
        holder.rgRepairTreatment.setTag(position);

        if (getStoreItemList.getIsUse().equals("Y")) {
            setSelectedPosition(getStoreItemList, position, holder.tvLuxuryRate);

            holder.rbRepairTreatment.setBackgroundResource(R.drawable.a_button_type_2);
            holder.rbRepairUntreatment.setBackgroundResource(R.drawable.a_button_type_3);

            holder.rbRepairTreatment.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            holder.rbRepairUntreatment.setTextColor(mContext.getResources().getColor(R.color.color_43425c));

            holder.tvLuxuryRate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = position;
                    notifyDataSetChanged();
                }
            });

            if (getStoreItemList.getItemType() == 2) {
                holder.tvItemName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.setItemName(getStoreItemList.getStoreItemID());
                    }
                });
            }
        } else {
            holder.rbRepairUntreatment.setBackgroundResource(R.drawable.a_button_type_2);
            holder.rbRepairTreatment.setBackgroundResource(R.drawable.a_button_type_3);

            holder.rbRepairUntreatment.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            holder.rbRepairTreatment.setTextColor(mContext.getResources().getColor(R.color.color_43425c));

            holder.tvItemName.setBackgroundResource(R.color.color_ebebeb);
            holder.tvLuxuryRate.setBackgroundResource(R.color.color_ebebeb);
        }

        holder.tvItemName.setText(getStoreItemList.getItemName());
        holder.tvLuxuryRate.setText(String.valueOf(getStoreItemList.getItemVisitPrice()));

        holder.rgRepairTreatment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                boolean isChecked = (checkedId == R.id.rb_repair_treatment);
                if (isChecked) {
                    EnviromentManager.getUpdateStoreItemState(mContext, mItemList.get(position).getStoreItemID(), "Y");
                } else {
                    EnviromentManager.getUpdateStoreItemState(mContext, mItemList.get(position).getStoreItemID(), "N");
                }
                mListener.setNotToUse();
            }
        });


        return convertView;
    }

    /**
     * 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setSelectedPosition(GetStoreItemList getStoreItemList, int position, TextView tvItemAmount) {
        if (selectedPosition == position) {
            mListener.setExistionCharge(mItemList.get(position).getItemVisitPrice(), getStoreItemList.getItemName(), getStoreItemList.getStoreItemID());
            tvItemAmount.setBackgroundResource(R.drawable.a_button_type_10);
        } else {
            tvItemAmount.setBackgroundResource(R.drawable.a_button_type_3);
        }
    }

    public class CustomViewHolder {
        public TextView tvItemName;
        public TextView tvLuxuryRate;

        public RadioGroup rgRepairTreatment;
        public RadioButton rbRepairTreatment;
        public RadioButton rbRepairUntreatment;
    }
}