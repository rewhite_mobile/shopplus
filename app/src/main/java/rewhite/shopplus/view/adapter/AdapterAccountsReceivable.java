package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityMemberDetails;
import rewhite.shopplus.client.model.NonPayUserManageList;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 운영관리 -> 미수회원 조회 View Adapter
 */
public class AdapterAccountsReceivable extends BaseAdapter {
    private static final String TAG = "NonPayUserManageList";
    private static final String USER_STORE_ID = "user_store_id";                //사용자 key

    private Context mContext;
    private List<NonPayUserManageList> mUserManagerList = new ArrayList<>();

    public AdapterAccountsReceivable(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mUserManagerList.size();
    }

    @Override
    public Object getItem(int position) {
        return mUserManagerList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_account_receivable, parent, false);
        }

        NonPayUserManageList userManageList = mUserManagerList.get(position);

        TextView tvMemberName = (TextView) convertView.findViewById(R.id.tv_member_name);
        TextView tvMemberPhoneNumber = (TextView) convertView.findViewById(R.id.tv_member_phone_number);
        TextView tvMemberAddress = (TextView) convertView.findViewById(R.id.tv_member_address);
        TextView tvLastOrderDate = (TextView) convertView.findViewById(R.id.tv_last_order_date);
        TextView tvCumulativeOrders = (TextView) convertView.findViewById(R.id.tv_cumulative_orders);
        TextView tvCumulativeOrderAmount = (TextView) convertView.findViewById(R.id.tv_cumulative_order_amount);
        TextView tvMileageAmount = (TextView)convertView.findViewById(R.id.tv_mileage_amount);
        TextView tvPreFilledGold = (TextView) convertView.findViewById(R.id.tv_pre_filled_gold);
        TextView tvAccruedAmount = (TextView) convertView.findViewById(R.id.tv_accrued_amount);

        LinearLayout tvMemberSearch = (LinearLayout) convertView.findViewById(R.id.tv_member_search);


        tvMemberName.setText(userManageList.getUserName());
        tvMemberPhoneNumber.setText(userManageList.get_UserPhone());
        tvMemberAddress.setText(userManageList.get_UserAddress());
        try {
            if(userManageList.getRecentOrderDate() != null){
                tvLastOrderDate.setText(DataFormatUtil.setDateWeekFormat2(userManageList.getRecentOrderDate()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException err){

        }
        tvCumulativeOrders.setText(String.valueOf(userManageList.getTotalOrderCount()));
        tvCumulativeOrderAmount.setText(DataFormatUtil.moneyFormatToWon(userManageList.getTotalOrderPrice()));
        tvMileageAmount.setText(DataFormatUtil.moneyFormatToWon(userManageList.getMileage()));
        tvPreFilledGold.setText(DataFormatUtil.moneyFormatToWon(userManageList.getTotalOrderPrice()));
        tvAccruedAmount.setText(DataFormatUtil.moneyFormatToWon(userManageList.getNonPayPrice()));

        tvMemberSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingLogic.setStoreUserId(mContext, String.valueOf(userManageList.getStoreUserID()));

                Intent intent = new Intent(mContext, ActivityMemberDetails.class);
                intent.putExtra(USER_STORE_ID, userManageList.getStoreUserID());
                mContext.startActivity(intent);
            }
        });
        return convertView;
    }

    public void addItem(List<NonPayUserManageList> userManageList){
        mUserManagerList = userManageList;
    }
}
