package rewhite.shopplus.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.FaqListModel;
import rewhite.shopplus.fragment.main.FragmentAskedQuestions;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;

/**
 * 자주하는 질문 Adapter
 */
public class AdapterAskedQuestions extends BaseAdapter {
    private static final String TAG = "AdapterAskedQuestions";

    private List<FaqListModel> mItem;
    private int selectedPosition = 0;
    private FragmentAskedQuestions.NoticeData mListener;

    public AdapterAskedQuestions(FragmentAskedQuestions.NoticeData listener) {
        this.mListener = listener;
    }


    @Override
    public int getCount() {
        return mItem.size();
    }

    @Override
    public Object getItem(int position) {
        return mItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service_center_list, parent, false);

        FaqListModel FaqListModel = mItem.get(position);

        LinearLayout llRoot = (LinearLayout) view.findViewById(R.id.ll_root);
        ImageView ivIconNext = (ImageView) view.findViewById(R.id.iv_icon_next);
        TextView tvCategoryName = (TextView) view.findViewById(R.id.tv_category_name);
        TextView tvContentsName = (TextView) view.findViewById(R.id.tv_contents_name);
        TextView tvHistoryDate = (TextView) view.findViewById(R.id.tv_history_date);

        Logger.d(TAG, "FaqListModel.getFaqTitle() : " + FaqListModel.getFaqTitle());
        tvCategoryName.setText(FaqListModel.getFaqTitle());
        tvContentsName.setText(FaqListModel.getFaqContent());
        try {
            tvHistoryDate.setText(DataFormatUtil.setDateMothFormat(FaqListModel.getRegDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (position == 0) {
            llRoot.setBackgroundResource(R.drawable.a_box_round_custom_type_3);
        }
        setSelectedPosition(position, llRoot, ivIconNext);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
            }
        });
        return view;
    }

    /**
     * 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setSelectedPosition(int position, LinearLayout llRoot, ImageView ivIconNext) {
        if (selectedPosition == position) {
            mListener.webViewUrl(mItem.get(position).getWebViewUrl());

            llRoot.setBackgroundResource(R.drawable.a_box_round_custom_type_3);
            ivIconNext.setBackgroundResource(R.drawable.ico_customerservice_list_01);
        } else {
            ivIconNext.setBackgroundResource(R.drawable.ico_customerservice_listchoice_00);
            llRoot.setBackgroundResource(R.drawable.a_box_round_custom_type_2);
        }
    }

    public void addItem(List<FaqListModel> item) {
        mItem = item;
    }
}