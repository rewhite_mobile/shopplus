package rewhite.shopplus.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import rewhite.shopplus.common.popup.activity.ActivityMemberHistory;
import rewhite.shopplus.fragment.sub.FragmentMileage;
import rewhite.shopplus.fragment.sub.FragmentPreFilledGold;

/**
 * 회원상세 내역 조회 ViewPager
 */
public class AdapterMemberHistory extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterMemberHistory";
    private AppCompatActivity mActivity;
    private int mUserStoreId;

    public AdapterMemberHistory(FragmentManager fragmentManager, ActivityMemberHistory activity, int userStoreId) {
        super(fragmentManager);
        this.mUserStoreId = userStoreId;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentMileage(mActivity, mUserStoreId);        // 마일리지
            case 1:
                return new FragmentPreFilledGold(mActivity, mUserStoreId);  // 선충전금
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
