package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;

public class AdapterCommonTag extends RecyclerView.Adapter {
    private static final String TAG = "AdapterCommonTag";
    private Context mContext;

    private List<String> mList;

    private AdapterCommonTag.CCViewHolder.OnAdapterListner mLisner;

    public AdapterCommonTag(Context context, AdapterCommonTag.CCViewHolder.OnAdapterListner lisner) {
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_common_tag, parent, false);
        RecyclerView.ViewHolder holder = new AdapterCommonTag.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        String tagName = mList.get(position);
        TextView tvTagName = (TextView) holder.itemView.findViewById(R.id.tv_tag_name);
        tvTagName.setText(tagName);
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public void addItem(List<String> listItem) {
        mList = listItem;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterCommonTag.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterCommonTag.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}