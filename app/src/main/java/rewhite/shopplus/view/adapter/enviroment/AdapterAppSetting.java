package rewhite.shopplus.view.adapter.enviroment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import rewhite.shopplus.fragment.enviroment.FragmentAccountInformation;
import rewhite.shopplus.fragment.enviroment.FregmentquentlyUsedMenu;

public class AdapterAppSetting extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterAppSetting";

    private AppCompatActivity mActivity;
    public AdapterAppSetting(FragmentManager fm, AppCompatActivity activity) {
        super(fm);
        this.mActivity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FregmentquentlyUsedMenu(mActivity);     // 자주쓰는메뉴
            case 1:
                return new FragmentAccountInformation(mActivity);  // 계정정보
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
