package rewhite.shopplus.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;

import rewhite.shopplus.fragment.sub.FragmentLaundryPickupDelivery;
import rewhite.shopplus.fragment.sub.FragmentRewhitePickupDelivery;
import rewhite.shopplus.fragment.sub.FragmentVisitReception;

public class AdapterMemberDetailsViewPager extends FragmentStatePagerAdapter {

    private static final String TAG = "AdapterMemberDetailsViewPager";
    private AppCompatActivity mActivity;

    public AdapterMemberDetailsViewPager(FragmentManager fm, AppCompatActivity activity) {
        super(fm);
        mActivity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentVisitReception(); //방문접수
            case 1:
                return new FragmentLaundryPickupDelivery(); //세탁소 수거배송
            case 2:
                return new FragmentRewhitePickupDelivery(); //리화이트 수거배송
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
