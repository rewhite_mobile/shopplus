package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.data.manager.ManagerComponentEdit;
import rewhite.shopplus.data.dto.ItemComponent;
import rewhite.shopplus.fragment.sub.FragmentComponent;

public class AdapterComponentEdit extends RecyclerView.Adapter {
    private static final String TAG = "AdapterComponent";
    private Context mContext;

    private List<StoreItemList> mListItems = new ArrayList<>();
    private AdapterComponent.CCViewHolder.OnAdapterListner mLisner;
    private FragmentComponent.onCompletItem mListner;
    private int mPosition;

    private String mSelected;

    public AdapterComponentEdit(Context context, FragmentComponent.onCompletItem listner, AdapterComponent.CCViewHolder.OnAdapterListner lisner, String type) {
        this.mContext = context;
        this.mListner = listner;
        this.mLisner = lisner;
        this.mSelected = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_component, parent, false);
        RecyclerView.ViewHolder holder = new AdapterComponent.CCViewHolder(v, mLisner);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final StoreItemList storeItemList = mListItems.get(position);
        ArrayList<ItemComponent> itemComponents = new ArrayList<>();
        CheckBox checkBoxBg = (CheckBox) holder.itemView.findViewById(R.id.checkbox_bg);
        final TextView tvItemRegistration = (TextView) holder.itemView.findViewById(R.id.tv_item_registration);
        final TextView tvWon = (TextView) holder.itemView.findViewById(R.id.tv_won);

        tvItemRegistration.setText(storeItemList.getItemName());
        tvWon.setText(String.valueOf(storeItemList.getItemVisitPrice()));

        ArrayList<ItemComponent> itemComponentList = ManagerComponentEdit.getInstance().getTComponent();

        for (ItemComponent itemComponent : itemComponentList) {
            if (itemComponent.getTitleName().equals(storeItemList.getItemName())) {

                checkBoxBg.setBackgroundResource(R.drawable.a_button_type_2);
                tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                itemComponents.add(itemComponent);

                ManagerComponentEdit.getInstance().setComponent(itemComponents);
            }
        }

        checkBoxBg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkBoxBg.setBackgroundResource(R.drawable.a_button_type_2);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    mListner.setSelectItem(mListItems.get(position).getItemName(), tvWon.getText().toString(), mListItems.get(position).getStoreItemID());
                } else {
                    checkBoxBg.setBackgroundResource(R.drawable.a_button_type_3);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.color_0fc5dc));
                    mListner.setUnSelectItem(mListItems.get(position).getItemName(), tvWon.getText().toString());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListItems == null ? 0 : mListItems.size();
    }

    public void addItem(List<StoreItemList> listItems) {
        mListItems = listItems;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterComponent.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterComponent.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
