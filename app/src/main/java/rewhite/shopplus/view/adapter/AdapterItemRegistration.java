package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityItemRegistration;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.data.dto.ItemAdditionalData;
import rewhite.shopplus.data.manager.ManagerItemAdditionalData;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.SharedPreferencesUtil;

public class AdapterItemRegistration extends BaseAdapter {
    private static final String TAG = "AdapterItemRegistration";

    private ArrayList<ItemAdditionalData> itemDataList;
    private AppCompatActivity mActivity;

    private ActivityItemRegistration.onClickItem mLisner;

    private TextView tvTacNumber;
    private TextView tvItem;
    private TextView tvTagTypeA;
    private TextView tvTagTypeE;
    private TextView tvPaymentAmount;
    int itemListTotalMoney = 0;
    int itemTotalMoney = 0;

    private ImageView imgTacColor;

    private LinearLayout llItemCopy;
    private LinearLayout llDelete;

    public AdapterItemRegistration(AppCompatActivity activity, ActivityItemRegistration.onClickItem lisner) {
        this.mActivity = activity;
        this.mLisner = lisner;
    }

    @Override
    public int getCount() {
        return itemDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ItemAdditionalData additionalData = itemDataList.get(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_registration, parent, false);
        }

        setLayout(convertView, position);
        setTextView(additionalData, position);
        setTagTextView(additionalData, position);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLisner.onClickItem(position);
            }
        });
        return convertView;
    }

    private void setLayout(View convertView, int position) {
        tvTacNumber = (TextView) convertView.findViewById(R.id.tv_tac_number);
        tvItem = (TextView) convertView.findViewById(R.id.tv_item);
        tvTagTypeA = (TextView) convertView.findViewById(R.id.tv_tag_type_a);
        tvTagTypeE = (TextView) convertView.findViewById(R.id.tv_tag_type_e);
        tvPaymentAmount = (TextView) convertView.findViewById(R.id.tv_payment_amount);
        imgTacColor = (ImageView) convertView.findViewById(R.id.img_tac_color);
        llDelete = (LinearLayout) convertView.findViewById(R.id.ll_delete);
        llItemCopy = (LinearLayout) convertView.findViewById(R.id.ll_item_copy);

        itemListTotalMoney = SharedPreferencesUtil.getIntSharedPreference(mActivity, PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY);

        llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(mActivity);
                thirdConfirmPopup.setTitle("취소팝업");
                thirdConfirmPopup.setContent("해당 품옥을 삭제할까요?");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        try {
                            int itemAddPay = 0;
                            ArrayList<ItemAdditionalData> itemAdditionalData = ManagerItemAdditionalData.getInstance().getItemAdditionalData();
                            itemAdditionalData.remove(position);

                            for (ItemAdditionalData itemAdditionalDatas : itemAdditionalData) {
                                itemAddPay += Integer.valueOf(itemAdditionalDatas.getTotalMoney());
                            }

                            SharedPreferencesUtil.putSharedPreference(mActivity, PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY, itemAddPay);

                            ManagerItemAdditionalData.getInstance().setItemAdditionalData(itemAdditionalData);
                            mLisner.setDelete(itemAddPay, itemAdditionalData.size());
                        } catch (Exception err) {
                            err.printStackTrace();
                        }
                        notifyDataSetChanged();
                    }
                });
                thirdConfirmPopup.show();
            }
        });

//        llItemCopy.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int itemListTotalMoney = 0;
//                ArrayList<ItemAdditionalData> itemAdditionalDatas = ManagerItemAdditionalData.getInstance().getItemAdditionalData();
//                itemListTotalMoney = SharedPreferencesUtil.getIntSharedPreference(mActivity, PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY);
//
//                ItemAdditionalData itemAdditionalData = new ItemAdditionalData();
//
//                itemAdditionalData.setStoreItemID(itemDataList.get(position).getStoreItemID()); //카테고리
//                itemAdditionalData.setTacNumber(itemDataList.get(position).getTacNumber());     //택번호
//                itemAdditionalData.setTacColor(itemDataList.get(position).getTacColor());       //택컬러
//                itemAdditionalData.setOptions(itemDataList.get(position).getOptions());         //세탁옵션
//                itemAdditionalData.setTotalMoney(itemDataList.get(position).getTotalMoney());   //세탁요금
//                itemAdditionalData.setRgbColor(itemDataList.get(position).getRgbColor());   //세탁요금
//
//                itemAdditionalDatas.add(itemAdditionalData);
//
//                itemListTotalMoney += Integer.valueOf(itemDataList.get(position).getTotalMoney());
//                SharedPreferencesUtil.putSharedPreference(mActivity, PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY, itemListTotalMoney);
//
//                ManagerItemAdditionalData.getInstance().setItemAdditionalData(itemAdditionalDatas);
//            }
//        });
    }

    /**
     * 텍스트 Setting View
     *
     * @param additionalData
     * @param position
     */
    private void setTextView(ItemAdditionalData additionalData, int position) {
        tvPaymentAmount.setText(DataFormatUtil.moneyFormatToWon(Integer.valueOf(additionalData.getTotalMoney())));

        if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
            tvTacNumber.setText(CommonUtil.setTagFormater(additionalData.getTacNumber()));
        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
            tvTacNumber.setText(CommonUtil.setFormater(additionalData.getTacNumber()));
        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
            tvTacNumber.setText(CommonUtil.setEtcTagFormaterChangeResponse(additionalData.getTacNumber()));
        }

        tvItem.setText(additionalData.getItemName());

        CommonUtil.getTacColor(mActivity, additionalData.getTacColor(), imgTacColor);

        llItemCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int itemAddPay = 0;
                int itemListTotalMoney = SharedPreferencesUtil.getIntSharedPreference(mActivity, PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY);
                try {
                    ArrayList<ItemAdditionalData> newItemAdditionalDatas = new ArrayList<>();
                    ArrayList<ItemAdditionalData> itemAdditionalDatas = ManagerItemAdditionalData.getInstance().getItemAdditionalData();

                    ArrayList<ItemAdditionalData.ItemRepairFreeEdit> optionTypeRepairFee = new ArrayList<>();
                    ArrayList<ItemAdditionalData.OrderItemOptionInput> options = new ArrayList<>();
                    ArrayList<ItemAdditionalData.ItemTechnicalEdit> optionTechnicalFree = new ArrayList<>();
                    ArrayList<ItemAdditionalData.ItemComponentEdit> optionTypeComponent = new ArrayList<>();

                    ItemAdditionalData itemAdditionalData = new ItemAdditionalData();

                    if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
                        Logger.d(TAG, "일제");
                        itemAdditionalData.setTacNumber(CommonUtil.setTagFormaterChangeResponse(CommonUtil.setTagFormaterChange(itemAdditionalDatas.get(position).getTacNumber())));        // 일제 택번호
                    } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
                        Logger.d(TAG, "바코드");
                        itemAdditionalData.setTacNumber(CommonUtil.setTagFormaterChangeTypeResponse(CommonUtil.setTagFormaterChangeType(itemAdditionalDatas.get(position).getTacNumber())));    // 일제바코드 택번호
                    } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
                        Logger.d(TAG, "기타");
                        itemAdditionalData.setTacNumber(CommonUtil.setEtcTagFormaterChangeResponse(itemAdditionalDatas.get(position).getTacNumber()));// 기타 택번호
                    }

                    itemAdditionalData.setStoreItemID(itemAdditionalDatas.get(position).getStoreItemID());
                    itemAdditionalData.setStoreItemName(itemAdditionalDatas.get(position).getStoreItemName());
                    itemAdditionalData.setHowToWash(itemAdditionalDatas.get(position).getHowToWash());
                    itemAdditionalData.setTacColor(itemAdditionalDatas.get(position).getTacColor());
                    itemAdditionalData.setItemName(itemAdditionalDatas.get(position).getItemName());
                    itemAdditionalData.setItemGrade(itemAdditionalDatas.get(position).getItemGrade());
                    itemAdditionalData.setOptionTypeRepairFee(optionTypeRepairFee);
                    itemAdditionalData.setOptions(options);
                    itemAdditionalData.setOptionTechnicalFree(optionTechnicalFree);
                    itemAdditionalData.setOptionTypeComponent(optionTypeComponent);

                    itemAdditionalData.setTotalMoney(String.valueOf(itemAdditionalDatas.get(position).getLaundryMoney()));

                    itemAdditionalDatas.add(itemAdditionalData);
                    ManagerItemAdditionalData.getInstance().setItemAdditionalData(itemAdditionalDatas);

                    for (ItemAdditionalData itemAdditional : itemAdditionalDatas) {
                        itemAddPay += Integer.valueOf(itemAdditional.getTotalMoney());
                    }

                    SharedPreferencesUtil.putSharedPreference(mActivity, PrefConstant.KEY_TOTAL_MOENY, PrefConstant.NAME_TOTAL_MOENY, itemAddPay);
                    mLisner.setCopyMoney(itemAddPay, itemAdditionalDatas.size());

                    notifyDataSetChanged();
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });
    }

    /**
     * 태그 TestView
     *
     * @param additionalData
     */
    private void setTagTextView(ItemAdditionalData additionalData, int position) {
        //태그 노출 우선순위
        try {

            if (additionalData.getOptions().get(position).getOptionType() == 3) {
                tvTagTypeA.setText("수선");
            } else if (additionalData.getOptions().get(position).getOptionType() == 1) {
                tvTagTypeA.setText("기술");
            } else if (additionalData.getOptions().get(position).getOptionType() == 2) {
                tvTagTypeA.setText("부속품");
            } else if (!TextUtils.isEmpty(ManagerItemAdditionalData.getInstance().getItemAdditionalData().get(position).getPhotoUri())) {
                tvTagTypeA.setText("사진");
            } else {
                tvTagTypeA.setText("세탁옵셥");
            }

            if (TextUtils.isEmpty(tvTagTypeA.getText().toString()) || tvTagTypeA.getText().toString() == null || tvTagTypeA.getText().toString().equals("")) {
                tvTagTypeE.setVisibility(View.GONE);
                tvTagTypeA.setVisibility(View.GONE);
            } else {
                tvTagTypeE.setVisibility(View.VISIBLE);
                tvTagTypeA.setVisibility(View.VISIBLE);
                tvTagTypeE.setText("+" + String.valueOf(additionalData.getTagCount()));
            }

        } catch (Exception err) {
            tvTagTypeE.setVisibility(View.GONE);
            tvTagTypeA.setVisibility(View.GONE);
            err.printStackTrace();
        }
    }

    public void addItem(ArrayList<ItemAdditionalData> item) {
        itemDataList = item;
    }
}
