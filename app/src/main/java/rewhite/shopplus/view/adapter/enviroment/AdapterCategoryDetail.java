package rewhite.shopplus.view.adapter.enviroment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.ClassListModel;

/**
 * 부속품 추가 Adapter
 */
public class AdapterCategoryDetail extends RecyclerView.Adapter {
    private static final String TAG = "AdapterCategoryDetail";
    private Context mContext;

    private int selectedPosition = 0;
    private List<ClassListModel> classListModelList;

    private AdapterCategoryDetail.CCViewHolder.OnAdapterListner mLisner;
    private int mType;

    public AdapterCategoryDetail(Context context, AdapterCategoryDetail.CCViewHolder.OnAdapterListner lisner, int type) {
        this.mContext = context;
        this.mType = type;
        this.mLisner = lisner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_laundry_add, parent, false);
        RecyclerView.ViewHolder holder = new AdapterCategoryDetail.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ClassListModel classListModel = classListModelList.get(position);
        TextView tvItemRegistration = holder.itemView.findViewById(R.id.tv_item_registration);

        tvItemRegistration.setText(classListModel.getClassName());


        if (mType == 0) {
            setSelectedPosition(position, tvItemRegistration);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = position;
                    mLisner.onClick(classListModelList.get(position).getClassID());
                    notifyDataSetChanged();
                }
            });
        } else {
            tvItemRegistration.setBackgroundResource(R.color.color_ebebeb);
            tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
        }
    }

    /**
     * 선택된 아이템 리스트 UI 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setSelectedPosition(int position, TextView view) {
        if (selectedPosition == position) {
            view.setBackgroundResource(R.drawable.a_button_type_2);
            view.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else {
            view.setBackgroundResource(R.drawable.a_button_type_3);
            view.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
        }
    }

    @Override
    public int getItemCount() {
        return classListModelList == null ? 0 : classListModelList.size();
    }

    public void addItem(List<ClassListModel> classListModels) {
        classListModelList = classListModels;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterCategoryDetail.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterCategoryDetail.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
