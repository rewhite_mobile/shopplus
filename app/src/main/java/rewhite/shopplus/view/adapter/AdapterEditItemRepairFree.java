package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.data.manager.ManagerRepairFreeEdit;
import rewhite.shopplus.data.dto.ItemEditItemRepair;
import rewhite.shopplus.data.dto.ItemRepairFree;
import rewhite.shopplus.data.manager.ManagerEditItemRepair;
import rewhite.shopplus.fragment.sub.FragmentEditItemRepairFee;
import rewhite.shopplus.util.Logger;

/**
 * 품목 수정 ->수선 View
 */
public class AdapterEditItemRepairFree extends RecyclerView.Adapter {
    private static final String TAG = "AdapterEditItemRepairFree";
    private Context mContext;

    private int selectedPosition = -1;
    private List<StoreItemList> mLaundryCategoryList;

    private AdapterComponent.CCViewHolder.OnAdapterListner mLisner;
    private FragmentEditItemRepairFee.onCompletItem mListner;

    private String mSelected;

    public AdapterEditItemRepairFree(Context context, FragmentEditItemRepairFee.onCompletItem listner, AdapterComponent.CCViewHolder.OnAdapterListner lisner, String type) {
        this.mContext = context;
        this.mListner = listner;
        this.mLisner = lisner;
        this.mSelected = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_repair_free, parent, false);
        RecyclerView.ViewHolder holder = new AdapterComponent.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        StoreItemList storeItemList = mLaundryCategoryList.get(position);

        ArrayList<ItemEditItemRepair> itemRepairs = new ArrayList<>();
        CheckBox checkBoxBg = (CheckBox) holder.itemView.findViewById(R.id.checkbox_bg);
        final TextView tvItemRegistration = (TextView) holder.itemView.findViewById(R.id.tv_item_registration);
        final TextView tvWon = (TextView) holder.itemView.findViewById(R.id.tv_won);

        viewSelecitionPrevious(storeItemList.getItemName(), checkBoxBg, tvItemRegistration, tvWon);

        tvItemRegistration.setText(storeItemList.getItemName());
        tvWon.setText(String.valueOf(storeItemList.getItemVisitPrice()));

        List<ItemEditItemRepair> itemEditItemRepairs = ManagerEditItemRepair.getmInstance().getManagerItemRepair();

        if (mSelected.equals("N")) {
            for (ItemEditItemRepair itemEditItemRepair : itemEditItemRepairs) {
                if (itemEditItemRepair.getOrderItemName().equals(storeItemList.getItemName())) {
                    checkBoxBg.setBackgroundResource(R.color.color_ebebeb);
                    checkBoxBg.setEnabled(false);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.color_0fc5dc));
                }
            }
        } else {
            for (ItemEditItemRepair itemEditItemRepair : itemEditItemRepairs) {
                if (itemEditItemRepair.getOrderItemName().equals(storeItemList.getItemName())) {
                    ItemEditItemRepair itemEditItem = new ItemEditItemRepair();

                    checkBoxBg.setBackgroundResource(R.drawable.a_button_type_2);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));

                    itemEditItem.setOrderItemName(itemEditItemRepair.getOrderItemName());
                    itemEditItem.setOrderItemAmount(itemEditItemRepair.getOrderItemAmount());
                    itemEditItem.setOrderItemID(itemEditItemRepair.getOrderItemID());

                    itemRepairs.add(itemEditItem);
                    ManagerEditItemRepair.getmInstance().setManagerItemRepair(itemRepairs);
                }
            }
        }

        checkBoxBg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Logger.d(TAG, "1");
                    checkBoxBg.setBackgroundResource(R.drawable.a_button_type_2);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    mListner.setSelectItem(mLaundryCategoryList.get(position).getItemName(), tvWon.getText().toString(), mLaundryCategoryList.get(position).getStoreItemID());
                } else {
                    Logger.d(TAG, "2");
                    checkBoxBg.setBackgroundResource(R.drawable.a_button_type_3);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.color_0fc5dc));
                    mListner.setUnSelectItem(mLaundryCategoryList.get(position).getItemName(), tvWon.getText().toString(), mLaundryCategoryList.get(position).getStoreItemID());
                }
            }
        });
    }

    /**
     * 이전 선택된 Item View Check
     *
     * @param itemName
     * @param tvItemRegistration
     * @param tvWon
     */
    private void viewSelecitionPrevious(String itemName, CheckBox checkBoxBg, TextView tvItemRegistration, TextView tvWon) {
        ArrayList<ItemRepairFree> itemComponentList = ManagerRepairFreeEdit.getInstance().getRepairFreeDates();
        if (itemComponentList.size() != 0) {
            for (ItemRepairFree itemRepairFree : new ArrayList<>(itemComponentList)) {
                if (itemRepairFree.getTitleName().equals(itemName)) {
                    checkBoxBg.setChecked(true);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mLaundryCategoryList == null ? 0 : mLaundryCategoryList.size();
    }

    public void addItem(List<StoreItemList> laundryCategoryList) {
        mLaundryCategoryList = laundryCategoryList;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterComponent.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterComponent.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}

