package rewhite.shopplus.view.adapter.enviroment;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.fragment.enviroment.FragmentOpeningHours;

/**
 * 환경설정 -> 예정휴무일 제거 Adapter
 */
public class AdapterCloseData extends RecyclerView.Adapter {
    private static final String TAG = "AdapterLaundryRateTable";
    private AppCompatActivity mActivity;

    private int selectedPosition = -1;
    private AdapterCloseData.CCViewHolder.OnAdapterListner mListner;
    private ArrayList<String> mItemList;
    private FragmentOpeningHours.onCompletItem mCompletListener;

    public AdapterCloseData(AppCompatActivity activity, AdapterCloseData.CCViewHolder.OnAdapterListner lisner, FragmentOpeningHours.onCompletItem completListener) {
        this.mActivity = activity;
        this.mListner = lisner;
        this.mCompletListener = completListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_close_data, parent, false);
        RecyclerView.ViewHolder holder = new AdapterCloseData.CCViewHolder(v, mListner);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        String closeData = mItemList.get(position);

        TextView tvCloseData = (TextView) holder.itemView.findViewById(R.id.tv_close_view);
        tvCloseData.setText(closeData.toString());

        tvCloseData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCompletListener.setRemoveClosingDay(closeData.toString());

            }
        });
    }

    public void addItem(ArrayList<String> itemList) {
        mItemList = itemList;
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterCloseData.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterCloseData.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
