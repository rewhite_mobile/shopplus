package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.FaqCategoryListModel;

/**
 * 자주하는 질문 카테고리
 */
public class AdapterServiceCenterCategory extends RecyclerView.Adapter {
    private static final String TAG = "AdapterServiceCenterCategory";
    private Context mContext;

    private int selectedPosition = 0;
    private FaqCategoryListModel faqCategoryListModel;
    private List<FaqCategoryListModel> mLaundryCategoryList;

    private AdapterServiceCenterCategory.CCViewHolder.OnAdapterListner mLisner;

    public AdapterServiceCenterCategory(Context context, AdapterServiceCenterCategory.CCViewHolder.OnAdapterListner lisner) {
        this.mContext = context;
        this.mLisner = lisner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service_center_category, parent, false);
        RecyclerView.ViewHolder holder = new AdapterServiceCenterCategory.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        TextView tvItemName = (TextView) holder.itemView.findViewById(R.id.tv_item_name);
        faqCategoryListModel = mLaundryCategoryList.get(position);

        tvItemName.setText(faqCategoryListModel.getFaqCategoryName());

        if (position == 0) {
            tvItemName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            tvItemName.setBackgroundResource(R.drawable.a_button_type_2);
        }

        setSelectedPosition(position, tvItemName);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
            }
        });
    }

    /**
     * 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setSelectedPosition(int position, TextView tvItemName) {
        if (selectedPosition == position) {
            mLisner.onClick(mLaundryCategoryList.get(position).getFaqCategoryID());
            tvItemName.setBackgroundResource(R.drawable.a_button_type_2);
            tvItemName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else {
            tvItemName.setBackgroundResource(R.drawable.a_button_type_3);
            tvItemName.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
        }
    }

    @Override
    public int getItemCount() {
        return mLaundryCategoryList == null ? 0 : mLaundryCategoryList.size();
    }

    public void addItem(List<FaqCategoryListModel> laundryCategoryList) {
        mLaundryCategoryList = laundryCategoryList;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterServiceCenterCategory.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterServiceCenterCategory.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);
            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}