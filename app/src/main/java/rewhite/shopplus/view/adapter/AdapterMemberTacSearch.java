package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.fragment.main.FragmentHome.onContentItemClickListener;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;

public class AdapterMemberTacSearch extends BaseAdapter {
    private static final String TAG = "AdapterMemberTacSearch";

    private Context mContext;
    private onContentItemClickListener mListener;

    private List<GetVisitOrderList> mListSite = new ArrayList<>();
    private GetVisitOrderList getVisitOrderList;

    public AdapterMemberTacSearch(Context context, onContentItemClickListener listener) {
        mContext = context;
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mListSite.size();
    }

    @Override
    public Object getItem(int position) {
        return mListSite.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        getVisitOrderList = mListSite.get(position);

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_member_tac_search, parent, false);
            setLayout(v);
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.memberSearch(getVisitOrderList.getTagNo());
            }
        });
        return v;

    }

    private void setLayout(View v) {
        TextView tvTagNumber = (TextView) v.findViewById(R.id.tv_tag_number);
        ImageView imgTacColor = (ImageView) v.findViewById(R.id.img_tac_color);
        TextView tvMemberName = (TextView) v.findViewById(R.id.tv_member_name);
        TextView tvMemberPhoneNumber = (TextView) v.findViewById(R.id.tv_member_phone_number);
        TextView tvMemberAddress = (TextView) v.findViewById(R.id.tv_member_address);
        TextView tvItemName = (TextView) v.findViewById(R.id.tv_item_name);
        TextView tvEnterDate = (TextView) v.findViewById(R.id.tv_enter_date);
        TextView tvPrice = (TextView) v.findViewById(R.id.tv_price);
        TextView tvNonPrice = (TextView) v.findViewById(R.id.tv_non_price);


        int idx = getVisitOrderList.getTagNo().indexOf("|");
        if (idx == 0) {
            tvTagNumber.setText(CommonUtil.setTagFormater(getVisitOrderList.getTagNo()));
        } else if (getVisitOrderList.getTagNo().length() >= 6 && getVisitOrderList.getTagNo().length() < 9) {
            tvTagNumber.setText(CommonUtil.setFormater(getVisitOrderList.getTagNo()));
        } else if (getVisitOrderList.getTagNo().contains("-")) {
            tvTagNumber.setText(CommonUtil.setTagFormaterChange(getVisitOrderList.getTagNo()));
        }

        CommonUtil.getTacColor(mContext, Integer.valueOf(getVisitOrderList.getTagColor()), imgTacColor);
        tvMemberName.setText(getVisitOrderList.getUserName());
        tvMemberPhoneNumber.setText(getVisitOrderList.get_UserPhone());
        tvMemberAddress.setText(getVisitOrderList.get_UserAddress());
        tvItemName.setText(getVisitOrderList.getStoreItemName());

        try {
            tvEnterDate.setText(DataFormatUtil.setDateItemCancelFormat(getVisitOrderList.getEnterDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tvPrice.setText(DataFormatUtil.moneyFormatToWon(getVisitOrderList.getPayPrice()));
        tvNonPrice.setText(DataFormatUtil.moneyFormatToWon(getVisitOrderList.getNonPayPrice()));
    }

    public void addItem(List<GetVisitOrderList> listSite) {
        mListSite = listSite;
    }
}