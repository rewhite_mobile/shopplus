package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.CancelAvailOrder;
import rewhite.shopplus.common.popup.activity.ActivityItemCancellation;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 품목취소 Item Adapter
 */
public class AdapterItemCancle extends BaseAdapter {
    private static final String TAG = "AdapterItemCancle";

    private List<CancelAvailOrder> mList = new ArrayList<>();
    private ActivityItemCancellation.itemCancle mListener;
    private CancelAvailOrder cancelAvailOrder;

    private Context mContext;
    private boolean isCheckedStatus;

    private CheckBox cbCheck;
    private TextView tvTagNumber;
    private ImageView ivTagColor;
    private TextView tvItemName;
    private TextView tvStatus;
    private TextView tvReWashing;
    private TextView tvMoneyCount;
    private TextView tvReceivables;
    private RecyclerView recyclerView;


    public AdapterItemCancle(Context context, boolean isCheckedStatus, ActivityItemCancellation.itemCancle listener) {
        this.mContext = context;
        this.isCheckedStatus = isCheckedStatus;
        this.mListener = listener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        cancelAvailOrder = mList.get(position);
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_adapter_item_cancle, parent, false);
        }

        setLayout(v);
        setTextView(position);
        setTagAdapter(v);
        return v;
    }

    public void addItem(List<CancelAvailOrder> item) {
        mList = item;
    }

    private void setLayout(View v) {
        cbCheck = (CheckBox) v.findViewById(R.id.cb_check);
        tvTagNumber = (TextView) v.findViewById(R.id.tv_tag_number);
        ivTagColor = (ImageView) v.findViewById(R.id.iv_tag_color);
        tvItemName = (TextView) v.findViewById(R.id.tv_item_name);
        tvStatus = (TextView) v.findViewById(R.id.tv_status);
        tvReWashing = (TextView) v.findViewById(R.id.tv_re_washing);
        tvMoneyCount = (TextView) v.findViewById(R.id.tv_money_count);
        tvReceivables = (TextView) v.findViewById(R.id.tv_receivables);
        recyclerView = (RecyclerView) v.findViewById(R.id.rv_tag);
    }

    private void setTextView(int position) {
        if (isCheckedStatus) {
            cbCheck.setChecked(true);
        } else {
            cbCheck.setChecked(false);
        }

        int idx = mList.get(position).getTagNo().indexOf("|");
        if (idx == 0) {
            tvTagNumber.setText(CommonUtil.setTagFormater(mList.get(position).getTagNo()));
        } else if (mList.get(position).getTagNo().length() >= 6 && mList.get(position).getTagNo().length() < 9) {
            tvTagNumber.setText(CommonUtil.setFormater(mList.get(position).getTagNo()));
        } else if (mList.get(position).getTagNo().contains("-")) {
            tvTagNumber.setText(CommonUtil.setTagFormaterChange(mList.get(position).getTagNo()));
        }

        tvItemName.setText(mList.get(position).getStoreItemName());
        tvStatus.setText(mList.get(position).getOrderItemStatusDesc());

        CommonUtil.getTacColor(mContext, Integer.valueOf(mList.get(position).getItemColor()), ivTagColor);
        try {
            tvReWashing.setText(DataFormatUtil.setDateItemCancelFormat(mList.get(position).getEnterDate()));
            tvMoneyCount.setText(DataFormatUtil.moneyFormatToWon(mList.get(position).getPayPrice()));
            tvReceivables.setText(DataFormatUtil.moneyFormatToWon(mList.get(position).getNonPayPrice()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        cbCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mListener.setSelectItem(cancelAvailOrder);
                } else {
                    mListener.setUnSelectItem(cancelAvailOrder);
                }
            }
        });
    }

    private void setTagAdapter(View view) {
        ArrayList<String> testCode = new ArrayList<>();
        for (int i = 0; i < cancelAvailOrder.getBadges().size(); i++) {
            testCode.add(cancelAvailOrder.getBadges().get(i).toString());
        }

        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(6, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        AdapterCommonTag adapterCommonTag = new AdapterCommonTag(mContext, mOnAdapterListner);
        adapterCommonTag.addItem(testCode);
        recyclerView.setAdapter(adapterCommonTag);
    }


    private AdapterCommonTag.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterCommonTag.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
}
