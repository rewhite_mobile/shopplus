package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.util.SharedPreferencesUtil;

import static rewhite.shopplus.util.CommonUtil.setTacColorView;

public class AdapterTacColor extends RecyclerView.Adapter {
    private static final String TAG = "AdapterTacColor";
    private Context mContext;

    private int mPosition;
    private int selectedPosition = -1;

    private List<String> mLaundryCategoryList;

    private CCViewHolder.OnAdapterListner mLisner;

    public AdapterTacColor(Context context, CCViewHolder.OnAdapterListner lisner) {
        this.mContext = context;
        this.mLisner = lisner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tac_adapter_color, parent, false);
        RecyclerView.ViewHolder holder = new AdapterTacColor.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        RelativeLayout llBg = (RelativeLayout) holder.itemView.findViewById(R.id.rl_bg);
        TextView tvColorName = (TextView) holder.itemView.findViewById(R.id.rv_color_name);
        View view = (View) holder.itemView.findViewById(R.id.view);

        setTacColorView(mContext, position, llBg, tvColorName);
        setSelectedPosition(position, view);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                SharedPreferencesUtil.putSharedPreference(mContext, PrefConstant.KEY_START_TAC_NUMBER, PrefConstant.NAME_START_TAC_NUMBER_POSITION, position);
                notifyDataSetChanged();
            }
        });
    }

    /**
     * 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setSelectedPosition(int position, View view) {
        if (selectedPosition == position) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mLaundryCategoryList == null ? 0 : mLaundryCategoryList.size();
    }

    public void addItem(List<String> laundryCategoryList) {
        mLaundryCategoryList = laundryCategoryList;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private OnAdapterListner listner;

        public CCViewHolder(View view, OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
