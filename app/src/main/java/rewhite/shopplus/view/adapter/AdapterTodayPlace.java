package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.common.naver.NMapViewer;

public class AdapterTodayPlace extends BaseAdapter {
    private static final String TAG = "AdapterCollection";

    private ArrayList<String> mItems;
    private Context mContext;

    public AdapterTodayPlace(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_today_place, parent, false);
        }
        LinearLayout llMap = (LinearLayout) convertView.findViewById(R.id.ll_map);
        LinearLayout llDetails = (LinearLayout) convertView.findViewById(R.id.ll_details);


        llMap.setOnClickListener(btnOnClickListener);
        llDetails.setOnClickListener(btnOnClickListener);

        return convertView;
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.ll_map:       //네이버 지도
                    Intent intent = new Intent(mContext, NMapViewer.class);
                    mContext.startActivity(intent);
                    break;

                case R.id.ll_details:   //자세히 보기

                    break;
            }
        }
    };

    public void addItem(ArrayList<String> item) {
        mItems = item;
    }
}