package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetMileageHistory;
import rewhite.shopplus.util.DataFormatUtil;

public class AdapterMileage extends BaseAdapter {
    private static final String TAG = "AdapterMileage";

    private List<GetMileageHistory> mList = new ArrayList<>();
    private Context mContext;

    public AdapterMileage(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_adapter_mileage, parent, false);
        }
        TextView tvMileageDate = (TextView) v.findViewById(R.id.tv_mileage_date);
        TextView tvEarningInformation = (TextView) v.findViewById(R.id.tv_earning_information);
        TextView tvMileage = (TextView) v.findViewById(R.id.tv_mileage);

        try {
            tvMileageDate.setText(DataFormatUtil.setDateMothFormat(mList.get(position).getApplyDate()));

            if (mList.get(position).getChangeTypeDesc().equals("적립")) {
                tvEarningInformation.setText(mList.get(position).getChangeTypeDesc());
                tvEarningInformation.setTextColor(mContext.getResources().getColor(R.color.color_ffa800));
                tvMileage.setText("+ " + DataFormatUtil.moneyFormatToWon(mList.get(position).getAmount()));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return v;
    }


    public void addItem(List<GetMileageHistory> item) {
        mList = item;
    }
}
