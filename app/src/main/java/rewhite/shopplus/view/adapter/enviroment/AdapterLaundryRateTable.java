package rewhite.shopplus.view.adapter.enviroment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.LaundryCategoryItemList;


public class AdapterLaundryRateTable extends RecyclerView.Adapter {
    private static final String TAG = "AdapterLaundryRateTable";
    private Context mContext;

    private int selectedPosition = 0;
    private AdapterLaundryRateTable.CCViewHolder.OnAdapterListner mLisner;
    private List<LaundryCategoryItemList> mItemList;

    public AdapterLaundryRateTable(Context context, AdapterLaundryRateTable.CCViewHolder.OnAdapterListner lisner) {
        this.mContext = context;
        this.mLisner = lisner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_laundry_rate_teble, parent, false);
        RecyclerView.ViewHolder holder = new AdapterLaundryRateTable.CCViewHolder(v, mLisner);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ImageView imgBg = (ImageView) holder.itemView.findViewById(R.id.img_bg);
        TextView tvCheckDate = (TextView) holder.itemView.findViewById(R.id.tv_item_registration);
        LaundryCategoryItemList LaundryCategoryItem = mItemList.get(position);

        tvCheckDate.setText(LaundryCategoryItem.getCategoryName());
        setSelectedPosition(position, tvCheckDate, imgBg);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                mLisner.onClick(position);
                notifyDataSetChanged();
            }
        });
    }

    /**
     * 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position    해당 view position
     * @param regisration 해당 registration
     * @param imgBg       해당 background
     */
    private void setSelectedPosition(int position, TextView regisration, ImageView imgBg) {
        if (selectedPosition == position) {
            regisration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            imgBg.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_2));
        } else {
            regisration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
            imgBg.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_3));
        }
    }

    /**
     * 이전 선택된 Item View Check
     *
     * @param itemName
     * @param tvItemRegistration
     * @param tvWon
     */
    private void viewSelecitionPrevious(String itemName, CheckBox checkBoxBg, TextView tvItemRegistration, TextView tvWon) {

    }

    public void addItem(List<LaundryCategoryItemList> itemList) {
        mItemList = itemList;
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterLaundryRateTable.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterLaundryRateTable.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
