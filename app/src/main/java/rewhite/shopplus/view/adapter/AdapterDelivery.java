package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import rewhite.shopplus.R;

/**
 * Main 오늘의 수거배달 배송 View
 */
public class AdapterDelivery extends BaseAdapter {
    private static final String TAG = "AdapterCollection";

    private ArrayList<String> mItems;
    private Context mContext;

    public AdapterDelivery(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_delivery, parent, false);
        }

        return convertView;
    }

    public void addItem(ArrayList<String> item) {
        mItems = item;
    }
}