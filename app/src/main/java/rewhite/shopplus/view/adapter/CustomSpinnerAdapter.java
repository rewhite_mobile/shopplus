package rewhite.shopplus.view.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import rewhite.shopplus.R;

/**
 * Created by wogns304 on 2018. 3. 5..
 * 스피너 커스텀해 공통 사용하는 CLASS
 */

public class CustomSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private final Context ctx;
    private ArrayList<String> items;
    private Activity mActivity;
    private Typeface typeFace;

    public CustomSpinnerAdapter(Activity activity, Context ctx, ArrayList<String> items) {
        this.items = items;
        this.ctx = ctx;
        this.mActivity = activity;
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(int i) {
        return items.get(i);
    }

    public long getItemId(int i) {
        return (long)i;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        typeFace = Typeface.createFromAsset(mActivity.getAssets(), "nanum_square_otfr_egular.otf");

        TextView txt = new TextView(mActivity);
        txt.setPadding(12, 16, 15, 16);
        txt.setTextSize(12);
        txt.setGravity(Gravity.CENTER_VERTICAL);
        txt.setText(items.get(position));
        txt.setTextColor(mActivity.getResources().getColor(R.color.color_43425c));
        txt.setTypeface(typeFace);
        return  txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        typeFace = Typeface.createFromAsset(mActivity.getAssets(), "nanum_square_otfr_egular.otf");

        TextView txt = new TextView(mActivity);
        txt.setPadding(12, 16, 15, 16);
        txt.setTextSize(12);
        txt.setText(items.get(i));
        txt.setTextColor(mActivity.getResources().getColor(R.color.color_43425c));
        txt.setTypeface(typeFace);
        return  txt;
    }
}