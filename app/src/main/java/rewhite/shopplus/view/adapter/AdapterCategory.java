package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.LaundryCategoryItemList;
import rewhite.shopplus.fragment.sub.FragmentCategory;
import rewhite.shopplus.util.Logger;

public class AdapterCategory extends RecyclerView.Adapter {
    private static final String TAG = "AdapterCategory";
    private Context mContext;

    private List<LaundryCategoryItemList> mLaundryCategoryItemList;

    private LaundryCategoryItemList laundryCategoryItemList;
    private AdapterComponent.CCViewHolder.OnAdapterListner mLisner;
    private FragmentCategory.onCompletItem mListner;
    private String mItemName;
    private int selectedPosition = -1;

    public AdapterCategory(Context context, FragmentCategory.onCompletItem listner, AdapterComponent.CCViewHolder.OnAdapterListner lisner) {
        this.mContext = context;
        this.mListner = listner;
        this.mLisner = lisner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_category, parent, false);
        RecyclerView.ViewHolder holder = new AdapterComponent.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        laundryCategoryItemList = mLaundryCategoryItemList.get(position);

        TextView tvItemRegistration = (TextView) holder.itemView.findViewById(R.id.tv_item_registration);
        LinearLayout llBg = (LinearLayout) holder.itemView.findViewById(R.id.ll_bg);

        tvItemRegistration.setText(laundryCategoryItemList.getCategoryName());
        setSelectedPosition(position, tvItemRegistration, llBg);

        if (!TextUtils.isEmpty(mItemName)) {
            if (mLaundryCategoryItemList.get(position).equals(mItemName)) {

                tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
                llBg.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_3));
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.d(TAG, "mLaundryCategoryItemList.get(position).getCategoryId() : " + mLaundryCategoryItemList.get(position).getCategoryId());
                mListner.setSelectItem(mLaundryCategoryItemList.get(position).getCategoryName(), mLaundryCategoryItemList.get(position).getCategoryId()); //클릭 인터페이스 데이터 전달

                selectedPosition = position;
                mLisner.onClick(position);
                notifyDataSetChanged();
            }
        });
    }

    /**
     * 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position    해당 view position
     * @param regisration 해당 registration
     * @param llView      해당 background
     */
    private void setSelectedPosition(int position, TextView regisration, LinearLayout llView) {
        if (selectedPosition == position) {
            regisration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            llView.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_2));
        } else {
            regisration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
            llView.setBackground(mContext.getResources().getDrawable(R.drawable.a_button_type_3));
        }
    }

    @Override
    public int getItemCount() {
        return mLaundryCategoryItemList == null ? 0 : mLaundryCategoryItemList.size();
    }

    public void addItem(List<LaundryCategoryItemList> laundryCategoryItemList) {
        mLaundryCategoryItemList = laundryCategoryItemList;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterComponent.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterComponent.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
