package rewhite.shopplus.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import rewhite.shopplus.fragment.main.FragmentAttemptedDelivery;
import rewhite.shopplus.fragment.main.FragmentUnreleased;

/**
 * MainActivity 오늘의 수거 배송 SettingViewPagerAdater
 */
public class AdapterVisitViewPager extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterMainPager";

    public AdapterVisitViewPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentUnreleased();// 수거
            case 1:
                return new FragmentAttemptedDelivery();// 배송
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}