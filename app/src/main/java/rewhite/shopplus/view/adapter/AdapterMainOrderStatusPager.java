package rewhite.shopplus.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import rewhite.shopplus.fragment.main.FragmentMainAttemptedDelivery;
import rewhite.shopplus.fragment.main.FragmentMainUnreleased;
import rewhite.shopplus.util.Logger;

/**
 * Main View
 */
public class AdapterMainOrderStatusPager extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterMainOrderStatusPager";

    public AdapterMainOrderStatusPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Logger.d(TAG, "positiosn : " + position);
        switch (position) {
            case 0:
                return new FragmentMainUnreleased(); //미출고
            case 1:
                return new FragmentMainAttemptedDelivery(); //출고미수
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
