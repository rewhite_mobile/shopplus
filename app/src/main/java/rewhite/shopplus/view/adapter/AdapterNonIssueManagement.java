package rewhite.shopplus.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import rewhite.shopplus.fragment.sub.FragmentMissedItemList;
import rewhite.shopplus.fragment.sub.FragmentMissingMembersList;

/**
 * 미수금 관리 탭 - 미수회원 목록 / 미수 품목 목록
 */
public class AdapterNonIssueManagement extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterAcoutsManagement";

    public AdapterNonIssueManagement(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentMissingMembersList();    //미출고 회원 목록
            case 1:
                return new FragmentMissedItemList();        //미출고 품목 목록
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}