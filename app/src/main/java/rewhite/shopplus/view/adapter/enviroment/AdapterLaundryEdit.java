package rewhite.shopplus.view.adapter.enviroment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetStoreItemList;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.fragment.enviroment.FragmentLaundryRateTable;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.Logger;

/**
 * 세탁요금표 Adapter
 */
public class AdapterLaundryEdit extends RecyclerView.Adapter {
    private static final String TAG = "AdapterLaundryEdit";
    private Context mContext;

    private int selectedPosition = -1;
    private List<GetStoreItemList> mItemList;
    private AdapterLaundryEdit.CCViewHolder.OnAdapterListner mLisner;
    private FragmentLaundryRateTable.existingCharge mListner;
    private InputMethodManager imm;

    private TextView tvNormalRate, tvLuxuryRate, tvChildRate;
    private TextView tvItemName;
    private RadioGroup rgTreatment;
    private RadioButton rbTreatment, rbUntreatment;

    private GetStoreItemList getStoreItemList;

    public AdapterLaundryEdit(Context context, AdapterLaundryEdit.CCViewHolder.OnAdapterListner lisner, FragmentLaundryRateTable.existingCharge listner) {
        this.mContext = context;
        this.mLisner = lisner;
        this.mListner = listner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_laundry_edit, parent, false);
        RecyclerView.ViewHolder holder = new AdapterLaundryEdit.CCViewHolder(v, mLisner);


        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        getStoreItemList = mItemList.get(position);

        tvItemName = (TextView) holder.itemView.findViewById(R.id.tv_item_name);

        tvNormalRate = (TextView) holder.itemView.findViewById(R.id.tv_normal_rate);
        tvLuxuryRate = (TextView) holder.itemView.findViewById(R.id.tv_luxury_rate);
        tvChildRate = (TextView) holder.itemView.findViewById(R.id.tv_child_rate);

        rgTreatment = (RadioGroup) holder.itemView.findViewById(R.id.rg_treatment);
        rbTreatment = (RadioButton) holder.itemView.findViewById(R.id.rb_treatment);
        rbUntreatment = (RadioButton) holder.itemView.findViewById(R.id.rb_untreatment);
        tvItemName.setText(getStoreItemList.getItemName());

        setTextView(getStoreItemList);
        setCheckedView(getStoreItemList, position);

        rgTreatment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                boolean isChecked = (checkedId == R.id.rb_treatment);
                mLisner.onClick(0);
                if (isChecked) {
                    EnviromentManager.getUpdateStoreItemState(mContext, mItemList.get(position).getStoreItemID(), "Y");
                } else {
                    EnviromentManager.getUpdateStoreItemState(mContext, mItemList.get(position).getStoreItemID(), "N");
                }
                mListner.setNotToUse();
            }
        });
    }


    /**
     * 사용여부 CHeckView
     *
     * @param position
     * @param tvNormalRate
     */
    private void setSelectedPosition(int position, TextView tvNormalRate) {
        if (selectedPosition == position) {
            tvNormalRate.setBackgroundResource(R.drawable.a_button_type_10);
        } else {
            tvNormalRate.setBackgroundResource(R.drawable.a_button_type_3);
        }
    }


    /**
     * TextSetting View 품목 텍스트
     *
     * @param getStoreItemList
     */
    private void setTextView(GetStoreItemList getStoreItemList) {
        tvNormalRate.setText(String.valueOf(getStoreItemList.getItemVisitPrice()));
        tvLuxuryRate.setText(String.valueOf(getStoreItemList.getItemVisitPrice2()));
        tvChildRate.setText(String.valueOf(getStoreItemList.getItemVisitPrice3()));
    }

    /**
     * setCheckedView 품목 상태 변경 상태
     *
     * @param getStoreItemList
     */
    private void setCheckedView(GetStoreItemList getStoreItemList, int position) {
        if (getStoreItemList.getIsUse().equals("Y")) {
            setSelectedPosition(position, tvNormalRate);

            rbTreatment.setBackgroundResource(R.drawable.a_button_type_2);
            rbUntreatment.setBackgroundResource(R.drawable.a_button_type_3);

            rbTreatment.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            rbUntreatment.setTextColor(mContext.getResources().getColor(R.color.color_43425c));

            tvItemName.setBackgroundResource(R.drawable.a_button_type_3);
            tvItemName.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
            tvNormalRate.setBackgroundResource(R.drawable.a_button_type_3);
            tvNormalRate.setTextColor(mContext.getResources().getColor(R.color.color_0fc5dc));
            tvLuxuryRate.setBackgroundResource(R.drawable.a_button_type_3);
            tvLuxuryRate.setTextColor(mContext.getResources().getColor(R.color.color_0fc5dc));
            tvChildRate.setBackgroundResource(R.drawable.a_button_type_3);
            tvChildRate.setTextColor(mContext.getResources().getColor(R.color.color_0fc5dc));

            if (getStoreItemList.getItemType() == 2) {
                tvItemName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Logger.d(TAG, "getStoreItemList.getItemName() : " + getStoreItemList.getItemName());
                        mListner.setItemName(getStoreItemList.getStoreItemID(), getStoreItemList.getItemName());
                    }
                });
            }
            tvNormalRate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Logger.d(TAG, "일반 : position");

                    tvNormalRate.setBackgroundResource(R.drawable.a_button_type_10);
                    tvLuxuryRate.setBackgroundResource(R.drawable.a_button_type_3);
                    tvChildRate.setBackgroundResource(R.drawable.a_button_type_3);

                    CommonUtil.hideKeyboardFrom(mContext, tvNormalRate);

                    mListner.setExistionCharge(mItemList.get(position).getItemVisitPrice(), getStoreItemList.getItemName() + "(일반)", getStoreItemList.getStoreItemID(), 0
                            , mItemList.get(position).getItemVisitPrice(), mItemList.get(position).getItemVisitPrice2(), mItemList.get(position).getItemVisitPrice3());
                    notifyDataSetChanged();
                }
            });

            tvLuxuryRate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Logger.d(TAG, "명품/고급 : position");

                    tvLuxuryRate.setBackgroundResource(R.drawable.a_button_type_10);
                    tvNormalRate.setBackgroundResource(R.drawable.a_button_type_3);
                    tvChildRate.setBackgroundResource(R.drawable.a_button_type_3);

                    CommonUtil.hideKeyboardFrom(mContext, tvLuxuryRate);

                    mListner.setExistionCharge(mItemList.get(position).getItemVisitPrice2(), getStoreItemList.getItemName() + "(명품/고급)", getStoreItemList.getStoreItemID(), 1
                            , mItemList.get(position).getItemVisitPrice(), mItemList.get(position).getItemVisitPrice2(), mItemList.get(position).getItemVisitPrice3());
                    notifyDataSetChanged();
                }
            });

            tvChildRate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Logger.d(TAG, "아동 : position");

                    tvChildRate.setBackgroundResource(R.drawable.a_button_type_10);
                    tvNormalRate.setBackgroundResource(R.drawable.a_button_type_3);
                    tvLuxuryRate.setBackgroundResource(R.drawable.a_button_type_3);

                    CommonUtil.hideKeyboardFrom(mContext, tvChildRate);
                    mListner.setExistionCharge(mItemList.get(position).getItemVisitPrice3(), getStoreItemList.getItemName() + "(아동)", getStoreItemList.getStoreItemID(), 2
                            , mItemList.get(position).getItemVisitPrice(), mItemList.get(position).getItemVisitPrice2(), mItemList.get(position).getItemVisitPrice3());

                    notifyDataSetChanged();
                }
            });
        } else {
            rbUntreatment.setBackgroundResource(R.drawable.a_button_type_2);
            rbTreatment.setBackgroundResource(R.drawable.a_button_type_3);

            rbUntreatment.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
            rbTreatment.setTextColor(mContext.getResources().getColor(R.color.color_43425c));

            tvItemName.setBackgroundResource(R.drawable.a_button_type_11);
            tvItemName.setTextColor(mContext.getResources().getColor(R.color.color_cacaca));
            tvNormalRate.setBackgroundResource(R.drawable.a_button_type_11);
            tvNormalRate.setTextColor(mContext.getResources().getColor(R.color.color_cacaca));
            tvLuxuryRate.setBackgroundResource(R.drawable.a_button_type_11);
            tvLuxuryRate.setTextColor(mContext.getResources().getColor(R.color.color_cacaca));
            tvChildRate.setBackgroundResource(R.drawable.a_button_type_11);
            tvChildRate.setTextColor(mContext.getResources().getColor(R.color.color_cacaca));
        }
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    public void addItem(List<GetStoreItemList> itemList) {
        mItemList = itemList;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterLaundryEdit.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterLaundryEdit.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}