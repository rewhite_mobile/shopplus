package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityPayment;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.network.StoreUserApiManager;
import rewhite.shopplus.data.manager.ManagerGetVisitOrderItem;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;

public class AdapterMainOrderInquiry extends BaseAdapter {
    private static final String TAG = "AdapterMainOrderInquiry";
    private AppCompatActivity mActivity;
    private List<GetVisitOrderList> mItem;
    private static final String paymentType = "1";  // 결제하기 Type;

    private LinearLayout llOrderInformation;

    private TextView tvNonPrice, tvPrice, tvItemStatus, tvItemShipDate, tvItemRecetionDate;
    private TextView tvUserName, tvUserPhoneNumber, tvTagTypeE, tvTagTypeA, tvItemName, tvTacNumber, tvUserAddress;

    private ImageView imgTacColor;

    public AdapterMainOrderInquiry(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public int getCount() {
        return mItem.size();
    }

    @Override
    public Object getItem(int position) {
        return mItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_main_order_inquiry, parent, false);
        }

        setLayout(v, position);
        setTextView(position);
        return v;
    }

    private void setLayout(View v, int position) {
        tvItemStatus = (TextView) v.findViewById(R.id.tv_item_status);
        tvItemShipDate = (TextView) v.findViewById(R.id.tv_item_ship_date);
        tvItemRecetionDate = (TextView) v.findViewById(R.id.tv_item_recetion_date);
        tvUserName = (TextView) v.findViewById(R.id.tv_user_name);
        tvUserPhoneNumber = (TextView) v.findViewById(R.id.tv_user_phoneNumber);
        llOrderInformation = (LinearLayout) v.findViewById(R.id.ll_order_information);
        tvTagTypeE = (TextView) v.findViewById(R.id.tv_tag_type_e);
        tvTagTypeA = (TextView) v.findViewById(R.id.tv_tag_type_a);
        tvItemName = (TextView) v.findViewById(R.id.tv_item_name);

        tvPrice = (TextView) v.findViewById(R.id.tv_price);
        tvNonPrice = (TextView) v.findViewById(R.id.tv_non_price);
        tvTacNumber = (TextView) v.findViewById(R.id.tv_tac_number);
        tvUserAddress = (TextView) v.findViewById(R.id.tv_user_address);

        llOrderInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Logger.d(TAG, "mItem.get(position).getStoreItemId() : " + mItem.get(position).getStoreUserID());
                StoreUserApiManager.getStoreUserInfo(mActivity, Long.valueOf(mItem.get(position).getStoreUserID()));

                ManagerGetVisitOrderItem.getmInstance().setGetVisitOrder(mItem);

                Intent intent = new Intent(mActivity, ActivityPayment.class);
                intent.putExtra(paymentType, 3);
                mActivity.startActivity(intent);
            }
        });
    }

    private void setTextView(int position) {
        try {
            tvUserName.setText(mItem.get(position).getUserName());
            tvUserPhoneNumber.setText(mItem.get(position).get_UserPhone());
            tvUserAddress.setText(mItem.get(position).get_UserAddress());
            tvItemName.setText(mItem.get(position).getStoreItemName());

            int idx = mItem.get(position).getTagNo().indexOf("|");
            if (idx == 0) {
                tvTacNumber.setText(CommonUtil.setTagFormater(mItem.get(position).getTagNo()));
            } else if (mItem.get(position).getTagNo().length() >= 6 && mItem.get(position).getTagNo().length() < 9) {
                tvTacNumber.setText(CommonUtil.setFormater(mItem.get(position).getTagNo()));
            } else if (mItem.get(position).getTagNo().contains("-")) {
                tvTacNumber.setText(CommonUtil.setTagFormaterChange(mItem.get(position).getTagNo()));
            }

            try {
                if (mItem.get(position).getOutDate() != null || !TextUtils.isEmpty(mItem.get(position).getOutDate())) {
                    tvItemShipDate.setText(DataFormatUtil.setDateItemCancelFormat(mItem.get(position).getOutDate()));
                }
                tvItemRecetionDate.setText(DataFormatUtil.setDateItemCancelFormat(mItem.get(position).getEnterDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tvItemStatus.setText(mItem.get(position).getOrderItemStatusDesc());
            tvPrice.setText(DataFormatUtil.moneyFormatToWon(mItem.get(position).getOrderItemPrice()));
            tvNonPrice.setText(DataFormatUtil.moneyFormatToWon(mItem.get(position).getNonPayPrice()));
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    public void addItem(List<GetVisitOrderList> item) {
        mItem = item;
    }
}
