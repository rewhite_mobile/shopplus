package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import rewhite.shopplus.R;

public class ViewPagerAdapter extends PagerAdapter {
    int size;
    AppCompatActivity act;
    View layout;
    ImageView pagenumber1, pagenumber2, pagenumber3;
    ImageView pageImage;
    Button click;

    public ViewPagerAdapter(AppCompatActivity mainActivity, int noofsize) {
        // TODO Auto-generated constructor stub
        size = noofsize;
        act = mainActivity;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return size;
    }

    @Override
    public Object instantiateItem(View container, int position) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = inflater.inflate(R.layout.item_view_pager_adapter, null);

        pagenumber1 = (ImageView) layout.findViewById(R.id.pagenumber1);
        pagenumber2 = (ImageView) layout.findViewById(R.id.pagenumber2);
        pagenumber3 = (ImageView) layout.findViewById(R.id.pagenumber3);
        pageImage = (ImageView) layout.findViewById(R.id.imageView1);
        int pagenumberTxt = position + 1;

        try {
            if (pagenumberTxt == 1) {
                pageImage.setBackgroundResource(R.drawable.sean_1);

                pagenumber1.setBackgroundResource(R.drawable.ico_main_gnb_pagecontrol_01);
                pagenumber2.setBackgroundResource(R.drawable.ico_main_gnb_pagecontrol_00);
                pagenumber3.setBackgroundResource(R.drawable.ico_main_gnb_pagecontrol_00);

            } else if (pagenumberTxt == 2) {
                pageImage.setBackgroundResource(R.drawable.sean_2);

                pagenumber2.setBackgroundResource(R.drawable.ico_main_gnb_pagecontrol_01);
                pagenumber1.setBackgroundResource(R.drawable.ico_main_gnb_pagecontrol_00);
                pagenumber3.setBackgroundResource(R.drawable.ico_main_gnb_pagecontrol_00);
            } else if (pagenumberTxt == 3) {
                pageImage.setBackgroundResource(R.drawable.sean_3);

                pagenumber3.setBackgroundResource(R.drawable.ico_main_gnb_pagecontrol_01);
                pagenumber1.setBackgroundResource(R.drawable.ico_main_gnb_pagecontrol_00);
                pagenumber2.setBackgroundResource(R.drawable.ico_main_gnb_pagecontrol_00);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        ((ViewPager) container).addView(layout, 0);
        return layout;
    }

    @Override
    public void destroyItem(View arg0, int arg1, Object arg2) {
        ((ViewPager) arg0).removeView((View) arg2);
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}