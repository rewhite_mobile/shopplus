package rewhite.shopplus.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import rewhite.shopplus.fragment.main.FragmentOrderDetailView;
import rewhite.shopplus.fragment.main.FragmentQuickDetails;

/**
 * 주문조회 조회 Tab
 */
public class AdapterOrderInquiry extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterSalesStatus";

    public AdapterOrderInquiry(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentOrderDetailView(); // 월별매출현황
            case 1:
                return new FragmentQuickDetails();   //간편조회
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}