package rewhite.shopplus.view.adapter.enviroment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;

import rewhite.shopplus.fragment.enviroment.FragmentBasicSetting;
import rewhite.shopplus.fragment.enviroment.FragmentContractInformation;
import rewhite.shopplus.fragment.enviroment.FragmentEmployeeManagement;
import rewhite.shopplus.fragment.enviroment.FragmentOpeningHours;

public class AdapterSalseSetup extends FragmentStatePagerAdapter {
    private static final String TAG = "AdapterSalsesSetup";
    private LayoutInflater layoutInflater;

    private AppCompatActivity mActivity;
    public AdapterSalseSetup(FragmentManager fm, AppCompatActivity activity) {
        super(fm);
        this.mActivity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentBasicSetting(mActivity);   //기본설정
            case 1:
                return new FragmentOpeningHours(mActivity);    //영업시간 및 휴무설정
            case 2:
                return new FragmentEmployeeManagement(mActivity);  //직원관리
            case 3:
                return new FragmentContractInformation(mActivity);  //계약정보
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

}
