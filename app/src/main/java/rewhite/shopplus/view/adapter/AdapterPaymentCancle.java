package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.StorePaymentCancelAvailList;
import rewhite.shopplus.common.popup.activity.ActivityCancelPayment;
import rewhite.shopplus.common.popup.activity.ActivityDetailsPaymentInfoPreFilled;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;

/**
 * 결제 취소 ApdateView List
 */
public class AdapterPaymentCancle extends BaseAdapter {
    private static final String TAG = "AdapterPaymentCancle";
    private static final String PAYMENY_ID = "payment_id";    //주문ID

    private List<StorePaymentCancelAvailList> mList = new ArrayList<>();
    private Context mContext;
    private TextView tvHowCharge, tvPaymentDate, tvPaymentAmount;
    private ActivityCancelPayment.canclePayment mLisner;

    public AdapterPaymentCancle(Context context, ActivityCancelPayment.canclePayment lisner) {
        this.mContext = context;
        this.mLisner = lisner;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_adapter_payment_cancel, parent, false);
        }

        tvHowCharge = (TextView) v.findViewById(R.id.tv_how_charge);
        tvPaymentDate = (TextView) v.findViewById(R.id.tv_payment_date);
        tvPaymentAmount = (TextView) v.findViewById(R.id.tv_payment_amount);
        LinearLayout llDetail = (LinearLayout) v.findViewById(R.id.ll_detail);
        LinearLayout llPaymentCancle = (LinearLayout) v.findViewById(R.id.ll_payment_cancle);

        setTextView(position);
        setBedgesView(v, position);

        llPaymentCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < mList.get(position).getPaymentDetails().size(); i++) {
                    try {
                        if (mList.get(position).getPaymentDetails().get(i).getPaymentType() == 1 && mList.get(position).getPaymentDetails().get(i + 1).getPaymentType() == 2) {
                            mLisner.canclePayCard(mList.get(position).getPaymentID(), mList.get(position).getPaymentDetails().get(i + 1).getTID(), mList.get(position).getReceiptID(), mList.get(position).getPaymentDetails().get(0).getPayYMD(), mList.get(position).getPaymentPrice());
                            return;
                        }
                    } catch (IndexOutOfBoundsException err) {
                        err.printStackTrace();
                    }
                    if (mList.get(position).getPaymentDetails().get(i).getPaymentType() == 1) {         //현금 취소
                        mLisner.cancleId(mList.get(position).getPaymentID(), 1, mList.get(position).getReceiptID(), mList.get(position).getPaymentDetails().get(i).getPayYMD(), mList.get(position).getPaymentPrice());
                        Logger.d(TAG, "현금");
                    } else if (mList.get(position).getPaymentDetails().get(i).getPaymentType() == 2) { //카드 취소
                        mLisner.cancleId(mList.get(position).getPaymentID(), 2, mList.get(position).getPaymentDetails().get(i).getTID(), mList.get(position).getPaymentDetails().get(i).getPayYMD(), mList.get(position).getPaymentPrice());
                        Logger.d(TAG, "카드");
                    }
                }

            }
        });

        llDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityDetailsPaymentInfoPreFilled.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(PAYMENY_ID, mList.get(position).getPaymentID());
                mContext.startActivity(intent);
            }
        });
        return v;
    }

    /**
     * 태그 정보 리스트 출력 Setting View
     *
     * @param position
     */
    private void setBedgesView(View v, int position) {
        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.rv_tag);

        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(6, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        AdapterCommonTag adapterCommonTag = new AdapterCommonTag(mContext, mOnAdapterListner);

        adapterCommonTag.addItem(mList.get(position).getBadges());
        recyclerView.setAdapter(adapterCommonTag);
    }

    /**
     * Text Data 바인딩
     *
     * @param position
     */
    private void setTextView(int position) {
        try {
            if (mList.get(position).getPaymentDivide() == 1) {
                tvHowCharge.setText("품목결제");
                tvHowCharge.setTextColor(mContext.getResources().getColor(R.color.color_ff2500));
            } else {
                tvHowCharge.setText("선충전금 충전");
                tvHowCharge.setTextColor(mContext.getResources().getColor(R.color.color_ffa800));
            }

            tvPaymentDate.setText(DataFormatUtil.setDatePaymentFormat2(mList.get(position).getPaymentDate())
                    + " (" + DataFormatUtil.getDayWeek(DataFormatUtil.setDateWeekFormat(mList.get(position).getPaymentDate())) + ") "
                    + DataFormatUtil.setDatTimeFormat(mList.get(position).getPaymentDate()));
            tvPaymentAmount.setText(DataFormatUtil.moneyFormatToWon(mList.get(position).getPaymentPrice()));

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void addItem(List<StorePaymentCancelAvailList> item) {
        mList = item;
    }

    private AdapterCommonTag.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterCommonTag.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
}
