package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetVisitOrderList;

/**
 * 품목 접수완료 Adapter
 */
public class AdapterDeliveryCompleted extends BaseAdapter {
    private static final String TAG = "AdapterDeliveryCompleted";

    private Context mContext;
    private List<GetVisitOrderList> mVisitOrderLists;

    public AdapterDeliveryCompleted(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mVisitOrderLists.size();
    }

    @Override
    public Object getItem(int position) {
        return mVisitOrderLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_payment_delivery, parent, false);

        }
        return view;
    }

    public void addItem(List<GetVisitOrderList> visitOrderLists) {
        mVisitOrderLists = visitOrderLists;
    }
}
