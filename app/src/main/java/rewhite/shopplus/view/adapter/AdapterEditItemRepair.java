package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.GetOrderItemList;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 품목수정 -> 수선 AdapterView
 */
public class AdapterEditItemRepair extends BaseAdapter {
    private static final String TAG = "AdapterEditItemRepair";

    private Context mContext;

    private List<GetOrderItemList.OrderItemOption> mOrderItemOption;

    public AdapterEditItemRepair(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mOrderItemOption.size();
    }

    @Override
    public Object getItem(int position) {
        return mOrderItemOption.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        GetOrderItemList.OrderItemOption orderItemOption = mOrderItemOption.get(position);
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_adapter_repair, parent, false);

            TextView tvTitleName = (TextView) v.findViewById(R.id.tv_title_name);
            TextView tvAmount = (TextView) v.findViewById(R.id.tv_amount);

            tvTitleName.setText(orderItemOption.getOptionItemName());
            tvAmount.setText(DataFormatUtil.moneyFormatToWon(orderItemOption.getOptionPrice()) + "원");
        }

        return v;
    }

    public void addItem(List<GetOrderItemList.OrderItemOption> orderItemOptions) {
        mOrderItemOption = orderItemOptions;
    }
}
