package rewhite.shopplus.view.adapter.enviroment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.view.adapter.AdapterComponent;

/**
 * 평일 영업 시작시간
 */
public class AdapterWeekdayStart extends RecyclerView.Adapter {
    private static final String TAG = "AdapterWeekdayStart";
    private Context mContext;

    private int selectedPosition = -1;
    private AdapterComponent.CCViewHolder.OnAdapterListner mLisner;
    private static ArrayList<String> mItemList;

    public AdapterWeekdayStart(Context context, AdapterComponent.CCViewHolder.OnAdapterListner lisner) {
        this.mContext = context;
        this.mLisner = lisner;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_opening_hours_time, parent, false);
        RecyclerView.ViewHolder holder = new AdapterComponent.CCViewHolder(v, mLisner);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        TextView tvCheckDate = (TextView) holder.itemView.findViewById(R.id.tv_item);
        tvCheckDate.setText(mItemList.get(position));

        setSelectedPosition(position, tvCheckDate);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
            }
        });
    }

    /**
     * 아이템 클릭 시 화면 변경 처리
     *
     * @param position
     * @param tvCheckData
     */
    private void setSelectedPosition(int position, TextView tvCheckData) {
        String from = DataFormatUtil.getCloseDateFormatMonning(position);
        if (selectedPosition == position) {
            if(from.equals("") || from.length() == 0 || TextUtils.isEmpty(from)){
                from = "09:00";
            }

            SettingLogic.setBusinessHoursSundayMorning(mContext, from);
            tvCheckData.setBackgroundResource(R.drawable.a_button_type_2);
            tvCheckData.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else {
            tvCheckData.setBackgroundResource(R.drawable.a_button_type_3);
            tvCheckData.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
        }
    }

    public static void addItem(ArrayList<String> itemList) {
        mItemList = itemList;
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterComponent.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterComponent.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
