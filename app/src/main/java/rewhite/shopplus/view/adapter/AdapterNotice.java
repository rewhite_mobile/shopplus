package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import rewhite.shopplus.R;

public class AdapterNotice extends BaseAdapter {
    private static final String TAG = "AdapterNotice";

    private Context mContext;
    private ArrayList<String> mItemList;

    public AdapterNotice(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return mItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_adapter_notice, parent, false);
        }
        return v;
    }


    public void ItemAdd(ArrayList<String> item){
        mItemList = item;
    }
}
