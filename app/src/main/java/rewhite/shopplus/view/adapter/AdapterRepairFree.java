package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.data.manager.ManagerItemAdditionalData;
import rewhite.shopplus.data.manager.ManagerRepairFree;
import rewhite.shopplus.data.dto.ItemAdditionalData;
import rewhite.shopplus.fragment.sub.FragmentRepairFee;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 수선요금 RecyclerView List
 */
public class AdapterRepairFree extends RecyclerView.Adapter {
    private static final String TAG = "AdapterRepairFree";
    private Context mContext;

    private int selectedPosition = -1;
    private List<StoreItemList> mLaundryCategoryList;

    private AdapterComponent.CCViewHolder.OnAdapterListner mLisner;
    private FragmentRepairFee.onCompletItem mListner;
    private String mType;

    private int mPosition;

    public AdapterRepairFree(Context context, FragmentRepairFee.onCompletItem listner, AdapterComponent.CCViewHolder.OnAdapterListner lisner, int position, String type) {
        this.mContext = context;
        this.mListner = listner;
        this.mLisner = lisner;
        this.mPosition = position;
        this.mType = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adapter_repair_free, parent, false);
        RecyclerView.ViewHolder holder = new AdapterComponent.CCViewHolder(v, mLisner);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        StoreItemList storeItemList = mLaundryCategoryList.get(position);

        CheckBox checkBoxBg = (CheckBox) holder.itemView.findViewById(R.id.checkbox_bg);
        final TextView tvItemRegistration = (TextView) holder.itemView.findViewById(R.id.tv_item_registration);
        final TextView tvWon = (TextView) holder.itemView.findViewById(R.id.tv_won);

        if (mType.equals("N")) {
            viewSelecitionPrevious(storeItemList.getItemName(), checkBoxBg, tvItemRegistration, tvWon);
        } else {
            viewSelecition(storeItemList.getItemName(), checkBoxBg, tvItemRegistration, tvWon);
        }

        tvItemRegistration.setText(storeItemList.getItemName());
        tvWon.setText(DataFormatUtil.moneyFormatToWon(storeItemList.getItemVisitPrice()));
        checkBoxBg.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    mListner.setSelectItem(mLaundryCategoryList.get(position).getItemName(), String.valueOf(mLaundryCategoryList.get(position).getItemVisitPrice()), mLaundryCategoryList.get(position).getStoreItemID());
                } else {
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.color_43425c));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.color_0fc5dc));

                    mListner.setUnSelectItem(mLaundryCategoryList.get(position).getItemName(), String.valueOf(mLaundryCategoryList.get(position).getItemVisitPrice()), mLaundryCategoryList.get(position).getStoreItemID());
                }
            }
        });
    }

    /**
     * 이전 선택된 Item View Check
     *
     * @param itemName
     * @param tvItemRegistration
     * @param tvWon
     */
    private void viewSelecition(String itemName, CheckBox checkBoxBg, TextView tvItemRegistration, TextView tvWon) {

        ArrayList<ItemAdditionalData.ItemRepairFreeEdit> itemComponentList = ManagerRepairFree.getInstance().getRepairFreeDates();

        if (itemComponentList.size() != 0) {
            for (ItemAdditionalData.ItemRepairFreeEdit itemRepairFreeEdit : new ArrayList<>(itemComponentList)) {
                if (itemRepairFreeEdit.getTitleName().equals(itemName)) {
                    checkBoxBg.setChecked(true);
                    tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                    tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                }
            }
        }
    }

    /**
     * 이전 선택된 Item View Check
     *
     * @param itemName
     * @param tvItemRegistration
     * @param tvWon
     */
    private void viewSelecitionPrevious(String itemName, CheckBox checkBoxBg, TextView tvItemRegistration, TextView tvWon) {
        try {
            if (ManagerItemAdditionalData.getInstance().getItemAdditionalData().get(mPosition).getOptionTypeRepairFee() != null) {
                ArrayList<ItemAdditionalData.ItemRepairFreeEdit> itemComponentList = ManagerItemAdditionalData.getInstance().getItemAdditionalData().get(mPosition).getOptionTypeRepairFee();
                ArrayList<ItemAdditionalData.ItemRepairFreeEdit> itemComponentList1 = ManagerRepairFree.getInstance().getRepairFreeDates();

                if (itemComponentList.size() != 0) {
                    for (ItemAdditionalData.ItemRepairFreeEdit itemRepairFreeEdit : new ArrayList<>(itemComponentList)) {
                        ItemAdditionalData.ItemRepairFreeEdit itemComponent = new ItemAdditionalData.ItemRepairFreeEdit();

                        if (itemRepairFreeEdit.getTitleName().equals(itemName)) {
                            itemComponent.setTitleName(itemRepairFreeEdit.getTitleName());
                            itemComponent.setOptionItemID(itemRepairFreeEdit.getOptionItemID());
                            itemComponent.setAmount(itemRepairFreeEdit.getAmount());

                            itemComponentList1.add(itemComponent);
                            ManagerRepairFree.getInstance().setRepairFeeDates(itemComponentList1);

                            checkBoxBg.setChecked(true);
                            tvItemRegistration.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                            tvWon.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
                        }
                    }
                }
            }
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mLaundryCategoryList == null ? 0 : mLaundryCategoryList.size();
    }

    public void addItem(List<StoreItemList> laundryCategoryList) {
        mLaundryCategoryList = laundryCategoryList;
    }

    public static class CCViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnFocusChangeListener {
        private AdapterComponent.CCViewHolder.OnAdapterListner listner;

        public CCViewHolder(View view, AdapterComponent.CCViewHolder.OnAdapterListner listner) {
            super(view);

            this.listner = listner;
            view.setOnClickListener(this);
            view.setOnFocusChangeListener(this);
            view.setFocusable(true);
        }

        public interface OnAdapterListner {
            public void onClick(int position);

            public void onFocusChange(View v, boolean hasFocus, int position);
        }

        @Override
        public void onClick(View v) {
            final int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onClick(position);
            }
        }

        /**
         * FocusEvent
         *
         * @param v        Focus된 View를 받는다
         * @param hasFocus Focus된 View가 Focus중인지 아닌지 가졍노다
         */
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            final int position = getAdapterPosition();

            if (position != RecyclerView.NO_POSITION) {
                if (listner != null)
                    listner.onFocusChange(v, hasFocus, position);
            }
        }
    }
}
