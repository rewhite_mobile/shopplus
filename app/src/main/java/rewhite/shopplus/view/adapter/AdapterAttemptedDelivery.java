package rewhite.shopplus.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AdapterAttemptedDelivery extends BaseAdapter {
    private static final String TAG = "AdapterAttemptedDelivery";

    private List<String> mList = new ArrayList<>();
    private Context mContext;
    private TextView tvHowCharge;

    public AdapterAttemptedDelivery(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            v = vi.inflate(R.layout.item_attempted_delivery, parent, false);
        }

        return v;
    }

    public void addItem(List<String> item) {
        mList = item;
    }
}
