package rewhite.shopplus.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.model.CutomerModel;
import rewhite.shopplus.fragment.main.FragmentNotice;
import rewhite.shopplus.util.DataFormatUtil;

/**
 * 카테고리 내용 Adapter
 */
public class AdapterServiceCenterList extends BaseAdapter {
    private static final String TAG = "AdapterServiceCenterList";

    private List<CutomerModel> mItem;
    private int selectedPosition = 0;
    private FragmentNotice.NoticeData mListener;

    public AdapterServiceCenterList(FragmentNotice.NoticeData listener){
        this.mListener = listener;
    }


    @Override
    public int getCount() {
        return mItem.size();
    }

    @Override
    public Object getItem(int position) {
        return mItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service_center_list, parent, false);

        CutomerModel cutomerModel = mItem.get(position);

        LinearLayout llRoot = (LinearLayout) view.findViewById(R.id.ll_root);
        ImageView ivIconNext = (ImageView) view.findViewById(R.id.iv_icon_next);
        TextView tvCategoryName = (TextView) view.findViewById(R.id.tv_category_name);
        TextView tvContentsName = (TextView) view.findViewById(R.id.tv_contents_name);
        TextView tvHistoryDate = (TextView) view.findViewById(R.id.tv_history_date);

        tvCategoryName.setText("공지");
        tvContentsName.setText(cutomerModel.getNoticeContent());
        try {
            tvHistoryDate.setText(DataFormatUtil.setDateMothFormat(cutomerModel.getRegDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (position == 0) {
            llRoot.setBackgroundResource(R.drawable.a_box_round_custom_type_3);
        }
        setSelectedPosition(position, llRoot, ivIconNext);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                notifyDataSetChanged();
            }
        });
        return view;
    }

    /**
     * 이미지 컬러 변경 하기위해서 사용하는 메소드
     *
     * @param position 해당 view position
     */
    private void setSelectedPosition(int position, LinearLayout llRoot, ImageView ivIconNext) {
        if (selectedPosition == position) {
            mListener.webViewUrl(mItem.get(position).getWebViewUrl());

            llRoot.setBackgroundResource(R.drawable.a_box_round_custom_type_3);
            ivIconNext.setBackgroundResource(R.drawable.ico_customerservice_list_01);
        } else {
            ivIconNext.setBackgroundResource(R.drawable.ico_customerservice_listchoice_00);
            llRoot.setBackgroundResource(R.drawable.a_box_round_custom_type_2);
        }
    }

    public void addItem(List<CutomerModel> item) {
        mItem = item;
    }
}