package rewhite.shopplus.util;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;

public class TextViewAutoSize extends LinearLayout {
    private static final String TAG = "TextViewAutoSize";

    public TextViewAutoSize(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public TextViewAutoSize(Context context) {
        super(context);
    }


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        final int oldWidth = getMeasuredWidth() - getPaddingBottom() - getPaddingTop();
        final int oldHeight = getMeasuredHeight() - getPaddingLeft() - getPaddingRight();

        TextView textView = (TextView) getChildAt(0);
        float size = getResources().getDimensionPixelSize(R.dimen.solutions_view_max_font_size);

        for (int textViewHeight = Integer.MAX_VALUE; textViewHeight > oldHeight; size -= 0.1f) {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
            textView.measure(MeasureSpec.makeMeasureSpec(oldWidth, MeasureSpec.EXACTLY), MeasureSpec.UNSPECIFIED);
            textViewHeight = textView.getMeasuredHeight();
        }
    }
}