package rewhite.shopplus.util;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileFilter;
import java.util.List;
import java.util.regex.Pattern;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ShopPlusApplication;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.data.type.NetworkStatus;

import static rewhite.shopplus.activity.ShopPlusApplication.getCurrentActivity;

/**
 * 공통으로 사용되는 Util 클래스
 */
public class CommonUtil {

    private static final String TAG = "CommonUtil";

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (context.getPackageName().equals(service.service.getPackageName())
                    && serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * 현재 실행중인 Activity 갯수를 반환
     *
     * @return
     */
    public static int getRunningActivity(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> info = activityManager.getRunningTasks(Integer.MAX_VALUE);
        String myPackageName = context.getPackageName();
        for (ActivityManager.RunningTaskInfo runningTaskInfo : info) {
            String packageName = runningTaskInfo.topActivity.getPackageName();
            if (myPackageName.equals(packageName)) {
                return runningTaskInfo.numActivities;
            }
        }
        return 0;
    }

    public static int getColorDrawableColor(ColorDrawable cd) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 11) {
            return cd.getColor();
        } else {
            Bitmap bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_4444);
            Canvas canvas = new Canvas(bitmap);
            cd.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            cd.draw(canvas);

            int color = bitmap.getPixel(0, 0);
            bitmap.recycle();

            return color;
        }
    }

    public static int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static Drawable getDrawable(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 21) {
            return context.getResources().getDrawable(id, context.getTheme());
        } else {
            return context.getResources().getDrawable(id);
        }
    }

    public static void setBackground(View view, Drawable drawable) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 16) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    public static int getNumOfCores() {
        int coreCnt = 1;
        if (Build.VERSION.SDK_INT >= 17) {
            coreCnt = Runtime.getRuntime().availableProcessors();
        } else {
            // Use saurabh64's answer
            coreCnt = getNumCoresOldPhones();
        }
        return coreCnt;
    }

    /**
     * Gets the number of cores available in this device, across all processors.
     * Requires: Ability to peruse the filesystem at "/sys/devices/system/cpu"
     *
     * @return The number of cores, or 1 if failed to get result
     */
    private static int getNumCoresOldPhones() {
        //Private Class to display only CPU devices in the directory listing
        class CpuFilter implements FileFilter {
            @Override
            public boolean accept(File pathname) {
                //Check if filename is "cpu", followed by a single digit number
                if (Pattern.matches("cpu[0-9]+", pathname.getName())) {
                    return true;
                }
                return false;
            }
        }

        try {
            //Get directory containing CPU info
            File dir = new File("/sys/devices/system/cpu/");
            //Filter to only list the devices we care about
            File[] files = dir.listFiles(new CpuFilter());
            //Return the number of cores (virtual CPU devices)
            return files.length;
        } catch (Exception e) {
            //Default to return 1 core
            return 1;
        }
    }

    public static boolean chkAppInstalled(Context context, String packageName) {
        boolean appInstalled = false;
        try {
            context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            appInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            appInstalled = false;
        }
        return appInstalled;
    }

    public static boolean isActivityRunning(AppCompatActivity activity) {
        if (activity == null || activity.isFinishing()) {
            return false;
        }
        return true;
    }

    public static void dismissDialog(DialogInterface dialog) {
        if (dialog == null) {
            return;
        }

        try {
            if (dialog instanceof AlertDialog) {
                if (((AlertDialog) dialog).isShowing()) {
                    ((AlertDialog) dialog).dismiss();
                }
                return;
            }

            if (dialog instanceof ProgressDialog) {
                if (((ProgressDialog) dialog).isShowing()) {
                    ((ProgressDialog) dialog).dismiss();
                }
                return;
            }

            if (dialog instanceof Dialog) {
                if (((Dialog) dialog).isShowing()) {
                    ((Dialog) dialog).dismiss();
                }
                return;
            }
        } catch (Exception e) {
            //ignore
            Logger.e(TAG, "dismissDialog err.", e);
        }
    }

    public static void showDialog(DialogInterface dialog) {
        if (dialog == null)
            return;

        try {
            if (dialog instanceof AlertDialog) {
                if (!((AlertDialog) dialog).isShowing()) {
                    ((AlertDialog) dialog).show();
                }
                return;
            }

            if (dialog instanceof ProgressDialog) {
                if (!((ProgressDialog) dialog).isShowing()) {
                    ((ProgressDialog) dialog).show();
                }
                return;
            }

            if (dialog instanceof Dialog) {
                if (!((Dialog) dialog).isShowing()) {
                    ((Dialog) dialog).show();
                }
                return;
            }
        } catch (Exception e) {
            //ignore
            Logger.e(TAG, "showDialog err.", e);
        }
    }

    /**
     * 동적으로 리스트뷰 높이를 조절하기 위해서 사용.
     *
     * @param listView
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            //listItem.measure(0, 0);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();

        params.height = totalHeight;
        listView.setLayoutParams(params);

        listView.requestLayout();
    }

    /**
     * Color Select 에 맞춰 색상 변경
     *
     * @param position
     * @param colorView
     */
    public static void setColorView(int position, View colorView) {
        if (position == 0) {
            colorView.setBackgroundResource(R.color.color_f5d228);
        } else if (position == 1) {
            colorView.setBackgroundResource(R.color.color_70bf41);
        } else if (position == 2) {
            colorView.setBackgroundResource(R.color.color_f38f18);
        } else if (position == 3) {
            colorView.setBackgroundResource(R.color.color_b36ae2);
        } else if (position == 4) {
            colorView.setBackgroundResource(R.color.color_51a7f9);
        } else if (position == 5) {
            colorView.setBackgroundResource(R.color.color_00882b);
        } else if (position == 6) {
            colorView.setBackgroundResource(R.color.color_de6a10);
        } else if (position == 7) {
            colorView.setBackgroundResource(R.color.color_c82506);
        } else if (position == 8) {
            colorView.setBackgroundResource(R.color.color_0364c0);
        } else if (position == 9) {
            colorView.setBackgroundResource(R.color.color_0a5d18);
        } else if (position == 10) {
            colorView.setBackgroundResource(R.color.color_bd5b0c);
        } else if (position == 11) {
            colorView.setBackgroundResource(R.color.color_861001);
        } else if (position == 12) {
            colorView.setBackgroundResource(R.color.color_002452);
        } else if (position == 13) {
            colorView.setBackgroundResource(R.color.color_a6aaa9);
        } else if (position == 14) {
            colorView.setBackgroundResource(R.color.color_570706);
        } else if (position == 15) {
            colorView.setBackgroundResource(R.color.color_000000);
        } else if (position == 16) {
            colorView.setBackgroundResource(R.drawable.img_ltemdetails_color_pattern_00);
        } else if (position == 17) {
            colorView.setBackgroundResource(R.drawable.img_ltemdetails_color_pattern_01);
        } else if (position == 18) {
            colorView.setBackgroundResource(R.drawable.img_ltemdetails_color_pattern_02);
        }
    }

    /**
     * 택 텍스트 컬러
     *
     * @param mContext
     * @param position
     * @param background
     * @param colorName
     */
    public static void setTacColorView(Context mContext, int position, View background, TextView colorName) {
        if (position == 0) {
            background.setBackgroundResource(R.color.Wite_color);
            colorName.setText("흰색");
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_666666));
        } else if (position == 1) {
            background.setBackgroundResource(R.color.color_a220a1);
            colorName.setText("보라");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 2) {
            background.setBackgroundResource(R.color.color_dd7bde);
            colorName.setText("연보라");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 3) {
            background.setBackgroundResource(R.color.color_fbdff8);
            colorName.setText("연분홍");
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_666666));
        } else if (position == 4) {
            background.setBackgroundResource(R.color.color_faed01);
            colorName.setText("노랑");
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_666666));
        } else if (position == 5) {
            background.setBackgroundResource(R.color.color_f5c90f);
            colorName.setText("진노랑");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 6) {
            background.setBackgroundResource(R.color.color_faeab9);
            colorName.setText("살색");
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_666666));
        } else if (position == 7) {
            background.setBackgroundResource(R.color.color_f2b1ab);
            colorName.setText("연분홍");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 8) {
            background.setBackgroundResource(R.color.color_90ef56);
            colorName.setText("연두");
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_666666));
        } else if (position == 9) {
            background.setBackgroundResource(R.color.color_a2fcbf);
            colorName.setText("옥색");
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_666666));
        } else if (position == 10) {
            background.setBackgroundResource(R.color.color_9fdbe6);
            colorName.setText("하늘색");
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_666666));
        } else if (position == 11) {
            background.setBackgroundResource(R.color.color_44b1ac);
            colorName.setText("청색");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 12) {
            background.setBackgroundResource(R.color.color_6d7833);
            colorName.setText("카키");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 13) {
            background.setBackgroundResource(R.color.color_c4d145);
            colorName.setText("연카키");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 14) {
            background.setBackgroundResource(R.color.color_f26228);
            colorName.setText("주홍");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 15) {
            background.setBackgroundResource(R.color.color_f7a432);
            colorName.setText("감색");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 16) {
            background.setBackgroundResource(R.color.color_a7aca6);
            colorName.setText("회색");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 17) {
            background.setBackgroundResource(R.color.color_c92229);
            colorName.setText("빨강");
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        }
    }

    /**
     * 택 번호 텍스트 color
     *
     * @param mContext
     * @param position
     * @param colorName
     */
    public static void setTacTextColor(Context mContext, int position, TextView colorName) {
        if (position == 0) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.Wite_color));
        } else if (position == 1) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_a220a1));
        } else if (position == 2) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_dd7bde));
        } else if (position == 3) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_fbdff8));
        } else if (position == 4) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_faed01));
        } else if (position == 5) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_f5c90f));
        } else if (position == 6) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_faeab9));
        } else if (position == 7) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_f2b1ab));
        } else if (position == 8) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_90ef56));
        } else if (position == 9) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_a2fcbf));
        } else if (position == 10) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_9fdbe6));
        } else if (position == 11) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_44b1ac));
        } else if (position == 12) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_6d7833));
        } else if (position == 13) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_c4d145));
        } else if (position == 14) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_f26228));
        } else if (position == 15) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_f7a432));
        } else if (position == 16) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_a7aca6));
        } else if (position == 17) {
            colorName.setTextColor(mContext.getResources().getColor(R.color.color_c92229));
        }
    }

    /**
     * 택 번호 텍스트 color
     *
     * @param position
     * @param imgColor
     */
    public static void setTacImgColor(int position, ImageView imgColor) {
        if (position == 0) {
            imgColor.setBackgroundResource(R.color.Wite_color);
        } else if (position == 1) {
            imgColor.setBackgroundResource(R.color.color_a220a1);
        } else if (position == 2) {
            imgColor.setBackgroundResource(R.color.color_dd7bde);
        } else if (position == 3) {
            imgColor.setBackgroundResource(R.color.color_fbdff8);
        } else if (position == 4) {
            imgColor.setBackgroundResource(R.color.color_faed01);
        } else if (position == 5) {
            imgColor.setBackgroundResource(R.color.color_f5c90f);
        } else if (position == 6) {
            imgColor.setBackgroundResource(R.color.color_faeab9);
        } else if (position == 7) {
            imgColor.setBackgroundResource(R.color.color_f2b1ab);
        } else if (position == 8) {
            imgColor.setBackgroundResource(R.color.color_90ef56);
        } else if (position == 9) {
            imgColor.setBackgroundResource(R.color.color_a2fcbf);
        } else if (position == 10) {
            imgColor.setBackgroundResource(R.color.color_9fdbe6);
        } else if (position == 11) {
            imgColor.setBackgroundResource(R.color.color_44b1ac);
        } else if (position == 12) {
            imgColor.setBackgroundResource(R.color.color_6d7833);
        } else if (position == 13) {
            imgColor.setBackgroundResource(R.color.color_c4d145);
        } else if (position == 14) {
            imgColor.setBackgroundResource(R.color.color_f26228);
        } else if (position == 15) {
            imgColor.setBackgroundResource(R.color.color_f7a432);
        } else if (position == 16) {
            imgColor.setBackgroundResource(R.color.color_a7aca6);
        } else if (position == 17) {
            imgColor.setBackgroundResource(R.color.color_c92229);
        }
    }

    /**
     * 회원정보 상세 TAG 색상
     */
    public static void getTacColor(Context mContext, int position, ImageView imgTagColor) {
        if (position == 0) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_00);
        } else if (position == 1) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_01);
        } else if (position == 2) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_2_1);
        } else if (position == 3) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_02);
        } else if (position == 4) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_03);
        } else if (position == 5) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_04);
        } else if (position == 6) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_05);
        } else if (position == 7) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_06);
        } else if (position == 8) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_07);
        } else if (position == 9) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_08);
        } else if (position == 10) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_09);
        } else if (position == 11) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_10);
        } else if (position == 12) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_11);
        } else if (position == 13) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_12);
        } else if (position == 14) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_13);
        } else if (position == 15) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_14);
        } else if (position == 16) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_15);
        } else if (position == 17) {
            imgTagColor.setBackgroundResource(R.drawable.ico_ltemdetails_color_16);
        }
    }

    /**
     * 택번호 서버저장 값 일제택
     *
     * @param tagNumber
     * @return
     */
    public static String setTagFormaterChangeResponse(String tagNumber) {
        String mTagNumber = tagNumber.substring(1, tagNumber.length());
        String str2 = tagNumber.substring(0, 1);

        String pre = null;
        switch (str2) {
            case "ⓞ":
                pre = "|0";
                break;
            case "①":
                pre = "|1";
                break;
            case "②":
                pre = "|2";
                break;
            case "③":
                pre = "|3";
                break;
            case "④":
                pre = "|4";
                break;
            case "⑤":
                pre = "|5";
                break;
            case "⑥":
                pre = "|6";
                break;
            case "⑦":
                pre = "|7";
                break;
            case "⑧":
                pre = "|8";
                break;
            case "⑨":
                pre = "|9";
                break;
            case "⑩":
                pre = "|10";
                break;
        }
        return pre + mTagNumber;
    }

    /**
     * 택번호 Format 일제택
     *
     * @param tagNumber
     * @return
     */
    public static String setTagFormater(String tagNumber) {
        String str2 = null;
        String str = null;
        //택번호 전부 사용시 초기화
        if (tagNumber.equals("10|000")) {
            tagNumber = "①001";
            return tagNumber;
        }

        int idx = tagNumber.indexOf("|");

        if (tagNumber.length() == 5) {
            str = tagNumber.substring(idx + 2);
            str2 = tagNumber.substring(1, 2);
        } else if (tagNumber.length() == 6) {
            str = tagNumber.substring(idx + 3);
            str2 = tagNumber.substring(1, 3);
        }

        String pre = null;
        switch (str2) {
            case "0":
                pre = "ⓞ";
                break;
            case "1":
                pre = "①";
                break;
            case "2":
                pre = "②";
                break;
            case "3":
                pre = "③";
                break;
            case "4":
                pre = "④";
                break;
            case "5":
                pre = "⑤";
                break;
            case "6":
                pre = "⑥";
                break;
            case "7":
                pre = "⑦";
                break;
            case "8":
                pre = "⑧";
                break;
            case "9":
                pre = "⑨";
                break;
            case "10":
                pre = "⑩";
                break;

        }
        return pre + str;
    }

    /**
     * 택번호 자동 증가 일제택
     *
     * @param tagNumber
     * @return
     */
    public static String setTagFormaterChange(String tagNumber) {
        String str2 = null;
        String str = null;
        //택번호 전부 사용시 초기화
        if (tagNumber.equals("|10000")) {
            tagNumber = "①001";
            return tagNumber;
        }
        int idx = tagNumber.indexOf("|");

        if (tagNumber.length() == 5) {
            str = tagNumber.substring(idx + 2);
            str2 = tagNumber.substring(1, 2);

        } else if (tagNumber.length() == 6) {
            str = tagNumber.substring(idx + 3);
            str2 = tagNumber.substring(1, 3);
        }


        int index = Integer.valueOf(str);
        int tagNumberIndex = index + 1;

        //종료번호까지 사용했을 경우 초기화
        if (tagNumberIndex == 1000) {
            tagNumberIndex = 000;
        }

        //종료번호까지 사용했을 경우 카운트 증가
        if (tagNumberIndex == 000) {
            int tagNUmber = Integer.valueOf(str2) + 1;
            str2 = String.valueOf(tagNUmber);
        }

        String pre = null;
        switch (str2) {
            case "0":
                pre = "ⓞ";
                break;
            case "1":
                pre = "①";
                break;
            case "2":
                pre = "②";
                break;
            case "3":
                pre = "③";
                break;
            case "4":
                pre = "④";
                break;
            case "5":
                pre = "⑤";
                break;
            case "6":
                pre = "⑥";
                break;
            case "7":
                pre = "⑦";
                break;
            case "8":
                pre = "⑧";
                break;
            case "9":
                pre = "⑨";
                break;
            case "10":
                pre = "⑩";
                break;

        }
        return pre + String.format("%03d", tagNumberIndex);
    }

    /**
     * 택번호 일제택 바코드
     *
     * @param tagNumber
     * @return
     */
    public static String setFormater(String tagNumber) {
        //택번호 전부 사용시 초기화
        String pre = null;
        String startTagNumber = null;
        if (tagNumber.equals("9999|000")) {
            tagNumber = "1①001";
            return tagNumber;
        }
        if (tagNumber.length() == 6) {
            startTagNumber = tagNumber.substring(0, 1);

            int idx = tagNumber.indexOf("|");
            String str = tagNumber.substring(idx + 1);
            String str2 = tagNumber.substring(1, idx);

            switch (str2) {
                case "0":
                    pre = "ⓞ";
                    break;
                case "1":
                    pre = "①";
                    break;
                case "2":
                    pre = "②";
                    break;
                case "3":
                    pre = "③";
                    break;
                case "4":
                    pre = "④";
                    break;
                case "5":
                    pre = "⑤";
                    break;
                case "6":
                    pre = "⑥";
                    break;
                case "7":
                    pre = "⑦";
                    break;
                case "8":
                    pre = "⑧";
                    break;
                case "9":
                    pre = "⑨";
                    break;
            }
            return startTagNumber + pre + str;
        } else if (tagNumber.length() == 7) {
            startTagNumber = tagNumber.substring(0, 2);

            int idx = tagNumber.indexOf("|");
            String str = tagNumber.substring(idx + 1);
            String str2 = tagNumber.substring(2, idx);

            switch (str2) {
                case "0":
                    pre = "ⓞ";
                    break;
                case "1":
                    pre = "①";
                    break;
                case "2":
                    pre = "②";
                    break;
                case "3":
                    pre = "③";
                    break;
                case "4":
                    pre = "④";
                    break;
                case "5":
                    pre = "⑤";
                    break;
                case "6":
                    pre = "⑥";
                    break;
                case "7":
                    pre = "⑦";
                    break;
                case "8":
                    pre = "⑧";
                    break;
                case "9":
                    pre = "⑨";
                    break;
            }
            return startTagNumber + pre + str;
        } else if (tagNumber.length() == 8) {
            startTagNumber = tagNumber.substring(0, 3);

            int idx = tagNumber.indexOf("|");
            String str = tagNumber.substring(idx + 1);
            String str2 = tagNumber.substring(3, idx);

            switch (str2) {
                case "0":
                    pre = "ⓞ";
                    break;
                case "1":
                    pre = "①";
                    break;
                case "2":
                    pre = "②";
                    break;
                case "3":
                    pre = "③";
                    break;
                case "4":
                    pre = "④";
                    break;
                case "5":
                    pre = "⑤";
                    break;
                case "6":
                    pre = "⑥";
                    break;
                case "7":
                    pre = "⑦";
                    break;
                case "8":
                    pre = "⑧";
                    break;
                case "9":
                    pre = "⑨";
                    break;
            }
            return startTagNumber + pre + str;
        }
        return null;
    }

    /**
     * 택번호 일제택 서버저장 값 바코드
     *
     * @param tagNumber
     * @return
     */
    public static String setTagFormaterChangeTypeResponse(String tagNumber) {
        String pre = null;
        String mTagNumber = null;
        String startTagNumber = null;
        if (tagNumber.length() == 5) {
            startTagNumber = tagNumber.substring(0, 1);
            mTagNumber = tagNumber.substring(2, tagNumber.length());
            String str2 = tagNumber.substring(1, 2);

            switch (str2) {
                case "ⓞ":
                    pre = "0|";
                    break;
                case "①":
                    pre = "1|";
                    break;
                case "②":
                    pre = "2|";
                    break;
                case "③":
                    pre = "3|";
                    break;
                case "④":
                    pre = "4|";
                    break;
                case "⑤":
                    pre = "5|";
                    break;
                case "⑥":
                    pre = "6|";
                    break;
                case "⑦":
                    pre = "7|";
                    break;
                case "⑧":
                    pre = "8|";
                    break;
                case "⑨":
                    pre = "9|";
                    break;
            }
            return startTagNumber + pre + mTagNumber;
        } else if (tagNumber.length() == 6) {
            startTagNumber = tagNumber.substring(0, 2);
            mTagNumber = tagNumber.substring(3, tagNumber.length());
            String str2 = tagNumber.substring(2, 3);

            switch (str2) {
                case "⓪":
                    pre = "0|";
                    break;
                case "①":
                    pre = "1|";
                    break;
                case "②":
                    pre = "2|";
                    break;
                case "③":
                    pre = "3|";
                    break;
                case "④":
                    pre = "4|";
                    break;
                case "⑤":
                    pre = "5|";
                    break;
                case "⑥":
                    pre = "6|";
                    break;
                case "⑦":
                    pre = "7|";
                    break;
                case "⑧":
                    pre = "8|";
                    break;
                case "⑨":
                    pre = "9|";
                    break;
            }
            return startTagNumber + pre + mTagNumber;
        } else if (tagNumber.length() == 7) {
            startTagNumber = tagNumber.substring(0, 3);
            mTagNumber = tagNumber.substring(4, tagNumber.length());
            String str2 = tagNumber.substring(3, 4);

            switch (str2) {
                case "⓪":
                    pre = "0|";
                    break;
                case "①":
                    pre = "1|";
                    break;
                case "②":
                    pre = "2|";
                    break;
                case "③":
                    pre = "3|";
                    break;
                case "④":
                    pre = "4|";
                    break;
                case "⑤":
                    pre = "5|";
                    break;
                case "⑥":
                    pre = "6|";
                    break;
                case "⑦":
                    pre = "7|";
                    break;
                case "⑧":
                    pre = "8|";
                    break;
                case "⑨":
                    pre = "9|";
                    break;
            }
            return startTagNumber + pre + mTagNumber;
        }
        return startTagNumber + pre + mTagNumber;
    }

    /**
     * 택번호 자동 증가 일제택 바코드
     *
     * @param tagNumber
     * @return
     */
    public static String setTagFormaterChangeType(String tagNumber) {
        //택번호 전부 사용시 초기화
        String pre = null;
        String mTagNumber = null;
        String startTagNumber = null;

        Logger.d(TAG, "tagNumber : " + tagNumber);
        if (tagNumber.equals("9999|000")) {
            tagNumber = "1①001";
            return tagNumber;
        }
        if (tagNumber.length() == 6) {
            startTagNumber = tagNumber.substring(0, 1);

            int idx = tagNumber.indexOf("|");
            String str = tagNumber.substring(idx + 1);
            String str2 = tagNumber.substring(1, idx);

            int index = Integer.valueOf(str);
            int tagNumberIndex = index + 1;

            //종료번호까지 사용했을 경우 초기화
            if (tagNumberIndex == 1000) {
                tagNumberIndex = 000;
            }

            //종료번호까지 사용했을 경우 카운트 증가
            if (str.equals("000")) {
                int tagNUmber = Integer.valueOf(str2) + 1;
                if (tagNUmber == 10) {
                    int startNumber = Integer.valueOf(startTagNumber) + 1;
                    tagNUmber = 0;
                    startTagNumber = String.valueOf(startNumber);
                }
                str2 = String.valueOf(tagNUmber);
            }

            switch (str2) {
                case "0":
                    pre = "⓪";
                    break;
                case "1":
                    pre = "①";
                    break;
                case "2":
                    pre = "②";
                    break;
                case "3":
                    pre = "③";
                    break;
                case "4":
                    pre = "④";
                    break;
                case "5":
                    pre = "⑤";
                    break;
                case "6":
                    pre = "⑥";
                    break;
                case "7":
                    pre = "⑦";
                    break;
                case "8":
                    pre = "⑧";
                    break;
                case "9":
                    pre = "⑨";
                    break;
            }
            return startTagNumber + pre + String.format("%03d", tagNumberIndex);
        } else if (tagNumber.length() == 7) {
            startTagNumber = tagNumber.substring(0, 2);

            int idx = tagNumber.indexOf("|");
            String str = tagNumber.substring(idx + 1);
            String str2 = tagNumber.substring(2, idx);

            int index = Integer.valueOf(str);
            int tagNumberIndex = index + 1;

            //종료번호까지 사용했을 경우 초기화
            if (tagNumberIndex == 1000) {
                tagNumberIndex = 000;
            }

            //종료번호까지 사용했을 경우 카운트 증가
            if (str.equals("000")) {
                int tagNUmber = Integer.valueOf(str2) + 1;
                if (tagNUmber == 10) {
                    int startNumber = Integer.valueOf(startTagNumber) + 1;
                    tagNUmber = 0;
                    startTagNumber = String.valueOf(startNumber);
                }
                str2 = String.valueOf(tagNUmber);
            }

            switch (str2) {
                case "0":
                    pre = "⓪";
                    break;
                case "1":
                    pre = "①";
                    break;
                case "2":
                    pre = "②";
                    break;
                case "3":
                    pre = "③";
                    break;
                case "4":
                    pre = "④";
                    break;
                case "5":
                    pre = "⑤";
                    break;
                case "6":
                    pre = "⑥";
                    break;
                case "7":
                    pre = "⑦";
                    break;
                case "8":
                    pre = "⑧";
                    break;
                case "9":
                    pre = "⑨";
                    break;
            }
            return startTagNumber + pre + String.format("%03d", tagNumberIndex);
        } else if (tagNumber.length() == 8) {
            startTagNumber = tagNumber.substring(0, 3);

            int idx = tagNumber.indexOf("|");
            String str = tagNumber.substring(idx + 1);
            String str2 = tagNumber.substring(3, idx);

            int index = Integer.valueOf(str);
            int tagNumberIndex = index + 1;

            //종료번호까지 사용했을 경우 초기화
            if (tagNumberIndex == 1000) {
                tagNumberIndex = 000;
            }

            //종료번호까지 사용했을 경우 카운트 증가
            if (str.equals("000")) {
                int tagNUmber = Integer.valueOf(str2) + 1;
                if (tagNUmber == 10) {
                    int startNumber = Integer.valueOf(startTagNumber) + 1;
                    tagNUmber = 0;
                    startTagNumber = String.valueOf(startNumber);
                }
                str2 = String.valueOf(tagNUmber);
            }

            switch (str2) {
                case "0":
                    pre = "⓪";
                    break;
                case "1":
                    pre = "①";
                    break;
                case "2":
                    pre = "②";
                    break;
                case "3":
                    pre = "③";
                    break;
                case "4":
                    pre = "④";
                    break;
                case "5":
                    pre = "⑤";
                    break;
                case "6":
                    pre = "⑥";
                    break;
                case "7":
                    pre = "⑦";
                    break;
                case "8":
                    pre = "⑧";
                    break;
                case "9":
                    pre = "⑨";
                    break;
            }
            return startTagNumber + pre + String.format("%03d", tagNumberIndex);
        }
        return null;
    }

    /**
     * 기타택번호 서버저장 값 일제택
     *
     * @param tagNumber
     * @return
     */
    public static String setEtcTagFormaterChangeResponse(String tagNumber) {
        String startTagNumber = null;
        String str2 = null;
        int tagNumberIndex = 0;

        if (tagNumber.equals("9999-9999")) {
            tagNumber = "0000-0001";
            return tagNumber;
        }

        if (tagNumber.length() == 6) {
            startTagNumber = tagNumber.substring(0, 1);

            int idx = tagNumber.indexOf("-");
            String str = tagNumber.substring(idx + 1);
            str2 = tagNumber.substring(1, idx);

            int index = Integer.valueOf(str);
            tagNumberIndex = index + 1;

            //종료번호까지 사용했을 경우 초기화
            if (tagNumberIndex == 10000) {
                tagNumberIndex = 0000;
            }

            //종료번호까지 사용했을 경우 카운트 증가
            if (tagNumberIndex == 0000) {
                int tagNUmber = Integer.valueOf(str2) + 1;
                if (tagNUmber == 10) {
                    int startNumber = Integer.valueOf(startTagNumber) + 1;
                    startTagNumber = String.valueOf(startNumber);
                }
            }
            return startTagNumber + "-" + String.format("%04d", tagNumberIndex);
        } else if (tagNumber.length() == 7) {
            startTagNumber = tagNumber.substring(0, 2);

            int idx = tagNumber.indexOf("-");
            String str = tagNumber.substring(idx + 1);
            str2 = tagNumber.substring(2, idx);

            int index = Integer.valueOf(str);
            tagNumberIndex = index + 1;

            //종료번호까지 사용했을 경우 초기화
            if (tagNumberIndex == 10000) {
                tagNumberIndex = 0000;
            }

            //종료번호까지 사용했을 경우 카운트 증가
            if (tagNumberIndex == 0000) {
                int tagNUmber = Integer.valueOf(str2) + 1;
                if (tagNUmber == 10) {
                    int startNumber = Integer.valueOf(startTagNumber) + 1;
                    startTagNumber = String.valueOf(startNumber);
                }
            }
            return startTagNumber + "-" + String.format("%04d", tagNumberIndex);
        } else if (tagNumber.length() == 8) {
            startTagNumber = tagNumber.substring(0, 3);
            int idx = tagNumber.indexOf("-");
            String str = tagNumber.substring(idx + 1);
            str2 = tagNumber.substring(3, idx);

            int index = Integer.valueOf(str);
            tagNumberIndex = index + 1;

            //종료번호까지 사용했을 경우 초기화
            if (tagNumberIndex == 10000) {
                tagNumberIndex = 0000;
            }

            //종료번호까지 사용했을 경우 카운트 증가
            if (tagNumberIndex == 0000) {
                int tagNUmber = Integer.valueOf(str2) + 1;
                if (tagNUmber == 10) {
                    int startNumber = Integer.valueOf(startTagNumber) + 1;
                    startTagNumber = String.valueOf(startNumber);
                }
            }
            return startTagNumber + "-" + String.format("%04d", tagNumberIndex);
        } else if (tagNumber.length() == 9) {
            startTagNumber = tagNumber.substring(0, 4);

            int idx = tagNumber.indexOf("-");
            String str = tagNumber.substring(idx + 1);
            str2 = tagNumber.substring(4, idx);

            int index = Integer.valueOf(str);
            tagNumberIndex = index + 1;

            //종료번호까지 사용했을 경우 초기화
            if (tagNumberIndex == 10000) {
                tagNumberIndex = 0000;
            }

            //종료번호까지 사용했을 경우 카운트 증가
            if (tagNumberIndex == 0000) {
                int tagNUmber = Integer.valueOf(str2) + 1;
                if (tagNUmber == 10) {
                    int startNumber = Integer.valueOf(startTagNumber) + 1;
                    startTagNumber = String.valueOf(startNumber);
                }
            }
            return startTagNumber + "-" + String.format("%04d", tagNumberIndex);
        }
        return null;
    }


    /**
     * 텍번호 String Formmat
     *
     * @param tagno
     * @return
     */
    public static String getSubTagFormmater(String tagno) {
        String pre = null;
        switch (tagno) {
            case "ⓞ":
                pre = "0";
                break;
            case "①":
                pre = "1";
                break;
            case "②":
                pre = "2";
                break;
            case "③":
                pre = "3";
                break;
            case "④":
                pre = "4";
                break;
            case "⑤":
                pre = "5";
                break;
            case "⑥":
                pre = "6";
                break;
            case "⑦":
                pre = "7";
                break;
            case "⑧":
                pre = "8";
                break;
            case "⑨":
                pre = "9";
                break;

            case "①0":
                pre = "⑩";
                break;
        }
        return pre;
    }

    /**
     * 텍번호 String Formmat
     *
     * @param tagno
     * @return
     */
    public static String getTagFormmater(String tagno) {
        String pre = null;
        switch (tagno) {
            case "0":
                pre = "ⓞ";
                break;
            case "1":
                pre = "①";
                break;
            case "2":
                pre = "②";
                break;
            case "3":
                pre = "③";
                break;
            case "4":
                pre = "④";
                break;
            case "5":
                pre = "⑤";
                break;
            case "6":
                pre = "⑥";
                break;
            case "7":
                pre = "⑦";
                break;
            case "8":
                pre = "⑧";
                break;
            case "9":
                pre = "⑨";
                break;

            case "①0":
                pre = "⑩";
                break;
        }
        return pre;
    }

    /**
     * 공용 팝업
     *
     * @param activity
     */
    public static void finishPopup(AppCompatActivity activity) {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
        thirdAlertPopup.setTitle("오류");
        thirdAlertPopup.setContent("핸드폰 뒷자리 4자리를 입력해주세요.");
        thirdAlertPopup.setButtonText("확인");

        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
            @Override
            public void onConfirmClick() {
                activity.finish();
            }
        });
        thirdAlertPopup.show();
    }

    /**
     * 서비스 준비중 공용 팝업
     *
     * @param activity
     */
    public static void servicePreparationPopup(AppCompatActivity activity) {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
        thirdAlertPopup.setTitle("서비스 준비중");
        thirdAlertPopup.setContent("현재 기능은 서비스 준비중입니다.");
        thirdAlertPopup.setButtonText("확인");
        thirdAlertPopup.show();
    }

    /**
     * 서비스 준비중 공용 팝업
     *
     * @param activity
     */
    public static void ItemAddErrPopup(AppCompatActivity activity, String title, String mag) {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
        thirdAlertPopup.setTitle(title);
        thirdAlertPopup.setContent(mag);
        thirdAlertPopup.setButtonText("확인");
        thirdAlertPopup.show();
    }

    /**
     * 입력 필드 데이터 오류 공용 팝업
     *
     * @param activity
     */
    public static void netWorkErrPoup(AppCompatActivity activity) {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
        thirdAlertPopup.setTitle("네트워크 연결 오류");
        thirdAlertPopup.setContent("인터넷 연결을 확인하세요.");
        thirdAlertPopup.setButtonText("확인");

        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
            @Override
            public void onConfirmClick() {
                Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            }
        });
        thirdAlertPopup.show();
    }

    /**
     * 입력 필드 데이터 오류 공용 팝업
     *
     * @param activity
     */
    public static void setfinishPopup(AppCompatActivity activity, String msg, String btnAgree) {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
        thirdAlertPopup.setTitle("오류");
        thirdAlertPopup.setContent(msg);
        thirdAlertPopup.setButtonText(btnAgree);

        thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
            @Override
            public void onConfirmClick() {
                activity.finish();
            }
        });
        thirdAlertPopup.show();
    }


    /**
     * API 호출 Err 팝업
     *
     * @param activity
     */
    public static void loginErrPopup(AppCompatActivity activity, String code) {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
        thirdAlertPopup.setTitle("오류");

        switch (code) {
            case "E0000":
                NetworkStatus networkStatus = ShopPlusApplication.getCurrnetNetworkStatus();
                if (!networkStatus.equals(NetworkStatus.WIFI)) {
                    CommonUtil.netWorkErrPoup(activity);
                }
                getCurrentActivity();
                break;

            case "F0000":
                thirdAlertPopup.setContent("서버 연결 실패하였습니다. \n 잠시후 다시 시도해주세요.");
                thirdAlertPopup.show();
                break;

            case "F1004":
                thirdAlertPopup.setContent("휴면 계정이에요. \n 고객센터로 연락주세요.");
                thirdAlertPopup.show();
                break;

            case "F1003":
                thirdAlertPopup.setContent("탈퇴한 계정입니다..\n 확인이 필요하시면 고객센터로 연락주세요.");
                thirdAlertPopup.show();
                break;

            case "F1001":
                thirdAlertPopup.setContent("존재하지 않는 아이디에요.");
                thirdAlertPopup.show();
                break;

            case "F1002":
                thirdAlertPopup.setContent("비밀번호가 일치하지 않아요.");
                thirdAlertPopup.show();
                break;

            case "F1111":
                thirdAlertPopup.setContent("데이터 유효성 실패했어요. \n 잠시후 다시 시도해주세요.");
                thirdAlertPopup.show();
                break;
        }

    }

    /**
     * Device info list
     */
    @SuppressLint("MissingPermission")
    public static void setDeviceTest(Context context) {
        // device 실제 해상도 정보값
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        Logger.d(TAG, "width : " + width);
        Logger.d(TAG, "height : " + height);
        Logger.d(TAG, "setDeviceTest");
        // device 고유 정보값
        Logger.d(TAG, "BOARD: " + Build.BOARD);
        Logger.d(TAG, "BRAND: " + Build.BRAND);
        Logger.d(TAG, "CPU_ABI: " + Build.CPU_ABI);
        Logger.d(TAG, "DEVICE: " + Build.DEVICE);
        Logger.d(TAG, "DISPLAY: " + Build.DISPLAY);
        Logger.d(TAG, "FINGERPRINT: " + Build.FINGERPRINT);
        Logger.d(TAG, "HOST: " + Build.HOST);
        Logger.d(TAG, "ID: " + Build.ID);
        Logger.d(TAG, "MANUFACTURER: " + Build.MANUFACTURER);
        Logger.d(TAG, "MODEL: " + Build.MODEL);
        Logger.d(TAG, "PRODUCT: " + Build.PRODUCT);
        Logger.d(TAG, "TAGS: " + Build.TAGS);
        Logger.d(TAG, "TIME: " + Build.TIME);
        Logger.d(TAG, "TYPE: " + Build.TYPE);
        Logger.d(TAG, "USER: " + Build.USER);

        // device 정보 가져오기
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        Logger.d(TAG, "getCallState : " + telephony.getCallState());
        Logger.d(TAG, "getDataActivity : " + telephony.getDataActivity());
        Logger.d(TAG, "getDataState : " + telephony.getDataState());
        Logger.d(TAG, "getDeviceId : " + telephony.getDeviceId());
        Logger.d(TAG, "getDeviceSoftwareVersion : " + telephony.getDeviceSoftwareVersion());
        Logger.d(TAG, "getLine1Number : " + telephony.getLine1Number());
        Logger.d(TAG, "getNetworkCountryIso : " + telephony.getNetworkCountryIso());
        Logger.d(TAG, "getNetworkOperator : " + telephony.getNetworkOperator());
        Logger.d(TAG, "getNetworkOperatorName : " + telephony.getNetworkOperatorName());
        Logger.d(TAG, "getNetworkType : " + telephony.getNetworkType());
        Logger.d(TAG, "getPhoneType : " + telephony.getPhoneType());
        Logger.d(TAG, "getSimCountryIso : " + telephony.getSimCountryIso());
        Logger.d(TAG, "getSubscriberId : " + telephony.getSubscriberId());
        Logger.d(TAG, "getVoiceMailAlphaTag : " + telephony.getVoiceMailAlphaTag());
        Logger.d(TAG, "getVoiceMailNumber : " + telephony.getVoiceMailNumber());
        Logger.d(TAG, "isNetworkRoaming : " + telephony.isNetworkRoaming());
        Logger.d(TAG, "hasIccCard : " + telephony.hasIccCard());
        Logger.d(TAG, "hashCode : " + telephony.hashCode());
        Logger.d(TAG, "toString : " + telephony.toString());

        String idByANDROID_ID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        Logger.d(TAG, "idByANDROID_ID : " + idByANDROID_ID);
        try {
            String idBySerialNumber = (String) Build.class.getField("SERIAL").get(null);
            Logger.d(TAG, "idBySerialNumber : " + idBySerialNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Logger.d(TAG, "idByANDROID_ID : " + idByANDROID_ID);

        Account[] accounts = AccountManager.get(context).getAccounts();
        Account account = null;
        for (int i = 0; i < accounts.length; i++) {
            account = accounts[i];
            Log.d(TAG, "Account - name: " + account.name + ", type :" + account.type);
            if (account.type.equals("com.google")) {     //이러면 구글 계정 구분 가능
            }
        }
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}