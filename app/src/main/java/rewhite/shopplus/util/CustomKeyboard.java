package rewhite.shopplus.util;

import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.EditText;

import java.util.List;

/**
 * 커스텀 키보드 숫자 패드만 가능.
 */
public class CustomKeyboard {
    private static final String TAG = "CustomKeyboard1";
    private KeyboardView mKeyboardView;
    private AppCompatActivity mHostActivity;

    private EditText edittext;
    private OnKeyboardActionListener mOnKeyboardActionListener = new OnKeyboardActionListener() {

        public final static int CodeDelete = -5;
        public final static int CodeCancel = -3;
        public final static int CodePrev = 55000;
        public final static int CodeAllLeft = 55001;
        public final static int CodeLeft = 55002;
        public final static int CodeRight = 55003;
        public final static int CodeAllRight = 55004;
        public final static int CodeNext = 55005;
        public final static int CodeClear = 55006;
        public final static int CodeComa = 99;

        @Override
        public void onKey(int primaryCode, int[] keyCodes) {
            Editable editable = edittext.getText();

            int start = edittext.getSelectionStart();
            if (primaryCode == CodeCancel) {
                edittext.setText("." + edittext.getText().toString());
            } else if (primaryCode == CodeDelete) {
                if (editable != null && start > 0) editable.delete(start - 1, start);
                if (edittext.hasSelection() == true) {
                    edittext.setText(Character.toString((char) primaryCode));
                }
            } else if (primaryCode == CodeClear) {
                if (editable != null) editable.clear();
            } else if (primaryCode == CodeLeft) {
                if (start > 0) edittext.setSelection(start - 1);
            } else if (primaryCode == CodeRight) {
                if (start < edittext.length()) edittext.setSelection(start + 1);
            } else if (primaryCode == CodeAllLeft) {
                edittext.setSelection(0);
            } else if (primaryCode == CodeAllRight) {
                edittext.setSelection(edittext.length());
            } else if (primaryCode == CodePrev) {
            } else if (primaryCode == CodeNext) {

            } else if (primaryCode == CodeComa) {
                if (!edittext.getText().toString().contains(".") || edittext.getText().toString().length() != 0) {
                    edittext.setText(edittext.getText().toString() + ".");
                }
                edittext.setSelection(edittext.length());
            } else { // insert character
                if (edittext.hasSelection() == true) {
                    edittext.setText(Character.toString((char) primaryCode));
                    edittext.setSelection(edittext.length());
                } else {
                    editable.insert(start, Character.toString((char) primaryCode));
                }
            }
        }

        @Override
        public void onPress(int arg0) {
        }

        @Override
        public void onRelease(int primaryCode) {
        }

        @Override
        public void onText(CharSequence text) {
        }

        @Override
        public void swipeDown() {
        }

        @Override
        public void swipeLeft() {
        }

        @Override
        public void swipeRight() {
        }

        @Override
        public void swipeUp() {
        }
    };

    public List<Keyboard.Key> myKeys = null;

    public CustomKeyboard(AppCompatActivity activity, View view, int viewid, int layoutid) {
        mHostActivity = activity;

        mKeyboardView = (KeyboardView) view.findViewById(viewid);
        mKeyboardView.setKeyboard(new Keyboard(view.getContext(), layoutid));
        mKeyboardView.setOnKeyboardActionListener(mOnKeyboardActionListener);

        myKeys = mKeyboardView.getKeyboard().getKeys();
    }

    public void registerEditText(View view, int resid) {
        edittext = (EditText) view.findViewById(resid);

        edittext.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                EditText edittext = (EditText) v;
                int inType = edittext.getInputType();
//                edittext.setInputType(InputType.TYPE_NULL);
//                edittext.onTouchEvent(event);
//                edittext.setInputType(inType);
                edittext.selectAll();

                return true;
            }
        });

        edittext.setInputType(edittext.getInputType() | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }
}