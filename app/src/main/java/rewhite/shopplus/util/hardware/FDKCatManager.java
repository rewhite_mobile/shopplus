package rewhite.shopplus.util.hardware;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.FT_Device;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import me.rewhite.van.library.common.util.MZCrypto;
import me.rewhite.van.library.common.util.UnicodeFormatter;
import rewhite.shopplus.util.Logger;

/**
 * firstData Manager 클래스
 */
public class FDKCatManager extends Service {
    private static final String TAG = "FDKCatManager";
    public static final String INTENT_ACTION = "intent.action.rewhite.van.service";
    protected SocketChannel mChannel;
    protected ReceiveThread mThread;
    public int RECV_BUFFER_SIZE = 1024 * 1024;

    public static final String serverAddr = "192.168.0.76";
    public static final short CLIENT_PORT = 6000;

    public static final int CONNECTION_CHECK = 1;    // 서비스에 대한 명령어
    public static final int PRINT_REQUEST = 2;    // 서비스에 대한 명령어
    public static final int SIGN_REQUEST = 3;    // 서비스에 대한 명령어
    public static final int CARDPAY_REQUEST = 4;    // 서비스에 대한 명령어
    public static final int CASHPAY_NUMBER_REQUEST = 5;    // 서비스에 대한 명령어
    public static final int CASHPAY_RECEIPT_REQUEST = 6;    // 서비스에 대한 명령어

    final Messenger mMessenger = new Messenger(new IncomingHandler());    // 클라이언트가 메세지를 보내는 도구

    // j2xx
    public static D2xxManager ftD2xx = null;
    FT_Device ftDev;
    int DevCount = -1;
    int currentPortIndex = -1;
    int portIndex = -1;

    enum DeviceStatus {
        DEV_NOT_CONNECT,
        DEV_NOT_CONFIG,
        DEV_CONFIG
    }

    // log tag
    final String TXR = "XM-Rec";

    // handler event
    final int UPDATE_TEXT_VIEW_CONTENT = 0;
    final int UPDATE_SEND_FILE_STATUS = 1;
    final int UPDATE_SEND_FILE_DONE = 2;
    final int MSG_SELECT_FOLDER_NOT_FILE = 7;
    final int MSG_XMODEM_SEND_FILE_TIMEOUT = 8;
    final int UPDATE_MODEM_RECEIVE_DATA = 9;
    final int UPDATE_MODEM_RECEIVE_DATA_BYTES = 10;
    final int UPDATE_MODEM_RECEIVE_DONE = 11;
    final int MSG_MODEM_RECEIVE_PACKET_TIMEOUT = 12;
    final int ACT_MODEM_SELECT_SAVED_FILE_FOLDER = 13;
    final int MSG_MODEM_OPEN_SAVE_FILE_FAIL = 14;
    final int MSG_YMODEM_PARSE_FIRST_PACKET_FAIL = 15;
    final int MSG_FORCE_STOP_SEND_FILE = 16;
    final int UPDATE_ASCII_RECEIVE_DATA_BYTES = 17;
    final int UPDATE_ASCII_RECEIVE_DATA_DONE = 18;
    final int MSG_FORCE_STOP_SAVE_TO_FILE = 19;
    final int UPDATE_ZMODEM_STATE_INFO = 20;
    final int ACT_ZMODEM_AUTO_START_RECEIVE = 21;

    final int MSG_SPECIAL_INFO = 98;
    final int MSG_UNHANDLED_CASE = 99;

    final byte XON = 0x11;    /* Resume transmission */
    final byte XOFF = 0x13;    /* Pause transmission */

    boolean INTERNAL_DEBUG_TRACE = false; // Toast message for debug

    // strings of file transfer protocols
    String currentProtocol;

    final int MODE_GENERAL_UART = 0;
    final int MODE_X_MODEM_CHECKSUM_RECEIVE = 1;
    final int MODE_X_MODEM_CHECKSUM_SEND = 2;
    final int MODE_X_MODEM_CRC_RECEIVE = 3;
    final int MODE_X_MODEM_CRC_SEND = 4;
    final int MODE_X_MODEM_1K_CRC_RECEIVE = 5;
    final int MODE_X_MODEM_1K_CRC_SEND = 6;
    final int MODE_Y_MODEM_1K_CRC_RECEIVE = 7;
    final int MODE_Y_MODEM_1K_CRC_SEND = 8;
    final int MODE_Z_MODEM_RECEIVE = 9;
    final int MODE_Z_MODEM_SEND = 10;

    int transferMode = MODE_GENERAL_UART;
    int tempTransferMode = MODE_GENERAL_UART;

    // X, Y, Z modem - UART MODE: Asynchronous?B8 data??bits?Bno parity?Bone stop??bit
    // X modem + //
    final int PACTET_SIZE_XMODEM_CHECKSUM = 132; // SOH,pkt,~ptk,128data,checksum
    final int PACTET_SIZE_XMODEM_CRC = 133;     // SOH,pkt,~ptk,128data,CRC-H,CRC-L
    final int PACTET_SIZE_XMODEM_1K_CRC = 1029;     // STX,pkt,~ptk,1024data,CRC-H,CRC-L

    final byte SOH = 1;    /* Start Of Header */
    final byte STX = 2;    /* Start Of Header 1K */
    final byte EOT = 4;    /* End Of Transmission */
    final byte ACK = 6;    /* ACKnowlege */
    final byte NAK = 0x15; /* Negative AcKnowlege */
    final byte CAN = 0x18; /* Cancel */
    final byte CHAR_C = 0x43; /* Character 'C' */
    final byte CHAR_G = 0x47; /* Character 'G' */

    final int DATA_SIZE_128 = 128;
    final int DATA_SIZE_256 = 256;
    final int DATA_SIZE_512 = 512;
    final int DATA_SIZE_1K = 1024;

    final int MODEM_BUFFER_SIZE = 2048;
    int[] modemReceiveDataBytes;
    byte[] modemDataBuffer;
    byte[] zmDataBuffer;
    byte receivedPacketNumber = 1;

    boolean bModemGetNak = false;
    boolean bModemGetAck = false;
    boolean bModemGetCharC = false;
    boolean bModemGetCharG = false;

    int totalModemReceiveDataBytes = 0;
    boolean bDataReceived = false;
    boolean bReceiveFirstPacket = false;
    boolean bDuplicatedPacket = false;

    boolean bUartModeTaskSet = true;
    boolean bReadDataProcess = true;

    String modemFileName;
    String modemFileSize;
    int modemRemainData = 0;

    final byte ZPAD = 0x2A; // '*' 052 Padding character begins frames
    final byte ZDLE = 0x18;

    final byte ZBIN = 0x41;        // 'A' Binary frame indicator (CRC-16)
    final byte ZHEX = 0x42;        // 'B' HEX frame indicator
    final byte ZBIN32 = 0x43;    // 'C' Binary frame with 32 bit CRC

    final int ZRQINIT = 0;   /* Request receive init */
    final int ZRINIT = 1;   /* Receive init */
    final int ZFILE = 4;     /* File name from sender */
    final int ZFIN = 8;      /* Finish session */
    final int ZRPOS = 9;     /* Resume data trans at this position */
    final int ZDATA = 10;    /* Data packet(s) follow */
    final int ZDATA_HEADER = 21;

    final int ZEOF = 11;     /* End of file */
    final int ZOO = 20;
    final int ZCRCW = 0x6B; // file info end

    final int ZDLE_END_SIZE_4 = 4; // zdle ZCRC? crc1 crc2
    final int ZDLE_END_SIZE_5 = 5; // zdle ZCRC? zdle crc1 crc2 || zdle ZCRC? crc1 zdle crc2
    final int ZDLE_END_SIZE_6 = 6; // zdle ZCRC? zdle crc1 zdle crc2

    int zmodemState = 0;

    // fixed pattern, used to check ZRQINIT
    final int ZMS_0 = 0;
    final int ZMS_1 = 1; // r
    final int ZMS_2 = 2; // z
    final int ZMS_3 = 3; // \r
    final int ZMS_4 = 4; // ZPAD (ZRQINIT)
    final int ZMS_5 = 5; // ZPAD
    final int ZMS_6 = 6; // ZDLE
    final int ZMS_7 = 7; // ZHEX
    final int ZMS_8 = 8; // 0x30
    final int ZMS_9 = 9; // 0x30
    final int ZMS_10 = 10; // 0x30
    final int ZMS_11 = 11; // 0x30
    final int ZMS_12 = 12; // 0x30
    final int ZMS_13 = 13; // 0x30
    final int ZMS_14 = 14; // 0x30
    final int ZMS_15 = 15; // 0x30
    final int ZMS_16 = 16; // 0x30
    final int ZMS_17 = 17; // 0x30
    final int ZMS_18 = 18; // 0x30
    final int ZMS_19 = 19; // 0x30
    final int ZMS_20 = 20; // 0x30
    final int ZMS_21 = 21; // 0x30 (14th 0x30)
    final int ZMS_22 = 22; // 0x0D
    final int ZMS_23 = 23; // 0x0A
    final int ZMS_24 = 24; // 0x11
    int zmStartState = 0;
    // Z modem -//

    // general data count
    int totalReceiveDataBytes = 0;
    int totalUpdateDataBytes = 0;

    // thread to read the data
    HandlerThread handlerThread; // update data to UI
    ReadThread readThread; // read data from USB

    boolean bSendButtonClick = false;
    boolean bLogButtonClick = false;
    boolean bContentFormatHex = false;

    // variables
    final int UI_READ_BUFFER_SIZE = 10240; // Notes: 115K:1440B/100ms, 230k:2880B/100ms
    byte[] writeBuffer;
    byte[] readBuffer;
    char[] readBufferToChar;
    int actualNumBytes;

    int baudRate; /* baud rate */
    byte stopBit; /* 1:1stop bits, 2:2 stop bits */
    byte dataBit; /* 8:8bit, 7: 7bit */
    byte parity; /* 0: none, 1: odd, 2: even, 3: mark, 4: space */
    byte flowControl; /* 0:none, 1: CTS/RTS, 2:DTR/DSR, 3:XOFF/XON */
    public Context global_context;
    boolean uart_configured = false;

    String uartSettings = "";
    BufferedOutputStream buf_save;
    boolean WriteFileThread_start = false;

    String fileNameInfo;
    int iFileSize = 0;
    int sendByteCount = 0;
    long start_time, end_time;
    long cal_time_1, cal_time_2;
    byte[] readDataBuffer; /* circular buffer */

    int iTotalBytes;
    int iReadIndex;

    final int MAX_NUM_BYTES = 65536;

    boolean bReadTheadEnable = false;

    public void showDialog(String _message) {
        Intent intent = new Intent();
        intent.setAction(INTENT_ACTION);
        intent.putExtra("RETCODE", "8888");
        intent.putExtra("MESSAGE", _message);
        intent.putExtra("RESULT", true);
        sendBroadcast(intent);
    }

    public void dismissDialog() {
        Intent intent = new Intent();
        intent.setAction(INTENT_ACTION);
        intent.putExtra("RETCODE", "8888");
        intent.putExtra("MESSAGE", "");
        intent.putExtra("RESULT", false);
        sendBroadcast(intent);
    }

    // 클라이언트로부터의 메세지를 받았을 때 처리할 클래스
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case CONNECTION_CHECK:
                    //Toast.makeText(getApplicationContext(), "Messenger Service", Toast.LENGTH_SHORT).show();
                    connectionCheck();

                    break;
                case PRINT_REQUEST:
                    //Toast.makeText(getApplicationContext(), "Messenger Service", Toast.LENGTH_SHORT).show();
                    byte[] cont = msg.getData().getByteArray("content");
                    print(cont);
                    break;
                case SIGN_REQUEST:
                    //Toast.makeText(getApplicationContext(), "Messenger Service", Toast.LENGTH_SHORT).show();
                    byte[] sign = msg.getData().getByteArray("sign");
                    sign(sign);
                    break;
                case CARDPAY_REQUEST:
                    //Toast.makeText(getApplicationContext(), "Messenger Service", Toast.LENGTH_SHORT).show();
                    byte[] payauth = msg.getData().getByteArray("payauth");
                    cardPay(payauth);
                    break;
                case CASHPAY_NUMBER_REQUEST:
                    byte[] noReq = msg.getData().getByteArray("noReq");
                    numberReq(noReq);
                    break;
                case CASHPAY_RECEIPT_REQUEST:
                    byte[] authReceipt = msg.getData().getByteArray("authReceipt");
                    authReceiptReq(authReceipt);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    public boolean isConnected() {
        if (mChannel == null) return false;
        Log.e("isConnected : ", mChannel.isConnected() + "");
        return mChannel.isConnected();
    }

    public void connectionCheck() {
        showDialog("연결상태 확인중..");

        try {
            byte ENQ = 0x05;
            byte[] reqBody = UnicodeFormatter.hexToByteArray(UnicodeFormatter.byteToHex(ENQ));

            Log.e("requestCode : ", UnicodeFormatter.byteArrayToHex(reqBody));

            ByteBuffer buffer = ByteBuffer.allocate(1);
            buffer = ByteBuffer.wrap(reqBody);
            int ret = send(buffer);
            Log.e("send ret : ", ret + "");
        } catch (Exception e) {
            Log.e("tag", "Data send error.\n" + e.getLocalizedMessage());
        }
    }

    public void print(byte[] content) {
        showDialog("프린트 시도중..");

        String serviceType = null;
        currentType = "5301";
        try {
            serviceType = UnicodeFormatter.stringToHex(currentType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + UnicodeFormatter.byteArrayToHex(content);
        Log.e("body print : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }

    public void sign(byte[] sign) {
        String serviceType = null;
        currentType = "5101";
        try {
            serviceType = UnicodeFormatter.stringToHex(currentType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + UnicodeFormatter.byteArrayToHex(sign);
        Log.e("body sign : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }

    String currentType = "";

    public void cardPay(byte[] payAuth) {
        showDialog("카드결제 시도..");

        Logger.d(TAG, "cardPay: ");
        String serviceType = null;
        currentType = "0101";
        try {
            serviceType = UnicodeFormatter.stringToHex(currentType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + UnicodeFormatter.byteArrayToHex(payAuth);
        Log.e("body cardPay : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }

    public void numberReq(byte[] req) {
        String serviceType = null;
        currentType = "5103";
        try {
            serviceType = UnicodeFormatter.stringToHex(currentType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + UnicodeFormatter.byteArrayToHex(req);
        Log.e("body numberReq : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }

    public void authReceiptReq(byte[] req) {
        showDialog("현금영수증 발행요청..");

        String serviceType = null;
        currentType = "0101";
        try {
            serviceType = UnicodeFormatter.stringToHex(currentType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + UnicodeFormatter.byteArrayToHex(req);
        Log.e("body authReceiptReq : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }

    private void requestCode(byte[] _reqBytes) {
        try {
            int reqSize = _reqBytes.length + 7;

            //String hexSize = UnicodeFormatter.stringToHex(String.valueOf(reqSize));

            String hexSize = UnicodeFormatter.stringToHex(String.format(Locale.KOREA, "%04d", reqSize));

            byte STX = 0x02;
            byte ETX = 0x03;
            byte[] calculateByte = UnicodeFormatter.hexToByteArray(hexSize + UnicodeFormatter.byteArrayToHex(_reqBytes) + UnicodeFormatter.byteToHex(ETX));
            byte BCC = MZCrypto.getBCC(calculateByte);

            byte[] reqBody = UnicodeFormatter.hexToByteArray(UnicodeFormatter.byteToHex(STX) + hexSize + UnicodeFormatter.byteArrayToHex(_reqBytes) + UnicodeFormatter.byteToHex(ETX) + UnicodeFormatter.byteToHex(BCC));

            Log.e("requestCode : ", "reqSize : " + hexSize + "\n" + UnicodeFormatter.byteArrayToHex(reqBody));

            ByteBuffer buffer = ByteBuffer.allocate(reqBody.length);
            buffer = ByteBuffer.wrap(reqBody);
            int ret = send(buffer);
            Log.e("send ret : ", ret + "");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("tag", "Data send error.\n" + e.getLocalizedMessage());
        }
    }

    private void sendAck06() {
        showDialog("결제승인 완료처리 중..");
        byte[] reqBody = UnicodeFormatter.hexToByteArray("060606");

        ByteBuffer buffer = ByteBuffer.allocate(reqBody.length);
        buffer = ByteBuffer.wrap(reqBody);
        int ret = send(buffer);
    }

    protected void setNetworkThreadPolicy() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void onCreate() {

        init();
        super.onCreate();

        Log.e("onCreate : ", "onCreate");
        Toast.makeText(getApplicationContext(), "service start..", Toast.LENGTH_SHORT).show();
        //sdcard에 있는 test.mp3 을 찾아검사
        boolean connRet = connect(serverAddr, CLIENT_PORT, 5, rHandler);
        if (connRet) {
            Log.e("connection : ", "Success");
        } else {
            Log.e("connection : ", "Failed");
        }

        createDeviceList();
        if (DevCount > 0) {
            connectFunction();
            setUARTInfoString();
            setConfig(baudRate, dataBit, stopBit, parity, flowControl);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        init();
        Log.e("onStartCommand : ", "onStartCommand");

        Toast.makeText(getApplicationContext(), "service start..", Toast.LENGTH_SHORT).show();
        //sdcard에 있는 test.mp3 을 찾아검사
        boolean connRet = connect(serverAddr, CLIENT_PORT, 5, rHandler);
        if (connRet) {
            Log.e("connection : ", "Success");
        } else {
            Log.e("connection : ", "Failed");
        }
        return START_STICKY;
    }

    private void init() {
        try {
            ftD2xx = D2xxManager.getInstance(this);
        } catch (D2xxManager.D2xxException e) {
            Log.e("FTDI_HT", "getInstance fail!!");
        }

        global_context = this;

        // init modem variables
        modemReceiveDataBytes = new int[1];
        modemReceiveDataBytes[0] = 0;
        modemDataBuffer = new byte[MODEM_BUFFER_SIZE];
        zmDataBuffer = new byte[MODEM_BUFFER_SIZE];


        /* allocate buffer */
        writeBuffer = new byte[512];
        readBuffer = new byte[UI_READ_BUFFER_SIZE];
        readBufferToChar = new char[UI_READ_BUFFER_SIZE];
        readDataBuffer = new byte[MAX_NUM_BYTES];
        actualNumBytes = 0;

        // start main text area read thread
        handlerThread = new HandlerThread(handler);
        handlerThread.start();
    }

    boolean openModemSaveFile() {
        modemRemainData = iFileSize = Integer.parseInt(modemFileSize);
        if (iFileSize <= 1024)
            fileNameInfo = "File:" + modemFileName + "(" + iFileSize + "Bytes)";
        else if (iFileSize <= 1048576)
            fileNameInfo = "File:" + modemFileName + "(" + new java.text.DecimalFormat("#.00").format(iFileSize / (double) 1024) + "KB)";
        else
            fileNameInfo = "File:" + modemFileName + "(" + new java.text.DecimalFormat("#.00").format(iFileSize / (double) 1024 / 1024) + "MB)";

        WriteFileThread_start = true;
        return true;
    }

    // call this API to show message
    void midToast(String str, int showTime) {
        Toast toast = Toast.makeText(global_context, str, showTime);
        toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);

        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(Color.YELLOW);
        toast.show();
    }

    public ReceiveEventHandler rHandler = new ReceiveEventHandler() {
        @Override
        public void onReceived(ByteBuffer buffer, int len) {
            byte[] getBytes = new byte[len];
            getBytes = buffer.array();
            String s = UnicodeFormatter.byteArrayToHex(getBytes);

            if (getBytes[0] == 0x02) {
                String resTypeHex = UnicodeFormatter.byteToHex(getBytes[5]) + UnicodeFormatter.byteToHex(getBytes[6]) + UnicodeFormatter.byteToHex(getBytes[7]) + UnicodeFormatter.byteToHex(getBytes[8]);
                byte[] resTypeBytes = UnicodeFormatter.hexToByteArray(resTypeHex);
                String resTypeStr = new String(resTypeBytes, 0, resTypeBytes.length);

                if ("0102".equals(resTypeStr)) {

                    byte[] pat = new byte[1];
                    pat[0] = (byte) 0x1C;
                    List<byte[]> ret = UnicodeFormatter.split(getBytes, pat);

                    byte[] retCode = ret.get(16);
                    String retCodeHex = UnicodeFormatter.byteArrayToHex(ret.get(16));
                    try {
                        if (retCodeHex.equals(UnicodeFormatter.stringToHex("00"))) {
                            showDialog("결제가 성공적으로 완료되었습니다.");
                            Intent intent = new Intent();
                            intent.setAction(INTENT_ACTION);
                            intent.putExtra("RETCODE", "0102");
                            intent.putExtra("RESULT", true);
                            sendBroadcast(intent);

                            sendAck06();
                        } else {
                            if (retCodeHex.equals(UnicodeFormatter.stringToHex("ZZ"))) {

                            }

                            Intent intent = new Intent();
                            intent.setAction(INTENT_ACTION);
                            intent.putExtra("RETCODE", "0102");
                            intent.putExtra("RESULT", false);
                            intent.putExtra("MESSAGE", new String(ret.get(17), "euc-kr"));
                            sendBroadcast(intent);

                            dismissDialog();
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                } else if ("9998".equals(resTypeStr)) {
                    // 단말기 상태정보
                    String deviceStatusCodeHex = UnicodeFormatter.byteToHex(getBytes[10]) + UnicodeFormatter.byteToHex(getBytes[11]);
                    byte[] deviceStatusCodeBytes = UnicodeFormatter.hexToByteArray(deviceStatusCodeHex);
                    String deviceStatusCode = new String(deviceStatusCodeBytes, 0, deviceStatusCodeBytes.length);

                    Log.e("[deviceStatusCode]==", deviceStatusCode);
                    if ("01".equals(deviceStatusCode)) {
                        showDialog("카드 삽입 대기 중입니다.");
                    } else if ("02".equals(deviceStatusCode)) {
                        showDialog("카드를 읽었습니다.");
                    } else if ("03".equals(deviceStatusCode)) {
                        showDialog("비밀번호 입력 대기 중입니다.");
                    } else if ("04".equals(deviceStatusCode)) {
                        showDialog("비밀번호 입력이 완료 되었습니다.");
                    } else if ("05".equals(deviceStatusCode)) {
                        showDialog("서명 입력 대기 중입니다.");
                    } else if ("06".equals(deviceStatusCode)) {
                        showDialog("서명 입력이 완료 되었습니다.");
                    } else if ("07".equals(deviceStatusCode)) {
                        showDialog("VAN 통신 중입니다.");
                    } else if ("08".equals(deviceStatusCode)) {
                        showDialog("VAN 통신이 완료되었습니다.");
                    } else if ("09".equals(deviceStatusCode)) {
                        showDialog("망취소 중입니다.");
                    } else if ("10".equals(deviceStatusCode)) {
                        showDialog("망취소가 완료되었습니다.");
                    }

                }

            } else if (getBytes[0] == 0x10) {
                Log.e("[Process]==", "END");
                dismissDialog();
            } else {
                //dismissDialog();
            }
            Log.e("Socket onReceived", s);
        }

        @Override
        public void onClosed() {
            Log.e("Socket onClosed", "");
        }

        @Override
        public void onThreadEvent() {
            Log.e("Socket onThreadEvent", "");
        }
    };

    public boolean connect(String ipAddress, short port, int timeout, ReceiveEventHandler handler) {
        setNetworkThreadPolicy();

        try {
            if (mChannel != null && mChannel.isConnected() == true)
                return false;

            mChannel = SocketChannel.open();
            mChannel.configureBlocking(false);
            mChannel.socket().setReceiveBufferSize(RECV_BUFFER_SIZE);

            mChannel.connect(new InetSocketAddress(ipAddress, port));

            Selector selector = Selector.open();
            SelectionKey clientKey = mChannel.register(selector, SelectionKey.OP_CONNECT);

            if (selector.select(timeout * 1000) > 0) {
                if (clientKey.isConnectable()) {
                    if (mChannel.finishConnect()) {
                        mThread = new ReceiveThread(mChannel, handler);
                        mThread.start();
                        return true;
                    }
                }
                mChannel.close();
                mChannel = null;
                return false;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public void close() {
        setNetworkThreadPolicy();
        try {
            if (mChannel != null) {
                if (mThread != null) {
                    mThread.mIsRunning = false;
                    mThread.join();
                }
                mChannel.close();
                mChannel = null;
                System.out.println("tcp client channel closed");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int send(ByteBuffer buffer) {
        setNetworkThreadPolicy();
        if (mChannel == null || !mChannel.isConnected()) return -1;
        try {
            return mChannel.write(buffer);
        } catch (IOException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    public interface ReceiveEventHandler {
        public void onReceived(ByteBuffer buffer, int len);

        public void onClosed();

        public void onThreadEvent();
    }

    protected class ReceiveThread extends Thread {
        private SocketChannel mChannel;
        public boolean mIsRunning = false;

        private ReceiveEventHandler mHandler;

        private static final int BUFFER_SIZE = 1024 * 4;

        public ReceiveThread(SocketChannel channel, ReceiveEventHandler handler) {
            mChannel = channel;
            mHandler = handler;
            mIsRunning = true;
        }

        @Override
        public void run() {
            System.out.println("receive thread start");

            try {
                Selector selector = Selector.open();
                mChannel.register(selector, SelectionKey.OP_READ);

                while (mIsRunning) {
                    //System.out.println("receive thread mIsRunning / selector isOpen : "+selector.isOpen());

                    if (selector.select(2 * 1000) > 0) {
                        Set<SelectionKey> selectedKey = selector.selectedKeys();
                        Iterator<SelectionKey> iterator = selectedKey.iterator();

                        System.out.println("selector.select(2*1000) : " + selectedKey.toString());

                        while (iterator.hasNext()) {
                            SelectionKey key = iterator.next();
                            iterator.remove();

                            if (key.isReadable()) {
                                SocketChannel channel = (SocketChannel) key.channel();
                                ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                                buffer.clear();

                                int ret;
                                try {
                                    ret = channel.read(buffer);
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                    if (mHandler != null) mHandler.onClosed();
                                    key.cancel();
                                    continue;
                                }

                                System.out.println("receive thread return value :" + ret);

                                if (ret <= 0) {
                                    System.out.println("SocketChannel.read returned " + ret);
                                    if (mHandler != null) mHandler.onClosed();
                                    key.cancel();
                                    continue;
                                }

                                buffer.rewind();

                                if (mHandler != null) mHandler.onReceived(buffer, ret);
                            }
                        }
                    } else {
                        //System.out.println("selector.select(2*1000) : " + selector.select(2*1000));
                    }
                    if (mHandler != null) mHandler.onThreadEvent();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            System.out.println("receive thread end");
        }
    }

    @Override
    public void onDestroy() {
        disconnectFunction();
        System.out.println("service destroy..");
        close();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }


    /*
    j2xx Function
     */
    public void updatePortNumberSelector() {
        midToast(DevCount + " port device attached", Toast.LENGTH_SHORT);
    }

    // j2xx functions +
    public void createDeviceList() {
        int tempDevCount = ftD2xx.createDeviceInfoList(global_context);

        if (tempDevCount > 0) {
            if (DevCount != tempDevCount) {
                DevCount = tempDevCount;
                updatePortNumberSelector();
            }
        } else {
            DevCount = -1;
            currentPortIndex = -1;
        }
    }

    public void disconnectFunction() {
        DevCount = -1;
        currentPortIndex = -1;
        bReadTheadEnable = false;
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (ftDev != null) {
            if (true == ftDev.isOpen()) {
                ftDev.close();
            }
        }
    }

    public void connectFunction() {
        if (portIndex + 1 > DevCount) {
            portIndex = 0;
        }

        if (currentPortIndex == portIndex
                && ftDev != null
                && true == ftDev.isOpen()) {
            //Toast.makeText(global_context,"Port("+portIndex+") is already opened.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (true == bReadTheadEnable) {
            bReadTheadEnable = false;
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (null == ftDev) {
            ftDev = ftD2xx.openByIndex(global_context, portIndex);
        } else {
            ftDev = ftD2xx.openByIndex(global_context, portIndex);
        }
        uart_configured = false;

        if (ftDev == null) {
            midToast("Open port(" + portIndex + ") NG!", Toast.LENGTH_LONG);
            return;
        }

        if (true == ftDev.isOpen()) {
            currentPortIndex = portIndex;
            //Toast.makeText(global_context, "open device port(" + portIndex + ") OK", Toast.LENGTH_SHORT).show();

            if (false == bReadTheadEnable) {
                readThread = new ReadThread(handler);
                readThread.start();
            }
        } else {
            midToast("Open port(" + portIndex + ") NG!", Toast.LENGTH_LONG);
        }
    }

    DeviceStatus checkDevice() {
        if (ftDev == null || false == ftDev.isOpen()) {
            midToast("Need to connect to cable.", Toast.LENGTH_SHORT);
            return DeviceStatus.DEV_NOT_CONNECT;
        } else if (false == uart_configured) {
            //midToast("CHECK: uart_configured == false", Toast.LENGTH_SHORT);
            midToast("Need to configure UART.", Toast.LENGTH_SHORT);
            return DeviceStatus.DEV_NOT_CONFIG;
        }

        return DeviceStatus.DEV_CONFIG;

    }

    void setUARTInfoString() {
        String parityString, flowString;

        switch (parity) {
            case 0:
                parityString = new String("None");
                break;
            case 1:
                parityString = new String("Odd");
                break;
            case 2:
                parityString = new String("Even");
                break;
            case 3:
                parityString = new String("Mark");
                break;
            case 4:
                parityString = new String("Space");
                break;
            default:
                parityString = new String("None");
                break;
        }

        switch (flowControl) {
            case 0:
                flowString = new String("None");
                break;
            case 1:
                flowString = new String("CTS/RTS");
                break;
            case 2:
                flowString = new String("DTR/DSR");
                break;
            case 3:
                flowString = new String("XOFF/XON");
                break;
            default:
                flowString = new String("None");
                break;
        }

        uartSettings = "Port " + portIndex + "; UART Setting  -  Baudrate:" + baudRate + "  StopBit:" + stopBit
                + "  DataBit:" + dataBit + "  Parity:" + parityString
                + "  FlowControl:" + flowString;

        resetStatusData();
    }

    void setConfig(int baud, byte dataBits, byte stopBits, byte parity, byte flowControl) {
        // configure port
        // reset to UART mode for 232 devices
        ftDev.setBitMode((byte) 0, D2xxManager.FT_BITMODE_RESET);

        ftDev.setBaudRate(baud);

        switch (dataBits) {
            case 7:
                dataBits = D2xxManager.FT_DATA_BITS_7;
                break;
            case 8:
                dataBits = D2xxManager.FT_DATA_BITS_8;
                break;
            default:
                dataBits = D2xxManager.FT_DATA_BITS_8;
                break;
        }

        switch (stopBits) {
            case 1:
                stopBits = D2xxManager.FT_STOP_BITS_1;
                break;
            case 2:
                stopBits = D2xxManager.FT_STOP_BITS_2;
                break;
            default:
                stopBits = D2xxManager.FT_STOP_BITS_1;
                break;
        }

        switch (parity) {
            case 0:
                parity = D2xxManager.FT_PARITY_NONE;
                break;
            case 1:
                parity = D2xxManager.FT_PARITY_ODD;
                break;
            case 2:
                parity = D2xxManager.FT_PARITY_EVEN;
                break;
            case 3:
                parity = D2xxManager.FT_PARITY_MARK;
                break;
            case 4:
                parity = D2xxManager.FT_PARITY_SPACE;
                break;
            default:
                parity = D2xxManager.FT_PARITY_NONE;
                break;
        }

        ftDev.setDataCharacteristics(dataBits, stopBits, parity);

        short flowCtrlSetting;
        switch (flowControl) {
            case 0:
                flowCtrlSetting = D2xxManager.FT_FLOW_NONE;
                break;
            case 1:
                flowCtrlSetting = D2xxManager.FT_FLOW_RTS_CTS;
                break;
            case 2:
                flowCtrlSetting = D2xxManager.FT_FLOW_DTR_DSR;
                break;
            case 3:
                flowCtrlSetting = D2xxManager.FT_FLOW_XON_XOFF;
                break;
            default:
                flowCtrlSetting = D2xxManager.FT_FLOW_NONE;
                break;
        }

        ftDev.setFlowControl(flowCtrlSetting, XON, XOFF);

        setUARTInfoString();
        midToast(uartSettings, Toast.LENGTH_SHORT);

        uart_configured = true;
    }

    void sendData(int numBytes, byte[] buffer) {
        if (ftDev.isOpen() == false) {
            Toast.makeText(global_context, "Device not open!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (numBytes > 0) {
            ftDev.write(buffer, numBytes);
        }
    }

    void sendData(byte buffer) {
        byte tmpBuf[] = new byte[1];
        tmpBuf[0] = buffer;
        ftDev.write(tmpBuf, 1);
    }
// j2xx functions -

    // get the first byte of incoming data
    byte firstData() {
        if (iTotalBytes > 0) {
            return readDataBuffer[iReadIndex];
        }

        return 0x00;
    }

    // For zmWaitReadData: Write data at offset of buffer.
    byte readData(int numBytes, int offset, byte[] buffer) {
        byte intstatus = 0x00; /* success by default */

        /* should be at least one byte to read */
        if ((numBytes < 1) || (0 == iTotalBytes)) {
            actualNumBytes = 0;
            intstatus = 0x01;
            return intstatus;
        }

        if (numBytes > iTotalBytes) {
            numBytes = iTotalBytes;
        }

        /* update the number of bytes available */
        iTotalBytes -= numBytes;
        actualNumBytes = numBytes;

        /* copy to the user buffer */
        for (int count = offset; count < numBytes + offset; count++) {
            buffer[count] = readDataBuffer[iReadIndex];
            iReadIndex++;
            iReadIndex %= MAX_NUM_BYTES;
        }

        return intstatus;
    }

    byte readData(int numBytes, byte[] buffer) {
        byte intstatus = 0x00; /* success by default */

        /* should be at least one byte to read */
        if ((numBytes < 1) || (0 == iTotalBytes)) {
            actualNumBytes = 0;
            intstatus = 0x01;
            return intstatus;
        }

        if (numBytes > iTotalBytes) {
            numBytes = iTotalBytes;
        }

        /* update the number of bytes available */
        iTotalBytes -= numBytes;
        actualNumBytes = numBytes;

        /* copy to the user buffer */
        for (int count = 0; count < numBytes; count++) {
            buffer[count] = readDataBuffer[iReadIndex];
            iReadIndex++;
            iReadIndex %= MAX_NUM_BYTES;
        }

        return intstatus;
    }

    void resetStatusData() {
//        String tempStr = "Format - " + (bContentFormatHex?"Hexadecimal":"Character") +"\n"+ uartSettings;
//        String tmp = tempStr.replace("\\n", "\n");
//        uartInfo.setText(tmp);
    }

    void updateStatusData(String str) {
        String temp;
        if (null == fileNameInfo)
            temp = "\n" + str;
        else
            temp = fileNameInfo + "\n" + str;

        String tmp = temp.replace("\\n", "\n");
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UPDATE_TEXT_VIEW_CONTENT:
                    if (actualNumBytes > 0) {
                        totalUpdateDataBytes += actualNumBytes;
                        for (int i = 0; i < actualNumBytes; i++) {
                            readBufferToChar[i] = (char) readBuffer[i];
                        }
                        //appendData(String.copyValueOf(readBufferToChar, 0, actualNumBytes));
                    }
                    break;

                case UPDATE_SEND_FILE_STATUS: {
                    String temp = currentProtocol;
                    if (sendByteCount <= 10240)
                        temp += " Send:" + sendByteCount + "B("
                                + new java.text.DecimalFormat("#.00").format(sendByteCount / (iFileSize / (double) 100)) + "%)";
                    else
                        temp += " Send:" + new java.text.DecimalFormat("#.00").format(sendByteCount / (double) 1024) + "KB("
                                + new java.text.DecimalFormat("#.00").format(sendByteCount / (iFileSize / (double) 100)) + "%)";

                    updateStatusData(temp);
                }
                break;

                case UPDATE_SEND_FILE_DONE: {
                    midToast("Send file Done.", Toast.LENGTH_SHORT);

                    String temp = currentProtocol;
                    if (0 == iFileSize) {
                        temp += " - The sent file is 0 byte";
                    } else if (iFileSize < 100) {
                        temp += " Send:" + sendByteCount + "B("
                                + new java.text.DecimalFormat("#.00").format(sendByteCount * 100 / iFileSize) + "%)";
                    } else {
                        if (sendByteCount <= 10240)
                            temp += " Send:" + sendByteCount + "B("
                                    + new java.text.DecimalFormat("#.00").format(sendByteCount / (iFileSize / (double) 100)) + "%)";
                        else
                            temp += " Send:" + new java.text.DecimalFormat("#.00").format(sendByteCount / (double) 1024) + "KB("
                                    + new java.text.DecimalFormat("#.00").format(sendByteCount / (iFileSize / (double) 100)) + "%)";
                    }

                    Double diffime = (double) (end_time - start_time) / 1000;
                    temp += " in " + diffime.toString() + " seconds";

                    updateStatusData(temp);

                    //resetSendButton();
                }
                break;

                case MSG_SELECT_FOLDER_NOT_FILE:
                    midToast("Do not pick a file.\n" +
                            "Plesae press \"Select Directory\" button to select current directory.", Toast.LENGTH_LONG);
                    break;

                case MSG_XMODEM_SEND_FILE_TIMEOUT: {
                    String temp = currentProtocol + " - No response when send file.";
                    midToast(temp, Toast.LENGTH_LONG);
                    updateStatusData(temp);

                    //resetSendButton();
                }
                break;

                case UPDATE_MODEM_RECEIVE_DATA:
                    midToast(currentProtocol + " - Receiving data...", Toast.LENGTH_LONG);

                case UPDATE_MODEM_RECEIVE_DATA_BYTES: {
                    String temp = currentProtocol;
                    if (totalModemReceiveDataBytes <= 10240)
                        temp += " Receive " + totalModemReceiveDataBytes + "Bytes";
                    else
                        temp += " Receive " + new java.text.DecimalFormat("#.00").format(totalModemReceiveDataBytes / (double) 1024) + "KBytes";

                    updateStatusData(temp);
                }
                break;

                case UPDATE_MODEM_RECEIVE_DONE: {
                    //saveFileActionDone();

                    String temp = currentProtocol;
                    if (totalModemReceiveDataBytes <= 10240)
                        temp += " Receive " + totalModemReceiveDataBytes + "Bytes";
                    else
                        temp += " Receive " + new java.text.DecimalFormat("#.00").format(totalModemReceiveDataBytes / (double) 1024) + "KBytes";

                    Double diffime = (double) (end_time - start_time) / 1000;
                    temp += " in " + diffime.toString() + " seconds";

                    updateStatusData(temp);
                }
                break;

                case MSG_MODEM_RECEIVE_PACKET_TIMEOUT: {
                    midToast(currentProtocol + " - No Incoming Data.", Toast.LENGTH_LONG);
                    String temp = currentProtocol;
                    if (totalModemReceiveDataBytes <= 10240)
                        temp += " Receive " + totalModemReceiveDataBytes + "Bytes";
                    else
                        temp += " Receive " + new java.text.DecimalFormat("#.00").format(totalModemReceiveDataBytes / (double) 1024) + "KBytes";

                    updateStatusData(temp);
                    //saveFileActionDone();
                }
                break;

                case ACT_MODEM_SELECT_SAVED_FILE_FOLDER:
                    setProtocolMode();

                    //getModemSelectedFolder();
                    break;

                case MSG_MODEM_OPEN_SAVE_FILE_FAIL:
                    midToast(currentProtocol + " - Open save file fail!", Toast.LENGTH_LONG);
                    break;

                case MSG_YMODEM_PARSE_FIRST_PACKET_FAIL:
                    midToast("YModem - Can't parse packet due to incorrect data format!", Toast.LENGTH_LONG);
                    //resetLogButton();
                    break;

                case MSG_FORCE_STOP_SEND_FILE:
                    midToast("Stop sending file.", Toast.LENGTH_LONG);
                    break;

                case UPDATE_ASCII_RECEIVE_DATA_BYTES: {
                    String temp = currentProtocol;
                    if (totalReceiveDataBytes <= 10240)
                        temp += " Receive " + totalReceiveDataBytes + "Bytes";
                    else
                        temp += " Receive " + new java.text.DecimalFormat("#.00").format(totalReceiveDataBytes / (double) 1024) + "KBytes";

                    long tempTime = System.currentTimeMillis();
                    Double diffime = (double) (tempTime - start_time) / 1000;
                    temp += " in " + diffime.toString() + " seconds";

                    updateStatusData(temp);
                }
                break;

                case UPDATE_ASCII_RECEIVE_DATA_DONE:
                    //saveFileActionDone();
                    break;

                case MSG_FORCE_STOP_SAVE_TO_FILE:
                    midToast("Stop saving to file.", Toast.LENGTH_LONG);
                    break;

                case UPDATE_ZMODEM_STATE_INFO:
                    updateStatusData("zmodemState:" + zmodemState);

                    if (ZOO == zmodemState) {
                        midToast("ZModem revice file done.", Toast.LENGTH_SHORT);
                    }
                    break;

                case ACT_ZMODEM_AUTO_START_RECEIVE:
                    bUartModeTaskSet = false;
                    transferMode = MODE_Z_MODEM_RECEIVE;
                    currentProtocol = "ZModem";


                    receivedPacketNumber = 1;
                    modemReceiveDataBytes[0] = 0;
                    totalModemReceiveDataBytes = 0;
                    bDataReceived = false;
                    bReceiveFirstPacket = false;
                    fileNameInfo = null;

                    //setLogButton();

                    zmodemState = ZRINIT;
                    start_time = System.currentTimeMillis();
                    ZModemReadDataThread zmReadThread = new ZModemReadDataThread(handler);
                    zmReadThread.start();
                    break;


                case MSG_SPECIAL_INFO:

                    midToast("INFO:" + (String) (msg.obj), Toast.LENGTH_LONG);
                    break;

                case MSG_UNHANDLED_CASE:
                    if (msg.obj != null)
                        midToast("UNHANDLED CASE:" + (String) (msg.obj), Toast.LENGTH_LONG);
                    else
                        midToast("UNHANDLED CASE ?", Toast.LENGTH_LONG);
                    break;
                default:
                    midToast("NG CASE", Toast.LENGTH_LONG);
                    //Toast.makeText(global_context, ".", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    void setProtocolMode() {
        if (true == bUartModeTaskSet) {
            transferMode = MODE_GENERAL_UART;
            currentProtocol = "Ascii";
        } else {
            transferMode = tempTransferMode;
            switch (transferMode) {
                case MODE_X_MODEM_CHECKSUM_RECEIVE:
                case MODE_X_MODEM_CHECKSUM_SEND:
                    currentProtocol = "XModem-Checksum";
                    break;

                case MODE_X_MODEM_CRC_RECEIVE:
                case MODE_X_MODEM_CRC_SEND:
                    currentProtocol = "XModem-CRC";
                    break;

                case MODE_X_MODEM_1K_CRC_RECEIVE:
                case MODE_X_MODEM_1K_CRC_SEND:
                    currentProtocol = "XModem-1KCRC";
                    break;

                case MODE_Y_MODEM_1K_CRC_RECEIVE:
                case MODE_Y_MODEM_1K_CRC_SEND:
                    currentProtocol = "YModem";
                    break;

                case MODE_Z_MODEM_RECEIVE:
                case MODE_Z_MODEM_SEND:
                    currentProtocol = "ZModem";
                    break;

                default:
                    currentProtocol = "unknown";
                    break;
            }
        }
    }

    // Update UI content
    class HandlerThread extends Thread {
        Handler mHandler;

        HandlerThread(Handler h) {
            mHandler = h;
        }

        public void run() {
            byte status;
            Message msg;

            while (true) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (true == bContentFormatHex) // consume input data at hex content format
                {
                    status = readData(UI_READ_BUFFER_SIZE, readBuffer);
                } else if (MODE_GENERAL_UART == transferMode) {
                    status = readData(UI_READ_BUFFER_SIZE, readBuffer);

                    if (0x00 == status) {
                        if (false == WriteFileThread_start) {
                            checkZMStartingZRQINIT();
                        }

                        // save data to file
                        if (true == WriteFileThread_start && buf_save != null) {
                            try {
                                buf_save.write(readBuffer, 0, actualNumBytes);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        msg = mHandler.obtainMessage(UPDATE_TEXT_VIEW_CONTENT);
                        mHandler.sendMessage(msg);
                    }
                }
            }
        }
    }

    class ReadThread extends Thread {
        final int USB_DATA_BUFFER = 8192;

        Handler mHandler;

        ReadThread(Handler h) {
            mHandler = h;
            this.setPriority(MAX_PRIORITY);
        }

        public void run() {
            byte[] usbdata = new byte[USB_DATA_BUFFER];
            int readcount = 0;
            int iWriteIndex = 0;
            bReadTheadEnable = true;

            while (true == bReadTheadEnable) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                while (iTotalBytes > (MAX_NUM_BYTES - (USB_DATA_BUFFER + 1))) {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                readcount = ftDev.getQueueStatus();
                //Log.e(">>@@","iavailable:" + iavailable);
                if (readcount > 0) {
                    if (readcount > USB_DATA_BUFFER) {
                        readcount = USB_DATA_BUFFER;
                    }
                    ftDev.read(usbdata, readcount);

                    if ((MODE_X_MODEM_CHECKSUM_SEND == transferMode)
                            || (MODE_X_MODEM_CRC_SEND == transferMode)
                            || (MODE_X_MODEM_1K_CRC_SEND == transferMode)) {
                        for (int i = 0; i < readcount; i++) {
                            modemDataBuffer[i] = usbdata[i];
                        }

                        if (NAK == modemDataBuffer[0]) {
                            bModemGetNak = true;
                        } else if (ACK == modemDataBuffer[0]) {
                            bModemGetAck = true;
                        } else if (CHAR_C == modemDataBuffer[0]) {
                            bModemGetCharC = true;
                        }
                        if (CHAR_G == modemDataBuffer[0]) {
                            bModemGetCharG = true;
                        }
                    } else {
                        totalReceiveDataBytes += readcount;
                        for (int count = 0; count < readcount; count++) {
                            readDataBuffer[iWriteIndex] = usbdata[count];
                            iWriteIndex++;
                            iWriteIndex %= MAX_NUM_BYTES;
                        }

                        if (iWriteIndex >= iReadIndex) {
                            iTotalBytes = iWriteIndex - iReadIndex;
                        } else {
                            iTotalBytes = (MAX_NUM_BYTES - iReadIndex) + iWriteIndex;
                        }

                        if ((MODE_X_MODEM_CHECKSUM_RECEIVE == transferMode) || (MODE_X_MODEM_CRC_RECEIVE == transferMode) || (MODE_X_MODEM_1K_CRC_RECEIVE == transferMode)
                                || (MODE_Y_MODEM_1K_CRC_RECEIVE == transferMode) || (MODE_Z_MODEM_RECEIVE == transferMode) || (MODE_Z_MODEM_SEND == transferMode)) {
                            modemReceiveDataBytes[0] += readcount;
                        }
                    }
                }
            }
        }
    }


    boolean parseModemPacket() {
        boolean parseOK = true;
        byte pktnum, notpktnum;
        int packetSize = 132;

        if (MODE_X_MODEM_CHECKSUM_RECEIVE == transferMode) {
            packetSize = PACTET_SIZE_XMODEM_CHECKSUM;
        } else if (MODE_X_MODEM_CRC_RECEIVE == transferMode) {
            packetSize = PACTET_SIZE_XMODEM_CRC;
        } else if (MODE_X_MODEM_1K_CRC_RECEIVE == transferMode ||
                MODE_Y_MODEM_1K_CRC_RECEIVE == transferMode) {
            packetSize = PACTET_SIZE_XMODEM_1K_CRC;
        }


        pktnum = modemDataBuffer[1];
        notpktnum = modemDataBuffer[2];


        if (MODE_Y_MODEM_1K_CRC_RECEIVE == transferMode && 0 == pktnum && false == bReceiveFirstPacket) // y modem:duplicated file info packet
        {
            bDuplicatedPacket = true;
            modemReceiveDataBytes[0] -= PACTET_SIZE_XMODEM_CRC;

            int tmpRange = packetSize - PACTET_SIZE_XMODEM_CRC; // Need to move 896(1029 - 133) data.

            for (int i = 0; i < tmpRange; i++) {
                modemDataBuffer[i] = modemDataBuffer[i + PACTET_SIZE_XMODEM_CRC];
            }
            return true;
        }


        if (pktnum != (~notpktnum)) {
            //DLog.e(TXR,"pktnum:"+pktnum+" notpktnum:"+notpktnum);
            parseOK = false;
        }
        if (pktnum != receivedPacketNumber) {
            parseOK = false;
        }


        if (MODE_X_MODEM_1K_CRC_RECEIVE == transferMode) {
            if (modemDataBuffer[0] != STX) {
                parseOK = false;
            }
        } else if (MODE_Y_MODEM_1K_CRC_RECEIVE == transferMode) {

            if (modemDataBuffer[0] == SOH) {
                packetSize = PACTET_SIZE_XMODEM_CRC;
            } else if (modemDataBuffer[0] == STX) {
            } else {
                parseOK = false;
            }
        } else // checksum, crc
        {
            if (modemDataBuffer[0] != SOH) {
                parseOK = false;
            }
        }
        modemReceiveDataBytes[0] -= packetSize;

        if (true == parseOK) {
            if (MODE_X_MODEM_CHECKSUM_RECEIVE == transferMode) {
                byte checksum = 0;
                for (int i = 3; i < (DATA_SIZE_128 + 3); i++) {
                    checksum += modemDataBuffer[i];
                }

                if (modemDataBuffer[131] != checksum) {
                    Log.e(TXR, "checksum check NG");
                    parseOK = false;
                }
            } else if (MODE_X_MODEM_CRC_RECEIVE == transferMode) {
                byte[] crcHL = calCrc(modemDataBuffer, 3, DATA_SIZE_128);

                if (crcHL[0] != modemDataBuffer[131] || crcHL[1] != modemDataBuffer[132]) {
                    parseOK = false;
                }
            } else if (MODE_Y_MODEM_1K_CRC_RECEIVE == transferMode && PACTET_SIZE_XMODEM_CRC == packetSize) {
                // TODO: should check crc; last packet, assume it is ok.
            } else if (MODE_X_MODEM_1K_CRC_RECEIVE == transferMode ||
                    MODE_Y_MODEM_1K_CRC_RECEIVE == transferMode) {
                byte[] crcHL = calCrc(modemDataBuffer, 3, DATA_SIZE_1K);

                if (crcHL[0] != modemDataBuffer[1027] || crcHL[1] != modemDataBuffer[1028]) {
                    parseOK = false;
                }
            } else {
                parseOK = false;
            }
        }

        // save data to file
        if (true == parseOK && true == WriteFileThread_start && buf_save != null) {
            try {
                if (MODE_Y_MODEM_1K_CRC_RECEIVE == transferMode) {
                    if (modemRemainData >= DATA_SIZE_1K) {
                        totalModemReceiveDataBytes += DATA_SIZE_1K;
                        buf_save.write(modemDataBuffer, 3, DATA_SIZE_1K); // data size: 1024
                    } else {
                        totalModemReceiveDataBytes += modemRemainData;
                        buf_save.write(modemDataBuffer, 3, modemRemainData); // remain data and this packet should be last packet
                    }
                    modemRemainData -= DATA_SIZE_1K;
                } else if (MODE_X_MODEM_1K_CRC_RECEIVE == transferMode) {
                    totalModemReceiveDataBytes += DATA_SIZE_1K;
                    buf_save.write(modemDataBuffer, 3, DATA_SIZE_1K); // data size: 1024
                } else // checksum, crc
                {
                    totalModemReceiveDataBytes += DATA_SIZE_128;
                    buf_save.write(modemDataBuffer, 3, DATA_SIZE_128); // data size: 128
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return parseOK;
    }

    boolean parseYModemFirstPacket() {
        boolean parseOK = true;
        char filename[] = new char[128];
        char filesize[] = new char[12];

        modemReceiveDataBytes[0] -= PACTET_SIZE_XMODEM_CRC;

        if (modemDataBuffer[0] != SOH) {
            parseOK = false;
        }

        if (modemDataBuffer[1] != 0x00) {
            parseOK = false;
        }

        if (modemDataBuffer[2] != ~modemDataBuffer[1]) {
            parseOK = false;
        }

        if (true == parseOK) {
            int i = 0;
            while (modemDataBuffer[i + 3] != 0x00) {
                filename[i] = (char) modemDataBuffer[i + 3];
                i++;
            }

            i++;
            int j = 0;
            while ((modemDataBuffer[j + i + 3] != 0x00) && ((j + i + 3) < (PACTET_SIZE_XMODEM_CRC - 1))) {
                filesize[j] = (char) modemDataBuffer[j + i + 3];
                j++;
            }

            modemFileName = String.copyValueOf(filename, 0, i - 1);
            modemFileSize = String.copyValueOf(filesize, 0, j);
        }

        return parseOK;
    }

    class ZModemReadDataThread extends Thread {
        Handler mHandler;

        ZModemReadDataThread(Handler h) {
            mHandler = h;
            this.setPriority(MAX_PRIORITY);
        }

        public void run() {
            Message msg;
            boolean bFileReciveDone = false;
            int[] getDataNum = new int[1];
            byte[] tempBuffer = new byte[24];

            bReadDataProcess = true;
            totalModemReceiveDataBytes = 0;

            cal_time_1 = System.currentTimeMillis();

            while (bReadDataProcess) {

                if (false == bLogButtonClick) {
                    msg = mHandler.obtainMessage(MSG_FORCE_STOP_SAVE_TO_FILE);
                    mHandler.sendMessage(msg);
                    continue;
                }

                if (zmodemState >= ZDATA) {
                    cal_time_2 = System.currentTimeMillis();
                    if ((cal_time_2 - cal_time_1) > 200) // update status every 200ms
                    {
                        msg = mHandler.obtainMessage(UPDATE_MODEM_RECEIVE_DATA_BYTES);
                        mHandler.sendMessage(msg);
                        cal_time_1 = cal_time_2;
                    }
                }

                switch (zmodemState) {
                    case ZRQINIT:
                        if (true == zmWaitReadData(3, 0, 20000)) {
                            if (modemDataBuffer[0] == 0x72 &&  // r
                                    modemDataBuffer[1] == 0x7A &&  // z
                                    modemDataBuffer[2] == 0x0D)      // \r
                            {
                                start_time = System.currentTimeMillis();

                                if (true == zmWaitReadData(21, 0, 10000)) {
                                    if (zmGetHeaderType() > 0) {
                                        zmodemState = ZRINIT;

                                        msg = mHandler.obtainMessage(UPDATE_MODEM_RECEIVE_DATA);
                                        mHandler.sendMessage(msg);
                                    }
                                }
                            }
                        }
                        break;

                    case ZEOF:
                    case ZRINIT:
                        // TODO: how to set ZRINIT packet
                        tempBuffer[0] = 0x2A;
                        tempBuffer[1] = 0x2A;
                        tempBuffer[2] = 0x18;
                        tempBuffer[3] = 0x42;
                        tempBuffer[4] = 0x30;
                        tempBuffer[5] = 0x31;
                        tempBuffer[6] = 0x30;
                        tempBuffer[7] = 0x30;
                        tempBuffer[8] = 0x30;
                        tempBuffer[9] = 0x30;
                        tempBuffer[10] = 0x30;
                        tempBuffer[11] = 0x30;
                        tempBuffer[12] = 0x30;
                        tempBuffer[13] = 0x33;
                        tempBuffer[14] = 0x39;
                        tempBuffer[15] = 0x61;
                        tempBuffer[16] = 0x33;
                        tempBuffer[17] = 0x32;
                        tempBuffer[18] = 0x0D;
                        tempBuffer[19] = 0x0A;
                        tempBuffer[20] = 0x11;
                        sendData(21, tempBuffer);

                        if (ZRINIT == zmodemState) {
                            zmodemState = ZFILE;
                        } else if (ZEOF == zmodemState) {
                            zmodemState = ZFIN;
                        }
                        break;

                    case ZFILE:

                        if (true == zmWaitReadData(10, 0, 10000)) {
                            if (modemDataBuffer[0] == ZPAD && modemDataBuffer[1] == ZDLE && modemDataBuffer[2] == ZBIN && modemDataBuffer[3] == ZFILE) {
                                if (true == zmWaitReadData(9, 0, 1000)) {
                                    getDataNum[0] = 9;
                                } else {
                                    getDataNum[0] = 0;
                                }
                                zmWaitFileInfoData(getDataNum);
                                zmParseFileInfo(getDataNum[0]);

                                if (false == openModemSaveFile()) {
                                    msg = mHandler.obtainMessage(MSG_MODEM_OPEN_SAVE_FILE_FAIL);
                                    mHandler.sendMessage(msg);
                                    bReadDataProcess = false;
                                    continue;
                                }
                            }
                            zmodemState = ZRPOS;

                            try {                    // clear remain data after zdle zcrcw
                                Thread.sleep(300);
                            } catch (Exception e) {
                            }
                            zmReadAllData(1000);

                            msg = mHandler.obtainMessage(UPDATE_ZMODEM_STATE_INFO);
                            mHandler.sendMessage(msg);
                        }
                        break;

                    case ZRPOS:
                        // TODO: how to set ZRPOS packet
                        tempBuffer[0] = 0x2A;
                        tempBuffer[1] = 0x2A;
                        tempBuffer[2] = 0x18;
                        tempBuffer[3] = 0x42;
                        tempBuffer[4] = 0x30;
                        tempBuffer[5] = 0x39;
                        tempBuffer[6] = 0x30;
                        tempBuffer[7] = 0x30;
                        tempBuffer[8] = 0x30;
                        tempBuffer[9] = 0x30;
                        tempBuffer[10] = 0x30;
                        tempBuffer[11] = 0x30;
                        tempBuffer[12] = 0x30;
                        tempBuffer[13] = 0x30;
                        tempBuffer[14] = 0x61;
                        tempBuffer[15] = 0x38;
                        tempBuffer[16] = 0x37;
                        tempBuffer[17] = 0x63;
                        tempBuffer[18] = 0x0D;
                        tempBuffer[19] = 0x0A;
                        tempBuffer[20] = 0x11;
                        sendData(21, tempBuffer);
                        zmodemState = ZDATA_HEADER;

                        break;

                    case ZDATA_HEADER:
                        if (true == zmWaitReadData(10, 0, 10000)) {
                            for (int i = 0; i < 10; i++)

                                if (modemDataBuffer[0] == ZPAD && modemDataBuffer[1] == ZDLE && modemDataBuffer[2] == ZBIN && modemDataBuffer[3] == 0x0A) // ZDATA{
                                    zmodemState = ZDATA;
                        } else {
                            bReadDataProcess = false;
                        }
                        break;

                    case ZDATA: {
                        getDataNum[0] = zmReadAllData(1000);

                        int zeofPos = zmCheckZEOF(getDataNum);
                        int numSaveDataBytes = zmParseDataPacket(getDataNum, zeofPos);

                        try {
                            buf_save.write(zmDataBuffer, 0, numSaveDataBytes);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        totalModemReceiveDataBytes += numSaveDataBytes;

                        if (zeofPos != -1) {
                            zmodemState = ZEOF;
                            bFileReciveDone = true;
                        }
                    }
                    break;

                    case ZFIN: {
                        if (true == zmWaitReadData(20, 0, 10000)) // get ZFIN
                        {
                            tempBuffer[0] = 0x2A; // ZPAD
                            tempBuffer[1] = 0x2A; // ZPAD
                            tempBuffer[2] = 0x18; // ZDLE
                            tempBuffer[3] = 0x42; // ZHEX
                            tempBuffer[4] = 0x30;
                            tempBuffer[5] = 0x38;
                            tempBuffer[6] = 0x30;
                            tempBuffer[7] = 0x30;
                            tempBuffer[8] = 0x30;
                            tempBuffer[9] = 0x30;
                            tempBuffer[10] = 0x30;
                            tempBuffer[11] = 0x30;
                            tempBuffer[12] = 0x30;
                            tempBuffer[13] = 0x30;
                            tempBuffer[14] = 0x30;
                            tempBuffer[15] = 0x32;
                            tempBuffer[16] = 0x32;
                            tempBuffer[17] = 0x64;
                            tempBuffer[18] = 0x0d;
                            tempBuffer[19] = (byte) 0x8a;
                            sendData(20, tempBuffer); // send ZFIN
                            zmodemState = ZOO;
                        } else {
                            bReadDataProcess = false;
                        }
                    }
                    break;

                    case ZOO:
                        if (true == zmWaitReadData(2, 0, 10000)) // get ZFIN
                        {
                            if (0x4F == modemDataBuffer[0] && 0x4F == modemDataBuffer[1]) {
                                bReadDataProcess = false;
                            } else {
                                bReadDataProcess = false;
                                int num = zmReadAllData(10);

                                if (num > 0) {
                                    for (int i = 0; i < num; i++);
                                }
                            }
                        } else {
                            bReadDataProcess = false;
                        }

                        break;

                    default:
                        break;
                }
            }

            transferMode = MODE_GENERAL_UART;
            bUartModeTaskSet = true;
            end_time = System.currentTimeMillis();
            if (true == bFileReciveDone) {
                msg = mHandler.obtainMessage(UPDATE_MODEM_RECEIVE_DONE);
                mHandler.sendMessage(msg);
            } else if (true == bLogButtonClick) {
                msg = mHandler.obtainMessage(MSG_MODEM_RECEIVE_PACKET_TIMEOUT);
                mHandler.sendMessage(msg);
            }
        }
    }

    boolean zmParseFileInfo(int dataNum) {
        boolean parseOK = true;
        char filename[] = new char[128];
        char filesize[] = new char[12];

        int i = 0;
        while (modemDataBuffer[i] != 0x00) {
            filename[i] = (char) modemDataBuffer[i];
            i++;
        }

        i++;
        int j = 0;
        // file size: xp hyperterm  end with 0x20, pcomm end with 0x00
        while ((modemDataBuffer[j + i] != 0x00 && modemDataBuffer[j + i] != 0x20) && ((j + i) < (dataNum - 1))) {
            filesize[j] = (char) modemDataBuffer[j + i];
            j++;
        }

        modemFileName = String.copyValueOf(filename, 0, i - 1);
        modemFileSize = String.copyValueOf(filesize, 0, j);
        return parseOK;
    }

    int zmReadAllData(int waitTime) // for Z modem read data
    {
        long time_1, time_2;
        time_1 = System.currentTimeMillis();

        do {
            if (modemReceiveDataBytes[0] > 0) {
                int dataNum = modemReceiveDataBytes[0];
                if (dataNum > DATA_SIZE_256)        // read 256 bytes data each time
                {
                    dataNum = DATA_SIZE_256;
                }

                readData(dataNum, modemDataBuffer);
                modemReceiveDataBytes[0] -= dataNum;
                return dataNum;
            }

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (false == bLogButtonClick && false == bSendButtonClick) {
                return 0;
            }

            time_2 = System.currentTimeMillis();
        }
        while ((time_2 - time_1) < waitTime);

        return 0;
    }

    int zmCheckZEOF(int[] dataNum) {
        for (int i = 0; i < dataNum[0]; i++) {
            if (ZPAD == modemDataBuffer[i]) {

                if (dataNum[0] < i + 3 + 1) // check whether there is enough data for parsing: zeof
                {
                    zmWaitReadData((i + 3 + 1 - dataNum[0]), dataNum[0], 1000);
                    dataNum[0] += (i + 3 + 1 - dataNum[0]);
                }

                if ((i + 3 + 1) <= dataNum[0]) {
                    if (ZDLE == modemDataBuffer[i + 1] &&
                            ZBIN == modemDataBuffer[i + 2] &&
                            0x0b == modemDataBuffer[i + 3]) // ZEOF
                    {

                        if (dataNum[0] < i + 9 + 1) {
                            zmWaitReadData((i + 9 + 1 - dataNum[0]), dataNum[0], 2000);
                            dataNum[0] += (i + 9 + 1 - dataNum[0]);
                        }

                        return i;
                    }
                } else {
                    if (true == INTERNAL_DEBUG_TRACE) {
                        Message msg = handler.obtainMessage(MSG_UNHANDLED_CASE, String.valueOf("zmCheckZEOF unhandle case"));
                        handler.sendMessage(msg);
                    }
                }
            }
        }
        return -1;
    }

    boolean zmWaitReadData(int numByte, int offset, int waitTime) // for Z modem read data
    {
        long time_1, time_2;
        time_1 = System.currentTimeMillis();
        do {
            if (modemReceiveDataBytes[0] >= numByte) {
                readData(numByte, offset, modemDataBuffer);
                modemReceiveDataBytes[0] -= numByte;
                return true;
            }

            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (false == bLogButtonClick && false == bSendButtonClick) {
                return false;
            }

            time_2 = System.currentTimeMillis();
        }
        while ((time_2 - time_1) < waitTime);
        return false;
    }

    void zmWaitFileInfoData(int[] dataNum) {

        int i = 0;
        while (true) {

            if (dataNum[0] < i + 1 + 1) {
                zmWaitReadData(1, dataNum[0], 1000); // // check whether there is enough data for parsing: zlde zcrcw
                dataNum[0] += 1;

            }

            if (ZDLE == modemDataBuffer[i])  // find zdle
            {
                if (ZCRCW == modemDataBuffer[i + 1]) {
                    return;
                }
            }

            i++;

            if (i > 256)
                break;
        }
    }

    int zmGetHeaderType() {
        byte headerType = 0;

        String logStr = new String("Data: ");
        for (int i = 0; i < 21; i++) {
            logStr += Integer.toHexString(modemDataBuffer[i]) + " ";
        }

        if (modemDataBuffer[0] != ZPAD) {
            return -1;
        }

        if (ZPAD == modemDataBuffer[1]) {

            if (modemDataBuffer[2] != ZDLE) {
                return -1;
            }

            if (modemDataBuffer[3] != ZHEX) {
                return -1;
            } else {
                headerType = ZHEX;
            }
        } else if (ZDLE == modemDataBuffer[1]) {

            if (ZBIN == modemDataBuffer[2]) {
                headerType = ZBIN;
            } else if (ZBIN32 == modemDataBuffer[2]) {
                headerType = ZBIN32;
            } else {
                return -1;
            }
        } else {
            return -1;
        }

        switch (headerType) {
            case ZHEX:
                break;
            case ZBIN:
                break;
            case ZBIN32:
                break;
            default:
                break;
        }
        return (headerType);
    }

    // Input readdata and zeofpos, save data to zmDataBuffer, return data number for saving
    int zmParseDataPacket(int[] dataNum, int zeofPos) {
        int j = 0;
        int frameendByte = ZDLE_END_SIZE_4;

        if (zeofPos >= 0) {
            dataNum[0] = zeofPos;
        }

        for (int i = 0; i < dataNum[0]; i++) {
            if (ZDLE == modemDataBuffer[i])  // find zdle
            {
                if (0 == i)

                // check whether there is enough data for parsing: zlde frameend crc1 crc2
                if (dataNum[0] < i + 3 + 1) {
                    zmWaitReadData((i + 3 + 1 - dataNum[0]), dataNum[0], 1000);
                    dataNum[0] += (i + 3 + 1 - dataNum[0]);
                    }

                if (((modemDataBuffer[i + 1] & 0x40) != 0) && ((modemDataBuffer[i + 1] & 0x20) == 0)) {
                    modemDataBuffer[i + 1] &= 0xbf;
                    zmDataBuffer[j++] = modemDataBuffer[i + 1];
                    i++; // i+1 is converted, skip this one
                } else {

                    // check two zdle case
                    if (ZDLE == modemDataBuffer[i + 2] || ZDLE == modemDataBuffer[i + 3]) {
                        zmWaitReadData(1, dataNum[0], 1000);
                        dataNum[0] += 1;
                        frameendByte = ZDLE_END_SIZE_5;

                        if (ZDLE == modemDataBuffer[i + 4])  // check three zlde case
                        {
                            zmWaitReadData(1, dataNum[0], 1000);
                            dataNum[0] += 1;
                            frameendByte = ZDLE_END_SIZE_6;
                        }
                    }

                    // skip frame data
                    i += frameendByte - 1;
                }
            } else {
                zmDataBuffer[j++] = modemDataBuffer[i];
            }
        }

        return j; // return j: data number for saving
    }

    void checkZMStartingZRQINIT() {
        Message msg;
        for (int i = 0; i < actualNumBytes; i++) {
            switch (zmStartState) {
                case ZMS_0:
                    if (0x72 == readBuffer[i]) zmStartState = ZMS_1;
                    break;
                case ZMS_1:
                    if (0x7A == readBuffer[i]) zmStartState = ZMS_2;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_2:
                    if (0x0D == readBuffer[i]) zmStartState = ZMS_3;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_3:
                    if (ZPAD == readBuffer[i]) zmStartState = ZMS_4;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_4:
                    if (ZPAD == readBuffer[i]) zmStartState = ZMS_5;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_5:
                    if (ZDLE == readBuffer[i]) zmStartState = ZMS_6;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_6:
                    if (ZHEX == readBuffer[i]) zmStartState = ZMS_7;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_7:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_8;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_8:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_9;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_9:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_10;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_10:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_11;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_11:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_12;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_12:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_13;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_13:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_14;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_14:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_15;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_15:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_16;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_16:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_17;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_17:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_18;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_18:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_19;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_19:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_20;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_20:
                    if (0x30 == readBuffer[i]) zmStartState = ZMS_21;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_21:
                    if (0x0D == readBuffer[i]) zmStartState = ZMS_22;
                    else zmStartState = ZMS_0;
                    break;
                case ZMS_22:
                    if (0x0A == readBuffer[i] || (byte) 0x8A == readBuffer[i])
                        zmStartState = ZMS_23;
                    else {
                        zmStartState = ZMS_0;
                    }
                    break;
                case ZMS_23:
                    if (0x11 == readBuffer[i]) zmStartState = ZMS_24;
                    else zmStartState = ZMS_0;
                    break;
                default:
                    break;
            }
            if (ZMS_24 == zmStartState) {
                zmStartState = ZMS_0;
                msg = handler.obtainMessage(ACT_ZMODEM_AUTO_START_RECEIVE);
                handler.sendMessage(msg);
            }
        }
    }

    byte[] calCrc(byte[] buffer, int startPos, int count) {
        int crc = 0, i;
        byte[] crcHL = new byte[2];

        while (--count >= 0) {
            crc = crc ^ (int) buffer[startPos++] << 8;
            for (i = 0; i < 8; ++i) {
                if ((crc & 0x8000) != 0) crc = crc << 1 ^ 0x1021;
                else crc = crc << 1;
            }
        }
        crc &= 0xFFFF;

        crcHL[0] = (byte) ((crc >> 8) & 0xFF);
        crcHL[1] = (byte) (crc & 0xFF);

        return crcHL;
    }
}