package rewhite.shopplus.util.hardware;

import android.content.DialogInterface;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import fdk.FDK_Module;
import me.rewhite.van.library.common.util.UnicodeFormatter;
import rewhite.shopplus.client.manager.ManagerGetSelectOrderItemList;
import rewhite.shopplus.client.manager.ManagerGetStoreDeviceInfo;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.manager.ManagerItemPaymentStoreOrder;
import rewhite.shopplus.client.manager.ManagerStoreContractInfo;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.model.GetOrderItemList;
import rewhite.shopplus.client.model.GetPaymentInfoItem;
import rewhite.shopplus.client.model.GetPaymentInfoModel;
import rewhite.shopplus.client.model.GetSelectOrderItemList;
import rewhite.shopplus.client.model.ManagerGetPaymentInfoModel;
import rewhite.shopplus.client.model.StoreUserInfoItemList;
import rewhite.shopplus.client.network.PaymentManager;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.PaymenrWaitCounter;
import rewhite.shopplus.util.SharedPreferencesUtil;

// 0A      : 줄바꿈
// 12      : 가운데 정렬
// 13      : 오른쪽 정렬
// 11      : 종이 커트
// 10      : 텍스트 Background
// 0F19    : 텍스트 크기변경

/**
 * FirstData 하드웨어 컨트롤 클래스
 */
public class FirstHardware {
    private static final String TAG = "FirstHardware";
    private PaymenrWaitCounter mPaymentWait;

    private String currentType = "";
    private SimpleDateFormat mFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static int validPeriodNumber = 0;
    private static String cardName = null;
    private static String authorizationNo = null;
    private static String validPeriod = null;
    private static String paymentDate = null;
    private static String maskingCardNo = null;
    private static String acquirerName = null;
    private static String merchantNo = null;

    /**
     * FistData Deliver App Setting Init
     *
     * @param activity
     */
    public static void init(AppCompatActivity activity) {
        Log.d(TAG, "setLayout");
        try {
            FDK_Module.Deliver_App_Resources_to_FDK("FTDI", activity);

            int iProcID = 0;
            int iRet = 0;
            String strMsg = "", strData = "";

            iProcID = FDK_Module.Creat();

//            iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/Util/SearchCAT");

//            FDK_Module.Input(iProcID, "Business Number", "2269139295");
//            FDK_Module.Input(iProcID, "CAT Serial Number", "SA80500589");
//
//            iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CATDataDownload_DL");

            if (0 == iRet) {
                strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
                strData = FDK_Module.Output(iProcID, "Response Code");
                strMsg = strMsg + strData;
                SharedPreferencesUtil.putSharedPreference(activity, PrefConstant.KEY_CONNECTION_STATUS, PrefConstant.NAME_CONNECTION_STATUS, true);
            } else if (-1000 == iRet) {
                strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
                strData = FDK_Module.Output(iProcID, "Response Code");
                strMsg = strMsg + strData;
                SharedPreferencesUtil.putSharedPreference(activity, PrefConstant.KEY_CONNECTION_STATUS, PrefConstant.NAME_CONNECTION_STATUS, false);
            } else {
                strMsg = "에러[" + iRet + "]\r\n";
                strData = FDK_Module.Output(iProcID, "ErrorInfo");
                strMsg = strMsg + strData;
                Log.d(TAG, "registerDevice strMsg : " + strMsg + "registerDevice strData : " + strData);
                SharedPreferencesUtil.putSharedPreference(activity, PrefConstant.KEY_CONNECTION_STATUS, PrefConstant.NAME_CONNECTION_STATUS, false);
            }
            Log.d(TAG, "registerDevice: " + strMsg);


            //Output Sample
            if (0 == iRet || -1000 == iRet) {
                strMsg += "STX : " + FDK_Module.Output(iProcID, "STX") + "\r\n";
                strMsg += "Work Type : " + FDK_Module.Output(iProcID, "Work Type") + "\r\n";
                strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
                strMsg += "Response Code : " + FDK_Module.Output(iProcID, "Response Code") + "\r\n";
                strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
                strMsg += "Business Number : " + FDK_Module.Output(iProcID, "Business Number") + "\r\n";
                strMsg += "FS3 : " + FDK_Module.Output(iProcID, "FS3") + "\r\n";
                strMsg += "CAT ID : " + FDK_Module.Output(iProcID, "CAT ID") + "\r\n";
                strMsg += "FS4 : " + FDK_Module.Output(iProcID, "FS4") + "\r\n";
                strMsg += "Representative Name : " + FDK_Module.Output(iProcID, "Representative Name") + "\r\n";
                strMsg += "FS5 : " + FDK_Module.Output(iProcID, "FS5") + "\r\n";
                strMsg += "Merchant Name : " + FDK_Module.Output(iProcID, "Merchant Name") + "\r\n";
                strMsg += "FS6 : " + FDK_Module.Output(iProcID, "FS6") + "\r\n";
                strMsg += "Merchant Address : " + FDK_Module.Output(iProcID, "Merchant Address") + "\r\n";
                strMsg += "FS7 : " + FDK_Module.Output(iProcID, "FS7") + "\r\n";
                strMsg += "Merchant Phone Number : " + FDK_Module.Output(iProcID, "Merchant Phone Number") + "\r\n";
                strMsg += "FS8 : " + FDK_Module.Output(iProcID, "FS8") + "\r\n";
                strMsg += "Agent Phone Number : " + FDK_Module.Output(iProcID, "Agent Phone Number") + "\r\n";
                strMsg += "FS9 : " + FDK_Module.Output(iProcID, "FS9") + "\r\n";
                strMsg += "ETX : " + FDK_Module.Output(iProcID, "ETX") + "\r\n";
                strMsg += "LRC : " + FDK_Module.Output(iProcID, "LRC") + "\r\n";

                Logger.d(TAG, "strMsg : " + strMsg);
            }

            //Output Sample
//            if (0 == iRet || -1000 == iRet) {
//                strMsg = "Output List :\r\n";
//                strMsg += "Model Name :" + FDK_Module.Output(iProcID, "H/W Model Name") + "\r\n";
//                SharedPreferencesUtil.putSharedPreference(activity, PrefConstant.KEY_MODEL_NAME, PrefConstant.NAME_MODEL_NAME, FDK_Module.Output(iProcID, "H/W Model Name"));
//                strMsg += "Serial Number :" + FDK_Module.Output(iProcID, "H/W Serial Number") + "\r\n";
//                SharedPreferencesUtil.putSharedPreference(activity, PrefConstant.KEY_SERIAL_NUMBER, PrefConstant.NAME_SERIAL_NUMBER, FDK_Module.Output(iProcID, "H/W Serial Number"));
//                strMsg += "CAT ID : " + FDK_Module.Output(iProcID, "CAT ID") + "\r\n";
//                SharedPreferencesUtil.putSharedPreference(activity, PrefConstant.KEY_CAT_ID, PrefConstant.NAME_CAT_ID, FDK_Module.Output(iProcID, "CAT ID"));
//                strMsg += "Port : " + FDK_Module.Output(iProcID, "Port") + "\r\n";
//                SharedPreferencesUtil.putSharedPreference(activity, PrefConstant.KEY_PORT, PrefConstant.NAME_PORT, FDK_Module.Output(iProcID, "Port"));
//                strMsg += "STX : " + FDK_Module.Output(iProcID, "STX") + "\r\n";
//                strMsg += "Work Type : " + FDK_Module.Output(iProcID, "Work Type") + "\r\n";
//                strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
//                strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
//                strMsg += "F/W Version : " + FDK_Module.Output(iProcID, "F/W Version") + "\r\n";
//                strMsg += "FS3 : " + FDK_Module.Output(iProcID, "FS3") + "\r\n";
//                strMsg += "H/W Serial Number : " + FDK_Module.Output(iProcID, "H/W Serial Number") + "\r\n";
//                strMsg += "FS4 : " + FDK_Module.Output(iProcID, "FS4") + "\r\n";
//                strMsg += "Integrity Info : " + FDK_Module.Output(iProcID, "Integrity Info") + "\r\n";
//                strMsg += "FS5 : " + FDK_Module.Output(iProcID, "FS5") + "\r\n";
//                strMsg += "ETX : " + FDK_Module.Output(iProcID, "ETX") + "\r\n";
//                strMsg += "LRC : " + FDK_Module.Output(iProcID, "LRC") + "\r\n";
//                Log.d(TAG, "registerDevice: " + strMsg);
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * FistData Deliver App Setting Init
     *
     * @param activity
     */
    public static String initStatus(AppCompatActivity activity) {
        Log.d(TAG, "setLayout");
        try {
            FDK_Module.Deliver_App_Resources_to_FDK("FTDI", activity);

            int iProcID = 0;
            String strMsg = "", strData = "";

            iProcID = FDK_Module.Creat();
            strMsg += "Response Code : " + FDK_Module.Output(iProcID, "Response Code") + "\r\n";

            return FDK_Module.Output(iProcID, "Response Code");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * FistData Connection Print Test
     */
    public static void testPrintFDK() {
        Log.d(TAG, "testPrintFDK");
        String printString = "";

        try {
            printString += "0E0B12" + UnicodeFormatter.stringToHex("프린터 테스트 인쇄", "euc-kr") + "0A";
            printString += "0E0B12" + UnicodeFormatter.stringToHex("=================", "euc-kr") + "0A";
            printString += "0A";
            printString += "0E0F12" + UnicodeFormatter.stringToHex("샵플러스 프린터의 테스트 인쇄입니다.", "euc-kr") + "0A";
            printString += "0E0F12" + UnicodeFormatter.stringToHex("프린터의 연결상태를 확인해주세요.", "euc-kr") + "0A";
            printString += "0E0F12" + UnicodeFormatter.stringToHex("문제가 해결되지 않을 경우,", "euc-kr") + "0A";
            printString += "0E0F12" + UnicodeFormatter.stringToHex("리화이트로 문의해주세요.", "euc-kr") + "0A";
            printString += "0E0F12" + UnicodeFormatter.stringToHex("고객센터 : 1544-2951(평일 10시~19시)", "euc-kr") + "0A";
            printString += "11";

            printStringExcute(printString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 회원 정보 데이터 출력
     */
    public static void getMemberInfoPrint() {
        List<StoreUserInfoItemList> storeUserInfoItemList = ManagerStoreUserInfo.getmInstance().getStoreUserInfo();

        /**
         * FistData Connection Print Test
         */
        Log.d(TAG, "testPrintFDK");
        try {
            String printString = "";

            printString += "0E0B12" + UnicodeFormatter.stringToHex("회원정보", "euc-kr") + "0A";
            printString += "0E0B12" + UnicodeFormatter.stringToHex("============", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("고객명 : " + storeUserInfoItemList.get(0).getData().getUserName(), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + storeUserInfoItemList.get(0).getData().get_UserPhone(), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + storeUserInfoItemList.get(0).getData().get_UserAddress(), "euc-kr") + "0A";
            printString += "0A";
            printString += UnicodeFormatter.stringToHex("────────────────────────", "euc-kr") + "0A";
            printString += "0E0F12" + UnicodeFormatter.stringToHex("■ 미수금 정보 ■", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("총 미수금 : " + DataFormatUtil.moneyFormatToWon(storeUserInfoItemList.get(0).getData().getNonPayPrice()), "euc-kr") + "0A";
            printString += "0A";
            printString += UnicodeFormatter.stringToHex("================================================", "euc-kr") + "0A";
            printString += "0A";
            printString += "0E0F12" + UnicodeFormatter.stringToHex("■ " + storeUserInfoItemList.get(0).getData().getUserName() + " 님의 잔액 ■", "euc-kr") + "0A";
            printString += "0A";
            printString += UnicodeFormatter.stringToHex("선충전금 : " + DataFormatUtil.moneyFormatToWon(storeUserInfoItemList.get(0).getData().getRealPrePaid()) + " \t\t 마일리지 :" + DataFormatUtil.moneyFormatToWon(storeUserInfoItemList.get(0).getData().getMileage()), "euc-kr") + "0A";

            printString += "11";

            printStringExcute(printString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 선충전금(현금) 데이터 출력 - 재발행 동일
     *
     * @param AmountPayment     총금액
     * @param accumulated       추가적립금액
     * @param totalChargeAmount 총 충전금액
     */
    public static void getPreFilledGoldPrint(String AmountPayment, String accumulated, String totalChargeAmount) {
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

        String getTime = sdf.format(date);
        List<StoreUserInfoItemList> storeUserInfoItemList = ManagerStoreUserInfo.getmInstance().getStoreUserInfo();
        try {
            String printString = "";

            printString += "0E0B12" + UnicodeFormatter.stringToHex("선충전금 충전내역 (현금)", "euc-kr") + "0A";
            printString += "0E0B12" + UnicodeFormatter.stringToHex("=======================", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("결제일자 : " + getTime, "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("고객명 : " + storeUserInfoItemList.get(0).getData().getUserName(), "euc-kr") + "140A";
            printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + storeUserInfoItemList.get(0).getData().get_UserPhone(), "euc-kr") + "140A";
            printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + storeUserInfoItemList.get(0).getData().get_UserAddress(), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("------------------------------------------------", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F0B13" + UnicodeFormatter.stringToHex("충전(결제) 금액 : " + AmountPayment, "euc-kr") + "0A";
            printString += "0F13" + UnicodeFormatter.stringToHex("추가적립금액 : " + accumulated, "euc-kr") + "0A";
            printString += "0F13" + UnicodeFormatter.stringToHex("총 충전금액 : " + totalChargeAmount, "euc-kr") + "0A";
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("------------------------------------------------", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("■ 충전 후 잔액 ■", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("선충전금 : " + AmountPayment + "\t\t적립금 : " + accumulated, "euc-kr") + "0A";
            printString += "0A";
            printString += UnicodeFormatter.stringToHex("────────────────────────", "euc-kr") + "0A";
            printString += "0A";
//            printString += "0E0B12" + UnicodeFormatter.stringToHex(ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().getStoreName(), "euc-kr") + "0A";
            printString += "0A";

            printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_MasterPhone(), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_StoreAddress2(), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getStoreGreetingText(), "euc-kr") + "0A";

            printString += "11";

            printStringExcute(printString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 선충전금(카드) 데이터 출력
     */
    public static String getPreFilledCardPrint(AppCompatActivity activity, int moneyCount, String AmountPayment, String accumulated, String totalChargeAmount) {
        Message msg = Message.obtain(null, FDKCatManager.PRINT_REQUEST, 0, 0);
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);

        int iProcID = 0;
        int iRet = 0;

        int taxation = Math.round(Integer.valueOf(moneyCount) / 11);
        Math.round(Double.valueOf(moneyCount) - taxation);
        StringBuffer strMsg = new StringBuffer();
        String strData = "";
        try {
            iProcID = FDK_Module.Creat();

//            FDK_Module.Input(iProcID, "Business Number", ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_BusinessRegistrationNumber());                // 사업자번호
            //TODO 등록된 사업자가 아니기 때문에 TEST강제 하드 코딩 배포시 위에 주석 해지..
            FDK_Module.Input(iProcID, "Business Number", "2268138295");                // 사업자번호
            FDK_Module.Input(iProcID, "Transaction Amount", String.valueOf(moneyCount)); // 결제금액
            FDK_Module.Input(iProcID, "Tax Amount", String.valueOf(Math.round(Integer.valueOf(moneyCount) - taxation)));                              // 세금
            FDK_Module.Input(iProcID, "Taxable Amount", String.valueOf(taxation));                         // 과세
            FDK_Module.Input(iProcID, "Non-Taxable Amount", "0");
            FDK_Module.Input(iProcID, "Receipt Print Flag", "2");

            iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CreditAuth_D6");

            Logger.d(TAG, "iRet : " + iRet);
            if (0 == iRet) {
                FDK_Module.Output(iProcID, "Response Code");

                strMsg.append("STX : " + FDK_Module.Output(iProcID, "STX"));
                strMsg.append("Work Type : " + FDK_Module.Output(iProcID, "Work Type"));
                strMsg.append("FS1 : " + FDK_Module.Output(iProcID, "FS1"));
                strMsg.append("Response Code : " + FDK_Module.Output(iProcID, "Response Code"));
                strMsg.append("FS2 : " + FDK_Module.Output(iProcID, "FS2"));
                strMsg.append("Business Number : " + FDK_Module.Output(iProcID, "Business Number"));
                strMsg.append("FS3 : " + FDK_Module.Output(iProcID, "FS3"));
                strMsg.append("CAT ID : " + FDK_Module.Output(iProcID, "CAT ID"));
                strMsg.append("FS4 : " + FDK_Module.Output(iProcID, "FS4"));
                strMsg.append("Representative Name : " + FDK_Module.Output(iProcID, "Representative Name"));
                strMsg.append("FS5 : " + FDK_Module.Output(iProcID, "FS5"));
                strMsg.append("Merchant Name : " + FDK_Module.Output(iProcID, "Merchant Name"));
                strMsg.append("FS6 : " + FDK_Module.Output(iProcID, "FS6"));
                strMsg.append("Merchant Address : " + FDK_Module.Output(iProcID, "Merchant Address"));
                strMsg.append("FS7 : " + FDK_Module.Output(iProcID, "FS7"));
                strMsg.append("Merchant Phone Number : " + FDK_Module.Output(iProcID, "Merchant Phone Number"));
                strMsg.append("FS8 : " + FDK_Module.Output(iProcID, "FS8"));
                strMsg.append("Agent Phone Number : " + FDK_Module.Output(iProcID, "Agent Phone Number"));
                strMsg.append("FS9 : " + FDK_Module.Output(iProcID, "FS9"));
                strMsg.append("ETX : " + FDK_Module.Output(iProcID, "ETX"));
                strMsg.append("LRC : " + FDK_Module.Output(iProcID, "LRC"));

                //Output Sample
                if (0 == iRet || -1000 == iRet) {
                    // 카드이름
                    cardName = FDK_Module.Output(iProcID, "Issuer Name");
                    // 거래일시
                    paymentDate = FDK_Module.Output(iProcID, "Authorization Date");

                    // 카드번호
                    String cardNo = FDK_Module.Output(iProcID, "Card Data");
                    maskingCardNo = cardNo.substring(0, 4) + "-" + cardNo.substring(4, 5) + "**-****-****";

                    // 유효기간(년/월):**/**
                    validPeriod = FDK_Module.Output(iProcID, "Installment Period"); // 값이 없을경우 일시불
                    if (validPeriod.length() == 0) {
                        validPeriod = "일시불";
                        validPeriodNumber = 0;
                    } else {
                        validPeriod = "할부" + validPeriod + "개월";
                        validPeriodNumber = Integer.valueOf(validPeriod);
                    }
                    // 가맹점번호
                    merchantNo = FDK_Module.Output(iProcID, "Merchant Number");
                    // 승인번호
                    authorizationNo = FDK_Module.Output(iProcID, "Authorization Number");
                    // 매입사
                    acquirerName = FDK_Module.Output(iProcID, "Acquirer Name");
                    // 무서명거래
                    String notice = FDK_Module.Output(iProcID, "Notice");
                }
                new Thread(new Runnable() {
                    public void run() {
                        String resultCod = PaymentManager.getManagerCardChargeStoreUserPrePaid(activity, Long.valueOf(SettingLogic.getStoreUserId(activity)), 2, moneyCount, authorizationNo, strMsg.toString(), cardName, maskingCardNo, validPeriodNumber);
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                if (resultCod.equals("S0000")) {
                                    try {
                                        long now = System.currentTimeMillis();
                                        Date date = new Date(now);
                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

                                        String getTime = sdf.format(date);
                                        List<StoreUserInfoItemList> storeUserInfoItemList = ManagerStoreUserInfo.getmInstance().getStoreUserInfo();
                                        String printString = "";
                                        printString += "0E0B12" + UnicodeFormatter.stringToHex("선충전금 충전내역 (카드)", "euc-kr") + "0A";

                                        printString += "0E0B12" + UnicodeFormatter.stringToHex("=======================", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("결제일자 : " + getTime, "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("고객명 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName(), "euc-kr") + "140A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserPhone(), "euc-kr") + "140A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserAddress(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("------------------------------------------------", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F0B13" + UnicodeFormatter.stringToHex("충전(결제) 금액 : " + AmountPayment, "euc-kr") + "0A";
                                        printString += "0F13" + UnicodeFormatter.stringToHex("추가적립금액 : " + accumulated, "euc-kr") + "0A";
                                        printString += "0F13" + UnicodeFormatter.stringToHex("총 충전금액 : " + totalChargeAmount, "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("------------------------------------------------", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("■ 충전 후 잔액 ■", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("선충전금 : " + AmountPayment + "\t\t적립금 : " + accumulated, "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringToHex("────────────────────────", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F0B12" + UnicodeFormatter.stringToHex("신용카드 매출전표", "euc-kr") + "0A";
                                        printString += "0F0B12" + UnicodeFormatter.stringToHex("=======================", "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex("[카드번호] : " + maskingCardNo, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex("[할부개월] : " + validPeriod, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex("[카 드 명] : " + cardName, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex("[승인번호] : " + authorizationNo, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex("[승인일시] : " + paymentDate, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex("[승인금액] : " + AmountPayment, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex("[매입사명] : " + acquirerName, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex("[가맹번호] : " + merchantNo, "euc-kr") + "0A";

                                        printString += UnicodeFormatter.stringToHex("────────────────────────", "euc-kr") + "0A";
                                        printString += "0A";
//                printString += "0E0B12" + UnicodeFormatter.stringToHex(ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().getStoreName(), "euc-kr") + "0A";
//                printString += "0A";
//                printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_MasterPhone(), "euc-kr") + "0A";
//                printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_StoreAddress2(), "euc-kr") + "0A";
//                printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getStoreGreetingText(), "euc-kr") + "0A";

                                        printString += "11";
                                        printStringExcute(printString);
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    thirdAlertPopup.setTitle("결제정보 전송오류");
                                    thirdAlertPopup.setContent("결제정보 전송중 오류 발생했어요. \n 고객센터로 문의해주세요.");
                                    thirdAlertPopup.show();
                                }
                            }
                        });
                    }
                }).start();


            } else if (-1000 == iRet) {
                thirdAlertPopup.setTitle("연결실패");
                thirdAlertPopup.setContent("카드 단말기 연결확인");
                thirdAlertPopup.show();
                FDK_Module.Output(iProcID, "Response Code");
            } else {
                FDK_Module.Output(iProcID, "ErrorInfo");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(iRet);
    }


    /**
     * 선충전금(카드) 재출력
     */
    public static String getReissueReceiptsCardPrint(List<GetPaymentInfoModel> mPaymentInfo) {
        Message msg = Message.obtain(null, FDKCatManager.PRINT_REQUEST, 0, 0);
        int iRet = 0;

        StringBuffer strMsg = new StringBuffer();

        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

        String getTime = sdf.format(date);
        String printString = "";
        try {
            printString += "0E0B12" + UnicodeFormatter.stringToHex("선충전금 충전내역 (카드)", "euc-kr") + "0A";

            printString += "0E0B12" + UnicodeFormatter.stringToHex("=======================", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("결제일자 : " + getTime, "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("고객명 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName(), "euc-kr") + "140A";
            printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserPhone(), "euc-kr") + "140A";
            printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserAddress(), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("------------------------------------------------", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F0B13" + UnicodeFormatter.stringToHex("충전(결제) 금액 : " + DataFormatUtil.moneyFormatToWon(mPaymentInfo.get(0).getData().getPaymentPrice()), "euc-kr") + "0A";
            printString += "0F13" + UnicodeFormatter.stringToHex("추가적립금액 : " + DataFormatUtil.moneyFormatToWon(mPaymentInfo.get(0).getData().getSaveMileage()), "euc-kr") + "0A";
            printString += "0F13" + UnicodeFormatter.stringToHex("총 충전금액 : " + DataFormatUtil.moneyFormatToWon(mPaymentInfo.get(0).getData().getPaymentPrice() + mPaymentInfo.get(0).getData().getSaveMileage()), "euc-kr") + "0A";
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("------------------------------------------------", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F0B12" + UnicodeFormatter.stringToHex("신용카드 매출전표", "euc-kr") + "0A";
            printString += "0F0B12" + UnicodeFormatter.stringToHex("=======================", "euc-kr") + "0A";
            printString += "0F14" + UnicodeFormatter.stringToHex("[카드번호] : " + mPaymentInfo.get(0).getData().getPaymentDetails().get(0).getCardNo(), "euc-kr") + "0A";
            printString += "0F14" + UnicodeFormatter.stringToHex("[할부개월] : " + mPaymentInfo.get(0).getData().getPaymentDetails().get(0).getCardDivide(), "euc-kr") + "0A";
            printString += "0F14" + UnicodeFormatter.stringToHex("[카 드 명] : " + mPaymentInfo.get(0).getData().getPaymentDetails().get(0).getCardName(), "euc-kr") + "0A";
            printString += "0F14" + UnicodeFormatter.stringToHex("[승인번호] : " + mPaymentInfo.get(0).getData().getPaymentDetails().get(0).getTID(), "euc-kr") + "0A";
            printString += "0F14" + UnicodeFormatter.stringToHex("[승인일시] : " + mPaymentInfo.get(0).getData().getPaymentDetails().get(0).getPayYMD(), "euc-kr") + "0A";
            printString += "0F14" + UnicodeFormatter.stringToHex("[승인금액] : " + mPaymentInfo.get(0).getData().getPaymentDetails().get(0).getPrice(), "euc-kr") + "0A";
            printString += "0F14" + UnicodeFormatter.stringToHex("[매입사명] : " + mPaymentInfo.get(0).getData().getPaymentDetails().get(0).getCardName(), "euc-kr") + "0A";
            printString += "0F14" + UnicodeFormatter.stringToHex("[가맹번호] : " + mPaymentInfo.get(0).getData().getPaymentDetails().get(0).getPGID(), "euc-kr") + "0A";

            printString += UnicodeFormatter.stringToHex("────────────────────────", "euc-kr") + "0A";
            printString += "0A";
//                printString += "0E0B12" + UnicodeFormatter.stringToHex(ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().getStoreName(), "euc-kr") + "0A";
//                printString += "0A";
//                printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_MasterPhone(), "euc-kr") + "0A";
//                printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_StoreAddress2(), "euc-kr") + "0A";
//                printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getStoreGreetingText(), "euc-kr") + "0A";

            printString += "11";
            printStringExcute(printString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(iRet);
    }


    /**
     * 접수증 재인쇄
     *
     * @param getPaymentInfoItems
     * @return
     */
    public static String getReissueOfReceipt(List<ManagerGetPaymentInfoModel> getPaymentInfoItems) {
        try {
            String printString = "";
            final String[] tacNumber = new String[1];
            int payPrice = 0;
            int preFilledGold = 0;
            int prePaymentGold = 0;
            int prePaymentCard = 0;

            int saveMileage = 0;
            int paymentCardType = 0;

            String CardNo = null;
            int CardDivide = 0;
            String PayYMD = null;
            String CardName = null;
            String CATInfo = null;
            String TID = null;

            printString += "0E0B12" + UnicodeFormatter.stringToHex("세탁물 접수증 (재발행)", "euc-kr") + "0A";
            printString += UnicodeFormatter.stringToHex("==================", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("접수일자 : " + DataFormatUtil.setDateItemCancelFormat2(getPaymentInfoItems.get(0).getOrderItems().get(0).getEnterDate()), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("세탁완료예정일 : " + DataFormatUtil.setDateMothFormat(getPaymentInfoItems.get(0).getOrderItems().get(0).getWashFinWillDate()), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("고객명 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName(), "euc-kr") + "140A";
            printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserPhone(), "euc-kr") + "140A";
            printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserAddress(), "euc-kr") + "0A";

            printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
            printString += UnicodeFormatter.stringJustifyToHex("택번호\t\t" + " 품목\t\t    ", "금액", 9) + "0A";
            printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";

            for (GetPaymentInfoItem.OrderItem getPaymentInfoItem : getPaymentInfoItems.get(0).getOrderItems()) {

                Logger.d(TAG, "getPaymentInfoItem : " + getPaymentInfoItem.getTagNo());
                int idx = getPaymentInfoItem.getTagNo().indexOf("|");
                if (idx == 0) {
                    tacNumber[0] = CommonUtil.setTagFormater(getPaymentInfoItem.getTagNo());
                } else if (getPaymentInfoItem.getTagNo().length() >= 6 && getPaymentInfoItem.getTagNo().length() < 9) {
                    tacNumber[0] = CommonUtil.setFormater(getPaymentInfoItem.getTagNo());
                } else if (getPaymentInfoItem.getTagNo().contains("-")) {
                    tacNumber[0] = CommonUtil.setTagFormaterChange(getPaymentInfoItem.getTagNo());
                }


//                Logger.d(TAG, "getPaymentInfoItem.getOptions().get(0).getOptionItemName() : " + getPaymentInfoItem.getOptions().get(0).getOptionItemName());
//                printString += UnicodeFormatter.stringJustifyToHex(tacNumber[0] + "\t\t" + getPaymentInfoItem.getOptions().get(0).getOptionItemName() + "\t\t",
//                        String.valueOf(getPaymentInfoItem.getOptions().get(0).getOptionPrice()), 5) + "0A";

//                for (int j = 0; j < getPaymentInfoItem.getOptions().size(); j++) {
//                    printString += UnicodeFormatter.stringJustifyToHex("     \t\t" + getPaymentInfoItem.getOptions().get(j).getOptionItemName() + "\t\t", String.valueOf(getPaymentInfoItem.getOptions().get(j).getOptionPrice()), 5) + "0A";
//                }
            }

            printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
            for (ManagerGetPaymentInfoModel getPaymentInfoItem : getPaymentInfoItems) {
                for (GetPaymentInfoItem.PaymentDetail paymentDetail : getPaymentInfoItem.getPaymentDetails()) {
                    if (paymentDetail.getPaymentType() == 1) {
                        prePaymentGold = paymentDetail.getPrice();
                    } else if (paymentDetail.getPaymentType() == 2) {
                        prePaymentCard = paymentDetail.getPrice();

                        CardNo = paymentDetail.getCardNo();
                        CardDivide = paymentDetail.getCardDivide();
                        PayYMD = paymentDetail.getPayYMD();
                        CardName = paymentDetail.getCardName();
                        CATInfo = paymentDetail.getCATInfo();
                        TID = paymentDetail.getTID();
                    } else if (paymentDetail.getPaymentType() == 3) {
                        preFilledGold = paymentDetail.getPrice();
                    } else if (paymentDetail.getPaymentType() == 4) {
                        saveMileage = paymentDetail.getPrice();
                    }
                }
            }

            printString += UnicodeFormatter.stringJustifyToHex("총 수량 :" + getPaymentInfoItems.size(), "총 금액 :" + DataFormatUtil.moneyFormatToWon(payPrice), 44);
            printString += "0A";

            printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
            printString += UnicodeFormatter.stringToHex("■결제 정보■", "euc-kr") + "0A";

            printString += UnicodeFormatter.stringJustifyToHex("선충전금 : " + preFilledGold + "\t", "마일리지 : " + saveMileage, 30) + "0A";
            printString += UnicodeFormatter.stringJustifyToHex("현    금 : " + prePaymentGold + "\t", "카    드 : " + prePaymentCard, 30) + "0A";
            printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
            printString += "0A";

            for (GetPaymentInfoItem.PaymentDetail paymentDetail : getPaymentInfoItems.get(0).getPaymentDetails()) {
                if (paymentDetail.getPaymentType() == 2) {
                    paymentCardType = 2;
                }
            }

            if (paymentCardType == 2) {
                printString += "0F12" + UnicodeFormatter.stringToHex("신용카드 매출전표", "euc-kr") + "0A";
                printString += "0F12" + UnicodeFormatter.stringToHex("=======================", "euc-kr") + "0A";
                printString += "0F14" + UnicodeFormatter.stringToHex(" [카드번호] : " + CardNo, "euc-kr") + "0A";
                printString += "0F14" + UnicodeFormatter.stringToHex(" [할부개월] : " + CardDivide, "euc-kr") + "0A";
                printString += "0F14" + UnicodeFormatter.stringToHex(" [카 드 명] : " + CardName, "euc-kr") + "0A";
                printString += "0F14" + UnicodeFormatter.stringToHex(" [승인번호] : " + TID, "euc-kr") + "0A";
                printString += "0F14" + UnicodeFormatter.stringToHex(" [승인일시] : " + PayYMD, "euc-kr") + "0A";
                printString += "0F14" + UnicodeFormatter.stringToHex(" [승인금액] : " + prePaymentCard, "euc-kr") + "0A";
//                printString += "0F14" + UnicodeFormatter.stringToHex(" [매입사명] : " + acquirerName, "euc-kr") + "0A";
//                printString += "0F14" + UnicodeFormatter.stringToHex(" [가맹번호] : " + merchantNo, "euc-kr") + "0A";
            }

            printString += "0A";
            printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("■미수금 정보■", "euc-kr") + "0A";
            printString += "0A";
            printString += UnicodeFormatter.stringToHex("총 미수금 : " + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getNonPayPrice()), "euc-kr") + "0A";
            printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("■" + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName() + "님의 잔액■", "euc-kr") + "0A";
            printString += "0A";
            printString += UnicodeFormatter.stringJustifyToHex("선충전금 : " + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getTotalPrePaid()),
                    "마일리지 :" + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getMileage()), 44);
            printString += "0A";
            printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
            printString += "0A";
            if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintDamageNotice().equals("Y")) {
                printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getDamageNotice(), "euc-kr") + "0A";
                printString += "0A";
                printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                printString += "0A";
            }
            if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintStorageNotice().equals("Y")) {
                printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getStorageNotice(), "euc-kr") + "0A";
                printString += "0A";
                printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                printString += "0A";
            }
            printString += "0E0B12" + UnicodeFormatter.stringToHex(ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().getStoreName(), "euc-kr") + "0A";
            printString += "0A";

            printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_MasterPhone(), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_StoreAddress2(), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getStoreGreetingText(), "euc-kr") + "0A";

            printString += "11";

            printStringExcute(printString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return paymentDate;
    }


    /**
     * 접수증 재인쇄 type2
     * 결제 정보 없는 경우
     *
     * @return
     */
    public static String getReissueOfReceiptType(List<GetOrderItemList> getOrderItemLists) {
        String printStatus = null;
        try {
            String printString = "";
            final String[] tacNumber = new String[1];
            int payPrice = 0;
            int payNonPrice = 0;
            int payTechnical = 0;
            int payRepair = 0;

            printString += "0E0B12" + UnicodeFormatter.stringToHex("세탁물 접수증 (재발행)", "euc-kr") + "0A";
            printString += UnicodeFormatter.stringToHex("==================", "euc-kr") + "0A";
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("접수일자 : " + DataFormatUtil.setDateItemCancelFormat2(getOrderItemLists.get(0).getEnterDate()), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("세탁완료예정일 : " + DataFormatUtil.setDateMothFormat(getOrderItemLists.get(0).getWashFinWillDate()), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("고객명 : " + getOrderItemLists.get(0).getUserName(), "euc-kr") + "140A";
            printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + getOrderItemLists.get(0).get_UserPhone(), "euc-kr") + "140A";
            printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + getOrderItemLists.get(0).get_UserAddress(), "euc-kr") + "0A";

            printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
            printString += "0F14" + UnicodeFormatter.stringToHex(String.format("%6s", "택번호") + "    ", "euc-kr") + "";
            printString += "0F12" + UnicodeFormatter.stringToHex("품목\t\t\t", "euc-kr") + "";
            printString += "0F12" + UnicodeFormatter.stringToHex("금액", "euc-kr") + "0A";

//            printString += UnicodeFormatter.stringJustifyToHex("택번호\t" + "품목\t\t\t   ", "금액", 9) + "0A";
//            printString += UnicodeFormatter.stringJustifyToHex("택번호\t" + "품목\t\t\t   ", "금액", 9) + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";

            for (GetOrderItemList getOrderItemList : getOrderItemLists) {
                payPrice += getOrderItemList.getOrderItemPrice();
                payNonPrice += getOrderItemList.getNonPayPrice();

                int idx = getOrderItemList.getTagNo().indexOf("|");
                if (idx == 0) {
                    tacNumber[0] = CommonUtil.setTagFormater(getOrderItemList.getTagNo());
                } else if (getOrderItemList.getTagNo().length() >= 6 && getOrderItemList.getTagNo().length() < 9) {
                    tacNumber[0] = CommonUtil.setFormater(getOrderItemList.getTagNo());
                } else if (getOrderItemList.getTagNo().contains("-")) {
                    tacNumber[0] = CommonUtil.setTagFormaterChange(getOrderItemList.getTagNo());
                }
                //Type.0
//                printString += "0F14" + UnicodeFormatter.stringToHex(String.format("%8s", tacNumber[0]) + "    ", "euc-kr") + "";
//                printString += "0F14" + UnicodeFormatter.stringToHex(getOrderItemList.getStoreItemName() + "              ", "euc-kr");
//                printString += "0F13" + UnicodeFormatter.stringToHex(getOrderItemList.getOrderItemPrice() + "", "euc-kr") + "0A";
                //Type.1
//                printString += "0F12" + UnicodeFormatter.stringJustifyToHex(String.format("%14s", getOrderItemList.getStoreItemName()), DataFormatUtil.moneyFormatToWon(getOrderItemList.getOrderItemPrice()), 36) + "0A";

                //Type.2
//                printString += "0F12" + UnicodeFormatter.stringToHex(getOrderItemList.getStoreItemName() + "\t       ", "euc-kr") + "";
//                printString += "0F12" + UnicodeFormatter.stringToHex(DataFormatUtil.moneyFormatToWon(getOrderItemList.getOrderItemPrice()), "euc-kr") + "0A";

                //Type.3
                printString += UnicodeFormatter.stringToHex(tacNumber[0] + "\t  ", "euc-kr");
                printString += "0F12" + UnicodeFormatter.stringJustifyToHex(getOrderItemList.getStoreItemName(), DataFormatUtil.moneyFormatToWon(getOrderItemList.getOrderItemPrice()), 32) + "0A";
//                printString += "0F12" + UnicodeFormatter.stringJustifyToHex(String.format("%8s", tacNumber[0]), String.format("%s%20s", getOrderItemList.getStoreItemName(), getOrderItemList.getOrderItemPrice()), 14) + "0A";
//                printString += "0F12" + UnicodeFormatter.stringJustifyToHex(String.format("%8s", tacNumber[0]), String.format("%s%10s", getOrderItemList.getStoreItemName(), getOrderItemList.getOrderItemPrice()), 14) + "0A";


                for (int j = 0; j < getOrderItemList.getOptions().size(); j++) {
                    if (getOrderItemList.getOptions().get(j).getOptionType() == 1) {
                        printString += "0F12" + UnicodeFormatter.stringJustifyToHex(String.format("%16.5s%s", "+기술 : ", getOrderItemList.getOptions().get(j).getOptionItemName()), "(" + DataFormatUtil.moneyFormatToWon(getOrderItemList.getOptions().get(j).getOptionPrice()) + ")", 43) + "0A";
                    } else if (getOrderItemList.getOptions().get(j).getOptionType() == 2) {
                        printString += "0F12" + UnicodeFormatter.stringJustifyToHex(String.format("%16.5s%s", "+수선 : ", getOrderItemList.getOptions().get(j).getOptionItemName()), "(" + DataFormatUtil.moneyFormatToWon(getOrderItemList.getOptions().get(j).getOptionPrice()) + ")", 43) + "0A";
                    } else {
                        printString += "0F12" + UnicodeFormatter.stringJustifyToHex(String.format("%16.5s%s", "+부속 : ", getOrderItemList.getOptions().get(j).getOptionItemName()), "(" + DataFormatUtil.moneyFormatToWon(getOrderItemList.getOptions().get(j).getOptionPrice()) + ")", 43) + "0A";
                    }

                }
                printString += "0A";
            }

            printString += "0F12" + UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringJustifyToHex("총 수량 :" + getOrderItemLists.size(), "총 금액 :" + DataFormatUtil.moneyFormatToWon(payPrice), 44);
            printString += "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("■미수금 정보■", "euc-kr") + "0A";
            printString += "0A";
            printString += UnicodeFormatter.stringToHex("총 미수금 : " + DataFormatUtil.moneyFormatToWon(payNonPrice), "euc-kr") + "0A";
            printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("■" + getOrderItemLists.get(0).getUserName() + "님의 잔액■", "euc-kr") + "0A";
            printString += "0A";
            printString += UnicodeFormatter.stringJustifyToHex("선충전금 : " + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getTotalPrePaid()),
                    "마일리지 :" + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getMileage()), 44);
            printString += "0A";
            printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
            printString += "0A";
            if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintDamageNotice().equals("Y")) {
                printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getDamageNotice(), "euc-kr") + "0A";
                printString += "0A";
                printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                printString += "0A";
            }
            if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintStorageNotice().equals("Y")) {
                printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getStorageNotice(), "euc-kr") + "0A";
                printString += "0A";
                printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                printString += "0A";
            }
            printString += "0E0B12" + UnicodeFormatter.stringToHex(ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().getStoreName(), "euc-kr") + "0A";
            printString += "0A";

            printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + getOrderItemLists.get(0).get_UserPhone(), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + getOrderItemLists.get(0).get_UserAddress(), "euc-kr") + "0A";
            printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getStoreGreetingText(), "euc-kr") + "0A";

            printString += "11";

            printStatus = printExcute(printString);

        } catch (Exception e) {

            e.printStackTrace();
        }

        Logger.d(TAG, "printStatus : " + printStatus);
        return printStatus;
    }

    /**
     * 현금 결제(현금영수증) 연동 로직
     *
     * @param activity
     * @param paymentMoney           총 금액
     * @param orderItemId            주문 ID
     * @param preFilledGoldet        선충전금
     * @param mileagePaymentAllUseet 마일리지
     * @return
     * @throws Exception
     */
    public static String getPaymentDataPrint(AppCompatActivity activity, int paymentMoney, String orderItemId, int preFilledGoldet, int mileagePaymentAllUseet, String cashReceipts) throws Exception {
        final String[] status = {null};
        final String[] regCashReceipt = new String[1];
        int iProcID = 0;
        int iRet = 0;

        int taxAmount = Math.round(paymentMoney / 11);
        int taxableAmount = paymentMoney - taxAmount;

        StringBuffer strMsg = new StringBuffer();

        if (cashReceipts.equals("Y")) {
            iProcID = FDK_Module.Creat();
            //            FDK_Module.Input(iProcID, "Business Number", ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_BusinessRegistrationNumber());                // 사업자번호
            //TODO 등록된 사업자가 아니기 때문에 TEST강제 하드 코딩 배포시 위에 주석 해지..
            FDK_Module.Input(iProcID, "Business Number", "2268138295");                // 사업자번호
            FDK_Module.Input(iProcID, "Transaction Type", "00");                       // 거래 종류
            FDK_Module.Input(iProcID, "Transaction Amount", String.valueOf(paymentMoney)); // 결제금액
            FDK_Module.Input(iProcID, "Tax Amount", String.valueOf(taxAmount));// 세금
            FDK_Module.Input(iProcID, "Taxable Amount", String.valueOf(taxableAmount));   // 과세
            FDK_Module.Input(iProcID, "Non-Taxable Amount", "0");
            FDK_Module.Input(iProcID, "Receipt Print Flag", "2");

            iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CashReceiptAuth_K6");
        }
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
        if (0 == iRet) {
            FDK_Module.Output(iProcID, "Response Code");
            if (0 == iRet || -1000 == iRet) {

                int finalIProcID = iProcID;
                String AuthorizationDat = FDK_Module.Output(finalIProcID, "Cash Receipt Authorization Number");

                new Thread(new Runnable() {
                    public void run() {
                        //현금결제
                        try {
                            regCashReceipt[0] = PaymentManager.getRegCashReceiptID(activity, ManagerItemPaymentStoreOrder.getmInstance().getItemAdditionalData().get(0).getData().getPaymentID(), AuthorizationDat
                                    , ManagerItemPaymentStoreOrder.getmInstance().getItemAdditionalData().get(0).getData().getExReceiptPrice());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        ManagerGetSelectOrderItemList managerGetSelectOrderItemList = VisitReceptionManager.getSelectOrderItemList(activity, orderItemId);

                        activity.runOnUiThread(new Runnable() {
                            public void run() {

                                if (regCashReceipt[0].equals("") || TextUtils.isEmpty(regCashReceipt[0])) {
                                    thirdAlertPopup.setTitle("결제정보 전송오류");
                                    thirdAlertPopup.setContent("결제정보 전송중 오류 발생했어요. \n 고객센터로 문의해주세요.");
                                    thirdAlertPopup.show();
                                    return;
                                }

                                if (regCashReceipt[0].equals("S0000")) {
                                    try {
                                        int payPrice = 0;
                                        String printString = "";
                                        List<GetSelectOrderItemList> getSelectOrderItemLists = managerGetSelectOrderItemList.getSelectOrderItemList();
                                        printString += "0E0B12" + UnicodeFormatter.stringToHex("세탁물 접수증", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringToHex("==================", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("접수일자 : " + getSelectOrderItemLists.get(0).getEnterDate(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("세탁완료예정일 : " + getSelectOrderItemLists.get(0).getWashFinWillDate(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("고객명 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName(), "euc-kr") + "140A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserPhone(), "euc-kr") + "140A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserAddress(), "euc-kr") + "0A";

                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("택번호\t\t" + " 품목\t\t    ", "금액", 9) + "0A";
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";

                                        for (int i = 0; i < getSelectOrderItemLists.size(); i++) {
                                            printString += UnicodeFormatter.stringJustifyToHex(getSelectOrderItemLists.get(i).getTagNo() + "\t\t" + getSelectOrderItemLists.get(i).getOptions().get(0).getOptionItemName() + "\t\t",
                                                    String.valueOf(getSelectOrderItemLists.get(i).getOptions().get(0).getOptionPrice()), 5) + "0A";

                                            for (int j = 0; j < getSelectOrderItemLists.get(i).getOptions().size(); j++) {
                                                printString += UnicodeFormatter.stringJustifyToHex("     \t\t" + getSelectOrderItemLists.get(i).getOptions().get(j).getOptionItemName() + "\t\t", String.valueOf(getSelectOrderItemLists.get(i).getOptions().get(j).getOptionPrice()), 5) + "0A";
                                            }
                                        }
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                        for (GetSelectOrderItemList getSelectOrderItemList : getSelectOrderItemLists) {
                                            payPrice += getSelectOrderItemList.getPayPrice();
                                        }
                                        printString += UnicodeFormatter.stringJustifyToHex("총 수량 :" + getSelectOrderItemLists.size(), "총 금액 :" + DataFormatUtil.moneyFormatToWon(payPrice), 44);
                                        printString += "0A";

                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringToHex("■결제 정보■", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("선충전금 : " + preFilledGoldet + "\t", "마일리지 : " + mileagePaymentAllUseet, 40) + "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("현    금 : " + paymentMoney + "\t", "카    드 : 0", 40) + "0A";
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("■미수금 정보■", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringToHex("총 미수금 : " + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getNonPayPrice()), "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("■" + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName() + "님의 잔액■", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("선충전금 : " + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getTotalPrePaid()),
                                                "마일리지 :" + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getMileage()), 44);
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += "0A";
                                        Logger.d(TAG, "ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintDamageNotice() : " + ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintDamageNotice());
                                        if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintDamageNotice().equals("Y")) {
                                            printString += "0F14" + UnicodeFormatter.stringToHex(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getDamageNotice(), "euc-kr") + "0A";
                                            printString += "0A";
                                            printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                            printString += "0A";
                                        }
                                        Logger.d(TAG, "ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintStorageNotice() : " + ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintStorageNotice());
                                        if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintStorageNotice().equals("Y")) {
                                            printString += "0F14" + UnicodeFormatter.stringToHex(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getStorageNotice(), "euc-kr") + "0A";
                                            printString += "0A";
                                            printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                            printString += "0A";
                                        }
                                        printString += "0E0B12" + UnicodeFormatter.stringToHex(ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().getStoreName(), "euc-kr") + "0A";
                                        printString += "0A";

                                        printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_MasterPhone(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_StoreAddress2(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getStoreGreetingText().toString(), "euc-kr") + "0A";

                                        printString += "11";

                                        printStringExcute(printString);
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    thirdAlertPopup.setTitle("결제정보 전송오류");
                                    thirdAlertPopup.setContent("결제정보 전송중 오류 발생했어요. \n 고객센터로 문의해주세요.");
                                    thirdAlertPopup.show();
                                }
                            }
                        });
                    }
                }).start();

            } else if (-1000 == iRet) {
                thirdAlertPopup.setTitle("연결실패");
                thirdAlertPopup.setContent("카드 단말기 연결확인");
                thirdAlertPopup.show();
                FDK_Module.Output(iProcID, "Response Code");
            } else {
                FDK_Module.Output(iProcID, "ErrorInfo");
            }
        }
        return String.valueOf(iRet);
    }

    /**
     * FirstData (카드)결제 연동 로직
     *
     * @param activity
     * @param paymentMoney           총 금액
     * @param orderItemId            주문 ID
     * @param itemCount              총 주문 카운트
     * @param preFilledGoldet        선충전금
     * @param mileagePaymentAllUseet 마일리지
     * @return
     * @throws Exception
     */
    public static String getCardPaymentDataPrint(AppCompatActivity activity, int paymentMoney, String orderItemId, int itemCount, int preFilledGoldet, int mileagePaymentAllUseet) throws Exception {
        final String[] status = {null};

        int iProcID = 0;
        int iRet = 0;

        final String[] tacNumber = new String[1];
        String strData = "";
        int taxation = Math.round(Integer.valueOf(paymentMoney) / 11);
        Math.round(Double.valueOf(paymentMoney) - taxation);

        StringBuffer strMsg = new StringBuffer();
        iProcID = FDK_Module.Creat();
        //            FDK_Module.Input(iProcID, "Business Number", ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_BusinessRegistrationNumber());                // 사업자번호
        //TODO 등록된 사업자가 아니기 때문에 TEST강제 하드 코딩 배포시 위에 주석 해지..
        FDK_Module.Input(iProcID, "Business Number", "2268138295");                // 사업자번호
        FDK_Module.Input(iProcID, "Transaction Amount", String.valueOf(paymentMoney));   // 결제금액
        FDK_Module.Input(iProcID, "Tax Amount", String.valueOf(Math.round(Integer.valueOf(paymentMoney) - taxation)));                              // 세금
        FDK_Module.Input(iProcID, "Taxable Amount", String.valueOf(taxation));                         // 과세
        FDK_Module.Input(iProcID, "Non-Taxable Amount", "0");
        FDK_Module.Input(iProcID, "Receipt Print Flag", "2");

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CreditAuth_D6");

        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
        if (0 == iRet) {
            FDK_Module.Output(iProcID, "Response Code");

            if (0 == iRet || -1000 == iRet) {
                strMsg.append("STX : " + FDK_Module.Output(iProcID, "STX"));
                strMsg.append("Work Type : " + FDK_Module.Output(iProcID, "Work Type"));
                strMsg.append("FS1 : " + FDK_Module.Output(iProcID, "FS1"));
                strMsg.append("Response Code : " + FDK_Module.Output(iProcID, "Response Code"));
                strMsg.append("FS2 : " + FDK_Module.Output(iProcID, "FS2"));
                strMsg.append("Business Number : " + FDK_Module.Output(iProcID, "Business Number"));
                strMsg.append("FS3 : " + FDK_Module.Output(iProcID, "FS3"));
                strMsg.append("CAT ID : " + FDK_Module.Output(iProcID, "CAT ID"));
                strMsg.append("FS4 : " + FDK_Module.Output(iProcID, "FS4"));
                strMsg.append("Representative Name : " + FDK_Module.Output(iProcID, "Representative Name"));
                strMsg.append("FS5 : " + FDK_Module.Output(iProcID, "FS5"));
                strMsg.append("Merchant Name : " + FDK_Module.Output(iProcID, "Merchant Name"));
                strMsg.append("FS6 : " + FDK_Module.Output(iProcID, "FS6"));
                strMsg.append("Merchant Address : " + FDK_Module.Output(iProcID, "Merchant Address"));
                strMsg.append("FS7 : " + FDK_Module.Output(iProcID, "FS7"));
                strMsg.append("Merchant Phone Number : " + FDK_Module.Output(iProcID, "Merchant Phone Number"));
                strMsg.append("FS8 : " + FDK_Module.Output(iProcID, "FS8"));
                strMsg.append("Agent Phone Number : " + FDK_Module.Output(iProcID, "Agent Phone Number"));
                strMsg.append("FS9 : " + FDK_Module.Output(iProcID, "FS9"));
                strMsg.append("ETX : " + FDK_Module.Output(iProcID, "ETX"));
                strMsg.append("LRC : " + FDK_Module.Output(iProcID, "LRC"));

                // 카드이름
                cardName = FDK_Module.Output(iProcID, "Issuer Name");
                // 거래일시
                paymentDate = FDK_Module.Output(iProcID, "Authorization Date");

                // 카드번호
                String cardNo = FDK_Module.Output(iProcID, "Card Data");
                maskingCardNo = cardNo.substring(0, 4) + "-" + cardNo.substring(4, 5) + "**-****-****";

                // 유효기간(년/월):**/**
                validPeriod = FDK_Module.Output(iProcID, "Installment Period"); // 값이 없을경우 일시불
                if (validPeriod.length() == 0) {
                    validPeriod = "일시불";
                    validPeriodNumber = 0;
                } else {
                    validPeriod = "할부" + validPeriod + "개월";
                    validPeriodNumber = Integer.valueOf(validPeriod);
                }
                // 가맹점번호
                merchantNo = FDK_Module.Output(iProcID, "Merchant Number");
                // 승인번호
                authorizationNo = FDK_Module.Output(iProcID, "Authorization Number");
                // 매입사
                acquirerName = FDK_Module.Output(iProcID, "Acquirer Name");
                // 무서명거래
                String notice = FDK_Module.Output(iProcID, "Notice");
                new Thread(new Runnable() {
                    public void run() {
                        status[0] = PaymentManager.getCardPaymentStoreOrderItem(activity, orderItemId, itemCount, 0, paymentMoney, preFilledGoldet, mileagePaymentAllUseet,
                                authorizationNo, strMsg.toString(), cardName, maskingCardNo, validPeriodNumber);
                        ManagerGetSelectOrderItemList managerGetSelectOrderItemList = VisitReceptionManager.getSelectOrderItemList(activity, orderItemId);

                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                if (status[0].equals("S0000")) {
                                    try {
                                        int payPrice = 0;
                                        String printString = "";
                                        List<GetSelectOrderItemList> getSelectOrderItemLists = managerGetSelectOrderItemList.getSelectOrderItemList();
                                        printString += "0E0B12" + UnicodeFormatter.stringToHex("세탁물 접수증", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringToHex("==================", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("접수일자 : " + getSelectOrderItemLists.get(0).getEnterDate(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("세탁완료예정일 : " + getSelectOrderItemLists.get(0).getWashFinWillDate(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("고객명 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName(), "euc-kr") + "140A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserPhone(), "euc-kr") + "140A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserAddress(), "euc-kr") + "0A";

                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("택번호\t\t" + " 품목\t\t    ", "금액", 9) + "0A";
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";

                                        for (int i = 0; i < getSelectOrderItemLists.size(); i++) {

                                            int idx = getSelectOrderItemLists.get(i).getTagNo().indexOf("|");
                                            if (idx == 0) {
                                                tacNumber[0] = CommonUtil.setTagFormater(getSelectOrderItemLists.get(i).getTagNo());
                                            } else if (getSelectOrderItemLists.get(i).getTagNo().length() >= 6 && getSelectOrderItemLists.get(i).getTagNo().length() < 9) {
                                                tacNumber[0] = CommonUtil.setFormater(getSelectOrderItemLists.get(i).getTagNo());
                                            } else if (getSelectOrderItemLists.get(i).getTagNo().contains("-")) {
                                                tacNumber[0] = CommonUtil.setTagFormaterChange(getSelectOrderItemLists.get(i).getTagNo());
                                            }

                                            printString += UnicodeFormatter.stringJustifyToHex(tacNumber[0] + "\t\t" + getSelectOrderItemLists.get(i).getOptions().get(0).getOptionItemName() + "\t\t",
                                                    String.valueOf(getSelectOrderItemLists.get(i).getOptions().get(0).getOptionPrice()), 5) + "0A";

                                            for (int j = 0; j < getSelectOrderItemLists.get(i).getOptions().size(); j++) {
                                                printString += UnicodeFormatter.stringJustifyToHex("     \t\t" + getSelectOrderItemLists.get(i).getOptions().get(j).getOptionItemName() + "\t\t", String.valueOf(getSelectOrderItemLists.get(i).getOptions().get(j).getOptionPrice()), 5) + "0A";
                                            }
                                        }
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                        for (GetSelectOrderItemList getSelectOrderItemList : getSelectOrderItemLists) {
                                            payPrice += getSelectOrderItemList.getPayPrice();
                                        }
                                        printString += UnicodeFormatter.stringJustifyToHex("총 수량 :" + getSelectOrderItemLists.size(), "총 금액 :" + DataFormatUtil.moneyFormatToWon(payPrice), 44);
                                        printString += "0A";

                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringToHex("■결제 정보■", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("선충전금 : " + preFilledGoldet + "\t", "마일리지 : " + mileagePaymentAllUseet, 30) + "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("현    금 : " + "0\t", "카    드 : " + paymentMoney, 30) + "0A";
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("신용카드 매출전표", "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("=======================", "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [카드번호] : " + maskingCardNo, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [할부개월] : " + validPeriod, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [카 드 명] : " + cardName, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [승인번호] : " + authorizationNo, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [승인일시] : " + paymentDate, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [승인금액] : " + paymentMoney, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [매입사명] : " + acquirerName, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [가맹번호] : " + merchantNo, "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("■미수금 정보■", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringToHex("총 미수금 : " + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getNonPayPrice()), "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("■" + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName() + "님의 잔액■", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("선충전금 : " + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getTotalPrePaid()),
                                                "마일리지 :" + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getMileage()), 44);
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += "0A";
                                        if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintDamageNotice().equals("Y")) {
                                            printString += "0F14" + UnicodeFormatter.stringToHex(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getDamageNotice(), "euc-kr") + "0A";
                                            printString += "0A";
                                            printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                            printString += "0A";
                                        }
                                        if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintStorageNotice().equals("Y")) {
                                            printString += "0F14" + UnicodeFormatter.stringToHex(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getStorageNotice(), "euc-kr") + "0A";
                                            printString += "0A";
                                            printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                            printString += "0A";
                                        }
                                        printString += "0E0B12" + UnicodeFormatter.stringToHex(ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().getStoreName(), "euc-kr") + "0A";
                                        printString += "0A";

                                        printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_MasterPhone(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_StoreAddress2(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getStoreGreetingText(), "euc-kr") + "0A";

                                        printString += "11";

                                        printStringExcute(printString);
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    thirdAlertPopup.setTitle("결제정보 전송오류");
                                    thirdAlertPopup.setContent("결제정보 전송중 오류 발생했어요. \n 고객센터로 문의해주세요.");
                                    thirdAlertPopup.show();
                                }
                            }
                        });
                    }
                }).start();

            } else if (-1000 == iRet) {
                thirdAlertPopup.setTitle("연결실패");
                thirdAlertPopup.setContent("카드 단말기 연결확인");
                thirdAlertPopup.show();
                FDK_Module.Output(iProcID, "Response Code");
            } else {
                FDK_Module.Output(iProcID, "ErrorInfo");
            }
        }
        return String.valueOf(iRet);
    }

    /**
     * FirstData (현금 + 카드) 결제 연동 로직
     *
     * @param activity               View
     * @param orderItemId            주문 ID
     * @param itemCount              총 주문 카운트
     * @param paymentMoney           현금결제금액
     * @param cardPaymentMoney       카드결제금액
     * @param preFilledGoldet        선충전금
     * @param mileagePaymentAllUseet 마일리지
     * @param mileagePaymentAllUseet 마일리지
     * @param mileagePaymentAllUseet 마일리지
     * @param cashReceipts           현금영수증 발행
     * @return
     * @throws Exception
     */
    public static String getCompoundPayment(AppCompatActivity activity, int paymentMoney, int cardPaymentMoney, String orderItemId, int itemCount, int preFilledGoldet, int mileagePaymentAllUseet, String cashReceipts) throws Exception {
        final String[] status = {null};

        int iProcID = 0;
        int cardIRet = 0;
        int payIRet = 0;

        String strData = "";
        int taxCardAmount = Math.round(cardPaymentMoney / 11);
        int taxableCardAmount = cardPaymentMoney - taxCardAmount;

        StringBuffer strMsg = new StringBuffer();
        iProcID = FDK_Module.Creat();
        //            FDK_Module.Input(iProcID, "Business Number", ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_BusinessRegistrationNumber());                // 사업자번호
        //TODO 등록된 사업자가 아니기 때문에 TEST강제 하드 코딩 배포시 위에 주석 해지..
        FDK_Module.Input(iProcID, "Business Number", "2268138295");                // 사업자번호
        FDK_Module.Input(iProcID, "Transaction Amount", String.valueOf(cardPaymentMoney));   // 결제금액
        FDK_Module.Input(iProcID, "Tax Amount", String.valueOf(taxCardAmount));                              // 세금
        FDK_Module.Input(iProcID, "Taxable Amount", String.valueOf(taxableCardAmount));                         // 과세
        FDK_Module.Input(iProcID, "Non-Taxable Amount", "0");
        FDK_Module.Input(iProcID, "Receipt Print Flag", "2");

        cardIRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CreditAuth_D6");

        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
        if (cardIRet == 0) {
            FDK_Module.Output(iProcID, "Response Code");
            // 카드이름
            cardName = FDK_Module.Output(iProcID, "Issuer Name");
            // 거래일시
            paymentDate = FDK_Module.Output(iProcID, "Authorization Date");

            // 카드번호
            String cardNo = FDK_Module.Output(iProcID, "Card Data");
            maskingCardNo = cardNo.substring(0, 4) + "-" + cardNo.substring(4, 5) + "**-****-****";

            // 유효기간(년/월):**/**
            validPeriod = FDK_Module.Output(iProcID, "Installment Period"); // 값이 없을경우 일시불
            if (validPeriod.length() == 0) {
                validPeriod = "일시불";
                validPeriodNumber = 0;
            } else {
                validPeriod = "할부" + validPeriod + "개월";
                validPeriodNumber = Integer.valueOf(validPeriod);
            }
            // 가맹점번호
            merchantNo = FDK_Module.Output(iProcID, "Merchant Number");
            // 승인번호
            authorizationNo = FDK_Module.Output(iProcID, "Authorization Number");
            // 매입사
            acquirerName = FDK_Module.Output(iProcID, "Acquirer Name");
            // 무서명거래
            String notice = FDK_Module.Output(iProcID, "Notice");

            if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getIsPrePaidCashReceipt().equals("Y") || cashReceipts.equals("Y")) {
                iProcID = FDK_Module.Creat();

                int taxAmount = Math.round(paymentMoney / 11);
                int taxableAmount = paymentMoney - taxAmount;

                //            FDK_Module.Input(iProcID, "Business Number", ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_BusinessRegistrationNumber());                // 사업자번호
                //TODO 등록된 사업자가 아니기 때문에 TEST강제 하드 코딩 배포시 위에 주석 해지..
                FDK_Module.Input(iProcID, "Business Number", "2268138295");                // 사업자번호
                FDK_Module.Input(iProcID, "Transaction Type", "00");                       // 거래 종류
                FDK_Module.Input(iProcID, "Transaction Amount", String.valueOf(paymentMoney)); // 결제금액
                FDK_Module.Input(iProcID, "Tax Amount", String.valueOf(taxAmount));// 세금
                FDK_Module.Input(iProcID, "Taxable Amount", String.valueOf(taxableAmount));   // 과세
                FDK_Module.Input(iProcID, "Non-Taxable Amount", "0");
                FDK_Module.Input(iProcID, "Receipt Print Flag", "2");

                payIRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CashReceiptAuth_K6");
            }

            if (payIRet == 0 || cardIRet == 0) {
                int finalIProcID = iProcID;
                String miProcID = FDK_Module.Output(finalIProcID, "Cash Receipt Authorization Number");
                new Thread(new Runnable() {
                    public void run() {
                        status[0] = PaymentManager.getPaymentStoreOrderItem(activity, orderItemId, itemCount, paymentMoney, cardPaymentMoney, preFilledGoldet, mileagePaymentAllUseet, cashReceipts, miProcID,
                                authorizationNo, strMsg.toString(), cardName, maskingCardNo, validPeriodNumber);
                        ManagerGetSelectOrderItemList managerGetSelectOrderItemList = VisitReceptionManager.getSelectOrderItemList(activity, orderItemId);

                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                if (status[0].equals("S0000")) {
                                    try {
                                        int payPrice = 0;
                                        String printString = "";
                                        List<GetSelectOrderItemList> getSelectOrderItemLists = managerGetSelectOrderItemList.getSelectOrderItemList();
                                        printString += "0E0B12" + UnicodeFormatter.stringToHex("세탁물 접수증", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringToHex("==================", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("접수일자 : " + getSelectOrderItemLists.get(0).getEnterDate(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("세탁완료예정일 : " + getSelectOrderItemLists.get(0).getWashFinWillDate(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("고객명 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName(), "euc-kr") + "140A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserPhone(), "euc-kr") + "140A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().get_UserAddress(), "euc-kr") + "0A";

                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("택번호\t\t" + " 품목\t\t    ", "금액", 9) + "0A";
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";

                                        for (int i = 0; i < getSelectOrderItemLists.size(); i++) {
                                            printString += UnicodeFormatter.stringJustifyToHex(getSelectOrderItemLists.get(i).getTagNo() + "\t\t" + getSelectOrderItemLists.get(i).getOptions().get(0).getOptionItemName() + "\t\t",
                                                    String.valueOf(getSelectOrderItemLists.get(i).getOptions().get(0).getOptionPrice()), 5) + "0A";

                                            for (int j = 0; j < getSelectOrderItemLists.get(i).getOptions().size(); j++) {
                                                printString += UnicodeFormatter.stringJustifyToHex("     \t\t" + getSelectOrderItemLists.get(i).getOptions().get(j).getOptionItemName() + "\t\t", String.valueOf(getSelectOrderItemLists.get(i).getOptions().get(j).getOptionPrice()), 5) + "0A";
                                            }
                                        }
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                        for (GetSelectOrderItemList getSelectOrderItemList : getSelectOrderItemLists) {
                                            payPrice += getSelectOrderItemList.getPayPrice();
                                        }
                                        printString += UnicodeFormatter.stringJustifyToHex("총 수량 :" + getSelectOrderItemLists.size(), "총 금액 :" + DataFormatUtil.moneyFormatToWon(payPrice), 44);
                                        printString += "0A";

                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringToHex("■결제 정보■", "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("선충전금 : " + preFilledGoldet + "\t", "마일리지 : " + mileagePaymentAllUseet, 30) + "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("현    금 : " + paymentMoney + "\t", "카    드 : " + cardPaymentMoney, 30) + "0A";
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("신용카드 매출전표", "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("=======================", "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [카드번호] : " + maskingCardNo, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [할부개월] : " + validPeriod, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [카 드 명] : " + cardName, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [승인번호] : " + authorizationNo, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [승인일시] : " + paymentDate, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [승인금액] : " + cardPaymentMoney, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [매입사명] : " + acquirerName, "euc-kr") + "0A";
                                        printString += "0F14" + UnicodeFormatter.stringToHex(" [가맹번호] : " + merchantNo, "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("■미수금 정보■", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringToHex("총 미수금 : " + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getNonPayPrice()), "euc-kr") + "0A";
                                        printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("■" + ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getUserName() + "님의 잔액■", "euc-kr") + "0A";
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringJustifyToHex("선충전금 : " + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getTotalPrePaid()),
                                                "마일리지 :" + DataFormatUtil.moneyFormatToWon(ManagerStoreUserInfo.getmInstance().getStoreUserInfo().get(0).getData().getMileage()), 44);
                                        printString += "0A";
                                        printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                        printString += "0A";
                                        if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintDamageNotice().equals("Y")) {
                                            printString += "0F14" + UnicodeFormatter.stringToHex(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getDamageNotice(), "euc-kr") + "0A";
                                            printString += "0A";
                                            printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                                            printString += "0A";
                                        }
                                        if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintStorageNotice().equals("Y")) {
                                            printString += "0F14" + UnicodeFormatter.stringToHex(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getStorageNotice(), "euc-kr") + "0A";
                                            printString += "0A";
                                            printString += UnicodeFormatter.stringToHex("============================================", "euc-kr") + "0A";
                                            printString += "0A";
                                        }
                                        printString += "0E0B12" + UnicodeFormatter.stringToHex(ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().getStoreName(), "euc-kr") + "0A";
                                        printString += "0A";

                                        printString += "0F12" + UnicodeFormatter.stringToHex("연락처 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_MasterPhone(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex("주소 : " + ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_StoreAddress2(), "euc-kr") + "0A";
                                        printString += "0F12" + UnicodeFormatter.stringToHex(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getStoreGreetingText(), "euc-kr") + "0A";

                                        printString += "11";

                                        printStringExcute(printString);
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    thirdAlertPopup.setTitle("결제정보 전송오류");
                                    thirdAlertPopup.setContent("결제정보 전송중 오류 발생했어요. \n 고객센터로 문의해주세요.");
                                    thirdAlertPopup.show();
                                }
                            }
                        });
                    }
                }).start();
            }

        } else if (payIRet == -1000 || cardIRet == -1000) {
            thirdAlertPopup.setTitle("연결실패");
            thirdAlertPopup.setContent("카드 단말기 연결확인");
            thirdAlertPopup.show();
            FDK_Module.Output(iProcID, "Response Code");
        } else {
            FDK_Module.Output(iProcID, "ErrorInfo");
        }
        return String.valueOf(payIRet);
    }


    /**
     * 카드 거래 취소 logic
     *
     * @param activity
     * @param receiptID    승인번호
     * @param PayYMD       승인일자
     * @param paymentPrice 승인금액
     * @throws Exception
     */
    public static String getPaymentCancle(AppCompatActivity activity, String receiptID, String PayYMD, int paymentPrice) throws Exception {
        Logger.d(TAG, "getPaymentCancle");
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        iProcID = FDK_Module.Creat();

        //            FDK_Module.Input(iProcID, "Business Number", ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_BusinessRegistrationNumber());                // 사업자번호
        //TODO 등록된 사업자가 아니기 때문에 TEST강제 하드 코딩 배포시 위에 주석 해지..
        FDK_Module.Input(iProcID, "Business Number", "2268138295");                  // 사업자번호
        FDK_Module.Input(iProcID, "Transaction Amount", String.valueOf(paymentPrice));   // 결제금액
        FDK_Module.Input(iProcID, "Original Authorization Number", receiptID);           // 승인번호
        FDK_Module.Input(iProcID, "Original Authorization Date", PayYMD);                // 승인일자
        FDK_Module.Input(iProcID, "Receipt Print Flag", "2");

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CreditCancel_D7");
        if (0 == iRet) {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            Logger.d(TAG, "strMsg : " + strMsg);
            FDK_Module.Output(iProcID, "Response Code");
            //Output Sample
            if (0 == iRet || -1000 == iRet) {
                strMsg += "Original Authorization Date : " + FDK_Module.Output(iProcID, "Original Authorization Date") + "\r\n";
            } else if (iRet == -1000) {
                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
                thirdAlertPopup.setTitle("연결실패");
                thirdAlertPopup.setContent("카드 단말기 연결확인");
                thirdAlertPopup.show();
                FDK_Module.Output(iProcID, "Response Code");
            } else {
                FDK_Module.Output(iProcID, "ErrorInfo");
            }
        }
        return String.valueOf(iRet);
    }

    /**
     * 현금영수증 취소 Logic
     *
     * @param activity
     * @param receiptID    승인번호
     * @param PayYMD       승인일자
     * @param paymentPrice 승인금액
     * @throws Exception
     */
    public static String payCashReceiptCancel(AppCompatActivity activity, String receiptID, String PayYMD, int paymentPrice) throws Exception {
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        iProcID = FDK_Module.Creat();

        Logger.d(TAG, "receiptID: " + receiptID);
        Logger.d(TAG, "PayYMD: " + PayYMD);
        Logger.d(TAG, "paymentPrice: " + paymentPrice);

        //            FDK_Module.Input(iProcID, "Business Number", ManagerStoreContractInfo.getmInstance().getStoreContractInfo().get(0).getData().get_BusinessRegistrationNumber());                // 사업자번호
        //TODO 등록된 사업자가 아니기 때문에 TEST강제 하드 코딩 배포시 위에 주석 해지..
        FDK_Module.Input(iProcID, "Business Number", "2268138295");
        FDK_Module.Input(iProcID, "Transaction Amount", String.valueOf(paymentPrice));
        FDK_Module.Input(iProcID, "Transaction Type", "00");
        FDK_Module.Input(iProcID, "Original Authorization Number", receiptID);
        FDK_Module.Input(iProcID, "Original Authorization Date", PayYMD);
        FDK_Module.Input(iProcID, "Cancel Reason", "1");
        FDK_Module.Input(iProcID, "Receipt Print Flag", "2");

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CashReceiptCancel_K7");

        if (0 == iRet) {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;

        } else if (-1000 == iRet) {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;

        } else {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;

        }
        return String.valueOf(iRet);
    }

    /**
     * SignPAD 전자서명 데이터 읽기 Logic
     */

    public static void getSingPAD(AppCompatActivity activity) throws Exception {
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        iProcID = FDK_Module.Creat();


        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/Read/Sign_UE");
        if (0 == iRet) {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        } else if (-1000 == iRet) {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        } else {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;
        }
        ShowMsg(strMsg, activity);

        //Output Sample
        if (0 == iRet || -1000 == iRet) {
            strMsg = "Output List :\r\n";
            strMsg += "STX : " + FDK_Module.Output(iProcID, "STX") + "\r\n";
            strMsg += "Work Type : " + FDK_Module.Output(iProcID, "Work Type") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "LCD Type : " + FDK_Module.Output(iProcID, "LCD Type") + "\r\n";
            strMsg += "Define Function : " + FDK_Module.Output(iProcID, "Define Function") + "\r\n";
            strMsg += "Model No : " + FDK_Module.Output(iProcID, "Model No") + "\r\n";
            strMsg += "SW Version : " + FDK_Module.Output(iProcID, "SW Version") + "\r\n";
            strMsg += "Compress Method : " + FDK_Module.Output(iProcID, "Compress Method") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            strMsg += "Hash Data : " + FDK_Module.Output(iProcID, "Hash Data") + "\r\n";
            strMsg += "FS3 : " + FDK_Module.Output(iProcID, "FS3") + "\r\n";
            strMsg += "Sign Data : " + FDK_Module.Output(iProcID, "Sign Data") + "\r\n";
            strMsg += "FS4 : " + FDK_Module.Output(iProcID, "FS4") + "\r\n";
            strMsg += "ETX : " + FDK_Module.Output(iProcID, "ETX") + "\r\n";
            strMsg += "LRC : " + FDK_Module.Output(iProcID, "LRC") + "\r\n";
            ShowMsg(strMsg, activity);
        }
        iRet = FDK_Module.Destroy(iProcID);
    }

    /**
     * 프린트 출력하기 위한 메소드
     *
     * @param _hex
     * @throws Exception
     */
    private static void printStringExcute(String _hex) throws Exception {

        Log.d(TAG, "printStringExcute: _hex : " + _hex);
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        iProcID = FDK_Module.Creat();

        //FDK_Module.Input(iProcID, "DATA", "123123");
        FDK_Module.Input(iProcID, "DATA", _hex);

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/Print/FreeStyle_F3");
        if (0 == iRet) {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        } else if (-1000 == iRet) {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        } else {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;
        }
        //ShowMsg(strMsg);

        //Output Sample
        if (0 == iRet || -1000 == iRet) {
            strMsg = "Output List :\r\n";
            strMsg += "Ascii : " + FDK_Module.Output(iProcID, "Ascii") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "AsciiLen : " + FDK_Module.Output(iProcID, "AsciiLen") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            //ShowMsg(strMsg);
        }
        iRet = FDK_Module.Destroy(iProcID);

        Logger.d(TAG, "iRet : " + iRet);
    }

    /**
     * 프린트 출력하기 위한 메소드
     *
     * @param _hex
     * @throws Exception
     */
    private static String printExcute(String _hex) throws Exception {

        Log.d(TAG, "printStringExcute: _hex : " + _hex);
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        iProcID = FDK_Module.Creat();

        //FDK_Module.Input(iProcID, "DATA", "123123");
        FDK_Module.Input(iProcID, "DATA", _hex);

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/Print/FreeStyle_F3");
        if (0 == iRet) {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        } else if (-1000 == iRet) {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        } else {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;
        }
        //ShowMsg(strMsg);

        //Output Sample
        if (0 == iRet || -1000 == iRet) {
            strMsg = "Output List :\r\n";
            strMsg += "Ascii : " + FDK_Module.Output(iProcID, "Ascii") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "AsciiLen : " + FDK_Module.Output(iProcID, "AsciiLen") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            //ShowMsg(strMsg);
        }
        iRet = FDK_Module.Destroy(iProcID);
        Logger.d(TAG, "iRet : " + iRet);

        return String.valueOf(iRet);
    }

    public static void ShowMsg(String strMsg, AppCompatActivity activity) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();     //닫기
            }
        });
        alert.setMessage(strMsg);
        alert.show();
    }
}