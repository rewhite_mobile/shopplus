package rewhite.shopplus.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

public class DataFormatUtil {
    private static final String TAG = "DataFormatUtil";

    /**
     * money format
     *
     * @param inputMoney
     * @return
     */


    public static String moneyFormatToWon(String inputMoney) {
        DecimalFormat decimalFormat = new DecimalFormat("#,###,###,###");
        String moneyFormat = decimalFormat.format(inputMoney);
        return moneyFormat;
    }

    public static String moneyFormatToWon(int inputMoney) {
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###,###");
        return decimalFormat.format(inputMoney);
    }

    public static String moneyFormatToWon(long inputMoney) {
        DecimalFormat decimalFormat = new DecimalFormat("#,###,###");
        return decimalFormat.format(inputMoney);
    }

    public static String moneyFormatToTag(String inputTag) {
        DecimalFormat decimalFormat = new DecimalFormat("####-####");
        return decimalFormat.format(inputTag);
    }


    /**
     * Time format
     */

    public static String setDateFormat(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("MM-dd");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }

    /**
     * Time format
     */

    public static String setDateMothFormat(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }

    /**
     * Time format
     */

    public static String setDatePaymentFormat(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }

    /**
     * Time format
     */

    public static String setDateWeekFormat(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyyMMdd");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }


    /**
     * Time format
     */

    public static String setDateWeekFormat2(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }

    /**
     * Time format
     */

    public static String setDateItemCancelFormat(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("MM/dd");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }

    /**
     * Time format
     */

    public static String setDateItemCancelFormat2(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }
    /**
     * Time format
     */

    public static String setDatePaymentFormat2(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy.MM.dd");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }

    /**
     * Time format
     */

    public static String setDatePaymentFormat3(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy.MM.dd");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }

    /**
     * Time format
     */

    public static String setDatePaymentFormat4(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyyMMdd");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }

    /**
     * Time format
     */

    public static String setDatTimeFormat(String dataNow) throws ParseException {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        oldFormat.setTimeZone(TimeZone.getTimeZone("KST"));
        SimpleDateFormat newFormat = new SimpleDateFormat("HH:mm");

        Date oldDate = oldFormat.parse(dataNow);
        String newDate = newFormat.format(oldDate);

        return newDate;
    }

    /**
     * 특정 날짜 구하기 위해 사용하는 메소드
     *
     * @param day
     * @return
     */
    public static String getDayWeek(String day) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        Date date = null;  // 날짜 입력하는곳 .
        try {
            date = formatter.parse(day);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        date = new Date(date.getTime() + (1000 * 60 * 60 * 24 * +1));  // 날짜에 하루를 더한 값
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int dayNum = cal.get(Calendar.DAY_OF_WEEK);   // 요일을 구해온다.

        String convertedString = "";

        switch (dayNum) {
            case 1:
                convertedString = "일";
                break;
            case 2:
                convertedString = "월";
                break;
            case 3:
                convertedString = "화";
                break;
            case 4:
                convertedString = "수";
                break;
            case 5:
                convertedString = "목";
                break;
            case 6:
                convertedString = "금";
                break;
            case 7:
                convertedString = "토";
                break;
        }

        return convertedString;
    }

    /**
     * HHmm 형식의 숫자 반환
     *
     * @return
     */
    public static int getCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdfNow = new SimpleDateFormat("HHmm");
        return Integer.parseInt(sdfNow.format(c.getTime()));
    }

    public static String getCurrentHour() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdfNow = new SimpleDateFormat("HH");
        return sdfNow.format(c.getTime());
    }

    /**
     * yyyyMMdd 형식의 오늘 문자열 반환 ex)20160707
     *
     * @return
     */
    public static String getCurrentDate() {
        return getDate(Calendar.getInstance());
    }

    public static String getCurrentDate16() {
        return getDate16(Calendar.getInstance());
    }


    public static String getCurrentDate16After30Min() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 30);
        return getDate16(calendar);
    }

    /**
     * yyyyMMdd 형식의 날짜 문자열 반환 ex)20160515
     *
     * @param c
     * @return
     */
    public static String getDate(Calendar c) {
        SimpleDateFormat sdfNow = new SimpleDateFormat("yyyyMMdd");
        return sdfNow.format(c.getTime());
    }

    public static String getDateHyphen(Calendar c) {
        SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy-MM-dd");
        return sdfNow.format(c.getTime());
    }

    public static String getDateHyphen(String c) {
        SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy-MM-dd");
        return sdfNow.format(c);
    }

    public static String getCoustemDate(Calendar c) {
        SimpleDateFormat sdfNow = new SimpleDateFormat("MM-dd");
        return sdfNow.format(c.getTime());
    }

    public static String getDate16(Calendar c) {
        SimpleDateFormat sdfNow = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdfNow.format(c.getTime());
    }

    public static String getDateFormat(Calendar date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(date.getTime());
    }

    public static Calendar getDateObjFormat(String date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        try {
            Date d = formatter.parse(date);
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            return c;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 날짜 변환(8자리)
     *
     * @param date 날짜 텍스트
     * @return 변환된 날짜 객체
     */
    public static Calendar getDate8(String date) {
        if (date != null) {
            return getDate14(date + "000000");
        }
        return null;
    }

    /**
     * 날짜 변환(14자리)
     *
     * @param date 날짜 텍스트
     * @return 변환된 날짜 객체
     */
    public static Calendar getDate14(String date) {
        try {
            String strValue = new String(date).trim();
            Calendar cal = Calendar.getInstance();
            // yyyyMMddHHmmss
            cal.set(Calendar.YEAR, Integer.parseInt(strValue.substring(0, 4)));
            cal.set(Calendar.MONTH, Integer.parseInt(strValue.substring(4, 6)) - 1);
            cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(strValue.substring(6, 8)));
            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(strValue.substring(8, 10)));
            cal.set(Calendar.MINUTE, Integer.parseInt(strValue.substring(10, 12)));
            cal.set(Calendar.SECOND, Integer.parseInt(strValue.substring(12, 14)));
            return cal;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 어제 날짜
     * @return
     */
    public static String getYesterday() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);  // 오늘 날짜에서 하루를 뺌.
        String date = sdf.format(calendar.getTime());
        return date;
    }

    /**
     * 오늘 날짜
     * @return
     */
    public static String getToday() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);  // 오늘 날짜에서 하루를 뺌.
        String date = sdf.format(calendar.getTime());
        return date;
    }

    /**
     * 일주일 전 날짜
     * @return
     */
    public static String get7DayAgoDate(){
        Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280, "KST"));
        cal.add(Calendar.DATE ,-7);
        java.util.Date weekago = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return formatter.format(weekago);
    }


    /**
     * 1개월 전 날짜
     * @return
     */
    public static String getMonthAgoDate(){
        Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280, "KST"));
        cal.add(Calendar.MONTH ,-1);
        java.util.Date weekago = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return formatter.format(weekago);
    }

    /**
     * 3개월 전 날짜
     * @return
     */
    public static String get3MonthAgoDate(){
        Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280, "KST"));
        cal.add(Calendar.MONTH ,-3);
        java.util.Date weekago = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return formatter.format(weekago);
    }

    /**
     * 6개월 전 날짜
     * @return
     */
    public static String get6MonthAgoDate(){
        Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280, "KST"));
        cal.add(Calendar.MONTH ,-6);
        java.util.Date weekago = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return formatter.format(weekago);
    }

    /**
     * 1년전 날짜
     * @return
     */
    public static String get1yearAgoDate(){
        Calendar cal = Calendar.getInstance(new SimpleTimeZone(0x1ee6280, "KST"));
        cal.add(Calendar.YEAR ,-1);
        java.util.Date weekago = cal.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return formatter.format(weekago);
    }

    public static int getRandomWorkHour() {
        Random rand = new Random();
        int num = rand.nextInt(9) + 9; // 9 ~ 17
        return num;
    }

    public static int getRandomSleepHour() {
        Random rand = new Random();
        int num = rand.nextInt(4) + 2; // 2 ~ 5
        return num;
    }

    public static int getAge(Calendar c) {
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        return getAge(year, month, day);
    }

    public static int getAge(int year, int month, int day) {
        String birthmd = String.format("%02d%02d", month, day);
        Calendar c = Calendar.getInstance();
        int age = c.get(Calendar.YEAR) - year;
        if (age < 0) return 0;

        String todaymd = String.format("%02d%02d", c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));
        if (Integer.parseInt(todaymd) < Integer.parseInt(birthmd)) {
            age--;
        }
        return age;
    }

    /**
     * yyyyMMDD 날짜 포맷을 받아 D-day 계산
     *
     * @param from
     * @return
     */
    public static int getDday(String from) {
        try {
            Calendar dday = getDate8(from);
            Calendar today = Calendar.getInstance();
            long day = dday.getTimeInMillis() / 86400000;
            long tday = today.getTimeInMillis() / 86400000;
            long count = day - tday;
            return (int) count + 1;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String trandFormat(String to, String toFormat, String fromFormat) {
        SimpleDateFormat to_formatter = new SimpleDateFormat(toFormat);
        SimpleDateFormat from_formatter = new SimpleDateFormat(fromFormat);
        try {
            Date toDate = to_formatter.parse(to);
            String ret = from_formatter.format(toDate);
            return ret;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String closingWeekType(int type) {
        String weekName = null;
        if (type == 1) {
            weekName = "매주";
        } else if (type == 2) {
            weekName = "격주(1,3주)";
        } else {
            weekName = "격주(2,4주)";
        }
        return weekName;
    }

    /**
     * 휴무일 (요일설정 DataFormat)
     *
     * @param dayName
     * @return
     */
    public static String daySettingType(String dayName) {
        StringBuffer stringBuffer = new StringBuffer();
        String str = null;

        if (dayName.charAt(0) == '1') {
            stringBuffer.append("월");
            stringBuffer.append(", ");
        }
        if (dayName.charAt(1) == '1') {
            stringBuffer.append("화");
            stringBuffer.append(", ");
        }
        if (dayName.charAt(2) == '1') {
            stringBuffer.append("수");
            stringBuffer.append(", ");
        }
        if (dayName.charAt(3) == '1') {
            stringBuffer.append("목");
            stringBuffer.append(", ");
        }
        if (dayName.charAt(4) == '1') {
            stringBuffer.append("금");
            stringBuffer.append(", ");
        }
        if (dayName.charAt(5) == '1') {
            stringBuffer.append("토");
            stringBuffer.append(", ");
        }
        if (dayName.charAt(6) == '1') {
            stringBuffer.append("일");
            stringBuffer.append(", ");
        }

        if (dayName.length() > 0 && dayName.charAt(dayName.length() - 1) == ',') {
            str = dayName.substring(0, dayName.length() - 1);
        }

        str = stringBuffer.toString().replace(stringBuffer.toString().substring(stringBuffer.toString().length() - 1), "");

        return stringBuffer.toString();
    }

    public static String getCloseDateFormatMonning(int position) {
        String from = null;
        if (position == 0) {
            from = "05:00";
        } else if (position == 1) {
            from = "06:00";
        } else if (position == 2) {
            from = "07:00";
        } else if (position == 3) {
            from = "08:00";
        } else if (position == 4) {
            from = "09:00";
        } else if (position == 5) {
            from = "10:00";
        } else if (position == 6) {
            from = "11:00";
        }
        return from;
    }

    public static String getCloseDateFormatAfternoon(int position) {
        String from = null;
        if (position == 0) {
            from = "01:00";
        } else if (position == 1) {
            from = "02:00";
        } else if (position == 2) {
            from = "03:00";
        } else if (position == 3) {
            from = "04:00";
        } else if (position == 4) {
            from = "05:00";
        } else if (position == 5) {
            from = "06:00";
        } else if (position == 6) {
            from = "07:00";
        } else if (position == 7) {
            from = "08:00";
        } else if (position == 8) {
            from = "09:00";
        } else if (position == 9) {
            from = "10:00";
        } else if (position == 10) {
            from = "11:00";
        } else if (position == 11) {
            from = "12:00";
        } else if (position == 12) {
            from = "00:00";
        }
        return from;
    }
}
