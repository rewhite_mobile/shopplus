package rewhite.shopplus.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.data.type.NetworkStatus;

public class NetworkUtils {
    private static final String TAG = "NetworkUtils";

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final int version = Build.VERSION.SDK_INT;
        if (version >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivity.getAllNetworks();
            NetworkInfo netInfo;
            for (Network network : networks) {
                netInfo = connectivity.getNetworkInfo(network);
                if (netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        } else {
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo networkInfo : info) {
                        if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static NetworkStatus getNetworkStatus(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                Logger.d(TAG, "wifi connected");
                return NetworkStatus.WIFI;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                Logger.d(TAG, "mobile connected");
                return NetworkStatus.MOBILE;
            }
        } else {
            Logger.d(TAG, "not connected");
            return NetworkStatus.NOT_CONNECT;
        }
        return NetworkStatus.NOT_CONNECT;
    }

    public static ArrayList<String> getWifiApList(Context context) {
        WifiManager wm = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);

        ArrayList<String> apList = new ArrayList<String>();

        List<ScanResult> scanResults = wm.getScanResults();
        for (ScanResult scanResult : scanResults) {
            apList.add(scanResult.SSID);
        }
        return apList;
    }

    public static WifiInfo getWifiInfo(Context context) {
        WifiManager wm = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wi = wm.getConnectionInfo();
        return wi;
    }
}