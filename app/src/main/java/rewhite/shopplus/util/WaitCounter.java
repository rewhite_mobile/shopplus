package rewhite.shopplus.util;


import android.content.Context;

import rewhite.shopplus.common.view.Wait;

/**
 * 한 화면에서 한개의 Wait Dialog만 활성화 되도록 관리하는 매니저
 */
public class WaitCounter {
    // 현재 화면 Context
    private Context mContext;

    // Wait Dialog
    private Wait mWait;

    // show 요청 횟수
    private long mWaitCount = 0;

    public WaitCounter(Context context) {
        mContext = context;
    }

    /**
     * Wait Dialog 생성 요청
     */
    public void show() {
        show(null);
    }

    public void show(Wait.WaitCancelListener listener) {
        if( mWait == null ) {
            mWait = Wait.display(mContext, listener);
        } else {
            mWait.setCancelListener(listener);
            mWait.show();
        }
        mWaitCount++;
    }

    public void setMessage(int resId) {
        setMessage(mContext.getString(resId));
    }

    public void setMessage(String text) {
        if(mWait == null) {
            mWait = new Wait(mContext);
        }
        mWait.setMessage(text);
    }

    /**
     * Wait Dialog 해제 요청
     */
    public void dismiss() {
        mWaitCount--;

        if( mWait != null && mWaitCount <= 0 ) {
            try {
                mWait.setMessage("");
                mWait.dismiss();
            } catch( Exception ex ) {
                // do nothing
            } finally {
                mWait = null;
            }
        }
    }

    public long getCurrentCount() {
        return mWaitCount;
    }

    public boolean isShowing() {
        return mWait != null && mWait.isShowing();
    }
}