package rewhite.shopplus.util;

import android.support.v7.app.AppCompatActivity;

import rewhite.shopplus.common.popup.ThirdAlertPopup;

public class ServerErrPopup {
    private static final String TAG = "ServerErrPopup";

    /**
     * ID / PW 서버 오류 팝업
     *
     * @param activity
     * @param code
     */
    public static void idPwSerch(AppCompatActivity activity, String code) {
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(activity);
        thirdAlertPopup.setTitle("오류");

        switch (code) {
            case "F1001":
                thirdAlertPopup.setContent("등록되지 않은 휴대폰번호에요.\n" + "휴대폰번호를 확인해주세요.");
                break;

        }
        thirdAlertPopup.show();
    }


}
