package rewhite.shopplus.util;

import android.os.Handler;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class TimerHandler {
    private static final String TAG = "TimerHandler";

    private String TIME_FORMAT = "aa hh:mm:ss";
    private SimpleDateFormat sdf;
    private TextView clock;
    private Handler handler = new Handler();
    private Runnable updater;

    private long updateTime = UP_SEC;

    private static final long THOUSAND = 1000;
    public static final long UP_SEC = THOUSAND * 1;
    public static final long UP_MIN = UP_SEC * 60;
    public static final long UP_HOUR = UP_MIN * 60;
    public static final long UP_H_DAY = UP_HOUR * 12;
    public static final long UP_A_DAY = UP_H_DAY * 2;

    public TimerHandler(TextView clock) {
        this.clock = clock;
        sdf = new SimpleDateFormat(TIME_FORMAT, Locale.KOREA);
    }

    public TimerHandler(TextView clock, String format) {
        TIME_FORMAT = format;
        this.clock = clock;
        sdf = new SimpleDateFormat(TIME_FORMAT, Locale.KOREA);
    }


    public TimerHandler(TextView clock, String format, long updateTime) {
        this(clock, format);
        this.updateTime = updateTime;
    }

    public void start() {
        Timer timer = new Timer();
        TimerTask tt = new TimerTask() {
            public void run() {
                update();
            }
        };
        timer.schedule(tt, 0, updateTime);
    }

    private void update() {
        updater = new Runnable() {
            public void run() {
                clock.setText(sdf.format(new Date()));
            }
        };
        handler.post(updater);
    }

}