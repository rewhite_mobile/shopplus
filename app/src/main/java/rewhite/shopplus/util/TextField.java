package rewhite.shopplus.util;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import rewhite.shopplus.R;

/**
 * Created by chris on 17/03/15.
 * For Calligraphy.
 */
public class TextField extends AppCompatTextView {

    public TextField(final Context context, final AttributeSet attrs) {
        super(context, attrs, R.attr.textFieldStyle);
    }

}
