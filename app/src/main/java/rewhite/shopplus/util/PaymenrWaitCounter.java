package rewhite.shopplus.util;

import android.content.Context;

import rewhite.shopplus.common.view.PaymentWait;

public class PaymenrWaitCounter {
    // 현재 화면 Context
    private Context mContext;

    // Wait Dialog
    private PaymentWait mWait;

    // show 요청 횟수
    private long mWaitCount = 0;

    public PaymenrWaitCounter(Context context) {
        mContext = context;
    }

    /**
     * Wait Dialog 생성 요청
     */
    public void show() {
        show(null);
    }

    public void show(PaymentWait.WaitCancelListener listener) {
        if( mWait == null ) {
            mWait = PaymentWait.display(mContext, listener);
        } else {
            mWait.setCancelListener(listener);
            mWait.show();
        }
        mWaitCount++;
    }

    public void setMessage(int resId) {
        setMessage(mContext.getString(resId));
    }

    public void setMessage(String text) {
        if(mWait == null) {
            mWait = new PaymentWait(mContext);
        }
        mWait.setMessage(text);
    }

    /**
     * Wait Dialog 해제 요청
     */
    public void dismiss() {
        mWaitCount--;

        if( mWait != null && mWaitCount <= 0 ) {
            try {
                mWait.setMessage("");
                mWait.dismiss();
            } catch( Exception ex ) {
                // do nothing
            } finally {
                mWait = null;
            }
        }
    }

    public long getCurrentCount() {
        return mWaitCount;
    }

    public boolean isShowing() {
        return mWait != null && mWait.isShowing();
    }
}