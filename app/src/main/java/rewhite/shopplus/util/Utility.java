package rewhite.shopplus.util;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.MainThread;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rewhite.shopplus.R;
import rewhite.shopplus.common.popup.AlertPopup;
import rewhite.shopplus.common.popup.ConfirmPopup;

/**
 * 각종 유틸리티 메서드 모음
 */
public class Utility {
    private Utility() {

    }

    /**
     * 빈 배열(배열 객체가 null이거나 배열 크기가 0인 경우)인지의 여부를 반환한다.
     *
     * @param array 배열 객체
     * @return 빈 배열일 경우 true, 그렇지 않은 경우 false 반환
     */
    public static boolean isArrayEmpty(Object[] array) {
        return (array == null || array.length == 0);
    }

    /**
     * 빈 문자열인지의 여부를 반환한다.
     *
     * @param s 문자열 객체
     * @return 빈 문자열일 경우 true, 그렇지 않은 경우 false 반환
     */
    public static boolean isStringEmpty(String s) {
        return (s == null || s.isEmpty());
    }

    /**
     * 빈 List 객체인지의 여부를 반환한다.
     *
     * @param list List 객체
     * @return 빈 List일 경우 true, 그렇지 않을 경우 false 반환
     */
    public static boolean isListEmpty(List list) {
        return (list == null || list.size() == 0);
    }

    public static int nextRandom(int value) {
        Random random = new Random();
        int next = random.nextInt(value);
        return next;
    }

    /**
     * Drawable 객체로부터 Bitmap 객체를 반환한다.
     *
     * @param drawable Drawable 객체
     * @return Bitmap 객체
     */
    public static Bitmap fromDrawable(Drawable drawable) {
        Bitmap bitmap;
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static void showToastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showToastShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static int toPixel(Context context, int dp) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static boolean existFlag(Window window, int flag) {
        return window != null && (window.getAttributes().flags & flag) != 0;
    }

    public static boolean isTransparentStatus(Window window) {
        return window != null &&
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT &&
                existFlag(window, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }

    public static void setTransparentStatus(Window window, boolean transparency) {
        if (window != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (transparency) {
                window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            } else {
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }
    }

    private static Executor sGlobalExecutor;

    @MainThread
    public static <T>
    void executeTask(AsyncTask<T, ?, ?> task, T... params) {
        if (task != null) {
            task.executeOnExecutor(getGlobalExecutor(), params);
        }
    }

    public static Executor getGlobalExecutor() {
        if (sGlobalExecutor == null) {
            sGlobalExecutor = AsyncTask.THREAD_POOL_EXECUTOR;
        }

        return sGlobalExecutor;
    }

    public static void debug(String tag, String format, Object... params) {
        try {
            Log.d(tag, String.format(Locale.getDefault(), format, params));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static <T> T cast(Object from, Class<T> to) {
        try {
            return to.cast(from);
        } catch (ClassCastException ex) {
            return null;
        }
    }

    /**
     * 텍스트 중 일부 색상을 변경
     *
     * @param text  전체 텍스트
     * @param color 변경할 색상
     * @param start 변경할 텍스트 시작 위치
     * @param end   변경할 테스트 끝 위치
     * @return 색상이 변경된 전체 텍스트
     */
    public static SpannableString getColorSpanText(String text, int color, int start, int end) {
        SpannableString span = new SpannableString(text);
        span.setSpan(new ForegroundColorSpan(color), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return span;
    }

    /**
     * 텍스트 중 일부 색상을 변경
     *
     * @param text     전체 텍스트
     * @param spanText 변경할 텍스트
     * @param color    변경할 색상
     * @return 색상이 변경된 전체 텍스트
     */
    public static SpannableString getColorSpanText(String text, String spanText, int color) {
        SpannableString span = new SpannableString(text);

        try {
            Pattern pattern = Pattern.compile(spanText);
            Matcher matcher = pattern.matcher(text);
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                span.setSpan(new ForegroundColorSpan(color), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return span;
    }

    public static SpannableString getRelativeSizeSpanText(String text, float relativeSize, int start, int end) {
        SpannableString span = new SpannableString(text);
        span.setSpan(new RelativeSizeSpan(relativeSize), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return span;
    }

    public static int getStatusBarHeight(Context context) {
        int id = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        int height = 0;

        if (id > 0) {
            height = context.getResources().getDimensionPixelSize(id);
        }

        return height;
    }

    public static String formatString(Context context, int resId, Object... params) {
        String format = context.getString(resId);
        return String.format(format, params);
    }

    public static AlertPopup showSimpleAlert(Context context, int resId) {
        return showSimpleAlert(context, context.getString(resId));
    }

    public static AlertPopup showSimpleAlert(Context context, String message) {
        AlertPopup popup = new AlertPopup(context);
        popup.setContent(message).show();

        return popup;
    }

    // 서버 response error 발생 시 디폴트로 보여주는 팝업
    public static void showNetworkCallbackErrorPopup(Context context) {
        showNetworkCallbackErrorPopup(context, null);
    }

    // 서버 response error 발생 시 디폴트로 보여주는 팝업
    public static void showNetworkCallbackErrorPopup(Context context, AlertPopup.OnConfirmClickListener listener) {
        AlertPopup popup = showSimpleAlert(context, R.string.network_callback_error);
        if (listener != null) {
            popup.setOnConfirmClickListener(listener);
        }
    }

    // 네트워크 자체가 가능한지의 여부를 판단하는 팝업
    public static boolean checkNetworkAlivePopup(Context context) {
        if (!ConnectionInfo.hasNetwork(context)) {
            showSimpleAlert(context, R.string.network_not_available_error);
            return false;
        }

        return true;
    }

    public static boolean checkNetworkAlivePopup(Context context, AlertPopup.OnConfirmClickListener listener) {
        if (!ConnectionInfo.hasNetwork(context)) {
            AlertPopup popup = showSimpleAlert(context, R.string.network_not_available_error);
            if (listener != null) {
                popup.setOnConfirmClickListener(listener);
            }
            return false;
        }

        return true;
    }

    public static int getColorFromText(String colorText, int defaultColor) throws IllegalArgumentException {
        if (TextUtils.isEmpty(colorText)) {
            return defaultColor;
        }
        String s = colorText.replaceAll("[^a-fA-F0-9]*", "").toUpperCase();

        if (s.length() != 3 && s.length() != 6 && s.length() != 8) {
            return defaultColor;
        }

        if (s.length() == 3) {
            s = String.format(Locale.getDefault(),
                    "%02X%s%s%s%s%s%s",
                    0xff,
                    s.charAt(0), s.charAt(0),
                    s.charAt(1), s.charAt(1),
                    s.charAt(2), s.charAt(2)
            );
        } else if (s.length() == 6) {
            s = String.format(Locale.getDefault(), "%02X%s", 0xff, s);
        }

        s = s.toUpperCase();

        String a = s.substring(0, 2);
        String r = s.substring(2, 4);
        String g = s.substring(4, 6);
        String b = s.substring(6, 8);

        return Color.argb(toHex(a), toHex(r), toHex(g), toHex(b));
    }

    private static int toHex(String s) {
        int lastIndex = s.length() - 1;
        int d = 0;
        int r = 0;

        while (lastIndex >= 0) {
            char c = s.charAt(lastIndex);
            int h = 0;

            if (c >= '0' && c <= '9') {
                h = c - '0';
            } else if (c >= 'A' && c <= 'F') {
                h = c - 'A' + 10;
            }

            r += h * Math.pow(16, d);

            d++;
            lastIndex--;
        }

        return r;
    }

    public static void showConfirmPopup(AppCompatActivity activity, String message, AlertPopup.OnConfirmClickListener onConfirmClickListener, ConfirmPopup.OnCancelClickListener onCancelClickListener) {
        ConfirmPopup popup = new ConfirmPopup(activity);
        popup.setContent(message);
        popup.setOnConfirmClickListener(onConfirmClickListener);
        popup.setOnCancelClickListener(onCancelClickListener);
        popup.show();
    }

    public static String getFormatNumber(Number n) {
//        DecimalFormat format = new DecimalFormat("#,###,###,###,###,###");
//
//        return format.format(n);
        return NumberFormat.getNumberInstance(Locale.US).format(n);
    }
}