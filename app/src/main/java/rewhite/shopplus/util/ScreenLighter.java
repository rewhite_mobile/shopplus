package rewhite.shopplus.util;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

/**
 * 화면 밝기를 조절하는 클래스
 */
public class ScreenLighter {
    private Window mWindow;
    private float currentBrightness;

    public ScreenLighter(Window window) {
        mWindow = window;
        currentBrightness = mWindow.getAttributes().screenBrightness;
    }

    public ScreenLighter(AppCompatActivity activity) {
        this(activity.getWindow());
    }

    public ScreenLighter(Fragment fragment) {
        this((AppCompatActivity)fragment.getActivity());
    }

    public void enlighten() {
        setBrightness(1f);
    }

    public void restore() {
        setBrightness(currentBrightness);
    }

    private void setBrightness(float value) {
        WindowManager.LayoutParams wlp = mWindow.getAttributes();
        wlp.screenBrightness = value;
        mWindow.setAttributes(wlp);
    }

}
