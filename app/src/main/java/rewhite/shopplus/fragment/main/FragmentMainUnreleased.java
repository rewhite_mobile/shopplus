package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.view.adapter.AdapterUnreleased;

public class FragmentMainUnreleased extends Fragment {
    private static final String TAG = "FragmentMainUnreleased";

    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_unreleased, container, false);

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        listView = (ListView) view.findViewById(R.id.lv);

        List<GetVisitOrderList> getVisitOrderList = ManagerGetVisitOrder.getmInstance().getGetVisitOrder();

        AdapterUnreleased adapterUnreleased = new AdapterUnreleased(getContext());
        adapterUnreleased.addItem(getVisitOrderList);
        listView.setAdapter(adapterUnreleased);
    }
}