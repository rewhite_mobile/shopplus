package rewhite.shopplus.fragment.main;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.common.CustomKeyboard1;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterMainOrderInquiry;

public class FragmentQuickDetails extends Fragment {
    private static final String TAG = "FragmentQuickDetails";
    private List<GetVisitOrderList> listItem;
    private static final int MAX_LENGTH_TAG_5 = 5;
    private static final int MAX_LENGTH_TAG_8 = 8;
    private static final int MAX_LENGTH_TAG_10 = 10;

    private int testType = 1;

    private WaitCounter mWait;
    private TextView tvSearch, tvItemCount;
    private LinearLayout llNoData;
    private ListView listView;

    private int mPosition;
    private Spinner Smember;
    private EditText etName;

    private String tacNumber = null;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quick_details, container, false);
        mWait = new WaitCounter(getActivity());

        setLayout(view);
        setCustomKeyBoard(view);
        return view;
    }


    private void setLayout(View view) {
        tvSearch = (TextView) view.findViewById(R.id.tv_search);
        listView = (ListView) view.findViewById(R.id.list_view);
        llNoData = (LinearLayout) view.findViewById(R.id.ll_no_data);

        etName = (EditText) view.findViewById(R.id.et_name);
        Smember = (Spinner) view.findViewById(R.id.s_member);
        tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
        tvSearch.setOnClickListener(btnOnClickListener);

        setOnClickView();
    }

    private void setOnClickView() {
        Smember.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        mPosition = 0;
//                        InputMethodManager immhide = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
//                        immhide.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                        break;
                    case 1:
                        mPosition = 1;
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                        break;
                    case 2:
                        mPosition = 2;
                        setEditTextView();
                        InputMethodManager mimmhide = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                        mimmhide.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void setEditTextView() {
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String tagNumberEnd = null;
                testType = ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType();
                if (testType == 1) {
                    if (start == 3) {
                        etName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_5)});
                        String name = CommonUtil.getTagFormmater(s.toString().substring(0, 1).toString());

                        etName.setText(name + s.toString().substring(1, s.toString().length()));
                        etName.setSelection(etName.getText().toString().length());

                    } else if (start == 4) {
                        etName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_5)});
                        tagNumberEnd = s.toString().substring(0, 2);

                        if (!tagNumberEnd.equals("①0")) {
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());
                            thirdAlertPopup.setTitle("알림");
                            thirdAlertPopup.setContent("정상적인 택번호가 아닙니다.");

                            thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    etName.setText("");
                                    return;
                                }
                            });
                            thirdAlertPopup.show();
                        } else {
                            String name = CommonUtil.getTagFormmater((tagNumberEnd.toString()));

                            etName.setText(s.toString().replace(tagNumberEnd, name));
                            etName.setSelection(etName.getText().toString().length());
                        }
                    }
                } else if (testType == 2) {
                    etName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_8)});
                    if (start == 4) {
                        String name = CommonUtil.getTagFormmater(s.toString().substring(1, 2).toString());
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(1, 2);
                        stringBuffer.insert(1, name);

                        String testName = stringBuffer.toString();

                        etName.setText(testName);
                        etName.setSelection(etName.getText().toString().length());

                    } else if (start == 5) {
                        String deleteNmae = CommonUtil.getSubTagFormmater(s.toString().substring(1, 2).toString());

                        String name = CommonUtil.getTagFormmater(s.toString().substring(2, 3).toString());
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(1, 2);
                        stringBuffer.insert(1, deleteNmae);
                        stringBuffer.delete(2, 3);
                        stringBuffer.insert(2, name);

                        String testName = stringBuffer.toString();
                        etName.setText(testName + s.toString());
                        etName.setSelection(etName.getText().toString().length());

                    } else if (start == 6) {
                        String deleteNmae = CommonUtil.getSubTagFormmater(s.toString().substring(2, 3).toString());

                        String name = CommonUtil.getTagFormmater(s.toString().substring(3, 4).toString());
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(2, 3);
                        stringBuffer.insert(2, deleteNmae);
                        stringBuffer.delete(3, 4);
                        stringBuffer.insert(3, name);

                        String testName = stringBuffer.toString();
                        etName.setText(testName + s.toString());
                        etName.setSelection(etName.getText().toString().length());

                    }
                } else if (testType == 3) {
                    etName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_10)});
                    String name = null;
                    String nameTo = null;
                    if (start == 4) {
                        name = s.toString().substring(0, 1);
                        nameTo = s.toString().substring(1, 5);

                        etName.setText(name + "-" + nameTo);
                        etName.setSelection(etName.getText().toString().length());
                    } else if (start == 6) {
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(1, 2);
                        stringBuffer.insert(2, "-");

                        etName.setText(stringBuffer.toString());
                        etName.setSelection(etName.getText().toString().length());
                    } else if (start == 7) {
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(2, 3);
                        stringBuffer.insert(3, "-");

                        etName.setText(stringBuffer.toString());
                        etName.setSelection(etName.getText().toString().length());
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mWait.show();

            if (mPosition == 0) {
                if (etName.getText().toString().length() != 4) {
                    mWait.dismiss();
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getContext());
                    thirdAlertPopup.setTitle("오류 팝업");
                    thirdAlertPopup.setContent("핸드폰 뒷자리 4자리를 입력해주세요.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                } else if (etName.getText().toString().length() == 0) {
                    mWait.dismiss();
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getContext());
                    thirdAlertPopup.setTitle("오류 팝업");
                    thirdAlertPopup.setContent("핸드폰 뒷자리 4자리를 입력해주세요.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                } else {
                    new Thread(new Runnable() {
                        public void run() {
                            ManagerGetVisitOrder managerCancelAvailOrderList = VisitReceptionManager.getGetVisit((AppCompatActivity)getActivity(), null, null, null, null, etName.getText().toString(), null, null, 1, 20000);

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    mWait.dismiss();
                                    if (managerCancelAvailOrderList.getResultCode().equals("S0000")) {
                                        llNoData.setVisibility(View.GONE);
                                        listView.setVisibility(View.VISIBLE);

                                        AdapterMainOrderInquiry adapterMainOrderInquiry = new AdapterMainOrderInquiry((AppCompatActivity)getActivity());
                                        listItem = managerCancelAvailOrderList.getGetVisitOrder();
                                        tvItemCount.setText(listItem.size() + "건 주문이 있습니다.");
                                        adapterMainOrderInquiry.addItem(listItem);
                                        listView.setAdapter(adapterMainOrderInquiry);
                                    } else {
                                        llNoData.setVisibility(View.VISIBLE);
                                        listView.setVisibility(View.GONE);
                                    }
                                }
                            });
                        }
                    }).start();
                }
            } else if (mPosition == 1) {
                if (etName.getText().toString().length() == 0) {
                    mWait.dismiss();
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getContext());
                    thirdAlertPopup.setTitle("오류 팝업");
                    thirdAlertPopup.setContent("회원명을 입력해주세요.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                } else {
                    new Thread(new Runnable() {
                        public void run() {
                            ManagerGetVisitOrder managerCancelAvailOrderList = VisitReceptionManager.getGetVisit((AppCompatActivity)getActivity(), null, null, null, null, null, etName.getText().toString(), null, 1, 20000);

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    mWait.dismiss();
                                    if (managerCancelAvailOrderList.getResultCode().equals("S0000")) {
                                        llNoData.setVisibility(View.GONE);
                                        listView.setVisibility(View.VISIBLE);

                                        AdapterMainOrderInquiry adapterMainOrderInquiry = new AdapterMainOrderInquiry((AppCompatActivity)getActivity());
                                        listItem = managerCancelAvailOrderList.getGetVisitOrder();
                                        tvItemCount.setText(listItem.size() + "건 주문이 있습니다.");

                                        adapterMainOrderInquiry.addItem(listItem);
                                        listView.setAdapter(adapterMainOrderInquiry);
                                    } else {
                                        llNoData.setVisibility(View.VISIBLE);
                                        listView.setVisibility(View.GONE);
                                    }
                                }
                            });
                        }
                    }).start();
                }
            } else if (mPosition == 2) {
                if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
                    tacNumber = CommonUtil.setTagFormaterChangeResponse(etName.getText().toString());        // 일제 택번호
                } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
                    tacNumber = CommonUtil.setTagFormaterChangeTypeResponse(etName.getText().toString());    // 일제바코드 택번호
                } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
                    tacNumber = etName.getText().toString();// 기타 택번호
                }
                if (etName.getText().toString().length() == 0) {
                    mWait.dismiss();
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getContext());
                    thirdAlertPopup.setTitle("오류 팝업");
                    thirdAlertPopup.setContent("택번호를 입력해주세요.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                } else {
                    new Thread(new Runnable() {
                        public void run() {
                            ManagerGetVisitOrder managerCancelAvailOrderList = VisitReceptionManager.getGetVisit((AppCompatActivity)getActivity(), null, null, null, null, null, null, tacNumber, 1, 20000);

                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    mWait.dismiss();
                                    if (managerCancelAvailOrderList.getResultCode().equals("S0000")) {
                                        llNoData.setVisibility(View.GONE);
                                        listView.setVisibility(View.VISIBLE);

                                        AdapterMainOrderInquiry adapterMainOrderInquiry = new AdapterMainOrderInquiry((AppCompatActivity)getActivity());
                                        listItem = managerCancelAvailOrderList.getGetVisitOrder();
                                        tvItemCount.setText(listItem.size() + "건 주문이 있습니다.");
                                        adapterMainOrderInquiry.addItem(listItem);
                                        listView.setAdapter(adapterMainOrderInquiry);
                                    } else {
                                        llNoData.setVisibility(View.VISIBLE);
                                        listView.setVisibility(View.GONE);
                                    }
                                }
                            });
                        }
                    }).start();
                }
            }
        }
    };

    /**
     * CustomKeyBoard reset
     *
     * @param view
     */
    private void setCustomKeyBoard(View view) {
        CustomKeyboard1 mCustomKeyboard = new CustomKeyboard1((AppCompatActivity)getActivity(), view, R.id.keyboard_view, R.xml.custom_keyboard);
        mCustomKeyboard.registerEditText(view, R.id.et_name);
    }

}