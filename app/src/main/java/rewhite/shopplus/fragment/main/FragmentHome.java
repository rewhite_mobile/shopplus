package rewhite.shopplus.fragment.main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityMemberDetails;
import rewhite.shopplus.activity.ActivityNewMemberRegistration;
import rewhite.shopplus.client.manager.ManagerGetMainSalesInfo;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.manager.ManagerGetVisitOrderNeedpay;
import rewhite.shopplus.client.manager.ManagerStoreUserList;
import rewhite.shopplus.client.model.GetMainSalesInfo;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.model.StoreUserInfoList;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.client.network.SalesStatusMangerment;
import rewhite.shopplus.client.network.StoreUserApiManager;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.common.CustomKeyboard1;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.common.popup.activity.ActivityOrderDetails;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.CommonUtil;
import rewhite.shopplus.util.CustomKeyboard;
import rewhite.shopplus.util.CustomViewPager;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterMemberSearch;
import rewhite.shopplus.view.adapter.AdapterMemberTacSearch;
import rewhite.shopplus.view.adapter.AdapterTodayPlace;
import rewhite.shopplus.view.adapter.CustomSpinnerAdapter;
import rewhite.shopplus.view.adapter.MainViewPagerSlidingAdapter;

/**
 * Main 홈 화면 카테고리 View
 */
@SuppressLint("ValidFragment")
public class FragmentHome extends Fragment {
    private static final String TAG = "FragmentHome";
    private static final String USER_STORE_ID = "user_store_id";
    private static final String MEMBER_DETAIL = "member_detail";  // 회원정보수정 Type;
    private static final int MAX_LENGTH_TAG_5 = 5;
    private static final int MAX_LENGTH_TAG_8 = 8;
    private static final int MAX_LENGTH_TAG_10 = 10;
    private static final String ITEM_ORDER_ID = "item_order_id";

    private int mPosition;

    private List<StoreUserInfoList> UserPhoneNumberInfo;
    private List<GetMainSalesInfo> getMainSalesInfo;

    ManagerGetVisitOrderNeedpay UserPhoneNumberInfoNeed;
    ManagerGetVisitOrder managerGetVisitOrder;
    private int testType = 1;

    private WaitCounter mWait;

    private static AlertDialog.Builder listViewDialog;
    private static AlertDialog alertDialog;

    private ListView listPlace;
    private CustomViewPager vp;
    private CustomKeyboard mCustomKeyboard;
    //TODO 2차 스펙 고도화
//    private LinearLayout llCollection;
//    private RelativeLayout rlCollection;
//    private TextView tvCollection;
//    private TextView tvCollectionNumber;
//
//    private LinearLayout llDelivery;
//    private RelativeLayout rlDelivery;
//    private TextView tvDelivery;
//    private TextView tvDeliveryNumber;
    private TextView tvSearch;

    private LinearLayout llUnreleased;
    private TextView tvUnreleased;
    private RelativeLayout rlUnreleased;
    private TextView tvUnreleasedCount;

    private LinearLayout llAttemptedDelivery;
    private TextView tvAttemptedDelivery;
    private RelativeLayout rlAttemptedDelivery;
    private TextView tvAttemptedCount;

    private TextView tvYesterdayReceiptCount, tvYesterdayReleaseCount, tvYesterdaySalesCount;
    private TextView tvTodayReceiptCount, tvTodayReleaseCount, tvTodaySalesCount;
    private TextView tvMonthReceiptCount, tvMoneyReleaseCount;

    private static Activity mActivity;
    private Spinner Smember;
    private ViewPager viewPagerImg;

    //    private CustomViewPager viewpager;
    private EditText etMemberSearch;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mWait = new WaitCounter(getActivity());

        init(view);
        return view;
    }

    @SuppressLint("ValidFragment")
    public FragmentHome(Activity activity) {
        mActivity = activity;
    }

    private void init(View view) {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    SalesStatusMangerment.getSalesStatusMangerment((AppCompatActivity) getActivity());

                    UserPhoneNumberInfoNeed = VisitReceptionManager.getGetVisitOrderNeedpay((AppCompatActivity) getActivity(), null, "", "");
                    managerGetVisitOrder = VisitReceptionManager.getGetVisitOrder((AppCompatActivity) getActivity(), "NOTOUT", null, "", "");

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                            getMainSalesInfo = ManagerGetMainSalesInfo.getmInstance().getGetMainSalesInfo();

                            setLayout(view);
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * LayoutView Setting
     *
     * @param view
     */
    private void setLayout(View view) {
        vp = (CustomViewPager) view.findViewById(R.id.custom_viewpager);

        Smember = (Spinner) view.findViewById(R.id.s_member);
        etMemberSearch = (EditText) view.findViewById(R.id.et_name);

        listPlace = (ListView) view.findViewById(R.id.list_place);

        llUnreleased = (LinearLayout) view.findViewById(R.id.ll_unreleased);
        tvUnreleased = (TextView) view.findViewById(R.id.tv_unreleased);
        rlUnreleased = (RelativeLayout) view.findViewById(R.id.rl_unreleased);
        tvUnreleasedCount = (TextView) view.findViewById(R.id.tv_unreleased_count);

        llAttemptedDelivery = (LinearLayout) view.findViewById(R.id.ll_attempted_delivery);
        tvAttemptedDelivery = (TextView) view.findViewById(R.id.tv_attempted_delivery);
        rlAttemptedDelivery = (RelativeLayout) view.findViewById(R.id.rl_attempted_delivery);
        tvAttemptedCount = (TextView) view.findViewById(R.id.tv_attempted_delivery_count);

        tvYesterdayReceiptCount = (TextView) view.findViewById(R.id.tv_yesterday_receipt_count);
        tvYesterdayReleaseCount = (TextView) view.findViewById(R.id.tv_yesterday_release_count);
        tvYesterdaySalesCount = (TextView) view.findViewById(R.id.tv_yesterday_sales_count);

        tvTodayReceiptCount = (TextView) view.findViewById(R.id.tv_today_receipt_count);
        tvTodayReleaseCount = (TextView) view.findViewById(R.id.tv_today_release_count);
        tvTodaySalesCount = (TextView) view.findViewById(R.id.tv_today_sales_count);

        tvMonthReceiptCount = (TextView) view.findViewById(R.id.tv_month_receipt_count);
        tvMoneyReleaseCount = (TextView) view.findViewById(R.id.tv_money_release_count);

//        llCollection = (LinearLayout) view.findViewById(R.id.ll_collection);
//        rlCollection = (RelativeLayout) view.findViewById(R.id.rl_collection);
//        tvCollection = (TextView) view.findViewById(R.id.tv_collection);
//        tvCollectionNumber = (TextView) view.findViewById(R.id.tv_collection_number);
//
//        llDelivery = (LinearLayout) view.findViewById(R.id.ll_delivery);
//        rlDelivery = (RelativeLayout) view.findViewById(R.id.rl_delivery);
//        tvDelivery = (TextView) view.findViewById(R.id.tv_delivery);
//        tvDeliveryNumber = (TextView) view.findViewById(R.id.tv_delivery_number);
        tvSearch = (TextView) view.findViewById(R.id.tv_search);
//
        viewPagerImg = (ViewPager) view.findViewById(R.id.view_pager_img);

//        viewpager = (CustomViewPager) view.findViewById(R.id.move_view_pager);

        LinearLayout llNewMembers = (LinearLayout) view.findViewById(R.id.ll_new_members);
        llNewMembers.setOnClickListener(onClickListener);
        tvSearch.setOnClickListener(onClickListener);

        final String[] arPlanets = getResources().getStringArray(R.array.member_views);
        ArrayList planets = new ArrayList(Arrays.asList(arPlanets));

        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), getContext(), planets);

        Smember.setAdapter(adapter);

//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.member_views, R.layout.spinner_layout);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        Smember.setAdapter(adapter);


        Smember.setOnItemSelectedListener(onItemSelectedListener);

        setCustomKeyBoard(view);
        setTextView();
//        setListPlaceView();
        setViewPagerImg();
//        setVisitViewpagerAdapter();
//        setViewPagerAdapter();
    }

    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 0:
                    mPosition = 0;
                    etMemberSearch.setHint("전화번호 뒷자리를 입력하세요.");
                    etMemberSearch.setMaxLines(4);
                    etMemberSearch.setText("");
                    break;
                case 1:
                    mPosition = 1;
                    etMemberSearch.setHint("택번호를 입력하세요.");
                    etMemberSearch.setText("");
                    setEditTextView();
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private void setEditTextView() {
        etMemberSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String tagNumberEnd = null;
                testType = ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType();
                if (testType == 1) {
                    etMemberSearch.setMaxLines(4);
                    if (start == 3) {
                        etMemberSearch.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_5)});
                        String name = CommonUtil.getTagFormmater(s.toString().substring(0, 1).toString());

                        etMemberSearch.setText(name + s.toString().substring(1, s.toString().length()));
                        etMemberSearch.setSelection(etMemberSearch.getText().toString().length());

                    } else if (start == 4) {
                        etMemberSearch.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_5)});
                        tagNumberEnd = s.toString().substring(0, 2);
                        String name = CommonUtil.getTagFormmater((tagNumberEnd.toString()));

                        etMemberSearch.setText(s.toString().replace(tagNumberEnd, name));
                        etMemberSearch.setSelection(etMemberSearch.getText().toString().length());
                    }
                } else if (testType == 2) {
                    etMemberSearch.setMaxLines(7);
                    etMemberSearch.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_8)});
                    if (start == 4) {
                        String name = CommonUtil.getTagFormmater(s.toString().substring(1, 2).toString());
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(1, 2);
                        stringBuffer.insert(1, name);

                        String testName = stringBuffer.toString();

                        etMemberSearch.setText(testName);
                        etMemberSearch.setSelection(etMemberSearch.getText().toString().length());

                    } else if (start == 5) {
                        String deleteNmae = CommonUtil.getSubTagFormmater(s.toString().substring(1, 2).toString());

                        String name = CommonUtil.getTagFormmater(s.toString().substring(2, 3).toString());
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(1, 2);
                        stringBuffer.insert(1, deleteNmae);
                        stringBuffer.delete(2, 3);
                        stringBuffer.insert(2, name);

                        String testName = stringBuffer.toString();
                        etMemberSearch.setText(testName);
                        etMemberSearch.setSelection(etMemberSearch.getText().toString().length());

                    } else if (start == 6) {
                        String deleteNmae = CommonUtil.getSubTagFormmater(s.toString().substring(2, 3).toString());

                        String name = CommonUtil.getTagFormmater(s.toString().substring(3, 4).toString());
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(2, 3);
                        stringBuffer.insert(2, deleteNmae);
                        stringBuffer.delete(3, 4);
                        stringBuffer.insert(3, name);

                        String testName = stringBuffer.toString();
                        etMemberSearch.setText(testName);
                        etMemberSearch.setSelection(etMemberSearch.getText().toString().length());
                    }

                } else if (testType == 3) {
                    etMemberSearch.setMaxLines(8);
                    etMemberSearch.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH_TAG_10)});
                    String name = null;
                    String nameTo = null;
                    if (start == 4) {
                        name = s.toString().substring(0, 1);
                        nameTo = s.toString().substring(1, 5);

                        etMemberSearch.setText(name + "-" + nameTo);
                        etMemberSearch.setSelection(etMemberSearch.getText().toString().length());
                    } else if (start == 6) {
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(1, 2);
                        stringBuffer.insert(2, "-");

                        etMemberSearch.setText(stringBuffer.toString());
                        etMemberSearch.setSelection(etMemberSearch.getText().toString().length());
                    } else if (start == 7) {
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(2, 3);
                        stringBuffer.insert(3, "-");

                        etMemberSearch.setText(stringBuffer.toString());
                        etMemberSearch.setSelection(etMemberSearch.getText().toString().length());
                    } else if (start == 8) {
                        StringBuffer stringBuffer = new StringBuffer(s.toString());

                        stringBuffer.delete(3, 4);
                        stringBuffer.insert(4, "-");

                        etMemberSearch.setText(stringBuffer.toString());
                        etMemberSearch.setSelection(etMemberSearch.getText().toString().length());

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setTextView() {
        try {
            tvUnreleasedCount.setText(String.valueOf(managerGetVisitOrder.itemGetVisitOrder.size()));
            tvAttemptedCount.setText(String.valueOf(UserPhoneNumberInfoNeed.itemGetVisitOrderNeedPay.size()));

            tvYesterdayReceiptCount.setText(String.valueOf(getMainSalesInfo.get(0).getData().getYesterDay_EnterCount()) + "건");
            tvYesterdayReleaseCount.setText(String.valueOf(getMainSalesInfo.get(0).getData().getYesterDay_OutCount()) + "건");
            tvYesterdaySalesCount.setText(DataFormatUtil.moneyFormatToWon(getMainSalesInfo.get(0).getData().getYesterDay_Sales()) + "원");

            tvTodayReceiptCount.setText(String.valueOf(getMainSalesInfo.get(0).getData().getToday_EnterCount()) + "건");
            tvTodayReleaseCount.setText(String.valueOf(getMainSalesInfo.get(0).getData().getToday_OutCount()) + "건");
            tvTodaySalesCount.setText(DataFormatUtil.moneyFormatToWon(getMainSalesInfo.get(0).getData().getToday_Sales()) + "원");

            tvMonthReceiptCount.setText(DataFormatUtil.moneyFormatToWon(getMainSalesInfo.get(0).getData().getMonth_Sales()) + "원");
            tvMoneyReleaseCount.setText(DataFormatUtil.moneyFormatToWon((int) Math.floor(getMainSalesInfo.get(0).getData().getDay_Average_Sales())) + "원");

            llUnreleased.setOnClickListener(movePageListener);
            llUnreleased.setTag(0);
            llAttemptedDelivery.setOnClickListener(movePageListener);
            llAttemptedDelivery.setTag(1);

            FragmentManager fragmentManager = getFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            FragmentMainUnreleased fragmentMainUnreleased = new FragmentMainUnreleased();
            fragmentTransaction.replace(R.id.fragment_home, fragmentMainUnreleased);
            fragmentTransaction.commit();

        } catch (Exception err) {
            err.printStackTrace();
        }
    }


    /**
     * ViewPager Timer for Auto Sliding
     */
    private void setViewPagerImg() {
        final int noofsize = 3;
        final int[] count = {0};

        MainViewPagerSlidingAdapter adapter = new MainViewPagerSlidingAdapter((AppCompatActivity) getActivity(), noofsize);
        viewPagerImg.setAdapter(adapter);
        viewPagerImg.setCurrentItem(0);

        // Timer for auto sliding
        Timer timer = new Timer();
        try {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (count[0] <= noofsize) {
                                viewPagerImg.setCurrentItem(count[0]);
                                count[0]++;
                            } else {
                                count[0] = 0;
                                viewPagerImg.setCurrentItem(count[0]);
                            }
                        }
                    });
                }
            }, 500, 3000);
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * Click Action Listener Setting
     */
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            Intent intent = null;
            switch (id) {
                case R.id.ll_new_members:   //신규회원등록
                    intent = new Intent(getActivity(), ActivityNewMemberRegistration.class);
                    intent.putExtra(MEMBER_DETAIL, "1");
                    startActivity(intent);
                    break;

                case R.id.tv_search:
                    mWait.show();
                    if (mPosition == 0) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                getActivity().runOnUiThread(new Runnable() {
                                    ManagerStoreUserList storeUserInfo = StoreUserApiManager.getStoreUserList((AppCompatActivity) getActivity(), null, etMemberSearch.getText().toString());

                                    @Override
                                    public void run() {
                                        mWait.dismiss();
                                        if (etMemberSearch.getText().toString().length() == 4) {
                                            if (storeUserInfo.getStoreUserInfo().size() == 0) {
                                                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getContext());
                                                thirdAlertPopup.setTitle("오류");
                                                thirdAlertPopup.setContent("등록된 핸드폰 번호가 존재하지 않습니다.");
                                                thirdAlertPopup.setButtonText("확인");
                                                thirdAlertPopup.show();
                                            } else {
                                                setMemberSearchList(storeUserInfo);
                                            }
                                        } else {
                                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getContext());
                                            thirdAlertPopup.setTitle("오류");
                                            thirdAlertPopup.setContent("핸드폰 뒷자리 4자리를 입력해주세요.");
                                            thirdAlertPopup.setButtonText("확인");
                                            thirdAlertPopup.show();
                                        }
                                    }
                                });
                            }
                        }).start();
                    } else if (mPosition == 1) {
                        String tacNumber = null;
                        mWait.dismiss();

                        Logger.d(TAG, "etMemberSearch.getText().toString() : " + etMemberSearch.getText().toString());
                        if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 1) {
                            Logger.d(TAG, "일제");
                            tacNumber = CommonUtil.setTagFormaterChangeResponse(etMemberSearch.getText().toString());
                        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 2) {
                            Logger.d(TAG, "바코드");
                            tacNumber = CommonUtil.setTagFormaterChangeTypeResponse(etMemberSearch.getText().toString());
                        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getTagNoType() == 3) {
                            Logger.d(TAG, "기타");
                            tacNumber = CommonUtil.setEtcTagFormaterChangeResponse(etMemberSearch.getText().toString());
                        }
                        Logger.d(TAG, "tacNumber : " + tacNumber);
                        setTacNumberSearch(tacNumber);
                    }

                    break;
            }
        }
    };

    /**
     * 택번호 검색 결과 조회
     *
     * @param tacNumber
     */
    private void setTacNumberSearch(String tacNumber) {
        new Thread(new Runnable() {
            public void run() {
                ManagerGetVisitOrder managerGetVisitOrder = VisitReceptionManager.getGetVisit((AppCompatActivity) getActivity(), null, null, null, null, null, null, tacNumber, 1, 1000);
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        List<GetVisitOrderList> getVisitOrderLists = managerGetVisitOrder.getGetVisitOrder();

                        if (getVisitOrderLists.size() == 0) {
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());
                            thirdAlertPopup.setTitle("오류팝업");
                            thirdAlertPopup.setContent("조회된 결과가 없어요.");
                            thirdAlertPopup.setButtonText("확인");
                            thirdAlertPopup.show();
                        } else if (getVisitOrderLists.size() == 1) {
                            Intent intent = new Intent(getActivity(), ActivityOrderDetails.class);
                            intent.putExtra(ITEM_ORDER_ID, getVisitOrderLists.get(0).getOrderId());
                            startActivity(intent);
                        } else {
                            listViewDialog = new AlertDialog.Builder(mActivity);
                            alertDialog = listViewDialog.create();
                            View view = mActivity.getLayoutInflater().inflate(R.layout.dialog_member_search_list, null);

                            WindowManager.LayoutParams params = alertDialog.getWindow().getAttributes();
                            params.width = 800;
                            params.height = 500;
                            alertDialog.getWindow().setAttributes(params);

                            ListView listview = (ListView) view.findViewById(R.id.lv_member_list);
                            LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);

                            AdapterMemberTacSearch adapterMemberSearch = new AdapterMemberTacSearch(mActivity, onContentItemClickListener);
                            adapterMemberSearch.addItem(getVisitOrderLists);
                            listview.setAdapter(adapterMemberSearch);

                            llClose.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(mActivity);
                                    thirdConfirmPopup.setTitle("종료팝업");
                                    thirdConfirmPopup.setContent("팝업을 종료할까요?");
                                    thirdConfirmPopup.setButtonText("예");
                                    thirdConfirmPopup.setCancelButtonText("아니오");
                                    thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                        @Override
                                        public void onConfirmClick() {
                                            alertDialog.dismiss();
                                        }
                                    });
                                    thirdConfirmPopup.show();
                                }
                            });

                            alertDialog.setView(view);
                            alertDialog.show();
                        }

                    }
                });
            }
        }).start();
    }

    /**
     * 회원 리스트 DialogView
     *
     * @param storeUserInfo
     */
    public static void getMemberSearchList(ManagerStoreUserList storeUserInfo) {
        listViewDialog = new AlertDialog.Builder(mActivity);
        alertDialog = listViewDialog.create();

        List<StoreUserInfoList> UserPhoneNumberInfo = storeUserInfo.getStoreUserInfo();

        View view = mActivity.getLayoutInflater().inflate(R.layout.item_member_search, null);
        AdapterMemberSearch adapterMemberSearch = new AdapterMemberSearch(mActivity, onContentItemClickListener);

        ListView listview = (ListView) view.findViewById(R.id.lv_member_list);
        LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);
        TextView tvMemberSearchCount = (TextView) view.findViewById(R.id.tv_member_search_count);
        tvMemberSearchCount.setText(storeUserInfo.getStoreUserInfo().size() + "건의 검색결과가 있습니다.");

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO Check dismiss
                alertDialog.dismiss();
            }
        });

        adapterMemberSearch.addItem(UserPhoneNumberInfo);
        listview.setAdapter(adapterMemberSearch);

        alertDialog.setView(view);
        alertDialog.show();
    }

    /**
     * MemberList Search List
     */
    private void setMemberSearchList(ManagerStoreUserList storeUserInfo) {
        listViewDialog = new AlertDialog.Builder(getActivity());
        alertDialog = listViewDialog.create();

        UserPhoneNumberInfo = storeUserInfo.getStoreUserInfo();

        View view = getActivity().getLayoutInflater().inflate(R.layout.item_member_search, null);
        AdapterMemberSearch adapterMemberSearch = new AdapterMemberSearch(getContext(), onContentItemClickListener);

        ListView listview = (ListView) view.findViewById(R.id.lv_member_list);
        LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);
        TextView tvMemberSearchCount = (TextView) view.findViewById(R.id.tv_member_search_count);
        tvMemberSearchCount.setText(storeUserInfo.getStoreUserInfo().size() + "건의 검색결과가 있습니다.");

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO Check dismiss
                alertDialog.dismiss();
            }
        });

        adapterMemberSearch.addItem(UserPhoneNumberInfo);
        listview.setAdapter(adapterMemberSearch);

        alertDialog.setView(view);
        alertDialog.show();
    }

    /**
     * Adapter View Click Event Data Interface
     */
    public interface onContentItemClickListener {
        void onClick(int position);

        void memberSearch(String tacNumber);
    }

    public static onContentItemClickListener onContentItemClickListener = new onContentItemClickListener() {
        @Override
        public void onClick(int storeUserID) {
            SettingLogic.setStoreUserId(mActivity, String.valueOf(storeUserID));

            Intent intent = new Intent(mActivity, ActivityMemberDetails.class);
            intent.putExtra(USER_STORE_ID, storeUserID);
            mActivity.startActivity(intent);
            alertDialog.dismiss();
        }

        @Override
        public void memberSearch(String tacNumber) {
            new Thread(new Runnable() {
                public void run() {
                    ManagerGetVisitOrder managerGetVisitOrder = VisitReceptionManager.getGetVisit((AppCompatActivity) mActivity, null, null, null, null, null, null, tacNumber, 1, 1000);
                    mActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            List<GetVisitOrderList> getVisitOrderLists = managerGetVisitOrder.getGetVisitOrder();
                            StoreUserApiManager.getStoreUserInfo((AppCompatActivity) mActivity, getVisitOrderLists.get(0).getStoreUserID());

                            Intent intent = new Intent(mActivity, ActivityOrderDetails.class);
                            intent.putExtra(ITEM_ORDER_ID, getVisitOrderLists.get(0).getOrderId());
                            mActivity.startActivity(intent);
                        }
                    });
                }
            }).start();
        }
    };

    /**
     * CustomKeyBoard reset
     *
     * @param view
     */
    private void setCustomKeyBoard(View view) {
        InputMethodManager immhide = (InputMethodManager) getActivity().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        immhide.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        CustomKeyboard1 mCustomKeyboard = new CustomKeyboard1((AppCompatActivity) getActivity(), view, R.id.key_boardview, R.xml.custom_keyboard);
        mCustomKeyboard.registerEditText(view, R.id.et_name);
    }

    private void setListPlaceView() {
        ArrayList<String> name = new ArrayList<>();

        name.add("정해인");
        name.add("정해인");
        name.add("정해인");
        name.add("정해인");
        name.add("정해인");

        AdapterTodayPlace adapterTodayPlace = new AdapterTodayPlace(getContext());
        adapterTodayPlace.addItem(name);
        listPlace.setAdapter(adapterTodayPlace);
    }


    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            vp.setCurrentItem(tag);
            FragmentManager fragmentManager = getFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            switch (tag) {
                case 0:
                    FragmentMainUnreleased fragmentMainUnreleased = new FragmentMainUnreleased();
                    fragmentTransaction.replace(R.id.fragment_home, fragmentMainUnreleased);
                    fragmentTransaction.commit();

                    llUnreleased.setBackgroundResource(R.drawable.a_button_type_2);
                    rlUnreleased.setBackgroundResource(R.drawable.icon_common_numbg_01);
                    tvUnreleased.setTextColor(getResources().getColor(R.color.Wite_color));
                    tvUnreleasedCount.setTextColor(getResources().getColor(R.color.Wite_color));

                    llAttemptedDelivery.setBackgroundResource(R.drawable.a_button_type_3);
                    rlAttemptedDelivery.setBackgroundResource(R.drawable.a_button_type_7);
                    tvAttemptedDelivery.setTextColor(getResources().getColor(R.color.color_9b9b9b));
                    tvAttemptedCount.setTextColor(getResources().getColor(R.color.Wite_color));
//                  setLlCollection();
                    break;
                case 1:
                    FragmentMainAttemptedDelivery fragmentMainAttemptedDelivery = new FragmentMainAttemptedDelivery();
                    fragmentTransaction.replace(R.id.fragment_home, fragmentMainAttemptedDelivery);
                    fragmentTransaction.commit();

                    llUnreleased.setBackgroundResource(R.drawable.a_button_type_3);
                    rlUnreleased.setBackgroundResource(R.drawable.a_button_type_7);
                    tvUnreleased.setTextColor(getResources().getColor(R.color.color_9b9b9b));
                    tvUnreleasedCount.setTextColor(getResources().getColor(R.color.Wite_color));


                    llAttemptedDelivery.setBackgroundResource(R.drawable.a_button_type_2);
                    rlAttemptedDelivery.setBackgroundResource(R.drawable.icon_common_numbg_01);
                    tvAttemptedDelivery.setTextColor(getResources().getColor(R.color.Wite_color));
                    tvAttemptedCount.setTextColor(getResources().getColor(R.color.Wite_color));
//                  setLlDelivery();
                    break;
            }
        }
    };

//    /**
//     * Collection / Delivery viewPager Setting
//     */
//    //TODO 2차 스펙
//    private void setViewPagerAdapter() {
//        vp.setAdapter(new AdapterTodayPager(getActivity().getSupportFragmentManager()));
//        vp.setCurrentItem(0);
//
//        llUnreleased.setOnClickListener(movePageListener);
//        llUnreleased.setTag(0);
//        llAttemptedDelivery.setOnClickListener(movePageListener);
//        llAttemptedDelivery.setTag(1);
//    }

//    /**
//     * Collection Click View Setting
//     */
//    private void setLlCollection() {
//        llCollection.setBackgroundResource(R.drawable.btn_common_tab_press_00);
//        rlCollection.setBackgroundResource(R.drawable.icon_common_numbg_00);
//        tvCollection.setTextColor(getResources().getColor(R.color.Wite_color));
//        tvCollectionNumber.setTextColor(getResources().getColor(R.color.color_0fc5dc));
//
//        llDelivery.setBackgroundResource(R.drawable.btn_common_tab_normal_00);
//        rlDelivery.setBackgroundResource(R.drawable.icon_common_numbg_01);
//        tvDelivery.setTextColor(getResources().getColor(R.color.color_0fc5dc));
//        tvDeliveryNumber.setTextColor(getResources().getColor(R.color.Wite_color));
//    }
//
//    /**
//     * Delivery Click View Setting
//     */
//    private void setLlDelivery() {
//        llDelivery.setBackgroundResource(R.drawable.btn_common_tab_press_00);
//        rlDelivery.setBackgroundResource(R.drawable.icon_common_numbg_00);
//        tvDelivery.setTextColor(getResources().getColor(R.color.Wite_color));
//        tvDeliveryNumber.setTextColor(getResources().getColor(R.color.color_0fc5dc));
//
//        llCollection.setBackgroundResource(R.drawable.btn_common_tab_normal_00);
//        rlCollection.setBackgroundResource(R.drawable.icon_common_numbg_01);
//        tvCollection.setTextColor(getResources().getColor(R.color.color_0fc5dc));
//        tvCollectionNumber.setTextColor(getResources().getColor(R.color.Wite_color));
//    }

    @Override
    public void onResume() {
        super.onResume();
        EnviromentManager.getManagerStoreManageInfo((AppCompatActivity) mActivity);
    }
}