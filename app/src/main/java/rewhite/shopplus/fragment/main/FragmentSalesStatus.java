package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.util.CustomViewPager;
import rewhite.shopplus.view.adapter.AdapterOperationManagement;

/**
 * Main 운영관리 카테고리 View
 */
public class FragmentSalesStatus extends Fragment {
    private static final String TAG = "FragmentSalesStatus";
    private CustomViewPager viewPager;

    private LinearLayout llMembershipManagement;
    private ImageView ivMembershipManagement;
    private TextView tvMembershipManagement;

    private LinearLayout llAcountsReceivableManagement;
    private ImageView ivAcountsReceivableManagement;
    private TextView tvAcountsReceivableManagement;

    private LinearLayout llNonIssueManagement;
    private ImageView ivNonIssueManagement;
    private TextView tvNonIssueManagement;

    private LinearLayout llCardPaymentManagement;
    private ImageView ivCardPaymentManagement;
    private TextView tvCardPaymentManagement;

    private LinearLayout llPreChargeManagement;
    private ImageView ivPreChargeManagement;
    private TextView tvPreChargeManagementt;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sales_status, container, false);

//        setLayout(view);
//        setViewPagerAdapter();

        return view;
    }

    private void setLayout(View view) {
        llMembershipManagement = (LinearLayout) view.findViewById(R.id.ll_membership_management);
        ivMembershipManagement = (ImageView) view.findViewById(R.id.iv_membership_management);
        tvMembershipManagement = (TextView) view.findViewById(R.id.tv_membership_management);

        llAcountsReceivableManagement = (LinearLayout) view.findViewById(R.id.ll_accounts_receivable_management);
        ivAcountsReceivableManagement = (ImageView) view.findViewById(R.id.iv_accounts_receivable_management);
        tvAcountsReceivableManagement = (TextView) view.findViewById(R.id.tv_accounts_receivable_management);

        llNonIssueManagement = (LinearLayout) view.findViewById(R.id.ll_non_issue_management);
        ivNonIssueManagement = (ImageView) view.findViewById(R.id.iv_non_issue_management);
        tvNonIssueManagement = (TextView) view.findViewById(R.id.tv_non_issue_management);

        llCardPaymentManagement = (LinearLayout) view.findViewById(R.id.ll_card_payment_management);
        ivCardPaymentManagement = (ImageView) view.findViewById(R.id.iv_card_payment_management);
        tvCardPaymentManagement = (TextView) view.findViewById(R.id.tv_card_payment_management);

        llPreChargeManagement = (LinearLayout) view.findViewById(R.id.ll_pre_charge_management);
        ivPreChargeManagement = (ImageView) view.findViewById(R.id.iv_pre_charge_management);
        tvPreChargeManagementt = (TextView) view.findViewById(R.id.tv_pre_charge_management);

        viewPager = (CustomViewPager) view.findViewById(R.id.vp_sales_status);
    }


    private void setViewPagerAdapter() {
        viewPager.setAdapter(new AdapterOperationManagement(getActivity().getSupportFragmentManager()));
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(4);
        setTapBackGround(0);

        llMembershipManagement.setOnClickListener(movePageListener);
        llMembershipManagement.setTag(0);
        llAcountsReceivableManagement.setOnClickListener(movePageListener);
        llAcountsReceivableManagement.setTag(1);
        llNonIssueManagement.setOnClickListener(movePageListener);
        llNonIssueManagement.setTag(2);
        llCardPaymentManagement.setOnClickListener(movePageListener);
        llCardPaymentManagement.setTag(3);
        llPreChargeManagement.setOnClickListener(movePageListener);
        llPreChargeManagement.setTag(4);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            viewPager.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    /**
     * 탭 UI 변경 분기처리 로직
     *
     * @param tag
     */
    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                setMembershipManagementSelect();
                break;
            case 1:
                setAcountsReceivableManagementSelect();
                break;
            case 2:
                setNonIssueManagementSelect();
                break;
            case 3:
                setCardPaymentManagementSelect();
                break;
            case 4:
                setPreChargeManagementSelect();
                break;
        }
    }

    private void setMembershipManagementSelect() {
        tvMembershipManagement.setTextColor(getResources().getColor(R.color.color_43425c));
        tvAcountsReceivableManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvNonIssueManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvCardPaymentManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvPreChargeManagementt.setTextColor(getResources().getColor(R.color.Wite_color));
    }

    private void setAcountsReceivableManagementSelect() {
        tvAcountsReceivableManagement.setTextColor(getResources().getColor(R.color.color_43425c));
        tvMembershipManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvNonIssueManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvCardPaymentManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvPreChargeManagementt.setTextColor(getResources().getColor(R.color.Wite_color));
    }

    private void setNonIssueManagementSelect() {
        tvNonIssueManagement.setTextColor(getResources().getColor(R.color.color_43425c));
        tvMembershipManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvAcountsReceivableManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvCardPaymentManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvPreChargeManagementt.setTextColor(getResources().getColor(R.color.Wite_color));
    }

    private void setCardPaymentManagementSelect() {
        tvCardPaymentManagement.setTextColor(getResources().getColor(R.color.color_43425c));
        tvAcountsReceivableManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvMembershipManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvNonIssueManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvPreChargeManagementt.setTextColor(getResources().getColor(R.color.Wite_color));
    }

    private void setPreChargeManagementSelect() {
        tvPreChargeManagementt.setTextColor(getResources().getColor(R.color.color_43425c));
        tvAcountsReceivableManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvMembershipManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvNonIssueManagement.setTextColor(getResources().getColor(R.color.Wite_color));
        tvCardPaymentManagement.setTextColor(getResources().getColor(R.color.Wite_color));
    }
}
