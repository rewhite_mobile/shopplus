package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.view.adapter.AdapterOrderInquiry;
import rewhite.shopplus.util.CustomViewPager;

/**
 * 주문조회 View
 */
public class FragmentOrderInquiry extends Fragment {
    private static final String TAG = "FragmentOrderInquiry";

    private CustomViewPager viewPagerMember;

    private TextView tvDetailedView, tvQuickView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_inquiry, container, false);

        setLayout(view);
        setViewPagerAdapter();
        return view;
    }

    private void setLayout(View view) {
        viewPagerMember = (CustomViewPager) view.findViewById(R.id.view_pager_member);
        tvDetailedView = (TextView) view.findViewById(R.id.tv_detailed_view);
        tvQuickView = (TextView) view.findViewById(R.id.tv_quick_view);
    }


    private void setViewPagerAdapter() {
        viewPagerMember.setAdapter(new AdapterOrderInquiry(getActivity().getSupportFragmentManager()));
        viewPagerMember.setCurrentItem(0);
        setTapBackGround(0);

        tvDetailedView.setOnClickListener(movePageListener);
        tvDetailedView.setTag(0);
        tvQuickView.setOnClickListener(movePageListener);
        tvQuickView.setTag(1);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            viewPagerMember.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    /**
     * 탭 UI 변경 분기처리 로직
     *
     * @param tag
     */
    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                tvDetailedView.setBackgroundResource(R.drawable.a_button_type_2);                // 월별매출현황
                tvDetailedView.setTextColor(getResources().getColor(R.color.color_c7cfe8));

                tvQuickView.setBackgroundResource(R.drawable.a_button_type_3);
                tvQuickView.setTextColor(getResources().getColor(R.color.color_43425c));
                break;

            case 1:
                tvQuickView.setBackgroundResource(R.drawable.a_button_type_2);                  // 일별매출현황
                tvQuickView.setTextColor(getResources().getColor(R.color.color_c7cfe8));

                tvDetailedView.setBackgroundResource(R.drawable.a_button_type_3);
                tvDetailedView.setTextColor(getResources().getColor(R.color.color_43425c));
                break;
        }
    }

}
