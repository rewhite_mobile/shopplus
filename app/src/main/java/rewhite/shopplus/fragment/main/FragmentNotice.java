package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerCutomerModel;
import rewhite.shopplus.client.model.CutomerModel;
import rewhite.shopplus.client.network.CustomerManager;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterServiceCenterList;

/**
 * 공지사항
 */
public class FragmentNotice extends Fragment {
    private static final String TAG = "FragmentNotice";
    private static final int MAX_COUNT = 8;     //총 노출 카운트
    private View view;

    private WaitCounter mWait;
    private ListView listView;
    private ManagerCutomerModel managerCutomerModel;

    private Button btnPrve, btnNext;

    private WebView webView;
    private WebSettings webSettings;

    private int pageCount = 1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notice, container, false);
        mWait = new WaitCounter(getActivity());

        init(pageCount);

        return view;
    }

    private void init(int page) {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    managerCutomerModel = CustomerManager.getNoticeListe((AppCompatActivity)getActivity(), page, MAX_COUNT);
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            setLayout();
                            loadList();
                            mWait.dismiss();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private void setLayout() {
        listView = (ListView) view.findViewById(R.id.list_view);
        webView = (WebView) view.findViewById(R.id.web_view);

        webView.setWebViewClient(new WebViewClient());
        webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(managerCutomerModel.getCutomerModel().get(0).getWebViewUrl());

        btnPrve = (Button) view.findViewById(R.id.btn_prve);
        btnNext = (Button) view.findViewById(R.id.btn_next);

        btnPrve.setOnClickListener(btnOnClickListener);
        btnNext.setOnClickListener(btnOnClickListener);
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {

                case R.id.btn_prve:
                    --pageCount;
                    if (pageCount == 1) {
                        btnPrve.setVisibility(View.GONE);
                    }
//
//                    if (managerCutomerModel.getCutomerModel().get(0).getLastPage() != pageCount) {
//                        btnNext.setVisibility(View.VISIBLE);
//                    }

                    init(pageCount);
                    break;

                case R.id.btn_next:
                    if (pageCount >= 2) {
                        btnPrve.setVisibility(View.VISIBLE);
                    }
//
//                    if (managerCutomerModel.getCutomerModel().get(0)..get(0).getLastPage() == pageCount) {
//                        btnNext.setVisibility(View.GONE);
//                    }

                    ++pageCount;
                    init(pageCount);
                    break;
            }
        }
    };

    private void loadList() {
        List<CutomerModel> itemCutomerModel = managerCutomerModel.getCutomerModel();

        AdapterServiceCenterList adapterServiceCenterList = new AdapterServiceCenterList(noticeData);

        adapterServiceCenterList.addItem(itemCutomerModel);
        listView.setAdapter(adapterServiceCenterList); //카테고리 리스트
    }

    public interface NoticeData {
        void webViewUrl(String Url);
    }

    private NoticeData noticeData = new NoticeData() {
        @Override
        public void webViewUrl(String Url) {
            webView.setWebViewClient(new WebViewClient());
            webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            webView.loadUrl(Url);
        }
    };
}
