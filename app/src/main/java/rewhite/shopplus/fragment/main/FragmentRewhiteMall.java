package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rewhite.shopplus.R;

/**
 * Main 리화이트몰 카테고리 View
 */
public class FragmentRewhiteMall extends Fragment {

    private static final String TAG = "FragmentRewhiteMall";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rewhite_mall, container, false);

        setLayout(view);

        return view;
    }

    private void setLayout(View view) {

    }
}