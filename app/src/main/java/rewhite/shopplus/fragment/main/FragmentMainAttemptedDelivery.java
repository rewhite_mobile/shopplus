package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetVisitOrderNeedpay;
import rewhite.shopplus.client.model.GetVisitOrderListNeed;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.view.adapter.AdapterUnreleasedNeed;

public class FragmentMainAttemptedDelivery extends Fragment {
    private static final String TAG = "FragmentMainAttemptedDelivery";

    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_attempted_delivery, container, false);

        Logger.d(TAG, "FragmentMainAttemptedDelivery");
        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        listView = (ListView) view.findViewById(R.id.list_view);

        List<GetVisitOrderListNeed> getVisitOrderListNeeds = ManagerGetVisitOrderNeedpay.getmInstance().getGetVisitOrderNeedPay();

        AdapterUnreleasedNeed adapterUnreleased = new AdapterUnreleasedNeed(getContext());
        adapterUnreleased.addItem(getVisitOrderListNeeds);
        listView.setAdapter(adapterUnreleased);
    }


}