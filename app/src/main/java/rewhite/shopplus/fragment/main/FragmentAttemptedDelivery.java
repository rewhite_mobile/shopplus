package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.view.adapter.AdapterCollection;

public class FragmentAttemptedDelivery extends Fragment {
    private static final String TAG = "FragmentAttemptedDelivery";
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_attempted_delivery, container, false);
        setLayout(view);

        return view;
    }

    private void setLayout(View view) {
        listView = (ListView) view.findViewById(R.id.lv);

        ArrayList<String> name = new ArrayList<>();

        name.add("정해인");
        name.add("정해인");
        name.add("정해인");
        name.add("정해인");
        name.add("정해인");


        AdapterCollection adapterFragmentHome = new AdapterCollection(getContext());
        adapterFragmentHome.addItem(name);
        listView.setAdapter(adapterFragmentHome);
    }
}
