package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.util.CustomViewPager;
import rewhite.shopplus.view.adapter.AdapterSalesStatus;

/**
 * Main 운영관리 카테고리 View
 */
public class FragmentOperationsManagement extends Fragment {

    private static final String TAG = "FragmentOperationsManagement";
    private CustomViewPager vpSalesStatus;

    private LinearLayout llMonthlySalesStatus;
    private ImageView ivIconMonthlySalesStatus;
    private TextView tvTabMonthlySalesStatus;

    private LinearLayout llDailySalesStatus;
    private ImageView ivIconDailySalesStatus;
    private TextView tvTabDailySalesStatus;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_operations_management, container, false);

        setLayout(view);
        setViewPagerAdapter();

        return view;
    }

    private void setLayout(View view) {
        llMonthlySalesStatus = (LinearLayout) view.findViewById(R.id.ll_monthly_sales_status);
        ivIconMonthlySalesStatus = (ImageView) view.findViewById(R.id.iv_icon_monthly_sales_status);
        tvTabMonthlySalesStatus = (TextView) view.findViewById(R.id.tv_tab_monthly_sales_status);

        llDailySalesStatus = (LinearLayout) view.findViewById(R.id.ll_daily_sales_status);
        ivIconDailySalesStatus = (ImageView) view.findViewById(R.id.iv_icon_daily_sales_status);
        tvTabDailySalesStatus = (TextView) view.findViewById(R.id.tv_tab_daily_sales_status);

        vpSalesStatus = (CustomViewPager) view.findViewById(R.id.vp_operations);
    }

    private void setViewPagerAdapter() {
        vpSalesStatus.setAdapter(new AdapterSalesStatus(getActivity().getSupportFragmentManager()));
        vpSalesStatus.setCurrentItem(0);
        vpSalesStatus.setOffscreenPageLimit(2);
        setTapBackGround(0);

        llMonthlySalesStatus.setOnClickListener(movePageListener);
        llMonthlySalesStatus.setTag(0);
        llDailySalesStatus.setOnClickListener(movePageListener);
        llDailySalesStatus.setTag(1);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            vpSalesStatus.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    /**
     * 탭 UI 변경 분기처리 로직
     *
     * @param tag
     */
    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                tvTabMonthlySalesStatus.setTextColor(getResources().getColor(R.color.color_43425c));
                tvTabDailySalesStatus.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                break;

            case 1:
                tvTabDailySalesStatus.setTextColor(getResources().getColor(R.color.color_43425c));
                tvTabMonthlySalesStatus.setTextColor(getResources().getColor(R.color.color_c7cfe8));
                break;
        }
    }
}