package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterMainOrderDetail;

public class FragmentOrderDetailView extends Fragment {
    private static final String TAG = "FragmentOrderDetailView";

    private ListView listView;
    private LinearLayout llBg, llNoData;

    private TextView tvAll, tvYesterday, tvToday, tv1week, tv1month, tv3month, tv6month, tv1year;

    private RelativeLayout rlAll, rlBlue, rlOrange, rlRed;
    private ImageView imgAll, imgBlue, imgOrange, imgRed;
    private TextView tvMemberAll, tvItemCount;
    private WaitCounter mWait;
    private List<GetVisitOrderList> listItem;
    private String startDate = null, endDate = null;
    private String userGrade;
    private String orderStatus;

    private Button btnSerch;
    private Spinner Smember;

    private int mPrice, mNonPrice;

    private TextView tvTotalItemCount, tvTotalPayPrice, tvNonPayPrice;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_detail_view, container, false);
        mWait = new WaitCounter(getActivity());
        init(view);
        return view;
    }

    private void init(View view) {
        setLayout(view);
    }


    private void setLayout(View view) {
        llBg = (LinearLayout) view.findViewById(R.id.ll_bg);
        listView = (ListView) view.findViewById(R.id.list_view);
        llNoData = (LinearLayout) view.findViewById(R.id.ll_no_data);
        Smember = (Spinner) view.findViewById(R.id.s_member);

        tvAll = (TextView) view.findViewById(R.id.tv_all);
        tvYesterday = (TextView) view.findViewById(R.id.tv_yesterday);
        tvToday = (TextView) view.findViewById(R.id.tv_today);
        tv1week = (TextView) view.findViewById(R.id.tv_1week);
        tv1month = (TextView) view.findViewById(R.id.tv_1month);
        tv3month = (TextView) view.findViewById(R.id.tv_3month);
        tv6month = (TextView) view.findViewById(R.id.tv_6month);
        tv1year = (TextView) view.findViewById(R.id.tv_1year);
        tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);

        rlAll = (RelativeLayout) view.findViewById(R.id.rl_all);
        rlBlue = (RelativeLayout) view.findViewById(R.id.rl_blue);
        rlOrange = (RelativeLayout) view.findViewById(R.id.rl_orange);
        rlRed = (RelativeLayout) view.findViewById(R.id.rl_red);

        imgAll = (ImageView) view.findViewById(R.id.img_all);
        imgBlue = (ImageView) view.findViewById(R.id.img_blue);
        imgOrange = (ImageView) view.findViewById(R.id.img_orange);
        imgRed = (ImageView) view.findViewById(R.id.img_red);

        tvTotalItemCount = (TextView) view.findViewById(R.id.tv_total_item_count);
        tvTotalPayPrice = (TextView) view.findViewById(R.id.tv_total_pay_price);
        tvNonPayPrice = (TextView) view.findViewById(R.id.tv_non_pay_price);

        tvMemberAll = (TextView) view.findViewById(R.id.tv_member_all);
        btnSerch = (Button) view.findViewById(R.id.btn_serch);
        setTextClickView();
        setOnClickView();
    }

    private void setOnClickView() {
        Smember.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Logger.d(TAG, "position : " + position + parent.getItemAtPosition(position));
                switch (position) {

                    case 0:
                        orderStatus = "ALL";
                        break;
                    case 1:
                        orderStatus = "ENTER";
                        break;
                    case 2:
                        orderStatus = "WASHFIN";
                        break;
                    case 3:
                        orderStatus = "OUTFIN";
                        break;
                    case 4:
                        orderStatus = "CNL";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnSerch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWait.show();
                new Thread(new Runnable() {
                    public void run() {
                        ManagerGetVisitOrder managerCancelAvailOrderList = VisitReceptionManager.getGetVisit((AppCompatActivity)getActivity(), startDate, endDate, orderStatus, userGrade, null, null, null, 1, 20000);

                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                mWait.dismiss();
                                if (managerCancelAvailOrderList.getResultCode().equals("S0000")) {
                                    llNoData.setVisibility(View.GONE);
                                    listView.setVisibility(View.VISIBLE);
                                    tvItemCount.setText(managerCancelAvailOrderList.getGetVisitOrder().size() + "건 주문이 있습니다.");

                                    tvTotalItemCount.setText(managerCancelAvailOrderList.getGetVisitOrder().size() + "건");
                                    List<GetVisitOrderList> itemGetVisitOrder = managerCancelAvailOrderList.getGetVisitOrder();

                                    for (GetVisitOrderList getVisitOrderList : itemGetVisitOrder) {
                                        mPrice += getVisitOrderList.getPayPrice();
                                        mNonPrice += getVisitOrderList.getNonPayPrice();
                                    }

                                    tvTotalPayPrice.setText(DataFormatUtil.moneyFormatToWon(mPrice) + "원");
                                    tvNonPayPrice.setText(DataFormatUtil.moneyFormatToWon(mNonPrice) + "원");

                                    AdapterMainOrderDetail adapterMainOrderInquiry = new AdapterMainOrderDetail((AppCompatActivity)getActivity());
                                    listItem = managerCancelAvailOrderList.getGetVisitOrder();

                                    adapterMainOrderInquiry.addItem(listItem);
                                    listView.setAdapter(adapterMainOrderInquiry);
                                } else {
                                    llNoData.setVisibility(View.VISIBLE);
                                    listView.setVisibility(View.GONE);
                                }
                            }
                        });
                    }
                }).start();
            }
        });
    }

    private void setTextClickView() {
        tvAll.setOnClickListener(ViewColorListener);
        tvAll.setTag(0);
        tvYesterday.setOnClickListener(ViewColorListener);
        tvYesterday.setTag(1);

        tvToday.setOnClickListener(ViewColorListener);
        tvToday.setTag(2);
        tv1week.setOnClickListener(ViewColorListener);
        tv1week.setTag(3);
        tv1month.setOnClickListener(ViewColorListener);
        tv1month.setTag(4);
        tv3month.setOnClickListener(ViewColorListener);
        tv3month.setTag(5);
        tv6month.setOnClickListener(ViewColorListener);
        tv6month.setTag(6);
        tv1year.setOnClickListener(ViewColorListener);
        tv1year.setTag(7);
        tvAll.setSelected(true);

        rlAll.setOnClickListener(memberClickListener);
        rlBlue.setOnClickListener(memberClickListener);
        rlOrange.setOnClickListener(memberClickListener);
        rlRed.setOnClickListener(memberClickListener);
    }


    private View.OnClickListener ViewColorListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();

            int selectPosition = 0;
            while (selectPosition < 8) {
                if (tag == selectPosition) {
                    llBg.findViewWithTag(selectPosition).setSelected(true);
                } else {
                    llBg.findViewWithTag(selectPosition).setSelected(false);
                }
                selectPosition++;
            }

            switch (tag) {
                case 0:
                    startDate = null;
                    endDate = null;
                    break;
                case 1:
                    endDate = DataFormatUtil.getToday();
                    startDate = DataFormatUtil.getYesterday();
                    break;
                case 2:
                    endDate = DataFormatUtil.getToday();
                    startDate = DataFormatUtil.getToday();
                    break;
                case 3:
                    endDate = DataFormatUtil.getToday();
                    startDate = DataFormatUtil.get7DayAgoDate();
                    break;
                case 4:
                    endDate = DataFormatUtil.getToday();
                    startDate = DataFormatUtil.getMonthAgoDate();
                    break;
                case 5:
                    endDate = DataFormatUtil.getToday();
                    startDate = DataFormatUtil.get3MonthAgoDate();
                    break;
                case 6:
                    endDate = DataFormatUtil.getToday();
                    startDate = DataFormatUtil.get6MonthAgoDate();
                    break;
                case 7:
                    endDate = DataFormatUtil.getToday();
                    startDate = DataFormatUtil.get1yearAgoDate();
                    break;

            }
        }
    };

    private View.OnClickListener memberClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.rl_all:
                    userGrade = null;
                    tvMemberAll.setVisibility(View.GONE);
                    imgAll.setVisibility(View.VISIBLE);

                    imgBlue.setVisibility(View.GONE);
                    imgOrange.setVisibility(View.GONE);
                    imgRed.setVisibility(View.GONE);

                    break;

                case R.id.rl_blue:
                    userGrade = "1";
                    tvMemberAll.setVisibility(View.VISIBLE);
                    imgAll.setVisibility(View.GONE);

                    imgBlue.setVisibility(View.VISIBLE);
                    imgOrange.setVisibility(View.GONE);
                    imgRed.setVisibility(View.GONE);

                    break;

                case R.id.rl_orange:
                    userGrade = "2";
                    tvMemberAll.setVisibility(View.VISIBLE);
                    imgAll.setVisibility(View.GONE);

                    imgBlue.setVisibility(View.GONE);
                    imgOrange.setVisibility(View.VISIBLE);
                    imgRed.setVisibility(View.GONE);

                    break;

                case R.id.rl_red:
                    userGrade = "3";
                    tvMemberAll.setVisibility(View.VISIBLE);
                    imgAll.setVisibility(View.GONE);

                    imgBlue.setVisibility(View.GONE);
                    imgOrange.setVisibility(View.GONE);
                    imgRed.setVisibility(View.VISIBLE);

                    break;
            }
        }
    };
}