package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerFaqCateforyList;
import rewhite.shopplus.client.manager.ManagerFaqList;
import rewhite.shopplus.client.model.FaqListModel;
import rewhite.shopplus.client.network.CustomerManager;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterAskedQuestions;
import rewhite.shopplus.view.adapter.AdapterServiceCenterCategory;

/**
 * 자주 묻는 질문 View
 */
public class FragmentAskedQuestions extends Fragment {

    private static final String TAG = "FragmentAskedQuestions";
    private static final int MAX_COUNT = 6;
    private WaitCounter mWait;

    private View view;
    private RecyclerView recycleView;
    private ListView listView;

    private RecyclerView.LayoutManager layoutManager;
    private Button btnPrve, btnNext;
    private int mFaqCategoryID;
    private int pageCount = 1;
    private WebView webView;
    private WebSettings webSettings;

    private EditText etSearch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_asked_questions, container, false);

        mWait = new WaitCounter(getActivity());


        setLayout();
        return view;
    }

    private void setLayout() {
        recycleView = (RecyclerView) view.findViewById(R.id.recycle_view);
        listView = (ListView) view.findViewById(R.id.list_view);
        webView = (WebView) view.findViewById(R.id.webView);

        btnPrve = (Button) view.findViewById(R.id.btn_prve);
        btnNext = (Button) view.findViewById(R.id.btn_next);

        btnNext.setOnClickListener(btnOnClickListener);
        btnPrve.setOnClickListener(btnOnClickListener);

        etSearch = (EditText) view.findViewById(R.id.et_search);
        layoutManager = new StaggeredGridLayoutManager(5, StaggeredGridLayoutManager.VERTICAL);
        recycleView.setLayoutManager(layoutManager);

        AdapterServiceCenterCategory adapterServiceCenterCategory = new AdapterServiceCenterCategory(getActivity(), mOnAdapterListner);
        adapterServiceCenterCategory.addItem(ManagerFaqCateforyList.getmInstance().getFaqCategoryListModel());
        recycleView.setAdapter(adapterServiceCenterCategory); //카테고리

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        serviceCenterListSearch(v.getText().toString());
                        break;

                    case EditorInfo.IME_ACTION_DONE:
                        break;
                    default:
                        // 기본 엔터키 동작
                        return false;
                }
                return true;
            }
        });
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.btn_next:
                    --pageCount;
                    serviceCenterList();
                    break;

                case R.id.btn_prve:
                    ++pageCount;
                    serviceCenterList();
                    break;


            }
        }
    };

    private void serviceCenterListSearch(String mag) {
        mWait.show();
        try {
            new Thread(new Runnable() {
                public void run() {
                    ManagerFaqList managerFaqList = CustomerManager.getFaqList((AppCompatActivity)getActivity(), mFaqCategoryID, mag, pageCount, MAX_COUNT);
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            AdapterAskedQuestions adapterServiceCenterList = new AdapterAskedQuestions(noticeData);
                            List<FaqListModel> faqListModels = managerFaqList.getManagerFaqList();

                            adapterServiceCenterList.addItem(faqListModels);
                            listView.setAdapter(adapterServiceCenterList); //카테고리 리스트

                            adapterServiceCenterList.notifyDataSetChanged();
                            mWait.dismiss();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }


    private void serviceCenterList() {
        mWait.show();
        try {
            new Thread(new Runnable() {
                public void run() {
                    ManagerFaqList managerFaqList = CustomerManager.getFaqList((AppCompatActivity)getActivity(), mFaqCategoryID, "", pageCount, MAX_COUNT);
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            AdapterAskedQuestions adapterServiceCenterList = new AdapterAskedQuestions(noticeData);
                            List<FaqListModel> faqListModels = managerFaqList.getManagerFaqList();

                            adapterServiceCenterList.addItem(faqListModels);
                            listView.setAdapter(adapterServiceCenterList); //카테고리 리스트

                            adapterServiceCenterList.notifyDataSetChanged();
                            mWait.dismiss();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    public interface NoticeData {
        void webViewUrl(String Url);
    }

    private FragmentAskedQuestions.NoticeData noticeData = new FragmentAskedQuestions.NoticeData() {
        @Override
        public void webViewUrl(String Url) {
            webView.setWebViewClient(new WebViewClient());
            webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);

            webView.loadUrl(Url);
        }
    };

    private AdapterServiceCenterCategory.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterServiceCenterCategory.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int faqCategoryID) {
            mFaqCategoryID = faqCategoryID;
            try {
                mWait.show();
                new Thread(new Runnable() {
                    public void run() {
                        ManagerFaqList managerFaqList = CustomerManager.getFaqList((AppCompatActivity)getActivity(), mFaqCategoryID, "", 1, MAX_COUNT);
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                AdapterAskedQuestions adapterServiceCenterList = new AdapterAskedQuestions(noticeData);
                                List<FaqListModel> faqListModels = managerFaqList.getManagerFaqList();

                                adapterServiceCenterList.addItem(faqListModels);
                                listView.setAdapter(adapterServiceCenterList); //카테고리 리스트

                                adapterServiceCenterList.notifyDataSetChanged();
                                mWait.dismiss();
                            }
                        });
                    }
                }).start();
            } catch (Exception err) {
                err.printStackTrace();
            }
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {
        }
    };
}
