package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rewhite.shopplus.R;

/**
 * MAIN 문자관리 카테고리 View
 */
public class FragmentCharacterManagement extends Fragment {

    private static final String TAG = "FragmentCharacterManagement";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_character_management, container, false);

        setLayout(view);

        return view;
    }

    private void setLayout(View view) {

    }
}