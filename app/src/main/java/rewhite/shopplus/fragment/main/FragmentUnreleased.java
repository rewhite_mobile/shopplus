package rewhite.shopplus.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterUnreleased;

public class FragmentUnreleased extends Fragment {

    private static final String TAG = "FragmentUnreleased";
    private ListView listView;

    private WaitCounter mWait;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_unreleased, container, false);


        mWait = new WaitCounter(getActivity());
        init(view);
        return view;
    }

    private void init(View view) {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                final ManagerGetVisitOrder managerGetVisitOrder = VisitReceptionManager.getGetVisitOrder((AppCompatActivity)getActivity(), "NOTOUT", SettingLogic.getStoreUserId(getActivity()), "", "");

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        setLayout(view);

                        List<GetVisitOrderList> getVisitOrderList = managerGetVisitOrder.getGetVisitOrder();

                        AdapterUnreleased adapterUnreleased = new AdapterUnreleased(getContext());
                        adapterUnreleased.addItem(getVisitOrderList);
                        listView.setAdapter(adapterUnreleased);
                    }
                });
            }
        }).start();
    }

    private void setLayout(View view) {
        listView = (ListView) view.findViewById(R.id.lv);
    }
}
