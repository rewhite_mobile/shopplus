package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.util.SharedPreferencesUtil;
import rewhite.shopplus.util.hardware.FirstHardware;

/**
 * 장비설정 -> 결제단말기 설정VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentPaymentTerminals extends Fragment {
    private static final String TAG = "FragmentPaymentTerminals";

    private RadioGroup rgOtherOptions, rgSignaturePad;
    private RadioButton rbOtherOptionsUnselect, rbFistData, rbSmartro, rbSignaturePadOn, rbSignaturePadOff;

    private LinearLayout llCat, llSignature;

    private View vLine , llCatLine;
    private Button btnConnection;
    private String firstDataInfo;

    private TextView tvConnectionText, tvProvider, tvPortNumber, tvModel, tvCatId, tvHwSerial;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_terminals, container, false);

        init(view);
        return view;
    }

    private void init(View view) {
        setLayout(view);
        FirstHardware.init((AppCompatActivity)getActivity());
    }

    /**
     * Layout Setting View
     *
     * @param view
     */
    private void setLayout(View view) {
        rgOtherOptions = (RadioGroup) view.findViewById(R.id.rg_other_options);
        rgSignaturePad = (RadioGroup) view.findViewById(R.id.rg_signature_pad);

        rbOtherOptionsUnselect = (RadioButton) view.findViewById(R.id.rb_other_options_unselect);
        rbFistData = (RadioButton) view.findViewById(R.id.rb_fist_data);
        rbSmartro = (RadioButton) view.findViewById(R.id.rb_smartro);

        tvConnectionText = (TextView) view.findViewById(R.id.tv_connection_text);
        tvProvider = (TextView) view.findViewById(R.id.tv_provider);
        tvPortNumber = (TextView) view.findViewById(R.id.tv_port_number);
        tvModel = (TextView) view.findViewById(R.id.tv_model);
        tvCatId = (TextView) view.findViewById(R.id.tv_cat_id);
        tvHwSerial = (TextView) view.findViewById(R.id.tv_hw_serial);

        rbSignaturePadOn = (RadioButton) view.findViewById(R.id.rb_signature_pad_on);
        rbSignaturePadOff = (RadioButton) view.findViewById(R.id.rb_signature_pad_off);

        llCat = (LinearLayout) view.findViewById(R.id.ll_cat);
        llSignature = (LinearLayout) view.findViewById(R.id.ll_signature);
        vLine = (View) view.findViewById(R.id.v_line);
        llCatLine = (View) view.findViewById(R.id.ll_cat_line);


        btnConnection = (Button) view.findViewById(R.id.btn_connection);
        btnConnection.setOnClickListener(btnClickListener);
        rgOtherOptions.setOnCheckedChangeListener(radioOnCheckedListener);
        rgSignaturePad.setOnCheckedChangeListener(radioOtherOptionsListener);
    }

    private View.OnClickListener btnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.btn_connection:
                    try {
                        //하드웨어 연결
                        FirstHardware.testPrintFDK();
                        setTextView();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener radioOtherOptionsListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbSignaturePadOn.isChecked()) {
                rbSignaturePadOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbSignaturePadOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rbSignaturePadOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbSignaturePadOff.setBackgroundResource(R.drawable.a_button_type_3);
            } else if (rbSignaturePadOff.isChecked()) {
                rbSignaturePadOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbSignaturePadOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rbSignaturePadOff.setBackgroundResource(R.drawable.a_button_type_2);
                rbSignaturePadOn.setBackgroundResource(R.drawable.a_button_type_3);
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener radioOnCheckedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            switch (checkedId) {
                case R.id.rb_other_options_unselect:        //사용 안함
                    rbOtherOptionsUnselect.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                    rbFistData.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    rbSmartro.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    rbOtherOptionsUnselect.setBackgroundResource(R.drawable.a_button_type_2);
                    rbFistData.setBackgroundResource(R.drawable.a_button_type_3);
                    rbSmartro.setBackgroundResource(R.drawable.a_button_type_3);
                    setOPrionsUnSelectStatus();
                    break;

                case R.id.rb_fist_data:                     // FirstData
                    rbOtherOptionsUnselect.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    rbFistData.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                    rbSmartro.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    rbOtherOptionsUnselect.setBackgroundResource(R.drawable.a_button_type_3);
                    rbFistData.setBackgroundResource(R.drawable.a_button_type_2);
                    rbSmartro.setBackgroundResource(R.drawable.a_button_type_3);
                    setFistdataSelectStatus();
                    break;

                case R.id.rb_smartro:                       // Smartro
                    rbOtherOptionsUnselect.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    rbFistData.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    rbSmartro.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));

                    rbOtherOptionsUnselect.setBackgroundResource(R.drawable.a_button_type_3);
                    rbFistData.setBackgroundResource(R.drawable.a_button_type_3);
                    rbSmartro.setBackgroundResource(R.drawable.a_button_type_2);
                    setSmartroSelectStatus();
                    break;
            }
        }
    };

    private void setTextView() {
        if (SharedPreferencesUtil.getBooleanSharedPreference(getActivity(), PrefConstant.KEY_CONNECTION_STATUS, PrefConstant.NAME_CONNECTION_STATUS, false)) {
            tvConnectionText.setText("정상적으로 연결되어 있어요.");
            tvProvider.setText("FirstData");
            tvPortNumber.setText(SharedPreferencesUtil.getStringSharedPreference(getActivity(), PrefConstant.KEY_PORT, PrefConstant.NAME_PORT));
            tvModel.setText(SharedPreferencesUtil.getStringSharedPreference(getActivity(), PrefConstant.KEY_MODEL_NAME, PrefConstant.NAME_MODEL_NAME));
            tvCatId.setText(SharedPreferencesUtil.getStringSharedPreference(getActivity(), PrefConstant.KEY_CAT_ID, PrefConstant.NAME_CAT_ID));
            tvHwSerial.setText(SharedPreferencesUtil.getStringSharedPreference(getActivity(), PrefConstant.KEY_SERIAL_NUMBER, PrefConstant.NAME_SERIAL_NUMBER));
        }
    }

    /**
     * UnSelect Status
     */
    private void setOPrionsUnSelectStatus() {
        rbOtherOptionsUnselect.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
        rbFistData.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
        rbSmartro.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

        llCat.setVisibility(View.GONE);
        llSignature.setVisibility(View.GONE);
        vLine.setVisibility(View.GONE);
        llCatLine.findViewById(View.GONE);

    }

    /**
     * FistData Select Status
     */
    private void setFistdataSelectStatus() {
        rbFistData.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
        rbOtherOptionsUnselect.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
        rbSmartro.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

        llCat.setVisibility(View.VISIBLE);
        llCatLine.findViewById(View.VISIBLE);
        llSignature.setVisibility(View.GONE);
        vLine.setVisibility(View.GONE);
    }

    /**
     * Smartro Select Status
     */
    private void setSmartroSelectStatus() {
        rbSmartro.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
        rbFistData.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
        rbOtherOptionsUnselect.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

        llCat.setVisibility(View.VISIBLE);
        llSignature.setVisibility(View.VISIBLE);
        vLine.setVisibility(View.VISIBLE);
    }
}