package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityLogin;
import rewhite.shopplus.client.manager.ManagerAuthStoreLogin;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.data.manager.ManagerChangePasswordModel;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.SharedPreferencesUtil;
import rewhite.shopplus.util.WaitCounter;

/**
 * 앱설정 -> 계정정보VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentAccountInformation extends Fragment {
    private static final String TAG = "FragmentAccountInformation";

    private RadioButton rbAutoLoginOn, rbAutoLoginOff;
    private TextView tvNowVersion, tvUserId;
    private WaitCounter mWait;
    private AlertDialog.Builder mDialog;

    private AppCompatActivity mActivity;

    private String deviceVersion;
    private String storeVersion;

    private EditText etBasicSecret, etNewSecret, etNewSecretCheck;

    public FragmentAccountInformation(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_infomation, container, false);

        mWait = new WaitCounter(getActivity());
        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        RadioGroup rgAutoLogin = (RadioGroup) view.findViewById(R.id.rg_auto_login);
        tvUserId = (TextView) view.findViewById(R.id.tv_user_id);

        Button btnLoginOut = (Button) view.findViewById(R.id.btn_login_out);
        Button btnAppUpdate = (Button) view.findViewById(R.id.btn_app_update);
        Button btnPassword = (Button) view.findViewById(R.id.btn_password);

        etBasicSecret = (EditText) view.findViewById(R.id.et_basic_secret);
        etNewSecret = (EditText) view.findViewById(R.id.et_new_secret);
        etNewSecretCheck = (EditText) view.findViewById(R.id.et_new_secret_check);

        rbAutoLoginOn = (RadioButton) view.findViewById(R.id.rb_auto_login_on);
        rbAutoLoginOff = (RadioButton) view.findViewById(R.id.rb_auto_login_off);
        tvNowVersion = (TextView) view.findViewById(R.id.tv_now_version);

        rgAutoLogin.setOnCheckedChangeListener(onRadioClickListener);

        btnLoginOut.setOnClickListener(btnOnClickListener);
        btnAppUpdate.setOnClickListener(btnOnClickListener);
        btnPassword.setOnClickListener(btnOnClickListener);

        setTextView();
    }

    private void setTextView() {
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), PackageManager.GET_META_DATA);
            String versionName = pInfo.versionName;
            tvNowVersion.setText(versionName);
            tvUserId.setText(ManagerAuthStoreLogin.getmInstance().getmanagerAuthStoreLogin().get(0).getData().getLoginID());

            //자동로그인 설정 데이터 저장
            if (SettingLogic.getAutoLogin(getActivity())) {
                SettingLogic.setAutoLogin(getContext(), true);
                rbAutoLoginOn.setBackgroundResource(R.color.color_43425c);
                rbAutoLoginOff.setBackgroundResource(R.color.Wite_color);

                rbAutoLoginOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbAutoLoginOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else {
                SettingLogic.setAutoLogin(getContext(), false);
                rbAutoLoginOn.setBackgroundResource(R.color.Wite_color);
                rbAutoLoginOff.setBackgroundResource(R.color.color_43425c);

                rbAutoLoginOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbAutoLoginOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.btn_login_out:
                    SharedPreferencesUtil.removeSharedPreference(getActivity(), PrefConstant.KEY_ACCESS_TOKEN, PrefConstant.NAME_ACCESS_TOKEN);

                    Intent intent = new Intent(getActivity(), ActivityLogin.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    getActivity().startActivity(intent);
                    break;

                case R.id.btn_app_update:
                    mDialog = new AlertDialog.Builder(getActivity());
                    new UpdateCheckLogic().execute();
                    break;

                case R.id.btn_password:
                    passwordChange();
                    break;
            }
        }
    };

    /**
     * 패스워드 변경 Logic
     */
    private void passwordChange() {
        mWait.show();
        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());

        if (etBasicSecret.getText().toString() == null || TextUtils.isEmpty(etBasicSecret.getText().toString()) || etNewSecret.getText().toString() == null || TextUtils.isEmpty(etNewSecret.getText().toString())
                || etNewSecretCheck.getText().toString() == null || TextUtils.isEmpty(etNewSecretCheck.getText().toString())) {
            mWait.dismiss();
            thirdAlertPopup.setTitle("비밀번호 입력 오류");
            thirdAlertPopup.setContent("비밀번호를 모두 입력하세요.");
            thirdAlertPopup.show();
        } else if (!etNewSecret.getText().toString().equals(etNewSecretCheck.getText().toString())) {
            mWait.dismiss();
            thirdAlertPopup.setTitle("비밀번호 입력 오류");
            thirdAlertPopup.setContent("신규비밀번호가 일치하지 않아요.\n" + "다시 입력해주세요.");
            thirdAlertPopup.show();
        } else {
            new Thread(new Runnable() {
                public void run() {
                    String resultCode = EnviromentManager.getChangePassword((AppCompatActivity) getActivity(), "", "");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                            Logger.d(TAG, "비밀번호 변경오류 : " + ManagerChangePasswordModel.getmInstance().getManagerItemRepair().get(0).getResultCode());
                            if (resultCode.equals("S0000")) {
                                thirdAlertPopup.setTitle("비밀번호 변경완료");
                                thirdAlertPopup.setContent("비밀번호가 변경되었어요.\n" + "로그인해주세요.");

                                thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                    @Override
                                    public void onConfirmClick() {
                                        Intent intent = new Intent(getActivity(), ActivityLogin.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);
                                    }
                                });
                                thirdAlertPopup.show();
                            } else if (resultCode.equals("F0000")) {
                                Logger.d(TAG, "비밀번호 변경오류 : " + ManagerChangePasswordModel.getmInstance().getManagerItemRepair().get(0).getMessage());
                                thirdAlertPopup.setTitle("비밀번호 변경오류");
                                thirdAlertPopup.setContent(ManagerChangePasswordModel.getmInstance().getManagerItemRepair().get(0).getMessage());
                                thirdAlertPopup.show();
                            }
                        }
                    });
                }
            }).start();
        }
    }


    private RadioGroup.OnCheckedChangeListener onRadioClickListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbAutoLoginOn.isChecked()) {
                SettingLogic.setAutoLogin(getContext(), true);
                rbAutoLoginOn.setBackgroundResource(R.color.color_43425c);
                rbAutoLoginOff.setBackgroundResource(R.color.Wite_color);

                rbAutoLoginOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbAutoLoginOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rbAutoLoginOff.isChecked()) {
                SettingLogic.setAutoLogin(getContext(), false);
                rbAutoLoginOn.setBackgroundResource(R.color.Wite_color);
                rbAutoLoginOff.setBackgroundResource(R.color.color_43425c);

                rbAutoLoginOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbAutoLoginOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

//    private void setVersionCode() {
//        try {
//            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageNam(), PackageManager.GET_META_DATA);
//
//            String versionName = pInfo.versionName;
//            int versionCode = pInfo.versionCode;
//
//            tvNowVersion.setText(versionName);
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * 업데이트 하기위한 로직
     */
    public class UpdateCheckLogic extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            //TODO 현재 스토어에 올라가지 않음.. 대기
            try {
                Document doc = Jsoup.connect("https://play.google.com/store/apps/details?id=패키지명 적으세요").get();
                Elements Version = doc.select(".content");

                for (Element v : Version) {
                    if (v.attr("itemprop").equals("softwareVersion")) {
                        storeVersion = v.text();
                    }
                }
                return storeVersion;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            PackageInfo pi = null;
            try {
                pi = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            deviceVersion = pi.versionName;
            storeVersion = result;

            if (!deviceVersion.equals(storeVersion)) {
                mDialog.setMessage("업데이트 후 사용해주세요.")
                        .setCancelable(false)
                        .setPositiveButton("업데이트 바로가기", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
                                marketLaunch.setData(Uri.parse("https://play.google.com/store/apps/details?id=패키지명 적으세요"));
                                startActivity(marketLaunch);
                            }
                        });
                AlertDialog alert = mDialog.create();
                alert.setTitle("안 내");
                alert.show();
            }
            super.onPostExecute(result);
        }
    }
}