package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreDeviceInfo;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;

/**
 * 장비설정 -> 인쇄설정 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentPrintSettings extends Fragment {
    private static final String TAG = "FragmentPrintSettings";

    private RadioGroup rgNumberCopies, rgPreFilledGold, rgDamageOccurred, rgLaundryStorage, rgGreeting;

    private RadioButton rgNumberCopiesOn, rgNumberCopiesOff, rgPreFilledGoldOn, rgPreFilledGoldOff;
    private RadioButton rgDamageOccurredOn, rgDamageOccurredOff, rgLaundryStorageOn, rgLaundryStorageOff, rgGreetingOn, rgGreetingOff;

    private String isPrintStoreNotice, IsPrintStotageNotice, isPrintDamageNotice;
    private int printCount;
    private String isPrintBalance;

    private TextView tvCompensationDamages, tvLaundryStorage, tvGreetings;
    private AppCompatActivity mActivity;

    private Button btnGreetingEdit, btnLaundryStorageEdit, btnDamageOccurredEdit;

    private WaitCounter mWait;

    public FragmentPrintSettings(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_print_setting, container, false);


        mWait = new WaitCounter(getContext());
        init(view);
        return view;
    }

    private void init(View view) {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                EnviromentManager.getStoreDeviceInfo((AppCompatActivity)getActivity());
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        setLayout(view);
                        setClickListener();
                    }
                });
            }
        }).start();
    }

    /**
     * 레이아웃 셋팅
     *
     * @param view
     */
    private void setLayout(View view) {
        tvCompensationDamages = (TextView) view.findViewById(R.id.tv_compensation_damages);
        tvLaundryStorage = (TextView) view.findViewById(R.id.tv_laundry_storage);
        tvGreetings = (TextView) view.findViewById(R.id.tv_greetings);

        rgNumberCopies = (RadioGroup) view.findViewById(R.id.rg_number_copies);
        rgPreFilledGold = (RadioGroup) view.findViewById(R.id.rg_pre_filled_gold);
        rgDamageOccurred = (RadioGroup) view.findViewById(R.id.rg_damage_occurred);
        rgLaundryStorage = (RadioGroup) view.findViewById(R.id.rg_laundry_storage);
        rgGreeting = (RadioGroup) view.findViewById(R.id.rg_greeting);

        rgNumberCopiesOn = (RadioButton) view.findViewById(R.id.rg_number_copies_on);
        rgNumberCopiesOff = (RadioButton) view.findViewById(R.id.rg_number_copies_off);
        rgPreFilledGoldOn = (RadioButton) view.findViewById(R.id.rg_pre_filled_gold_on);
        rgPreFilledGoldOff = (RadioButton) view.findViewById(R.id.rg_pre_filled_gold_off);

        rgDamageOccurredOn = (RadioButton) view.findViewById(R.id.rg_damage_occurred_on);
        rgDamageOccurredOff = (RadioButton) view.findViewById(R.id.rg_damage_occurred_off);
        rgLaundryStorageOn = (RadioButton) view.findViewById(R.id.rg_laundry_storage_on);
        rgLaundryStorageOff = (RadioButton) view.findViewById(R.id.rg_laundry_storage_off);
        rgGreetingOn = (RadioButton) view.findViewById(R.id.rg_greeting_on);
        rgGreetingOff = (RadioButton) view.findViewById(R.id.rg_greeting_off);

        btnGreetingEdit = (Button) view.findViewById(R.id.btn_greeting_edit);
        btnLaundryStorageEdit = (Button) view.findViewById(R.id.btn_laundry_storage_edit);
        btnDamageOccurredEdit = (Button) view.findViewById(R.id.btn_damage_occurred_edit);

        setCheckViewSetting();
        setTextView();
    }

    /**
     * TEXT VIEW SETTING
     */
    private void setTextView() {
        try {
            tvCompensationDamages.setText(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getDamageNotice());
            tvLaundryStorage.setText(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getStorageNotice());
            tvGreetings.setText(ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getStoreNotice());
        } catch (Exception err) {
            err.printStackTrace();
        }

    }

    /**
     * 초기 사용 여부 Check Logic View Setting
     */
    private void setCheckViewSetting() {
        //인쇄 매수 설정
        try {
            if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getPrintCount() == 1) {
                rgNumberCopiesOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgNumberCopiesOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgNumberCopiesOn.setBackgroundResource(R.color.color_43425c);
                rgNumberCopiesOff.setBackgroundResource(R.color.Wite_color);
            } else if (rgNumberCopiesOff.isChecked()) {
                rgNumberCopiesOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgNumberCopiesOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgNumberCopiesOff.setBackgroundResource(R.color.color_43425c);
                rgNumberCopiesOn.setBackgroundResource(R.color.Wite_color);
            }

            //회원 잔액 출력 여부
            if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintBalance().equals("Y")) {
                rgPreFilledGoldOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgPreFilledGoldOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgPreFilledGoldOn.setBackgroundResource(R.color.color_43425c);
                rgPreFilledGoldOff.setBackgroundResource(R.color.Wite_color);
            } else {
                rgPreFilledGoldOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgPreFilledGoldOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgPreFilledGoldOff.setBackgroundResource(R.color.color_43425c);
                rgPreFilledGoldOn.setBackgroundResource(R.color.Wite_color);
            }

            //손해 배상 기준 출력 여부
            if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintDamageNotice().equals("Y")) {
                rgDamageOccurredOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgDamageOccurredOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgDamageOccurredOn.setBackgroundResource(R.color.color_43425c);
                rgDamageOccurredOff.setBackgroundResource(R.color.Wite_color);
            } else {
                rgDamageOccurredOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgDamageOccurredOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgDamageOccurredOff.setBackgroundResource(R.color.color_43425c);
                rgDamageOccurredOn.setBackgroundResource(R.color.Wite_color);
            }

            //세탁물 보관료 안내 출력 여부
            if (ManagerGetStoreDeviceInfo.getmInstance().getGetStoreDeviceInfo().get(0).getData().getIsPrintStorageNotice().equals("Y")) {
                rgLaundryStorageOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgLaundryStorageOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgLaundryStorageOn.setBackgroundResource(R.color.color_43425c);
                rgLaundryStorageOff.setBackgroundResource(R.color.Wite_color);
            } else {
                rgGreetingOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgGreetingOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgGreetingOff.setBackgroundResource(R.color.color_43425c);
                rgGreetingOn.setBackgroundResource(R.color.Wite_color);
            }

        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * RadioGrop Click View Stting
     */
    private void setClickListener() {
        rgNumberCopies.setOnCheckedChangeListener(OnNumberCopiesClicListenr);
        rgPreFilledGold.setOnCheckedChangeListener(OnPreFilledClicListenr);
        rgDamageOccurred.setOnCheckedChangeListener(OnDamageOccurredClicListenr);
        rgLaundryStorage.setOnCheckedChangeListener(OnLaundryStorageClicListenr);
        rgGreeting.setOnCheckedChangeListener(OnGreetingClicListenr);

        btnGreetingEdit.setOnClickListener(btnClickViewListener);
        btnLaundryStorageEdit.setOnClickListener(btnClickViewListener);
        btnDamageOccurredEdit.setOnClickListener(btnClickViewListener);
    }

    private View.OnClickListener btnClickViewListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.btn_greeting_edit:
                    setEditTextDialog(0);
                    break;
                case R.id.btn_laundry_storage_edit:
                    setEditTextDialog(1);
                    break;
                case R.id.btn_damage_occurred_edit:
                    setEditTextDialog(2);
                    break;
            }
        }
    };


    /**
     * 인사말 수정하기 위한 DialogView Setting
     */
    private void setEditTextDialog(int type) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        AlertDialog alertDialog = dialog.create();

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_edittext_view, null);

        LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);
        Button btnSave = (Button) view.findViewById(R.id.btn_save);
        TextView tvDialogTitleName = (TextView) view.findViewById(R.id.tv_dialog_title_name);

        EditText editText = (EditText) view.findViewById(R.id.et_dialog_greeting_president);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWait.show();

                if (type == 0) {            //피해발생 시 손해배상기
                    getUpdateStoreNotice(editText);
                } else if (type == 1) {     //세탁물 보관료 안내
                    getUpdateStorageNotice(editText);
                } else if (type == 2) {     //세탁소 인사말
                    setUpdateDamageNotice(editText);
                }

                new Thread(new Runnable() {
                    public void run() {
                        EnviromentManager.getStoreDeviceInfo((AppCompatActivity)getActivity());
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                mWait.dismiss();
                                setTextView();
                            }
                        });
                    }
                }).start();


                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                alertDialog.dismiss();
            }
        });

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }

    /**
     * 패해발생 시 손해배상기준 ServerData REQUEST
     *
     * @param editText
     */
    private void setUpdateDamageNotice(EditText editText) {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                Logger.d(TAG, " setUpdateDamageNotice : " + editText.getText().toString());
                EnviromentManager.getUpdateDamageNotice((AppCompatActivity)getActivity(), isPrintStoreNotice, editText.getText().toString());
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                    }
                });
            }
        }).start();
    }

    /**
     * 세탁소 보관료 안내 ServerData REQUEST
     *
     * @param editText
     */
    private void getUpdateStorageNotice(EditText editText) {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                Logger.d(TAG, " getUpdateStorageNotice : " + editText.getText().toString());
                EnviromentManager.getUpdateStorageNotice((AppCompatActivity)getActivity(), IsPrintStotageNotice, editText.getText().toString());
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                    }
                });
            }
        }).start();
    }

    /**
     * 세탁소 인사말 ServerData REQUEST
     *
     * @param editText
     */
    private void getUpdateStoreNotice(EditText editText) {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                EnviromentManager.getUpdateStoreNotice((AppCompatActivity)getActivity(), IsPrintStotageNotice, editText.getText().toString());
                EnviromentManager.getStoreDeviceInfo((AppCompatActivity)getActivity());
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                    }
                });
            }
        }).start();
    }

    /**
     * 인쇄매수 설정 Listener
     */
    private RadioGroup.OnCheckedChangeListener OnNumberCopiesClicListenr = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rgNumberCopiesOn.isChecked()) {
                printCount = 1;
                rgNumberCopiesOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgNumberCopiesOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgNumberCopiesOn.setBackgroundResource(R.color.color_43425c);
                rgNumberCopiesOff.setBackgroundResource(R.color.Wite_color);
            } else if (rgNumberCopiesOff.isChecked()) {
                printCount = 2;
                rgNumberCopiesOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgNumberCopiesOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgNumberCopiesOff.setBackgroundResource(R.color.color_43425c);
                rgNumberCopiesOn.setBackgroundResource(R.color.Wite_color);
            }
            updateStorePrintSetting();
        }
    };


    /**
     * 선충전금, 마일리지 잔액 출력 Lisetenr
     */
    private RadioGroup.OnCheckedChangeListener OnPreFilledClicListenr = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rgPreFilledGoldOn.isChecked()) {
                isPrintBalance = "Y";
                rgPreFilledGoldOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgPreFilledGoldOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgPreFilledGoldOn.setBackgroundResource(R.color.color_43425c);
                rgPreFilledGoldOff.setBackgroundResource(R.color.Wite_color);
            } else if (rgPreFilledGoldOff.isChecked()) {
                isPrintBalance = "N";

                rgPreFilledGoldOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgPreFilledGoldOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgPreFilledGoldOff.setBackgroundResource(R.color.color_43425c);
                rgPreFilledGoldOn.setBackgroundResource(R.color.Wite_color);
            }
            updateStorePrintSetting();
        }
    };

    /**
     * 가맹점 접수증 인쇄 설정 Setting
     */
    private void updateStorePrintSetting() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                EnviromentManager.getUpdateStorePrintSetting((AppCompatActivity)getActivity(), printCount, isPrintBalance);
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                    }
                });
            }
        }).start();
    }

    /**
     * 피해발생 시 손해배상기 ClickListener
     */
    private RadioGroup.OnCheckedChangeListener OnDamageOccurredClicListenr = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rgDamageOccurredOn.isChecked()) {
                isPrintStoreNotice = "Y";
                rgDamageOccurredOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgDamageOccurredOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgDamageOccurredOn.setBackgroundResource(R.color.color_43425c);
                rgDamageOccurredOff.setBackgroundResource(R.color.Wite_color);
            } else if (rgDamageOccurredOff.isChecked()) {
                isPrintStoreNotice = "N";
                rgDamageOccurredOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgDamageOccurredOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgDamageOccurredOff.setBackgroundResource(R.color.color_43425c);
                rgDamageOccurredOn.setBackgroundResource(R.color.Wite_color);
            }
        }
    };

    /**
     * 세탁물 보관료 안내 ClickListener
     */
    private RadioGroup.OnCheckedChangeListener OnLaundryStorageClicListenr = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rgLaundryStorageOn.isChecked()) {
                IsPrintStotageNotice = "Y";
                rgLaundryStorageOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgLaundryStorageOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgLaundryStorageOn.setBackgroundResource(R.color.color_43425c);
                rgLaundryStorageOff.setBackgroundResource(R.color.Wite_color);
            } else if (rgLaundryStorageOff.isChecked()) {
                IsPrintStotageNotice = "N";
                rgLaundryStorageOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgLaundryStorageOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgLaundryStorageOff.setBackgroundResource(R.color.color_43425c);
                rgLaundryStorageOn.setBackgroundResource(R.color.Wite_color);
            }
        }
    };

    /**
     * 세탁물 인사말 ClickListener
     */
    private RadioGroup.OnCheckedChangeListener OnGreetingClicListenr = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rgGreetingOn.isChecked()) {
                isPrintDamageNotice = "Y";
                rgGreetingOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgGreetingOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgGreetingOn.setBackgroundResource(R.color.color_43425c);
                rgGreetingOff.setBackgroundResource(R.color.Wite_color);
            } else if (rgGreetingOff.isChecked()) {
                isPrintDamageNotice = "N";
                rgGreetingOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgGreetingOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgGreetingOff.setBackgroundResource(R.color.color_43425c);
                rgGreetingOn.setBackgroundResource(R.color.Wite_color);
            }
        }
    };
}
