package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import rewhite.shopplus.R;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.Logger;

/**
 * 앱설정 -> 자주쓰는 메뉴 VIEW
 */
@SuppressLint("ValidFragment")
public class FregmentquentlyUsedMenu extends Fragment {
    private static final String TAG = "FregmentquentlyUsedMenu";

    private RadioGroup rgSendText, rgMonthlySales, rgDailySales, rgMobileSales, rgVisitedReception;
    private RadioGroup rgAutomaticWarhouese, rgLaundryAutomatic, rgMaileageManagement, rgChargeManagement, rgAccountsReceivable, rgStatusUnloadedLaundry;

    private RadioButton rbSendTextOn, rbSendTextOff, rbMonthlySalesOn, rbMonthlySalesOff, rgDailySalesOn, rgDailySalesOff, rgMobileSalesOn, rgMobileSalesOff;
    private RadioButton rgVisitedReceptionOn, rgVisitedReceptionOff, rbAutomaticWarhoueseOn, rbAutomaticWarhoueseOff, rbLaundryAutomaticOn, rbLaundryAutomaticOff;
    private RadioButton rbMaileageManagementOn, rbMaileageManagementOff, rgChargeManagementOn, rgChargeManagementOff, rbAccountsReceivableOn, rbAccountsReceivableOff;
    private RadioButton rbStatusUnloadedLaundryOn, rbStatusUnloadedLaundryOff;

    private AppCompatActivity mActivity;
    public FregmentquentlyUsedMenu(AppCompatActivity activity){
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmently_used_menu, container, false);

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        setRadioLayout(view);
        setRadioClick();
    }

    /**
     * 레이아웃 셋팅
     *
     * @param view
     */
    private void setRadioLayout(View view) {
        rgSendText = (RadioGroup) view.findViewById(R.id.rg_send_text);
        rbSendTextOn = (RadioButton) view.findViewById(R.id.rb_send_text_on);
        rbSendTextOff = (RadioButton) view.findViewById(R.id.rb_send_text_off);

        rgMonthlySales = (RadioGroup) view.findViewById(R.id.rg_monthly_sales);
        rbMonthlySalesOn = (RadioButton) view.findViewById(R.id.rb_monthly_sales_on);
        rbMonthlySalesOff = (RadioButton) view.findViewById(R.id.rb_monthly_sales_off);

        rgDailySales = (RadioGroup) view.findViewById(R.id.rg_daily_sales);
        rgDailySalesOn = (RadioButton) view.findViewById(R.id.rg_daily_sales_on);
        rgDailySalesOff = (RadioButton) view.findViewById(R.id.rg_daily_sales_off);

        rgMobileSales = (RadioGroup) view.findViewById(R.id.rg_mobile_sales);
        rgMobileSalesOn = (RadioButton) view.findViewById(R.id.rb_mobile_sales_on);
        rgMobileSalesOff = (RadioButton) view.findViewById(R.id.rb_mobile_sales_off);

        rgVisitedReception = (RadioGroup) view.findViewById(R.id.rg_visited_reception);
        rgVisitedReceptionOn = (RadioButton) view.findViewById(R.id.rg_visited_reception_on);
        rgVisitedReceptionOff = (RadioButton) view.findViewById(R.id.rg_visited_reception_off);

        rgAutomaticWarhouese = (RadioGroup) view.findViewById(R.id.rg_automatic_warhouese);
        rbAutomaticWarhoueseOn = (RadioButton) view.findViewById(R.id.rb_automatic_warhouese_on);
        rbAutomaticWarhoueseOff = (RadioButton) view.findViewById(R.id.rb_automatic_warhouese_off);

        rgLaundryAutomatic = (RadioGroup) view.findViewById(R.id.rg_laundry_automatic);
        rbLaundryAutomaticOn = (RadioButton) view.findViewById(R.id.rb_laundry_automatic_on);
        rbLaundryAutomaticOff = (RadioButton) view.findViewById(R.id.rb_laundry_automatic_off);

        rgMaileageManagement = (RadioGroup) view.findViewById(R.id.rg_maileage_management);
        rbMaileageManagementOn = (RadioButton) view.findViewById(R.id.rb_maileage_management_on);
        rbMaileageManagementOff = (RadioButton) view.findViewById(R.id.rb_maileage_management_off);

        rgChargeManagement = (RadioGroup) view.findViewById(R.id.rg_charge_management);
        rgChargeManagementOn = (RadioButton) view.findViewById(R.id.rg_charge_management_on);
        rgChargeManagementOff = (RadioButton) view.findViewById(R.id.rg_charge_management_off);

        rgAccountsReceivable = (RadioGroup) view.findViewById(R.id.rg_accounts_receivable);
        rbAccountsReceivableOn = (RadioButton) view.findViewById(R.id.rb_accounts_receivable_on);
        rbAccountsReceivableOff = (RadioButton) view.findViewById(R.id.rb_accounts_receivable_off);

        rgStatusUnloadedLaundry = (RadioGroup) view.findViewById(R.id.rg_status_unloaded_laundry);
        rbStatusUnloadedLaundryOn = (RadioButton) view.findViewById(R.id.rb_status_unloaded_laundry_on);
        rbStatusUnloadedLaundryOff = (RadioButton) view.findViewById(R.id.rb_status_unloaded_laundry_off);

//        radioButtonDefultSetting();
    }

//    private void radioButtonDefultSetting() {
//        setSendTextOnDefult();
//        setMonthlySalesDefult();
//    }
//
//    private void setSendTextOnDefult() {
//        if (SettingLogic.getSendText(getContext()) == true) {
//            rbSendTextOn.setChecked(true);
//        } else {
//            rbSendTextOff.setChecked(false);
//        }
//    }
//
//    private void setMonthlySalesDefult() {
//        if (SettingLogic.getSendText(getContext()) == true) {
//            rbMonthlySalesOn.setChecked(true);
//        } else {
//            rbMonthlySalesOff.setChecked(false);
//        }
//    }

    /**
     * RadioButton Click Listener Setting
     */
    private void setRadioClick() {
        rgSendText.setOnCheckedChangeListener(OnSendTextCheckedListener);
        rgMonthlySales.setOnCheckedChangeListener(OnMonthlySalesCheckedListener);
        rgDailySales.setOnCheckedChangeListener(OnDailySalesCheckedListener);
        rgMobileSales.setOnCheckedChangeListener(OnMobileSalesListener);
        rgVisitedReception.setOnCheckedChangeListener(OnVisitedReceptionListener);
        rgAutomaticWarhouese.setOnCheckedChangeListener(OnAutomaticWarhoueseListener);
        rgLaundryAutomatic.setOnCheckedChangeListener(OnLaundryAutomaticListener);
        rgMaileageManagement.setOnCheckedChangeListener(OnMaileageManagementListener);
        rgChargeManagement.setOnCheckedChangeListener(OnChargeManagementListener);
        rgAccountsReceivable.setOnCheckedChangeListener(OnAccountsReceivableListener);
        rgStatusUnloadedLaundry.setOnCheckedChangeListener(OnStatusUnloadedLaundryListener);
    }

    /**
     * 문자보내기 설정
     */
    private RadioGroup.OnCheckedChangeListener OnSendTextCheckedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbSendTextOn.isChecked()) {
                Logger.d(TAG, "SendTex 1");
                SettingLogic.setSendText(getContext(), true);
                rbSendTextOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbSendTextOff.setBackgroundResource(R.drawable.a_button_type_3);

                rbSendTextOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbSendTextOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rbSendTextOff.isChecked()) {
                Logger.d(TAG, "SendTex 2");
                SettingLogic.setSendText(getContext(), false);
                rbSendTextOn.setBackgroundResource(R.drawable.a_button_type_3);
                rbSendTextOff.setBackgroundResource(R.drawable.a_button_type_2);

                rbSendTextOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbSendTextOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

    /**
     * 월별 매출현황
     */
    private RadioGroup.OnCheckedChangeListener OnMonthlySalesCheckedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbMonthlySalesOn.isChecked()) {
                SettingLogic.setMonthlySalesStatus(getContext(), true);
                rbMonthlySalesOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbMonthlySalesOff.setBackgroundResource(R.drawable.a_button_type_3);

                rbMonthlySalesOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbMonthlySalesOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rbMonthlySalesOff.isChecked()) {
                SettingLogic.setMonthlySalesStatus(getContext(), false);

                rbMonthlySalesOn.setBackgroundResource(R.drawable.a_button_type_3);
                rbMonthlySalesOff.setBackgroundResource(R.drawable.a_button_type_2);
                rbMonthlySalesOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbMonthlySalesOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

    /**
     * 일별 매출현황
     */
    private RadioGroup.OnCheckedChangeListener OnDailySalesCheckedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rgDailySalesOn.isChecked()) {
                SettingLogic.setDailySalesStatus(getContext(), true);

                rgDailySalesOn.setBackgroundResource(R.drawable.a_button_type_2);
                rgDailySalesOff.setBackgroundResource(R.drawable.a_button_type_3);
                rgDailySalesOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgDailySalesOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rgDailySalesOff.isChecked()) {
                SettingLogic.setDailySalesStatus(getContext(), false);

                rgDailySalesOn.setBackgroundResource(R.drawable.a_button_type_3);
                rgDailySalesOff.setBackgroundResource(R.drawable.a_button_type_2);
                rgDailySalesOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rgDailySalesOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

    /**
     * 모바일 매출현황
     */
    private RadioGroup.OnCheckedChangeListener OnMobileSalesListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rgMobileSalesOn.isChecked()) {
                SettingLogic.setMobileSalseStatus(getContext(), true);

                rgMobileSalesOn.setBackgroundResource(R.drawable.a_button_type_2);
                rgMobileSalesOff.setBackgroundResource(R.drawable.a_button_type_3);

                rgMobileSalesOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgMobileSalesOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rgMobileSalesOff.isChecked()) {
                SettingLogic.setMobileSalseStatus(getContext(), false);

                rgMobileSalesOn.setBackgroundResource(R.drawable.a_button_type_3);
                rgMobileSalesOff.setBackgroundResource(R.drawable.a_button_type_2);

                rgMobileSalesOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rgMobileSalesOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

    /**
     * 방문접수 매출현황
     */
    private RadioGroup.OnCheckedChangeListener OnVisitedReceptionListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rgVisitedReceptionOn.isChecked()) {
                SettingLogic.setVisitedReceptionSales(getContext(), true);

                rgVisitedReceptionOn.setBackgroundResource(R.drawable.a_button_type_2);
                rgVisitedReceptionOff.setBackgroundResource(R.drawable.a_button_type_3);

                rgVisitedReceptionOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgVisitedReceptionOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rgVisitedReceptionOff.isChecked()) {
                SettingLogic.setVisitedReceptionSales(getContext(), false);

                rgVisitedReceptionOn.setBackgroundResource(R.drawable.a_button_type_3);
                rgVisitedReceptionOff.setBackgroundResource(R.drawable.a_button_type_2);

                rgVisitedReceptionOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rgVisitedReceptionOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

    /**
     * 세탁물 자동출고
     */
    private RadioGroup.OnCheckedChangeListener OnAutomaticWarhoueseListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbAutomaticWarhoueseOn.isChecked()) {
                SettingLogic.setAutomaticWarehoueseLaundry(getContext(), true);

                rbAutomaticWarhoueseOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbAutomaticWarhoueseOff.setBackgroundResource(R.drawable.a_button_type_3);

                rbAutomaticWarhoueseOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbAutomaticWarhoueseOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rbAutomaticWarhoueseOff.isChecked()) {
                SettingLogic.setAutomaticWarehoueseLaundry(getContext(), false);

                rbAutomaticWarhoueseOn.setBackgroundResource(R.drawable.a_button_type_3);
                rbAutomaticWarhoueseOff.setBackgroundResource(R.drawable.a_button_type_2);

                rbAutomaticWarhoueseOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbAutomaticWarhoueseOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

    /**
     * 세탁물 자동 세탁완료
     */
    private RadioGroup.OnCheckedChangeListener OnLaundryAutomaticListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbLaundryAutomaticOn.isChecked()) {
                SettingLogic.setLaundryAutomaticLaundry(getContext(), true);
                rbLaundryAutomaticOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbLaundryAutomaticOff.setBackgroundResource(R.drawable.a_button_type_3);

                rbLaundryAutomaticOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbLaundryAutomaticOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rbLaundryAutomaticOff.isChecked()) {
                SettingLogic.setLaundryAutomaticLaundry(getContext(), false);
                rbLaundryAutomaticOn.setBackgroundResource(R.drawable.a_button_type_3);
                rbLaundryAutomaticOff.setBackgroundResource(R.drawable.a_button_type_2);

                rbLaundryAutomaticOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbLaundryAutomaticOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

    /**
     * 마일리지 관리
     */
    private RadioGroup.OnCheckedChangeListener OnMaileageManagementListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbMaileageManagementOn.isChecked()) {
                SettingLogic.setMileageManagement(getContext(), true);
                rbMaileageManagementOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbMaileageManagementOff.setBackgroundResource(R.drawable.a_button_type_3);

                rbMaileageManagementOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbMaileageManagementOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rbMaileageManagementOff.isChecked()) {
                SettingLogic.setMileageManagement(getContext(), false);
                rbMaileageManagementOn.setBackgroundResource(R.drawable.a_button_type_3);
                rbMaileageManagementOff.setBackgroundResource(R.drawable.a_button_type_2);

                rbMaileageManagementOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbMaileageManagementOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

    /**
     * 선충전금 관리
     */
    private RadioGroup.OnCheckedChangeListener OnChargeManagementListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rgChargeManagementOn.isChecked()) {
                SettingLogic.setPreChargeManaement(getContext(), true);
                rgChargeManagementOn.setBackgroundResource(R.drawable.a_button_type_2);
                rgChargeManagementOff.setBackgroundResource(R.drawable.a_button_type_3);

                rgChargeManagementOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgChargeManagementOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rgChargeManagementOff.isChecked()) {
                SettingLogic.setPreChargeManaement(getContext(), false);
                rgChargeManagementOn.setBackgroundResource(R.drawable.a_button_type_3);
                rgChargeManagementOff.setBackgroundResource(R.drawable.a_button_type_2);

                rgChargeManagementOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rgChargeManagementOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

    /**
     * 미수금 관리
     */
    private RadioGroup.OnCheckedChangeListener OnAccountsReceivableListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbAccountsReceivableOn.isChecked()) {
                SettingLogic.setAccountReceivableManagement(getContext(), true);
                rbAccountsReceivableOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbAccountsReceivableOff.setBackgroundResource(R.drawable.a_button_type_3);

                rbAccountsReceivableOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbAccountsReceivableOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rbAccountsReceivableOff.isChecked()) {
                SettingLogic.setAccountReceivableManagement(getContext(), false);
                rbAccountsReceivableOn.setBackgroundResource(R.drawable.a_button_type_3);
                rbAccountsReceivableOff.setBackgroundResource(R.drawable.a_button_type_2);

                rbAccountsReceivableOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbAccountsReceivableOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

    /**
     * 미출고 세탁물현황
     */
    private RadioGroup.OnCheckedChangeListener OnStatusUnloadedLaundryListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbStatusUnloadedLaundryOn.isChecked()) {
                SettingLogic.setStatusUnLoadedLaundry(getContext(), true);
                rbStatusUnloadedLaundryOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbStatusUnloadedLaundryOff.setBackgroundResource(R.drawable.a_button_type_3);

                rbStatusUnloadedLaundryOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbStatusUnloadedLaundryOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rbStatusUnloadedLaundryOff.isChecked()) {
                SettingLogic.setStatusUnLoadedLaundry(getContext(), false);
                rbStatusUnloadedLaundryOn.setBackgroundResource(R.drawable.a_button_type_3);
                rbStatusUnloadedLaundryOff.setBackgroundResource(R.drawable.a_button_type_2);

                rbStatusUnloadedLaundryOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbStatusUnloadedLaundryOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            }
        }
    };

}