package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerClassList;
import rewhite.shopplus.client.manager.ManagerGetStoreItemList;
import rewhite.shopplus.client.manager.ManagerLaundryCategory;
import rewhite.shopplus.client.model.ClassListModel;
import rewhite.shopplus.client.model.GetStoreItemList;
import rewhite.shopplus.client.model.LaundryCategoryItemList;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.common.CustomKeyboard1;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.enviroment.AdapterCategoryDetail;
import rewhite.shopplus.view.adapter.enviroment.AdapterLaundryEdit;
import rewhite.shopplus.view.adapter.enviroment.AdapterLaundryRateTable;
import rewhite.shopplus.view.adapter.enviroment.AdapterLundryItemAdd;

/**
 * 요금표설정 -> 세탁요금표 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentLaundryRateTable extends Fragment {
    private static final String TAG = "FragmentLaundryRateTable";

    private RecyclerView laundryRecyclerView, detailRecyclerView, categoreDetailRecyclerView;
    private RecyclerView.LayoutManager laundryLayoutManager;
    private RecyclerView.LayoutManager detailLayoutManager;
    private RecyclerView.LayoutManager laundryDetailLayoutManager;
    private Dialog dialog;
    private WaitCounter mWait;

    private LinearLayout llNoResultsFound, llListExistence;
    private EditText etMoneyCount;
    private TextView tvExistingCharge, tvItemNameTitle, tvTitleName;
    private Button btnItemAdd, btnSave;
    private CustomKeyboard1 mCustomKeyboard;
    private InputMethodManager imm;

    private String mStoreItemName;
    private int itemVisitPrice, itemVisitPrice2, itemVisitPrice3;

    private int classID, categoryID, feeType;
    private boolean itemAddStatus = true;

    private List<GetStoreItemList> storeItemListAdd;

    private List<LaundryCategoryItemList> storeItemList;
    private AppCompatActivity mActivity;

    private long mStoreItemID;

    private View view = null;

    public FragmentLaundryRateTable(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_laundry_rate_table, container, false);
        mWait = new WaitCounter(getContext());

        init(view);
        return view;
    }

    private void init(View view) {
        setLayout(view);
        setAdapterListData();
    }

    /**
     * layout Setting
     *
     * @param view
     */
    private void setLayout(View view) {
        tvTitleName = (TextView) view.findViewById(R.id.tv_title_name);
        etMoneyCount = (EditText) view.findViewById(R.id.et_money_count);
        btnItemAdd = (Button) view.findViewById(R.id.btn_item_add);
        btnSave = (Button) view.findViewById(R.id.btn_save);

        llNoResultsFound = (LinearLayout) view.findViewById(R.id.ll_no_results_found);
        llListExistence = (LinearLayout) view.findViewById(R.id.ll_list_existence);

        tvExistingCharge = (TextView) view.findViewById(R.id.tv_existing_charge);
        tvItemNameTitle = (TextView) view.findViewById(R.id.tv_item_name_title);

        laundryRecyclerView = (RecyclerView) view.findViewById(R.id.laundry_recycler_view);
        detailRecyclerView = (RecyclerView) view.findViewById(R.id.detail_recycler_view);

        setCustomKeyBoard();
        btnItemAdd.setOnClickListener(btnItemAddListener);
        btnSave.setOnClickListener(btnItemAddListener);
    }


    /**
     * CustomKeyBoard reset
     * //     * @param view
     */
    private void setCustomKeyBoard() {
        mCustomKeyboard = new CustomKeyboard1((AppCompatActivity)getActivity(), view, R.id.keyboardview, R.xml.custom_keyboard_type2);
        mCustomKeyboard.registerEditText(view, R.id.et_money_count);
    }


    /**
     * 품목 정보 리스트
     */
    public void setAdapterListData() {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    final ManagerLaundryCategory managerLaundryCategory = LaundryRegistrationManager.getManagerStoreUserList((AppCompatActivity)getActivity(), Long.valueOf(1));
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                            storeItemList = managerLaundryCategory.getLaundryCategoty();

                            //세탁물 취급설정에 따른 View 변경
                            if (storeItemList.size() == 0) {
                                llNoResultsFound.setVisibility(View.VISIBLE);
                                llListExistence.setVisibility(View.GONE);
                            } else {
                                llNoResultsFound.setVisibility(View.GONE);
                                llListExistence.setVisibility(View.VISIBLE);

                                tvTitleName.setText(storeItemList.get(0).getCategoryName());    //디폴트 TextName 설정
                                categoryID = storeItemList.get(0).getCategoryId();

                                setAdapterDetailListData(); //디폴트 품목 상세 리스트 View
                            }

                            laundryRecyclerView.setHasFixedSize(true);

                            laundryLayoutManager = new StaggeredGridLayoutManager(11, StaggeredGridLayoutManager.VERTICAL);
                            laundryRecyclerView.setLayoutManager(laundryLayoutManager);

                            AdapterLaundryRateTable adapterLaundryRateTable = new AdapterLaundryRateTable(getActivity(), mOnAdapterListner);
                            adapterLaundryRateTable.addItem(storeItemList);
                            laundryRecyclerView.setAdapter(adapterLaundryRateTable);
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * 세탁 품목 상세 리스트 조회
     */
    private void setAdapterDetailListData() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                final ManagerGetStoreItemList managerGetStoreItemList = EnviromentManager.getManagerGetStoreItemList((AppCompatActivity)getActivity(), 1, categoryID, 0);

                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        storeItemListAdd = managerGetStoreItemList.getStoreItemList();

                        detailRecyclerView.setHasFixedSize(true);
                        detailLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                        detailRecyclerView.setLayoutManager(detailLayoutManager);

                        AdapterLaundryEdit adapterLaundryRateTable = new AdapterLaundryEdit(getActivity(), laundryEditLisner, existingCharge);
                        adapterLaundryRateTable.addItem(storeItemListAdd);
                        detailRecyclerView.setAdapter(adapterLaundryRateTable);
                        adapterLaundryRateTable.notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }


    private View.OnClickListener btnItemAddListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_item_add:
                    setClosedRegularlyDialog(0);
                    break;
                case R.id.btn_save:
                    if (etMoneyCount.getText().toString().equals("") || TextUtils.isEmpty(etMoneyCount.getText().toString())) {
                        etMoneyCount.setText("0");
                    }
                    new Thread(new Runnable() {
                        public void run() {
                            //부속품 요금 변경 API
                            if (feeType == 0) {
                                EnviromentManager.getRUpdateStoreItemPrice((AppCompatActivity)getActivity(), mStoreItemID, Integer.valueOf(etMoneyCount.getText().toString()), itemVisitPrice2, itemVisitPrice3, 0, 0, 0);
                            } else if (feeType == 1) {
                                EnviromentManager.getRUpdateStoreItemPrice((AppCompatActivity)getActivity(), mStoreItemID, itemVisitPrice, Integer.valueOf(etMoneyCount.getText().toString()), itemVisitPrice3, 0, 0, 0);
                            } else {
                                EnviromentManager.getRUpdateStoreItemPrice((AppCompatActivity)getActivity(), mStoreItemID, itemVisitPrice, itemVisitPrice2, Integer.valueOf(etMoneyCount.getText().toString()), 0, 0, 0);
                            }

                            mActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    //ListView Date Refresh
                                    etMoneyCount.setText("");
                                    setAdapterDetailListData();
                                }
                            });
                        }
                    }).start();

                    break;
            }
        }
    };

    /**
     * 세탁요금표 소분류 추가 Dialog View
     */
    private void setClosedRegularlyDialog(int type) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_lundry_item_add, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);

        RecyclerView categoreRecyclerView = (RecyclerView) view.findViewById(R.id.categore_recycle_view);
        LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);
        categoreDetailRecyclerView = (RecyclerView) view.findViewById(R.id.categore_detail_recycle_view);
        TextView tvTitleName = (TextView) view.findViewById(R.id.tv_title_name);
        ImageView imgCloose = (ImageView) view.findViewById(R.id.img_close);

        imgCloose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(getActivity());
                thirdConfirmPopup.setTitle("종료 팝업");
                thirdConfirmPopup.setContent("등록을 취소할까요?");
                thirdConfirmPopup.setButtonText("예");
                thirdConfirmPopup.setCancelButtonText("아니요");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        dialog.dismiss();
                    }
                });
                thirdConfirmPopup.show();
            }
        });
        EditText etItemName = (EditText) view.findViewById(R.id.et_item_name);
        Button btnItemAdd = (Button) view.findViewById(R.id.btn_item_add);

        if (type == 0) {
            tvTitleName.setText("세탁품목 추가");
            btnItemAdd.setText("추가하기");
        } else {
            etItemName.setHint(mStoreItemName);
            tvTitleName.setText("세탁품목 수정");
            btnItemAdd.setText("수정하기");
        }

        setMiddleClassAdapter(1, type);

        AdapterLundryItemAdd adapterLundryItemAdd = new AdapterLundryItemAdd(getActivity(), adapterListner, type);
        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);

        categoreRecyclerView.setHasFixedSize(true);
        categoreRecyclerView.setLayoutManager(layoutManager);
        adapterLundryItemAdd.addItem(storeItemList);
        categoreRecyclerView.setAdapter(adapterLundryItemAdd);

        btnItemAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etItemName.getText().toString().length() == 0) {
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());

                    thirdAlertPopup.setTitle("알림");
                    thirdAlertPopup.setContent("품목명을 적어주세요.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                } else {
                    List<GetStoreItemList> getStoreItemLists = ManagerGetStoreItemList.getmInstance().getStoreItemList();
                    for (GetStoreItemList getStoreItemList : getStoreItemLists) {
                        if (getStoreItemList.getItemName().equals(etItemName.getText().toString())) {
                            itemAddStatus = false;
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());

                            thirdAlertPopup.setTitle("알림");
                            thirdAlertPopup.setContent("이미 존재하는 이름이에요. 다시 입력해주세요.");
                            thirdAlertPopup.setButtonText("확인");
                            thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    itemAddStatus = true;
                                }
                            });
                            thirdAlertPopup.show();
                        }
                    }
                    if (itemAddStatus == true) {
                        setItemAdd(etItemName.getText().toString(), type);
                        etItemName.setText("");
                    }
                }
            }
        });

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * 정상적으로 품목이 추가 된 결과 체크 로직
     * (데이터 화면 갱신 하기 위한 로직 추가.)
     *
     * @param itemName
     */
    private void setItemAdd(String itemName, int type) {
        new Thread(new Runnable() {
            public void run() {
                if (type == 0) {
                    EnviromentManager.getAddStoreItem((AppCompatActivity)getActivity(), classID, itemName);   //품목 추가
                } else {
                    EnviromentManager.getUpdateStoreItem((AppCompatActivity)getActivity(), mStoreItemID, itemName); //품목 Name 수정
                }
                EnviromentManager.getManagerStoreManageInfo((AppCompatActivity)getActivity());
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());
                        if (type == 0) {

                            thirdAlertPopup.setTitle("품목추가");
                            thirdAlertPopup.setContent(itemName + " 추가 완료 되었습니다.");
                            thirdAlertPopup.setButtonText("확인");
                        } else {
                            thirdAlertPopup.setTitle("품목수정");
                            thirdAlertPopup.setContent(itemName + " 수정 되었습니다.");
                            thirdAlertPopup.setButtonText("확인");
                        }
                        thirdAlertPopup.show();
                        dialog.dismiss();


                        //Fragment View 갱신
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.detach(FragmentLaundryRateTable.this).attach(FragmentLaundryRateTable.this).commit();
                    }
                });
            }
        }).start();
    }

    /**
     * 중분류 상세 리스트
     *
     * @param categoryID
     */
    private void setMiddleClassAdapter(int categoryID, int type) {
//        if (type == 1) {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    ManagerClassList managerClassList = LaundryRegistrationManager.getManagerClassList((AppCompatActivity)getActivity(), categoryID);
                    mActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                            List<ClassListModel> storeItemLists = managerClassList.getClassListInfo();

                            Logger.d(TAG, "storeItemLists : " + storeItemLists.size());

                            categoreDetailRecyclerView.setHasFixedSize(true);
                            laundryDetailLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
                            categoreDetailRecyclerView.setLayoutManager(laundryDetailLayoutManager);

                            AdapterCategoryDetail adapterCategoryDetail = new AdapterCategoryDetail(getActivity(), adapterDetailListner, 0);
                            adapterCategoryDetail.addItem(storeItemLists);
                            categoreDetailRecyclerView.setAdapter(adapterCategoryDetail);
                            adapterCategoryDetail.notifyDataSetChanged();
                        }
                    });
                }
            }).start();
        }
//    }


    public interface existingCharge {
        void setExistionCharge(int amount, String itemName, long storeItemID, int type, int getItemVisitPrice, int getItemVisitPrice2, int getItemVisitPrice3);

        void setNotToUse(); //품목사용 여부 변경 reflesh

        void setItemName(int storeItemID, String storeItemName);
    }

    private existingCharge existingCharge = new existingCharge() {
        @Override
        public void setExistionCharge(int amount, String itemName, long storeItemID, int type, int getItemVisitPrice, int getItemVisitPrice2, int getItemVisitPrice3) {
            tvExistingCharge.setText(String.valueOf(amount));
            tvItemNameTitle.setText(itemName);
            mStoreItemID = storeItemID;

            itemVisitPrice = getItemVisitPrice;
            itemVisitPrice2 = getItemVisitPrice2;
            itemVisitPrice3 = getItemVisitPrice3;
            feeType = type;
        }

        @Override
        public void setNotToUse() {
            setAdapterDetailListData();
        }

        @Override
        public void setItemName(int storeItemID, String storeItemName) {
            mStoreItemID = storeItemID;
            mStoreItemName = storeItemName;
            setClosedRegularlyDialog(1);
        }
    };

    /**
     * 대분류 (세탁 품목 상세 리스트 조회) ViewHolder Adapter
     */
    private AdapterLaundryEdit.CCViewHolder.OnAdapterListner laundryEditLisner = new AdapterLaundryEdit.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int amount) {

        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {
        }
    };

    /**
     * 대분류 (품목 정보 리스트) ViewHolder Adapter
     */
    private AdapterLaundryRateTable.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterLaundryRateTable.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
            categoryID = storeItemList.get(position).getCategoryId();

            tvTitleName.setText(storeItemList.get(position).getCategoryName()); //선택된 View 변경
            setAdapterDetailListData();
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

    /**
     * 소분류 ViewHolder Adapter
     */
    private AdapterCategoryDetail.CCViewHolder.OnAdapterListner adapterDetailListner = new AdapterCategoryDetail.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
            classID = position;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

    /**
     * 중분류 ViewHolder Adapter
     */
    private AdapterLundryItemAdd.CCViewHolder.OnAdapterListner adapterListner = new AdapterLundryItemAdd.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int categoryID) {
            setMiddleClassAdapter(categoryID, 0);
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
}