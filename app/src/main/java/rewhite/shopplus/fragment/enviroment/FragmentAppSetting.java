package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.util.CustomViewPager;
import rewhite.shopplus.view.adapter.enviroment.AdapterAppSetting;

/**
 * 앱설정 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentAppSetting extends Fragment {
    private static final String TAG = "FragmentAppSetting";

    private CustomViewPager viewPager;

    private TextView tvQuentlyUsedMenu, tvAccountInfomation;
    private LinearLayout llBg;

    private AppCompatActivity mActivity;

    public FragmentAppSetting(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_app_setting, container, false);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FragmentAccountInformation fragmentAccountInformation = new FragmentAccountInformation((AppCompatActivity)getActivity());   //세탁물 취급설정
        fragmentTransaction.replace(R.id.fragment_app_setting_ratetable, fragmentAccountInformation);
        fragmentTransaction.commit();

//        setLayout(view);
//        setViewPagerAdapter();
        return view;
    }

    private void setLayout(View view) {
//        viewPager = (CustomViewPager) view.findViewById(R.id.setting_custom_viewpager);

        llBg = (LinearLayout) view.findViewById(R.id.ll_bg);
//        tvQuentlyUsedMenu = (TextView) view.findViewById(R.id.tv_quently_used_menu);
        tvAccountInfomation = (TextView) view.findViewById(R.id.tv_account_information);
    }


    private void setViewPagerAdapter() {
        tvQuentlyUsedMenu.setOnClickListener(movePageListener);
        tvQuentlyUsedMenu.setTag(0);
        tvAccountInfomation.setOnClickListener(movePageListener);
        tvAccountInfomation.setTag(1);

        tvQuentlyUsedMenu.setSelected(true);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        viewPager.setAdapter(new AdapterAppSetting(getActivity().getSupportFragmentManager(), mActivity));
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(2);
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            int selectPosition = 0;

            while (selectPosition < 2) {
                if (position == selectPosition) {
                    llBg.findViewWithTag(selectPosition).setSelected(true);
                } else {
                    llBg.findViewWithTag(selectPosition).setSelected(false);
                }
                selectPosition++;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();

            int selectPosition = 0;
            while (selectPosition < 2) {
                if (tag == selectPosition) {
                    llBg.findViewWithTag(selectPosition).setSelected(true);
                } else {
                    llBg.findViewWithTag(selectPosition).setSelected(false);
                }
                selectPosition++;
            }
            viewPager.setCurrentItem(tag);
        }
    };
}