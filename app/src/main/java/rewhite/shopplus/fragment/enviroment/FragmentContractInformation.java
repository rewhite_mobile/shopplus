package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerStoreContractInfo;
import rewhite.shopplus.client.model.GetStoreContractInfoModel;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.util.WaitCounter;

/**
 * 영업설정 -> 계약정보 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentContractInformation extends Fragment {
    private static final String TAG = "FragmentContractInformation";
    private WaitCounter mWait;
    private List<GetStoreContractInfoModel> getStoreContractInfoModels;
    private AppCompatActivity mActivity;

    private TextView tvBusinessName, tvPhoneNumber, tvAddress, tvSuggestedCode;
    private TextView tvBanks, tvAccount, tvMobilePay, tvReceivablesFunction, tvNaverOrder, tvSalespersonName, tvSalespersonPhone;
    private TextView tvStoreName, tvBusinessRegistrationNumber, tvUserBanks, tvUserBanksAccount, tvMasterEmail, tvSettlementOfficer, tvContactsNumber;

    public FragmentContractInformation(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contract, container, false);
        mWait = new WaitCounter(getActivity());

        init(view);
        return view;
    }

    /**
     * API init
     * @param view
     */
    private void init(View view) {
        try {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    EnviromentManager.getStoreContractInfo((AppCompatActivity)getActivity());
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            getStoreContractInfoModels = ManagerStoreContractInfo.getmInstance().getStoreContractInfo();
                            setLayout(view);
                            mWait.dismiss();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    /**
     * 레이아웃 View 셋팅
     * @param view
     */
    private void setLayout(View view) {
        tvBusinessName = (TextView) view.findViewById(R.id.tv_business_name);
        tvPhoneNumber = (TextView) view.findViewById(R.id.tv_phone_number);
        tvAddress = (TextView) view.findViewById(R.id.tv_address);
        tvSuggestedCode = (TextView) view.findViewById(R.id.tv_suggested_code);

        tvBanks = (TextView) view.findViewById(R.id.tv_banks);
        tvAccount = (TextView) view.findViewById(R.id.tv_account);
        tvMobilePay = (TextView) view.findViewById(R.id.tv_mobile_pay);
        tvReceivablesFunction = (TextView) view.findViewById(R.id.tv_receivables_function);
        tvNaverOrder = (TextView) view.findViewById(R.id.tv_naver_order);
        tvSalespersonName = (TextView) view.findViewById(R.id.tv_salesperson_name);
        tvSalespersonPhone = (TextView) view.findViewById(R.id.tv_salesperson_phone);

        tvStoreName = (TextView) view.findViewById(R.id.tv_store_name);
        tvBusinessRegistrationNumber = (TextView) view.findViewById(R.id.tv_business_registration_number);
        tvUserBanks = (TextView) view.findViewById(R.id.tv_user_banks);
        tvUserBanksAccount = (TextView) view.findViewById(R.id.tv_user_banks_account);
        tvMasterEmail = (TextView) view.findViewById(R.id.tv_master_email);
        tvSettlementOfficer = (TextView) view.findViewById(R.id.tv_settlement_officer);
        tvContactsNumber = (TextView) view.findViewById(R.id.tv_contacts_number);

        setTextView();
    }

    /**
     * 초기 TextView Setting
     */
    private void setTextView() {
        tvBusinessName.setText(getStoreContractInfoModels.get(0).getData().getStoreName());
        tvPhoneNumber.setText(getStoreContractInfoModels.get(0).getData().get_MasterPhone());
        tvAddress.setText(getStoreContractInfoModels.get(0).getData().get_StoreAddress1());
        tvSuggestedCode.setText(getStoreContractInfoModels.get(0).getData().getShareCode());

        tvBanks.setText(getStoreContractInfoModels.get(0).getData().getAccountBankName());
        tvAccount.setText(getStoreContractInfoModels.get(0).getData().get_AccountBankNumber());
        tvSalespersonName.setText(getStoreContractInfoModels.get(0).getData().getContractManagerName());
        tvSalespersonPhone.setText(getStoreContractInfoModels.get(0).getData().get_ContractManagerPhone());
    }
}