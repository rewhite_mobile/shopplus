package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.Logger;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterComponent;
import rewhite.shopplus.view.adapter.enviroment.AdapterCloseData;
import rewhite.shopplus.view.adapter.enviroment.AdapterSaturdayEnd;
import rewhite.shopplus.view.adapter.enviroment.AdapterSaturdayStart;
import rewhite.shopplus.view.adapter.enviroment.AdapterSundayEnd;
import rewhite.shopplus.view.adapter.enviroment.AdapterSundayStart;
import rewhite.shopplus.view.adapter.enviroment.AdapterWeek;
import rewhite.shopplus.view.adapter.enviroment.AdapterWeekdayEnd;
import rewhite.shopplus.view.adapter.enviroment.AdapterWeekdayStart;

/**
 * 영업설정 -> 영업시간 및 휴무설정 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentOpeningHours extends Fragment {
    private static final String TAG = "FragmentOpeningHours";
    private View view;

    private LinearLayout llNoClosedSaturday, llClosedSaturday, llNoClosedSunday, llClosedSunday;

    private ArrayList<String> closeDate;
    private TextView tvWeeklySettings, tvDaySetting;
    private Button btnClosedRegularly, btnBusinessHours;
    private RadioButton rgEveryWeek, rbBiweeklyA, rbBiweeklyB;
    private WaitCounter mWait;

    private Dialog dialog, HoursDialog;
    private AppCompatActivity mActivity;

    private LinearLayout llGoneBusinessHoursSaturday, llBusinessHoursSaturday, llGoneBusinessHoursSunday, llBusinessHoursSunday;

    private TextView idBusinessHoursWeekday, idBusinessHoursAfternoon, idSaturdayMorning, idSaturdayAfternoon, idHoursSundayMorning, idHoursSundayAfternoon;
    private String closedRegularly, closedDays;
    private RecyclerView recycleClosedView;

    private EditText etTimeRequired;

    public FragmentOpeningHours(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_opening_hours, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mWait = new WaitCounter(mActivity);
        init();
    }

    private void init() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                EnviromentManager.getManagerStoreManageInfo(mActivity);
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        setLayout();

                    }
                });
            }
        }).start();
    }


    /**
     * 레이아웃 셋팅
     */
    private void setLayout() {
        llNoClosedSaturday = (LinearLayout) view.findViewById(R.id.ll_no_closed_saturday);
        llClosedSaturday = (LinearLayout) view.findViewById(R.id.ll_closed_saturday);
        llNoClosedSunday = (LinearLayout) view.findViewById(R.id.ll_no_closed_sunday);
        llClosedSunday = (LinearLayout) view.findViewById(R.id.ll_closed_sunday);

        tvWeeklySettings = (TextView) view.findViewById(R.id.tv_weekly_settings);
        tvDaySetting = (TextView) view.findViewById(R.id.tv_day_setting);

        btnClosedRegularly = (Button) view.findViewById(R.id.btn_closed_regularly);
        btnBusinessHours = (Button) view.findViewById(R.id.btn_business_hours);
        LinearLayout btnCalender = (LinearLayout) view.findViewById(R.id.btn_calender);
        recycleClosedView = (RecyclerView) view.findViewById(R.id.recycle_closed_view);

        idBusinessHoursWeekday = (TextView) view.findViewById(R.id.id_business_hours_weekday);
        idBusinessHoursAfternoon = (TextView) view.findViewById(R.id.id_business_hours_afternoon);
        idSaturdayMorning = (TextView) view.findViewById(R.id.id_saturday_morning);
        idSaturdayAfternoon = (TextView) view.findViewById(R.id.id_saturday_afternoon);
        idHoursSundayMorning = (TextView) view.findViewById(R.id.id_hours_sunday_morning);
        idHoursSundayAfternoon = (TextView) view.findViewById(R.id.id_hours_sunday_afternoon);

        etTimeRequired = (EditText) view.findViewById(R.id.et_time_required);
        btnCalender.setOnClickListener(btnOnClickListener);
        btnClosedRegularly.setOnClickListener(btnOnClickListener);
        btnBusinessHours.setOnClickListener(btnOnClickListener);
        setClosedDateView();
        setTextView();
    }

    /**
     * Text Setting
     */
    private void setTextView() {
        try {
//            EnviromentManager.getUpdateWashingCostDay(getActivity(), Integer.valueOf(etTimeRequired.toString()));
            etTimeRequired.setText(etTimeRequired.toString());
        } catch (Exception err) {
            err.printStackTrace();
        }

        try {
            etTimeRequired.setText(String.valueOf(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getWashingCostDay()));

            idBusinessHoursWeekday.setText(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getServiceStartTime());
            idBusinessHoursAfternoon.setText(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getServiceEndTime());

            idSaturdayMorning.setText(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getServiceStartTime_Sat());
            idSaturdayAfternoon.setText(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getServiceEndTime_Sat());

            idHoursSundayMorning.setText(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getServiceStartTime_Sun());
            idHoursSundayAfternoon.setText(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getServiceEndTime_Sun());

            tvWeeklySettings.setText(DataFormatUtil.closingWeekType(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingWeekType()));
            tvDaySetting.setText(DataFormatUtil.daySettingType(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingWeek()));

//            setClosedDaysView();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.btn_closed_regularly:
                    setClosedRegularlyDialog();
                    break;

                case R.id.btn_business_hours:
                    setBusinessHoursDialog();
                    break;

                case R.id.btn_calender:
                    Calendar mcurrentDate = Calendar.getInstance();
                    int mYear = mcurrentDate.get(Calendar.YEAR);
                    int mMonth = mcurrentDate.get(Calendar.MONTH + 1);
                    int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                    Logger.d(TAG, "mYear : " + mYear);
                    Logger.d(TAG, "mMonth : " + mMonth);
                    Logger.d(TAG, "mDay : " + mDay);
                    DatePickerDialog dialog = new DatePickerDialog(getActivity(), listener, mYear, mMonth, mDay);
                    dialog.show();
                    break;
            }
        }
    };

    /**
     * 예정 휴무일 View
     */
    private void setClosedDateView() {
        closeDate = ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingDays();

        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(5, StaggeredGridLayoutManager.VERTICAL);
        recycleClosedView.setHasFixedSize(true);
        recycleClosedView.setLayoutManager(layoutManager);

        AdapterCloseData closeData = new AdapterCloseData((AppCompatActivity)getActivity(), onAdapterListner, onCompletItem);
        closeData.addItem(closeDate);
        recycleClosedView.setAdapter(closeData);
    }

    private DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Logger.d(TAG, "year : " + year);
            Logger.d(TAG, "monthOfYear : " + monthOfYear);
            Logger.d(TAG, "dayOfMonth : " + dayOfMonth);

            Calendar selectDay = Calendar.getInstance();
            selectDay.set(Calendar.YEAR, year);
            selectDay.set(Calendar.MONTH, dayOfMonth);
            selectDay.set(Calendar.DATE, monthOfYear);

            String day = DataFormatUtil.getDateHyphen(selectDay);

            Logger.d(TAG, "day  : " + day);
//            for (String close : closeDate) {
//                if (close.equals("day")) {
//                    Logger.d(TAG, "day1  : " + day);
//                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());
//                    thirdAlertPopup.setTitle("알림");
//                    thirdAlertPopup.setContent("이미 등록된 날짜에요.");
//                    thirdAlertPopup.show();
//                } else {
            mWait.show();
            new Thread(new Runnable() {
                public void run() {
                    Logger.d(TAG, "day2  : " + day);
                    EnviromentManager.getAddClosingDay((AppCompatActivity)getActivity(), day);
                    mActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                        }
                    });
                }
            }).start();
        }
//            }

//        }
    };

    /**
     * 정기휴무일 설정 Dialog View
     */
    private void setClosedRegularlyDialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_closed_regularly_setting, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        AdapterWeek adapterWeek = new AdapterWeek(getActivity(), mOnAdapterWeekListner, onCompletItem, DataFormatUtil.daySettingType(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingWeek()));

        String[] weekDay = getResources().getStringArray(R.array.week);
        ArrayList<String> startTimeList = new ArrayList<>(Arrays.asList(weekDay));

        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(7, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);

        adapterWeek.itemAdd(startTimeList);
        recyclerView.setAdapter(adapterWeek);

        LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);
        Button btnSave = (Button) view.findViewById(R.id.btn_save);
        RadioGroup rgCloseWeek = (RadioGroup) view.findViewById(R.id.rg_close_week);

        rgEveryWeek = (RadioButton) view.findViewById(R.id.rg_every_week);
        rbBiweeklyA = (RadioButton) view.findViewById(R.id.rb_biweekly_a);
        rbBiweeklyB = (RadioButton) view.findViewById(R.id.rb_biweekly_b);
        setCloseDefaultView();

        rgCloseWeek.setOnCheckedChangeListener(radioPortableCheckedListener);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mWait.show();
                    new Thread(new Runnable() {
                        public void run() {
                            if (!closedDays.equals("") || !TextUtils.isEmpty(closedDays) || closedRegularly != null || !closedRegularly.equals("") || !TextUtils.isEmpty(closedRegularly)) {
                                EnviromentManager.getUpdateClosedRegularly((AppCompatActivity)getActivity(), Integer.valueOf(closedRegularly), closedDays);
                            } else {
                                mWait.dismiss();
                                dialog.dismiss();
                            }
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());
                                    thirdAlertPopup.setTitle("알림");
                                    thirdAlertPopup.setContent("저장이 완료되었어요.");
                                    thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                        @Override
                                        public void onConfirmClick() {
                                            mWait.dismiss();
                                            dialog.dismiss();
                                            init();
                                        }
                                    });
                                    thirdAlertPopup.show();
                                }
                            });
                        }
                    }).start();
                } catch (Exception err) {
                    err.printStackTrace();
                }
            }
        });

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regularlyClosed("변경하신 내용은 삭제됩니다.\n" + "팝업을 종료할까요?", dialog);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });
        dialog.show();
    }

    private void regularlyClosed(String mag, Dialog dialog) {
        AlertDialog.Builder closedDialog = new AlertDialog.Builder(getActivity());
        AlertDialog alertDialog = closedDialog.create();

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_confirm_popup, null);
        TextView tvmsg = (TextView) view.findViewById(R.id.tv_msg);
        Button btnCancle = (Button) view.findViewById(R.id.left_button);
        Button btnAgree = (Button) view.findViewById(R.id.right_button);


        tvmsg.setText(mag);
        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                dialog.dismiss();
            }
        });

        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }


    /**
     * 휴무간격 View Image 설정
     */
    private RadioGroup.OnCheckedChangeListener radioPortableCheckedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rgEveryWeek.isChecked()) {
                closedRegularly = "1";
                rgEveryWeek.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbBiweeklyA.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbBiweeklyB.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgEveryWeek.setBackgroundResource(R.drawable.a_button_type_2);
                rbBiweeklyA.setBackgroundResource(R.drawable.a_button_type_3);
                rbBiweeklyB.setBackgroundResource(R.drawable.a_button_type_3);
            } else if (rbBiweeklyA.isChecked()) {
                closedRegularly = "2";
                rbBiweeklyA.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rgEveryWeek.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rbBiweeklyB.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgEveryWeek.setBackgroundResource(R.drawable.a_button_type_3);
                rbBiweeklyA.setBackgroundResource(R.drawable.a_button_type_2);
                rbBiweeklyB.setBackgroundResource(R.drawable.a_button_type_3);

            } else if (rbBiweeklyB.isChecked()) {
                closedRegularly = "3";
                rbBiweeklyB.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbBiweeklyA.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                rgEveryWeek.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                rgEveryWeek.setBackgroundResource(R.drawable.a_button_type_3);
                rbBiweeklyA.setBackgroundResource(R.drawable.a_button_type_3);
                rbBiweeklyB.setBackgroundResource(R.drawable.a_button_type_2);
            }
        }
    };

    /**
     * 정기 휴무일 설정 초기 Default view
     */
    private void setCloseDefaultView() {
        Logger.d(TAG, "ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingWeekType() : " + ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingWeekType());
        if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingWeekType() == 1) {
            closedRegularly = "1";
            rgEveryWeek.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            rbBiweeklyA.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            rbBiweeklyB.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

            rgEveryWeek.setBackgroundResource(R.drawable.a_button_type_2);
            rbBiweeklyA.setBackgroundResource(R.drawable.a_button_type_3);
            rbBiweeklyB.setBackgroundResource(R.drawable.a_button_type_3);
        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingWeekType() == 2) {
            closedRegularly = "2";
            rbBiweeklyA.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            rgEveryWeek.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            rbBiweeklyB.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

            rgEveryWeek.setBackgroundResource(R.drawable.a_button_type_3);
            rbBiweeklyA.setBackgroundResource(R.drawable.a_button_type_2);
            rbBiweeklyB.setBackgroundResource(R.drawable.a_button_type_3);

        } else if (ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingWeekType() == 3) {
            closedRegularly = "3";
            rbBiweeklyB.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
            rbBiweeklyA.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            rgEveryWeek.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

            rgEveryWeek.setBackgroundResource(R.drawable.a_button_type_3);
            rbBiweeklyA.setBackgroundResource(R.drawable.a_button_type_3);
            rbBiweeklyB.setBackgroundResource(R.drawable.a_button_type_2);
        }

    }

    /**
     * 정기휴무일
     */
    private void setClosedDaysView() {
        //정기 휴무일 설정 View - 토요일
        if (DataFormatUtil.daySettingType(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingWeek()).contains("토")) {
            llNoClosedSaturday.setVisibility(View.GONE);
            llClosedSaturday.setVisibility(View.VISIBLE);

            llGoneBusinessHoursSaturday.setVisibility(View.VISIBLE);
            llBusinessHoursSaturday.setVisibility(View.GONE);
        } else {
            llNoClosedSaturday.setVisibility(View.VISIBLE);
            llClosedSaturday.setVisibility(View.GONE);

            llBusinessHoursSaturday.setVisibility(View.VISIBLE);
            llGoneBusinessHoursSaturday.setVisibility(View.GONE);
        }

        //정기 휴무일 설정 View - 일요일
        if (DataFormatUtil.daySettingType(ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList().get(0).getData().getClosingWeek()).contains("일")) {
            llNoClosedSunday.setVisibility(View.GONE);
            llClosedSunday.setVisibility(View.VISIBLE);

            llGoneBusinessHoursSunday.setVisibility(View.VISIBLE);
            llBusinessHoursSunday.setVisibility(View.GONE);
        } else {
            llNoClosedSunday.setVisibility(View.VISIBLE);
            llClosedSunday.setVisibility(View.GONE);

            llBusinessHoursSunday.setVisibility(View.VISIBLE);
            llGoneBusinessHoursSunday.setVisibility(View.GONE);
        }
    }

    /**
     * 영업 설정 Dialog View
     */
    private void setBusinessHoursDialog() {
        HoursDialog = new Dialog(getActivity());
        HoursDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_business_hours, null);
        HoursDialog.setContentView(view);
        HoursDialog.setCancelable(false);

        llGoneBusinessHoursSaturday = (LinearLayout) view.findViewById(R.id.ll_gone_business_hours_saturday);
        llBusinessHoursSaturday = (LinearLayout) view.findViewById(R.id.ll_business_hours_saturday);
        llGoneBusinessHoursSunday = (LinearLayout) view.findViewById(R.id.ll_gone_business_hours_sunday);
        llBusinessHoursSunday = (LinearLayout) view.findViewById(R.id.ll_business_hours_sunday);

        RecyclerView rvWeekdayStart = (RecyclerView) view.findViewById(R.id.weekday_start_recycler_view);
        RecyclerView rvWeekdayEnd = (RecyclerView) view.findViewById(R.id.weekday_end_recycler_view);

        RecyclerView rvSaturdayStart = (RecyclerView) view.findViewById(R.id.saturday_start_recycler_view);
        RecyclerView rvSaturdayEnd = (RecyclerView) view.findViewById(R.id.saturday_end_recycler_view);

        RecyclerView rvSundayStart = (RecyclerView) view.findViewById(R.id.sunday_start_recycler_view);
        RecyclerView rvSundayEnd = (RecyclerView) view.findViewById(R.id.sunday_end_recycler_view);

        setClosedDaysView();

        String[] startTime = getResources().getStringArray(R.array.start_time);
        String[] endTime = getResources().getStringArray(R.array.end_time);
        ArrayList<String> startTimeList = new ArrayList<>(Arrays.asList(startTime));
        ArrayList<String> endTimeList = new ArrayList<>(Arrays.asList(endTime));

        AdapterSaturdayStart adpaterSaturdayStart = new AdapterSaturdayStart(getActivity(), mOnAdapterListner);
        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(7, StaggeredGridLayoutManager.VERTICAL);
        rvWeekdayStart.setHasFixedSize(true);
        rvWeekdayStart.setLayoutManager(layoutManager);
        adpaterSaturdayStart.addItem(startTimeList);
        rvWeekdayStart.setAdapter(adpaterSaturdayStart);

        AdapterSaturdayEnd adapterSaturdayEnd = new AdapterSaturdayEnd(getActivity(), mOnAdapterListner);
        RecyclerView.LayoutManager layoutManager_a = new StaggeredGridLayoutManager(7, StaggeredGridLayoutManager.VERTICAL);
        rvWeekdayEnd.setHasFixedSize(true);
        rvWeekdayEnd.setLayoutManager(layoutManager_a);
        adapterSaturdayEnd.addItem(endTimeList);
        rvWeekdayEnd.setAdapter(adapterSaturdayEnd);

        AdapterSundayStart adapterSundayStart = new AdapterSundayStart(getActivity(), mOnAdapterListner);
        RecyclerView.LayoutManager layoutManager_b = new StaggeredGridLayoutManager(7, StaggeredGridLayoutManager.VERTICAL);
        rvSaturdayStart.setHasFixedSize(true);
        rvSaturdayStart.setLayoutManager(layoutManager_b);
        adapterSundayStart.addItem(startTimeList);
        rvSaturdayStart.setAdapter(adapterSundayStart);

        AdapterSundayEnd adapterSundayEnd = new AdapterSundayEnd(getActivity(), mOnAdapterListner);
        RecyclerView.LayoutManager layoutManager_c = new StaggeredGridLayoutManager(7, StaggeredGridLayoutManager.VERTICAL);
        rvSaturdayEnd.setHasFixedSize(true);
        rvSaturdayEnd.setLayoutManager(layoutManager_c);
        adapterSundayEnd.addItem(endTimeList);
        rvSaturdayEnd.setAdapter(adapterSundayEnd);

        AdapterWeekdayStart adapterWeekdayStart = new AdapterWeekdayStart(getActivity(), mOnAdapterListner);
        RecyclerView.LayoutManager layoutManager_d = new StaggeredGridLayoutManager(7, StaggeredGridLayoutManager.VERTICAL);
        rvSundayStart.setHasFixedSize(true);
        rvSundayStart.setLayoutManager(layoutManager_d);
        adapterWeekdayStart.addItem(startTimeList);
        rvSundayStart.setAdapter(adapterWeekdayStart);

        AdapterWeekdayEnd adapterWeekdayEnd = new AdapterWeekdayEnd(getActivity(), mOnAdapterListner);
        RecyclerView.LayoutManager layoutManager_e = new StaggeredGridLayoutManager(7, StaggeredGridLayoutManager.VERTICAL);
        rvSundayEnd.setHasFixedSize(true);
        rvSundayEnd.setLayoutManager(layoutManager_e);
        adapterWeekdayEnd.addItem(endTimeList);
        rvSundayEnd.setAdapter(adapterWeekdayEnd);

        LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);
        Button btnSave = (Button) view.findViewById(R.id.btn_save);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mWait.show();
                new Thread(new Runnable() {
                    public void run() {

                        EnviromentManager.getUpdateServiceTime((AppCompatActivity)getActivity(), SettingLogic.getBusinessHoursWeekday(getActivity()), SettingLogic.getBusinessHoursWeekdayAfternoon(getActivity()), SettingLogic.getBusinessHoursSaturdayMorning(getActivity()),
                                SettingLogic.getBusinessHoursSaturdayAfternoon(getActivity()), SettingLogic.getBusinessHoursSundayMorning(getActivity()), SettingLogic.getBusinessHoursSundayAfternoon(getActivity()));
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                init();
                                mWait.dismiss();
                                HoursDialog.dismiss();
                                init();
                            }
                        });
                    }
                }).start();
            }
        });

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regularlyClosed("변경하신 내용은 삭제됩니다.\n" + "팝업을 종료할까요?", HoursDialog);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });
        HoursDialog.show();
    }

    public interface onCompletItem {
        void setSelectItem(String closed);

        void setRemoveClosingDay(String closingDay);
    }

    private FragmentOpeningHours.onCompletItem onCompletItem = new onCompletItem() {
        @Override
        public void setSelectItem(String closed) {
            closedDays = closed;
        }

        @Override
        public void setRemoveClosingDay(String closingDay) {
            AlertDialog.Builder closedDialog = new AlertDialog.Builder(getActivity());
            AlertDialog alertDialog = closedDialog.create();

            View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_confirm_popup, null);
            TextView tvmsg = (TextView) view.findViewById(R.id.tv_msg);
            Button btnCancle = (Button) view.findViewById(R.id.left_button);
            Button btnAgree = (Button) view.findViewById(R.id.right_button);

            tvmsg.setText("휴무일을 삭제할까요?");
            btnAgree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mWait.show();
                    new Thread(new Runnable() {
                        public void run() {
                            EnviromentManager.getRemoveClosingDay(mActivity, closingDay);
                            getActivity().runOnUiThread(new Runnable() {
                                public void run() {
                                    init();
                                    mWait.dismiss();
                                    alertDialog.dismiss();
                                }
                            });
                        }
                    }).start();
                }
            });

            btnCancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
            alertDialog.setView(view);
            alertDialog.show();

        }
    };

    /**
     * 정기 휴무일 리스너
     */
    private AdapterWeek.CCViewHolder.OnAdapterListner mOnAdapterWeekListner = new AdapterWeek.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {

        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };


    private AdapterCloseData.CCViewHolder.OnAdapterListner onAdapterListner = new AdapterCloseData.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {

        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
}