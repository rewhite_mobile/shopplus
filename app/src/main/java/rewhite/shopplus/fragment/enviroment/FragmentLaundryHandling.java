package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetHandleItemList;
import rewhite.shopplus.client.manager.ManagerGetStoreOutSideList;
import rewhite.shopplus.client.model.GetHandleItemList;
import rewhite.shopplus.client.model.GetStoreOutSideList;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.enviroment.AdapterLaundryHandling;

/**
 * 영업설정 -> 세탁물 취급설정 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentLaundryHandling extends Fragment {
    private static final String TAG = "FragmentLaundryHandling";

    private ListView lvHandlingItem, lvHandling;
    private WaitCounter mWait;
    private LinearLayout llSubcontractor;
    private Button btnOutLaundryAdd;

    private List<GetStoreOutSideList> getStoreOutSideLists;
    private AdapterLaundryHandling adapterLaundryHandling;
    private AppCompatActivity mActivity;

    public FragmentLaundryHandling(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_laundry_handling, container, false);

        init(view);
        return view;
    }

    private void init(View view) {
        mWait = new WaitCounter(getActivity());
        setLayout(view);
    }

    private void setLayout(View view) {
        lvHandlingItem = (ListView) view.findViewById(R.id.lv_handling_item);
        lvHandling = (ListView) view.findViewById(R.id.lv_handling);

        llSubcontractor = (LinearLayout) view.findViewById(R.id.ll_subcontractor);
        btnOutLaundryAdd = (Button) view.findViewById(R.id.btn_out_laundry_add);

        btnOutLaundryAdd.setOnClickListener(btnOutLaundryAddListener);
        setLaundryAdapter();
    }

    private View.OnClickListener btnOutLaundryAddListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.btn_out_laundry_add:
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());
                    thirdAlertPopup.setTitle("알림");
                    thirdAlertPopup.setContent("죄송합니다. 서비스 준비중입니다.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                    break;
            }
        }
    };

    private void setLaundryAdapter() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                ManagerGetHandleItemList laundryGetHandleItemList = EnviromentManager.getManagerGetHandleItemList((AppCompatActivity)getActivity(), 1);
                ManagerGetHandleItemList managerGetHandleItemList = EnviromentManager.getManagerGetHandleItemList((AppCompatActivity)getActivity(), 2);

                ManagerGetStoreOutSideList managerGetStoreOutSideList = EnviromentManager.getManagerGetStoreOutSideList((AppCompatActivity)getActivity());

                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        List<GetHandleItemList> lundryHandleItemList = managerGetHandleItemList.getGetHandleItemList();
                        List<GetHandleItemList> GetHandleItemList = laundryGetHandleItemList.getGetHandleItemList();

                        getStoreOutSideLists = managerGetStoreOutSideList.getGetStoreOutSideList();

                        for (GetHandleItemList getHandleItemList : GetHandleItemList) {
                            lundryHandleItemList.add(getHandleItemList);
                        }

                        adapterLaundryHandling = new AdapterLaundryHandling(getContext(), onCheckedClick);
                        adapterLaundryHandling.addItem(lundryHandleItemList);
                        lvHandlingItem.setAdapter(adapterLaundryHandling);

                        adapterLaundryHandling.notifyDataSetChanged();
                        setGetStoreOutSideLists();
                    }
                });
            }
        }).start();
    }

    public interface onCheckedClick {
        void onChecked(int position);
    }

    private onCheckedClick onCheckedClick = new onCheckedClick() {
        @Override
        public void onChecked(int position) {
            //새로고침
            setLaundryAdapter();
        }
    };

    private void setGetStoreOutSideLists() {
        if (getStoreOutSideLists.size() == 0) {
            llSubcontractor.setVisibility(View.VISIBLE);
            lvHandling.setVisibility(View.GONE);
        } else {
            lvHandling.setVisibility(View.VISIBLE);
            llSubcontractor.setVisibility(View.GONE);
        }
    }
}