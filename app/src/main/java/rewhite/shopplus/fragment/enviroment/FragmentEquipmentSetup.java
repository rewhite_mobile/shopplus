package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.util.CustomViewPager;
import rewhite.shopplus.view.adapter.enviroment.AdapterHardwareSetting;

/**
 * 장비설정 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentEquipmentSetup extends Fragment {
    private static final String TAG = "FragmentEquipmentSetup";
    private CustomViewPager viewPager;

    private TextView tvPrintSetting, tvHardwareSetting, tvPaymentTerminals;
    private LinearLayout llBg;

    private AppCompatActivity mActivity;

    public FragmentEquipmentSetup(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_equipment_setup, container, false);

        setLayout(view);
        setViewPagerAdapter();
        return view;
    }

    private void setLayout(View view) {
        viewPager = (CustomViewPager) view.findViewById(R.id.equipment_custom_viewpager);

        llBg = (LinearLayout) view.findViewById(R.id.ll_bg);
        tvPrintSetting = (TextView) view.findViewById(R.id.tv_print_setting);
        tvHardwareSetting = (TextView) view.findViewById(R.id.tv_hardware_setting);
        tvPaymentTerminals = (TextView) view.findViewById(R.id.tv_payment_terminals);
    }

    private void setViewPagerAdapter() {
        tvPrintSetting.setOnClickListener(movePageListener);
        tvPrintSetting.setTag(0);
        tvHardwareSetting.setOnClickListener(movePageListener);
        tvHardwareSetting.setTag(1);
        tvPaymentTerminals.setOnClickListener(movePageListener);
        tvPaymentTerminals.setTag(2);

        tvPrintSetting.setSelected(true);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        viewPager.setAdapter(new AdapterHardwareSetting(getActivity().getSupportFragmentManager(), mActivity));
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(2);
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            int selectPosition = 0;

            while (selectPosition < 3) {
                if (position == selectPosition) {
                    llBg.findViewWithTag(selectPosition).setSelected(true);
                } else {
                    llBg.findViewWithTag(selectPosition).setSelected(false);
                }
                selectPosition++;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();

            int selectPosition = 0;
            while (selectPosition < 3) {
                if (tag == selectPosition) {
                    llBg.findViewWithTag(selectPosition).setSelected(true);
                } else {
                    llBg.findViewWithTag(selectPosition).setSelected(false);
                }
                selectPosition++;
            }
            viewPager.setCurrentItem(tag);
        }
    };
}