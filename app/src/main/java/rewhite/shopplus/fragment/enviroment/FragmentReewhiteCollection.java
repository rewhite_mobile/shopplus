package rewhite.shopplus.fragment.enviroment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rewhite.shopplus.R;

/**
 * 영업설정 -> 리화이트 수거배송 설정 VIEW
 */
public class FragmentReewhiteCollection extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reewhite_collection, container, false);

        return view;
    }

}
