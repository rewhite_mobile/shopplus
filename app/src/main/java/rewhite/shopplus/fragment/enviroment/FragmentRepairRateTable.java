package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerClassList;
import rewhite.shopplus.client.manager.ManagerGetStoreItemList;
import rewhite.shopplus.client.manager.ManagerLaundryCategory;
import rewhite.shopplus.client.model.ClassListModel;
import rewhite.shopplus.client.model.GetStoreItemList;
import rewhite.shopplus.client.model.LaundryCategoryItemList;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.common.CustomKeyboard1;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.enviroment.AdapterCategoryDetail;
import rewhite.shopplus.view.adapter.enviroment.AdapterDetailListData;
import rewhite.shopplus.view.adapter.enviroment.AdapterLaundryRateTable;
import rewhite.shopplus.view.adapter.enviroment.AdapterLundryItemAdd;

/**
 * 요금표설정 -> 수선요금표 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentRepairRateTable extends Fragment {
    private static final String TAG = "FragmentRepairRateTable";
    private RecyclerView laundryRecyclerView, categoreDetailRecyclerView;
    private RecyclerView.LayoutManager laundryLayoutManager, detailLayoutManager, laundryDetailLayoutManager;
    private boolean itemAddStatus = true;

    private ListView detailRecyclerView;
    private Dialog dialog;

    private int categoryID, classID;
    private Button btnItemAdd, btnSave;
    private long mStoreItemID;
    private TextView tvItemNameTitle, tvExistingCharge, tvItemName;
    private EditText etAmount;

    private CustomKeyboard1 mCustomKeyboard;
    private List<LaundryCategoryItemList> storeItemList;
    private WaitCounter mWait;

    private AppCompatActivity mActivity;

    public FragmentRepairRateTable(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repair_rate_table, container, false);

        init(view);
        return view;
    }

    private void init(View view) {
        mWait = new WaitCounter(getActivity());

        setLayout(view);
        setAdapterListData();
        setAdapterDetailListData();
    }

    /**
     * layout setting view
     *
     * @param view
     */
    private void setLayout(View view) {
        tvItemName = (TextView) view.findViewById(R.id.tv_item_name);

        laundryRecyclerView = (RecyclerView) view.findViewById(R.id.laundry_recycler_view);
        detailRecyclerView = (ListView) view.findViewById(R.id.list_view);
        btnItemAdd = (Button) view.findViewById(R.id.btn_item_add);

        tvExistingCharge = (TextView) view.findViewById(R.id.tv_existing_charge);
        tvItemNameTitle = (TextView) view.findViewById(R.id.tv_item_name_title);
        etAmount = (EditText) view.findViewById(R.id.et_money_count);

        btnSave = (Button) view.findViewById(R.id.btn_save);
        btnItemAdd.setOnClickListener(btnOnClickListener);
        btnSave.setOnClickListener(btnOnClickListener);
        setCustomKeyBoard(view);
    }

    /**
     * CustomKeyBoard reset
     *
     * @param view
     */
    private void setCustomKeyBoard(View view) {
        mCustomKeyboard = new CustomKeyboard1((AppCompatActivity)getActivity(), view, R.id.input_keyboard_view, R.xml.custom_keyboard);
        mCustomKeyboard.registerEditText(view, R.id.et_money_count);
    }

    /**
     * 품목 정보 리스트
     */
    private void setAdapterListData() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                final ManagerLaundryCategory managerLaundryCategory = LaundryRegistrationManager.getManagerStoreUserList((AppCompatActivity)getActivity(), Long.valueOf(2));

                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        storeItemList = managerLaundryCategory.getLaundryCategoty();
                        try {
                            tvItemName.setText(storeItemList.get(0).getCategoryName());
                        } catch (Exception err) {
                            err.printStackTrace();
                        }

                        laundryRecyclerView.setHasFixedSize(true);

                        laundryLayoutManager = new StaggeredGridLayoutManager(11, StaggeredGridLayoutManager.VERTICAL);
                        laundryRecyclerView.setLayoutManager(laundryLayoutManager);

                        AdapterLaundryRateTable adapterLaundryRateTable = new AdapterLaundryRateTable(getActivity(), mOnAdapterListner);
                        adapterLaundryRateTable.addItem(storeItemList);
                        laundryRecyclerView.setAdapter(adapterLaundryRateTable);
                    }
                });
            }
        }).start();
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_item_add:
                    setRepairRateAddDialog(0);
                    break;
                case R.id.btn_save:
                    mWait.show();
                    new Thread(new Runnable() {
                        public void run() {
                            EnviromentManager.getRUpdateStoreItemPrice((AppCompatActivity)getActivity(), mStoreItemID, Integer.valueOf(etAmount.getText().toString()), 0, 0, 0, 0, 0);
                            mActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    mWait.dismiss();
                                    etAmount.setText("");
                                    setAdapterDetailListData();
                                }
                            });
                        }
                    }).start();
                    break;
            }
        }
    };

    /**
     * 수선요금표 Item ListView
     */
    private void setAdapterDetailListData() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                final ManagerGetStoreItemList managerGetStoreItemList = EnviromentManager.getManagerGetStoreItemList((AppCompatActivity)getActivity(), 2, categoryID, 0);

                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        List<GetStoreItemList> storeItemList = managerGetStoreItemList.getStoreItemList();
                        try {
                            if (storeItemList.size() == 0) {
                                tvItemNameTitle.setText(storeItemList.get(0).getItemName());
                            }
                        } catch (Exception err) {
                            err.printStackTrace();
                        }
                        detailLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);

                        AdapterDetailListData adapterLaundryRateTable = new AdapterDetailListData(getActivity(), existingCharge);
                        adapterLaundryRateTable.addItem(storeItemList);
                        detailRecyclerView.setAdapter(adapterLaundryRateTable);
                        adapterLaundryRateTable.notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }

    /**
     * 수선요금표 소분류 추가 Dialog View
     */
    private void setRepairRateAddDialog(int type) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_lundry_item_add, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);

        RecyclerView categoreRecyclerView = (RecyclerView) view.findViewById(R.id.categore_recycle_view);
        categoreDetailRecyclerView = (RecyclerView) view.findViewById(R.id.categore_detail_recycle_view);

        TextView tvTitleName = (TextView) view.findViewById(R.id.tv_title_name);
        LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);
        EditText etItemName = (EditText) view.findViewById(R.id.et_item_name);
        Button btnItemAdd = (Button) view.findViewById(R.id.btn_item_add);

        if (type == 0) {
            tvTitleName.setText("수선 품목 추가하기");
            btnItemAdd.setText("추가하기");
        } else {
            tvTitleName.setText("수선 품목 수정하기");
            btnItemAdd.setText("수정하기");
        }

        AdapterLundryItemAdd adapterLundryItemAdd = new AdapterLundryItemAdd(getActivity(), laundryItemListner, type);
        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);

        categoreRecyclerView.setHasFixedSize(true);
        categoreRecyclerView.setLayoutManager(layoutManager);
        adapterLundryItemAdd.addItem(storeItemList);
        categoreRecyclerView.setAdapter(adapterLundryItemAdd);

        setMiddleClassAdapter(2);

        btnItemAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etItemName.getText().toString().length() == 0) {
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());

                    thirdAlertPopup.setTitle("알림");
                    thirdAlertPopup.setContent("품목명을 적어주세요.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                } else {
                    List<GetStoreItemList> getStoreItemLists = ManagerGetStoreItemList.getmInstance().getStoreItemList();
                    for (GetStoreItemList getStoreItemList : getStoreItemLists) {
                        if (getStoreItemList.getItemName().equals(etItemName.getText().toString())) {
                            itemAddStatus = false;
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());

                            thirdAlertPopup.setTitle("알림");
                            thirdAlertPopup.setContent("이미 존재하는 이름이에요. 다시 입력해주세요.");
                            thirdAlertPopup.setButtonText("확인");
                            thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    itemAddStatus = true;
                                }
                            });
                            thirdAlertPopup.show();
                        }
                    }
                    if (itemAddStatus == true) {
                        setItemAdd(etItemName.getText().toString(), type);
                        etItemName.setText("");
                    }
                }
            }
        });

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(getActivity());
                thirdConfirmPopup.setTitle("팝업종료");
                thirdConfirmPopup.setContent("수선품목 추가 \n 팝업을 종료할까요?");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        dialog.dismiss();
                    }
                });
                thirdConfirmPopup.show();
            }
        });
        dialog.show();
    }

    /**
     * 중분류 상세 리스트
     */
    private void setMiddleClassAdapter(int categoryId) {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                ManagerClassList managerClassList = LaundryRegistrationManager.getManagerClassList((AppCompatActivity)getActivity(), categoryId);
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        try {
                            List<ClassListModel> storeItemLists = managerClassList.getClassListInfo();


                            classID = storeItemLists.get(0).getClassID();

                            categoreDetailRecyclerView.setHasFixedSize(true);
                            laundryDetailLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
                            categoreDetailRecyclerView.setLayoutManager(laundryDetailLayoutManager);

                            AdapterCategoryDetail adapterCategoryDetail = new AdapterCategoryDetail(getActivity(), categotyListner, 0);
                            adapterCategoryDetail.addItem(storeItemLists);
                            categoreDetailRecyclerView.setAdapter(adapterCategoryDetail);
                            adapterCategoryDetail.notifyDataSetChanged();
                        } catch (IndexOutOfBoundsException err) {
                            err.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    /**
     * 정상적으로 품목이 추가 된 결과 체크 로직
     * (데이터 화면 갱신 하기 위한 로직 추가.)
     *
     * @param itemName
     */
    private void setItemAdd(String itemName, int type) {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                if (type == 0) {
                    EnviromentManager.getAddStoreItem((AppCompatActivity)getActivity(), classID, itemName);   //수선요금 추가
                } else {
                    EnviromentManager.getUpdateStoreItem((AppCompatActivity)getActivity(), mStoreItemID, itemName); //수선요금 Name 수정
                }
                EnviromentManager.getManagerStoreManageInfo((AppCompatActivity)getActivity());

                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());
                        if (type == 0) {
                            thirdAlertPopup.setTitle("품목추가");
                            thirdAlertPopup.setContent(itemName + " 추가 완료 되었습니다.");
                            thirdAlertPopup.setButtonText("확인");
                        } else {
                            thirdAlertPopup.setTitle("수정하기");
                            thirdAlertPopup.setContent(itemName + " 수정 완료 되었습니다.");
                            thirdAlertPopup.setButtonText("확인");

                        }
                        thirdAlertPopup.show();

                        //Fragment View 갱신
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.detach(FragmentRepairRateTable.this).attach(FragmentRepairRateTable.this).commit();
                    }
                });
            }
        }).start();
    }

    public interface existingCharge {
        void setExistionCharge(int amount, String itemName, long storeItemID);   //데이터 입력값 변경

        void setNotToUse(); //품목사용 여부 변경 reflesh

        void setItemName(int storeItemID);
    }

    private FragmentAccessories.existingCharge existingCharge = new FragmentAccessories.existingCharge() {
        @Override
        public void setExistionCharge(int amount, String itemName, long storeItemID) {
            tvItemNameTitle.setText(itemName);
            tvExistingCharge.setText(DataFormatUtil.moneyFormatToWon(amount));
            mStoreItemID = storeItemID;
        }

        @Override
        public void setNotToUse() {
            setAdapterDetailListData();
        }

        @Override
        public void setItemName(int storeItemID) {
            mStoreItemID = storeItemID;
            setRepairRateAddDialog(1);
        }
    };

    private AdapterLaundryRateTable.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterLaundryRateTable.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
            tvItemName.setText(storeItemList.get(position).getCategoryName());
            categoryID = storeItemList.get(position).getCategoryId();

            setAdapterDetailListData();
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

    /**
     * 수선 품목 추가 대분류
     */
    private AdapterLundryItemAdd.CCViewHolder.OnAdapterListner laundryItemListner = new AdapterLundryItemAdd.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int categoryId) {
            setMiddleClassAdapter(categoryId);
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

    /**
     * 수선 품목 추가 중분류
     */
    private AdapterCategoryDetail.CCViewHolder.OnAdapterListner categotyListner = new AdapterCategoryDetail.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int classId) {
            classID = classId;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
}