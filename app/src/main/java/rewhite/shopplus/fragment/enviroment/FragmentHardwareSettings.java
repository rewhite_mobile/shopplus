package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import rewhite.shopplus.R;

/**
 * 장비설정-> 하드웨어설정 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentHardwareSettings extends Fragment {
    private static final String TAG = "FragmentHardwareSettings";

    private RadioGroup rgUsingBarcodes, rgSelectPrinter, rgUsingSelect;
    private RadioButton rbUsingBarcodesOn, rbUsingBarcodesOff, rbSelectPrinterOn, rbSelectPrinterOff, rbUsingSelectOn, rbUsingSelectOff;

    private AppCompatActivity mActivity;

    public FragmentHardwareSettings(AppCompatActivity activity){
        this.mActivity = activity;
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hardware_settings, container, false);

        setLayout(view);
        setRadioGropClick();
        return view;
    }

    private void setLayout(View view) {
        rgUsingBarcodes = (RadioGroup) view.findViewById(R.id.rg_using_barcodes);
        rgSelectPrinter = (RadioGroup) view.findViewById(R.id.rg_select_printer);
        rgUsingSelect = (RadioGroup) view.findViewById(R.id.rg_using_select);

        rbUsingBarcodesOn = (RadioButton) view.findViewById(R.id.rb_using_barcodes_on);
        rbUsingBarcodesOff = (RadioButton) view.findViewById(R.id.rb_using_barcodes_off);

        rbSelectPrinterOn = (RadioButton) view.findViewById(R.id.rb_select_printer_on);
        rbSelectPrinterOff = (RadioButton) view.findViewById(R.id.rb_select_printer_off);

        rbUsingSelectOn = (RadioButton) view.findViewById(R.id.rb_using_select_on);
        rbUsingSelectOff = (RadioButton) view.findViewById(R.id.rb_using_select_off);
    }

    private void setRadioGropClick() {
        rgUsingBarcodes.setOnCheckedChangeListener(onClickUsingBarcodesListener);
        rgSelectPrinter.setOnCheckedChangeListener(onClickSelectPrinterListener);
        rgUsingSelect.setOnCheckedChangeListener(onClickUsingSelectListener);

    }

    private RadioGroup.OnCheckedChangeListener onClickUsingBarcodesListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbUsingBarcodesOn.isChecked()) {
                rbUsingBarcodesOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbUsingBarcodesOff.setBackgroundResource(R.drawable.a_button_type_3);

                rbUsingBarcodesOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbUsingBarcodesOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

            } else if (rbUsingBarcodesOff.isChecked()) {
                rbUsingBarcodesOff.setBackgroundResource(R.drawable.a_button_type_2);
                rbUsingBarcodesOn.setBackgroundResource(R.drawable.a_button_type_3);

                rbUsingBarcodesOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbUsingBarcodesOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener onClickSelectPrinterListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbSelectPrinterOn.isChecked()) {
                rbSelectPrinterOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbSelectPrinterOff.setBackgroundResource(R.drawable.a_button_type_3);

                rbSelectPrinterOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbSelectPrinterOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            } else if (rbSelectPrinterOff.isChecked()) {
                rbSelectPrinterOff.setBackgroundResource(R.drawable.a_button_type_2);
                rbSelectPrinterOn.setBackgroundResource(R.drawable.a_button_type_3);

                rbSelectPrinterOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbSelectPrinterOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener onClickUsingSelectListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbUsingSelectOn.isChecked()) {
                rbUsingSelectOn.setBackgroundResource(R.drawable.a_button_type_2);
                rbUsingSelectOff.setBackgroundResource(R.drawable.a_button_type_3);

                rbUsingSelectOn.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbUsingSelectOff.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

            } else if (rbUsingSelectOff.isChecked()) {
                rbUsingSelectOff.setBackgroundResource(R.drawable.a_button_type_2);
                rbUsingSelectOn.setBackgroundResource(R.drawable.a_button_type_3);

                rbUsingSelectOff.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                rbUsingSelectOn.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
            }
        }
    };

}