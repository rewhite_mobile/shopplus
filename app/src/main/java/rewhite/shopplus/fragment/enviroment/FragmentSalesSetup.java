package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;

/**
 * 영업설정 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentSalesSetup extends Fragment {
    private static final String TAG = "FragmentSalesSetup";
    private TextView tvBasicSetting, tvOpeningHours, tvEmployeeManagement, tvContractInformation;
    private LinearLayout llBg;

    private AppCompatActivity mActivity;

    public FragmentSalesSetup(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sales_setup, container, false);

        init(view);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FragmentBasicSetting fragmentBasicSetting = new FragmentBasicSetting((AppCompatActivity)getActivity()); //기본설정
        fragmentTransaction.replace(R.id.fragment_setup_custom, fragmentBasicSetting);
        fragmentTransaction.commit();
    }

    private void init(View view) {
        setLayout(view);
        setViewPagerAdapter();
    }

    private void setLayout(View view) {
        llBg = (LinearLayout) view.findViewById(R.id.ll_bg);
        tvBasicSetting = (TextView) view.findViewById(R.id.tv_basic_setting);
        tvOpeningHours = (TextView) view.findViewById(R.id.tv_opening_hours);

        tvEmployeeManagement = (TextView) view.findViewById(R.id.tv_employee_management);
        tvContractInformation = (TextView) view.findViewById(R.id.tv_contract_information);
    }

    private void setViewPagerAdapter() {
        tvBasicSetting.setOnClickListener(movePageListener);
        tvBasicSetting.setTag(0);
        tvOpeningHours.setOnClickListener(movePageListener);
        tvOpeningHours.setTag(1);
        tvEmployeeManagement.setOnClickListener(movePageListener);
        tvEmployeeManagement.setTag(2);
        tvContractInformation.setOnClickListener(movePageListener);
        tvContractInformation.setTag(3);

        tvBasicSetting.setSelected(true);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            switch (tag) {
                case 0:
                    FragmentBasicSetting fragmentBasicSetting = new FragmentBasicSetting((AppCompatActivity)getActivity()); //기본설정
                    fragmentTransaction.replace(R.id.fragment_setup_custom, fragmentBasicSetting);
                    fragmentTransaction.commit();
                    break;

                case 1:
                    FragmentOpeningHours fragmentOpeningHours = new FragmentOpeningHours((AppCompatActivity)getActivity()); //영업시간 및 휴무설정
                    fragmentTransaction.replace(R.id.fragment_setup_custom, fragmentOpeningHours);
                    fragmentTransaction.commit();
                    break;

                case 2:
                    FragmentEmployeeManagement fragmentEmployeeManagement = new FragmentEmployeeManagement((AppCompatActivity)getActivity()); //직원관리
                    fragmentTransaction.replace(R.id.fragment_setup_custom, fragmentEmployeeManagement);
                    fragmentTransaction.commit();
                    break;

                case 3:
                    FragmentContractInformation fragmentContractInformation = new FragmentContractInformation((AppCompatActivity)getActivity());  //계약정보
                    fragmentTransaction.replace(R.id.fragment_setup_custom, fragmentContractInformation);
                    fragmentTransaction.commit();
                    break;
            }

            int selectPosition = 0;
            while (selectPosition < 4) {
                if (tag == selectPosition) {
                    llBg.findViewWithTag(selectPosition).setSelected(true);
                } else {
                    llBg.findViewWithTag(selectPosition).setSelected(false);
                }
                selectPosition++;
            }
        }
    };
}