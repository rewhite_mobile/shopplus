package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;

/**
 * 요금표 설정 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentSetRatetable extends Fragment {
    private static final String TAG = "FragmentSetRatetable";

    private Fragment reFragment;

    private TextView tvLaundryRateTable, tvRepairRateTable, tvAdditionalTechnicalTariffs, tvAccessories, tvLaundryHandling;
    private LinearLayout llBg;
    private AppCompatActivity mActivity;

    public FragmentSetRatetable(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_set_ratetable, container, false);

        setLayout(view);
        setViewPagerAdapter();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FragmentLaundryHandling fragmentLaundryHandling = new FragmentLaundryHandling((AppCompatActivity)getActivity());
        fragmentTransaction.replace(R.id.fragment_ratetable, fragmentLaundryHandling);
        fragmentTransaction.commit();
    }

    private void setLayout(View view) {
        llBg = (LinearLayout) view.findViewById(R.id.ll_bg);
        tvLaundryHandling = (TextView) view.findViewById(R.id.tv_laundry_handling);
        tvLaundryRateTable = (TextView) view.findViewById(R.id.tv_laundry_rate_table);
        tvRepairRateTable = (TextView) view.findViewById(R.id.tv_repair_rate_table);
        tvAdditionalTechnicalTariffs = (TextView) view.findViewById(R.id.tv_additional_technical_tariffs);
        tvAccessories = (TextView) view.findViewById(R.id.tv_accessories);
    }

    private void setViewPagerAdapter() {
        tvLaundryHandling.setOnClickListener(movePageListener);
        tvLaundryHandling.setTag(0);
        tvLaundryRateTable.setOnClickListener(movePageListener);
        tvLaundryRateTable.setTag(1);
        tvRepairRateTable.setOnClickListener(movePageListener);
        tvRepairRateTable.setTag(2);
        tvAdditionalTechnicalTariffs.setOnClickListener(movePageListener);
        tvAdditionalTechnicalTariffs.setTag(3);
        tvAccessories.setOnClickListener(movePageListener);
        tvAccessories.setTag(4);

        tvLaundryHandling.setSelected(true);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            switch (tag) {
                case 0:
                    FragmentLaundryHandling fragmentLaundryHandling = new FragmentLaundryHandling((AppCompatActivity)getActivity());   //세탁물 취급설정
                    fragmentTransaction.replace(R.id.fragment_ratetable, fragmentLaundryHandling);
                    fragmentTransaction.commit();
                    break;

                case 1:
                    FragmentLaundryRateTable fragmentLaundryRateTable = new FragmentLaundryRateTable((AppCompatActivity)getActivity()); //세탁 요금표
                    fragmentTransaction.replace(R.id.fragment_ratetable, fragmentLaundryRateTable);
                    fragmentTransaction.commit();
                    break;

                case 2:
                    FragmentRepairRateTable fragmentRepairRateTable = new FragmentRepairRateTable((AppCompatActivity)getActivity()); //수선 요금표
                    fragmentTransaction.replace(R.id.fragment_ratetable, fragmentRepairRateTable);
                    fragmentTransaction.commit();
                    break;

                case 3:
                    FragmentAdditionalTechnicalTariffs fragmentAdditionalTechnicalTariffs = new FragmentAdditionalTechnicalTariffs((AppCompatActivity)getActivity());  //추가기술 요금표
                    fragmentTransaction.replace(R.id.fragment_ratetable, fragmentAdditionalTechnicalTariffs);
                    fragmentTransaction.commit();
                    break;

                case 4:
                    FragmentAccessories fragmentAccessories = new FragmentAccessories((AppCompatActivity)getActivity()); // 부속품
                    fragmentTransaction.replace(R.id.fragment_ratetable, fragmentAccessories);
                    fragmentTransaction.commit();
                    break;

            }

            int selectPosition = 0;
            while (selectPosition < 5) {
                if (tag == selectPosition) {
                    llBg.findViewWithTag(selectPosition).setSelected(true);
                } else {
                    llBg.findViewWithTag(selectPosition).setSelected(false);
                }
                selectPosition++;
            }
        }
    };
}