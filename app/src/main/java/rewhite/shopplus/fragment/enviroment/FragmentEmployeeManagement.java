package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rewhite.shopplus.R;

/**
 * 영업설정 -> 직원관리 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentEmployeeManagement extends Fragment {
    private static final String TAG = "FragmentEmployeeManagement";

    private AppCompatActivity mActivity;
    public FragmentEmployeeManagement(AppCompatActivity activity){
        mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_employee, container, false);

        return view;
    }

}
