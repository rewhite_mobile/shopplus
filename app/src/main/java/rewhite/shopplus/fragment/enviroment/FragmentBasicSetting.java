package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetStoreManageInfo;
import rewhite.shopplus.client.model.GetStoreManageInfo;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.common.popup.keyboard.DialogCustomKeyboardTypeC;
import rewhite.shopplus.common.popup.keyboard.DialogCustomKeyboardTypeD;
import rewhite.shopplus.logic.BusProvider;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.WaitCounter;

/**
 * 영업설정 -> 기본설정 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentBasicSetting extends Fragment {
    private static final String TAG = "FragmentBasicSetting";
    private View view;

    private Button btnPreilledGoldSetting, btnMileageSetting, btnGreetingPresidentedit;

    private TextView tvGreetingPresident;
    private WaitCounter mWait;

    private boolean isRbTerminalNo, isRbTerminalYes, isRbPortableNo, isRbPortableYes;
    private LinearLayout llJapanesePaperBag, llJapanesePaperBagTeg, llJapanesePaperBagEtc;
    private ImageView ivJapanesePaperBag, ivJapanesePaperBagTeg, ivJapanesePaperEtc;

    private RadioGroup rgCardTerminal, rgPortableCardTerminal;
    private RadioButton rbTerminalYes, rbTerminalNo, rbPortableYes, rbPortableNo;
    private TextView tvEarning, tvCardRatio, tvCashReserveRatio, tvCashReceipts;
    private TextView tvMileageEarning, tvMileageRatio, tvMileageCashRatio, tvMileageCashReceipts;
    private String availPaymentType = "00";
    private List<GetStoreManageInfo> getStoreManageInfos = new ArrayList<>();
    private AppCompatActivity mActivity;

    private int tagType = 1;

    public FragmentBasicSetting(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusProvider.getInstance().register(this);   //Activity에서 Click 이벤트 감지 setting
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_basic_setting, container, false);
        mWait = new WaitCounter(mActivity);
        init();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void init() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                EnviromentManager.getManagerStoreManageInfo((AppCompatActivity)getActivity());
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        getStoreManageInfos = ManagerGetStoreManageInfo.getmInstance().getStoreManageInfoList();
                        setLayout();
                    }
                });
            }
        }).start();
    }

    /**
     * Layout Setting View
     */
    private void setLayout() {
        llJapanesePaperBag = (LinearLayout) view.findViewById(R.id.ll_japanese_paper_bag);
        llJapanesePaperBagTeg = (LinearLayout) view.findViewById(R.id.ll_japanese_paper_bag_teg);
        llJapanesePaperBagEtc = (LinearLayout) view.findViewById(R.id.ll_japanese_paper_bag_etc);

        ivJapanesePaperBag = (ImageView) view.findViewById(R.id.iv_japanese_paper_bag);
        ivJapanesePaperBagTeg = (ImageView) view.findViewById(R.id.iv_japanese_paper_bag_teg);
        ivJapanesePaperEtc = (ImageView) view.findViewById(R.id.iv_japanese_paper_etc);

        btnMileageSetting = (Button) view.findViewById(R.id.btn_mileage_setting);
        btnPreilledGoldSetting = (Button) view.findViewById(R.id.btn_preilled_gold_setting);
        btnGreetingPresidentedit = (Button) view.findViewById(R.id.btn_greeting_presidentedit);

        rgCardTerminal = (RadioGroup) view.findViewById(R.id.rg_card_terminal);
        rgPortableCardTerminal = (RadioGroup) view.findViewById(R.id.rg_portable_card_terminal);

        rbPortableYes = (RadioButton) view.findViewById(R.id.rb_portable_yes);
        rbPortableNo = (RadioButton) view.findViewById(R.id.rb_portable_no);
        rbTerminalYes = (RadioButton) view.findViewById(R.id.rb_terminal_yes);
        rbTerminalNo = (RadioButton) view.findViewById(R.id.rb_terminal_no);

        tvEarning = (TextView) view.findViewById(R.id.tv_earning);
        tvCardRatio = (TextView) view.findViewById(R.id.tv_card_ratio);
        tvCashReserveRatio = (TextView) view.findViewById(R.id.tv_cash_reserve_ratio);
        tvCashReceipts = (TextView) view.findViewById(R.id.tv_cash_receipts);

        tvMileageEarning = (TextView) view.findViewById(R.id.tv_mileage_earning);
        tvMileageRatio = (TextView) view.findViewById(R.id.tv_mileage_ratio);
        tvMileageCashRatio = (TextView) view.findViewById(R.id.tv_mileage_cash_ratio);
        tvMileageCashReceipts = (TextView) view.findViewById(R.id.tv_mileage_cash_receipts);

        tvGreetingPresident = (TextView) view.findViewById(R.id.tv_greeting_president);
        tvGreetingPresident.setMovementMethod(new ScrollingMovementMethod());

        btnPreilledGoldSetting.setOnClickListener(btnOnclickListener);
        btnMileageSetting.setOnClickListener(btnOnclickListener);
        btnGreetingPresidentedit.setOnClickListener(btnOnclickListener);
        tvGreetingPresident.setOnClickListener(btnOnclickListener);

        llJapanesePaperBag.setOnClickListener(btnOnclickListener);
        llJapanesePaperBagTeg.setOnClickListener(btnOnclickListener);
        llJapanesePaperBagEtc.setOnClickListener(btnOnclickListener);
        rgCardTerminal.setOnCheckedChangeListener(radioPortableCheckedListener);
        rgPortableCardTerminal.setOnCheckedChangeListener(radioCardTerminalCheckedListener);

        setTextView();
        setTacNumberSetting();
    }

    /**
     * 초기 TextView 셋팅
     */
    private void setTextView() {
        DecimalFormat df = new DecimalFormat("#.##");

        tvGreetingPresident.setText(getStoreManageInfos.get(0).getData().getStoreGreetingText());

        tvCardRatio.setText(String.valueOf(df.format(getStoreManageInfos.get(0).getData().getCardBonusPrePaidRate())) + " % ");
        tvCashReserveRatio.setText(String.valueOf(df.format(getStoreManageInfos.get(0).getData().getCashBonusPrePaidRate())) + " % ");
        tvMileageRatio.setText(String.valueOf(df.format(getStoreManageInfos.get(0).getData().getCashMileageRate())) + " % ");
        tvMileageCashRatio.setText(String.valueOf(df.format(getStoreManageInfos.get(0).getData().getCardMileageRate())) + " % ");
        tvMileageCashReceipts.setText(DataFormatUtil.moneyFormatToWon(getStoreManageInfos.get(0).getData().getMileageUseMinPrice()) + "원");
        imageStatus(getStoreManageInfos.get(0).getData().getTagNoType());

        //마일리지 설정
        if (getStoreManageInfos.get(0).getData().getIsSaveMileage().equals("Y")) {
            tvMileageEarning.setText("적립");
        } else {
            tvMileageEarning.setText("적립안함");
        }

        //선충전금 설정
        if (getStoreManageInfos.get(0).getData().getIsPrePaidCashReceipt().equals("Y")) {
            tvCashReceipts.setText("발급");
        } else {
            tvCashReceipts.setText("발급안함");
        }

        //선충전금 보너스 지급 여부
        if (getStoreManageInfos.get(0).getData().getIsGiveBonusPrePaid().equals("Y")) {
            tvEarning.setText("적립");
        } else {
            tvEarning.setText("적립안함");
        }
    }

    /**
     * 지원 결제 방식 초기 셋팅
     */
    private void setTacNumberSetting() {
        if (getStoreManageInfos.get(0).getData().getAvailPaymentType().equals("11")) {
            rbTerminalYes.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
            rbTerminalYes.setBackgroundResource(R.drawable.a_button_type_2);

            rbPortableYes.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
            rbPortableYes.setBackgroundResource(R.drawable.a_button_type_2);
            isRbTerminalYes = true;
            isRbPortableYes = true;
        } else if (getStoreManageInfos.get(0).getData().getAvailPaymentType().equals("10")) {
            rbTerminalYes.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
            rbTerminalYes.setBackgroundResource(R.drawable.a_button_type_2);

            rbPortableNo.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
            rbPortableNo.setBackgroundResource(R.drawable.a_button_type_2);
            isRbTerminalYes = true;
            isRbPortableNo = true;
        } else if (getStoreManageInfos.get(0).getData().getAvailPaymentType().equals("01")) {
            rbTerminalNo.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
            rbTerminalNo.setBackgroundResource(R.drawable.a_button_type_2);

            rbPortableYes.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
            rbPortableYes.setBackgroundResource(R.drawable.a_button_type_2);
            isRbTerminalNo = true;
            isRbPortableYes = true;
        } else if (getStoreManageInfos.get(0).getData().getAvailPaymentType().equals("00")) {
            rbTerminalNo.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
            rbTerminalNo.setBackgroundResource(R.drawable.a_button_type_2);

            rbPortableNo.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
            rbPortableNo.setBackgroundResource(R.drawable.a_button_type_2);
            isRbTerminalNo = true;
            isRbPortableNo = true;
        }
    }

    private RadioGroup.OnCheckedChangeListener radioPortableCheckedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbTerminalYes.isChecked()) {
                rbTerminalYes.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
                rbTerminalNo.setTextColor(mActivity.getResources().getColor(R.color.color_43425c));

                rbTerminalYes.setBackgroundResource(R.drawable.a_button_type_2);
                rbTerminalNo.setBackgroundResource(R.drawable.a_button_type_3);
                isRbTerminalYes = true;
                isRbTerminalNo = false;

            } else if (rbTerminalNo.isChecked()) {
                rbTerminalNo.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
                rbTerminalYes.setTextColor(mActivity.getResources().getColor(R.color.color_43425c));

                rbTerminalNo.setBackgroundResource(R.drawable.a_button_type_2);
                rbTerminalYes.setBackgroundResource(R.drawable.a_button_type_3);
                isRbTerminalNo = true;
                isRbTerminalYes = false;
            }
            setIsCardStatus();
        }
    };

    private RadioGroup.OnCheckedChangeListener radioCardTerminalCheckedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (rbPortableYes.isChecked()) {
                rbPortableYes.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
                rbPortableNo.setTextColor(mActivity.getResources().getColor(R.color.color_43425c));

                rbPortableYes.setBackgroundResource(R.drawable.a_button_type_2);
                rbPortableNo.setBackgroundResource(R.drawable.a_button_type_3);
                isRbPortableYes = true;
                isRbPortableNo = false;
            } else if (rbPortableNo.isChecked()) {
                rbPortableNo.setTextColor(mActivity.getResources().getColor(R.color.Wite_color));
                rbPortableYes.setTextColor(mActivity.getResources().getColor(R.color.color_43425c));

                rbPortableNo.setBackgroundResource(R.drawable.a_button_type_2);
                rbPortableYes.setBackgroundResource(R.drawable.a_button_type_3);
                isRbPortableNo = true;
                isRbPortableYes = false;
            }
            setIsCardStatus();
        }
    };

    /**
     * 지원결제 방식 변경 Data
     */
    private void setIsCardStatus() {
        if (isRbTerminalYes == true && isRbPortableYes == true) {
            availPaymentType = "11";
        } else if (isRbTerminalYes == true && isRbPortableNo == true) {
            availPaymentType = "10";
        } else if (isRbPortableYes == true && isRbTerminalNo == true) {
            availPaymentType = "01";
        } else if (isRbTerminalNo == true && isRbPortableNo == true) {
            availPaymentType = "00";
        }
        //지원결제 방식 변경 Request 처리 지원방식이 변경 될때 마다 호출
        EnviromentManager.getUpdateAvailPaymentType((AppCompatActivity)getActivity(), availPaymentType);
    }

    private View.OnClickListener btnOnclickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            Intent intent = null;
            switch (id) {
                case R.id.btn_greeting_presidentedit:
                case R.id.tv_greeting_president:
                    setEditTextDialog();
                    break;
                case R.id.btn_preilled_gold_setting:    //선충전금 설정
                     intent = new Intent(mActivity, DialogCustomKeyboardTypeD.class);
                    getActivity().startActivity(intent);
                    break;

                case R.id.btn_mileage_setting:          //마일리지 설정
                    intent = new Intent(mActivity, DialogCustomKeyboardTypeC.class);
                    getActivity().startActivity(intent);
                    break;

                case R.id.ll_japanese_paper_bag:
                    imageStatus(1);
                    break;

                case R.id.ll_japanese_paper_bag_teg:
                    imageStatus(2);
                    break;

                case R.id.ll_japanese_paper_bag_etc:
                    imageStatus(3);
                    break;
            }
        }
    };

    /**
     * 택번호 선택 View 변경 데이터 request 로직 추가.
     *
     * @param tagType
     */
    private void imageStatus(int tagType) {
        if (tagType == 1) {
            ivJapanesePaperBag.setBackgroundResource(R.drawable.btn_settings_tagchoice_02);
            ivJapanesePaperBagTeg.setBackgroundResource(R.drawable.btn_settings_tagchoice_00);
            ivJapanesePaperEtc.setBackgroundResource(R.drawable.btn_settings_tagchoice_00);
        } else if (tagType == 2) {
            ivJapanesePaperBag.setBackgroundResource(R.drawable.btn_settings_tagchoice_00);
            ivJapanesePaperBagTeg.setBackgroundResource(R.drawable.btn_settings_tagchoice_02);
            ivJapanesePaperEtc.setBackgroundResource(R.drawable.btn_settings_tagchoice_00);
        } else if (tagType == 3) {
            ivJapanesePaperBag.setBackgroundResource(R.drawable.btn_settings_tagchoice_00);
            ivJapanesePaperBagTeg.setBackgroundResource(R.drawable.btn_settings_tagchoice_00);
            ivJapanesePaperEtc.setBackgroundResource(R.drawable.btn_settings_tagchoice_02);
        }
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                EnviromentManager.getUpdateTagNoType((AppCompatActivity)getActivity(), tagType);
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                    }
                });
            }
        }).start();
    }

    /**
     * 인사말 수정하기 위한 DialogView Setting
     */
    private void setEditTextDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        AlertDialog alertDialog = dialog.create();

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_edittext_view, null);

        LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);
        Button btnSave = (Button) view.findViewById(R.id.btn_save);

        EditText editText = (EditText) view.findViewById(R.id.et_dialog_greeting_president);

        editText.setHint(tvGreetingPresident.getText().toString());
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvGreetingPresident.setText(editText.getText().toString());

                mWait.show();
                new Thread(new Runnable() {
                    public void run() {
                        EnviromentManager.getUpdateStoreGreeting((AppCompatActivity)getActivity(), editText.getText().toString());
                        mActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                mWait.dismiss();

                                ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());
                                thirdAlertPopup.setTitle("안내 팝업");
                                thirdAlertPopup.setContent("저장이 완료되었어요.");
                                thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                    @Override
                                    public void onConfirmClick() {
                                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                        alertDialog.dismiss();
                                    }
                                });
                                thirdAlertPopup.show();
                            }
                        });
                    }
                }).start();
            }
        });

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(getActivity());
                thirdConfirmPopup.setTitle("팝업종료");
                thirdConfirmPopup.setContent("변경하신 내용은 삭제됩니다. \n 팝업을 종료할까요?");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        alertDialog.dismiss();
                    }
                });
                thirdConfirmPopup.show();
            }
        });
        alertDialog.setView(view);
        alertDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusProvider.getInstance().unregister(this); //Activity에서 Click 이벤트 해지 setting
    }
}