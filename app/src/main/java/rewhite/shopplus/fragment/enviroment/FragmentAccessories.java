package rewhite.shopplus.fragment.enviroment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerClassList;
import rewhite.shopplus.client.manager.ManagerGetStoreItemList;
import rewhite.shopplus.client.model.ClassListModel;
import rewhite.shopplus.client.model.GetStoreItemList;
import rewhite.shopplus.client.network.EnviromentManager;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.util.CustomKeyboard;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.enviroment.AdapterCategoryDetail;
import rewhite.shopplus.view.adapter.enviroment.AdapterDetailListData;

/**
 * 요금표설정 -> 부속품 VIEW
 */
@SuppressLint("ValidFragment")
public class FragmentAccessories extends Fragment {
    private static final String TAG = "FragmentAccessories";

    public AppCompatActivity mActivity;
    private Button btnItemAdd, btnSave;
    private Dialog dialog;

    private TextView tvItemNameTitle, tvExistingCharge;
    private EditText etMoneyCount;

    private List<GetStoreItemList> storeItemList;
    private ListView detailRecyclerView;
    private boolean itemAddStatus = true;
    private int classID = 39; //초기 defult classID

    private CustomKeyboard mCustomKeyboard;
    private WaitCounter mWait;

    private long mStoreItemID;

    public FragmentAccessories(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_accessories, container, false);

        init(view);
        return view;
    }

    private void init(View view) {
        mWait = new WaitCounter(getActivity());

        setLayout(view);
        setAdapterDetailListData();
    }

    /**
     * Layout Setting View
     *
     * @param view
     */
    private void setLayout(View view) {
        setCustomKeyBoard(view);

        tvExistingCharge = (TextView) view.findViewById(R.id.tv_existing_charge);
        tvItemNameTitle = (TextView) view.findViewById(R.id.tv_item_name_title);
        etMoneyCount = (EditText) view.findViewById(R.id.et_money_count);

        btnSave = (Button) view.findViewById(R.id.btn_save);
        detailRecyclerView = (ListView) view.findViewById(R.id.list_view);
        btnItemAdd = (Button) view.findViewById(R.id.btn_item_add);

        btnItemAdd.setOnClickListener(btnItemOnClickListener);
        btnSave.setOnClickListener(btnItemOnClickListener);
    }

    /**
     * CustomKeyBoard reset
     *
     * @param view
     */
    private void setCustomKeyBoard(View view) {
        mCustomKeyboard = new CustomKeyboard((AppCompatActivity) getActivity(), view, R.id.laundry_keyboard_view, R.xml.custom_keyboard);
        mCustomKeyboard.registerEditText(view, R.id.et_money_count);
    }

    private View.OnClickListener btnItemOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_item_add:
                    setRepairRateAddDialog(0);
                    break;

                case R.id.btn_save:
                    mWait.show();
                    new Thread(new Runnable() {
                        public void run() {
                            //부속품 요금 변경 API
                            EnviromentManager.getRUpdateStoreItemPrice((AppCompatActivity) getActivity(), mStoreItemID, Integer.valueOf(etMoneyCount.getText().toString()), 0, 0, 0, 0, 0);
                            mActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    mWait.dismiss();
                                    //ListView Date Refresh
                                    etMoneyCount.setText("");
                                    setAdapterDetailListData();
                                }
                            });
                        }
                    }).start();
                    break;
            }
        }
    };

    /**
     * 부속품 아이템 리스트 Setting Adapter
     */
    private void setAdapterDetailListData() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                final ManagerGetStoreItemList managerGetStoreItemList = EnviromentManager.getManagerGetStoreItemList((AppCompatActivity) getActivity(), Integer.valueOf(6), 0, 0);

                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        storeItemList = managerGetStoreItemList.getStoreItemList();

                        AdapterDetailListData adapterLaundryRateTable = new AdapterDetailListData(getActivity(), existingCharge);
                        adapterLaundryRateTable.addItem(storeItemList);
                        detailRecyclerView.setAdapter(adapterLaundryRateTable);
                        adapterLaundryRateTable.notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }


    /**
     * 수선요금표 소분류 추가 Dialog View
     */
    private void setRepairRateAddDialog(int type) {
        mWait.show();
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_component_view, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);

        RecyclerView componentRecyclerView = (RecyclerView) view.findViewById(R.id.categore_recycle_view);
        TextView tvTitleName = (TextView) view.findViewById(R.id.tv_title_name);
        LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);
        LinearLayout llCategoreView = (LinearLayout) view.findViewById(R.id.ll_categore_view);

        EditText etItemName = (EditText) view.findViewById(R.id.et_item_name);
        Button btnItemAdd = (Button) view.findViewById(R.id.btn_item_add);

        if (type == 0) {
            tvTitleName.setText("부속품목 추가하기");
            btnItemAdd.setText("추가하기");
        } else {
            tvTitleName.setText("부속품목 수정");
            btnItemAdd.setText("수정하기");
        }

        new Thread(new Runnable() {
            public void run() {
                ManagerClassList managerClassList = LaundryRegistrationManager.getManagerClassList((AppCompatActivity) getActivity(), 16);
                mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        List<ClassListModel> storeItemLists = managerClassList.getClassListInfo();

                        componentRecyclerView.setHasFixedSize(true);
                        StaggeredGridLayoutManager laundryDetailLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
                        componentRecyclerView.setLayoutManager(laundryDetailLayoutManager);

                        AdapterCategoryDetail adapterCategoryDetail = new AdapterCategoryDetail(getActivity(), laundryItemListner, type);
                        adapterCategoryDetail.addItem(storeItemLists);
                        componentRecyclerView.setAdapter(adapterCategoryDetail);
                        adapterCategoryDetail.notifyDataSetChanged();
                    }
                });
            }
        }).start();

        btnItemAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etItemName.getText().toString().length() == 0) {
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());

                    thirdAlertPopup.setTitle("알림");
                    thirdAlertPopup.setContent("품목명을 적어주세요.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                } else {
                    List<GetStoreItemList> getStoreItemLists = ManagerGetStoreItemList.getmInstance().getStoreItemList();
                    for (GetStoreItemList getStoreItemList : getStoreItemLists) {
                        if (getStoreItemList.getItemName().equals(etItemName.getText().toString())) {
                            itemAddStatus = false;
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());

                            thirdAlertPopup.setTitle("알림");
                            thirdAlertPopup.setContent("이미 존재하는 이름이에요. 다시 입력해주세요.");
                            thirdAlertPopup.setButtonText("확인");
                            thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    itemAddStatus = true;
                                }
                            });
                            thirdAlertPopup.show();
                        }
                    }
                    if (itemAddStatus == true) {
                        setItemAdd(etItemName.getText().toString(), type);
                        etItemName.setText("");
                    }
                }
            }
        });

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(getActivity());
                thirdConfirmPopup.setTitle("팝업종료");
                thirdConfirmPopup.setContent("수선품목 추가 \n 팝업을 종료할까요?");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        dialog.dismiss();
                    }
                });
                thirdConfirmPopup.show();

            }
        });
        dialog.show();
    }

    /**
     * 정상적으로 품목이 추가 된 결과 체크 로직
     * (데이터 화면 갱신 하기 위한 로직 추가.)
     *
     * @param itemName
     */
    private void setItemAdd(String itemName, int type) {
        if (mActivity != null) {
            new Thread(new Runnable() {
                public void run() {
                    if (type == 0) {
                        EnviromentManager.getAddStoreItem((AppCompatActivity) getActivity(), classID, itemName);   //품목 추가
                    } else {
                        EnviromentManager.getUpdateStoreItem((AppCompatActivity) getActivity(), mStoreItemID, itemName); //품목 Name 수정
                    }
                    EnviromentManager.getManagerStoreManageInfo((AppCompatActivity)getActivity());

                    mActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getActivity());

                            if (type == 0) {
                                thirdAlertPopup.setTitle("품목추가");
                                thirdAlertPopup.setContent(itemName + " 추가 완료되었어요");
                                thirdAlertPopup.setButtonText("확인");
                            } else {
                                thirdAlertPopup.setTitle("품목수정");
                                thirdAlertPopup.setContent(itemName + " 수정 되었습니다.");
                                thirdAlertPopup.setButtonText("확인");
                            }
                            thirdAlertPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                                @Override
                                public void onConfirmClick() {
                                    dialog.dismiss();
                                }
                            });
                            thirdAlertPopup.show();

                            //Fragment View 갱신
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            ft.detach(FragmentAccessories.this).attach(FragmentAccessories.this).commit();
                        }
                    });
                }
            }).start();
        }
    }

    public interface existingCharge {
        void setExistionCharge(int amount, String itemName, long storeItemID);   //데이터 입력값 변경

        void setNotToUse(); //품목사용 여부 변경 reflesh

        void setItemName(int storeItemID);
    }


    private FragmentAccessories.existingCharge existingCharge = new FragmentAccessories.existingCharge() {
        @Override
        public void setExistionCharge(int amount, String itemName, long storeItemID) {
            tvItemNameTitle.setText(itemName);
            tvExistingCharge.setText(DataFormatUtil.moneyFormatToWon(amount));
            mStoreItemID = storeItemID;
        }

        @Override
        public void setNotToUse() {
            setAdapterDetailListData();
        }

        @Override
        public void setItemName(int storeItemID) {
            mStoreItemID = storeItemID;
            setRepairRateAddDialog(1);
        }
    };

    /**
     * 부속품 추가 대분류
     */
    private AdapterCategoryDetail.CCViewHolder.OnAdapterListner laundryItemListner = new AdapterCategoryDetail.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
            classID = position;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
}