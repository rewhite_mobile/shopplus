package rewhite.shopplus.fragment.sub;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.view.adapter.AdapterDelivery;
import rewhite.shopplus.view.adapter.AdapterTodayPager;

/**
 * Main 오늘의 수거 ViewPager View
 */
public class FragmentDelivery extends Fragment {

    private static final String TAG = "FragmentCollectionManagement";

    private ListView listDelivery;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delivery, container, false);

        setLayout(view);

        return view;
    }

    private void setLayout(View view) {
        listDelivery = (ListView) view.findViewById(R.id.list_delivery);

        ArrayList<String> testName = new ArrayList<>();

        testName.add("티모달인입니다.");
        testName.add("티모달인입니다.");
        testName.add("티모달인입니다.");
        testName.add("티모달인입니다.");

        AdapterDelivery adapterDelivery = new AdapterDelivery(getContext());

        adapterDelivery.addItem(testName);
        listDelivery.setAdapter(adapterDelivery);
    }
}

