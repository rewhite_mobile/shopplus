package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterAll;

@SuppressLint("ValidFragment")
public class FragmentDeliveryCompleted extends Fragment {

    private static final String TAG = "FragmentDeliveryCompleted";
    private WaitCounter mWait;

    private ListView listDelivery;
    private boolean mIsCheckBox;

    public FragmentDeliveryCompleted(FragmentVisitReception.OnArticleSelectedListener onArticleSelectedListener, boolean isCheckBox) {
        this.mIsCheckBox = isCheckBox;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delivery, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        mWait = new WaitCounter(getActivity());
        mWait.show();

        setLayout(view);
        setDataList();
    }

    private void setLayout(View view) {
        listDelivery = (ListView) view.findViewById(R.id.list_delivery);
    }

    private void setDataList() {
        new Thread(new Runnable() {
            public void run() {
                final ManagerGetVisitOrder managerGetVisitOrder = VisitReceptionManager.getGetVisitOrder((AppCompatActivity)getActivity(), "OUTFIN", SettingLogic.getStoreUserId(getActivity()), "", "");

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        List<GetVisitOrderList> getVisitOrderList = managerGetVisitOrder.getGetVisitOrder();
                        AdapterAll adapterAll = new AdapterAll(getContext(), mIsCheckBox, onAdapterListner);

                        adapterAll.addItem(getVisitOrderList);
                        listDelivery.setAdapter(adapterAll);
                    }
                });
            }
        }).start();
    }

    public interface OnAdapterListner {
        public void onAllCategory(List<GetVisitOrderList> getVisitOrderLists);
    }

    private FragmentAll.OnAdapterListner onAdapterListner = new FragmentAll.OnAdapterListner() {
        @Override
        public void onAllCategory(List<GetVisitOrderList> getVisitOrderLists) {

        }

        @Override
        public void unAllCategory(List<GetVisitOrderList> getVisitOrderLists) {

        }

        @Override
        public void onAllCategoryItem(boolean allStatus) {

        }

        @Override
        public void setSelectItem(GetVisitOrderList getVisitOrderLists, int payment, int nonPayment) {

        }

        @Override
        public void setUnSelectItem(GetVisitOrderList getVisitOrderLists, int payment, int nonPayment) {

        }
    };
}