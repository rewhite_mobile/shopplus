package rewhite.shopplus.fragment.sub;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.IOException;

import rewhite.shopplus.R;

public class FragmentPhoto extends Fragment {
    private static final String TAG = "FragmentPhoto";

    final int REQ_CODE_SELECT_IMAGE = 100;

    private ImageView imgPhotoViewFirst;
    private ImageView imgPhotoViewSecond;
    private ImageView imgPhotoViewThried;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_photo, null);

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        Button btnGallery = (Button) view.findViewById(R.id.btn_gallery);
        Button btnPhotoShoot = (Button) view.findViewById(R.id.btn_photo_shoot);

        imgPhotoViewFirst = (ImageView) view.findViewById(R.id.img_photo_view_first);
        imgPhotoViewSecond = (ImageView) view.findViewById(R.id.img_photo_view_second);
        imgPhotoViewThried = (ImageView) view.findViewById(R.id.img_photo_view_third);

        btnGallery.setOnClickListener(btnOnClickListener);
        btnPhotoShoot.setOnClickListener(btnOnClickListener);
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.btn_gallery:
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
                    intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, REQ_CODE_SELECT_IMAGE);
                    break;

                case R.id.btn_photo_shoot:

                    break;
            }
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_CODE_SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String name_Str = getImageNameToUri(data.getData());
                    Bitmap image_bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    imgPhotoViewFirst.setImageBitmap(image_bitmap);

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 사진 파일명 반환하기 위한 로직
     *
     * @param data
     * @return
     */
    public String getImageNameToUri(Uri data) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(data, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String imgPath = cursor.getString(column_index);
        String imgName = imgPath.substring(imgPath.lastIndexOf("/") + 1);

        return imgName;
    }
}