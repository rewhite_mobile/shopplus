package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityMemberDetails;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.data.dto.ItemGetVisitOrderList;
import rewhite.shopplus.logic.SettingLogic;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterAll;

@SuppressLint("ValidFragment")
public class FragmentAll extends Fragment {

    private static final String TAG = "FragmentAll";
    private List<GetVisitOrderList> mGetVisitOrderLists = new ArrayList<>();

    private WaitCounter mWait;
    private ListView lvAll;
    private boolean mIsCheckBox;

    private int mPayment, mNonPayment;

    public FragmentAll(FragmentVisitReception.OnArticleSelectedListener onArticleSelectedListener, boolean isCheckBox) {
        this.mIsCheckBox = isCheckBox;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        mWait = new WaitCounter(getActivity());
        mWait.show();

        setLayout(view);
        setDataList();
    }

    private void setLayout(View view) {
        lvAll = (ListView) view.findViewById(R.id.lv_all);
    }

    private void setDataList() {
        new Thread(new Runnable() {
            public void run() {
                final ManagerGetVisitOrder managerGetVisitOrder = VisitReceptionManager.getGetVisitOrder((AppCompatActivity)getActivity(), "ALL", SettingLogic.getStoreUserId(getActivity()), "", "");

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        List<GetVisitOrderList> getVisitOrderList = managerGetVisitOrder.getGetVisitOrder();

                        AdapterAll adapterAll = new AdapterAll(getContext(), mIsCheckBox, onAdapterListner);
                        adapterAll.addItem(getVisitOrderList);
                        lvAll.setAdapter(adapterAll);
                    }
                });
            }
        }).start();
    }

    public interface OnAdapterListner {
        void onAllCategory(List<GetVisitOrderList> getVisitOrderLists);

        void unAllCategory(List<GetVisitOrderList> getVisitOrderLists);

        void onAllCategoryItem(boolean allStatus);

        void setSelectItem(GetVisitOrderList getVisitOrderLists, int payment, int nonPayment);

        void setUnSelectItem(GetVisitOrderList getVisitOrderLists, int payment, int nonPayment);
    }

    private OnAdapterListner onAdapterListner = new OnAdapterListner() {
        @Override
        public void onAllCategory(List<GetVisitOrderList> getVisitOrderLists) {
            FragmentVisitReception.OnArticleSelectedListener.onAllCategory(getVisitOrderLists);
            ItemGetVisitOrderList.getmInstance().setGetVisitOrder(getVisitOrderLists);
        }

        @Override
        public void unAllCategory(List<GetVisitOrderList> getVisitOrderLists) {
            List<GetVisitOrderList> visitOrderLists = ItemGetVisitOrderList.getmInstance().getGetVisitOrder();
            try {
                for (GetVisitOrderList orderList : visitOrderLists) {
                    if (orderList.getStoreItemName().equals(getVisitOrderLists.get(visitOrderLists.size()).getStoreItemName())) {
                        visitOrderLists.remove(orderList);
                    }
                }
                ItemGetVisitOrderList.getmInstance().setGetVisitOrder(visitOrderLists);
            } catch (Exception err) {
                err.printStackTrace();
            }
        }

        @Override
        public void onAllCategoryItem(boolean allStatus) {
            FragmentVisitReception.OnArticleSelectedListener.onAllCategoryStatus(allStatus);
        }

        @Override
        public void setSelectItem(GetVisitOrderList getVisitOrderLists, int payment, int nonPayment) {
            mGetVisitOrderLists.add(getVisitOrderLists);
            mPayment += payment;
            mNonPayment += nonPayment;

            ItemGetVisitOrderList.getmInstance().setGetVisitOrder(mGetVisitOrderLists);

            ActivityMemberDetails.CountMoney(mGetVisitOrderLists.size(), mPayment, mNonPayment);
        }

        @Override
        public void setUnSelectItem(GetVisitOrderList getVisitOrderLists, int payment, int nonPayment) {
            mPayment -= payment;
            mNonPayment -= nonPayment;


            List<GetVisitOrderList> visitOrderLists = ItemGetVisitOrderList.getmInstance().getGetVisitOrder();
            try {
                for (GetVisitOrderList orderList : visitOrderLists) {
                    if (orderList.getStoreItemName().equals(getVisitOrderLists.getStoreItemName())) {
                        visitOrderLists.remove(orderList);
                    }
                }

                ActivityMemberDetails.CountMoney(visitOrderLists.size(), mPayment, mNonPayment);
                ItemGetVisitOrderList.getmInstance().setGetVisitOrder(visitOrderLists);
            } catch (Exception err) {
                err.printStackTrace();
            }
        }
    };
}