package rewhite.shopplus.fragment.sub;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import rewhite.shopplus.R;

/**
 * 미수금관리 View
 */
public class FragmentAcountsReceivableManagement extends Fragment {
    private static final String TAG = "FragmentAcountsReceivableManagement";

    private FrameLayout frameLayout;
    private TextView tvAttemptedMember, tvCommitmentItem;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_acounts_receivable_management, container, false);

        setLayout(view);
        setViewPagerAdapter();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FramgnetAccountsReceivable fragmentBasicSetting = new FramgnetAccountsReceivable(); //기본설정
        fragmentTransaction.replace(R.id.view_account_receivarble_member, fragmentBasicSetting);
        fragmentTransaction.commit();
    }

    private void setLayout(View view) {
        frameLayout = (FrameLayout) view.findViewById(R.id.view_account_receivarble_member);

        tvAttemptedMember = (TextView) view.findViewById(R.id.tv_attempted_member);
        tvCommitmentItem = (TextView) view.findViewById(R.id.tv_commitment_item);
    }


    private void setViewPagerAdapter() {

        tvAttemptedMember.setOnClickListener(movePageListener);
        tvAttemptedMember.setTag(0);
        tvCommitmentItem.setOnClickListener(movePageListener);
        tvCommitmentItem.setTag(1);

        tvAttemptedMember.setSelected(true);

    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            setTapBackGround(tag);

            switch (tag) {
                case 0:
                    FramgnetAccountsReceivable framgnetAccountsReceivable = new FramgnetAccountsReceivable();
                    fragmentTransaction.replace(R.id.view_account_receivarble_member, framgnetAccountsReceivable);
                    fragmentTransaction.commit();
                    break;

                case 1:
                    FragmentCommitmentItem fragmentCommitmentItem = new FragmentCommitmentItem();
                    fragmentTransaction.replace(R.id.view_account_receivarble_member, fragmentCommitmentItem);
                    fragmentTransaction.commit();
                    break;
            }
        }
    };

    /**
     * 탭 UI 변경 분기처리 로직
     *
     * @param tag
     */
    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                tvAttemptedMember.setBackgroundResource(R.drawable.a_button_type_2);
                tvAttemptedMember.setTextColor(getResources().getColor(R.color.color_c7cfe8));

                tvCommitmentItem.setBackgroundResource(R.drawable.a_button_type_3);
                tvCommitmentItem.setTextColor(getResources().getColor(R.color.color_43425c));
                break;

            case 1:
                tvCommitmentItem.setBackgroundResource(R.drawable.a_button_type_2);
                tvCommitmentItem.setTextColor(getResources().getColor(R.color.color_c7cfe8));

                tvAttemptedMember.setBackgroundResource(R.drawable.a_button_type_3);
                tvAttemptedMember.setTextColor(getResources().getColor(R.color.color_43425c));
                break;
        }
    }
}