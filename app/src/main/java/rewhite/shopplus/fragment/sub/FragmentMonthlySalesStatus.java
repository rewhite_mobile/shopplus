package rewhite.shopplus.fragment.sub;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.util.Logger;

public class FragmentMonthlySalesStatus extends Fragment {
    private static final String TAG = "FragmentMonthlySalesStatus";
    private LinearLayout llBg;
    private TextView tvAll, tvMonth, tvMonthToday, tv3month, tv6month, tv1year, tv2year, tv3year;

    private Button btnSearch;
    private WebView webView;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_monthly_sales_status, container, false);

        Logger.d(TAG, "FragmentMonthlySalesStatus");
        setLayout(view);
        setWebViewSetting();
        return view;
    }

    /**
     * WebView Setting
     */
    private void setWebViewSetting(){
        // 하드웨어 가속
        // 캐쉬 끄기
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setJavaScriptEnabled(true);
        //HTML5 Local Storage API 설정
        webView.getSettings().setDomStorageEnabled(true);

        webView.setWebChromeClient(new WebChromeClient());


        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });
        webView.loadUrl("https://www.rewhite.me/");
    }


    /**
     * 레이아웃 Setting
     * @param view
     */
    private void setLayout(View view) {
        webView = (WebView) view.findViewById(R.id.web_view);
        llBg = (LinearLayout) view.findViewById(R.id.ll_bg);
        btnSearch = (Button) view.findViewById(R.id.btn_search);

        tvAll = (TextView) view.findViewById(R.id.tv_all);
        tvMonth = (TextView) view.findViewById(R.id.tv_month);
        tvMonthToday = (TextView) view.findViewById(R.id.tv_month_today);
        tv3month = (TextView) view.findViewById(R.id.tv_3month);
        tv6month = (TextView) view.findViewById(R.id.tv_6month);
        tv1year = (TextView) view.findViewById(R.id.tv_1year);
        tv2year = (TextView) view.findViewById(R.id.tv_2year);
        tv3year = (TextView) view.findViewById(R.id.tv_3year);
        setTextClickView();
    }

    private void setTextClickView() {
        btnSearch.setOnClickListener(btnSearchListener);

        tvAll.setOnClickListener(movePageListener);
        tvAll.setTag(0);
        tvMonth.setOnClickListener(movePageListener);
        tvMonth.setTag(1);

        tvMonthToday.setOnClickListener(movePageListener);
        tvMonthToday.setTag(2);
        tv3month.setOnClickListener(movePageListener);
        tv3month.setTag(3);
        tv6month.setOnClickListener(movePageListener);
        tv6month.setTag(4);
        tv1year.setOnClickListener(movePageListener);
        tv1year.setTag(5);
        tv2year.setOnClickListener(movePageListener);
        tv2year.setTag(6);
        tv3year.setOnClickListener(movePageListener);
        tv3year.setTag(7);
        tvAll.setSelected(true);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();

            int selectPosition = 0;
            while (selectPosition < 8) {
                if (tag == selectPosition) {
                    llBg.findViewWithTag(selectPosition).setSelected(true);
                } else {
                    llBg.findViewWithTag(selectPosition).setSelected(false);
                }
                selectPosition++;
            }

            if(v.getId() == R.id.btn_search){
                
            }
        }
    };

    private View.OnClickListener btnSearchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };
}
