package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetPrePaidChargeHistory;
import rewhite.shopplus.client.model.GetPrePaidUseHistory;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterUsageHistory;

@SuppressLint("ValidFragment")
public class FragmentUsageHistory extends Fragment {
    private static final String TAG = "FragmentUsageHistory";

    private AppCompatActivity mActivity;
    private ListView lvMemberList;
    private int mUserStoreId;
    private WaitCounter mWait;
    private LinearLayout llUnsearch;

    public FragmentUsageHistory(int userStoreid) {
        this.mUserStoreId = userStoreid;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_usage_history, null);

        lvMemberList = (ListView) view.findViewById(R.id.lv_member_list);
        llUnsearch = (LinearLayout) view.findViewById(R.id.ll_unsearch);

        mWait = new WaitCounter(getActivity());

        settingView(view);
        return view;
    }

    private void settingView(View view) {
        List<GetPrePaidUseHistory> getmileageHistory = ManagerGetPrePaidChargeHistory.getmInstance().getManagerGetPrePaidUseHistory();
        if (getmileageHistory.size() == 0) {
            llUnsearch.setVisibility(View.VISIBLE);
            lvMemberList.setVisibility(View.GONE);
        } else {
            llUnsearch.setVisibility(View.GONE);
            lvMemberList.setVisibility(View.VISIBLE);

            AdapterUsageHistory adapterMileage = new AdapterUsageHistory(getActivity());
            adapterMileage.addItem(getmileageHistory);

            lvMemberList.setAdapter(adapterMileage);

        }
    }
}
