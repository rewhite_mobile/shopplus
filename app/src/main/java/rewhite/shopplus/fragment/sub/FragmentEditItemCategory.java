package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityEditItem;
import rewhite.shopplus.client.manager.ManagerLaundryCategory;
import rewhite.shopplus.client.model.LaundryCategoryItemList;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterCategory;
import rewhite.shopplus.view.adapter.AdapterComponent;

@SuppressLint("ValidFragment")
public class FragmentEditItemCategory extends Fragment{
    private static final String TAG = "FragmentCategory";

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private List<LaundryCategoryItemList> laundryCategoty;
    private ActivityEditItem.OnArticleSelectedListener mListener;
    private WaitCounter mWait;

    @SuppressLint("ValidFragment")
    public FragmentEditItemCategory(ActivityEditItem.OnArticleSelectedListener articleSelectedListener) {
        this.mListener = articleSelectedListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_catefory_name, null);
        mWait = new WaitCounter(getActivity());
        mWait.show();

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        setListData();
    }

    private void setListData() {
        try {
            new Thread(new Runnable() {
                public void run() {
                    laundryCategoty = ManagerLaundryCategory.getmInstance().getLaundryCategoty();

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            mWait.dismiss();
                            mRecyclerView.setHasFixedSize(true);

                            layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
                            mRecyclerView.setLayoutManager(layoutManager);

                            AdapterCategory adapterComponent = new AdapterCategory(getActivity(), onCompletItem, mOnAdapterListner);
                            adapterComponent.addItem(laundryCategoty);
                            mRecyclerView.setAdapter(adapterComponent);
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    public interface onCompletItem {
        void setSelectItem(String dataNumber, int getCategoryId);
    }

    public FragmentCategory.onCompletItem onCompletItem = new FragmentCategory.onCompletItem() {
        @Override
        public void setSelectItem(String dataNumber, int getCategoryId) {
            mListener.onItemSelected(dataNumber, getCategoryId);
        }
    };

    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

}
