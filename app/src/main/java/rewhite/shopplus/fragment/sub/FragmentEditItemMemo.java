package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityEditItem;
import rewhite.shopplus.client.manager.ManagerMemoitemList;
import rewhite.shopplus.client.model.MemoItemList;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.common.popup.ThirdConfirmPopup;
import rewhite.shopplus.data.constant.PrefConstant;
import rewhite.shopplus.data.manager.ManagerMemo;
import rewhite.shopplus.util.SharedPreferencesUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterComponent;
import rewhite.shopplus.view.adapter.AdapterMemoEdit;

@SuppressLint("ValidFragment")
public class FragmentEditItemMemo extends Fragment {
    private static final String TAG = "FragmentEditItemMemo";

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private WaitCounter mWait;
    private static int mPosition;
    private static String mType;

    private String addMemoData;
    private ActivityEditItem.OnArticleSelectedListener mListener;

    private LinearLayout llMemo;
    private TextView tvMemoData;

    private Button btnEdit;

    @SuppressLint("ValidFragment")
    public FragmentEditItemMemo(ActivityEditItem.OnArticleSelectedListener articleSelectedListener) {
        this.mListener = articleSelectedListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_memo, null);

        setWaitCount();
        setLayout(view);
        return view;
    }

    /**
     * Progress Loding Setting
     */
    private void setWaitCount() {
        mWait = new WaitCounter(getActivity());
        mWait.show();
    }

    /**
     * layout Setting
     *
     * @param view
     */
    private void setLayout(View view) {
        tvMemoData = (TextView) view.findViewById(R.id.tv_memo_data);
        llMemo = (LinearLayout) view.findViewById(R.id.ll_memo);
        btnEdit = (Button) view.findViewById(R.id.btn_edit);

        Button btnDelete = (Button) view.findViewById(R.id.btn_delete);
        Button btnMemoEdit = (Button) view.findViewById(R.id.btn_memo_edit);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view_memo);

        btnEdit.setOnClickListener(btnOnClickListener);
        btnDelete.setOnClickListener(btnOnClickListener);
        btnMemoEdit.setOnClickListener(btnOnClickListener);

        setListData();
    }

    /**
     * memo edit Dialog
     */
    private void setMemoAddDataDialog(int type) {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);

        View view = getActivity().getLayoutInflater().inflate(R.layout.activity_edit_filter, null);
        dialog.setContentView(view);
        dialog.setCancelable(false);

        Button btnNext = (Button) view.findViewById(R.id.btn_next);
        final EditText etMemo = (EditText) view.findViewById(R.id.et_filter_name);
        LinearLayout llClose = (LinearLayout) view.findViewById(R.id.ll_close);

        llClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ThirdConfirmPopup thirdConfirmPopup = new ThirdConfirmPopup(getActivity());
                thirdConfirmPopup.setTitle("종료팝업");
                thirdConfirmPopup.setContent("메모등록을 종료할까요?");
                thirdConfirmPopup.setButtonText("예");
                thirdConfirmPopup.setCancelButtonText("아니요");
                thirdConfirmPopup.setOnConfirmClickListener(new ThirdAlertPopup.OnConfirmClickListener() {
                    @Override
                    public void onConfirmClick() {
                        dialog.dismiss();
                    }
                });
                thirdConfirmPopup.show();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMemoData = etMemo.getText().toString();

                if (!TextUtils.isEmpty(addMemoData)) {
                    llMemo.setVisibility(View.VISIBLE);
                    tvMemoData.setText(addMemoData);
                    btnEdit.setVisibility(View.GONE);

                    //키보드 없애기
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etMemo.getWindowToken(), 0);

                    ArrayList<String> itemMemo = ManagerMemo.getInstance().getManagerMemo();

                    if (itemMemo.size() > 0) {
                        for (int i = 0; i < itemMemo.size(); i++) {
                            if (itemMemo.get(i).toString().equals(SharedPreferencesUtil.getStringSharedPreference(getActivity(), PrefConstant.KEY_EDITE_MEMO, PrefConstant.NAME_EDITE_MEMO))) {
                                itemMemo.remove(itemMemo.get(i).toString());
                                itemMemo.add(addMemoData);

                                SharedPreferencesUtil.putSharedPreference(getActivity(), PrefConstant.KEY_EDITE_MEMO, PrefConstant.NAME_EDITE_MEMO, addMemoData);
                                ManagerMemo.getInstance().setTManagerMemo(itemMemo);
                                mListener.onMemo(itemMemo);
                            } else if(type == 1){
                                if (i == 0) {
                                    itemMemo.add(addMemoData);
                                    ManagerMemo.getInstance().setTManagerMemo(itemMemo);
                                    mListener.onMemo(itemMemo);
                                    SharedPreferencesUtil.putSharedPreference(getActivity(), PrefConstant.KEY_EDITE_MEMO, PrefConstant.NAME_EDITE_MEMO, addMemoData);
                                }
                            }
                        }
                    } else {
                        itemMemo.add(addMemoData);
                        ManagerMemo.getInstance().setTManagerMemo(itemMemo);
                        mListener.onMemo(itemMemo);
                        SharedPreferencesUtil.putSharedPreference(getActivity(), PrefConstant.KEY_EDITE_MEMO, PrefConstant.NAME_EDITE_MEMO, addMemoData);
                    }
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * 메모 데이터 바인딩
     */
    private void setListData() {
        new Thread(new Runnable() {
            public void run() {
                final ManagerMemoitemList memoItemList = LaundryRegistrationManager.getMemoItemList((AppCompatActivity)getActivity(), 2);

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        mRecyclerView.setHasFixedSize(true);
                        List<MemoItemList> memoData = memoItemList.getStoreItemList();

                        layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
                        mRecyclerView.setLayoutManager(layoutManager);

                        AdapterMemoEdit adapterMemoEdit = new AdapterMemoEdit(getActivity(), onCompletItem, mOnAdapterListner, mPosition, mType);
                        adapterMemoEdit.addItem(memoData);
                        mRecyclerView.setAdapter(adapterMemoEdit);
                    }
                });
            }
        }).start();
    }

    public interface onCompletItem {
        void setSelectItem(String dataNumber);

        void setUnSelectItem(String dataNumber);

    }

    public FragmentMemo.onCompletItem onCompletItem = new FragmentMemo.onCompletItem() {
        @Override
        public void setSelectItem(String memoData) {
            ArrayList<String> itemMemo = ManagerMemo.getInstance().getManagerMemo();

            itemMemo.add(memoData);
            ManagerMemo.getInstance().setTManagerMemo(itemMemo);
            mListener.onMemo(itemMemo);
        }

        @Override
        public void setUnSelectItem(String unSelectItem) {
            ArrayList<String> itemMemo = ManagerMemo.getInstance().getManagerMemo();

            for (String memoData : new ArrayList<>(itemMemo)) {
                if (memoData.equals(unSelectItem)) {
                    itemMemo.remove(memoData);
                }
            }
            ManagerMemo.getInstance().setTManagerMemo(itemMemo);
            mListener.onMemo(itemMemo);
        }
    };

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.btn_edit:
                    setMemoAddDataDialog(1);
                    break;

                case R.id.btn_delete:
                    llMemo.setVisibility(View.GONE);
                    btnEdit.setVisibility(View.VISIBLE);
                    tvMemoData.setText("");

                    ArrayList<String> itemMemo = ManagerMemo.getInstance().getManagerMemo();
                    if (itemMemo.size() > 0) {
                        for (String memoData : new ArrayList<>(itemMemo)) {
                            if (memoData.equals(SharedPreferencesUtil.getStringSharedPreference(getActivity(), PrefConstant.KEY_EDITE_MEMO, PrefConstant.NAME_EDITE_MEMO))) {
                                itemMemo.remove(memoData);
                                ManagerMemo.getInstance().setTManagerMemo(itemMemo);
                                mListener.onMemo(itemMemo);
                            }
                        }
                    }

                    break;

                case R.id.btn_memo_edit:
                    setMemoAddDataDialog(2);
                    break;
            }
        }
    };

    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

    public static void setSeelctedItem(String type) {
        mType = type;
    }
}
