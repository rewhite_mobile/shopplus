package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityItemRegistration;

/**
 * 세탁방법 View
 */
@SuppressLint("ValidFragment")
public class FragmentHowToWash extends Fragment {
    private static final String TAG = "FragmentHowToWash";

    private Button btnDry, btnWaterWashing, btnOnDry;
    private ActivityItemRegistration.OnArticleSelectedListener mListener;

    private boolean isHowToWash, IroningStatus;

    @SuppressLint("ValidFragment")
    public FragmentHowToWash(ActivityItemRegistration.OnArticleSelectedListener onArticleSelectedListener) {
        this.mListener = onArticleSelectedListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_how_to_wash, null);

        init(view);
        return view;
    }

    private void init(View view) {
        setLayout(view);
    }

    private void setLayout(View view) {
        btnDry = (Button) view.findViewById(R.id.btn_dry);
        btnWaterWashing = (Button) view.findViewById(R.id.btn_water_washing);
        btnOnDry = (Button) view.findViewById(R.id.btn_on_dry);

        btnDry.setOnClickListener(btnStatusListiner);
        btnWaterWashing.setOnClickListener(btnStatusListiner);
        btnOnDry.setOnClickListener(btnStatusListiner);
    }

    private View.OnClickListener btnStatusListiner = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.btn_dry:
                    setIsHowtoWash(id);
                    break;
                case R.id.btn_water_washing:
                    setIsHowtoWash(id);
                    break;
                case R.id.btn_on_dry:
                    setIsHowtoWash(id);
                    break;
            }
        }
    };

    private void setIsHowtoWash(int id) {
        if (id == R.id.btn_dry) {
            setOtionSelect(1);
            btnDry.setTextColor(getResources().getColor(R.color.Wite_color));
            btnDry.setBackground(getResources().getDrawable(R.drawable.a_button_type_2));

            btnWaterWashing.setTextColor(getResources().getColor(R.color.color_43425c));
            btnWaterWashing.setBackground(getResources().getDrawable(R.drawable.a_button_type_3));

            btnOnDry.setTextColor(getResources().getColor(R.color.color_43425c));
            btnOnDry.setBackground(getResources().getDrawable(R.drawable.a_button_type_3));
        } else if (id == R.id.btn_water_washing) {
            setOtionSelect(2);
            btnWaterWashing.setTextColor(getResources().getColor(R.color.Wite_color));
            btnWaterWashing.setBackground(getResources().getDrawable(R.drawable.a_button_type_2));

            btnDry.setTextColor(getResources().getColor(R.color.color_43425c));
            btnDry.setBackground(getResources().getDrawable(R.drawable.a_button_type_3));

            btnOnDry.setTextColor(getResources().getColor(R.color.color_43425c));
            btnOnDry.setBackground(getResources().getDrawable(R.drawable.a_button_type_3));
        } else if (id == R.id.btn_on_dry) {
            setOtionSelect(3);
            btnOnDry.setTextColor(getResources().getColor(R.color.Wite_color));
            btnOnDry.setBackground(getResources().getDrawable(R.drawable.a_button_type_2));

            btnDry.setTextColor(getResources().getColor(R.color.color_43425c));
            btnDry.setBackground(getResources().getDrawable(R.drawable.a_button_type_3));

            btnWaterWashing.setTextColor(getResources().getColor(R.color.color_43425c));
            btnWaterWashing.setBackground(getResources().getDrawable(R.drawable.a_button_type_3));
        }
    }

    private void setOtionSelect(int position) {
        if (position == 1) {
            mListener.onHowToWash("드라이, 다림질");
        } else if (position == 2) {
            mListener.onHowToWash("물세탁, 다림질");
        } else if (position == 3) {
            mListener.onHowToWash("드라이만");
        }
    }
}
