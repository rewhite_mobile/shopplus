package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityItemRegistration;
import rewhite.shopplus.client.manager.ManagerStoreItemList;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.data.dto.ItemAdditionalData;
import rewhite.shopplus.data.manager.ManagerRepairFree;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterComponent;
import rewhite.shopplus.view.adapter.AdapterRepairFree;

/**
 * 품목등록 수선요금 추가 View
 */
@SuppressLint("ValidFragment")
public class FragmentRepairFee extends Fragment {
    private static final String TAG = "FragmentCategory";
    private static int mPosition;
    private static String mType; //수정 /추가 type 구분

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private WaitCounter mWait;
    private ActivityItemRegistration.OnArticleSelectedListener mListener;

    @SuppressLint("ValidFragment")
    public FragmentRepairFee(ActivityItemRegistration.OnArticleSelectedListener onArticleSelectedListener) {
        this.mListener = onArticleSelectedListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_repair_fee, null);
        mWait = new WaitCounter(getActivity());
        mWait.show();

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);

        setListData();
    }

    private void setListData() {
        new Thread(new Runnable() {
            public void run() {
                final ManagerStoreItemList managerStoreItemList = LaundryRegistrationManager.getRepairItemList((AppCompatActivity)getActivity(), Long.valueOf("2"));

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        mRecyclerView.setHasFixedSize(true);

                        List<StoreItemList> storeItemList = managerStoreItemList.getStoreItemList();
                        layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
                        mRecyclerView.setLayoutManager(layoutManager);

                        AdapterRepairFree adapterComponent = new AdapterRepairFree(getActivity(), onCompletItem, mOnAdapterListner, mPosition, mType);
                        adapterComponent.addItem(storeItemList);
                        mRecyclerView.setAdapter(adapterComponent);
                    }
                });
            }
        }).start();
    }

    public interface onCompletItem {
        void setSelectItem(String dataNumber, String amount, int storeItemID);
        void setUnSelectItem(String dataNumber, String amount, int storeItemID);
    }

    public FragmentRepairFee.onCompletItem onCompletItem = new FragmentRepairFee.onCompletItem() {
        @Override
        public void setSelectItem(String dataName, String amount, int storeItemID) {
            ItemAdditionalData.ItemRepairFreeEdit itemComponent = new ItemAdditionalData.ItemRepairFreeEdit();

            ArrayList<ItemAdditionalData.ItemRepairFreeEdit> itemComponentList = ManagerRepairFree.getInstance().getRepairFreeDates();
            itemComponent.setTitleName(dataName);
            itemComponent.setAmount(amount);
            itemComponent.setOptionItemID(String.valueOf(storeItemID));

            itemComponentList.add(itemComponent);

            ManagerRepairFree.getInstance().setRepairFeeDates(itemComponentList);
            mListener.onRepairFee(itemComponentList);
        }

        @Override
        public void setUnSelectItem(String dataNumber, String amount, int storeItemID) {
            ArrayList<ItemAdditionalData.ItemRepairFreeEdit> itemComponentList = ManagerRepairFree.getInstance().getRepairFreeDates();

            for (ItemAdditionalData.ItemRepairFreeEdit itemRepairFree : new ArrayList<>(itemComponentList)) {
                if (dataNumber.equals(itemRepairFree.getTitleName())) {
                    itemComponentList.remove(itemRepairFree);
                }
            }
            ManagerRepairFree.getInstance().setRepairFeeDates(itemComponentList);
            mListener.onRepairFee(itemComponentList);
        }
    };


    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

    public static void setSeelctedItem(int position, String type) {
        mPosition = position;
        mType = type;
    }

}