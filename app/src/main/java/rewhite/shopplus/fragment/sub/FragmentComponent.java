package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityItemRegistration;
import rewhite.shopplus.client.manager.ManagerStoreItemList;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.data.dto.ItemAdditionalData;
import rewhite.shopplus.data.manager.ManagerComponent;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterComponent;


@SuppressLint("ValidFragment")
public class FragmentComponent extends Fragment {
    private static final String TAG = "FragmentComponent";

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private WaitCounter mWait;

    private static String mType;
    private static int mPosition;

    private ActivityItemRegistration.OnArticleSelectedListener mListener;

    @SuppressLint("ValidFragment")
    public FragmentComponent(ActivityItemRegistration.OnArticleSelectedListener onArticleSelectedListener) {
        this.mListener = onArticleSelectedListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_component, null);
        mWait = new WaitCounter(getActivity());
        mWait.show();

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        setListData();
    }

    private void setListData() {
        new Thread(new Runnable() {
            public void run() {
                final ManagerStoreItemList managerStoreItemList = LaundryRegistrationManager.getComponentItemList((AppCompatActivity) getActivity(), Long.valueOf("6"));

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        mRecyclerView.setHasFixedSize(true);
                        List<StoreItemList> storeItemList = managerStoreItemList.getStoreItemList();

                        layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
                        mRecyclerView.setLayoutManager(layoutManager);

                        AdapterComponent adapterComponent = new AdapterComponent(getActivity(), onCompletItem, mOnAdapterListner, mPosition, mType);
                        adapterComponent.addItem(storeItemList);
                        mRecyclerView.setAdapter(adapterComponent);
                    }
                });
            }
        }).start();
    }

    public interface onCompletItem {
        void setSelectItem(String dataNumber, String moeny, int orderItemID);

        void setUnSelectItem(String dataNumber, String moeny);
    }

    public onCompletItem onCompletItem = new onCompletItem() {
        @Override
        public void setSelectItem(String dataName, String moeny, int orderItemID) {
            ItemAdditionalData.ItemComponentEdit itemComponent = new ItemAdditionalData.ItemComponentEdit();

            ArrayList<ItemAdditionalData.ItemComponentEdit> itemComponentList = ManagerComponent.getInstance().getTComponent();
            itemComponent.setTitleName(dataName);
            itemComponent.setAmount(moeny);
            itemComponent.setOptionItemID(String.valueOf(orderItemID));
            itemComponentList.add(itemComponent);

            ManagerComponent.getInstance().setComponent(itemComponentList);
            mListener.onComponent(itemComponentList);
        }

        @Override
        public void setUnSelectItem(String dataNumber, String moeny) {
            ArrayList<ItemAdditionalData.ItemComponentEdit> itemComponentList = ManagerComponent.getInstance().getTComponent();

            for (ItemAdditionalData.ItemComponentEdit itemRepairFree : new ArrayList<>(itemComponentList)) {
                if (dataNumber.equals(itemRepairFree.getTitleName())) {
                    itemComponentList.remove(itemRepairFree);
                }
            }
            ManagerComponent.getInstance().setComponent(itemComponentList);
            mListener.onComponent(itemComponentList);
        }
    };


    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

    public static void setSeelctedItem(int position, String type) {
        mPosition = position;
        mType = type;
    }

}
