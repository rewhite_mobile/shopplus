package rewhite.shopplus.fragment.sub;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerNotOutUserManageList;
import rewhite.shopplus.client.model.NotOutUserManageList;
import rewhite.shopplus.client.network.OperationsManagement;
import rewhite.shopplus.common.CustomKeyboard1;
import rewhite.shopplus.common.popup.ThirdAlertPopup;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterMissingMemberList;

/**
 * 미출고 회원 목록 View
 */
public class FragmentMissingMembersList extends Fragment {
    private static final String TAG = "FragmentMissingMembersList";

    private List<NotOutUserManageList> notOutUserManageList;
    private String searchPhone = null;
    private String userName = null;
    private WaitCounter mWait;

    private Button btnSearch;
    private EditText etKeyboardText;
    private Spinner sMember;

    private View view;
    private int mPosition;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_missing_members_list, container, false);

        mWait = new WaitCounter(getActivity());

        init(view);
        setCustomKeyBoard(view);
        return view;
    }

    private void init(View view) {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {

                OperationsManagement.getNotOutUserManageList((AppCompatActivity)getActivity(), searchPhone, userName);
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        notOutUserManageList = ManagerNotOutUserManageList.getmInstance().getUserManageList();

                        setLayout();
                        ListView listView = (ListView) view.findViewById(R.id.list_view_missing);

                        AdapterMissingMemberList adapterMissingMemberList = new AdapterMissingMemberList(getContext());
                        adapterMissingMemberList.addItem(notOutUserManageList);
                        listView.setAdapter(adapterMissingMemberList);
                    }
                });
            }
        }).start();
    }

    private void setLayout() {
        btnSearch = (Button) view.findViewById(R.id.btn_search);
        sMember = (Spinner) view.findViewById(R.id.s_member);

        etKeyboardText = (EditText) view.findViewById(R.id.et_keyboard_text);
        etKeyboardText.requestFocus();

        TextView tvMemberCount = (TextView) view.findViewById(R.id.tv_member_count);
        tvMemberCount.setText(notOutUserManageList.size() + " 명의 회원이 있습니다.");

        sMember.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager imm;
                switch (position) {
                    case 0:
                        mPosition = 0;
                        InputMethodManager immhide = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                        immhide.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                        break;
                    case 1:
                        mPosition = 1;
                        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
//        setEditTextView();
        btnSearch.setOnClickListener(btnOnClickListener);
    }

    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mPosition == 0) {
                if (etKeyboardText.getText().toString().length() != 4) {
                    mWait.dismiss();
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getContext());
                    thirdAlertPopup.setTitle("오류 팝업");
                    thirdAlertPopup.setContent("핸드폰 뒷자리 4자리를 입력해주세요.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                } else if (etKeyboardText.getText().toString().length() == 0) {
                    mWait.dismiss();
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getContext());
                    thirdAlertPopup.setTitle("오류 팝업");
                    thirdAlertPopup.setContent("핸드폰 뒷자리 4자리를 입력해주세요.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                } else {
                    searchPhone = etKeyboardText.getText().toString();
                }
            } else if (mPosition == 1) {
                if (etKeyboardText.getText().toString().length() == 0) {
                    mWait.dismiss();
                    ThirdAlertPopup thirdAlertPopup = new ThirdAlertPopup(getContext());
                    thirdAlertPopup.setTitle("오류 팝업");
                    thirdAlertPopup.setContent("회원명을 입력해주세요.");
                    thirdAlertPopup.setButtonText("확인");
                    thirdAlertPopup.show();
                } else {
                    userName = etKeyboardText.getText().toString();
                }
            }
            init(view);
        }
    };

    /**
     * CustomKeyBoard reset
     *
     * @param view
     */
    private void setCustomKeyBoard(View view) {
        InputMethodManager immhide = (InputMethodManager) getActivity().getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        immhide.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        CustomKeyboard1 mCustomKeyboard = new CustomKeyboard1((AppCompatActivity)getActivity(), view, R.id.key_boardview, R.xml.custom_keyboard);
        mCustomKeyboard.registerEditText(view, R.id.et_keyboard_text);
    }
}