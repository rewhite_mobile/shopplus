package rewhite.shopplus.fragment.sub;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import rewhite.shopplus.R;

/**
 * 매출 현황 (월별)
 */
public class FragmentDailySalesStatus extends Fragment {
    private static final String TAG = "FragmentDailySalesStatus";

    private LinearLayout llBg;
    private TextView tvAll, tvMonth, tvMonthToday, tv3Month;

    private Button btnSearch;
    private WebView webView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_daily_sales_status, container, false);

        init(view);
        return view;
    }

    private void init(View view) {
        setLayout(view);
        setWebViewSetting();
        setTextClickView();
    }

    /**
     * 레이아웃 셋팅 View
     *
     * @param view
     */
    private void setLayout(View view) {
        webView = (WebView) view.findViewById(R.id.web_view);
        tvAll = (TextView) view.findViewById(R.id.tv_all);
        tvMonth = (TextView) view.findViewById(R.id.tv_month);
        tvMonthToday = (TextView) view.findViewById(R.id.tv_month_today);
        tv3Month = (TextView) view.findViewById(R.id.tv_3month);

        btnSearch = (Button) view.findViewById(R.id.btn_search);
    }

    /**
     * 웹뷰 셋팅 View
     */
    private void setWebViewSetting() {
        // 하드웨어 가속
        // 캐쉬 끄기
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setJavaScriptEnabled(true);
        //HTML5 Local Storage API 설정
        webView.getSettings().setDomStorageEnabled(true);

        webView.setWebChromeClient(new WebChromeClient());


        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });
        webView.loadUrl("https://www.rewhite.me/");
    }

    private void setTextClickView() {
        btnSearch.setOnClickListener(btnSearchListener);

        tvAll.setOnClickListener(movePageListener);
        tvAll.setTag(0);
        tvMonth.setOnClickListener(movePageListener);
        tvMonth.setTag(1);

        tvMonthToday.setOnClickListener(movePageListener);
        tvMonthToday.setTag(2);
        tv3Month.setOnClickListener(movePageListener);
        tv3Month.setTag(3);
        tvAll.setSelected(true);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();

            int selectPosition = 0;
            while (selectPosition < 8) {
                if (tag == selectPosition) {
                    llBg.findViewWithTag(selectPosition).setSelected(true);
                } else {
                    llBg.findViewWithTag(selectPosition).setSelected(false);
                }
                selectPosition++;
            }

            if (v.getId() == R.id.btn_search) {

            }
        }
    };

    private View.OnClickListener btnSearchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };
}
