package rewhite.shopplus.fragment.sub;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerUserManageList;
import rewhite.shopplus.client.model.UserManageList;
import rewhite.shopplus.client.network.OperationsManagement;
import rewhite.shopplus.util.DataFormatUtil;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterMembershipManagement;

/**
 * 회원관리 View
 */
public class FragmentMembershipManagement extends Fragment {
    private static final String TAG = "FragmentMembershipManagement";

    private WaitCounter mWait;
    private List<UserManageList> userManagerList;

    private TextView tvTabAll, tvLately1Month, tvLately3Month, tvLately6Month;
    private TextView tvType1, tvMemberCount;
    private ListView listView;

    private LinearLayout llType1, llType2, llType3, llType4;
    private ImageView imgType2, imgType3, imgType4;

    private Button btnSerch;
    private TextView tvStartCalender, tvEndCalender;

    private String userGrade = null;
    private String startDate = null;
    private String endDate = null;

    private View view;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_membership_management, container, false);

        mWait = new WaitCounter(getContext());
        init(view);
        return view;
    }

    private void init(View view) {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {

                OperationsManagement.getManagerUserManageList((AppCompatActivity)getActivity(), userGrade, startDate, endDate);
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        userManagerList = ManagerUserManageList.getmInstance().getUserManageList();

                        setLayout(view);
                        AdapterMembershipManagement adapterMembershipManagement = new AdapterMembershipManagement(getContext());
                        adapterMembershipManagement.addItem(userManagerList);
                        listView.setAdapter(adapterMembershipManagement);
                    }
                });
            }
        }).start();
    }

    private void setLayout(View view) {
        listView = (ListView) view.findViewById(R.id.list_view);

        tvMemberCount = (TextView) view.findViewById(R.id.tv_member_count);
        tvTabAll = (TextView) view.findViewById(R.id.tv_tab_all);
        tvLately1Month = (TextView) view.findViewById(R.id.tv_tab_lately_1month);
        tvLately3Month = (TextView) view.findViewById(R.id.tv_tab_lately_3month);
        tvLately6Month = (TextView) view.findViewById(R.id.tv_tab_lately_6month);

        llType1 = (LinearLayout) view.findViewById(R.id.ll_type1);
        llType2 = (LinearLayout) view.findViewById(R.id.ll_type2);
        llType3 = (LinearLayout) view.findViewById(R.id.ll_type3);
        llType4 = (LinearLayout) view.findViewById(R.id.ll_type4);

        tvType1 = (TextView) view.findViewById(R.id.tv_type_1);
        imgType2 = (ImageView) view.findViewById(R.id.img_type_2);
        imgType3 = (ImageView) view.findViewById(R.id.img_type_3);
        imgType4 = (ImageView) view.findViewById(R.id.img_type_4);

        btnSerch = (Button) view.findViewById(R.id.btn_serch);
        setTabClickView();
        setTextView();
    }

    private void setTextView() {
        tvMemberCount.setText(userManagerList.size() + "명의 회원이 있습니다.");
    }

    private void setTabClickView() {
        tvTabAll.setOnClickListener(tabOnClickListener);
        tvLately1Month.setOnClickListener(tabOnClickListener);
        tvLately3Month.setOnClickListener(tabOnClickListener);
        tvLately6Month.setOnClickListener(tabOnClickListener);
        btnSerch.setOnClickListener(tabOnClickListener);

        llType1.setOnClickListener(memberOnClickListener);
        llType2.setOnClickListener(memberOnClickListener);
        llType3.setOnClickListener(memberOnClickListener);
        llType4.setOnClickListener(memberOnClickListener);
    }

    private View.OnClickListener memberOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.ll_type1:
                    llType1.setBackgroundResource(R.drawable.a_button_type_2);
                    userGrade = null;
                    tvType1.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));

                    imgType2.setVisibility(View.GONE);
                    imgType3.setVisibility(View.GONE);
                    imgType4.setVisibility(View.GONE);
                    break;

                case R.id.ll_type2:
                    userGrade = "1";
                    llType1.setBackgroundResource(R.drawable.a_button_type_3);
                    tvType1.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    imgType2.setVisibility(View.VISIBLE);
                    imgType3.setVisibility(View.GONE);
                    imgType4.setVisibility(View.GONE);
                    break;

                case R.id.ll_type3:
                    userGrade = "2";
                    llType1.setBackgroundResource(R.drawable.a_button_type_3);
                    tvType1.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    imgType2.setVisibility(View.GONE);
                    imgType3.setVisibility(View.VISIBLE);
                    imgType4.setVisibility(View.GONE);
                    break;

                case R.id.ll_type4:
                    userGrade = "3";
                    llType1.setBackgroundResource(R.drawable.a_button_type_3);
                    tvType1.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    imgType2.setVisibility(View.GONE);
                    imgType3.setVisibility(View.GONE);
                    imgType4.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    private View.OnClickListener tabOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.tv_tab_all:
                    startDate = null;
                    endDate = null;
                    tvTabAll.setBackgroundResource(R.drawable.a_button_type_2);
                    tvTabAll.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));

                    tvLately1Month.setBackgroundResource(R.drawable.a_button_type_3);
                    tvLately1Month.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    tvLately3Month.setBackgroundResource(R.drawable.a_button_type_3);
                    tvLately3Month.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    tvLately6Month.setBackgroundResource(R.drawable.a_button_type_3);
                    tvLately6Month.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    break;

                case R.id.tv_tab_lately_1month:
                    startDate = DataFormatUtil.getMonthAgoDate();
                    endDate = DataFormatUtil.getToday();

                    tvTabAll.setBackgroundResource(R.drawable.a_button_type_3);
                    tvTabAll.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    tvLately1Month.setBackgroundResource(R.drawable.a_button_type_2);
                    tvLately1Month.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));

                    tvLately3Month.setBackgroundResource(R.drawable.a_button_type_3);
                    tvLately3Month.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    tvLately6Month.setBackgroundResource(R.drawable.a_button_type_3);
                    tvLately6Month.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    break;

                case R.id.tv_tab_lately_3month:
                    startDate = DataFormatUtil.get3MonthAgoDate();
                    endDate = DataFormatUtil.getToday();

                    tvTabAll.setBackgroundResource(R.drawable.a_button_type_3);
                    tvTabAll.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    tvLately1Month.setBackgroundResource(R.drawable.a_button_type_3);
                    tvLately1Month.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    tvLately3Month.setBackgroundResource(R.drawable.a_button_type_2);
                    tvLately3Month.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));

                    tvLately6Month.setBackgroundResource(R.drawable.a_button_type_3);
                    tvLately6Month.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    break;

                case R.id.tv_tab_lately_6month:
                    startDate = DataFormatUtil.get6MonthAgoDate();
                    endDate = DataFormatUtil.getToday();

                    tvTabAll.setBackgroundResource(R.drawable.a_button_type_3);
                    tvTabAll.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    tvLately1Month.setBackgroundResource(R.drawable.a_button_type_3);
                    tvLately1Month.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    tvLately3Month.setBackgroundResource(R.drawable.a_button_type_3);
                    tvLately3Month.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));

                    tvLately6Month.setBackgroundResource(R.drawable.a_button_type_2);
                    tvLately6Month.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                    break;

                case R.id.btn_serch:
                    //TODO 검색 로직 추가.
                    init(view);
                    break;
            }
        }
    };
}