package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityEditItem;
import rewhite.shopplus.client.manager.ManagerStoreItemList;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterComponent;
import rewhite.shopplus.view.adapter.AdapterLaundryCharge;

@SuppressLint("ValidFragment")
public class FragmentEditItemLaundryCharge extends Fragment {
    private static final String TAG = "FragmentEditItemLaundryCharge";
    private static String mLaundryName;
    private ActivityEditItem.OnArticleSelectedListener mListener;

    private int mCategoryId;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private WaitCounter mWait;


    @SuppressLint("ValidFragment")
    public FragmentEditItemLaundryCharge(ActivityEditItem.OnArticleSelectedListener onArticleSelectedListener) {
        this.mListener = onArticleSelectedListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_laundry_charge, null);
        mWait = new WaitCounter(getActivity());
        mWait.show();

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        TextView laundryName = (TextView) view.findViewById(R.id.tv_laundry_title_name);
        laundryName.setText(mLaundryName);
        setListData();
    }

    private void setListData() {
        new Thread(new Runnable() {
            public void run() {
                final ManagerStoreItemList managerStoreItemList = LaundryRegistrationManager.getStoreItemList((AppCompatActivity)getActivity(), 1, mCategoryId, 0);

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        List<StoreItemList> storeItemList = managerStoreItemList.getStoreItemList();
                        mRecyclerView.setHasFixedSize(true);

                        layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
                        mRecyclerView.setLayoutManager(layoutManager);

                        AdapterLaundryCharge adapterComponent = new AdapterLaundryCharge(getActivity(), onCompletItem, mOnAdapterListner);
                        adapterComponent.addItem(storeItemList);
                        mRecyclerView.setAdapter(adapterComponent);
                    }
                });
            }
        }).start();
    }

    public interface onCompletItem {
        void setSelectItem(String dataNumber, int money, int storeItemID, int itemGrade);
        void onElementClick(int position, CheckBox elementIndex, boolean checked);
    }

    public FragmentLaundryCharge.onCompletItem onCompletItem = new FragmentLaundryCharge.onCompletItem() {
        @Override
        public void setSelectItem(String dataNumber, int money, int storeItemID, int itemGrade) {
            mListener.onLaundryCharge(dataNumber, money, storeItemID, itemGrade);
        }

        @Override
        public void onElementClick(int position, CheckBox elementIndex, boolean checked) {
            if (checked) {
                elementIndex.setBackgroundResource(R.drawable.a_button_type_2);
            } else {
                elementIndex.setBackgroundResource(R.drawable.a_button_type_3);
            }
        }
    };


    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {

        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {
        }
    };


    public static void setLaundryName(String laundryName) {
        mLaundryName = laundryName;
    }
}