package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityEditItem;
import rewhite.shopplus.client.manager.ManagerStoreItemList;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.data.dto.ItemTechnical;
import rewhite.shopplus.data.manager.ManagerTechnicalFeeEdit;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterComponent;
import rewhite.shopplus.view.adapter.AdapterTechnicalFeeEdit;

@SuppressLint("ValidFragment")
public class FragmentEditItemTechnicalFee extends Fragment {
    private static final String TAG = "FragmentEditItemTechnicalFee";
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private WaitCounter mWait;

    private ActivityEditItem.OnArticleSelectedListener mListener;

    private static String mSelected;
    @SuppressLint("ValidFragment")
    public FragmentEditItemTechnicalFee(ActivityEditItem.OnArticleSelectedListener onArticleSelectedListener) {
        this.mListener = onArticleSelectedListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_technica_fee, null);
        mWait = new WaitCounter(getActivity());
        mWait.show();

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        setListData();
    }

    private void setListData() {
        new Thread(new Runnable() {
            public void run() {
                final ManagerStoreItemList managerStoreItemList = LaundryRegistrationManager.getTechnicalItemList((AppCompatActivity)getActivity(), Long.valueOf("5"));

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        mRecyclerView.setHasFixedSize(true);
                        List<StoreItemList> storeItemList = managerStoreItemList.getStoreItemList();

                        layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
                        mRecyclerView.setLayoutManager(layoutManager);

                        AdapterTechnicalFeeEdit adapterTechnicalFee = new AdapterTechnicalFeeEdit(getActivity(), onCompletItem, mOnAdapterListner, mSelected);
                        adapterTechnicalFee.addItem(storeItemList);
                        mRecyclerView.setAdapter(adapterTechnicalFee);
                    }
                });
            }
        }).start();
    }


    public interface onCompletItem {
        void setSelectItem(String dataNumber, String amount);

        void setUnSelectItem(String dataNumber, String amount);
    }

    public FragmentTechnicalFee.onCompletItem onCompletItem = new FragmentTechnicalFee.onCompletItem() {
        @Override
        public void setSelectItem(String dataName, String amount, int oderItemId) {
            ItemTechnical itemTechnical = new ItemTechnical();

            ArrayList<ItemTechnical> itemTechnicals = ManagerTechnicalFeeEdit.getInstance().getTechnicalFee();
            itemTechnical.setTitleName(dataName);
            itemTechnical.setAmount(amount);
            itemTechnicals.add(itemTechnical);

            ManagerTechnicalFeeEdit.getInstance().setTechnicalFee(itemTechnicals);
            mListener.onTechnicalFree(itemTechnicals);
        }

        @Override
        public void setUnSelectItem(String dataNumber, String amount) {
            ArrayList<ItemTechnical> itemTechnicals = ManagerTechnicalFeeEdit.getInstance().getTechnicalFee();
            for (ItemTechnical itemRepairFree : new ArrayList<>(itemTechnicals)) {
                if (dataNumber.equals(itemRepairFree.getTitleName())) {
                    itemTechnicals.remove(itemRepairFree);
                }
            }
            ManagerTechnicalFeeEdit.getInstance().setTechnicalFee(itemTechnicals);
            mListener.onTechnicalFree(itemTechnicals);
        }
    };


    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

    public static void setSelected(String type){
        mSelected = type;
    }
}