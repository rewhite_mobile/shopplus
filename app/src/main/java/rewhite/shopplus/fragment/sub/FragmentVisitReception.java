package rewhite.shopplus.fragment.sub;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetVisitOrder;
import rewhite.shopplus.client.manager.ManagerStoreUserInfo;
import rewhite.shopplus.client.model.GetVisitOrderList;
import rewhite.shopplus.client.model.StoreUserInfoItemList;
import rewhite.shopplus.client.network.VisitReceptionManager;
import rewhite.shopplus.data.dto.ItemRepairFree;
import rewhite.shopplus.logic.SettingLogic;

/**
 * 방문 접수 View
 */
public class FragmentVisitReception extends Fragment {
    private static final String TAG = "FragmentVisitReception";

    private static List<GetVisitOrderList> makePaymentList = new ArrayList<>();
    private LinearLayout rlAll;
    private LinearLayout rlDeliveryCompleted;
    private LinearLayout rlUnreleased;
    private LinearLayout rlAttemptedDelivery;
    private List<GetVisitOrderList> getVisitOrderListALL, getVisitOrderListOUT, getVisitOrderListNOTOUT, getVisitOrderListMISSOUT;
    private List<StoreUserInfoItemList> storeUserInfoItemList;

    private TextView tvAll, tvDeliveryCompleted, tvUnreleased, tvAttemptedDelivery;
    private TextView tvAllCount, tvDeliveryCompletedCount, tvUnreleasedCount, tvAttemptedCount;

    private static CheckBox checkboxBg;
    private boolean isCheckBox = false;
    private int categoryPosition = 0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_visit_reception, container, false);
        init(view);
        return view;
    }

    /**
     * init 회원정보 바인딩
     */
    private void init(View view) {
        try {
            new Thread(new Runnable() {
                public void run() {
                    final ManagerGetVisitOrder managerGetVisitOrderALL = VisitReceptionManager.getGetVisitOrder((AppCompatActivity)getActivity(), "ALL", SettingLogic.getStoreUserId(getActivity()), "", "");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            getVisitOrderListALL = managerGetVisitOrderALL.getGetVisitOrder();
                            storeUserInfoItemList = ManagerStoreUserInfo.getmInstance().getStoreUserInfo();

                            setLayout(view);
                            setBtnClickListener();
                        }
                    });
                }
            }).start();
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setLayout(View view) {
        checkboxBg = (CheckBox) view.findViewById(R.id.checkbox_bg);
        rlAll = (LinearLayout) view.findViewById(R.id.rl_all);
        rlDeliveryCompleted = (LinearLayout) view.findViewById(R.id.rl_delivery_completed);
        rlUnreleased = (LinearLayout) view.findViewById(R.id.rl_unreleased);
        rlAttemptedDelivery = (LinearLayout) view.findViewById(R.id.rl_attempted_delivery);

        tvAllCount = (TextView) view.findViewById(R.id.tv_all_count);
        tvDeliveryCompletedCount = (TextView) view.findViewById(R.id.tv_delivery_completed_count);
        tvUnreleasedCount = (TextView) view.findViewById(R.id.tv_unreleased_count);
        tvAttemptedCount = (TextView) view.findViewById(R.id.tv_attempted_count);

        tvAll = (TextView) view.findViewById(R.id.tv_all);
        tvDeliveryCompleted = (TextView) view.findViewById(R.id.tv_delivery_completed);
        tvUnreleased = (TextView) view.findViewById(R.id.tv_unreleased);
        tvAttemptedDelivery = (TextView) view.findViewById(R.id.tv_attempted_delivery);
        LinearLayout llSetView = (LinearLayout) view.findViewById(R.id.ll_set_view);

        if (getVisitOrderListALL.size() == 0) {
            llSetView.setVisibility(View.VISIBLE);
        } else {
            try {
                llSetView.setVisibility(View.GONE);
                FragmentManager fragmentManager = getFragmentManager();
                android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                FragmentAll fragmentAll = new FragmentAll(OnArticleSelectedListener, isCheckBox);
                fragmentTransaction.replace(R.id.fragment_item_registation, fragmentAll);
                fragmentTransaction.commit();
            } catch (IllegalStateException err) {
                err.printStackTrace();
            }
        }
        setClickView();
        setTextView();
    }

    private void setBtnClickListener() {
        rlAll.setOnClickListener(movePageListener);
        rlDeliveryCompleted.setOnClickListener(movePageListener);
        rlUnreleased.setOnClickListener(movePageListener);
        rlAttemptedDelivery.setOnClickListener(movePageListener);
        checkboxBg.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    private void setTextView() {
        try {
            tvAllCount.setText("(" + String.valueOf(storeUserInfoItemList.get(0).getData().getTotalOrderCount()) + ")");
            tvDeliveryCompletedCount.setText("(" + String.valueOf(storeUserInfoItemList.get(0).getData().getOutOrderCount()) + ")");
            tvUnreleasedCount.setText("(" + String.valueOf(storeUserInfoItemList.get(0).getData().getNotOutOrderCount()) + ")");
            tvAttemptedCount.setText("(" + String.valueOf(storeUserInfoItemList.get(0).getData().getOutNonPayOrderCount()) + ")");
        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            FragmentManager fragmentManager = getFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if (categoryPosition == 0) {
                FragmentAll fragmentAll = new FragmentAll(OnArticleSelectedListener, isChecked);
                fragmentTransaction.replace(R.id.fragment_item_registation, fragmentAll);
                fragmentTransaction.commit();
            } else if (categoryPosition == 1) {
                FragmentDeliveryCompleted fragmentDeliveryCompleted = new FragmentDeliveryCompleted(OnArticleSelectedListener, isCheckBox);
                fragmentTransaction.replace(R.id.fragment_item_registation, fragmentDeliveryCompleted);
                fragmentTransaction.commit();
            } else if (categoryPosition == 2) {
                FragmentUnreleased fragmentUnreleased = new FragmentUnreleased(OnArticleSelectedListener);
                fragmentTransaction.replace(R.id.fragment_item_registation, fragmentUnreleased);
                fragmentTransaction.commit();
            } else if (categoryPosition == 3) {
                FragmentAttemptedDelivery fragmentAttemptedDelivery = new FragmentAttemptedDelivery(OnArticleSelectedListener);
                fragmentTransaction.replace(R.id.fragment_item_registation, fragmentAttemptedDelivery);
                fragmentTransaction.commit();
            }
        }
    };

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FragmentManager fragmentManager = getFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            switch (v.getId()) {
                case R.id.rl_all:
                    categoryPosition = 0;
                    setClickView();
                    FragmentAll fragmentAll = new FragmentAll(OnArticleSelectedListener, isCheckBox);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentAll);
                    fragmentTransaction.commit();
                    break;

                case R.id.rl_delivery_completed:
                    categoryPosition = 1;
                    setClickView();
                    FragmentDeliveryCompleted fragmentDeliveryCompleted = new FragmentDeliveryCompleted(OnArticleSelectedListener, isCheckBox);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentDeliveryCompleted);
                    fragmentTransaction.commit();
                    break;

                case R.id.rl_unreleased:
                    categoryPosition = 2;
                    setClickView();
                    FragmentUnreleased fragmentUnreleased = new FragmentUnreleased(OnArticleSelectedListener);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentUnreleased);
                    fragmentTransaction.commit();
                    break;

                case R.id.rl_attempted_delivery:
                    categoryPosition = 3;
                    setClickView();
                    FragmentAttemptedDelivery fragmentAttemptedDelivery = new FragmentAttemptedDelivery(OnArticleSelectedListener);
                    fragmentTransaction.replace(R.id.fragment_item_registation, fragmentAttemptedDelivery);
                    fragmentTransaction.commit();
                    break;
            }
        }
    };

    private void setClickView() {
        switch (categoryPosition) {
            case 0:
                rlAll.setBackgroundResource(R.color.color_43425c);
                rlDeliveryCompleted.setBackgroundResource(R.drawable.a_table_view_round);
                rlUnreleased.setBackgroundResource(R.drawable.a_table_view_round);
                rlAttemptedDelivery.setBackgroundResource(R.drawable.a_table_view_round);

                tvAll.setTextColor(getResources().getColor(R.color.Wite_color));
                tvDeliveryCompleted.setTextColor(getResources().getColor(R.color.color_43425c));
                tvUnreleased.setTextColor(getResources().getColor(R.color.color_43425c));
                tvAttemptedDelivery.setTextColor(getResources().getColor(R.color.color_43425c));

                tvAllCount.setTextColor(getResources().getColor(R.color.Wite_color));
                tvDeliveryCompletedCount.setTextColor(getResources().getColor(R.color.color_43425c));
                tvUnreleasedCount.setTextColor(getResources().getColor(R.color.color_43425c));
                tvAttemptedCount.setTextColor(getResources().getColor(R.color.color_43425c));
                break;

            case 1:
                rlDeliveryCompleted.setBackgroundResource(R.color.color_43425c);
                rlAll.setBackgroundResource(R.drawable.a_table_view_round);
                rlUnreleased.setBackgroundResource(R.drawable.a_table_view_round);
                rlAttemptedDelivery.setBackgroundResource(R.drawable.a_table_view_round);

                tvDeliveryCompleted.setTextColor(getResources().getColor(R.color.Wite_color));
                tvAll.setTextColor(getResources().getColor(R.color.color_43425c));
                tvUnreleased.setTextColor(getResources().getColor(R.color.color_43425c));
                tvAttemptedDelivery.setTextColor(getResources().getColor(R.color.color_43425c));

                tvDeliveryCompletedCount.setTextColor(getResources().getColor(R.color.Wite_color));
                tvAllCount.setTextColor(getResources().getColor(R.color.color_43425c));
                tvUnreleasedCount.setTextColor(getResources().getColor(R.color.color_43425c));
                tvAttemptedCount.setTextColor(getResources().getColor(R.color.color_43425c));
                break;

            case 2:
                rlUnreleased.setBackgroundResource(R.color.color_43425c);
                rlAll.setBackgroundResource(R.drawable.a_table_view_round);
                rlDeliveryCompleted.setBackgroundResource(R.drawable.a_table_view_round);
                rlAttemptedDelivery.setBackgroundResource(R.drawable.a_table_view_round);

                tvUnreleased.setTextColor(getResources().getColor(R.color.Wite_color));
                tvAll.setTextColor(getResources().getColor(R.color.color_43425c));
                tvDeliveryCompleted.setTextColor(getResources().getColor(R.color.color_43425c));
                tvAttemptedDelivery.setTextColor(getResources().getColor(R.color.color_43425c));

                tvUnreleasedCount.setTextColor(getResources().getColor(R.color.Wite_color));
                tvAllCount.setTextColor(getResources().getColor(R.color.color_43425c));
                tvDeliveryCompletedCount.setTextColor(getResources().getColor(R.color.color_43425c));
                tvAttemptedCount.setTextColor(getResources().getColor(R.color.color_43425c));
                break;

            case 3:
                rlAttemptedDelivery.setBackgroundResource(R.color.color_43425c);
                rlAll.setBackgroundResource(R.drawable.a_table_view_round);
                rlDeliveryCompleted.setBackgroundResource(R.drawable.a_table_view_round);
                rlUnreleased.setBackgroundResource(R.drawable.a_table_view_round);

                tvAttemptedDelivery.setTextColor(getResources().getColor(R.color.Wite_color));
                tvAll.setTextColor(getResources().getColor(R.color.color_43425c));
                tvDeliveryCompleted.setTextColor(getResources().getColor(R.color.color_43425c));
                tvUnreleased.setTextColor(getResources().getColor(R.color.color_43425c));

                tvAttemptedCount.setTextColor(getResources().getColor(R.color.Wite_color));
                tvAllCount.setTextColor(getResources().getColor(R.color.color_43425c));
                tvDeliveryCompletedCount.setTextColor(getResources().getColor(R.color.color_43425c));
                tvUnreleasedCount.setTextColor(getResources().getColor(R.color.color_43425c));
                break;
        }
    }

    public interface OnArticleSelectedListener {
        void onAllCategory(List<GetVisitOrderList> getVisitOrderLists);    //전체

        void onAllCategoryStatus(boolean status);                          //전체선택 해지

        void onDeliveryCompleted(String laundryCharge, int money);         //출고완료

        void onUnreleased(String HowToWash);                               //미출고

        void onAttemptedDelivery(ArrayList<ItemRepairFree> RepairFee);     //출고미수
    }

    public static OnArticleSelectedListener OnArticleSelectedListener = new OnArticleSelectedListener() {
        @Override
        public void onAllCategory(List<GetVisitOrderList> getVisitOrderLists) {
            makePaymentList = getVisitOrderLists;
        }

        @Override
        public void onAllCategoryStatus(boolean status) {
            if (status == false) {
                checkboxBg.setBackgroundResource(R.drawable.ic_select_all_normal);
            }
        }

        @Override
        public void onDeliveryCompleted(String laundryCharge, int money) {
        }

        @Override
        public void onUnreleased(String HowToWash) {
        }

        @Override
        public void onAttemptedDelivery(ArrayList<ItemRepairFree> RepairFee) {
        }
    };


}
