package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityEditItem;
import rewhite.shopplus.client.manager.ManagerStoreItemList;
import rewhite.shopplus.client.model.StoreItemList;
import rewhite.shopplus.client.network.LaundryRegistrationManager;
import rewhite.shopplus.data.dto.ItemComponent;
import rewhite.shopplus.data.manager.ManagerComponentEdit;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterComponent;
import rewhite.shopplus.view.adapter.AdapterComponentEdit;


@SuppressLint("ValidFragment")
public class FragmentEditItemComponent extends Fragment {
    private static final String TAG = "FragmentEditItemComponent";

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private WaitCounter mWait;
    private static String mSelected;
    private ActivityEditItem.OnArticleSelectedListener mListener;

    @SuppressLint("ValidFragment")
    public FragmentEditItemComponent(ActivityEditItem.OnArticleSelectedListener onArticleSelectedListener) {
        this.mListener = onArticleSelectedListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_component, null);
        mWait = new WaitCounter(getActivity());
        mWait.show();

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        setListData();
    }

    private void setListData() {
        new Thread(new Runnable() {
            public void run() {
                final ManagerStoreItemList managerStoreItemList = LaundryRegistrationManager.getComponentItemList((AppCompatActivity)getActivity(), Long.valueOf("6"));

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        mRecyclerView.setHasFixedSize(true);
                        List<StoreItemList> storeItemList = managerStoreItemList.getStoreItemList();

                        layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
                        mRecyclerView.setLayoutManager(layoutManager);

                        AdapterComponentEdit adapterComponent = new AdapterComponentEdit(getActivity(), onCompletItem, mOnAdapterListner, mSelected);
                        adapterComponent.addItem(storeItemList);
                        mRecyclerView.setAdapter(adapterComponent);
                    }
                });
            }
        }).start();
    }


    public interface onCompletItem {
        void setSelectItem(String dataNumber, String moeny, int oderItemID);

        void setUnSelectItem(String dataNumber, String moeny);
    }

    public FragmentComponent.onCompletItem onCompletItem = new FragmentComponent.onCompletItem() {
        @Override
        public void setSelectItem(String dataName, String moeny, int oderItemID) {
            ItemComponent itemComponent = new ItemComponent();

            ArrayList<ItemComponent> itemComponentList = ManagerComponentEdit.getInstance().getTComponent();
            itemComponent.setTitleName(dataName);
            itemComponent.setAmount(moeny);
            itemComponent.setOptionItemID(String.valueOf(oderItemID));
            itemComponentList.add(itemComponent);

            ManagerComponentEdit.getInstance().setComponent(itemComponentList);
            mListener.onComponent(itemComponentList);
        }

        @Override
        public void setUnSelectItem(String dataNumber, String moeny) {
            ArrayList<ItemComponent> itemComponentList = ManagerComponentEdit.getInstance().getTComponent();

            for (ItemComponent itemRepairFree : new ArrayList<>(itemComponentList)) {
                if (dataNumber.equals(itemRepairFree.getTitleName())) {
                    itemComponentList.remove(itemRepairFree);
                }
            }
            ManagerComponentEdit.getInstance().setComponent(itemComponentList);
            mListener.onComponent(itemComponentList);
        }
    };


    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };
    public static void setSelected(String type){
        mSelected = type;
    }
}
