package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetMileageHistory;
import rewhite.shopplus.client.model.GetMileageHistory;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterMileage;

/**
 * 회원상세 > 내역 조회 > 마일리지 View
 */
@SuppressLint("ValidFragment")
public class FragmentMileage extends Fragment {
    private static final String TAG = "FragmentMileage";

    private AppCompatActivity mActivity;
    private ListView lvMemberList;
    private int mUserStoreId;
    private WaitCounter mWait;
    private LinearLayout llUnsearch;

    public FragmentMileage(AppCompatActivity activity, int userStoreId) {
        this.mActivity = activity;
        this.mUserStoreId = userStoreId;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mileage, container, false);

        mWait = new WaitCounter(getActivity());
        lvMemberList = (ListView) view.findViewById(R.id.lv_member_list);
        llUnsearch = (LinearLayout) view.findViewById(R.id.ll_unsearch);

        settingView(view);
        return view;
    }

    /**
     * 데이터 데이터 View
     * @param view
     */
    private void settingView(View view) {
        List<GetMileageHistory> getmileageHistory = ManagerGetMileageHistory.getmInstance().getmileageHistory();
        if (getmileageHistory.size() == 0) {
            llUnsearch.setVisibility(View.VISIBLE);
            lvMemberList.setVisibility(View.GONE);
        } else {
            llUnsearch.setVisibility(View.GONE);
            lvMemberList.setVisibility(View.VISIBLE);

            AdapterMileage adapterMileage = new AdapterMileage(getActivity());
            adapterMileage.addItem(getmileageHistory);

            lvMemberList.setAdapter(adapterMileage);
        }
    }
}