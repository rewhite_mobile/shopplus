package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import rewhite.shopplus.R;
import rewhite.shopplus.activity.ActivityItemRegistration;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterComponent;
import rewhite.shopplus.view.adapter.AdapterFragmentColor;

@SuppressLint("ValidFragment")
public class FragmentColor extends Fragment {
    private static final String TAG = "FragmentColor";

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private WaitCounter mWait;

    private ActivityItemRegistration.OnArticleSelectedListener mListener;

    @SuppressLint("ValidFragment")
    public FragmentColor(ActivityItemRegistration.OnArticleSelectedListener onArticleSelectedListener) {
        this.mListener = onArticleSelectedListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_color, null);

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        mWait = new WaitCounter(getActivity());

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        setListData();
    }

    private void setListData() {
        mWait.show();
        mRecyclerView.setHasFixedSize(true);

        ArrayList<String> colorName = new ArrayList<>();

        layoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);

        AdapterFragmentColor adapterFragmentColor = new AdapterFragmentColor(getActivity(), onCompletItem, mOnAdapterListner);

        colorName.add("f5d228");
        colorName.add("70bf41");
        colorName.add("f38f18");
        colorName.add("b36ae2");
        colorName.add("51a7f9");
        colorName.add("00882b");
        colorName.add("de6a10");
        colorName.add("c82506");
        colorName.add("0364c0");
        colorName.add("0a5d18");
        colorName.add("bd5b0c");
        colorName.add("861001");
        colorName.add("002452");
        colorName.add("a6aaa9");
        colorName.add("570706");
        colorName.add("000000");
        colorName.add("000000");
        colorName.add("000000");
        colorName.add("000000");
        colorName.add("000000");
        adapterFragmentColor.addItem(colorName);
        mRecyclerView.setAdapter(adapterFragmentColor);
        mWait.dismiss();
    }

    public interface onCompletItem {
        void setSelectItem(int position);
    }

    public FragmentColor.onCompletItem onCompletItem = new FragmentColor.onCompletItem() {
        @Override
        public void setSelectItem(int position) {
            mListener.onColor(position);
        }
    };


    private AdapterComponent.CCViewHolder.OnAdapterListner mOnAdapterListner = new AdapterComponent.CCViewHolder.OnAdapterListner() {
        @Override
        public void onClick(int position) {
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus, int position) {

        }
    };

}
