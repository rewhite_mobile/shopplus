package rewhite.shopplus.fragment.sub;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rewhite.shopplus.R;
import rewhite.shopplus.view.adapter.AdapterNonIssueManagement;
import rewhite.shopplus.util.CustomViewPager;

/**
 * 미출고관리 VIew
 */
public class FragmentNonIssueManagement extends Fragment {
    private static final String TAG = "FragmentNonIssueManagement";

    private CustomViewPager customViewPager;
    private TextView tvAttemptedMember, tvCommitmentItem;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nonissue_management, container, false);

        setLayout(view);
        setViewPagerAdapter();
        return view;
    }

    private void setLayout(View view) {
        customViewPager = (CustomViewPager) view.findViewById(R.id.view_pager_nonissue);

        tvAttemptedMember = (TextView) view.findViewById(R.id.tv_attempted);
        tvCommitmentItem = (TextView) view.findViewById(R.id.tv_commitment);
    }


    private void setViewPagerAdapter() {
        customViewPager.setAdapter(new AdapterNonIssueManagement(getActivity().getSupportFragmentManager()));
        customViewPager.setCurrentItem(0);
        setTapBackGround(0);

        tvAttemptedMember.setOnClickListener(movePageListener);
        tvAttemptedMember.setTag(0);
        tvCommitmentItem.setOnClickListener(movePageListener);
        tvCommitmentItem.setTag(1);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            customViewPager.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    /**
     * 탭 UI 변경 분기처리 로직
     *
     * @param tag
     */
    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                tvAttemptedMember.setBackgroundResource(R.drawable.a_button_type_2);
                tvAttemptedMember.setTextColor(getResources().getColor(R.color.color_c7cfe8));

                tvCommitmentItem.setBackgroundResource(R.drawable.a_button_type_3);
                tvCommitmentItem.setTextColor(getResources().getColor(R.color.color_43425c));
                break;

            case 1:
                tvCommitmentItem.setBackgroundResource(R.drawable.a_button_type_2);
                tvCommitmentItem.setTextColor(getResources().getColor(R.color.color_c7cfe8));

                tvAttemptedMember.setBackgroundResource(R.drawable.a_button_type_3);
                tvAttemptedMember.setTextColor(getResources().getColor(R.color.color_43425c));
                break;
        }
    }
}