package rewhite.shopplus.fragment.sub;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerNonPayOrderItemManageList;
import rewhite.shopplus.client.model.UserOperationModel;
import rewhite.shopplus.client.network.OperationsManagement;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterCommitment;

/**
 * 미수품목목록 VIEW
 */
public class FragmentCommitmentItem extends Fragment {
    private static final String TAG = "FragmentCommitmentItem";

    private TextView tvTabAll, tvTabUnreleased, tvTabRelease;
    private TextView tvNonMemberCount, tvItemCount, tvItemTotalPayment, tvItemAttemptedRelease, tvAttemptedDelivery, tvTotalNonPayment;
    private WaitCounter mWait;

    private ListView lvNonItemList;
    private List<UserOperationModel> userManageLists;

    private String searchType = null;
    private View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_commitment_item, container, false);
        mWait = new WaitCounter(getActivity());

        init();
        return view;
    }

    private void init() {
        mWait.show();
        new Thread(new Runnable() {
            public void run() {
                OperationsManagement.getNonPayOrderItemManageList((AppCompatActivity)getActivity(), searchType);
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        mWait.dismiss();
                        userManageLists = ManagerNonPayOrderItemManageList.getmInstance().getNonPayOrderItemManageList();

                        setLayout(view);
                        setClickView();

                        AdapterCommitment adapterMembershipManagement = new AdapterCommitment(getContext());
                        adapterMembershipManagement.addItem(userManageLists);
                        lvNonItemList.setAdapter(adapterMembershipManagement);
                    }
                });
            }
        }).start();
    }

    private void setLayout(View view) {
        int totalPayment = 0;
        int totalNonPayment = 0;
        tvTabAll = (TextView) view.findViewById(R.id.tv_tab_all);
        tvTabUnreleased = (TextView) view.findViewById(R.id.tv_tab_unreleased);
        tvTabRelease = (TextView) view.findViewById(R.id.tv_tab_release);
        lvNonItemList = (ListView) view.findViewById(R.id.tv_non_item_list);

        tvItemAttemptedRelease = (TextView) view.findViewById(R.id.tv_item_attempted_release);
        tvItemCount = (TextView) view.findViewById(R.id.tv_item_count);
        tvNonMemberCount = (TextView) view.findViewById(R.id.tv_non_member_count);
        tvItemTotalPayment = (TextView) view.findViewById(R.id.tv_item_total_payment);
        tvAttemptedDelivery = (TextView) view.findViewById(R.id.tv_attempted_delivery);
        tvTotalNonPayment = (TextView) view.findViewById(R.id.tv_total_non_payment);

        for(UserOperationModel userOperationModel : userManageLists){
            totalPayment += userOperationModel.getStoreItemPrice();
            totalNonPayment += userOperationModel.getNonPayPrice();
        }

        tvItemCount.setText(userManageLists.size() + "건");
        tvItemTotalPayment.setText(totalPayment + "원");
        tvItemAttemptedRelease.setText("");
        tvAttemptedDelivery.setText("");
        tvTotalNonPayment.setText(totalNonPayment);
        tvNonMemberCount.setText(userManageLists.size() + "건의 미수 품목이 있습니다.");
    }

    private void setClickView() {
        tvTabAll.setOnClickListener(tabOnClickListener);
        tvTabRelease.setOnClickListener(tabOnClickListener);
        tvTabUnreleased.setOnClickListener(tabOnClickListener);
    }


    private View.OnClickListener tabOnClickListener = new View.OnClickListener() {


        @Override
        public void onClick(View v) {
            int id = v.getId();

            switch (id) {
                case R.id.tv_tab_all:
                    searchType = null;
                    tvTabAll.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                    tvTabAll.setBackgroundResource(R.drawable.a_button_type_2);

                    tvTabUnreleased.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    tvTabUnreleased.setBackgroundResource(R.drawable.a_button_type_3);

                    tvTabRelease.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    tvTabRelease.setBackgroundResource(R.drawable.a_button_type_3);
                    break;
                case R.id.tv_tab_unreleased:
                    searchType = "NOTOUT";
                    tvNonMemberCount.setText(userManageLists.size() + "건의 미출고 품목이 있습니다.");
                    tvTabAll.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    tvTabAll.setBackgroundResource(R.drawable.a_button_type_3);

                    tvTabUnreleased.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                    tvTabUnreleased.setBackgroundResource(R.drawable.a_button_type_2);

                    tvTabRelease.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    tvTabRelease.setBackgroundResource(R.drawable.a_button_type_3);
                    break;
                case R.id.tv_tab_release:
                    searchType = "OUTFIN";
                    tvNonMemberCount.setText(userManageLists.size() + "건의 출고 품목이 있습니다.");
                    tvTabAll.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    tvTabAll.setBackgroundResource(R.drawable.a_button_type_3);

                    tvTabUnreleased.setTextColor(getActivity().getResources().getColor(R.color.color_43425c));
                    tvTabUnreleased.setBackgroundResource(R.drawable.a_button_type_3);

                    tvTabRelease.setTextColor(getActivity().getResources().getColor(R.color.Wite_color));
                    tvTabRelease.setBackgroundResource(R.drawable.a_button_type_2);
                    break;
            }
            init();
        }
    };
}