package rewhite.shopplus.fragment.sub;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rewhite.shopplus.R;

/**
 * 미출고 품목 목록 View
 */
public class FragmentMissedItemList extends Fragment {
    private static final String TAG = "FragmentMissingMembersList";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_missing_item_list, container, false);

        return view;
    }
}