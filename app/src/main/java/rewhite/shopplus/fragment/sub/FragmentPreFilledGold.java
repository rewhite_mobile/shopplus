package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import rewhite.shopplus.R;
import rewhite.shopplus.util.CustomViewPager;
import rewhite.shopplus.view.adapter.AdapterPreFilledGold;

@SuppressLint("ValidFragment")
public class FragmentPreFilledGold extends Fragment {
    private static final String TAG = "FragmentPreFilledGold";

    private AppCompatActivity mActivity;
    private Button btnChargeHistory, btnUsageHistory;
    private CustomViewPager customViewPager;
    private int mUserStoreId;

    public FragmentPreFilledGold(AppCompatActivity activity, int userStoreId) {
        this.mActivity = activity;
        this.mUserStoreId = userStoreId;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pre_filled_gold, container, false);

        setLayout(view);
        return view;
    }

    private void setLayout(View view) {
        btnChargeHistory = (Button) view.findViewById(R.id.btn_charge_history);
        btnUsageHistory = (Button) view.findViewById(R.id.btn_usage_history);

        customViewPager = (CustomViewPager) view.findViewById(R.id.custom_pre_view_pager);
        setViewPagerAdapter();
    }

    /**
     * ViewPager Setting
     */
    private void setViewPagerAdapter() {
        btnChargeHistory.setOnClickListener(movePageListener);
        btnChargeHistory.setTag(0);
        btnUsageHistory.setOnClickListener(movePageListener);
        btnUsageHistory.setTag(1);

        customViewPager.setAdapter(new AdapterPreFilledGold(getActivity().getSupportFragmentManager(), (AppCompatActivity)getActivity(), mUserStoreId));
        customViewPager.setCurrentItem(0);
    }

    private View.OnClickListener movePageListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int tag = (int) v.getTag();
            customViewPager.setCurrentItem(tag);
            setTapBackGround(tag);
        }
    };

    /**
     * 카테고리 View 변경
     *
     * @param tag
     */
    private void setTapBackGround(int tag) {
        switch (tag) {
            case 0:
                btnChargeHistory.setTextColor(getResources().getColor(R.color.Wite_color));
                btnChargeHistory.setBackgroundResource(R.drawable.a_button_type_2);

                btnUsageHistory.setTextColor(getResources().getColor(R.color.color_43425c));
                btnUsageHistory.setBackgroundResource(R.drawable.a_button_type_3);
                break;

            case 1:
                btnUsageHistory.setTextColor(getResources().getColor(R.color.Wite_color));
                btnUsageHistory.setBackgroundResource(R.drawable.a_button_type_2);

                btnChargeHistory.setTextColor(getResources().getColor(R.color.color_43425c));
                btnChargeHistory.setBackgroundResource(R.drawable.a_button_type_3);

                break;
        }
    }
}
