package rewhite.shopplus.fragment.sub;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import rewhite.shopplus.R;
import rewhite.shopplus.client.manager.ManagerGetPrePaidUseHistory;
import rewhite.shopplus.client.model.GetPrePaidUseHistory;
import rewhite.shopplus.util.WaitCounter;
import rewhite.shopplus.view.adapter.AdapterChargeHistory;

/**
 * 선충전금 사용 내역 View
 */
@SuppressLint("ValidFragment")
public class FragmentChargeHistory extends Fragment {
    private static final String TAG = "FragmentChargeHistory";

    private AppCompatActivity mActivity;
    private ListView lvMemberList;
    private int mUserStoreId;
    private WaitCounter mWait;

    private LinearLayout llUnsearch;

    public FragmentChargeHistory(AppCompatActivity activity, int userStoreId) {
        this.mActivity = activity;
        this.mUserStoreId = userStoreId;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) getActivity().getLayoutInflater().inflate(R.layout.fragment_charge_history, null);

        lvMemberList = (ListView) view.findViewById(R.id.lv_member_list);
        llUnsearch = (LinearLayout) view.findViewById(R.id.ll_unsearch);

        mWait = new WaitCounter(getActivity());

        settingView(view);
        return view;
    }

    private void settingView(View view) {
        List<GetPrePaidUseHistory> getPrePaidUseHistories = ManagerGetPrePaidUseHistory.getmInstance().getGetPrePaidUseHistory();

        if (getPrePaidUseHistories.size() == 0) {
            llUnsearch.setVisibility(View.VISIBLE);
            lvMemberList.setVisibility(View.GONE);
        } else {
            llUnsearch.setVisibility(View.GONE);
            lvMemberList.setVisibility(View.VISIBLE);

            AdapterChargeHistory adapterChargeHistory = new AdapterChargeHistory(getActivity());
            adapterChargeHistory.addItem(getPrePaidUseHistories);

            lvMemberList.setAdapter(adapterChargeHistory);

        }
    }
}
